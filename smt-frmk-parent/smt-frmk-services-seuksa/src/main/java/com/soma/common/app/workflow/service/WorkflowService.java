package com.soma.common.app.workflow.service;

import com.soma.common.app.history.HistoryException;
import com.soma.common.app.workflow.model.*;
import com.soma.common.app.workflow.model.*;
import org.hibernate.criterion.Order;
import org.seuksa.frmk.model.entity.EMainEntity;
import org.seuksa.frmk.service.BaseEntityService;

import java.util.List;



/**
 * 
 * @author kimsuor.seang
 * 
 */
public interface WorkflowService extends BaseEntityService {
	< T extends EntityWkf> WkfHistoConfig getHistoConfig(Class<T> entityWkfClass);

	WkfHistoConfig getHistoConfig(String entityWkfClassName);
	
	WkfHistoConfig getHistoConfig(EMainEntity mainEntity);

	List<EWkfStatus> getWkfStatuses(EWkfFlow flow);

	List<EWkfStatus> getNextWkfStatuses(EWkfFlow flow, EWkfStatus eStatus) throws HistoryException;

	<T extends WkfBaseHistory> void saveHistory(T his) throws HistoryException;

	<T extends WkfBaseHistoryItem> List<T> getHistories(WkfHistoryItemRestriction restrictions) throws HistoryException;

	<T extends WkfBaseHistoryItem> List<T> getHistoriesByFlow(EWkfFlow flow, long entityId) throws HistoryException;
	
	<T extends WkfBaseHistoryItem> List<T> getHistoriesByFlow(EWkfFlow flow, long entityId, Order order) throws HistoryException;

	<T extends WkfBaseHistoryItem> List<T> getHistoriesByFlow(EWkfFlow flow, long entityId, String propertyName) throws HistoryException;

	<T extends WkfBaseHistoryItem> List<T> getHistoriesByFlow(EWkfFlow flow, long entityId, String propertyName, Order order) throws HistoryException;

	<T extends WkfBaseHistoryItem> T getPreviousHistoSubStatus(EWkfFlow flow, long entityId) throws HistoryException;

	<T extends WkfBaseHistoryItem> T getPreviousHistoStatus(EWkfFlow flow, long entityId) throws HistoryException;

	<T extends WkfBaseHistoryItem> T getPreviousHistoProperty(EWkfFlow flow, long entityId, String propertyName) throws HistoryException;
	
	<T extends WkfBaseTempItem> List<T> getTempsByFlow(EWkfFlow flow, long entityId) throws HistoryException;
	
	<T extends WkfBaseTempItem> List<T> getTempsByFlow(EWkfFlow flow, long entityId, String propertyName, Order order) throws HistoryException;
	
	void initHistoConfigCache();

	void init();
	
	void updateValidatedEntityByTemps(EntityWkf entityWkf, Class clazz);
	
	void updateRefusedEntityByTemps(EntityWkf entityWkf, Class clazz);

}
