/**
 * Auto-generated on Tue Dec 22 11:42:32 ICT 2015
 */
package com.soma.common.app.workflow.model;

/**
 * Meta data of WkfBaseHistoryItem
 * @author 
 */
public interface MWkfBaseHistoryItem {

	public final static String WKFHISTORY = "wkfHistory";
	public final static String PROCESSTIME = "processTime";

}
