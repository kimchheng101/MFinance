package com.soma.common.app.cfield.service;

import java.util.List;

import org.seuksa.frmk.service.BaseEntityService;

import com.soma.common.app.cfield.model.CusField;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface CusFieldService extends BaseEntityService {

	
	List<CusField> listFields(long tableId);
	List<CusField> listFields(String tableName);
	
}
