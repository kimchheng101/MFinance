package com.soma.common.app.workflow.service;

import com.soma.common.app.workflow.model.EWkfFlow;
import org.seuksa.frmk.dao.hql.BaseRestrictions;

/**
 * @author kimsuor.seang
 * 
 */
public class WkfFlowRestriction extends BaseRestrictions<EWkfFlow> {
	/** */
	private static final long serialVersionUID = 706489846193494976L;

	private Long flowId;

    /**
     *
     */
    public WkfFlowRestriction() {
        super(EWkfFlow.class);
    }

    @Override
	public void preBuildAssociation() {
    	
	}

    
    /**
     * @see org.seuksa.frmk.mvc.dao.hql.BaseRestrictions#preBuildSpecificCriteria()
     */
    @Override
    public void preBuildSpecificCriteria() {

    	
    }

	/**
	 * @return the flowId
	 */
	public Long getFlowId() {
		return flowId;
	}

	/**
	 * @param flowId the flowId to set
	 */
	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	
    
}
