package com.soma.common.app.workflow.dao;

import org.seuksa.frmk.dao.BaseEntityDao;

import com.soma.common.app.workflow.model.WkfBaseHistory;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface WorkflowDao extends BaseEntityDao {

	<T extends WkfBaseHistory> void saveHistory(T his);
	
}
