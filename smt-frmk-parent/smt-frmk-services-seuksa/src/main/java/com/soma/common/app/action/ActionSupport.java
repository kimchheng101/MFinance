package com.soma.common.app.action;

import java.io.Serializable;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface ActionSupport extends Serializable {

	String execute(Object[] params) throws Exception;

}
