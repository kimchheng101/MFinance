package com.soma.common.app.tools;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface UserSessionKeys extends BaseSessionKeys {
	public static final String KEY_USER_SESSION = "@userSession@";

	public static final String KEY_CONTROL_PROFILE_PRIVILEGES = "@controlProfilePrivileges@";
	public static final String KEY_I18N_LOCALE = "@i18nLocale@";


}
