package com.soma.frmk.security.spring.session;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author kimsuor.seang
 */
public interface InvalidSessionHandler {
	void sessionInvalidated(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException;
}
