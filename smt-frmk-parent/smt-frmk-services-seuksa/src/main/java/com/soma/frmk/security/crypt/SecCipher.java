package com.soma.frmk.security.crypt;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface SecCipher {
	public static final String UTF8 = "UTF8";
	
	public abstract void init();
	public abstract String encrypt(final String in);
	public abstract String decrypt(final String in);
}
