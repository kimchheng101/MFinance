/**
 * Auto-generated on Tue Sep 08 09:21:21 ICT 2015
 */
package com.soma.frmk.security.model;


/**
 * Meta data of com.soma.frmk.security.model.ESecPage
 * @author 
 */
public interface MSecPage {

	public final static String APPLICATION = "application";
	public final static String CONTROL = "control";
	public final static String ENTITYREF = "entityRef";

}
