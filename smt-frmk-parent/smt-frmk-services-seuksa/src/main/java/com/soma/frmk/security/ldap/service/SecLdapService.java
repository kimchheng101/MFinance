package com.soma.frmk.security.ldap.service;

import com.soma.frmk.security.model.SecProfile;
import org.seuksa.frmk.service.BaseEntityService;

import com.soma.frmk.security.ldap.model.LdapUser;
import com.soma.frmk.security.model.SecProfile;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface SecLdapService extends BaseEntityService {
	LdapUser createUser(String login, String rawPwd);
	LdapUser authenticateUser(String login, String rawPwd);
	boolean checkUserByUsername(String username);
	LdapUser loadUserByUsername(String username);
	SecProfile convertToESecProfileFromLdap(String ldapProfile);
}
