package com.soma.frmk.security.spring.encoding;

import org.springframework.security.authentication.encoding.PasswordEncoder;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface SecPasswordEncoder extends PasswordEncoder {
}