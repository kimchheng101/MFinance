package com.soma.frmk.security.dao;

import org.seuksa.frmk.dao.impl.BaseEntityDaoImpl;
import org.springframework.stereotype.Repository;


/**
 * 
 * @author kimsuor.seang
 * @version $Revision$
 */
@Repository
public class SecurityDaoImpl extends BaseEntityDaoImpl implements SecurityDao {

	/**
	 * 
	 */
	public SecurityDaoImpl() {
		
	}

}
