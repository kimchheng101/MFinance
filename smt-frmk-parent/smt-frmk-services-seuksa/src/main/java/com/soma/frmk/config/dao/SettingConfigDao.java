package com.soma.frmk.config.dao;

import java.util.List;

import com.soma.frmk.config.model.SettingConfig;
import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.tools.exception.DaoException;

import com.soma.frmk.config.model.SettingConfig;

/**
 * 
 * @author kimsuor.seang
 * @version $Revision$
 */
public interface SettingConfigDao extends BaseEntityDao {

    List<SettingConfig> list() throws DaoException;

  
}
