package com.soma.frmk.security.service;

import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.security.model.SecUser;

/**
 *
 * @author kimsuor.seang
 */
public interface AuthenticationServiceAware {
    
    public SecUser authenticate(String username, String password);

	void logout();

}
