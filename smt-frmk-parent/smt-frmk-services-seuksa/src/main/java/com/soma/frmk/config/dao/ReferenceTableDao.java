package com.soma.frmk.config.dao;

import java.util.List;

import com.soma.frmk.config.model.RefTable;
import org.hibernate.criterion.Order;
import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.model.entity.EntityRefA;
import org.seuksa.frmk.tools.exception.DaoException;

import com.soma.frmk.config.model.RefTable;

/**
 * 
 * @author kimsuor.seang
 * @version $Revision$
 */
public interface ReferenceTableDao extends BaseEntityDao {

    List<RefTable> getTables(Order order) throws DaoException;

    List<RefTable> getTables() throws DaoException;

    <T extends EntityRefA> List<T> getValues(BaseRestrictions<T> restricton) throws DaoException;

    <T extends EntityRefA> List<T> getValues(Class<T> entityClass) throws DaoException;

    String generateNextSequenceCode(String entitySimpleNames) throws DaoException;

}
