package com.soma.frmk.security.spring.encoding;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface SecFixedSalt {
	String getFixedSalt();
}
