/**
 * 
 */
package com.soma.frmk.security.spring.encoding;

import org.springframework.security.core.userdetails.UserDetails;


/**
 * @author kimsuor.seang
 * 
 */
public interface SaltedUser extends UserDetails {
	Long getId();
	String getLogin();
	String getPasswordSalt();	
	
}
