package com.soma.frmk.config.dao;

import java.util.List;

import com.soma.frmk.config.model.RefData;
import com.soma.frmk.config.model.RefTable;
import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.model.eref.BaseERefData;
import org.seuksa.frmk.tools.exception.DaoException;

import com.soma.frmk.config.model.RefData;
import com.soma.frmk.config.model.RefTable;

/**
 * 
 * @author kimsuor.seang
 * @version $Revision$
 */
public interface RefDataDao extends BaseEntityDao {
	<T extends BaseERefData> List<RefData> getValues(Class<T> refDataClazz) throws DaoException;

	<T extends BaseERefData> List<RefData> getValues(String refDataClazzName) throws DaoException;

	String generateNextSequenceCode(RefTable refTable);

	Long generateNextIde(RefTable refTable);

}