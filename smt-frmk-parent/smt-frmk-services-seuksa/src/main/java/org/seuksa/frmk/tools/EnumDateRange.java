package org.seuksa.frmk.tools;

/**
 * @author kimsuor.seang
 * @version $Revision: 57 $
 */
public enum EnumDateRange {
    TODAY, YESTERDAY,  LAST_WEEK, CURRENT_WEEK, LAST_MONTH, CURRENT_MONTH, LAST_YEAR, CURRENT_YEAR;
}
