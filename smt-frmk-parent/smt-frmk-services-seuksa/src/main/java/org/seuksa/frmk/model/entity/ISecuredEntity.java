package org.seuksa.frmk.model.entity;

import java.util.List;

/**
 * Is a secured Entity
 * 
 * @author kimsuor.seang
 * @version $Revision$
 */
public interface ISecuredEntity {
    List<String> getEncryptedProperties();
}
