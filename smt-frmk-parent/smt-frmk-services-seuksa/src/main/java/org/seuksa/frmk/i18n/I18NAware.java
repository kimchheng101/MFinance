package org.seuksa.frmk.i18n;

/**
 * Aware interface for object to be translated
 *
 * @author kimsuor.seang
 */
public interface I18NAware {
    public String getKey();
}