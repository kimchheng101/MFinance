package org.seuksa.frmk.dao.criteria;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface FilterContent {
	
	public Object[] getFieldValues();
}
