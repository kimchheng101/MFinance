package org.seuksa.frmk.model.entity;

import java.util.List;



/**
 * 
 * @author kimsuor.seang
 * @version $Revision$
 */
public interface StatusRecordListAware {
	List<EStatusRecord> getStatusRecordList();
	void setStatusRecordList(List<EStatusRecord> statusRecordList);
}
