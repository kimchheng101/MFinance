package org.seuksa.frmk.model.entity;

/**
 * Id is a string code
 * 
 * @author kimsuor.seang
 * @version $Revision$
 */
public interface IHasAssignedIdS {
    /**
     * @param code
     */
    void setCode(String code);
}
