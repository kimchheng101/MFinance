package org.seuksa.frmk.model.entity;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface CrudActionAware {
	CrudAction getCrudAction();
	void setCrudAction(CrudAction crudAction);
}
