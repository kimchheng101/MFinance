package org.seuksa.frmk.model.entity;



/**
 * 
 * @author kimsuor.seang
 *
 */
public interface IBasicEntityRef extends Entity {
    String getCode();
    
    String getDesc();

}
