package org.seuksa.frmk.dao.hql;

import java.util.List;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface ColumnInfosAware {
	List<ColumnInfo> getColumnInfos();
}
