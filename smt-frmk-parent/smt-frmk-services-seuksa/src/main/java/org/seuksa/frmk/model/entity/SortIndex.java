package org.seuksa.frmk.model.entity;


/**
 * @author kimsuor.seang
 */
public interface SortIndex {
	
	Integer getSortIndex();
	
}
