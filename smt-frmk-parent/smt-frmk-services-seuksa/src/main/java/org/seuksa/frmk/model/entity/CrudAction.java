package org.seuksa.frmk.model.entity;

/**
 * 
 * @author kimsuor.seang
 *
 */
public enum CrudAction {
	NONE,
	CREATE,
	REFRESH,
	UPDATE,
	DELETE,
	RECYCLE,
	RESTORE
}
