package org.seuksa.frmk.tools.exception;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface ErrorHandlerExceptionAware {
	ErrorHandler getErrorHandler();
}
