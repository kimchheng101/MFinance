package org.seuksa.frmk.model.entity;

import java.util.Date;

/**
 * Id is a Date
 * 
 * @author kimsuor.seang
 * @version $Revision$
 */
public interface IHasAssignedIdDate {
    /**
     * 
     * @param dtId
     */
    void setIdDate(Date dtId);
}
