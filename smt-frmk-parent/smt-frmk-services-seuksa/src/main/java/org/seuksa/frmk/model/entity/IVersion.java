package org.seuksa.frmk.model.entity;

/**
 * Shall implements Hibernate version annotation
 * 
 * @author kimsuor.seang
 * @version $Revision$
 */
public interface IVersion {
	Long getVersion();
	void setVersion(Long version);
}
