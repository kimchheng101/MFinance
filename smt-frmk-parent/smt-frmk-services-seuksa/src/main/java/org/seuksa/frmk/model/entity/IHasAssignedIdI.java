package org.seuksa.frmk.model.entity;

/**
 * Id is an Integer
 * 
 * @author kimsuor.seang
 * @version $Revision$
 */
public interface IHasAssignedIdI {
    /**
     * @param id
     */
    void setId(Long id);
}
