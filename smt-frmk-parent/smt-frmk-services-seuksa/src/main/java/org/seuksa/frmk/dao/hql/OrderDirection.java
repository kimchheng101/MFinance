package org.seuksa.frmk.dao.hql;

/**
 * 
 * @author kimsuor.seang
 *
 */
public enum OrderDirection {
	DESC, ASC;
}
