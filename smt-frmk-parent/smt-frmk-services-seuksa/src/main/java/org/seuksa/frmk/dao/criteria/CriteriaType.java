package org.seuksa.frmk.dao.criteria;


/**
 * 
 * @author kimsuor.seang
 *
 */
public enum CriteriaType {
    AND,
    OR,
}
