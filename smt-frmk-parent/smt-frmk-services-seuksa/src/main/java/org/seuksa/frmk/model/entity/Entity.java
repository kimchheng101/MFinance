package org.seuksa.frmk.model.entity;

import java.io.Serializable;


/**
 * Entity interface
 * 
 * @author kimsuor.seang
 * @version $Revision$
 */
public interface Entity extends Serializable, Cloneable {
	Long getId();
}