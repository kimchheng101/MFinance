package org.seuksa.frmk.model.entity;



/**
 * 
 * @author kimsuor.seang
 *
 */
public interface RefDataId extends Entity {

//    Long getId();
    
    String getCode();

    String getDesc();

    String getDescEn();
    
}
