package org.seuksa.frmk.model.entity;



/**
 * 
 * @author kimsuor.seang
 * @version $Revision$
 */
public interface StatusRecordAware {
	EStatusRecord getStatusRecord();
	void setStatusRecord(EStatusRecord statusRecord);
}
