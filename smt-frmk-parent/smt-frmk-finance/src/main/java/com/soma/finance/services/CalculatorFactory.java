package com.soma.finance.services;

import com.soma.finance.services.exception.InvalidCashFlowLayoutException;
import com.soma.finance.services.impl.BaseCalculatorStrategyImpl;
import com.soma.finance.services.shared.CashFlowLayout;

/**
 * Calculator factory
 * @author kimsuor.seang
 */
public class CalculatorFactory {
	
	/**
	 * @param cashFlowLayout
	 * @return
	 * @throws InvalidCashFlowLayoutException
	 */
	public static Calculator getInstance(CashFlowLayout cashFlowLayout)
			throws InvalidCashFlowLayoutException {
		return getInstance(cashFlowLayout, new BaseCalculatorStrategyImpl());
	}

	/**
	 * @param cashFlowLayout
	 * @param calculatorStrategy
	 * @return
	 * @throws InvalidCashFlowLayoutException
	 */
	public static Calculator getInstance(CashFlowLayout cashFlowLayout, CalculatorStrategy calculatorStrategy)
			throws InvalidCashFlowLayoutException {
		return null;
	}
}
