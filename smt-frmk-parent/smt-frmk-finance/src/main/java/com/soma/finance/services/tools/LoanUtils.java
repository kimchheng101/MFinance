package com.soma.finance.services.tools;

import com.soma.finance.services.shared.system.EFrequency;

/**
 * 
 * @author kimsuor.seang
 *
 */
public final class LoanUtils {
	/**
	 * Get Number Of Periods
	 * @param termInMonth
	 * @param frequency
	 * @return
	 */
	public static int getNumberOfPeriods(int termInMonth, EFrequency frequency) {
		return termInMonth / frequency.getNbMonths();
	}
	
	
}
