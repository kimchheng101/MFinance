/*
 * Created on 21/05/2015.
 */
package com.soma.ersys.vaadin.ui.referential.reftable.list;

import com.soma.ersys.vaadin.ui.referential.reftable.refdata.IRefTableDataForm;
import com.soma.ersys.vaadin.ui.referential.reftable.refdata.IRefTableDataFormRender;
import com.soma.ersys.vaadin.ui.referential.reftable.refdata.IRefTableDataForm;
import com.soma.ersys.vaadin.ui.referential.reftable.refdata.IRefTableDataFormRender;

/**
 * Ref Table Data Form Render.
 * 
 * @author kimsuor.seang
 *
 */
public class RefTableDataFormRender implements IRefTableDataFormRender {

	/** */
	private static final long serialVersionUID = 2892507964597344234L;

	/**
	 * @param clazz
	 */
	@Override
	public IRefTableDataForm renderForm(Class<?> clazz) {
		return null;
	}

}
