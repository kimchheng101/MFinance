package com.soma.ersys.vaadin.ui.scheduler;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface SchedulerConstants {
    String ID = "id";
    String CODE = "code";
    String DESC = "desc";
    String DESC_EN = "descEn";
    String DESC_LOCALE = "descLocale";
    String NAME ="name";
    String NAME_EN ="nameEn";
    String NAME_LOCALE ="nameLocale";

}
