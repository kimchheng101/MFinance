/*
 * Created on 21/05/2015.
 */
package com.soma.ersys.vaadin.ui.referential.reftable.refdata;

import org.seuksa.frmk.i18n.I18N;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.soma.common.app.tools.helper.AppServicesHelper;
import com.soma.frmk.config.model.RefData;
import com.soma.frmk.config.model.RefTable;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

/**
 * RefTableData Pop-up Panel.
 * 
 * @author kimsuor.seang
 *
 */
public class RefTableDataPopupPanel extends Window implements AppServicesHelper {
	/**	 */
	private static final long serialVersionUID = -8135717900914778306L;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private IRefTableDataForm refTableDataForm;

	private Long refTableId;

	/**
	 * 
	 * @param refTableDataTablePanel
	 * @param caption
	 * @param refDataForm
	 */
	public RefTableDataPopupPanel(final RefTableDataTablePanel refTableDataTablePanel, String caption, IRefTableDataForm refDataForm) {
		super(caption);
		this.refTableDataForm = refDataForm;
		
		/**
		 * On Save
		 */
		Button btnSave = new NativeButton(I18N.message(I18N.message("save")),
				new Button.ClickListener() {

					/** */
					private static final long serialVersionUID = -8810879161108212590L;

					@Override
					public void buttonClick(ClickEvent event) {
						if (refTableDataForm.validate()) {
							RefData refData = refTableDataForm.getValue();
							if (refData.getId() == null) {
								RefTable table = ENTITY_SRV.getById(RefTable.class, refTableId);
								refData.setTable(table);
							}
							REFDATA_SRV.saveOrUpdate(refData);
							/*
							 * Do not comment
							 */
//							RefDataToolsSrvRsc.callWsFlushCacheInAllApps();
							refTableDataTablePanel.refreshTable(refTableId);
							close();
						} else {
							refTableDataForm.displayErrors();
						}
					}

				});
		btnSave.setIcon(new ThemeResource("../smt-default/icons/16/save.png"));

		/**
		 * On cancel
		 */
		Button btnCancel = new NativeButton(
				I18N.message(I18N.message("close")),
				new Button.ClickListener() {					
					/**
					 * 
					 */
					private static final long serialVersionUID = 780402777333190539L;

					public void buttonClick(ClickEvent event) {
						close();
					}
				});
		
		btnCancel.setIcon(new ThemeResource("../smt-default/icons/16/close.png"));
		
		NavigationPanel navigationPanel = new NavigationPanel();
		navigationPanel.addButton(btnSave);
		navigationPanel.addButton(btnCancel);

		VerticalLayout contentLayout = new VerticalLayout();
		contentLayout.setSpacing(true);
		contentLayout.addComponent(navigationPanel);
		contentLayout.addComponent(refTableDataForm.getForm());
		
		setModal(true);
		setContent(contentLayout);
		setWidth(500, Unit.PIXELS);
		setHeight(330, Unit.PIXELS);
		setResizable(true);
		UI.getCurrent().addWindow(this);
	}

	/**
	 * Reset Panel
	 */

	public void reset() {
		refTableDataForm.reset();
		
		markAsDirty();
	}

	/**
	 * @param entityId
	 */
	public void assignValues(Long entityId) {
		if (entityId != null) {
			RefData entity = ENTITY_SRV.getById(RefData.class, entityId);
			refTableDataForm.assignValues(entity);
		}
	}

	/**
	 * @return the refTableId
	 */
	public Long getRefTableId() {
		return refTableId;
	}

	/**
	 * @param refTableId the refTableId to set
	 */
	public void setRefTableId(Long refTableId) {
		this.refTableId = refTableId;
	}
	
	/**
	 * 
	 * @param field
	 * @return
	 */
	public Double getDouble(AbstractTextField field) {
		try {
			return Double.valueOf(Double.parseDouble((String) field.getValue()));
		} catch (Exception e) {
			logger.warn(e.getMessage(), e);
		}
		return null;
	}
	
	/**
	 * 
	 * @param field
	 * @return
	 */
	public Integer getInteger(AbstractTextField field) {
		try {
			return Integer.valueOf(Integer.parseInt((String) field.getValue()));
		} catch (Exception e) {
			logger.warn(e.getMessage(), e);
		}
		return null;
	}
}
