/*
 * Created on 21/05/2015.
 */
package com.soma.ersys.vaadin.ui.referential.reftable.list;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;

import com.soma.frmk.config.model.ERefType;
import com.soma.frmk.config.model.RefTable;
import com.soma.frmk.config.service.RefTableRestriction;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.BooleanOptionGroup;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/**
 * RefTable Search Panel.
 * 
 * @author kimsuor.seang
 *
 */
public class RefTableSearchPanel extends AbstractSearchPanel<RefTable> {

	/**	 */
	private static final long serialVersionUID = -7475468649215844920L;

	protected TextField txtSearchText;
	protected BooleanOptionGroup cbReadOnly;
	protected BooleanOptionGroup cbVisible;
	
	/**
	 * 
	 * @param districtTablePanel
	 */
	public RefTableSearchPanel(RefTableTablePanel districtTablePanel) {
		super(null, districtTablePanel);
	}
	
	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#createForm()
	 */
	@Override
	protected Component createForm() {
		txtSearchText = ComponentFactory.getTextField(null, false, 100, 200);

		
		HorizontalLayout optionsLayout = new HorizontalLayout();
	    Label lblIsReadonly = ComponentFactory.getLabel("read.only");
	    Label lblIsVisible = ComponentFactory.getLabel("visible");
		cbReadOnly = new BooleanOptionGroup();
		cbVisible = new BooleanOptionGroup(Boolean.TRUE);
		optionsLayout.addComponent(lblIsReadonly);
		optionsLayout.addComponent(new Label("  ", ContentMode.PREFORMATTED));
		optionsLayout.addComponent(cbReadOnly);
		optionsLayout.addComponent(new Label("      ", ContentMode.PREFORMATTED));
		optionsLayout.addComponent(lblIsVisible);
		optionsLayout.addComponent(new Label("  ", ContentMode.PREFORMATTED));
		optionsLayout.addComponent(cbVisible);
		optionsLayout.setComponentAlignment(lblIsReadonly, Alignment.MIDDLE_CENTER);
		optionsLayout.setComponentAlignment(lblIsVisible, Alignment.MIDDLE_CENTER);
		cbVisible.setVisible(false);
		lblIsVisible.setVisible(false);

	    final GridLayout criteriaLayout = new GridLayout(4, 2);
	    criteriaLayout.setSpacing(true);
	    Label lblSearchText = ComponentFactory.getLabel("search.text");
	    criteriaLayout.addComponent(lblSearchText);
		criteriaLayout.addComponent(txtSearchText);
		criteriaLayout.addComponent(new Label("      ", ContentMode.PREFORMATTED));
		criteriaLayout.addComponent(optionsLayout);
		criteriaLayout.setComponentAlignment(lblSearchText, Alignment.MIDDLE_CENTER);
		
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.addComponent(criteriaLayout);
		mainLayout.addComponent(new Label("  ", ContentMode.PREFORMATTED));
        
		return mainLayout;
	}

	
	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#getRestrictions()
	 */
	@Override
	public BaseRestrictions<RefTable> getRestrictions() {
		RefTableRestriction restrictions = new RefTableRestriction(RefTable.class);		
		List<Criterion> criterions = new ArrayList<Criterion>();
		
		if(StringUtils.isNotEmpty(txtSearchText.getValue())) {
			restrictions.setDesc(txtSearchText.getValue());
		}
		Collection<Boolean> selectedReadonlyItems = (Collection<Boolean>) cbReadOnly.getValue();
		if (selectedReadonlyItems.size() != 2) {
			restrictions.setIsReadonly(selectedReadonlyItems.contains(Boolean.TRUE));
		}
		Collection<Boolean> selectedVisibleItems = (Collection<Boolean>) cbVisible.getValue();
		if (selectedVisibleItems.size() != 2) {
			restrictions.setIsVisible(selectedVisibleItems.contains(Boolean.TRUE));
		}
		
		criterions.add(Restrictions.eq(RefTable.TYPE, ERefType.REFDATA));
		
		restrictions.setCriterions(criterions);
		restrictions.addOrder(Order.asc(RefTable.ID));
		return restrictions;
	}

	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#reset()
	 */
	@Override
	protected void reset() {
		txtSearchText.setValue("");
		cbReadOnly.setValue(false);
	}
}
