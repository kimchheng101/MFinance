/*
 * Created on 22/06/2015.
 */
package com.soma.ersys.vaadin.ui.workflow;

import javax.annotation.PostConstruct;

import com.soma.ersys.vaadin.ui.workflow.item.WorkFlowItemTablePanel;
import com.soma.ersys.vaadin.ui.workflow.status.WorkFlowStatusTablePanel;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ru.xpoft.vaadin.VaadinView;

import com.soma.ersys.vaadin.ui.workflow.item.WorkFlowItemTablePanel;
import com.soma.ersys.vaadin.ui.workflow.status.WorkFlowStatusTablePanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.SelectedTabChangeListener;

/**
 * 
 * @author kimsuor.seang
 *
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(WorkFlowHolderPanel.NAME)
public class WorkFlowHolderPanel extends AbstractTabsheetPanel implements View {
	/** */
	private static final long serialVersionUID = -535053372024287048L;

	public static final String NAME = "workflows";
	
	@Autowired
	private WorkFlowTablePanel tablePanel;
	@Autowired
	private WorkFlowFormPanel formPanel;
	@Autowired
	private WorkFlowStatusTablePanel statusTablePanel;
	@Autowired
	private WorkFlowItemTablePanel itemTablePanel;
	
	@Override
	public void enter(ViewChangeEvent event) { }
	
	@PostConstruct
	public void PostConstruct() {
		super.init();
		tablePanel.setMainPanel(this);
		formPanel.setCaption(I18N.message("detail"));
		statusTablePanel.setCaption(I18N.message("workflow.status"));
		itemTablePanel.setCaption(I18N.message("workflow.items"));
		getTabSheet().setTablePanel(tablePanel);
		
		getTabSheet().addSelectedTabChangeListener(new SelectedTabChangeListener() {
			/** */
			private static final long serialVersionUID = -5587887484824043025L;

			@Override
			public void selectedTabChange(SelectedTabChangeEvent event) {
				if (event.getTabSheet().getSelectedTab().equals(itemTablePanel)) {
					itemTablePanel.assignValuesToControls(tablePanel.getItemSelectedId());
				}
			}
		});
	}

	@Override
	public void onAddEventClick() {
		formPanel.reset();
		getTabSheet().addFormPanel(formPanel);
		getTabSheet().setSelectedTab(formPanel);
	}

	@Override
	public void onEditEventClick() {
		statusTablePanel.assignValuesToControls(tablePanel.getItemSelectedId());
		getTabSheet().addFormPanel(formPanel);
		getTabSheet().addFormPanel(statusTablePanel);
		getTabSheet().addFormPanel(itemTablePanel);
		initSelectedTab(formPanel);
	}

	@Override
	public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
		if (selectedTab == formPanel) {
			formPanel.assignValues(tablePanel.getItemSelectedId());
		} else if (selectedTab == tablePanel && getTabSheet().isNeedRefresh()) {
			tablePanel.refresh();
		} 
		getTabSheet().setSelectedTab(selectedTab);
	}

}
