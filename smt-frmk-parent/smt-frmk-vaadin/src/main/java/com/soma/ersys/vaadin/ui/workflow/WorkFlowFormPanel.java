/*
 * Created on 22/06/2015.
 */
package com.soma.ersys.vaadin.ui.workflow;

import javax.annotation.PostConstruct;

import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.EntityFactory;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.soma.common.app.workflow.model.EWkfFlow;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/**
 * 
 * @author kimsuor.seang
 *
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WorkFlowFormPanel extends AbstractFormPanel {
	/** */
	private static final long serialVersionUID = -3096847346299943785L;

	private EWkfFlow entity;
	
	private TextField txtCode;
	private TextField txtDesc;
    private TextField txtDescEn;
    private CheckBox cbActive;
    
    @PostConstruct
	public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
		navigationPanel.addSaveClickListener(this);
	}

	@Override
	protected com.vaadin.ui.Component createForm() {
		
		txtCode = ComponentFactory.getTextField("code", true, 60, 200);
		txtDescEn = ComponentFactory.getTextField("desc.en", true, 60, 200);        
		txtDesc = ComponentFactory.getTextField35("desc", false, 60, 200);	
		cbActive = new CheckBox(I18N.message("active"));
        cbActive.setValue(true);

        final FormLayout formPanel = new FormLayout();
        formPanel.addComponent(txtCode);
        formPanel.addComponent(txtDesc);
        formPanel.addComponent(txtDescEn);
        formPanel.addComponent(cbActive);
        
		VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        verticalLayout.setSpacing(true);
        verticalLayout.addComponent(formPanel);
        
        Panel mainPanel = ComponentFactory.getPanel();        
		mainPanel.setContent(verticalLayout);        
		return mainPanel;
	}

	@Override
	protected Entity getEntity() {
		entity.setCode(txtCode.getValue());
		entity.setDesc(txtDesc.getValue());
		entity.setDescEn(txtDescEn.getValue());
		entity.setStatusRecord(cbActive.getValue() ? EStatusRecord.ACTIV : EStatusRecord.INACT);
		return entity;
	}
	
	/**
	 * @param asmakId
	 */
	public void assignValues(Long id) {
		super.reset();
		if (id != null) {
			entity = ENTITY_SRV.getById(EWkfFlow.class, id);
			txtCode.setValue(entity.getCode());
			txtDescEn.setValue(entity.getDescEn());
			txtDesc.setValue(entity.getDesc());
			cbActive.setValue(entity.getStatusRecord().equals(EStatusRecord.ACTIV));
		}
	}
	
	/**
	 * Reset
	 */
	@Override
	public void reset() {
		super.reset();
		entity = EntityFactory.createInstance(EWkfFlow.class);
		txtCode.setValue("");
		txtDescEn.setValue("");
		txtDesc.setValue("");
		cbActive.setValue(true);
		
		markAsDirty();
	}
	
	/**
	 * @return
	 */
	@Override
	protected boolean validate() {
		checkMandatoryField(txtCode, "code");		
		checkMandatoryField(txtDescEn, "desc.en");
		return errors.isEmpty();
	}
}
