package com.soma.frmk.vaadin.util;

import org.seuksa.frmk.tools.spring.SpringUtils;

import com.soma.common.app.tools.helper.AppServicesHelper;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface VaadinServicesHelper extends AppServicesHelper {
	VaadinSessionManager VAADIN_SESSION_MNG = SpringUtils.getBean("myVaadinSessionManager");

}
