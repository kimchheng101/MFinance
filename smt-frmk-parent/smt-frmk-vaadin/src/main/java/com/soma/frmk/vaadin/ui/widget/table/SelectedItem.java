package com.soma.frmk.vaadin.ui.widget.table;

import com.vaadin.data.Item;

/**
 * @author kimsuor.seang
 */
public interface SelectedItem {
	Item getSelectedItem();
}  