package com.soma.frmk.vaadin.ui.widget.wizards.event;

import com.soma.frmk.vaadin.ui.widget.wizards.Wizard;

public class WizardCompletedEvent extends AbstractWizardEvent {

    /**	 */
	private static final long serialVersionUID = 6413822386488685411L;

	public WizardCompletedEvent(Wizard source) {
        super(source);
    }

}
