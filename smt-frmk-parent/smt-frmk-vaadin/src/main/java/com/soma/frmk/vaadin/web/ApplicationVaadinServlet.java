package com.soma.frmk.vaadin.web;

import javax.servlet.ServletException;

import com.soma.frmk.vaadin.web.listener.VaadinSessionInitListener;
import ru.xpoft.vaadin.SpringVaadinServlet;

import com.soma.frmk.vaadin.web.listener.VaadinSessionInitListener;

/**
 * 
 * @author kimsuor.seang
 *
 */
public class ApplicationVaadinServlet extends SpringVaadinServlet {
	/** */
	private static final long serialVersionUID = -1351470317930079824L;

	@Override
    protected final void servletInitialized() throws ServletException {
        super.servletInitialized();
        getService().addSessionInitListener(new VaadinSessionInitListener());
    }
}