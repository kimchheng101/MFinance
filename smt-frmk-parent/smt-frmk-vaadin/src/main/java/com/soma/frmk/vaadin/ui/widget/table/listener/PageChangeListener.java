package com.soma.frmk.vaadin.ui.widget.table.listener;

import org.seuksa.frmk.model.entity.Entity;

import com.soma.frmk.vaadin.ui.widget.table.event.PagedTableChangeEvent;



/**
 * Page change listener
 * @author kimsuor.seang
 *
 */
public interface PageChangeListener <T extends Entity> {
    public void pageChanged(PagedTableChangeEvent<T> event);
}
