package com.soma.frmk.vaadin.ui.widget.toolbar;

import com.soma.common.app.tools.helper.AppConfigFileHelper;
import com.soma.frmk.vaadin.ui.widget.toolbar.event.AddClickListener;
import com.soma.frmk.vaadin.util.i18n.I18N;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.NativeButton;

/**
 * Add click listener
 * @author kimsuor.seang
 */
public class AddClickButton extends NativeButton {
	private static final long serialVersionUID = 1361495121996626709L;
	
	/**
	 * @param listener
	 */
	public AddClickButton(final AddClickListener listener) {
		super(I18N.message("add"));
		
		if (AppConfigFileHelper.isFontAwesomeIcon()) {
        	setIcon(FontAwesome.PLUS);
        }
        else {
        	setIcon(new ThemeResource("../smt-default/icons/16/add.png"));
        }
		addClickListener(new ClickListener() {
			private static final long serialVersionUID = -946176322406700098L;
			@Override
             public void buttonClick(ClickEvent event) {
				listener.addButtonClick(event);
             }
         });
	}
}
