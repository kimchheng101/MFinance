package com.soma.frmk.vaadin.ui.widget.toolbar;

import com.soma.common.app.tools.helper.AppConfigFileHelper;
import com.soma.frmk.vaadin.ui.widget.toolbar.event.SaveClickListener;
import com.soma.frmk.vaadin.util.i18n.I18N;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.NativeButton;

/**
 * Save click listener
 * @author kimsuor.seang
 */
public class SaveClickButton extends NativeButton {
	private static final long serialVersionUID = -7962536477684472233L;

	/**
	 * @param listener
	 */
	public SaveClickButton(final SaveClickListener listener) {
		super(I18N.message("save"));

		if (AppConfigFileHelper.isFontAwesomeIcon()) {
        	setIcon(FontAwesome.SAVE);
        }
        else {
        	setIcon(new ThemeResource("../smt-default/icons/16/save.png"));
        }
		addClickListener(new ClickListener() {
			private static final long serialVersionUID = -946176322406700098L;
			
			@Override
             public void buttonClick(ClickEvent event) {
				listener.saveButtonClick(event);
             }
         });
	}
}
