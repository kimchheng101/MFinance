package com.soma.frmk.vaadin.ui.widget.table;

import org.seuksa.frmk.model.entity.Entity;


/**
 * Paged table
 * @author kimsuor.seang
 */
public interface PagedTable<T extends Entity> {
	
    /**
     * Get current page index
     * @return
     */
    int getCurrentPage();

    /**
     * Get total amount of pages
     * @return
     */
    int getTotalAmountOfPages();
    
}