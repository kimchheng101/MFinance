package com.soma.frmk.vaadin.ui.widget.tabsheet;

import org.seuksa.frmk.model.entity.Entity;


/**
 * 
 * @author kimsuor.seang
 * @version $Revision$
 */
public interface DisplayEntityInfo {
	
	void updateDisplayInfo(Entity entity);
	
}
