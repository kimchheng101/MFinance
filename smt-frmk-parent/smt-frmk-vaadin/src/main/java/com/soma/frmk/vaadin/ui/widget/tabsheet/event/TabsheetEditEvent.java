package com.soma.frmk.vaadin.ui.widget.tabsheet.event;

import com.soma.frmk.vaadin.ui.widget.tabsheet.TabSheet;


/**
 * @author kimsuor.seang
 */
public class TabsheetEditEvent extends AbstractTabsheetEvent {

    /**	 */
	private static final long serialVersionUID = 1468373790677796219L;

	public TabsheetEditEvent(TabSheet source) {
        super(source);
    }

}
