package com.soma.frmk.vaadin.ui.factory;

import org.seuksa.frmk.model.entity.Entity;

import com.vaadin.ui.Component;

/**
 * Entity table factory
 * @author kimsuor.seang
 */
public interface EntityTableFactory {
    Component create(final Class <? extends Entity> entityClass, boolean firstLoad);
}
