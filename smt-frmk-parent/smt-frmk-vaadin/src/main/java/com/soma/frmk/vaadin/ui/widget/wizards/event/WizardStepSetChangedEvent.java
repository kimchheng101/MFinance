package com.soma.frmk.vaadin.ui.widget.wizards.event;

import com.soma.frmk.vaadin.ui.widget.wizards.Wizard;

public class WizardStepSetChangedEvent extends AbstractWizardEvent {

    /**	 */
	private static final long serialVersionUID = -414469115462364154L;

	public WizardStepSetChangedEvent(Wizard source) {
        super(source);
    }

}
