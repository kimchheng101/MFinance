package com.soma.frmk.vaadin.ui.widget.toolbar.event;

import java.io.Serializable;

import com.vaadin.ui.Button.ClickEvent;

/**
 * @author kimsuor.seang
 */
public interface EditClickListener extends Serializable {
	void editButtonClick(ClickEvent event);
}
