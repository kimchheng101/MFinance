package com.soma.frmk.vaadin.ui.widget.tabsheet.event;

import com.soma.frmk.vaadin.ui.widget.tabsheet.TabSheet;


/**
 * @author kimsuor.seang
 */
public class TabsheetPrintEvent extends AbstractTabsheetEvent {

    /**	 */
	private static final long serialVersionUID = -862079951688783153L;

	public TabsheetPrintEvent(TabSheet source) {
        super(source);
    }

}
