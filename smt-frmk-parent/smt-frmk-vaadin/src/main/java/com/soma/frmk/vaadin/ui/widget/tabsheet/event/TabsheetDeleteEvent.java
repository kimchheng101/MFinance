package com.soma.frmk.vaadin.ui.widget.tabsheet.event;

import com.soma.frmk.vaadin.ui.widget.tabsheet.TabSheet;


/**
 * @author kimsuor.seang
 */
public class TabsheetDeleteEvent extends AbstractTabsheetEvent {

    /**	 */
	private static final long serialVersionUID = 4468435518003807446L;

	public TabsheetDeleteEvent(TabSheet source) {
        super(source);
    }

}
