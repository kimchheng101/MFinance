package com.soma.frmk.vaadin.util;

import com.soma.common.app.web.session.AppSessionKeys;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface VaadinSessionKeys extends AppSessionKeys {
	public static final String KEY_ROOT_MENU_ITEMS = "@rootMenuItems@";
	public static final String KEY_MAIN_MENU_BAR = "@mainMenuBar@";


}
