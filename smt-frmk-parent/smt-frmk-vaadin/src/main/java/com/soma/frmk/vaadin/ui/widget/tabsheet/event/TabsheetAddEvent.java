package com.soma.frmk.vaadin.ui.widget.tabsheet.event;

import com.soma.frmk.vaadin.ui.widget.tabsheet.TabSheet;



/**
 * @author kimsuor.seang
 */
public class TabsheetAddEvent extends AbstractTabsheetEvent {

    /**	 */
	private static final long serialVersionUID = -3145751838090460821L;

	public TabsheetAddEvent(TabSheet source) {
        super(source);
    }

}
