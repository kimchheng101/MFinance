package com.soma.frmk.vaadin.ui.widget.table;

import java.io.Serializable;

/**
 * @author kimsuor.seang
 *
 */
public interface ColumnRenderer extends Serializable {
	
	Object getValue();
}
