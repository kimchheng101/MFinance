package com.soma.frmk.vaadin.ui.panel;

import org.seuksa.frmk.model.entity.Entity;

import com.soma.frmk.vaadin.ui.factory.EntityTableFactory;
import com.vaadin.ui.TabSheet;


/**
 * Entity tab sheet
 * @author kimsuor.seang
 */
public class EntityTabSheet extends TabSheet {
	private static final long serialVersionUID = -5068755056780262041L;
	    
	/**
	 * 
	 * @param title
	 * @param entityClass
	 * @param entityTableFactory
	 * @param firstLoad
	 */
    public EntityTabSheet(final String title, final Class<? extends Entity> entityClass, final EntityTableFactory entityTableFactory, final boolean firstLoad) {
    	setSizeFull();
    	super.removeAllComponents();
    	addTab(entityTableFactory.create(entityClass, firstLoad), title);
    }
}
