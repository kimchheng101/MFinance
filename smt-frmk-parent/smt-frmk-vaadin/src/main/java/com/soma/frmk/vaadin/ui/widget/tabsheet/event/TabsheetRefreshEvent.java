package com.soma.frmk.vaadin.ui.widget.tabsheet.event;

import com.soma.frmk.vaadin.ui.widget.tabsheet.TabSheet;


/**
 * @author kimsuor.seang
 */
public class TabsheetRefreshEvent extends AbstractTabsheetEvent {

    /**	 */
	private static final long serialVersionUID = -4079995153703969916L;

	public TabsheetRefreshEvent(TabSheet source) {
        super(source);
    }

}
