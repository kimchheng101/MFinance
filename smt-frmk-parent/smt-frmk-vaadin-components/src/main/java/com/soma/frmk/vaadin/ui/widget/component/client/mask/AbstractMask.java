package com.soma.frmk.vaadin.ui.widget.component.client.mask;

/**
 * @author ky.nora
 */
public abstract class AbstractMask implements Mask {

	public char getChar(char character) {
		return character;
	}
}
