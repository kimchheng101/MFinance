package com.soma.ersys.collab.project.dao;

import org.seuksa.frmk.dao.BaseEntityDao;

/**
 * Project Dao
 * @author kimsuor.seang
 *
 */
public interface ProjectDao extends BaseEntityDao {

}
