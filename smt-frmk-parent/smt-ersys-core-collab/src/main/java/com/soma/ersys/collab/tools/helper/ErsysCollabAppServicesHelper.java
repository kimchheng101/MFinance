package com.soma.ersys.collab.tools.helper;

import org.seuksa.frmk.tools.spring.SpringUtils;

import com.soma.common.app.tools.helper.AppServicesHelper;
import com.soma.ersys.collab.project.service.ProjectService;
import com.soma.ersys.collab.project.service.TaskService;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface ErsysCollabAppServicesHelper extends AppServicesHelper {

	ProjectService PROJECT_SRV = SpringUtils.getBean(ProjectService.class);
	TaskService TASK_SRV = SpringUtils.getBean(TaskService.class);

}
