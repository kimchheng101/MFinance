package com.soma.ersys.collab.project.dao.impl;

import org.seuksa.frmk.dao.impl.BaseEntityDaoImpl;
import org.springframework.stereotype.Repository;

import com.soma.ersys.collab.project.dao.ProjectDao;

/**
 * Project Dao Impl
 * @author kimsuor.seang
 *
 */
@Repository
public class ProjectDaoImpl extends BaseEntityDaoImpl implements ProjectDao {

}
