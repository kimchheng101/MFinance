package com.soma.frmk.web.struts.action;

import java.io.Serializable;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface IViewForm extends Serializable {
	boolean validateForm(ActionResult actionResult);
}
