package com.soma.frmk.security.sys;

import com.soma.frmk.security.service.AuthenticationServiceAware;


/**
 * 
 * @author kimsuor.seang
 *
 */
public interface SysAuthenticationService extends AuthenticationServiceAware {
}
