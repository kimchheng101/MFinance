package com.soma.frmk.auditlog.envers.service;

import org.hibernate.envers.AuditReader;
import org.seuksa.frmk.service.BaseEntityService;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface VersioningService extends BaseEntityService {

	AuditReader getAuditReader();

}
