/**
 * 
 */
package com.soma.frmk.auditlog.envers.listener.revision;

import org.hibernate.envers.RevisionListener;
import org.seuksa.frmk.model.entity.EntityA;

import com.soma.common.app.tools.UserSession;
import com.soma.common.app.tools.UserSessionManager;
import com.soma.frmk.auditlog.envers.model.SimpleRevisionEntity;

/**
 * @author kimsuor.seang
 * 
 */
public class SimpleRevisionListener implements RevisionListener {

	@Override
	public void newRevision(Object revisionEntity) {
		SimpleRevisionEntity entity = (SimpleRevisionEntity) revisionEntity;
		if (UserSession.isAuthenticated()) {
			entity.setSecUsrId(UserSessionManager.getCurrentUser().getId());
			entity.setSecUsrLogin(UserSessionManager.getCurrentUser().getLogin());
		} else {
			entity.setSecUsrId(null);
			entity.setSecUsrLogin(EntityA.NOT_AUTHENTICATED_USER);
		}
	}
	


}
