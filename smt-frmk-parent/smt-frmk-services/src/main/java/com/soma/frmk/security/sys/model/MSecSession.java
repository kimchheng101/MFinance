/**
 * Auto-generated on Tue Oct 06 10:28:14 ICT 2015
 */
package com.soma.frmk.security.sys.model;

import org.seuksa.frmk.model.entity.MEntityA;

/**
 * Meta data of SecSession
 * @author 
 */
public interface MSecSession extends MEntityA {

	public final static String USRID = "usrId";
	public final static String STARTTS = "startTS";
	public final static String ENDTS = "endTS";
	public final static String LASTHEARTBEATTS = "lastHeartbeatTS";
	public final static String NBDAYSALREADYUSED = "nbDaysAlreadyUsed";
	public final static String MESSAGE = "message";

}
