package com.soma.frmk.helper;

import com.soma.frmk.helper.SeuksaServicesHelper;
import com.soma.frmk.security.sys.SysAuthenticationService;
import com.soma.frmk.security.sys.service.SecSessionService;
import org.seuksa.frmk.tools.spring.SpringUtils;

import com.soma.frmk.auditlog.envers.service.VersioningService;
import com.soma.frmk.security.service.AuthenticationService;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface FrmkServicesHelper extends SeuksaServicesHelper {
    AuthenticationService AUTHENTICAT_SRV = SpringUtils.getBean(AuthenticationService.class);
    SecSessionService SYS_SESS_SRV = SpringUtils.getBean(SecSessionService.class);
    SysAuthenticationService SYS_AUTH_SRV = SpringUtils.getBean(SysAuthenticationService.class);
    VersioningService VERSION_SRV = SpringUtils.getBean(VersioningService.class);

}
