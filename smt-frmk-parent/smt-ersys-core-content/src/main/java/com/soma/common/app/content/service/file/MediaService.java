/**
 * 
 */
package com.soma.common.app.content.service.file;

import org.seuksa.frmk.service.BaseEntityService;

import com.soma.common.app.content.model.file.FileData;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface MediaService extends BaseEntityService {

	void forceRemoveFileData(Long fileId) throws Exception;
	FileData findFileExist(String fileName);
	
}
