package com.soma.common.app.content.service;

import com.soma.common.app.content.service.file.MediaService;

/**
 * 
 * @author piseth.ing
 *
 */
public interface FileDataService extends MediaService {
}
