package com.soma.common.app.content.service.text;

import java.util.List;

import com.soma.common.app.content.model.doc.Text;
import com.soma.common.app.content.model.doc.TextDependency;
import com.soma.common.app.content.model.eref.ETextDependencyType;
import org.seuksa.frmk.service.MainEntityService;

/**
 * 
 * @author kimsuor.seang
 * 
 */
public interface TextService extends MainEntityService {
	
	public  List<TextDependency> getParents(long texId);

	void createDependency(Text text, Text other, ETextDependencyType type, Integer sortIndex);

	void createDependency(Text text, Text other, ETextDependencyType type);
}
