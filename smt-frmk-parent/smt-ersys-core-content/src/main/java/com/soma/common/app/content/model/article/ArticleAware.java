package com.soma.common.app.content.model.article;

/**
 * 
 * @author kong.phirun
 * @version $Revision$
 */
public interface ArticleAware {	
	Article getArticle();
	void setArticle(Article article);
}
