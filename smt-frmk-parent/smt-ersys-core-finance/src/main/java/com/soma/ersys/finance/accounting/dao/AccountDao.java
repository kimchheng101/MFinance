package com.soma.ersys.finance.accounting.dao;

import org.seuksa.frmk.dao.BaseEntityDao;

/**
 * Account Dao
 * @author kimsuor.seang
 */
public interface AccountDao extends BaseEntityDao {

}
