package com.soma.ersys.finance.accounting.model;

/**
 * Meta data of com.soma.ersys.finance.accounting.model.MAccountLedger
 * @author kimsuor.seang
 */
public interface MAccountLedger {
	
	public final static String JOURNALENTRY = "journalEntry";
	public final static String ACCOUNT = "account";
	public final static String LABEL = "label";
	public final static String DEBIT = "debit";
	public final static String CREDIT = "credit";
	public final static String BALANCE = "balance";
	public final static String WHEN = "when";
	public final static String INFO = "info";
	public final static String OTHERINFO = "otherInfo";

}
