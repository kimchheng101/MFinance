package com.soma.ersys.finance.accounting.model;

/**
 * Meta of com.soma.ersys.finance.accounting.model.MJournalEventAccount
 * @author kimsuor.seang
 */
public interface MJournalEventAccount {
	
	public final static String EVENT = "event";
	public final static String ACCOUNT = "account";
	public final static String ISDEBIT = "isDebit";
	public final static String SORTINDEX = "sortIndex";
	
	
}
