package com.soma.ersys.finance.accounting.dao.impl;

import org.seuksa.frmk.dao.impl.BaseEntityDaoImpl;
import org.springframework.stereotype.Repository;

import com.soma.ersys.finance.accounting.dao.AccountDao;

/**
 * Account Dao Impl
 * @author kimsuor.seang
 */
@Repository
public class AccountDaoImpl extends BaseEntityDaoImpl implements AccountDao {

}
