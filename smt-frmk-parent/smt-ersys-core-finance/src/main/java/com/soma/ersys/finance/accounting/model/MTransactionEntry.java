/**
 * Auto-generated on Tue Nov 17 11:03:58 ICT 2015
 */
package com.soma.ersys.finance.accounting.model;

/**
 * Meta data of com.soma.ersys.accounting.model.JournalEntry
 * @author 
 */
public interface MTransactionEntry {

	public final static String DESC = "desc";
	public final static String DESCEN = "descEn";
	public final static String WKFSTATUS = "wkfStatus";
}
