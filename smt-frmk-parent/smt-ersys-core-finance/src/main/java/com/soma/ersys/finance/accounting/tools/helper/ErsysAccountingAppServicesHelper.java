package com.soma.ersys.finance.accounting.tools.helper;

import org.seuksa.frmk.tools.spring.SpringUtils;

import com.soma.common.app.tools.helper.AppServicesHelper;
import com.soma.ersys.finance.accounting.service.AccountingService;

/**
 * @author kimsuor.seang
 */
public interface ErsysAccountingAppServicesHelper extends AppServicesHelper {
	
	AccountingService ACCOUNTING_SRV = SpringUtils.getBean(AccountingService.class);

}
