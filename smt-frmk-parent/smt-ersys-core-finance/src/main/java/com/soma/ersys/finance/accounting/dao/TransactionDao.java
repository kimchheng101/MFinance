package com.soma.ersys.finance.accounting.dao;

import org.seuksa.frmk.dao.BaseEntityDao;

/**
 * Transaction Dao
 * @author kimsuor.seang
 */
public interface TransactionDao extends BaseEntityDao {

}
