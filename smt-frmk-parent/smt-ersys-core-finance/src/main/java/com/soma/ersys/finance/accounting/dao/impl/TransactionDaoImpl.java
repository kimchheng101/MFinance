package com.soma.ersys.finance.accounting.dao.impl;

import org.seuksa.frmk.dao.impl.BaseEntityDaoImpl;
import org.springframework.stereotype.Repository;

import com.soma.ersys.finance.accounting.dao.TransactionDao;

/**
 * Transaction Dao Implement
 * @author kimsuor.seang
 */
@Repository
public class TransactionDaoImpl extends BaseEntityDaoImpl implements TransactionDao {

}
