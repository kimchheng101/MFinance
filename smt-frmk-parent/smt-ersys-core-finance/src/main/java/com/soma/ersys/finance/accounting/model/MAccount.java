package com.soma.ersys.finance.accounting.model;

import org.seuksa.frmk.model.entity.MEntityRefA;

/**
 * @author kimsuor.seang
 */
public interface MAccount extends MEntityRefA {
	
	public final static String CATEGORY = "category";
	public final static String NAME = "name";
	public final static String NAMEEN = "nameEn";
	public final static String STARTINGBALANCE = "startingBalance";
	public final static String INFO = "info";
	
}
