package com.soma.ersys.finance.accounting.service;

import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.ersys.finance.accounting.model.JournalEntry;
import com.soma.ersys.finance.accounting.model.TransactionEntry;

/**
 * 
 * @author kimsuor.seang
 *
 */
public class TransactionEntryRestriction extends BaseRestrictions<TransactionEntry> {
	/** */
	private static final long serialVersionUID = 7523043853502262131L;

	private EWkfStatus wkfStatus;
	
	/**
	 */
	public TransactionEntryRestriction() {
		super(TransactionEntry.class);
	}
	
	/**
     * 
     */
    @Override
    public void preBuildAssociation() {
   }
    
    /**
     * @see org.seuksa.frmk.dao.hql.BaseRestrictions#preBuildCommunMapCriteria()
     */
    @Override
	public void preBuildSpecificCriteria() {  	
		if (wkfStatus != null) {
			addCriterion(Restrictions.eq(JournalEntry.WKFSTATUS, wkfStatus));
		}
    }

	/**
	 * @return the wkfStatus
	 */
	public EWkfStatus getWkfStatus() {
		return wkfStatus;
	}

	/**
	 * @param wkfStatus the wkfStatus to set
	 */
	public void setWkfStatus(EWkfStatus wkfStatus) {
		this.wkfStatus = wkfStatus;
	}
}
