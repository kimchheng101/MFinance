/**
 * Auto-generated on Fri Nov 27 15:26:57 ICT 2015
 */
package com.soma.ersys.finance.accounting.model;

/**
 * Meta data of com.soma.ersys.finance.accounting.model.JournalEvent
 * @author 
 */
public interface MJournalEvent {

	public final static String JOURNAL = "journal";
	public final static String EVENTGROUP = "eventGroup";

}
