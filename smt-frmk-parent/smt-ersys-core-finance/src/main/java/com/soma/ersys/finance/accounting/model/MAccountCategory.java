package com.soma.ersys.finance.accounting.model;

import org.seuksa.frmk.model.entity.MEntityRefA;

/**
 * @author kimsuor.seang
 */
public interface MAccountCategory extends MEntityRefA {
	
	public final static String PARENT = "parent";
	public final static String ROOT = "root";

}
