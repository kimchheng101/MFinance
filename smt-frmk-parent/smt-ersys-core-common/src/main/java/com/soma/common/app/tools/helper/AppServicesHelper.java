package com.soma.common.app.tools.helper;

import com.soma.common.app.scheduler.service.SchedulerService;
import org.seuksa.frmk.tools.spring.SpringUtils;

import com.soma.common.app.cfield.service.CusFieldService;
import com.soma.common.app.history.HistoryService;
import com.soma.common.app.menu.service.MenuService;
import com.soma.common.app.tools.AppSessionManager;
import com.soma.ersys.core.finance.service.bank.BankService;
import com.soma.ersys.core.hr.service.EmployeeService;
import com.soma.ersys.core.partner.service.PartnerService;
import com.soma.frmk.helper.FrmkServicesHelper;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface AppServicesHelper extends FrmkServicesHelper {
	AppSessionManager APP_SESSION_MNG = SpringUtils.getBean("myAppSessionManager");

	EmployeeService EMPL_SRV = SpringUtils.getBean(EmployeeService.class);
    MenuService MENU_SRV = SpringUtils.getBean(MenuService.class);
    HistoryService HISTO_SRV = SpringUtils.getBean(HistoryService.class);
    CusFieldService CUS_FIELD_SRV = SpringUtils.getBean(CusFieldService.class);
    SchedulerService SCHEDULER_SRV = SpringUtils.getBean(SchedulerService.class);
	BankService BANK_SRV = SpringUtils.getBean(BankService.class);
	PartnerService PARTNER_SRV = SpringUtils.getBean(PartnerService.class);
}
