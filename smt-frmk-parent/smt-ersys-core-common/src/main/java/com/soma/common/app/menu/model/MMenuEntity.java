/**
 * Auto-generated on Thu Nov 12 20:40:11 ICT 2015
 */
package com.soma.common.app.menu.model;

/**
 * Meta data of MenuEntity
 * @author 
 */
public interface MMenuEntity {

	public final static String APPLICATION = "application";
	public final static String TYPE = "type";
	public final static String ISSYNCHRONIZED = "isSynchronized";

}
