package com.soma.common.app.tools;

/**
 * 
 * @author kimsuor.seang
 *
 */
public class AppConstants {

	public static final String BREAK_LINE_TEXT = "\r\n";
	public static final String BREAK_LINE_HTML = "<br\\>";


}
