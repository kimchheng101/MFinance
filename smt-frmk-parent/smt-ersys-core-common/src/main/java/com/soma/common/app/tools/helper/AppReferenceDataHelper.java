package com.soma.common.app.tools.helper;

import com.soma.common.app.eventlog.model.SecEventAction;
import com.soma.frmk.helper.FrmkReferenceDataHelper;

/**
 * 
 * @author kimsuor.seang
 *
 */
public class AppReferenceDataHelper extends FrmkReferenceDataHelper {

	public static SecEventAction getSecEventActionLOGIN() {
		return ENTITY_SRV.getById(SecEventAction.class, SecEventAction.LOGIN);
	}
	public static SecEventAction getSecEventActionLOGOUT() {
		return ENTITY_SRV.getById(SecEventAction.class, SecEventAction.LOGOUT);
	}

}
