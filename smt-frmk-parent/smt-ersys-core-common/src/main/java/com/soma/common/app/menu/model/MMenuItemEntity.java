/**
 * Auto-generated on Thu Nov 12 20:35:40 ICT 2015
 */
package com.soma.common.app.menu.model;

/**
 * Meta data of MenuItemEntity
 * @author 
 */
public interface MMenuItemEntity {

	public final static String MENU = "menu";
	public final static String ACTION = "action";
	public final static String ICONPATH = "iconPath";
	public final static String PARENT = "parent";
	public final static String ISPOPUPWINDOW = "isPopupWindow";
	public final static String CONTROL = "control";

}
