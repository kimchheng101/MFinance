package com.soma.ersys.core.hr.dao.impl;

import org.seuksa.frmk.dao.impl.BaseEntityDaoImpl;
import org.springframework.stereotype.Repository;

import com.soma.ersys.core.hr.dao.OrgStructureDao;

/**
 * 
 * @author kimsuor.seang
 *
 */
@Repository
public class OrgStructureDaoImpl extends BaseEntityDaoImpl implements OrgStructureDao {


}
