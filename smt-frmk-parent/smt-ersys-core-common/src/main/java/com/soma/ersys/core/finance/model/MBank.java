/**
 * Auto-generated on Fri Sep 25 16:34:21 ICT 2015
 */
package com.soma.ersys.core.finance.model;

import org.seuksa.frmk.model.entity.MEntityA;

/**
 * Meta data of com.soma.common.app.finance.model.Bank
 * @author 
 */
public interface MBank extends MEntityA {
	
	public final static String AGENCYCODE = "agencyCode";
	public final static String SWIFT = "swift";
	public final static String ADDRESS = "address";
	public final static String COMMENT = "comment";
	public final static String LICENCENO = "licenceNo";
	public final static String TYPECOMPANY = "typeCompany";
	public final static String COUNTRY = "country";
	

}
