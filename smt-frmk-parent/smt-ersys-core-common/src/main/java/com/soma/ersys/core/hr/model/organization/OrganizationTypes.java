package com.soma.ersys.core.hr.model.organization;

import com.soma.ersys.core.hr.model.eref.ETypeOrganization;
import com.soma.ersys.core.hr.model.eref.ETypeOrganization;

/**
 * 
 * @author kimsuor.seang
 *
 */
public class OrganizationTypes {
	public final static ETypeOrganization MAIN = new ETypeOrganization("MAIN", 1);
	public final static ETypeOrganization INSURANCE= new ETypeOrganization("INSURANCE", 2);
	public final static ETypeOrganization AGENT = new ETypeOrganization("AGENT", 3);
	public final static ETypeOrganization LOCATION = new ETypeOrganization("LOCATION", 4);
	
}

