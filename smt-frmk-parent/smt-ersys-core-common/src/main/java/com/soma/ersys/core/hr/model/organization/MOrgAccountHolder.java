package com.soma.ersys.core.hr.model.organization;

import org.seuksa.frmk.model.entity.MEntityA;

/**
 * Meta data of com.soma.ersys.core.hr.model.organization.OrgAccountHolder
 * @author kimsuor.seang
 */
public interface MOrgAccountHolder extends MEntityA {

	public final static String ORGANIZATION = "organization";
	public final static String ACCOUNTHOLDERID = "accountHolder";
	
}
