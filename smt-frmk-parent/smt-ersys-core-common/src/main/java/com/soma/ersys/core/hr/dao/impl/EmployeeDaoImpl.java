package com.soma.ersys.core.hr.dao.impl;

import org.seuksa.frmk.dao.impl.BaseEntityDaoImpl;
import org.springframework.stereotype.Repository;

import com.soma.ersys.core.hr.dao.EmployeeDao;

/**
 * 
 * @author kimsuor.seang
 *
 */
@Repository
public class EmployeeDaoImpl extends BaseEntityDaoImpl implements EmployeeDao {


}
