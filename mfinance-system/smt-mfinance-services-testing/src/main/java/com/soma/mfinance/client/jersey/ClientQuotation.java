package com.soma.mfinance.client.jersey;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jackson.JacksonFeature;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

/**
 * Created by cheasocheat on 4/3/17.
 */
public class ClientQuotation extends FinWsClient {
    private static final String RSC_LIST = "/quotations";
    private static final String RSC_GET = "/quotations/{id}";
    private static final String RSC_MP_LES_GET = "/quotations/lessee/{reference}";
    private static final String RSC_EF_LES_GET = "/contract/lessee/{reference}";


    /**
     * Default WebService
     */
    public static String getQuotationList(String url) {
        ClientConfig config = new ClientConfig();
        final Client client = ClientBuilder.newClient(config).register(JacksonFeature.class);
        String response = client.target(getServiceURL(url, RSC_LIST)).request(MediaType.TEXT_PLAIN).get(String.class);
        return response;
    }

    /**
     * Get Contract in EF by Rference
     */
    public static Response getEFContractByReference(String url, String reference) {
        ClientConfig config = new ClientConfig();
        final Client client = ClientBuilder.newClient(config).register(JacksonFeature.class);
        System.out.println("====>>> invoke getEFContractByReference(" + url + ", " + reference + ")");
        System.out.println("====>>> url to mfinance=" + getEFServiceURL(url, RSC_EF_LES_GET + "/", reference));
        Response result = client.target(getEFServiceURL(url, RSC_EF_LES_GET + "/", reference))
                .request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .get(Response.class);
        System.out.println("=====>>>> response code=" + result.getStatus());
        return result;
    }

    /**
     * Get getQuotationById
     */
    public static Response getQuotationByReference(String url, String reference, String userName) {
        ClientConfig config = new ClientConfig();
        final Client client = ClientBuilder.newClient(config).register(JacksonFeature.class);
        Response result = client.target(getMPServiceURL(url, RSC_MP_LES_GET + "/", reference))
                .request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .header("Content-Type", "application/json")
                .header("user-name", userName)
                .get(Response.class);
        return result;
    }

    private static URI getBaseURI(String url) {
        return UriBuilder.fromUri(url).build();
    }


    private static String getEFServiceURL(String url, String path, String reference) {
        String serviceURL = url + path;
        if (reference != null) {
            serviceURL = serviceURL.replace("{reference}", reference);
        }
        logger.debug("Service URL: [" + serviceURL + "]");
        return serviceURL;
    }

    private static String getMPServiceURL(String url, String path, String reference) {
        String serviceURL = getServiceURL(url, path);

        if (reference != null) {
            serviceURL = serviceURL.replace("{reference}", reference);
        }
        logger.debug("Service URL: [" + serviceURL + "]");

        return serviceURL;
    }

    private static String getServiceURL(String url, String path) {
//        String serviceURL = url + SERVICE_PREFIX + path;
        String serviceURL = url + path; // actually /messaging not exist anymore

        logger.debug("Service URL: [" + serviceURL + "]");

        return serviceURL;
    }
}
