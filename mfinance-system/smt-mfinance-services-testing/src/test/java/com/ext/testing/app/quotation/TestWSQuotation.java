package com.ext.testing.app.quotation;

import com.soma.mfinance.client.jersey.ClientQuotation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;

/**
 * Created by cheasocheat on 4/4/17.
 */
public class TestWSQuotation {
    protected final static Logger logger = LoggerFactory.getLogger(TestWSQuotation.class);

    private static final String URL = "http://localhost:8080/moto4plus_app";
    private static final String EF_RSC_URL = "http://192.168.7.8:8080/smt_mfinance_webservice/mfinance";

    @Test
    public void getQuotationList() throws Exception {
        String response = ClientQuotation.getQuotationList(URL);
        logger.info("Response : " + response);
    }

    @Test
    public void getQuotationByReference() throws Exception{
        Response response = ClientQuotation.getQuotationByReference(URL, "GLF-TAK-01-00015238","");
        logger.info("Response Status: " + response.getStatus());
        logger.info("Response : " + response.readEntity(String.class));
    }
}