package com.soma.mfinance.core.workflow;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import org.seuksa.frmk.tools.reflection.MyClassUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Quotation status
 *
 * @author kimsuor.seang
 *
 */
public class QuotationWkfStatus {

	// GLF
	public final static EWkfStatus QUO = EWkfStatus.getById(1);  // Quotation or Application
	public final static EWkfStatus PRO = EWkfStatus.getById(2);  // Proposal,
	public final static EWkfStatus DEC = EWkfStatus.getById(3);  // Declined
	public final static EWkfStatus PRA = EWkfStatus.getById(4);  // proposal.approved
	public final static EWkfStatus SUB = EWkfStatus.getById(5);  // Submitted
	public final static EWkfStatus RAD = EWkfStatus.getById(6);  // Request additional info
	public final static EWkfStatus RFC = EWkfStatus.getById(29);  // Request field check
	public final static EWkfStatus CAN = EWkfStatus.getById(8); // Cancelled
	public final static EWkfStatus REJ = EWkfStatus.getById(9);  // Rejected
	public final static EWkfStatus APU = EWkfStatus.getById(10); // Approved(UW)
	public final static EWkfStatus APS = EWkfStatus.getById(11); // Approved(US)
	public final static EWkfStatus AWU = EWkfStatus.getById(12); // Approved with condition(UW)
	public final static EWkfStatus AWS = EWkfStatus.getById(13); // Approved with condition(US)
	public final static EWkfStatus AWT = EWkfStatus.getById(14); // Approved with condition
	public final static EWkfStatus APV = EWkfStatus.getById(15); // Approved
	public final static EWkfStatus ACT = EWkfStatus.getById(16); // Activated
	public final static EWkfStatus REU = EWkfStatus.getById(17);  // Rejected(UW)
	public final static EWkfStatus RCG = EWkfStatus.getById(18); // Request change guarantor
	public final static EWkfStatus LCG = EWkfStatus.getById(19); // Allowed change guarantor
	public final static EWkfStatus ACG = EWkfStatus.getById(20); // Approved change guarantor
	public final static EWkfStatus RVG = EWkfStatus.getById(21); // Request validate change guarantor
	public final static EWkfStatus PPO = EWkfStatus.getById(22); // Pending purchase order
	public final static EWkfStatus APP = EWkfStatus.getById(23); // Pending Asset Appraisal
	public final static EWkfStatus AUP = EWkfStatus.getById(24); // Approved (AC)
	public final static EWkfStatus WIV = EWkfStatus.getById(25); // Waiting back office validation
	public final static EWkfStatus WCA = EWkfStatus.getById(26); // Waiting Contract Activation
	public final static EWkfStatus ACS = EWkfStatus.getById(28); // Approved (CS)
	//public final static EWkfStatus WIV2 = EWkfStatus.getById(30); // Waiting Insurance validation
	public final static EWkfStatus PAC = EWkfStatus.getById(45); // Pending Appraisal Committee

	/**
	 *
	 * @return
	 */
    public static List<EWkfStatus> values() {
    	List<EWkfStatus> statuses = (List<EWkfStatus>) MyClassUtils.getStaticValues(QuotationWkfStatus.class, EWkfStatus.class);
    	return statuses;
    }

	public static List<EWkfStatus> getComboBoxQuotationStatus(){
		if(ProfileUtil.isUnderwriterSupervisor2()){
			List<EWkfStatus> eWkfStatuses = new ArrayList<>();
			eWkfStatuses.add(QuotationWkfStatus.PRO);
			eWkfStatuses.add(QuotationWkfStatus.DEC);
			eWkfStatuses.add(QuotationWkfStatus.APU);
			eWkfStatuses.add(QuotationWkfStatus.APS);
			eWkfStatuses.add(QuotationWkfStatus.PRA);
			eWkfStatuses.add(QuotationWkfStatus.AWU);
			eWkfStatuses.add(QuotationWkfStatus.AWS);
			eWkfStatuses.add(QuotationWkfStatus.AWT);
			eWkfStatuses.add(QuotationWkfStatus.APV);
			eWkfStatuses.add(QuotationWkfStatus.ACT);
			eWkfStatuses.add(QuotationWkfStatus.REU);
			eWkfStatuses.add(QuotationWkfStatus.REJ);
			return eWkfStatuses;
		} else if(ProfileUtil.isAdmin()){
			List<EWkfStatus> eWkfStatuses = new ArrayList<>();
			eWkfStatuses.addAll(values());
			eWkfStatuses.add(ContractWkfStatus.THE);
			eWkfStatuses.add(ContractWkfStatus.ACC);
			eWkfStatuses.add(ContractWkfStatus.REP);
			eWkfStatuses.add(ContractWkfStatus.EAR);
			eWkfStatuses.add(ContractWkfStatus.CLO);
			return eWkfStatuses;
		} else {
			return values();
		}
	}

	// getQuotationStatusAfterApproved (include Approved itself)
	public static List<EWkfStatus> getQuotationStatusAfterApproved(){
		List<EWkfStatus> eWkfStatus = new ArrayList<>();

		// Quotation Status after Approved (include Approved itself)
		eWkfStatus.add(AWT);
		eWkfStatus.add(APV);
		eWkfStatus.add(WIV);
		eWkfStatus.add(WCA);
		eWkfStatus.add(ACT);

		// add all Contract Status
		eWkfStatus.addAll(ContractWkfStatus.values());

		return eWkfStatus;
	}

}