package com.soma.mfinance.core.asset.panel.assetprice;

import com.soma.mfinance.core.asset.model.AssetMake;
import com.soma.mfinance.core.asset.model.AssetModel;
import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.asset.service.AssetModelRestriction;
import com.soma.mfinance.core.asset.service.AssetRangeRestriction;
import com.soma.mfinance.core.auction.model.AssetPrice;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.mfinance.core.asset.model.AssetMake;
import com.soma.mfinance.core.asset.model.AssetModel;
import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.asset.service.AssetModelRestriction;
import com.soma.mfinance.core.asset.service.AssetRangeRestriction;
import com.soma.mfinance.core.auction.model.AssetPrice;
import com.vaadin.data.Property;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;

/**
 * Created by cheasocheat on 3/23/17.
 */
public class AssetPriceConfigSearchPanel extends AbstractSearchPanel<AssetPrice> implements FMEntityField{
    private TextField txtDescEn;
    private TextField txtCode;
    private EntityComboBox<AssetMake> cbxAssetMake;
    private EntityComboBox<AssetRange> cbxAssetRange;
    private EntityComboBox<AssetModel> cbxAssetModel;

    public AssetPriceConfigSearchPanel(AssetPriceConfigTablePanel tablePanel) {
        super(I18N.message("search"), tablePanel);
    }

    @Override
    protected void reset() {
        txtDescEn.setValue("");
        txtCode.setValue("");
        cbxAssetModel.setSelectedEntity(null);
        cbxAssetRange.setSelectedEntity(null);
        cbxAssetMake.setSelectedEntity(null);
    }

    @Override
    protected Component createForm() {
        GridLayout gridLayout = new GridLayout(2, 3);
        gridLayout.setWidth(100.0F, Unit.PERCENTAGE);


        cbxAssetMake = new EntityComboBox<>(AssetMake.class, I18N.message("asset.make"), "descEn" , "");
        cbxAssetMake.setWidth(170, Unit.PIXELS);
        cbxAssetMake.renderer();
        cbxAssetMake.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if (cbxAssetMake.getValue() != null) {
                    AssetRangeRestriction restriction = new AssetRangeRestriction();
                    restriction.setAssetMake(cbxAssetMake.getSelectedEntity());
                    cbxAssetRange.renderer(restriction);
                } else {
                    cbxAssetRange.renderer();
                }
            }
        });

        cbxAssetRange = new EntityComboBox<>(AssetRange.class, I18N.message("asset.range"), "descEn" , "");
        cbxAssetRange.setWidth(170, Unit.PIXELS);
        cbxAssetRange.renderer();
        cbxAssetRange.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if (cbxAssetRange.getValue() != null) {
                    AssetModelRestriction restriction = new AssetModelRestriction();
                    restriction.setAssetRange(cbxAssetRange.getSelectedEntity());
                    cbxAssetModel.renderer(restriction);
                } else {
                    cbxAssetModel.renderer();
                }
            }
        });


        cbxAssetModel = new EntityComboBox<>(AssetModel.class, I18N.message("asset.model"), "descEn" , "");
        cbxAssetModel.setWidth(170, Unit.PIXELS);
        cbxAssetModel.renderer();

        txtDescEn = ComponentFactory.getTextField(I18N.message("desc.en"), false, 60, 180);
        txtCode = ComponentFactory.getTextField(I18N.message("code"), false, 60, 180);

        FormLayout frmlayoutLeft = new FormLayout();
        frmlayoutLeft.addComponent(cbxAssetMake);
        frmlayoutLeft.addComponent(cbxAssetRange);
        frmlayoutLeft.addComponent(cbxAssetModel);


        FormLayout frmLayoutRight = new FormLayout();
        frmLayoutRight.addComponent(txtCode);
        frmLayoutRight.addComponent(txtDescEn);

        gridLayout.setSpacing(true);
        gridLayout.addComponent(frmlayoutLeft);
        gridLayout.addComponent(frmLayoutRight);
        return gridLayout;
    }

    @Override
    public BaseRestrictions<AssetPrice> getRestrictions() {
        BaseRestrictions<AssetPrice> restrictions = new BaseRestrictions<>(AssetPrice.class);
        if (cbxAssetMake.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq(ASSET_MAKE + "." + ID, cbxAssetMake.getSelectedEntity().getId()));
        }
        if (cbxAssetRange.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq(ASSET_RANGE + "." + ID, cbxAssetRange.getSelectedEntity().getId()));
        }
        if (cbxAssetModel.getSelectedEntity() != null){
            restrictions.addCriterion(Restrictions.eq(ASSET_MODEL + "." + ID, cbxAssetModel.getSelectedEntity().getId()));
        }
        if (StringUtils.isNotEmpty(txtDescEn.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("descEn", txtDescEn.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtCode.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("code", txtCode.getValue(), MatchMode.ANYWHERE));
        }

        return restrictions;
    }
}
