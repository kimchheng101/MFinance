package com.soma.mfinance.core.utils;

import org.seuksa.frmk.model.entity.EntityA;

import java.io.Serializable;

/**
 * @author by kimsuor.seang  on 12/9/2017.
 */
public class ObjectUtil<T extends EntityA> implements Serializable {

    public static  ObjectUtil sInstance = null;

    public ObjectUtil() {
        super();
        sInstance = this;
    }

    private ObjectUtil getInstance(){
        if(sInstance != null){
            return sInstance;
        }
        return sInstance  = new ObjectUtil<T>();
    }

    public static <T> T objectValue(T value) {
        if (value != null) {
            return  value;
        }
        return null;
    }

    public static <T> T objectValue(final T... values) {
        if (values != null) {
            for (final T val : values) {
                if (val != null) {
                    return val;
                }
            }
        }
        return null;
    }
}