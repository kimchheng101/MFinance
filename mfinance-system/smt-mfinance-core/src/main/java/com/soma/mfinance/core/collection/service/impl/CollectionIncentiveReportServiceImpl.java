package com.soma.mfinance.core.collection.service.impl;

import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.collection.model.CollectionIncentiveReport;
import com.soma.mfinance.core.collection.model.ContractCollectionHistory;
import com.soma.mfinance.core.collection.service.CollectionIncentiveReportService;
import com.soma.mfinance.core.payment.model.Payment;
import com.soma.mfinance.core.utils.ObjectUtil;
import com.soma.mfinance.core.collection.model.CollectionIncentiveReport;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.seuksa.frmk.tools.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.beans.Transient;
import java.util.Date;
import java.util.List;

import static com.soma.mfinance.core.helper.FinServicesHelper.CASHFLOW_SRV;
import static com.soma.mfinance.core.helper.FinServicesHelper.COL_SRV;
import static com.soma.frmk.helper.SeuksaServicesHelper.ENTITY_SRV;

/**
 * @author by kimsuor.seang  on 12/4/2017.
 */
@Repository
public class CollectionIncentiveReportServiceImpl extends BaseEntityServiceImpl implements CollectionIncentiveReportService {

    @Autowired
    private EntityDao dao;

    @Override
    public BaseEntityDao getDao() {
        return dao;
    }

    @Override
    public void calculateCollectionIncentiveReport(Date selectDate) {

        String reference;
        Integer lastNumberInstall;

        List<Collection> collections = COL_SRV.getCollectionIncentive(selectDate);
        if (collections != null) {
             for (Collection collection : collections){
                 if(collection  != null){
                     CollectionIncentiveReport collectionIncentiveReport = new CollectionIncentiveReport();
                     collectionIncentiveReport.setDate(selectDate);
                     collectionIncentiveReport.setNbOverdueInDay(collection.getDueDay());
                     collectionIncentiveReport.setAssignee(ObjectUtil.sInstance.objectValue(collection.getCollectionOfficer()));
                     /*collectionIncentiveReport.setArea(ObjectUtil.sInstance.objectValue(collection.getArea()));
                     collectionIncentiveReport.setCollectionTask(ObjectUtil.sInstance.objectValue(collection.getCollectionTask()));*/
                     collectionIncentiveReport.setColResult(ObjectUtil.sInstance.objectValue(collection.getColResult()));
                     collectionIncentiveReport.setCollectionOfficer(ObjectUtil.sInstance.objectValue(collection.getCollectionOfficer()));
                     collectionIncentiveReport.setCollectionStatus(ObjectUtil.sInstance.objectValue(collection.getWkfStatus()));
                     collectionIncentiveReport.setCollection(ObjectUtil.sInstance.objectValue(collection));

                    if(collection.getContract() != null){
                        reference = collection.getContract().getReference();
                        lastNumberInstall = collection.getContract().getLastPaidNumInstallment();
                        Date paymentDate = CASHFLOW_SRV.getPaymentDateByByLastNumberInstallment(reference,lastNumberInstall);
                        if(paymentDate != null){
                            ContractCollectionHistory contractCollectionHistory = getLatestContractCollectionHistory(collection.getContract().getContractCollectionHistories(), paymentDate);
                            if (contractCollectionHistory != null) {
                                collectionIncentiveReport.setCollectionStatus(contractCollectionHistory.getCollectionStatus());
                                collectionIncentiveReport.setCustomerAttribute(contractCollectionHistory.getCustField());
                                collectionIncentiveReport.setAmountPromiseToPay(contractCollectionHistory.getAmountPromiseToPayUsd());
                                collectionIncentiveReport.setStartPeriodPromiseToPay(contractCollectionHistory.getStartPeriodPromiseToPay());
                                collectionIncentiveReport.setEndPeriodPromiseToPay(contractCollectionHistory.getEndPeriodPromiseToPay());
                            }
                        }
                    }
                     saveOrUpdate(collectionIncentiveReport);
                 }
             }
         }
    }

    @Override
    public List<CollectionIncentiveReport> getCollectionIncentiveReports(Date selectedDate, Payment payment) {
        BaseRestrictions<CollectionIncentiveReport> reportBaseRestrictions = new BaseRestrictions<>(CollectionIncentiveReport.class);
        reportBaseRestrictions.addCriterion(Restrictions.ge("date", DateUtils.getDateAtBeginningOfDay(selectedDate)));
        reportBaseRestrictions.addCriterion(Restrictions.le("date", DateUtils.getDateAtEndOfDay(selectedDate)));
        reportBaseRestrictions.addCriterion(Restrictions.eq("payment.id", payment.getId()));
        if (ENTITY_SRV.list(reportBaseRestrictions) != null && ENTITY_SRV.list(reportBaseRestrictions).size() < 0) {
            return ENTITY_SRV.list(reportBaseRestrictions);
        }
        return null;
    }

    @Transient
    private ContractCollectionHistory getLatestContractCollectionHistory(List<ContractCollectionHistory> contractCollectionHistories, Date paymentDate) {
        if (contractCollectionHistories == null || contractCollectionHistories.isEmpty()) {
            return null;
        }
        ContractCollectionHistory contractCollectionHistory = contractCollectionHistories.get(0);
        for (int i = 1; i < contractCollectionHistories.size(); i++) {
            if (paymentDate.after(contractCollectionHistories.get(i).getCreateDate())) {
                if (contractCollectionHistories.get(i).getCreateDate().after(contractCollectionHistory.getCreateDate())) {
                    contractCollectionHistory = contractCollectionHistories.get(i);
                }
            }
        }
        if (paymentDate.after(contractCollectionHistory.getCreateDate())) {
            return contractCollectionHistory;
        } else {
            return null;
        }
    }
}
