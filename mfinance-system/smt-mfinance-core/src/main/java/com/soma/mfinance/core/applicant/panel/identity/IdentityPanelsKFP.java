package com.soma.mfinance.core.applicant.panel.identity;

/***
 * @author p.leap
 * modify kimsuor.seang 10/18/2017
 */

import com.soma.common.app.eref.ECountry;
import com.soma.ersys.core.hr.model.address.*;
import com.soma.ersys.core.hr.model.eref.*;
import com.soma.ersys.core.hr.model.organization.ContactInfo;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.soma.mfinance.core.Enum.EHousing;
import com.soma.mfinance.core.applicant.model.*;
import com.soma.mfinance.core.applicant.service.ValidateLetter;
import com.soma.mfinance.core.applicant.service.ValidateNumbers;
import com.soma.mfinance.core.custom.component.CustomEntityRefComboBox;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.Runo;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.soma.ersys.core.hr.model.eref.ETypeContactInfo.MOBILE;


public class IdentityPanelsKFP extends AbstractTabPanel implements FinServicesHelper {

    private TextField txtFamilyName;
    private TextField txtFamilyNameOTH;
    private TextField txtFirstName;
    private TextField txtFirstNameOTH;
    private TextField txtNickname;
    private TextField txtSpouseFamilyName;
    private TextField txtSpouseFirstName;
    private TextField txtNumberOfChildren;
    private TextField txtMobilePhone1;
    private TextField txtMobilePhone2;
    private TextField txtSpouseMobilePhone;
    private ERefDataComboBox<ECivility> cbxCivility;
    private ERefDataComboBox<EGender> cbxGender;
    private ERefDataComboBox<EMaritalStatus> cbxMaritalStatus;
    private EntityRefComboBox<Province> cbxProvinceOfBirth;
    private ERefDataComboBox<ENationality> cbxNationality;
    private AutoDateField dfDateOfBirths;
    private TextField txtHouseNo;
    private TextField txtStreet;
    private ERefDataComboBox<ECountry> cbxCountry;
    private ERefDataComboBox<EHousing> cbxHousing;
    private TextField txtZipCode;
    private EntityRefComboBox<Province> cbxProvince;
    private EntityRefComboBox<District> cbxDistrict;
    private TextField txtYear;
    private TextField txtMonth;
    private EntityRefComboBox<Commune> cbxCommune;
    private EntityRefComboBox<Village> cbxVillage;
    private Panel currentAddressPanel;

    private Button btnAddPhoneNumber;
    private Button btnShowPhoneNumber;

    private Individual individual;

    private IdentityCoAppPanel identityPanelsCoApp;
    public IdentityPanelsKFP() {
        super();
        setMargin(true);
        setSizeFull();
    }

    @Override
    protected Component createForm() {
        final Panel identityPanel = new Panel("Identity KFP");
        ValidateNumbers identityValidation = new ValidateNumbers();
        ArrayList txt = new ArrayList();

        identityPanelsCoApp = new IdentityCoAppPanel();

        cbxCivility = new ERefDataComboBox<>(INDIVI_SRV.listECivility());
        cbxCivility.setSelectedEntity(ECivility.MR);
        cbxCivility.setImmediate(true);
        cbxCivility.setWidth("140px");

        cbxGender = new ERefDataComboBox<>(EGender.values());
        cbxGender.setImmediate(true);
        cbxGender.setWidth("140px");

        cbxMaritalStatus = new ERefDataComboBox<>(EMaritalStatus.values());
        cbxMaritalStatus.setImmediate(true);
        cbxMaritalStatus.setWidth("140px");

        cbxProvinceOfBirth = new EntityRefComboBox<Province>();
        cbxProvinceOfBirth.setRestrictions(new BaseRestrictions<>(Province.class));
        cbxProvinceOfBirth.setImmediate(true);
        cbxProvinceOfBirth.setWidth("140px");
        cbxProvinceOfBirth.renderer();

        cbxNationality = new ERefDataComboBox<ENationality>(ENationality.values());
        cbxNationality.setImmediate(true);
        cbxNationality.setWidth("140px");

        txtFamilyName = ComponentFactory.getTextField(60, 140);

        txtFamilyNameOTH = ComponentFactory.getTextField(60, 140);

        txtFirstName = ComponentFactory.getTextField(60, 140);

        txtFirstNameOTH = ComponentFactory.getTextField(60, 140);


        txtNickname = ComponentFactory.getTextField("", false, 60, 140);
        txtSpouseFamilyName = ComponentFactory.getTextField("", false, 60, 140);
        txtSpouseFirstName = ComponentFactory.getTextField("", false, 60, 140);
        txtNumberOfChildren = ComponentFactory.getTextField("", false, 60, 140);
        txt.add(txtNumberOfChildren);
        txtMobilePhone1 = ComponentFactory.getTextField("", false, 60, 100);
        txtMobilePhone2 = ComponentFactory.getTextField("", false, 60, 140);
        txtSpouseMobilePhone = ComponentFactory.getTextField("", false, 60, 140);

        dfDateOfBirths = ComponentFactory.getAutoDateField("", false);
        dfDateOfBirths.setWidth(140, Unit.PIXELS);

        btnAddPhoneNumber = new Button();
        btnAddPhoneNumber.setVisible(true);
        btnAddPhoneNumber.setStyleName(Runo.BUTTON_LINK);
        btnAddPhoneNumber.setIcon(new ThemeResource("../smt-default/icons/16/add.png"));

        btnAddPhoneNumber.addClickListener(new Button.ClickListener() {
            private static final long serialVersionUID = -3348402917006996671L;

            @Override
            public void buttonClick(Button.ClickEvent event) {

                final Window winAddPhone = new Window();
                winAddPhone.setModal(true);
                winAddPhone.setWidth(300, Unit.PIXELS);
                winAddPhone.setHeight(220, Unit.PIXELS);
                winAddPhone.setCaption(I18N.message("phone.number"));

                final TextField txtPhoneNumber = ComponentFactory.getTextField("phone.number", true, 30, 130);

                Button btnCancel = new NativeButton(I18N.message("close"), new Button.ClickListener() {
                    private static final long serialVersionUID = 3975121141565713259L;

                    public void buttonClick(Button.ClickEvent event) {
                        winAddPhone.close();
                    }
                });
                btnCancel.setIcon(new ThemeResource("../smt-default/icons/16/delete.png"));
                Button btnSave = new NativeButton(I18N.message("save"), new Button.ClickListener() {
                    private static final long serialVersionUID = 8088485001713740490L;

                    public void buttonClick(Button.ClickEvent event) {
                        Boolean isValidPhoneNumber = ValidateNumbers.isValidPhoneNumber(txtPhoneNumber.getValue());
                        if (!txtPhoneNumber.getValue().equals("") && isValidPhoneNumber) {
                            if (individual != null && individual.getId() != null) {
                                ContactInfo contactInfo = new ContactInfo();
                                contactInfo.setValue(txtPhoneNumber.getValue());
                                contactInfo.setTypeInfo(MOBILE);
                                ENTITY_SRV.saveOrUpdate(contactInfo);
                                IndividualContactInfo individualContactInfoObject = new IndividualContactInfo();
                                individualContactInfoObject.setContactInfo(contactInfo);
                                individualContactInfoObject.setIndividual(individual);
                                ENTITY_SRV.saveOrUpdate(individualContactInfoObject);
                            } else {
                                btnAddPhoneNumber.setVisible(false);
                                btnShowPhoneNumber.setVisible(false);
                            }
                            winAddPhone.close();
                        } else {
                            MessageBox mb = new MessageBox(UI.getCurrent(), "320px", "140px", I18N.message("information"),
                                    MessageBox.Icon.ERROR, "The " + I18N.message("phone.number.is.not.correct"), Alignment.MIDDLE_RIGHT,
                                    new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, I18N.message("ok")));
                            mb.show();
                        }
                    }
                });
                btnSave.setIcon(new ThemeResource("../smt-default/icons/16/save.png"));

                NavigationPanel navigationPanel = new NavigationPanel();
                navigationPanel.addButton(btnSave);
                navigationPanel.addButton(btnCancel);
                VerticalLayout contentLayout = new VerticalLayout();
                contentLayout.setMargin(true);
                contentLayout.addComponent(new FormLayout(txtPhoneNumber));
                winAddPhone.setContent(new VerticalLayout(navigationPanel, contentLayout));

                UI.getCurrent().addWindow(winAddPhone);
            }
        });


        btnShowPhoneNumber = new Button();
        btnShowPhoneNumber.setVisible(true);
        btnShowPhoneNumber.setStyleName(Runo.BUTTON_LINK);
        btnShowPhoneNumber.setIcon(new ThemeResource("../smt-default/icons/16/table.png"));

        btnShowPhoneNumber.addClickListener(new Button.ClickListener() {
            private static final long serialVersionUID = -5719845559566313177L;

            @SuppressWarnings("unchecked")
            @Override
            public void buttonClick(Button.ClickEvent event) {
                final Window winAddPhone = new Window();
                winAddPhone.setModal(true);
                winAddPhone.setWidth(500, Unit.PIXELS);
                winAddPhone.setHeight(310, Unit.PIXELS);
                winAddPhone.setCaption(I18N.message("phone.numbers"));
                List<ColumnDefinition> columnDefinitions = new ArrayList<>();
                columnDefinitions.add(new ColumnDefinition("phone.number", I18N.message("phone.number"), String.class, Table.Align.LEFT, 100));
                columnDefinitions.add(new ColumnDefinition("create.user", I18N.message("create.user"), String.class, Table.Align.LEFT, 120));
                columnDefinitions.add(new ColumnDefinition("create.date", I18N.message("create.date"), Date.class, Table.Align.LEFT, 120));
                SimplePagedTable<ContactInfo> pagedTable = new SimplePagedTable<>(columnDefinitions);
                IndexedContainer indexedContainer = new IndexedContainer();
                for (ColumnDefinition column : columnDefinitions) {
                    indexedContainer.addContainerProperty(column.getPropertyId(), column.getPropertyType(), null);
                }
                List<IndividualContactInfo> individualContactInfos = INDIVI_SRV.getIndividualContractInfos(individual);
                if (individualContactInfos != null) {
                    for (IndividualContactInfo individualContactInfo : individualContactInfos) {
                        Item item = indexedContainer.addItem(individualContactInfo.getContactInfo().getId());
                        item.getItemProperty("phone.number").setValue(individualContactInfo.getContactInfo().getValue());
                        item.getItemProperty("create.user").setValue(individualContactInfo.getCreateUser());
                        item.getItemProperty("create.date").setValue(individualContactInfo.getCreateDate());
                    }
                    pagedTable.setContainerDataSource(indexedContainer);
                }
                winAddPhone.setContent(new VerticalLayout(pagedTable, pagedTable.createControls()));
                UI.getCurrent().addWindow(winAddPhone);
            }
        });

        CustomLayout customLayout = createCustomLayout("identity/identityFormLayout");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("civility")), "lblCivility");
        customLayout.addComponent(cbxCivility, "cbxCivility");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("familyname")), "lblFamilyName");
        customLayout.addComponent(txtFamilyName, "txtFamilyName");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("familyname.oth")), "lblFamilyNameOTH");
        customLayout.addComponent(txtFamilyNameOTH, "txtFamilyNameOTH");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("firstname")), "lblFirstName");
        customLayout.addComponent(txtFirstName, "txtFirstName");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("firstname.oth")), "lblFirstNameOTH");
        customLayout.addComponent(txtFirstNameOTH, "txtFirstNameOTH");

        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("nickname")), "lblNickname");
        customLayout.addComponent(txtNickname, "txtNickname");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("spouse.familyname")), "lblSpouseFamilyName");
        customLayout.addComponent(txtSpouseFamilyName, "txtSpouseFamilyName");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("spouse.firstname")), "lblSpouseFirstName");
        customLayout.addComponent(txtSpouseFirstName, "txtSpouseFirstName");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("number.of.children")), "lblNumberOfChildren");
        customLayout.addComponent(txtNumberOfChildren, "txtNumberOfChildren");

        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("gender")), "lblGender");
        customLayout.addComponent(cbxGender, "cbxGender");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("marital.status")), "lblMaritalStatus");
        customLayout.addComponent(cbxMaritalStatus, "cbxMaritalStatus");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("province.of.birth")), "lblProvinceOfBirth");
        customLayout.addComponent(cbxProvinceOfBirth, "cbxProvinceOfBirth");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("dateofbirth")), "lblDateOfBirth");
        customLayout.addComponent(dfDateOfBirths, "dfDateOfBirth");

        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("nationality")), "lblNationality");
        customLayout.addComponent(cbxNationality, "cbxNationality");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("mobile.phone1")), "lblMobilePhone1");
        customLayout.addComponent(txtMobilePhone1, "txtMobilePhone1");
        customLayout.addComponent(btnAddPhoneNumber, "btnAddPhoneNumber");
        customLayout.addComponent(btnShowPhoneNumber, "btnShowPhoneNumber");


        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("mobile.phone2")), "lblMobilePhone2");
        customLayout.addComponent(txtMobilePhone2, "txtMobilePhone2");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("mobile.phone.spouse")), "lblSpouseMobilePhone");
        customLayout.addComponent(txtSpouseMobilePhone, "txtSpouseMobilePhone");

        HorizontalLayout identityLayout = new HorizontalLayout();
        identityLayout.addComponent(customLayout);
        identityLayout.setMargin(true);
        identityPanel.setContent(identityLayout);

        currentAddressPanel = new Panel("Current address");

        cbxCountry = new ERefDataComboBox<ECountry>(ECountry.values());
        cbxCountry.setImmediate(true);
        cbxCountry.setWidth("140px");

        cbxHousing = new ERefDataComboBox<EHousing>(EHousing.values());
        cbxHousing.setImmediate(true);
        cbxHousing.setWidth("140px");

        cbxProvince = new CustomEntityRefComboBox();
        cbxProvince.setRestrictions(new BaseRestrictions<>(Province.class));
        cbxProvince.setImmediate(true);
        cbxProvince.setWidth("140px");
        cbxProvince.renderer();

        cbxDistrict = new CustomEntityRefComboBox();
        cbxDistrict.setImmediate(true);
        cbxDistrict.setWidth("140px");

        cbxCommune = new CustomEntityRefComboBox();
        cbxCommune.setImmediate(true);
        cbxCommune.setWidth("140px");

        cbxVillage = new EntityRefComboBox();
        cbxVillage.setImmediate(true);
        cbxVillage.setWidth("140px");

        ((CustomEntityRefComboBox) cbxProvince).setFilteringComboBox(cbxDistrict, District.class);
        ((CustomEntityRefComboBox) cbxDistrict).setFilteringComboBox(cbxCommune, Commune.class);
        ((CustomEntityRefComboBox) cbxCommune).setFilteringComboBox(cbxVillage, Village.class);

        txtHouseNo = ComponentFactory.getTextField(60, 80);
        txtStreet = ComponentFactory.getTextField(60, 243);
        txtZipCode = ComponentFactory.getTextField(60, 360);
        txt.add(txtZipCode);
        txtYear = ComponentFactory.getTextField(40, 80);
        txt.add(txtYear);
        txtMonth = ComponentFactory.getTextField(40, 80);
        txt.add(txtMonth);

        VerticalLayout verticalLayoutAddress = new VerticalLayout();
        verticalLayoutAddress.setMargin(true);
        verticalLayoutAddress.setSpacing(true);
        CustomLayout customAddressLayout = createCustomLayout("identity/currentAddressFormLayout");

        customAddressLayout.addComponent(ComponentFactory.getLabel(I18N.message("house.no")), "lblHouseNo");
        customAddressLayout.addComponent(txtHouseNo, "txtHouseNo");
        customAddressLayout.addComponent(ComponentFactory.getLabel(I18N.message("street")), "lblStreet");
        customAddressLayout.addComponent(txtStreet, "txtStreet");
        customAddressLayout.addComponent(ComponentFactory.getLabel(I18N.message("country")), "lblCountry");
        customAddressLayout.addComponent(cbxCountry, "cbxCountry");
        customAddressLayout.addComponent(ComponentFactory.getLabel(I18N.message("housing")), "lblHousing");
        customAddressLayout.addComponent(cbxHousing, "cbxHousing");

        customAddressLayout.addComponent(ComponentFactory.getLabel(I18N.message("zip.code")), "lblZipCode");
        customAddressLayout.addComponent(txtZipCode, "txtZipCode");
        customAddressLayout.addComponent(ComponentFactory.getLabel(I18N.message("province")), "lblProvince");
        customAddressLayout.addComponent(cbxProvince, "cbxProvince");
        customAddressLayout.addComponent(ComponentFactory.getLabel(I18N.message("district")), "lblDistrict");
        customAddressLayout.addComponent(cbxDistrict, "cbxDistrict");

        customAddressLayout.addComponent(ComponentFactory.getLabel("Time at this address"), "lblTime");
        customAddressLayout.addComponent(txtYear, "txtYear");
        customAddressLayout.addComponent(ComponentFactory.getLabel(I18N.message("year")), "lblYear");
        customAddressLayout.addComponent(txtMonth, "txtMonth");
        customAddressLayout.addComponent(ComponentFactory.getLabel(I18N.message("month")), "lblMonth");
        customAddressLayout.addComponent(ComponentFactory.getLabel(I18N.message("commune.st")), "lblCommune");
        customAddressLayout.addComponent(cbxCommune, "cbxCommune");
        customAddressLayout.addComponent(ComponentFactory.getLabel(I18N.message("village")), "lblVillage");
        customAddressLayout.addComponent(cbxVillage, "cbxVillage");


        cbxCivility.addValueChangeListener(new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                if (cbxCivility.getSelectedEntity() == ECivility.getByCode("MR")) {
                    cbxGender.setSelectedEntity(EGender.M);
                } else if (cbxCivility.getSelectedEntity() == ECivility.getByCode("MRS") || cbxCivility.getSelectedEntity() == ECivility.getByCode("MS") || cbxCivility.getSelectedEntity() == ECivility.getByCode("MISS")) {
                    cbxGender.setSelectedEntity(EGender.F);
                }
            }
        });

        HorizontalLayout addressLayout = new HorizontalLayout();
        addressLayout.addComponent(customAddressLayout);
        addressLayout.setMargin(true);
        currentAddressPanel.setContent(addressLayout);


        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addComponent(identityPanel);
        verticalLayout.addComponent(currentAddressPanel);
        verticalLayout.addComponent(identityPanelsCoApp.identityPanel());
        verticalLayout.addComponent(identityPanelsCoApp.currentAddressPanel());

        identityValidation.validateNumber(txt);
        return verticalLayout;
    }

    private CustomLayout createCustomLayout(String template) {
        InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
        CustomLayout customLayout = null;
        try {
            customLayout = new CustomLayout(layoutFile);
        } catch (IOException e) {
            Notification.show("Could not locate template " + template, e.getMessage(), Notification.Type.ERROR_MESSAGE);
        }
        return customLayout;
    }

    public void getIdentityPanel(Individual individual) {
        IndividualSpouse individualSpouse = individual.getIndividualSpouse();
        IndividualAddress individualAddress = individual.getIndividualAddress(ETypeAddress.MAIN);
        Address address = individual.getMainAddress();

        if (individualSpouse == null) {
            individualSpouse = new IndividualSpouse();
        }

        if (individualAddress == null) {
            individualAddress = new IndividualAddress();
        }

        if (address == null) {
            address = new Address();
        }

        individual.setCivility(cbxCivility.getSelectedEntity());

        individual.setFirstName(txtFirstNameOTH.getValue() != null ? txtFamilyNameOTH.getValue().trim() : "");
        individual.setFirstName( txtFirstNameOTH.getValue() == null ? "" : txtFirstNameOTH.getValue().trim() );
        individual.setFirstNameEn(txtFirstName.getValue());
        individual.setLastName(txtFamilyNameOTH.getValue() != null ? txtFamilyNameOTH.getValue().trim() : "");
        individual.setLastName( txtFamilyNameOTH.getValue() == null ? "" : txtFamilyNameOTH.getValue().trim() );
        individual.setLastNameEn(txtFamilyName.getValue());
        individualSpouse.setFirstName(txtSpouseFirstName.getValue());
        individualSpouse.setLastName(txtSpouseFamilyName.getValue());

        individual.setNickName(txtNickname.getValue());
        individual.setNumberOfChildren(getInteger(txtNumberOfChildren, 0));
        individual.setGender(cbxGender.getSelectedEntity() != null ? cbxGender.getSelectedEntity() : EGender.U);
        individual.setMaritalStatus(cbxMaritalStatus.getSelectedEntity() != null ? cbxMaritalStatus.getSelectedEntity() : EMaritalStatus.UNKNOWN);
        individual.setPlaceOfBirth(cbxProvinceOfBirth.getSelectedEntity());
        individual.setBirthDate(dfDateOfBirths.getValue());
        individual.setNationality(cbxNationality.getSelectedEntity());
        individual.setMobilePerso(txtMobilePhone1.getValue());
        individual.setMobilePerso2(txtMobilePhone2.getValue());
        individualSpouse.setMobilePhone(txtSpouseMobilePhone.getValue());

        address.setHouseNo(txtHouseNo.getValue());
        address.setStreet(txtStreet.getValue());
        address.setCountry(cbxCountry.getSelectedEntity());
        individualAddress.setHousing(cbxHousing.getSelectedEntity());
        individualAddress.setZipCode(txtZipCode.getValue());
        address.setProvince(cbxProvince.getSelectedEntity());
        address.setDistrict(cbxDistrict.getSelectedEntity());
        address.setCommune(cbxCommune.getSelectedEntity());
        address.setVillage(cbxVillage.getSelectedEntity());
        address.setType(ETypeAddress.MAIN);
        individualAddress.setAddress(address);
        individualAddress.setTimeAtAddressInYear(getInteger(txtYear, 0));
        individualAddress.setTimeAtAddressInMonth(getInteger(txtMonth, 0));

        List<IndividualAddress> individualAddresses = individual.getIndividualAddresses();

        if (individualAddresses == null) {
            individualAddresses = new ArrayList<>();
        }

        individualAddresses.add(individualAddress);

        List<IndividualSpouse> individualSpouses = individual.getIndividualSpouses();

        if (individualSpouses == null) {
            individualSpouses = new ArrayList<>();
        }

        individualSpouses.add(individualSpouse);

        individual.setIndividualAddresses(individualAddresses);
        individual.setIndividualSpouses(individualSpouses);

    }

    public void assignValues(Quotation quotation) {
        Applicant applicant = quotation.getApplicant();
        if (applicant == null) {
            applicant = quotation.getQuotationApplicants().get(0).getApplicant();
        }
        if (applicant != null) {
            this.individual = applicant.getIndividual();
            if (individual != null) {

                cbxCivility.setSelectedEntity(individual.getCivility() != null ?  individual.getCivility()  : null);

                txtFirstName.setValue(individual.getFirstNameEn());
                txtFirstNameOTH.setValue(individual.getFirstName());
                txtFamilyName.setValue(individual.getLastNameEn());
                txtFamilyNameOTH.setValue(individual.getLastName());

                txtNickname.setValue(individual.getNickName());
                cbxGender.setSelectedEntity(individual.getGender());
                cbxMaritalStatus.setSelectedEntity(individual.getMaritalStatus());
                cbxProvinceOfBirth.setSelectedEntity(individual.getPlaceOfBirth());
                dfDateOfBirths.setValue(individual.getBirthDate());
                cbxNationality.setSelectedEntity(individual.getNationality());
                txtMobilePhone1.setValue(individual.getMobilePerso());
                txtMobilePhone2.setValue(individual.getMobilePerso2());

                if (individual.getIndividualSpouse() != null) {
                    txtSpouseFamilyName.setValue(individual.getIndividualSpouse().getLastName());
                    txtSpouseFirstName.setValue(individual.getIndividualSpouse().getFirstName());
                    txtSpouseMobilePhone.setValue(individual.getIndividualSpouse().getMobilePhone());
                }

                if (individual.getNumberOfChildren() != null) {
                    txtNumberOfChildren.setValue(individual.getNumberOfChildren().toString());
                }

                Address mainAddress = individual.getMainAddress();
                IndividualAddress mainIndividualAddress = individual.getIndividualAddress(ETypeAddress.MAIN);

                if (mainAddress != null) {
                    txtHouseNo.setValue(mainAddress.getHouseNo());
                    txtStreet.setValue(mainAddress.getStreet());
                    cbxCountry.setSelectedEntity(mainAddress.getCountry());
                    cbxProvince.setSelectedEntity(mainAddress.getProvince());
                    cbxDistrict.setSelectedEntity(mainAddress.getDistrict());
                    cbxCommune.setSelectedEntity(mainAddress.getCommune());
                    cbxVillage.setSelectedEntity(mainAddress.getVillage());
                }

                if (mainIndividualAddress != null) {
                    txtZipCode.setValue(mainIndividualAddress.getZipCode());
                    txtYear.setValue(getDefaultString(mainIndividualAddress.getTimeAtAddressInYear()));
                    txtMonth.setValue(getDefaultString(mainIndividualAddress.getTimeAtAddressInMonth()));
                    cbxHousing.setSelectedEntity(mainIndividualAddress.getHousing());
                }
            }
            identityPanelsCoApp.assignValues(quotation);
        }

    }

    public void reset() {
        cbxCivility.setSelectedEntity(null);
        txtFamilyName.setValue("");
        txtFamilyNameOTH.setValue("");
        txtFirstName.setValue("");
        txtFirstNameOTH.setValue("");
        txtNickname.setValue("");
        txtSpouseFamilyName.setValue("");
        txtSpouseFirstName.setValue("");
        txtNumberOfChildren.setValue("");
        cbxGender.setSelectedEntity(null);
        cbxMaritalStatus.setSelectedEntity(null);
        cbxProvinceOfBirth.setSelectedEntity(null);
        dfDateOfBirths.setValue(null);
        cbxNationality.setSelectedEntity(null);
        txtMobilePhone1.setValue("");
        txtMobilePhone2.setValue("");
        txtSpouseMobilePhone.setValue("");
        txtHouseNo.setValue("");
        txtStreet.setValue("");
        txtZipCode.setValue("");
        txtYear.setValue("");
        txtMonth.setValue("");
        cbxCountry.setSelectedEntity(null);
        cbxHousing.setSelectedEntity(null);
        cbxProvince.setSelectedEntity(null);
        cbxDistrict.setSelectedEntity(null);
        cbxCommune.setSelectedEntity(null);
        cbxVillage.setSelectedEntity(null);
    }

    public List<String> isValid() {
        super.reset();
        checkMandatorySelectField(cbxCivility, "civility");
        checkMandatoryField(txtFamilyName, "familyname");
        checkMandatorySelectField(cbxGender, "gender");
        checkMandatorySelectField(cbxNationality, "nationality");
        checkMandatoryField(txtFamilyNameOTH, "familyname.oth");
        checkMandatorySelectField(cbxMaritalStatus, "marital.status");
        checkMandatoryField(txtMobilePhone1, "mobile..phone.1");
        checkMandatoryField(txtFirstName, "firstname");
        checkMandatorySelectField(cbxProvinceOfBirth, "province.of.birth");
        checkMandatoryField(txtFirstNameOTH, "firstname.oth");
        checkMandatoryDateField(dfDateOfBirths, "dateofbirth");
        checkMandatoryField(txtYear, "year");
        checkMandatoryField(txtMonth, "month");
        checkMandatorySelectField(cbxCountry, "country");
        checkMandatorySelectField(cbxProvince, "province");
        checkMandatorySelectField(cbxCommune, "commune");
        checkMandatorySelectField(cbxHousing, "housing");
        checkMandatorySelectField(cbxDistrict, "district");
        checkMandatorySelectField(cbxVillage, "village");

        if (INDIVI_SRV.isIndividualOver18Years(dfDateOfBirths.getValue())) {
            errors.add(I18N.message("applicant.not.over.18.and.65.years"));
        }
        if (!ValidateNumbers.isValidOnlyNumber(txtMobilePhone1.getValue())) {
            errors.add(I18N.message("Mobile Phone1 * contain character"));
        }
        if (!ValidateNumbers.isValidOnlyNumber(txtMobilePhone2.getValue())) {
            errors.add(I18N.message("Mobile Phone2 contain character"));
        }
        if (!ValidateNumbers.isValidOnlyNumber(txtSpouseMobilePhone.getValue())) {
            errors.add(I18N.message("Spouse phone number contain character"));
        }
         /*
        Check Khmer Phone Number
         */
        if (!ValidateNumbers.isValidPhoneNumber(txtMobilePhone1.getValue())) {
            errors.add(I18N.message("Mobile Phone 1 is invalid!"));
        }
        if (!ValidateNumbers.isValidPhoneNumber(txtMobilePhone2.getValue())) {
            errors.add(I18N.message("Mobile Phone 2 is invalid!"));
        }
        if (!ValidateNumbers.isValidPhoneNumber(txtSpouseMobilePhone.getValue())) {
            errors.add(I18N.message("Spouse phone is invalid!"));
        }
        if (!ValidateLetter.isKhmerLetter(txtFamilyNameOTH.getValue())) {
            errors.add(I18N.message("family.name.allow.input.khmer.only"));
        }
        if (!ValidateLetter.isKhmerLetter(txtFirstNameOTH.getValue())) {
            errors.add(I18N.message("first.name.allow.input.khmer.only"));
        }
        if (!ValidateLetter.isEnglishLetter(txtFamilyName.getValue())) {
            errors.add(I18N.message("last.name.not.allow.to.input.number.or.firstspace.endspace.or.symbols.en"));
        }
        if (!ValidateLetter.isEnglishLetter(txtFirstName.getValue())) {
            errors.add(I18N.message("first.name.not.allow.to.input.number.or.firstspace.endspace.or.symbols.en"));
        }
        return errors;
    }

    public Address getAddress() {
        Address address = new Address();
        address.setHouseNo(txtHouseNo.getValue());
        address.setStreet(txtStreet.getValue());
        address.setCountry(cbxCountry.getSelectedEntity());
        address.setProvince(cbxProvince.getSelectedEntity());
        address.setDistrict(cbxDistrict.getSelectedEntity());
        address.setCommune(cbxCommune.getSelectedEntity());
        address.setVillage(cbxVillage.getSelectedEntity());
        address.setType(ETypeAddress.MAIN);
        return address;
    }

    public void setEnableIdentityPanel(boolean isTrue) {
        txtFamilyNameOTH.setEnabled(isTrue);
        txtFirstName.setEnabled(isTrue);
        txtFirstNameOTH.setEnabled(isTrue);
        txtFamilyName.setEnabled(isTrue);
        txtNickname.setEnabled(isTrue);
        dfDateOfBirths.setEnabled(isTrue);
        cbxProvinceOfBirth.setEnabled(isTrue);
        cbxCivility.setEnabled(isTrue);
        cbxGender.setEnabled(isTrue);
        cbxMaritalStatus.setEnabled(isTrue);
        cbxNationality.setEnabled(isTrue);
        txtMobilePhone1.setEnabled(isTrue);
        txtMobilePhone2.setEnabled(isTrue);
        txtSpouseFirstName.setEnabled(isTrue);
        txtSpouseFamilyName.setEnabled(isTrue);
        txtSpouseMobilePhone.setEnabled(isTrue);
        txtNumberOfChildren.setEnabled(isTrue);

        currentAddressPanel.setEnabled(isTrue);
    }
}
