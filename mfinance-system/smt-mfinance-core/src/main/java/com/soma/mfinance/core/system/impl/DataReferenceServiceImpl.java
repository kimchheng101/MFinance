package com.soma.mfinance.core.system.impl;

import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.quotation.model.SupportDecision;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.springframework.stereotype.Service;

import com.soma.common.app.tools.helper.AppServicesHelper;
import com.soma.mfinance.core.document.service.DocumentService;
import com.soma.mfinance.core.financial.model.ProductLine;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.system.DataReferenceService;

/**
 * Created by cheasocheat on 2/27/17.
 */
@Service("dataReferenceService")
public class DataReferenceServiceImpl implements DataReferenceService,AppServicesHelper, FMEntityField {
    @Override
    public void initialized() {
        //EntityService entityService = (EntityService) SecApplicationContextHolder.getContext().getBean("entityService");

        DocumentService documentService= SpringUtils.getBean(DocumentService.class);
        try{
            DataReference.getInstance().setDocuments(documentService.getDocuments());
            DataReference.getInstance().setProductLines(ENTITY_SRV.list(ProductLine.class, 
    				Restrictions.eq(STATUS_RECORD, EStatusRecord.ACTIV), Order.asc(DESC_EN)));
            DataReference.getInstance().setSupportDecisions(ENTITY_SRV.list(SupportDecision.class,
                    Restrictions.eq(STATUS_RECORD, EStatusRecord.ACTIV), Order.asc(CODE)));

            BaseRestrictions<Dealer> restrictionsDealer = new BaseRestrictions<Dealer>(Dealer.class);
            restrictionsDealer.getStatusRecordList().add(EStatusRecord.ACTIV);
            DataReference.getInstance().setDealers(ENTITY_SRV.list(restrictionsDealer));

        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
