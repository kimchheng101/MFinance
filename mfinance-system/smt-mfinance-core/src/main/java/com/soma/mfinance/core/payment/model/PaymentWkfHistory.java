package com.soma.mfinance.core.payment.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.soma.common.app.workflow.model.WkfBaseHistory;

/**
 * 
 * @author kimsuor.seang
 *
 */
@Entity
@Table(name = "td_payment_wkf_history")
public class PaymentWkfHistory extends WkfBaseHistory {
	/** */
	private static final long serialVersionUID = -4389520369025754771L;

	@Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "wkf_his_id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}
	
	@SuppressWarnings("unchecked")
	@OneToMany(mappedBy = "wkfHistory", fetch = FetchType.LAZY)
	@Override
	public List<PaymentWkfHistoryItem> getHistItems() {
		return (List<PaymentWkfHistoryItem>) super.getHistItems();
	}

}
