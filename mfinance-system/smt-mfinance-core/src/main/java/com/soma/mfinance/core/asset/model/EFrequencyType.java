package com.soma.mfinance.core.asset.model;

import java.util.List;

import javax.persistence.AttributeConverter;

import org.seuksa.frmk.model.eref.BaseERefData;

/**
 * 
 * @author p.ly
 *
 */
public class EFrequencyType extends BaseERefData implements AttributeConverter<EFrequencyType, Long> {
	/** */
	private static final long serialVersionUID = 8800717908072047609L;

	/**
	 */
	public EFrequencyType() {
	}
	
	/**
	 * @param code
	 * @param id
	 */
	public EFrequencyType(String code, long id) {
		super(code, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public EFrequencyType convertToEntityAttribute(Long id) {
		return super.convertToEntityAttribute(id);
	}
	
	@Override
	public Long convertToDatabaseColumn(EFrequencyType arg0) {
		return super.convertToDatabaseColumn(arg0);
	}

	/** 
	 * @return
	 */
	public static List<EFrequencyType> values() {
		return getValues(EFrequencyType.class);
	}
	
	/**
	 * @param code
	 * @return
	 */
	public static EFrequencyType getByCode(String code) {
		return getByCode(EFrequencyType.class, code);
	}
	
	/**
	 * @param id
	 * @return
	 */
	public static EFrequencyType getById(long id) {
		return getById(EFrequencyType.class, id);
	}
}
