package com.soma.mfinance.core.custom.component;

import com.soma.mfinance.core.widget.EntityRefListSelect;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.model.entity.EntityRefA;

import java.util.List;

/**
 * Created by Dang DIM
 * Date     : 28-Jun-17, 3:58 PM
 * Email    : d.dim@gl-f.com
 */
public class TwinEntityRefListSelect<T extends EntityRefA> extends HorizontalLayout implements Button.ClickListener {

    private GridLayout mainLayout;
    private VerticalLayout verticalLayout;
    private NativeButton buttonRight;
    private NativeButton buttonLeft;
    private EntityRefListSelect<T> sourceListSelect;
    private EntityRefListSelect<T> resultListSelect;
    private ItemChangeListener<T> itemChangeListener;

    public TwinEntityRefListSelect() {
        this(null);
    }

    public TwinEntityRefListSelect(String caption) {
        initComponent(caption);
    }

    private void initComponent(String caption) {
        resultListSelect = new EntityRefListSelect(caption);
        sourceListSelect = new EntityRefListSelect(caption);
        buttonRight = new NativeButton();
        buttonRight.setIcon(FontAwesome.ANGLE_RIGHT);
        buttonRight.addClickListener(this);
        buttonLeft = new NativeButton();
        buttonLeft.setIcon(FontAwesome.ANGLE_LEFT);
        buttonLeft.addClickListener(this);

        mainLayout = new GridLayout(3, 1);
        verticalLayout = new VerticalLayout();
        verticalLayout.setMargin(new MarginInfo(true, true, true, true));
        verticalLayout.addComponent(buttonRight);
        verticalLayout.addComponent(buttonLeft);
        mainLayout.addComponent(sourceListSelect, 0, 0);
        mainLayout.addComponent(verticalLayout, 1, 0);
        mainLayout.addComponent(resultListSelect, 2, 0);
        addComponent(mainLayout);
    }

    public void setRestrictions(BaseRestrictions<T> restrictions) {
        sourceListSelect.setRestrictions(restrictions);
    }

    public void addItemChangeListener(ItemChangeListener<T> itemChangeListener) {
        this.itemChangeListener = itemChangeListener;
    }

    public List<T> getSourceSelectEntities() {
        return sourceListSelect.getSelectedEntities();
    }

    public void setSourceSelectEntities(List<T> list) {
        this.sourceListSelect.setSelectedEntities(list);
    }

    public List<T> getResultSelectEntities() {
        return resultListSelect.getSelectedEntities();
    }

    public void setResultSelectEntities(List<T> list) {
        this.resultListSelect.setSelectedEntities(list);
    }

    public void resultSelectAddEntity(T entity) {
        this.resultListSelect.addEntity(entity);
    }

    public void resultSelectAddEntities(List<T> list) {
        this.resultListSelect.addEntities(list);
    }

    public List<T> getSourceAllEntities() {
        return sourceListSelect.getAllEntities();
    }

    public List<T> getResultAllEntities() {
        return resultListSelect.getAllEntities();
    }

    public EntityRefListSelect<T> getSourceListSelect() {
        return sourceListSelect;
    }

    public EntityRefListSelect<T> getResultListSelect() {
        return resultListSelect;
    }

    public void renderer() {
        sourceListSelect.renderer();
    }

    public boolean isEmpty() {
        return resultListSelect.getAllEntities().isEmpty();
    }

    public void setWidth(int width) {
        sourceListSelect.setWidth(200, Unit.PIXELS);
        resultListSelect.setWidth(200, Unit.PIXELS);
    }

    public void setMultiSelect(boolean multiSelect) {
        sourceListSelect.setMultiSelect(multiSelect);
        resultListSelect.setMultiSelect(multiSelect);
    }

    public NativeButton getButtonRight() {
        return buttonRight;
    }

    public NativeButton getButtonLeft() {
        return buttonLeft;
    }

    private void moveItem(EntityRefListSelect source, EntityRefListSelect destination) {
        List<T> list = source.getSelectedEntities();
        for (T object : list) {
            destination.addEntity(object);
            source.removeEntity(object);
        }
    }

    @Override
    public void buttonClick(Button.ClickEvent event) {
        Button btn = (Button) event.getSource();
        if (btn.equals(buttonRight)) {
            moveItem(sourceListSelect, resultListSelect);
            if (itemChangeListener != null)
                itemChangeListener.onItemAdded(resultListSelect.getAllEntities());
        } else {
            if (itemChangeListener != null)
                itemChangeListener.onItemRemoved(resultListSelect.getSelectedEntities());
            moveItem(resultListSelect, sourceListSelect);
        }
    }

    public interface ItemChangeListener<T extends EntityRefA> {
        void onItemAdded(List<T> list);

        void onItemRemoved(List<T> list);
    }
}
