package com.soma.mfinance.core.financial.model;

import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
/**
 * Created by b.chea on 4/5/2017.
 */
@Entity
@Table(name = "tu_lease_amount")
public class LeaseAmountPercent extends EntityRefA implements MLeaseAmount{

    private Double value;

    /**
     * @return id
     */
    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "les_amount_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * @see org.seuksa.frmk.model.entity.EntityRefA#getCode()
     */
    @Override
    @Transient
    public String getCode() {
        return "TMP";
    }

    @Override
    @Column(name = "les_amount_desc", nullable = true, length=255)
    public String getDesc() {
        return super.getDesc();
    }

    @Override
    @Column(name = "les_amount_desc_en", nullable = true, length=255)
    public String getDescEn() {
        return super.getDescEn();
    }

    /**
     * @return the value
     */
    @Column(name = "les_amount_value", nullable = true)
    public Double getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Double value) {
        this.value = value;
    }
}
