package com.soma.mfinance.core.financial.model;

/**
 * Meta data of com.soma.mfinance.core.financial.model.InsuranceClaims
 * @author kimsuor.seang
 */
public interface MInsuranceClaims {
	
	public final static String INSURANCE = "insurance";
	public final static String RANGEOFYEAR = "rangeOfYear";
	public final static String FROM = "from";
	public final static String TO = "to";
	public final static String PREMIUMNREFUNDEDPERCENTAGE = "premiumnRefundedPercentage";
	
}
