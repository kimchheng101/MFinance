package com.soma.mfinance.core.applicant.service;

import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.ersys.core.hr.model.address.Address;
import com.soma.ersys.core.hr.model.address.Commune;
import com.soma.ersys.core.hr.model.address.District;
import com.soma.ersys.core.hr.model.address.Province;
import com.soma.ersys.core.hr.model.address.Village;
import com.soma.common.app.eref.ECountry;
import com.soma.ersys.core.hr.model.eref.ETypeAddress;
import com.soma.ersys.core.hr.model.organization.OrgStructure;

import java.util.List;

/**
 * Address service interface
 * @author kimsuor.seang
 *
 */
public interface AddressService extends BaseEntityService {

	/**
	 * Get province by code
	 * @param country
	 * @param code
	 * @return
	 */
	Province getProvineByCode(ECountry country, String provinceCode);
	
	/**
	 * Get District by code
	 * @param country
	 * @param code
	 * @return
	 */
	District getDistrictByCode(Province province, String districtCode);
	
	/**
	 * Get Commune by code
	 * @param country
	 * @param code
	 * @return
	 */
	Commune getCommuneByCode(District district, String communeCode);
	
	/**
	 * Get Village by code
	 * @param country
	 * @param code
	 * @return
	 */
	Village getVillageByCode(Commune commune, String villageCode);
	
	/**
	 * @param address
	 * @param applicant
	 * @return
	 */
	boolean isAddressUsedByOtherApplicant(Address address, Applicant applicant);
	
	/**
	 * Add unknown addresses reference
	 */
	void addUnknownAddressesReference();
	
	/**
	 * 
	 * @param address
	 * @return
	 */
	String getDetailAddress(Address address);
	
	/**
	 * 
	 * @param orgStructure
	 * @return
	 */
	String getBranchAddress(OrgStructure orgStructure);
	
	/**
	 * 
	 * @param dealer
	 * @return
	 */
	String getDealerAddress(Dealer dealer);
	
	/**
	 * Get Addres to Return / Repo
	 * @param contract
	 * @param typeAddress
	 * @return
	 */
	Address getAddress(Contract contract, ETypeAddress typeAddress);

	List<District> findDistrictByProvince(Province province) ;
	List<Commune> findCommuneByDistrict(District district);
	List<Village> findVillageByCommune(Commune commune);
}
