/**
 * 
 */
package com.soma.mfinance.glf.mplus.accounting.service.impl;

import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.payment.model.EPaymentMethod;
import com.soma.mfinance.core.payment.model.EPaymentType;
import com.soma.mfinance.core.payment.model.Payment;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.mplus.accounting.PaymentServiceMfp;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.mfinance.core.workflow.PaymentWkfStatus;
import com.soma.frmk.security.model.SecUser;
import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.seuksa.frmk.tools.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author vi.sok
 *
 */
@Service("paymentServiceMfp")
@Transactional
public class PaymentServiceImplMfp extends BaseEntityServiceImpl implements PaymentServiceMfp, FinServicesHelper {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2899642349987538497L;
	protected Logger LOG = LoggerFactory.getLogger(getClass());
	@Autowired
    private EntityDao dao;
	

	@Override
	public BaseEntityDao getDao() {
		// TODO Auto-generated method stub
		return dao;
	}
	@Override
	public Payment createPayment(List<Cashflow> cashflows, Date paymentDate, int penaltyDay, Dealer dealer, EPaymentMethod paymentMethod) {
		double totalPaid = 0d;
		SecUser receivedUser;
		for (Cashflow cashflow : cashflows) {
			totalPaid += cashflow.getTiInstallmentAmount() + cashflow.getVatInstallmentAmount();
			ENTITY_SRV.saveOrUpdate(cashflow);
		}
		Cashflow cashflow = cashflows.get(0);
		Contract contract = cashflow.getContract();
		Payment payment = new Payment();
		payment.setApplicant(contract.getApplicant());
		payment.setContract(contract);
		payment.setPaymentDate(paymentDate);
		payment.setTePaidAmount(totalPaid);
		payment.setTiPaidAmount(totalPaid);
		payment.setPaid(true);
		payment.setWkfStatus(PaymentWkfStatus.PAI);
		try {
			receivedUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		}catch (NullPointerException e) {
			receivedUser = PAYMENT_SRV.getByDesc(SecUser.class, paymentMethod.toString());
		}
		payment.setReceivedUser(receivedUser);
		payment.setPaymentType(EPaymentType.IRC);
		payment.setNumPenaltyDays(penaltyDay);
		payment.setPaymentMethod(paymentMethod);
		payment.setDealer(dealer);
		payment.setCashflows(cashflows);
		contract.setLastPaidNumInstallment(cashflow.getNumInstallment());
		contract.setLastPaidDateInstallment(cashflow.getInstallmentDate());

		if(cashflow.getNumInstallment() == contract.getTerm()){
			CONT_SRV.changeContractStatus(contract, ContractWkfStatus.CLO);
			Quotation quotation = contract.getQuotation();
			quotation.setWkfStatus(ContractWkfStatus.CLO);
			ENTITY_SRV.saveOrUpdate(quotation);
		}

		ENTITY_SRV.saveOrUpdate(contract);
		ENTITY_SRV.saveOrUpdate(payment);
		for (Cashflow cashflowpaid : cashflows) {
			cashflowpaid.setPayment(payment);
			cashflowpaid.setPaid(true);
			ENTITY_SRV.saveOrUpdate(cashflowpaid);
		}
		return payment;
	}
	
	@Override
	public Payment createPaidOffPayment(List<Cashflow> cashflows,
			Date paymentDate, int penaltyDay, Dealer dealer,
			EPaymentMethod paymentMethod) {

		SecUser receivedUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		double totalPaid = 0d;
		for (Cashflow cashflow : cashflows) {
			totalPaid += cashflow.getTiInstallmentAmount() + cashflow.getVatInstallmentAmount();
			ENTITY_SRV.saveOrUpdate(cashflow);
		}
		Cashflow cashflow = cashflows.get(0);
		Contract contract = cashflow.getContract();
		Payment payment = new Payment();
		payment.setApplicant(contract.getApplicant());
		payment.setContract(contract);
		payment.setPaymentDate(paymentDate);
		payment.setTePaidAmount(totalPaid);
		payment.setTiPaidAmount(totalPaid);
		payment.setPaid(true);
		payment.setWkfStatus(PaymentWkfStatus.PAI);
		payment.setReceivedUser(receivedUser);	
		payment.setPaymentType(EPaymentType.IRC);
		payment.setNumPenaltyDays(penaltyDay);
		payment.setPaymentMethod(paymentMethod);
		payment.setDealer(dealer);
		payment.setCashflows(cashflows);
		contract.setPreviousPaidNumInstallment(contract.getLastPaidNumInstallment());
		contract.setLastPaidNumInstallment(cashflow.getNumInstallment());
		contract.setLastPaidDateInstallment(cashflow.getInstallmentDate());
		contract.setWkfStatus(ContractWkfStatus.EAR);
		Quotation quotation = contract.getQuotation();
		quotation.setWkfStatus(ContractWkfStatus.EAR);

		ENTITY_SRV.saveOrUpdate(contract);
		ENTITY_SRV.saveOrUpdate(quotation);
		ENTITY_SRV.saveOrUpdate(payment);
		for (Cashflow cashflowpaid : cashflows) {
			cashflowpaid.setPayment(payment);
			cashflowpaid.setPaid(true);
			ENTITY_SRV.saveOrUpdate(cashflowpaid);
		}
		return payment;
	}
	
	@Override
	public void cancelInstallment(Payment payment) {
		payment.setCancelDate(DateUtils.today());
		payment.setPaid(false);
		List<Cashflow> cashflows = payment.getCashflows();
		for (Cashflow cashflow : cashflows) {
			cashflow.setCancel(true);
			cashflow.setCancelationDate(DateUtils.today());
			saveOrUpdate(cashflow);
		}
		Contract contract = payment.getContract();
		contract.setLastPaidNumInstallment(contract.getLastPaidNumInstallment() - 1);
		contract.setLastDueDate(DateUtils.addMonthsDate(cashflows.get(0).getInstallmentDate(), -1));
		saveOrUpdate(contract);
		saveOrUpdate(payment);
	}
}
