package com.soma.mfinance.core.web.spring;

import javax.servlet.ServletContextEvent;

import com.soma.common.app.web.spring.StartupApplicationContextListener;
import com.soma.mfinance.core.helper.FinSettingConfigHelper;
import com.soma.mfinance.core.shared.quotation.SequenceManager;
import com.soma.mfinance.core.system.DataReferenceService;
import org.seuksa.frmk.tools.spring.SpringUtils;

/**
 * 
 * @author kimsuor.seang
 *
 */
public class FinStartupInitializer extends StartupApplicationContextListener {
	private static final String APP_NAME = "EFinance System";
    /**
     * @see org.springframework.web.context.ContextLoaderListener#contextInitialized(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextInitialized(final ServletContextEvent event) {
        logger.info(APP_NAME + " - Context - Initializing...");

        super.contextInitialized(event);
        	
        try {

            DataReferenceService dataReferenceService = SpringUtils.getBean(DataReferenceService.class);

        	logger.info("---------------------- Startup : >> Initialized data reference ------------------------");

            dataReferenceService.initialized();

    		logger.info("---------------------- Startup : << Initialized data reference ------------------------");
    		
    		logger.info("---------------------- Startup : >> Initialized sequence number ------------------------");
    		
    		SequenceManager.getInstance().setQuotationReferenceNumber(FinSettingConfigHelper.getQuotationReferenceNumber());
    		
    		logger.info("---------------------- Sequence number = " + SequenceManager.getInstance().getQuotationReferenceNumber());
    		
    		logger.info("---------------------- Startup : << Initialized sequence number ------------------------");

        }
        catch (Exception e) {
        	String errMsg = APP_NAME + " - Error while context initializing.";
            logger.error(errMsg, e);
        }
        	
        logger.info(APP_NAME + " - Context successfully initialized.");
    }

    /**
     * @see org.springframework.web.context.ContextLoaderListener#contextDestroyed(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextDestroyed(final ServletContextEvent event) {
        super.contextDestroyed(event);
        try {

        }
        catch (Exception e) {
        	String errMsg = APP_NAME + " - Error while context destroying.";
            logger.error(errMsg, e);
        }
		
    }
}
