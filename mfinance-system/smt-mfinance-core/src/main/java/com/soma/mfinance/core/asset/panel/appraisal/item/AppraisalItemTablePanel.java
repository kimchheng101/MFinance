package com.soma.mfinance.core.asset.panel.appraisal.item;

import com.soma.mfinance.core.asset.model.appraisal.AppraisalItem;
import com.soma.mfinance.core.shared.asset.AssetEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 
 * @author vi.sok
 *
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AppraisalItemTablePanel extends AbstractTablePanel<AppraisalItem> implements AssetEntityField {
	
	/** */
	private static final long serialVersionUID = -1435121854065342012L;

	@PostConstruct
	public void PostConstruct() {
		setCaption(I18N.message("appraisal.items"));
		setSizeFull();
		setMargin(true);
		setSpacing(true);
				
		super.init(I18N.message("appraisal.item"));
		
		addDefaultNavigation();
	}	
	
	/**
	 * Get item selected id
	 * @return
	 */
	public Long getItemSelectedId() {
		if (selectedItem != null) {
			return (Long) selectedItem.getItemProperty(ID).getValue();
		}
		return null;
	}	
	
	/**
	 * Get Paged definition
	 * @return
	 */
	@Override
	protected PagedDataProvider<AppraisalItem> createPagedDataProvider() {
		PagedDefinition<AppraisalItem> pagedDefinition = new PagedDefinition<>(searchPanel.getRestrictions());
		pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 50);
		pagedDefinition.addColumnDefinition(CODE, I18N.message("cide"), String.class, Align.LEFT, 170);
		pagedDefinition.addColumnDefinition(DESC_EN, I18N.message("name"), String.class, Align.LEFT, 170);	
		pagedDefinition.addColumnDefinition("appraisalCategory."+ DESC_EN, I18N.message("appraisal.category"), String.class, Align.LEFT, 170);
		pagedDefinition.addColumnDefinition("value", I18N.message("value"), Double.class, Align.LEFT, 170);
		pagedDefinition.addColumnDefinition(coefficient, I18N.message("coefficient"), Double.class, Align.LEFT, 170);
		pagedDefinition.addColumnDefinition("appraisalGroup." + DESC_EN, I18N.message("groupdetail.group"), Double.class, Align.LEFT, 170);
		pagedDefinition.addColumnDefinition("assetRange." + DESC_EN, I18N.message("asset.range").toUpperCase(), String.class, Align.LEFT, 170);
		pagedDefinition.addColumnDefinition(SORT_INDEX, I18N.message("sortIndex"), Integer.class, Align.LEFT, 170);
		EntityPagedDataProvider<AppraisalItem> pagedDataProvider = new EntityPagedDataProvider<>();
		pagedDataProvider.setPagedDefinition(pagedDefinition);
		return pagedDataProvider;
	}

	/**
	 * @see AbstractTablePanel#getEntity()
	 */
	@Override
	protected AppraisalItem getEntity() {
		final Long id = getItemSelectedId();
		if (id != null) {
		    return ENTITY_SRV.getById(AppraisalItem.class, id);
		}
		return null;
	}

	/**
	 * @see AbstractTablePanel#createSearchPanel()
	 */
	@Override
	protected AppraisalItemSearchPanel createSearchPanel() {
		return new AppraisalItemSearchPanel(this);		
	}
}
