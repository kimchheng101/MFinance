package com.soma.mfinance.core.Enum;

import org.seuksa.frmk.model.eref.BaseERefData;

import javax.persistence.AttributeConverter;
import java.util.List;

/**
 * Created by b.chea on 2/25/2017.
 */
public class EFamilyMember extends BaseERefData implements AttributeConverter<EFamilyMember, Long> {

    public static final EFamilyMember BRO = new EFamilyMember("BRO", 1L);
    public static final EFamilyMember SIS = new EFamilyMember("SIS", 2L);

    public EFamilyMember(){}

    public EFamilyMember(String code, long id){ super(code, id);}

    @Override
    public Long convertToDatabaseColumn(EFamilyMember arg0) {
        return super.convertToDatabaseColumn(arg0);
    }

    @Override
    public EFamilyMember convertToEntityAttribute(Long dbData) {
        return (EFamilyMember) super.convertToEntityAttribute(id);
    }


    public static List<EFamilyMember> values() {
        return getValues(EFamilyMember.class);
    }

    public static EFamilyMember getByCode(String code) {
        return (EFamilyMember) getByCode(EFamilyMember.class, code);
    }

    public static EFamilyMember getById(long id) {
        return (EFamilyMember) getById(EFamilyMember.class, Long.valueOf(id));
    }
}
