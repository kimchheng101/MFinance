package com.soma.mfinance.core.accounting;

import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.shared.contract.PenaltyVO;
import org.seuksa.frmk.tools.amount.Amount;

import java.util.Date;
import java.util.List;

/**
 * 
 * @author vi.sok
 *
 */
public class PayOffVO {
	private int numInstallment;
	private Date installmentDate;
	private Amount pricipal;
	private Amount interest;
	private Amount insuranceFee;
	private Amount serviceFee;
	private Amount penaltyAmount;
	private Amount otherAmount;
	private Amount transferOwnerShip;
	private Amount commissionFee;
	private List<Cashflow> cashflows;
	private List<PenaltyVO> penaltyVOs;
	private double insuranceFeeReturn;
	/**
	 * @return the numInstallment
	 */
	public int getNumInstallment() {
		return numInstallment;
	}
	/**
	 * @param numInstallment the numInstallment to set
	 */
	public void setNumInstallment(int numInstallment) {
		this.numInstallment = numInstallment;
	}
	/**
	 * @return the installmentDate
	 */
	public Date getInstallmentDate() {
		return installmentDate;
	}
	/**
	 * @param installmentDate the installmentDate to set
	 */
	public void setInstallmentDate(Date installmentDate) {
		this.installmentDate = installmentDate;
	}
	/**
	 * @return the pricipal
	 */
	public Amount getPricipal() {
		return pricipal;
	}
	/**
	 * @param pricipal the pricipal to set
	 */
	public void setPricipal(Amount pricipal) {
		this.pricipal = pricipal;
	}
	/**
	 * @return the interest
	 */
	public Amount getInterest() {
		return interest;
	}
	/**
	 * @param interest the interest to set
	 */
	public void setInterest(Amount interest) {
		this.interest = interest;
	}
	/**
	 * @return the insuranceFee
	 */
	public Amount getInsuranceFee() {
		return insuranceFee;
	}
	/**
	 * @param insuranceFee the insuranceFee to set
	 */
	public void setInsuranceFee(Amount insuranceFee) {
		this.insuranceFee = insuranceFee;
	}
	/**
	 * @return the serviceFee
	 */
	public Amount getServiceFee() {
		return serviceFee;
	}
	/**
	 * @param serviceFee the serviceFee to set
	 */
	public void setServiceFee(Amount serviceFee) {
		this.serviceFee = serviceFee;
	}
	/**
	 * @return the otherAmount
	 */
	public Amount getOtherAmount() {
		return otherAmount;
	}
	/**
	 * @param otherAmount the otherAmount to set
	 */
	public void setOtherAmount(Amount otherAmount) {
		this.otherAmount = otherAmount;
	}
	/**
	 * @return the penaltyAmount
	 */
	public Amount getPenaltyAmount() {
		return penaltyAmount;
	}
	/**
	 * @param penaltyAmount the penaltyAmount to set
	 */
	public void setPenaltyAmount(Amount penaltyAmount) {
		this.penaltyAmount = penaltyAmount;
	}
	/**
	 * @return the cashflows
	 */
	public List<Cashflow> getCashflows() {
		return cashflows;
	}
	/**
	 * @param cashflows the cashflows to set
	 */
	public void setCashflows(List<Cashflow> cashflows) {
		this.cashflows = cashflows;
	}

	public List<PenaltyVO> getPenaltyVOs(){
		return penaltyVOs;
	}
	public void setPenaltyVOs(List<PenaltyVO> penaltyVOs){
		this.penaltyVOs=penaltyVOs;
	}

	public Amount getTransferOwnerShip() {
		return transferOwnerShip;
	}
	/**
	 * @param transferOwnerShip the otherAmount to set
	 */
	public void setTransferOwnerShip(Amount transferOwnerShip) {
		this.transferOwnerShip = transferOwnerShip;
	}

	public Amount getCommissionFee() {
		return commissionFee;
	}
	public void setCommissionFee(Amount commissionFee) {
		this.commissionFee = commissionFee;
	}

	public double getInsuranceFeeReturn() {
		return insuranceFeeReturn;
	}

	public void setInsuranceFeeReturn(double insuranceFeeReturn) {
		this.insuranceFeeReturn = insuranceFeeReturn;
	}
}
