package com.soma.mfinance.core.collection.model;

import java.util.Date;

import javax.persistence.*;

import com.soma.mfinance.core.address.model.Area;
import org.seuksa.frmk.model.entity.EntityA;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.payment.model.Payment;
import com.soma.frmk.security.model.SecUser;

/**
 * 
 * @author kimsuor.seang
 *
 */
@Entity
@Table(name = "tm_collection_incentive_report")
public class CollectionIncentiveReport extends EntityA {
	
	private static final long serialVersionUID = -5850483262377014737L;
	
	private Date date;
	private SecUser collectionOfficer;
	private Payment payment;
	private EWkfStatus collectionStatus;
	private EColTask collectionTask;
	private EColGroup collectionGroup;
	private ColCustField customerAttribute; 
	private Double amountPromiseToPay;
	private Date startPeriodPromiseToPay;
	private Date endPeriodPromiseToPay;
	private Integer nbOverdueInDay;
	private EColResult colResult;
	private Collection collection;
	private Area area;

	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "colec_id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	@Column(name = "colec_date", nullable = true)
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pay_id", nullable = true)
	public Payment getPayment() {
		return payment;
	}
	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_usr_id", nullable = true)
	public SecUser getAssignee() {
		return collectionOfficer;
	}
	public void setAssignee(SecUser collectionOfficer) {
		this.collectionOfficer = collectionOfficer;
	}

    @Column(name = "wkf_sta_id", nullable = true)
    @Convert(converter = EWkfStatus.class)
	public EWkfStatus getCollectionStatus() {
		return collectionStatus;
	}

	public void setCollectionStatus(EWkfStatus collectionStatus) {
		this.collectionStatus = collectionStatus;
	}
    @Column(name = "col_tas_id", nullable = true)
    @Convert(converter = EColTask.class)
	public EColTask getCollectionTask() {
		return collectionTask;
	}

	public void setCollectionTask(EColTask collectionTask) {
		this.collectionTask = collectionTask;
	}
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "col_grp_id", nullable = true)
	public EColGroup getGroup() {
		return collectionGroup;
	}

	public void setGroup(EColGroup collectionGroup) {
		this.collectionGroup = collectionGroup;
	}
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "col_cus_fie_id", nullable = true)
	public ColCustField getCustomerAttribute() {
		return customerAttribute;
	}

	public void setCustomerAttribute(ColCustField customerAttribute) {
		this.customerAttribute = customerAttribute;
	}
	@Column(name = "colec_am_amount_promise_to_pay_usd", nullable = true)
	public Double getAmountPromiseToPay() {
		return amountPromiseToPay;
	}

	public void setAmountPromiseToPay(Double amountPromiseToPayUsd) {
		this.amountPromiseToPay = amountPromiseToPayUsd;
	}
	@Column(name = "colec_dt_start_period_promise_to_pay", nullable = true)
	public Date getStartPeriodPromiseToPay() {
		return startPeriodPromiseToPay;
	}

	public void setStartPeriodPromiseToPay(Date startPeriodPromiseToPay) {
		this.startPeriodPromiseToPay = startPeriodPromiseToPay;
	}
	@Column(name = "colec_dt_end_period_promise_to_pay", nullable = true)
	public Date getEndPeriodPromiseToPay() {
		return endPeriodPromiseToPay;
	}

	public void setEndPeriodPromiseToPay(Date endPeriodPromiseToPay) {
		this.endPeriodPromiseToPay = endPeriodPromiseToPay;
	}

	@Column(name = "colec_nb_overdue_in_day", nullable = true)
	public Integer getNbOverdueInDay() {
		return nbOverdueInDay;
	}

	public void setNbOverdueInDay(Integer nbOverdueInDay) {
		this.nbOverdueInDay = nbOverdueInDay;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "col_res_id", nullable = true)
	public EColResult getColResult() {
		return colResult;
	}
	public void setColResult(EColResult eColResult){
		this.colResult = eColResult;
	}

	public void setCollectionOfficer(SecUser collectionOfficer) {
		this.collectionOfficer = collectionOfficer;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "col_id", nullable = true)
	public Collection getCollection() {
		return collection;
	}
	public void setCollection(Collection collection) {
		this.collection = collection;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "are_id", nullable = true)
	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}
}
