package com.soma.mfinance.core.contract.model;

import org.seuksa.frmk.model.entity.MEntityA;


/**
 * Meta data of com.soma.mfinance.core.contract_old.model.ContractNote
 * @author kimsuor.seang
 */
public interface MContractNote extends MEntityA {
	
	public final static String CONTRACT = "contract";
	public final static String SUBJECT = "subject";
	public final static String NOTE = "note";
	public final static String COMMENT = "comment";
	public final static String USER = "user";

}
