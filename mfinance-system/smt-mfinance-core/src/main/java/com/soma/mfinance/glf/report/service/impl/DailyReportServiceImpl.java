package com.soma.mfinance.glf.report.service.impl;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.shared.util.DateFilterUtil;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.mfinance.glf.report.model.DailyReport;
import com.soma.mfinance.glf.report.service.DailyReportService;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.seuksa.frmk.tools.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import static com.soma.mfinance.core.quotation.model.MQuotation.WKFSTATUS;

/**
 * @author kimsuor.seang
 */
@Service("dailyReportService")
public class DailyReportServiceImpl extends BaseEntityServiceImpl implements DailyReportService, QuotationEntityField {

    /**
     */
    private static final long serialVersionUID = -4067699348715869380L;

    @Autowired
    private EntityDao entityDao;

    @Override
    public EntityDao getDao() {
        return entityDao;
    }

    @Override
    public DailyReport calculateDailyReportByDealer(Dealer dealer, Date selectDate) {
        return calculateDailyReportByDealer(dealer, selectDate, false);
    }

    @Override
    public DailyReport calculateDailyReportByDealer(Dealer dealer, Date selectDate, boolean closed) {

        BaseRestrictions<DailyReport> restrictions = new BaseRestrictions<>(DailyReport.class);
        restrictions.addCriterion(Restrictions.eq("dealer.id", dealer.getId()));
        restrictions.addCriterion(Restrictions.ge("date", DateUtils.getDateAtBeginningOfDay(selectDate)));
        restrictions.addCriterion(Restrictions.le("date", DateUtils.getDateAtEndOfDay(selectDate)));
        List<DailyReport> dailyReports = list(restrictions);

        DailyReport dailyReport = null;
        if (dailyReports != null && !dailyReports.isEmpty()) {
            dailyReport = dailyReports.get(0);
        } else {
            dailyReport = new DailyReport();
        }
        if (selectDate != null  && DateUtils.isSameDay(selectDate, DateUtils.todayH00M00S00()) && !dailyReport.isClosed()) {

            dailyReport.setDealer(dealer);
            dailyReport.setDate(DateUtils.getDateAtBeginningOfDay(selectDate));
            dailyReport.setNbDealerAppraisal(getNbAppraisal(dealer, selectDate));
            dailyReport.setNumberProcessAppraisal(getNumberProcessAtAppraisal(dealer, selectDate));
            dailyReport.setApply(getNumApply(dealer, selectDate));
            dailyReport.setApprove(getNbQuotationByStatus(dealer, QuotationWkfStatus.APV, selectDate));
            dailyReport.setInProcessAtPoS(getNumberProcessAtPOS(dealer, selectDate));
            dailyReport.setInProcessAtUW(getNumberProcessAtUW(dealer, selectDate));
            dailyReport.setReject(getNbQuotationByStatus(dealer, QuotationWkfStatus.REJ, selectDate));
            dailyReport.setDecline(getNbQuotationByStatus(dealer, QuotationWkfStatus.DEC, selectDate));
            dailyReport.setPendingNewContract(getNumberPendingNewContract(dealer, selectDate));
            dailyReport.setNewContract(getNbDealerContractsToday(dealer, selectDate));

            dailyReport.setNbDealerApplicationsLastMonthFromBeginToDate(getNbApplicationsLastMonthFromBeginToDate(selectDate, dealer));
            dailyReport.setNbDealerContractsLastMonthFromBeginToDate(getNbContractsLastMonthFromBeginToDate(selectDate, dealer));

            dailyReport.setDealerAccumulateNewContracts(getDealerAccumulateNewContracts(dealer));
            dailyReport.setNbDealerContractsUntilLastMonth(getNbDealerContractsUntilLastMonth(selectDate, dealer));
            dailyReport.setNbDealerApplicationsFromBeginOfMonth(getNbDealerApplicationsFromBeginOfMonth(selectDate, dealer));
            dailyReport.setNbDealerContractsFromBeginOfMonth(getNbDealerContractsFromBeginOfMonth(selectDate, dealer));

            dailyReport.setClosed(closed);
            saveOrUpdate(dailyReport);
        }

        return dailyReport;
    }

    /**
     * @param selectDate
     */
    public void recalculateDailyReportOnSelectDate(Date selectDate) {

        List<Dealer> dealers = DataReference.getInstance().getDealers();

        for (Dealer dealer : dealers) {
            BaseRestrictions<DailyReport> restrictions = new BaseRestrictions<>(DailyReport.class);
            restrictions.addCriterion(Restrictions.eq("dealer.id", dealer.getId()));
            restrictions.addCriterion(Restrictions.ge("date", DateUtils.getDateAtBeginningOfDay(selectDate)));
            restrictions.addCriterion(Restrictions.le("date", DateUtils.getDateAtEndOfDay(selectDate)));
            List<DailyReport> dailyReports = list(restrictions);

            DailyReport dailyReport = null;
            if (dailyReports != null && !dailyReports.isEmpty()) {
                dailyReport = dailyReports.get(0);
            }
            if (selectDate != null) {
                dailyReport.setDealer(dealer);
                dailyReport.setDate(DateUtils.getDateAtBeginningOfDay(selectDate));

                dailyReport.setNbDealerAppraisal(getNbAppraisal(dealer, selectDate));
                dailyReport.setNumberProcessAppraisal(getNumberProcessAtAppraisal(dealer, selectDate));
                dailyReport.setApply(getNumApply(dealer, selectDate));
                dailyReport.setApprove(getNbQuotationByStatus(dealer, QuotationWkfStatus.APV, selectDate));
                dailyReport.setInProcessAtPoS(getNumberProcessAtPOS(dealer, selectDate));
                dailyReport.setInProcessAtUW(getNumberProcessAtUW(dealer, selectDate));
                dailyReport.setReject(getNbQuotationByStatus(dealer, QuotationWkfStatus.REJ, selectDate));
                dailyReport.setDecline(getNbQuotationByStatus(dealer, QuotationWkfStatus.DEC, selectDate));
                dailyReport.setPendingNewContract(getNumberPendingNewContract(dealer, selectDate));
                dailyReport.setNewContract(getNbDealerContractsToday(dealer, selectDate));

                dailyReport.setNbDealerApplicationsLastMonthFromBeginToDate(getNbApplicationsLastMonthFromBeginToDate(selectDate, dealer));
                dailyReport.setNbDealerContractsLastMonthFromBeginToDate(getNbContractsLastMonthFromBeginToDate(selectDate, dealer));

                dailyReport.setDealerAccumulateNewContracts(getDealerAccumulateNewContracts(dealer));
                dailyReport.setNbDealerContractsUntilLastMonth(getNbDealerContractsUntilLastMonth(selectDate, dealer));
                dailyReport.setNbDealerApplicationsFromBeginOfMonth(getNbDealerApplicationsFromBeginOfMonth(selectDate, dealer));
                dailyReport.setNbDealerContractsFromBeginOfMonth(getNbDealerContractsFromBeginOfMonth(selectDate, dealer));

                saveOrUpdate(dailyReport);
            }
        }

    }

    /**
     * @param from
     * @param to
     */
    @Override
    public synchronized void recalculateDailyReport(Date from, Date to) {

        BaseRestrictions<DailyReport> restrictions = new BaseRestrictions<>(DailyReport.class);
        restrictions.addCriterion(Restrictions.ge("date", DateUtils.getDateAtBeginningOfDay(from)));
        restrictions.addCriterion(Restrictions.le("date", DateUtils.getDateAtEndOfDay(to)));
        List<DailyReport> dailyReports = list(restrictions);

        for (DailyReport dailyReport : dailyReports) {
            Date selectDate = dailyReport.getDate();
            Dealer dealer = dailyReport.getDealer();
            dailyReport.setDate(DateUtils.getDateAtBeginningOfDay(selectDate));

            dailyReport.setApply(getNumApply(dealer, selectDate));
            dailyReport.setNbDealerAppraisal(getNbAppraisal(dealer, selectDate));
            dailyReport.setNumberProcessAppraisal(getNumberProcessAtAppraisal(dealer, selectDate));
            dailyReport.setApprove(getNbQuotationByStatus(dealer, QuotationWkfStatus.APV, selectDate));
            dailyReport.setReject(getNbQuotationByStatus(dealer, QuotationWkfStatus.REJ, selectDate));
            dailyReport.setDecline(getNbQuotationByStatus(dealer, QuotationWkfStatus.DEC, selectDate));
            dailyReport.setNewContract(getNbDealerContractsToday(dealer, selectDate));

            dailyReport.setNbDealerApplicationsLastMonthFromBeginToDate(getNbApplicationsLastMonthFromBeginToDate(selectDate, dealer));
            dailyReport.setNbDealerContractsLastMonthFromBeginToDate(getNbContractsLastMonthFromBeginToDate(selectDate, dealer));

            dailyReport.setDealerAccumulateNewContracts(getDealerAccumulateNewContracts(dealer));
            dailyReport.setNbDealerContractsUntilLastMonth(getNbDealerContractsUntilLastMonth(selectDate, dealer));
            dailyReport.setNbDealerApplicationsFromBeginOfMonth(getNbDealerApplicationsFromBeginOfMonth(selectDate, dealer));
            dailyReport.setNbDealerContractsFromBeginOfMonth(getNbDealerContractsFromBeginOfMonth(selectDate, dealer));
            dailyReport.setNumberAppraisalDealerLastMonthFromBeginTodate(getAppraisalLastMonth(dealer, selectDate));

            saveOrUpdate(dailyReport);
        }
    }

    private long getNbAppraisal(Dealer dealer, Date date) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.ge("firstSubmissionDate", DateFilterUtil.getStartDate(date)));
        restrictions.addCriterion(Restrictions.le("firstSubmissionDate", DateFilterUtil.getEndDate(date)));
        if (dealer != null)
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        return count(restrictions);
    }

    /**
     * @param dealer
     * @param date
     * @returnm GET NEW CONTRACT FOR ----- ROW IN DATE NUMBER
     */
    private long getNbDealerContractsToday(Dealer dealer, Date date) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.eq(WKFSTATUS, QuotationWkfStatus.ACT));
        restrictions.addCriterion(Restrictions.ge("contractStartDate", DateFilterUtil.getStartDate(DateUtils.getDateAtBeginningOfMonth(date))));
        restrictions.addCriterion(Restrictions.le("contractStartDate", DateFilterUtil.getEndDate(date)));
        if (dealer != null)
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        return count(restrictions);
    }

    /**
     * @param date
     * @return  NUMBER APPRAISAL IN THE NUMBER OF DATE
     */
    public long getNumberProcessAtAppraisal(Dealer dealer, Date date) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.in(WKF_STATUS, new EWkfStatus[]{QuotationWkfStatus.APP, QuotationWkfStatus.AUP}));
        restrictions.addCriterion(Restrictions.ge("quotationDate", DateFilterUtil.getStartDate(DateUtils.getDateAtBeginningOfMonth(date))));
        restrictions.addCriterion(Restrictions.le("quotationDate", DateFilterUtil.getEndDate(date)));
        if (dealer != null)
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        return count(restrictions);
    }

    /**
     * @param dealer
     * @param date
     * @return  GET PROCESS AT POS IN DATE COUNT
     */
    public long getNumberProcessAtPOS(Dealer dealer, Date date) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.in(WKF_STATUS, new EWkfStatus[]{QuotationWkfStatus.ACS, QuotationWkfStatus.RFC, QuotationWkfStatus.QUO}));
        restrictions.addCriterion(Restrictions.ge("quotationDate", DateFilterUtil.getStartDate(DateUtils.getDateAtBeginningOfMonth(date))));
        restrictions.addCriterion(Restrictions.le("quotationDate", DateFilterUtil.getEndDate(date)));
        if (dealer != null)
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        return count(restrictions);
    }

    /**
     * @param dealer
     * @param selectDate
     * @return ------------ GET PROCESS AT UW IN DATE COUNT
     */
    private Long getNumberProcessAtUW(Dealer dealer, Date selectDate) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.in(WKF_STATUS, new EWkfStatus[]{QuotationWkfStatus.PRO, QuotationWkfStatus.PRA, QuotationWkfStatus.APU, QuotationWkfStatus.APS, QuotationWkfStatus.AWU, QuotationWkfStatus.AWS}));
        restrictions.addCriterion(Restrictions.ge("quotationDate", DateFilterUtil.getStartDate(DateUtils.getDateAtBeginningOfMonth(selectDate))));
        restrictions.addCriterion(Restrictions.le("quotationDate", DateFilterUtil.getEndDate(selectDate)));
        if (dealer != null)
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        return count(restrictions);
    }


    /**
     * @param date
     * @return
     */
    public long getNbQuotationByStatus(Dealer dealer, EWkfStatus quotationStatus, Date date) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.eq(WKF_STATUS, quotationStatus));
        if (quotationStatus.equals(QuotationWkfStatus.APV)) {
            restrictions.addCriterion(Restrictions.ge("quotationDate", DateFilterUtil.getStartDate(DateUtils.getDateAtBeginningOfMonth(date))));
            restrictions.addCriterion(Restrictions.le("quotationDate", DateFilterUtil.getEndDate(date)));
        } else if (quotationStatus.equals(QuotationWkfStatus.REJ)) {
            restrictions.addCriterion(Restrictions.ge("rejectDate", DateFilterUtil.getStartDate(DateUtils.getDateAtBeginningOfMonth(date))));
            restrictions.addCriterion(Restrictions.le("rejectDate", DateFilterUtil.getEndDate(date)));
        } else if (quotationStatus.equals(QuotationWkfStatus.DEC)) {
            restrictions.addCriterion(Restrictions.ge("declineDate", DateFilterUtil.getStartDate(DateUtils.getDateAtBeginningOfMonth(date))));
            restrictions.addCriterion(Restrictions.le("declineDate", DateFilterUtil.getEndDate(date)));
        }
        if (dealer != null)
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        return count(restrictions);
    }

    // COUNT PENDING NEW CONTRACT
    public long getNumberPendingNewContract(Dealer dealer, Date date) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.in(WKF_STATUS, new EWkfStatus[] {QuotationWkfStatus.APV, QuotationWkfStatus.AWT,QuotationWkfStatus.WIV}));
        restrictions.addCriterion(Restrictions.ge("quotationDate", DateFilterUtil.getStartDate(DateUtils.getDateAtBeginningOfMonth(date))));
        restrictions.addCriterion(Restrictions.le("quotationDate", DateFilterUtil.getEndDate(date)));
        if (dealer != null)
            restrictions.addCriterion(Restrictions.eq( DEALER + "." + ID, dealer.getId()));
        return count(restrictions);
    }

    /**
     * @param date
     * @param dealer
     * @return number visitor apply
     */
    private long getNumApply(Dealer dealer, Date date) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.ge("quotationDate", DateFilterUtil.getStartDate(DateUtils.getDateAtBeginningOfMonth(date))));
        restrictions.addCriterion(Restrictions.le("quotationDate", DateFilterUtil.getEndDate(date)));
        if (dealer != null) {
            restrictions.addCriterion(Restrictions.eq("dealer.id", dealer.getId()));
        }
        return count(restrictions);
    }

    /**
     * @param dealer
     * @param date
     * @return number of application current date
     */
    private long getNbDealerApplicationsFromBeginOfMonth(Date date, Dealer dealer) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        Date dateBeginningOfMonth = DateUtils.getDateAtBeginningOfMonth(date);
        restrictions.addCriterion(Restrictions.ge("firstSubmissionDate", DateFilterUtil.getStartDate(dateBeginningOfMonth)));
        restrictions.addCriterion(Restrictions.le("firstSubmissionDate", DateFilterUtil.getEndDate(date)));
        if (dealer != null) {
            restrictions.addCriterion(Restrictions.eq("dealer.id", dealer.getId()));
        }
        return count(restrictions);
    }

    /**
     * @param date
     * @return number of total application last month
     */
    private long getNbApplicationsLastMonthFromBeginToDate(Date date, Dealer dealer) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        Date dateEnd = DateUtils.addMonthsDate(date, -1);
        Date dateBeginning = DateUtils.getDateAtBeginningOfMonth(dateEnd);
        restrictions.addCriterion(Restrictions.ge("quotationDate", DateFilterUtil.getStartDate(dateBeginning)));
        restrictions.addCriterion(Restrictions.le("quotationDate", DateFilterUtil.getEndDate(dateEnd)));
        if (dealer != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        }
        return count(restrictions);
    }


    /**
     * @param dealer
     * @param date
     * @return number of current contract
     */
    private long getNbDealerContractsFromBeginOfMonth(Date date, Dealer dealer) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.eq(WKF_STATUS, QuotationWkfStatus.ACT));
        restrictions.addCriterion(Restrictions.ge("activationDate", DateFilterUtil.getStartDate(DateUtils.getDateAtBeginningOfMonth(date))));
        restrictions.addCriterion(Restrictions.le("activationDate", DateFilterUtil.getEndDate(date)));
        if (dealer != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        }
        return count(restrictions);
    }


    /**
     * @param dealer
     * @param date
     * @return number of contract last month
     */
    private long getNbContractsLastMonthFromBeginToDate(Date date, Dealer dealer) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        Date dateEnd = DateUtils.addMonthsDate(date, -1);
        Date dateBeginning = DateUtils.getDateAtBeginningOfMonth(dateEnd);
        restrictions.addCriterion(Restrictions.eq(WKF_STATUS, QuotationWkfStatus.ACT));
        restrictions.addCriterion(Restrictions.ge("activationDate", DateFilterUtil.getStartDate(dateBeginning)));
        restrictions.addCriterion(Restrictions.le("activationDate", DateFilterUtil.getEndDate(dateEnd)));
        if (dealer != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        }
        return count(restrictions);
    }


    /**
     * @param dealer
     * @return total accumulate new contracts
     */
    private long getDealerAccumulateNewContracts(Dealer dealer) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.eq("wkfStatus", QuotationWkfStatus.ACT));
        restrictions.addCriterion(Restrictions.isNotNull("contractStartDate"));
        if (dealer != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        }
        return count(restrictions);
    }

    /**
     * @param date
     * @param dealer
     * @return total previous month contract
     */
    private long getNbDealerContractsUntilLastMonth(Date date, Dealer dealer) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        Date dateEndOfMonth = DateUtils.getDateAtEndOfMonth(DateUtils.addMonthsDate(date, -1));
        restrictions.addCriterion(Restrictions.eq("wkfStatus", QuotationWkfStatus.ACT));
        restrictions.addCriterion(Restrictions.le("activationDate", DateFilterUtil.getEndDate(dateEndOfMonth)));
        if (dealer != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        }
        return (int) count(restrictions);

    }

    private long getAppraisalLastMonth(Dealer dealer, Date date){
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        Date lastMonthDate = DateUtils.getDateAtEndOfMonth(DateUtils.addMonthsDate(date, -1));
        Date dateBeginning = DateUtils.getDateAtBeginningOfMonth(lastMonthDate);
        restrictions.addCriterion(Restrictions.ge("quotationDate", DateFilterUtil.getStartDate(dateBeginning)));
        restrictions.addCriterion(Restrictions.le("quotationDate", DateFilterUtil.getEndDate(lastMonthDate)));
        if (dealer != null)
            restrictions.addCriterion(Restrictions.eq(DEALER+"."+ID, dealer.getId()));
        return count(restrictions);
    }

}
