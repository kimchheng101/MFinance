package com.soma.mfinance.core.financial.model;

import org.seuksa.frmk.model.entity.MEntityA;

/**
 * Meta data of com.soma.mfinance.core.contract.model.CampaignTerm
 * @author kimsuor.seang
 */
public interface MCampaignTerm extends MEntityA {

	public final static String TERM = "term";	
	public final static String CAMPAIGN = "campaign";

}
