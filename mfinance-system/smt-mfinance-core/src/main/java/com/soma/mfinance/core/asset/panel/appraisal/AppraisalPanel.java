package com.soma.mfinance.core.asset.panel.appraisal;

import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.asset.model.EAssetCondition;
import com.soma.mfinance.core.asset.model.appraisal.*;
import com.soma.mfinance.core.common.reference.model.EComponetType;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.ersys.core.hr.model.address.Province;
import com.soma.ersys.core.hr.model.eref.EColor;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.data.Property;
import com.vaadin.event.FieldEvents;
import com.vaadin.ui.*;
import org.apache.commons.lang.StringUtils;
import org.seuksa.frmk.dao.criteria.FilterMode;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.CrudAction;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by seng Kimsour on 7/13/17.
 * Modified: soth sreyrath 09/05/17
 * Modified: kimsuor.seang 10/10/17
 */
public class AppraisalPanel extends AbstractTabPanel implements Property.ValueChangeListener, FieldEvents.BlurListener, AppraisalConstant, FinServicesHelper {

	private TextField txtAssSecMarketPrice;
	private TextField txtAssOriginalPrice;
	private TextArea txtCommentCO;
	private TextArea txtCommentApr;
	private TextField txtOriAppraisalPrice;
	private AssetAppraisal assetAppraisal;
	private Map<String, String> hashError;
	private Quotation quotation;
	private ComboBox cbxValue;
	private EntityRefComboBox cbxProValue;
	private TextField txtValue;
	private TextField txtAdjValue;
	private List<AppraisalItem> appraisalItems;
	private Map<AppraisalItem, Object> mapAppraisalCondition;
	private Map<AppraisalItem, TextField> mapAppraisalAdjValue;
	private Map<Long, TextField> mapAppraisalRemarkCO;
	private Map<Long, TextField> mapAppraisalRemarkApr;
	private Map<Long, List<AppraisalItem>> mapAppraisalCatecory;
	List<AppraisalGroup> appraisalGroupList ;
	private VerticalLayout mainLayout;
	private  Boolean isDream = false;
	private Boolean isWave = false;

	private Boolean checkEvent = false;

	public AppraisalPanel() {
		super();
		this.setSizeFull();
	}

	@Override
	protected Component createForm() {
		mapAppraisalCondition = new HashMap<AppraisalItem, Object>();
		mapAppraisalAdjValue = new HashMap<AppraisalItem, TextField>();
		mapAppraisalRemarkCO = new HashMap<Long, TextField>();
		mapAppraisalRemarkApr = new HashMap<Long, TextField>();
		appraisalItems = new ArrayList<AppraisalItem>();
		mapAppraisalCatecory = new HashMap<>();
		appraisalGroupList = new ArrayList<>();
		txtAssSecMarketPrice = ComponentFactory.getTextField(I18N.message("ass.sec.market.price"), false, 80, 150);

		txtAssOriginalPrice = ComponentFactory.getTextField(I18N.message("ass.original.price"), false, 80, 150);
		txtAssOriginalPrice.addBlurListener(this);

		if(ProfileUtil.isAuctionController()) {
			txtAssSecMarketPrice.setEnabled(true);
			txtAssOriginalPrice.setEnabled(true);
		}else {
			txtAssSecMarketPrice.setEnabled(false);
			txtAssOriginalPrice.setEnabled(false);
		}

		txtOriAppraisalPrice = ComponentFactory.getTextField(I18N.message("ass.app.original.price"), false, 80, 150);
		if(ProfileUtil.isAdmin())
			txtOriAppraisalPrice.setEnabled(true);
		else
			txtOriAppraisalPrice.setEnabled(false);
		txtCommentCO = ComponentFactory.getTextArea(I18N.message("pos"), false, 500, 100);
		txtCommentApr = ComponentFactory.getTextArea(I18N.message("auction.controller"), false, 500, 100);

		mainLayout = new VerticalLayout();
		mainLayout.setSizeFull();
		mainLayout.setSpacing(true);
		reset();
		return mainLayout;
	}

	public void buildMainLayoutByAprraisalCatecory(List<AppraisalCategory> appraisalCategories) {
		// remove main panel then create it
		mainLayout.removeAllComponents();
		mapAppraisalCondition = new HashMap<AppraisalItem, Object>();
		mapAppraisalAdjValue = new HashMap<AppraisalItem, TextField>();
		mapAppraisalRemarkCO = new HashMap<Long, TextField>();
		mapAppraisalRemarkApr = new HashMap<Long, TextField>();
		appraisalItems = new ArrayList<AppraisalItem>();
		mapAppraisalCatecory = new HashMap<>();
		mainLayout.addComponent(getAppraisalSummaryPanel());
		mainLayout.addComponent(buildAppraisalItemLayout(appraisalCategories));
		mainLayout.addComponent(this.getCommentPanel());
	}
	public void assignValue(Quotation quotation) {
		if (quotation != null) {
			this.quotation = quotation;
			if (quotation.getAsset() != null) {
				Asset asset = quotation.getAsset();
				this.assetAppraisal = asset.getAssetAppraisal();
				txtAssOriginalPrice.setValue(MyNumberUtils.formatDoubleToString(MyNumberUtils.getDouble(asset.getTeAssetPrice())));
				txtAssSecMarketPrice.setValue(MyNumberUtils.formatDoubleToString(MyNumberUtils.getDouble(asset.getTiAssetSecondHandPrice())));
				AssetRange assetRange = asset.getAssetRange();
				if(assetRange != null && ASSET_WAVE_CODE.equals(assetRange.getCode())) {
					isWave = true;
				} else {
					isWave = false;
				}
				if (this.assetAppraisal != null && this.assetAppraisal.getId() != null ) {
					if(this.assetAppraisal.getAssetOrigPrice() != null && this.assetAppraisal.getAssetOrigPrice() != 0){
						txtAssOriginalPrice.setValue(MyNumberUtils.formatDoubleToString(MyNumberUtils.getDouble(this.assetAppraisal.getAssetOrigPrice())));
					}else{
						txtAssOriginalPrice.setValue(MyNumberUtils.formatDoubleToString(MyNumberUtils.getDouble(asset.getTeAssetPrice())));
					}
					if(this.assetAppraisal.getAssetSecondPrice() != null && this.assetAppraisal.getAssetSecondPrice() != 0){
						txtAssSecMarketPrice.setValue(MyNumberUtils.formatDoubleToString(MyNumberUtils.getDouble(this.assetAppraisal.getAssetSecondPrice())));
					}else{
						txtAssSecMarketPrice.setValue(MyNumberUtils.formatDoubleToString(MyNumberUtils.getDouble(asset.getTiAssetSecondHandPrice())));
					}
					if(this.assetAppraisal.getOrigAppraisalPrice() != null && this.assetAppraisal.getOrigAppraisalPrice() != 0){
						txtOriAppraisalPrice.setValue(MyNumberUtils.formatDoubleToString(MyNumberUtils.getDouble(assetAppraisal.getOrigAppraisalPrice())));
					}
					if (assetAppraisal.getCommentCo() != null) {
						txtCommentCO.setValue(assetAppraisal.getCommentCo());
					} else {
						txtCommentCO.setValue("");
					}
					if (assetAppraisal.getCommentApr() != null) {
						txtCommentApr.setValue(assetAppraisal.getCommentApr());
					} else {
						txtCommentApr.setValue("");
					}

					int ind = 0;
					if (this.assetAppraisal.getAppraisals() != null) {
						for (Appraisal appraisal : this.assetAppraisal.getAppraisals()) {
							AppraisalItem foundItem = null;
							for (AppraisalItem appraisalItem : appraisalItems) {
								if (appraisal.getItemId() != null && appraisalItem.getId().equals(appraisal.getItemId())) {
									foundItem = appraisalItem;
								}
							}
							if (foundItem != null) {
								// found appraisalItem from map
								if (mapAppraisalCondition.get(foundItem) != null) {
									if (appraisal.getItemId() != null) {
										Object value = mapAppraisalCondition.get(foundItem);
										if (value != null) {
											if (value instanceof TextField) {
												((TextField) value).setValue(getDefaultString(appraisal.getCondition()));
											} else if (value instanceof EntityRefComboBox && ((EntityRefComboBox) value).getData().equals(Province.class.getSimpleName())) {
												if(appraisal.getFreeFieldValue() != null){
													Province province = ENTITY_SRV.getById(Province.class, Long.valueOf(appraisal.getFreeFieldValue()));
													((EntityRefComboBox) value).setSelectedEntity(province);
												}
											} else if (value instanceof ERefDataComboBox) {
												if (((ERefDataComboBox) value).getData().equals(EColor.class.getSimpleName())) {
													if(appraisal.getFreeFieldValue() != null){
														EColor eColor = EColor.getById(EColor.class, Long.valueOf(appraisal.getFreeFieldValue()));
														((ERefDataComboBox) value).setSelectedEntity(eColor);
													}else{
														((ERefDataComboBox) value).setSelectedEntity(asset.getColor());
													}
												} else if (((ERefDataComboBox) value).getData().equals(EAssetCondition.class.getSimpleName())) {
													if(appraisal.getFreeFieldValue() != null){
														Long id = Long.valueOf(appraisal.getFreeFieldValue());
														EAssetCondition eAssetCondition = EAssetCondition.getById(id);
														((ERefDataComboBox) value).setSelectedEntity(eAssetCondition);
													}
												}
											}
										}
									}
								}
							}
							if (mapAppraisalRemarkApr.get(appraisal.getItemId()) != null) {
								if (appraisal.getRemarkAu() != null) {
									mapAppraisalRemarkApr.get(appraisal.getItemId()).setValue(getDefaultString(appraisal.getRemarkAu()));
								} else {
									mapAppraisalRemarkApr.get(appraisal.getItemId()).setValue("");
								}
							}
							if (mapAppraisalRemarkCO.get(appraisal.getItemId()) != null) {
								if (appraisal.getRemarkCO() != null) {
									mapAppraisalRemarkCO.get(appraisal.getItemId()).setValue(getDefaultString(appraisal.getRemarkCO()));
								} else {
									mapAppraisalRemarkCO.get(appraisal.getItemId()).setValue("");
								}
							}
							if (mapAppraisalAdjValue.get(foundItem) != null) {
								mapAppraisalAdjValue.get(foundItem).setValue(getDefaultString(appraisal.getAdjustmentValue()));
							}
							ind++;
						}
					}
				} else {
					this.assetAppraisal = AssetAppraisal.createInstance();
				}
			}
			displayControls();
		}
	}

	/**
	 * Reset Form
	 */
	public void reset() {
			super.reset();
			mapAppraisalCondition.forEach((key, value) -> {
				if (value instanceof TextField) {
					((TextField) value).setValue("");
				} else if (value instanceof EntityRefComboBox) {
					((EntityRefComboBox) value).setSelectedEntity(null);
				} else if (value instanceof ERefDataComboBox) {
					((ERefDataComboBox) value).setSelectedEntity(null);
				}
			});

			mapAppraisalRemarkCO.forEach((key, value) -> {
				value.setValue("");
			});
			mapAppraisalRemarkApr.forEach((key, value) -> {
				value.setValue("");
			});
			mapAppraisalAdjValue.forEach((key, value) -> {
				value.setValue("");
			});

			txtAssSecMarketPrice.setValue("");
			txtAssOriginalPrice.setValue("");
			txtOriAppraisalPrice.setValue("");
			txtCommentCO.setValue("");
			txtCommentApr.setValue("");

	}

    /* Validate Form*/
	public List<String> isValid() {
		super.reset();
		mapAppraisalCatecory.forEach((key, appraisalItems) -> {
			for (AppraisalItem item : appraisalItems) {
				if (item.getMandetory()) {
					Object value = mapAppraisalCondition.get(item);
					if (value instanceof EntityRefComboBox) {
						checkMandatorySelectField((EntityRefComboBox) value, item.getDesc());
					} else if (value instanceof ERefDataComboBox) {
						checkMandatorySelectField((ERefDataComboBox) value, item.getDesc());
					} else if (value instanceof TextField) {
						checkMandatoryField((TextField) value, item.getDesc());
					}
				}
			}
		});
		Double validAssprasialPrice = MyNumberUtils.getDouble(txtOriAppraisalPrice.getValue(), 0);

		if (validAssprasialPrice < 500) {
			errors.add("motorcycle.appraisal.price.must.more.than.500");
		}
		return errors;
	}

	@Override
	public void blur(FieldEvents.BlurEvent event) {
		if(event.getComponent() != null){
			this.calMotoApprPrice();
		}
	}

	@Override
	public void valueChange(Property.ValueChangeEvent event) {
        if(event.getProperty().getValue() != null && getClickTrigger()){
            this.calMotoApprPrice();
        }
	}

	/**
	 * builder dynamic layout follow appraisal item config from ra application
	 * @param appraisalCategories
	 * @return
	 */
	private VerticalLayout buildAppraisalItemLayout(List<AppraisalCategory> appraisalCategories) {
		Assert.notNull(appraisalCategories, "Asset category list could not be null, ...!");
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setSizeFull();
		verticalLayout.setSpacing(true);
		BaseRestrictions<AppraisalGroup> appraisalItemBaseRestrictions = new BaseRestrictions<>(AppraisalGroup.class);
		appraisalItemBaseRestrictions.addCriterion("statusRecord", FilterMode.EQUALS, new Object[] { EStatusRecord.ACTIV });
		appraisalGroupList = ENTITY_SRV.list(appraisalItemBaseRestrictions);


		for (AppraisalCategory appraisalCat : appraisalCategories) {
			Panel appraisalCategoryPanel = new Panel(appraisalCat.getDescEn());
			if(appraisalCat.getAppraisalRange().equals(EAppraisalRange.DREAM)) {
				isDream = true;
			}
			appraisalItems = ASSET_APPRAISAL_SERVICE.getAppraisalItemByCategory(appraisalCat);
			GridLayout appraisalItemGridLayout = new GridLayout(5, appraisalItems.size() + 1);
			appraisalItemGridLayout.setSpacing(true);
			appraisalItemGridLayout.setMargin(true);
			int ind = 0;
			for (AppraisalItem item : appraisalItems) {
				FormLayout formLayout1 = new FormLayout();
				FormLayout formLayout2 = new FormLayout();
				FormLayout formLayout3 = new FormLayout();
				TextField txtRemarkCO = ComponentFactory.getTextField(I18N.message("remark.co"), false, 60, 200);
				txtRemarkCO.setVisible(item.getVisible());
				formLayout1.addComponent(txtRemarkCO);
				TextField txtRemarkApr = ComponentFactory.getTextField(I18N.message("remark.apr"), false, 60, 200);
				txtRemarkApr.setVisible(item.getVisible());
				formLayout2.addComponent(txtRemarkApr);
				txtAdjValue = ComponentFactory.getTextField(I18N.message("adjusted.value"), false, 60, 100);
				txtAdjValue.setVisible(item.getVisible());
				txtAdjValue.addBlurListener(this);

				formLayout3.addComponent(txtAdjValue);

				if (ProfileUtil.isPOS()) {
					txtRemarkApr.setEnabled(false);
					txtAdjValue.setEnabled(false);
				}
				if (ProfileUtil.isAuctionController()) {
					txtRemarkCO.setEnabled(false);

				}
				if (ProfileUtil.isAdmin(ProfileUtil.getCurrentUser())){
					txtRemarkApr.setEnabled(true);
					txtAdjValue.setEnabled(true);
					txtRemarkCO.setEnabled(true);

				}

				item.getSortIndex();
				FormLayout formLayout4 = new FormLayout();
				if (EComponetType.COMBOBOX.equals(item.getComponetType())) {
					if (APPRAISAL_MOTO_COLOR.equals(item.getCode())) {
						cbxValue = new ERefDataComboBox<EColor>(EColor.values());
						cbxValue.setCaption(item.getDescEn());
						cbxValue.setRequired(item.getMandetory());
						cbxValue.setWidth("100");
						formLayout4.addComponent(cbxValue);
						cbxValue.addValueChangeListener(this);
						mapAppraisalCondition.put(item, cbxValue);
						cbxValue.setData(EColor.class.getSimpleName());
						cbxValue.setValue(null);
						cbxValue.setVisible(item.getVisible());
                        cbxValue.setImmediate(true);
                       if (ProfileUtil.isAuctionController()){
						   cbxValue.setEnabled(false);
					   }else if (ProfileUtil.isAdmin(ProfileUtil.getCurrentUser())){
						   cbxValue.setEnabled(true);
					   }

					} else if (APPRAISAL_PLAT_NUMBER.equals(item.getCode())) {
						cbxProValue = new EntityRefComboBox<Province>();
						cbxProValue.setRestrictions(new BaseRestrictions<>(Province.class));
						cbxProValue.setImmediate(true);
						cbxProValue.setRequired(item.getMandetory());
						cbxProValue.setWidth("100");
						cbxProValue.renderer();
						cbxProValue.setCaption(item.getDescEn());
						formLayout4.addComponent(cbxProValue);
						cbxProValue.addValueChangeListener(this);
						mapAppraisalCondition.put(item, cbxProValue);
						cbxProValue.setData(Province.class.getSimpleName());
						cbxProValue.setValue(true);
						cbxProValue.setVisible(item.getVisible());
						if (ProfileUtil.isAuctionController())
							cbxProValue.setEnabled(false);
					} else {
						cbxValue = new ERefDataComboBox<>(EAssetCondition.values());
						cbxValue.setRequired(item.getMandetory());
						cbxValue.setWidth("100");
						cbxValue.setCaption(item.getDescEn());
						formLayout4.addComponent(cbxValue);
						cbxValue.addValueChangeListener(this);
						mapAppraisalCondition.put(item, cbxValue);
						cbxValue.setData(EAssetCondition.class.getSimpleName());
						cbxValue.setValue(null);
						cbxValue.setVisible(item.getVisible());
						cbxValue.setImmediate(true);
						if (ProfileUtil.isAuctionController())
							cbxValue.setEnabled(false);
					}

				} else if (EComponetType.TEXTFILED.equals(item.getComponetType())) {
					txtValue = ComponentFactory.getTextField(item.getDescEn(), item.getMandetory(), 60, 200);
					txtValue.addBlurListener(this);
					txtValue.setWidth("100");
					formLayout4.addComponent(txtValue);
					mapAppraisalCondition.put(item, txtValue);
					txtValue.setValue("");
					txtValue.setVisible(item.getVisible());
					txtValue.setImmediate(true);
					if (ProfileUtil.isAuctionController())
						txtValue.setEnabled(false);
				}

				appraisalItemGridLayout.addComponent(formLayout4, 0, ind);
				appraisalItemGridLayout.addComponent(formLayout3, 1, ind);
				appraisalItemGridLayout.addComponent(formLayout1, 2, ind);
				appraisalItemGridLayout.addComponent(formLayout2, 3, ind);

				mapAppraisalAdjValue.put(item, txtAdjValue);
				mapAppraisalRemarkApr.put(item.getId(), txtRemarkApr);
				mapAppraisalRemarkCO.put(item.getId(), txtRemarkCO);
				ind++;
				appraisalCategoryPanel.setContent(appraisalItemGridLayout);
			}

			verticalLayout.addComponent(appraisalCategoryPanel);
			mapAppraisalCatecory.put(appraisalCat.getId(), appraisalItems);
		}
		return verticalLayout;
	}

	/**
	 * CommentPanel
	 */
	private Panel getCommentPanel() {
		FormLayout formLayout = getFormLayout(true);
		formLayout.addComponent(txtCommentCO);
		formLayout.addComponent(txtCommentApr);
		HorizontalLayout horLayout = new HorizontalLayout();
		horLayout.addComponent(formLayout);
		Panel panel = getPanel("comment", horLayout);
		return panel;
	}

	/**
	 * CommentPanel
	 */
	private Panel getAppraisalSummaryPanel() {
		FormLayout formLayout = getFormLayout(true);

		formLayout.addComponent(txtAssSecMarketPrice);
		formLayout.addComponent(txtAssOriginalPrice);
		formLayout.addComponent(txtOriAppraisalPrice);
		HorizontalLayout horLayout = new HorizontalLayout();
		horLayout.addComponent(formLayout);
		Panel panel = getPanel("appraisal.summary", horLayout);
		return panel;
	}

	private FormLayout getFormLayout(boolean margin) {
		FormLayout formLayout = new FormLayout();
		formLayout.setStyleName("myform-align-left");
		formLayout.setMargin(margin);
		return formLayout;
	}

	private Panel getPanel(String caption, AbstractOrderedLayout horLayout) {
		Panel mainPanel = new Panel(I18N.message(caption));
		mainPanel.setContent(horLayout);
		mainPanel.setSizeFull();
		return mainPanel;
	}


	private Double getDouble(String stringDouble, double defaultValue) {
		try {
			return Double.valueOf(Double.parseDouble(stringDouble));
		} catch (Exception var5) {
			return Double.valueOf(defaultValue);
		}
	}

	public Map<String, String> getHashError() {
		return hashError;
	}

	private void displayControls() {
		if (ProfileUtil.isPOS()) {
			txtCommentCO.setEnabled(true);
			txtCommentApr.setEnabled(false);
		} else if (ProfileUtil.isAuctionController()) {
			txtCommentCO.setEnabled(false);
			txtCommentApr.setEnabled(true);
		}
	}

	public AssetAppraisal getAssetAppraisal() {

		//Set data to asset and return bean
		List<Appraisal> appraisalList = null;
		if (mapAppraisalCatecory != null && !mapAppraisalCatecory.isEmpty()) {
			for (Map.Entry<Long, List<AppraisalItem>> entry : mapAppraisalCatecory.entrySet()) {
				appraisalList = new ArrayList<>();
				List<AppraisalItem> appraisalItems = entry.getValue();
				if (appraisalItems != null) {
					for (AppraisalItem item : appraisalItems) {
						if (item.getId() != null) {
							//Appraisal appraisal = Appraisal.createInstance();
							Appraisal appraisal = this.assetAppraisal.getAppraisalById(item.getId());
							if (appraisal == null) {
								appraisal = new Appraisal();
								appraisal.setCrudAction(CrudAction.CREATE);
								appraisalList.add(appraisal);
							} else {
								appraisal.setCrudAction(CrudAction.UPDATE);
							}
							if (mapAppraisalCondition.get(item) != null) {
								Object value = mapAppraisalCondition.get(item);

								if (value instanceof ERefDataComboBox) {
									if (((ERefDataComboBox) value).getSelectedEntity() != null) {
										appraisal.setCondition(MyNumberUtils.getInteger(((ERefDataComboBox) value).getSelectedEntity().getDesc(), 0));
										appraisal.setFreeFieldValue(getDefaultString(((ERefDataComboBox) value).getSelectedEntity().getId()));
									}
								} else if (value instanceof EntityRefComboBox) {
									Object selectedEntity = ((EntityRefComboBox) value).getSelectedEntity();
									if (selectedEntity instanceof EColor) {
										EColor selectedColor = (EColor) selectedEntity;
										if (selectedColor != null) {
											if (BLACK_COLOR_CODE.equals(selectedColor.getCode())) {
												appraisal.setCondition(1);
											} else {
												appraisal.setCondition(0);
											}
											appraisal.setFreeFieldValue(String.valueOf(selectedColor.getId()));
										}
									} else if (selectedEntity instanceof Province) {
										Province selectedProvince = (Province) selectedEntity;
										if (selectedProvince != null) {
											if (PHNOM_PENH_CODE.equals(selectedProvince.getCode())) {
												appraisal.setCondition(1);
											} else {
												appraisal.setCondition(0);
											}
											appraisal.setFreeFieldValue(String.valueOf(selectedProvince.getId()));
										}
									}
								} else if (value instanceof TextField) {
									appraisal.setCondition(MyNumberUtils.getInteger(((TextField) value).getValue(), 0));
								}
							}
							if (mapAppraisalAdjValue.get(item) != null) {
								appraisal.setAdjustmentValue(MyNumberUtils.getDouble(mapAppraisalAdjValue.get(item).getValue(), 0));
							}
							if (mapAppraisalRemarkApr.get(item.getId()) != null) {
								String remarkApr = mapAppraisalRemarkApr.get(item.getId()).getValue();
								appraisal.setRemarkAu(remarkApr);
							}
							if (mapAppraisalRemarkCO.get(item.getId()) != null) {
								String remarkCo = mapAppraisalRemarkCO.get(item.getId()).getValue ();
								appraisal.setRemarkCO(remarkCo);
							}

							appraisal.setItemId(item.getId());
							appraisal.setCoefficient(item.getCoefficient());
							if(!isDream) {
								if(item.getAppraisalGroup() != null ) {
									//appraisal.setCoefficient(ASSET_APPRAISAL_SERVICE.getAvgCoefficientByGroup(item));
									appraisal.setCoefficient(getAvgCoefficientByGroup(item));
								}
								if(isWave) {
									appraisal.setCoefficient(50d);
								}
							}

							appraisal.setWieght(item.getItemWieght(getDefaultString(appraisal.getCondition()),isDream));
							if (appraisal.getCondition() != null || appraisal.getCondition() != null) {
								//appraisal.setValue((appraisal.getCondition() + item.getValue()) * appraisal.getCoefficient());
								appraisal.setValue(appraisal.getWieght() * appraisal.getCoefficient());
							}

							appraisal.setAssetAppraisal(this.assetAppraisal);
							appraisalList.add(appraisal);
						}
					}
				}
			}
//			assetAppraisal.setEstimateAppraisalPrice(getDouble(txtEstimatApprisalPrice));
			this.assetAppraisal.setOrigAppraisalPrice(getDouble(txtOriAppraisalPrice));
			this.assetAppraisal.setAppraisals(appraisalList);
			if (ProfileUtil.isPOS()) {
				this.assetAppraisal.setCommentCo(txtCommentCO.getValue());
			} else if (ProfileUtil.isAuctionController()) {
				this.assetAppraisal.setCommentApr(txtCommentApr.getValue());
			}
			this.assetAppraisal.setAssetOrigPrice(getDouble(txtAssOriginalPrice, 0));
			this.assetAppraisal.setAssetSecondPrice(getDouble(txtAssSecMarketPrice, 0));
		}
		return this.assetAppraisal;
	}


	public void calMotoApprPrice() {
		Double motorAppraisalPrice = 0.d;
		Double totalAdjVal = 0.d;
		for (Map.Entry<AppraisalItem, Object> entry : mapAppraisalCondition.entrySet()) {
			AppraisalItem appraisalItem = entry.getKey();
			Object appraisalValue =  entry.getValue();
			Double calPrice = 0d;

			if (appraisalValue instanceof TextField) {

				if(APPRAISAL_ORIG_NEW_SELLING_PRICE.equals(appraisalItem.getCode())) {
					calPrice = calAppraisalItemMotoPrice(appraisalItem,txtAssOriginalPrice.getValue());
				}else if (APPRAISAL_CONSTANST.equals(appraisalItem.getCode()))
					calPrice = calAppraisalItemMotoPrice(appraisalItem,null);
				else if (!((TextField) appraisalValue).getValue().equals("0") && !((TextField) appraisalValue).getValue().isEmpty()){
					TextField txtCondition = (TextField) appraisalValue;
					if (txtCondition != null && !StringUtils.isEmpty(txtCondition.getValue()))
						calPrice = calAppraisalItemMotoPrice(appraisalItem,txtCondition.getValue());
				}
			}else if(appraisalValue instanceof EntityRefComboBox && ((EntityRefComboBox) entry.getValue()).getSelectedEntity() != null) {

				Object selectCondition = ((EntityRefComboBox) entry.getValue()).getSelectedEntity();
					if(selectCondition != null && selectCondition instanceof Province){
						Province selectedProvince = (Province) selectCondition;
						if (selectedProvince != null) {
							if (PHNOM_PENH_CODE.equals(selectedProvince.getCode()))
								calPrice = calAppraisalItemMotoPrice(appraisalItem,"1");
							else
								calPrice = calAppraisalItemMotoPrice(appraisalItem,"0");
						}
					}
			} else if (appraisalValue instanceof ERefDataComboBox && ((ERefDataComboBox) appraisalValue).getSelectedEntity() != null) {

				Object selectCondition = ((ERefDataComboBox) entry.getValue()).getSelectedEntity();
				if (selectCondition != null && selectCondition instanceof EColor) {
					EColor selectedColor = (EColor) selectCondition;
					if (BLACK_COLOR_CODE.equals(selectedColor.getCode()))
						calPrice = calAppraisalItemColorMotoPrice(appraisalItem,"1");
					else
						calPrice = calAppraisalItemColorMotoPrice(appraisalItem,"0");
				} else if (selectCondition != null && selectCondition instanceof EAssetCondition) {
					calPrice = calAppraisalItemMotoPrice(appraisalItem,((EAssetCondition) selectCondition).getDesc());
				}
			}
			motorAppraisalPrice = motorAppraisalPrice + calPrice;
		}

		for (Map.Entry<AppraisalItem, TextField> entry : mapAppraisalAdjValue.entrySet()) {
			if (entry.getValue() != null) {
				String adjValue = entry.getValue().getValue();
				totalAdjVal = totalAdjVal + getDouble(adjValue, 0);
			}
        }
		motorAppraisalPrice =  motorAppraisalPrice  + totalAdjVal;

		txtOriAppraisalPrice.setValue(MyNumberUtils.formatDoubleToString((double) Math.round(motorAppraisalPrice)));
	}

    public void setAssetAppraisal(AssetAppraisal assetAppraisal) {
        this.assetAppraisal = assetAppraisal;
    }

    private Double calAppraisalItemMotoPrice(AppraisalItem appraisalItem,String inputVal) {
		Double result;
		try {
			if(isDream) {
				result = appraisalItem.getItemWieght(inputVal,isDream) * appraisalItem.getCoefficient();
			} else {
				if(appraisalItem.getAppraisalGroup() != null) {
					result = appraisalItem.getItemWieght(inputVal,isDream) * getAvgCoefficientByGroup(appraisalItem);
				} else {
					result = appraisalItem.getItemWieght(inputVal,isDream) * appraisalItem.getCoefficient();
				}
			}
		}catch (Exception e) {
			result = 0d;
		}
		return result;
	}

	private Double calAppraisalItemColorMotoPrice(AppraisalItem appraisalItem,String inputVal) {
		Double result = 0d;
		try {
			if(isDream) {
				result = appraisalItem.getItemWieght(inputVal,isDream) * appraisalItem.getCoefficient();
			} else {
				result = appraisalItem.getItemWieght(inputVal,isDream) * appraisalItem.getCoefficient();
				if(isWave) {
					if(appraisalItem.getAppraisalGroup() != null) {
						result = appraisalItem.getItemWieght(inputVal,isDream) * getAvgCoefficientByGroup(appraisalItem);
					} else {
						result = appraisalItem.getItemWieght(inputVal,isDream) * 50d;
					}
				}

			}
		}catch (Exception e) {
			result = 0d;
		}
		return result;
	}

	public Double getAvgCoefficientByGroup(AppraisalItem appraisalItem) {
		AppraisalGroup appraisalGroup = appraisalItem.getAppraisalGroup();
		List<AppraisalItem> appraisalItemList = appraisalGroup.getAppraisalItems();
		Double result = 0D;
		if(appraisalItemList != null && appraisalItemList.size() > 0) {
			if(appraisalItem.getWeightCoefficient() != null && appraisalItem.getWeightCoefficient().doubleValue() < 1d) {
				result = appraisalGroup.getCoefficient().doubleValue();
			} else {
				result = appraisalGroup.getCoefficient().doubleValue() / appraisalItemList.size();
			}
		}
		return result;
	}
	public void setClickTrigger(Boolean b){
	   this.checkEvent = b;
    }
    public Boolean getClickTrigger(){
	    return this.checkEvent;
    }
}