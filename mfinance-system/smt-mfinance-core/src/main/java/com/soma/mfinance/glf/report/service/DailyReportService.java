package com.soma.mfinance.glf.report.service;

import java.util.Date;

import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.glf.report.model.DailyReport;


/**
 * @author kimsuor.seang
 */
public interface DailyReportService extends BaseEntityService {
	
	/**
	 * @param report
	 * @param reportParameter
	 */
	DailyReport calculateDailyReportByDealer(Dealer dealer, Date selectDate);
	
	/**
	 * @param report
	 * @param reportParameter
	 * @param closed
	 */
	DailyReport calculateDailyReportByDealer(Dealer dealer, Date selectDate, boolean closed);
	
	/**
	 * @param from
	 * @param to
	 */
	void recalculateDailyReport(Date from, Date to);
	
	/**
	 * @param selectDate
	 */
	void recalculateDailyReportOnSelectDate(Date selectDate);
}
