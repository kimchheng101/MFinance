package com.soma.mfinance.core.financial.model;

import org.seuksa.frmk.model.entity.MEntityRefA;

/**
 * Meta data of com.soma.mfinance.core.financial.model.CampaignAssetModel
 * @author kimsuor.seang
 */
public interface MCampaignAssetModel extends MEntityRefA {
	
	public final static String CAMPAIGN = "campaign";
	public final static String ASSETMODEL = "assetModel";
	public final static String DEFAULTFINANCEAMOUNT = "defaultFinanceAmount";
	public final static String STANDARDFINANCEAMOUNT = "standardFinanceAmount";

}
