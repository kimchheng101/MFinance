package com.soma.mfinance.core.auction.panel.result;

import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.auction.model.EStorageLocation;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.financial.model.FinProduct;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.workflow.AuctionWkfStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;

/**
 * Auction Result search panel
 *
 * @author kimsuor.seang
 */
public class AuctionResultSearchPanel extends AbstractSearchPanel<Contract> {

    private static final long serialVersionUID = -233337691727200970L;

    private TextField txtReference;
    private ERefDataComboBox<EStorageLocation> cbxEStorageLocation;
    private TextField txtEngineNumber;
    private TextField txtChassisNumber;
    private TextField txtPlateNumber;
    private TextField txtLastNameEn;
    private TextField txtFirstNameEn;
    private HorizontalLayout horizontalLayout;
    private FormLayout formLayout;
    private EntityRefComboBox<FinProduct> cbxFinancialProduct;

    public AuctionResultSearchPanel(AuctionResultTablePanel tablePanel) {
        super(I18N.message("search"), tablePanel);
    }

    @Override
    protected Component createForm() {

        cbxFinancialProduct = new EntityRefComboBox<FinProduct>(I18N.message("financial.product"));
        BaseRestrictions<FinProduct> finfProduct = new BaseRestrictions<>(FinProduct.class);
        finfProduct.getStatusRecordList().add(EStatusRecord.ACTIV);

        cbxFinancialProduct.setRestrictions(finfProduct);
        cbxFinancialProduct.setImmediate(true);
        cbxFinancialProduct.renderer();
        cbxFinancialProduct.setSelectedEntity(null);

        txtReference = ComponentFactory.getTextField("reference", false, 50, 200);
        cbxEStorageLocation = new ERefDataComboBox<>(I18N.message("storage.location"), EStorageLocation.class);

        txtEngineNumber = ComponentFactory.getTextField("engine.number", false, 50, 200);
        txtChassisNumber = ComponentFactory.getTextField("chassis.number", false, 50, 200);
        txtPlateNumber = ComponentFactory.getTextField("plate.number", false, 50, 200);
        txtLastNameEn = ComponentFactory.getTextField("lastname.en", false, 50, 200);
        txtFirstNameEn = ComponentFactory.getTextField("firstname.en", false, 50, 200);

        horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);

        formLayout = new FormLayout();//1
        formLayout.addComponent(txtReference);
        formLayout.addComponent(txtEngineNumber);
        horizontalLayout.addComponent(formLayout);

        formLayout = new FormLayout();//2
        formLayout.addComponent(cbxEStorageLocation);
        formLayout.addComponent(txtChassisNumber);
        horizontalLayout.addComponent(formLayout);

        formLayout = new FormLayout();
        formLayout.addComponent(txtPlateNumber);
        formLayout.addComponent(cbxFinancialProduct);
        horizontalLayout.addComponent(formLayout);

        if (ProfileUtil.isCollectionSupervisor() || ProfileUtil.isAuctionController()) {
            formLayout = new FormLayout();
            formLayout.addComponent(txtLastNameEn);
            formLayout.addComponent(txtFirstNameEn);
            horizontalLayout.addComponent(formLayout);
        }

        return horizontalLayout;
    }

    @Override
    public BaseRestrictions<Contract> getRestrictions() {
        BaseRestrictions<Contract> restrictions = new BaseRestrictions<Contract>(Contract.class);
        restrictions.addCriterion(Restrictions.eq("requestRepossess", Boolean.TRUE));
        restrictions.addCriterion(Restrictions.eq("auctionStatus", AuctionWkfStatus.WRE));

        restrictions.addAssociation("quotations", "quoa", JoinType.INNER_JOIN);
        restrictions.addAssociation("quoa.asset", "qouasset", JoinType.INNER_JOIN);

        restrictions.addAssociation("quoa.quotationApplicants", "quoapp", JoinType.INNER_JOIN);
        restrictions.addAssociation("quoapp.applicant", "app", JoinType.INNER_JOIN);
        restrictions.addCriterion("quoapp.applicantType", EApplicantType.C);

        if (cbxFinancialProduct.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq("financialProduct", cbxFinancialProduct.getSelectedEntity()));
        }
        if (cbxEStorageLocation.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq("storageLocation", cbxEStorageLocation.getSelectedEntity()));
        }

        if (StringUtils.isNotEmpty(txtReference.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("reference", txtReference.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtEngineNumber.getValue())) {

            restrictions.addCriterion(Restrictions.ilike("qouasset.engineNumber", txtEngineNumber.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtChassisNumber.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("qouasset.chassisNumber", txtChassisNumber.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtPlateNumber.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("qouasset.plateNumber", txtPlateNumber.getValue(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotEmpty(txtLastNameEn.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("app.lastNameEn", txtLastNameEn.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("app.firstNameEn", txtFirstNameEn.getValue(), MatchMode.ANYWHERE));
        }
        return restrictions;
    }

    @Override
    protected void reset() {
        txtReference.setValue("");
        cbxEStorageLocation.setSelectedEntity(null);
        txtEngineNumber.setValue("");
        txtChassisNumber.setValue("");
        txtPlateNumber.setValue("");
        txtFirstNameEn.setValue("");
        txtLastNameEn.setValue("");
        cbxFinancialProduct.setSelectedEntity(null);

    }
}
