package com.soma.mfinance.core.auction.panel.stock;

import com.soma.mfinance.core.auction.model.EStorageLocation;
import com.soma.mfinance.core.collection.model.EStockStatus;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.soma.mfinance.core.collection.model.EStockStatus;
import com.vaadin.ui.FormLayout;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Asset Stock Form panel
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AssetStockFormPanel extends AbstractFormPanel {


	private static final long serialVersionUID = 1544186002086985748L;
	private ERefDataComboBox<EStorageLocation> cbxEStorageLocation;
	private ERefDataComboBox<EStockStatus> cbxEStockStatus;
	private Contract contract;

	@PostConstruct
	public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
		navigationPanel.addSaveClickListener(this);
	}

	/**
	 * @see AbstractFormPanel#createForm()
	 */
	@Override
	protected com.vaadin.ui.Component createForm() {
		cbxEStorageLocation = new ERefDataComboBox<>(I18N.message("storage.location"), EStorageLocation.class);
		cbxEStockStatus = new ERefDataComboBox<EStockStatus>(I18N.message("stock.status"), EStockStatus.class);

		FormLayout formLayout = new FormLayout();
		formLayout.setSpacing(true);
		formLayout.addComponent(cbxEStorageLocation);
		formLayout.addComponent(cbxEStockStatus);

		return formLayout;
	}

	/**
	 * @see AbstractFormPanel#getEntity()
	 */
	@Override
	protected Entity getEntity() {
		this.contract.setStorageLocation(cbxEStorageLocation.getSelectedEntity());
		this.contract.setStockStatus(cbxEStockStatus.getSelectedEntity());
		return this.contract;
	}
	
	/**
	 * Assign value to form
	 * @param id
	 */
	public void assignValues (Long id) {
		this.contract = ENTITY_SRV.getById(Contract.class, id);
		cbxEStorageLocation.setSelectedEntity(contract.getStorageLocation());
		cbxEStockStatus.setSelectedEntity(contract.getStockStatus());
	}

}
