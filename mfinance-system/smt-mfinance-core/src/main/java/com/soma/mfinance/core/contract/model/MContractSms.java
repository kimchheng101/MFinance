package com.soma.mfinance.core.contract.model;

import org.seuksa.frmk.model.entity.MEntityA;


/**
 * Meta data of com.soma.mfinance.core.contract_old.model.ContractSms
 * @author kimsuor.seang
 */
public interface MContractSms extends MEntityA {
	
	// For Vaadin Grid
	public final static String STATUS = "status";
	public final static String ACTION = "action";
	
	public final static String CONTRACT = "contract";
	public final static String SENDTO = "sendTo";
	public final static String PHONENUMBER = "phoneNumber";
	public final static String USER = "user";
	public final static String MESSAGE = "message";

}
