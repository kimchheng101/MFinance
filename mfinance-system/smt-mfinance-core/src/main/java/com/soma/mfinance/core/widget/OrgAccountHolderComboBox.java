package com.soma.mfinance.core.widget;

import java.util.List;

import com.gl.finwiz.share.domain.AP.AccountHolderDTO;
import com.soma.mfinance.third.finwiz.client.ap.ClientAccountHolder;
import com.soma.ersys.core.hr.model.organization.OrgAccountHolder;

/**
 * 
 * @author kimsuor.seang
 *
 */
public class OrgAccountHolderComboBox extends EntityAComboBox<OrgAccountHolder> {

	/** */
	private static final long serialVersionUID = -4257081344801007835L;

	/**
	 * 
	 * @param orgAccountHolders
	 */
	public OrgAccountHolderComboBox(List<OrgAccountHolder> orgAccountHolders) {
		super(orgAccountHolders);
	}
	
	/**
	 * 
	 * @param caption
	 * @param orgAccountHolders
	 */
	public OrgAccountHolderComboBox(String caption, List<OrgAccountHolder> orgAccountHolders) {
		super(caption, orgAccountHolders);
	}

	/**
	 * @see com.soma.mfinance.core.widget.EntityAComboBox#getEntityACaption(org.seuksa.frmk.model.entity.EntityA)
	 */
	@Override
	protected String getEntityACaption(OrgAccountHolder orgAccountHolder) {
		AccountHolderDTO accountHolderDTO = ClientAccountHolder.getAccountHolderById(orgAccountHolder.getAccountHolder());
		return accountHolderDTO == null ? null : accountHolderDTO.getName();
	}

}
