package com.soma.mfinance.core.asset.model;

/**
 * Meta data of com.soma.mfinance.core.masset.model.AssetRange
 * @author kimsuor.seang
 */
public interface MAssetRange {

	public final static String ASSETMAKE = "assetMake";
	public final static String DESCLOCAL = "descLocale";
	
}
