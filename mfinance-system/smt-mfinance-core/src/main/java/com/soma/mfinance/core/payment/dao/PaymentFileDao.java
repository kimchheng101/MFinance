package com.soma.mfinance.core.payment.dao;

import org.seuksa.frmk.dao.BaseEntityDao;

/**
 * Payment Dao
 * @author kimsuor.seang
 */
public interface PaymentFileDao extends BaseEntityDao {

}
