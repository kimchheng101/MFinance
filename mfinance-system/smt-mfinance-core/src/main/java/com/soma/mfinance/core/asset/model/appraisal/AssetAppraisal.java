package com.soma.mfinance.core.asset.model.appraisal;

import org.seuksa.frmk.model.EntityFactory;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cheasocheat on 3/2/17.
 */

@Entity
@Table(name = "td_asset_appraisal")
public class AssetAppraisal extends AbstractAssetAppraisal {

    private String commentCo;
    private String commentApr;
    private List<Appraisal> appraisals;
    private Double assetOrigPrice;
    private Double assetSecondPrice;
    private Double origAppraisalPrice;
    private Double estimateAppraisalPrice;

    public static AssetAppraisal createInstance() {
        AssetAppraisal instance = EntityFactory.createInstance(AssetAppraisal.class);
        return instance;
    }

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ass_app_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name = "ass_app_comment_co", nullable = true, length = 1000)
    public String getCommentCo() {
        return commentCo;
    }

    public void setCommentCo(String commentCo) {
        this.commentCo = commentCo;
    }

    @Column(name = "ass_app_comment_apr", nullable = true, length = 1000)
    public String getCommentApr() {
        return commentApr;
    }

    public void setCommentApr(String commentApr) {
        this.commentApr = commentApr;
    }

    @OneToMany(mappedBy = "assetAppraisal", fetch = FetchType.LAZY)
    public List<Appraisal> getAppraisals() {
        return appraisals;
    }

    public void setAppraisals(List<Appraisal> appraisals) {
        this.appraisals = appraisals;
    }

    @Column(name = "ass_apr_orig_price", nullable = true)
    public Double getAssetOrigPrice() {
        return assetOrigPrice;
    }

    public void setAssetOrigPrice(Double assetOrigPrice) {
        this.assetOrigPrice = assetOrigPrice;
    }

    @Column(name = "ass_apr_second_price", nullable = true)
    public Double getAssetSecondPrice() {
        return assetSecondPrice;
    }

    public void setAssetSecondPrice(Double assetSecondPrice) {
        this.assetSecondPrice = assetSecondPrice;
    }

    public void setOrigAppraisalPrice(Double origAppraisalPrice) {
        this.origAppraisalPrice = origAppraisalPrice;
    }

    public void setEstimateAppraisalPrice(Double estimateAppraisalPrice) {
        this.estimateAppraisalPrice = estimateAppraisalPrice;
    }

    @Column(name = "ass_apr_price", nullable = true)
    public Double getOrigAppraisalPrice() {
        return origAppraisalPrice;
    }

    @Column(name = "ass_apr_estimate_price", nullable = true)
    public Double getEstimateAppraisalPrice() {
        return estimateAppraisalPrice;
    }

    @Transient
    public void addAppraisal(Appraisal appraisal) {
        if (this.appraisals == null) {
            appraisals = new ArrayList<>();
        }
        appraisals.add(appraisal);
    }

    @Transient
    public Appraisal getAppraisalById(Long appId) {
        if (appraisals != null && !appraisals.isEmpty()) {
            for (Appraisal appraisal : appraisals) {
                if (appraisal.getItemId() != null && appraisal.getItemId().equals(appId)) {
                    return appraisal;
                }
            }
        }
        return null;
    }

}
