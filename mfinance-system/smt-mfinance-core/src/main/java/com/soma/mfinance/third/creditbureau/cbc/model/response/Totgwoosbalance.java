/**
 * 
 */
package com.soma.mfinance.third.creditbureau.cbc.model.response;

import java.io.Serializable;

/**
 * @author tha.bunsath
 *
 */
public class Totgwoosbalance implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String tot_gwo_os_curr;
	private Double tot_gwo_os_bal;
	public String getTot_gwo_os_curr() {
		return tot_gwo_os_curr;
	}
	public void setTot_gwo_os_curr(String tot_gwo_os_curr) {
		this.tot_gwo_os_curr = tot_gwo_os_curr;
	}
	public Double getTot_gwo_os_bal() {
		return tot_gwo_os_bal;
	}
	public void setTot_gwo_os_bal(Double tot_gwo_os_bal) {
		this.tot_gwo_os_bal = tot_gwo_os_bal;
	}

}
