package com.soma.mfinance.core.applicant.panel;

import com.soma.common.app.eref.EProductLineCode;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.EApplicantCategory;
import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.mfinance.core.applicant.panel.employee.EmployeePanelMFP;
import com.soma.mfinance.core.applicant.panel.employee.EmploymentPanelKFP;
import com.soma.mfinance.core.applicant.panel.identity.IdentityCoAppPanel;
import com.soma.mfinance.core.applicant.panel.identity.IdentityPanelsKFP;
import com.soma.mfinance.core.applicant.panel.identity.IdentityPanelsMFP;
import com.soma.mfinance.core.applicant.panel.otherInformation.OtherInfoPanel;
import com.soma.mfinance.core.loanaccount.LoanTabPanel;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.quotation.QuotationProfileUtils;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.tabsheet.TabSheet;
import com.soma.mfinance.core.applicant.panel.employee.EmployeePanelMFP;
import com.soma.mfinance.core.applicant.panel.employee.EmploymentPanelKFP;
import com.soma.mfinance.core.applicant.panel.identity.IdentityCoAppPanel;
import com.soma.mfinance.core.applicant.panel.identity.IdentityPanelsKFP;
import com.soma.mfinance.core.applicant.panel.identity.IdentityPanelsMFP;
import com.soma.mfinance.core.applicant.panel.otherInformation.OtherInfoPanel;
import com.vaadin.ui.VerticalLayout;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.soma.mfinance.core.helper.FinServicesHelper.PRODUCT_LINE_SERVICE;

/**
 * Created by cheasocheat on 2/2/17.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ApplicantDetailPanel extends AbstractTabPanel implements com.vaadin.ui.TabSheet.SelectedTabChangeListener {
    private TabSheet applicantTabSheet;

    private IdentityPanelsMFP identityPanelsMFP;
    private OtherInfoPanel otherInfoPanel;
    private EmployeePanelMFP employeePanelMFP;

    private IdentityPanelsKFP identityPanelsKFP;
    private EmploymentPanelKFP employmentPanelKFP;
    private EquipmentRentalPanel equipmentRentalPanel;
    private LiabilityPanel liabilityPanel;
    private FamilyAndExpensePanel familyAndExpensePanel;
    private IdentityCoAppPanel identityCoAppPanel;

    private Individual individual;
    private LoanTabPanel loanTabPanel;
    private Quotation quotation;

    public ApplicantDetailPanel() {
        super();
        setCaption(I18N.message("applicant"));
    }

    @Override
    protected com.vaadin.ui.Component createForm() {
        identityPanelsMFP = new IdentityPanelsMFP();
        employeePanelMFP = new EmployeePanelMFP();

        identityPanelsKFP = new IdentityPanelsKFP();
        employmentPanelKFP = new EmploymentPanelKFP();
        identityCoAppPanel = new IdentityCoAppPanel();
        equipmentRentalPanel = new EquipmentRentalPanel();
        liabilityPanel = new LiabilityPanel();
        familyAndExpensePanel = new FamilyAndExpensePanel();
        otherInfoPanel = new OtherInfoPanel();
        loanTabPanel = new LoanTabPanel();
        applicantTabSheet = new TabSheet();
        applicantTabSheet.addSelectedTabChangeListener(this);
        displayTabs();
        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setSpacing(true);
        contentLayout.addComponent(applicantTabSheet);

        return contentLayout;
    }

    public void assignValues(Quotation quotation) {
        this.removeAllComponents();
        this.quotation = quotation;
        super.init();

        if (quotation != null && quotation.getApplicant() != null) {
            individual = quotation.getApplicant().getIndividual();
            if(PRODUCT_LINE_SERVICE.getEProductLineCodeByQuotation(quotation) != null){
                if(PRODUCT_LINE_SERVICE.getEProductLineCodeByQuotation(quotation).getId() != null){
                    if (PRODUCT_LINE_SERVICE.getEProductLineCodeByQuotation(quotation).getId().equals(EProductLineCode.MFP.getId())) {
                        identityPanelsMFP.assignValues(quotation);
                        employeePanelMFP.setQuotation(quotation);
                        employeePanelMFP.assignValues(individual);
                        otherInfoPanel.assignValues(quotation.getApplicant());
                    } else if (PRODUCT_LINE_SERVICE.getEProductLineCodeByQuotation(quotation).getId().equals(EProductLineCode.KFP.getId())) {
                        identityPanelsKFP.assignValues(quotation);
                        employmentPanelKFP.assignValues(individual);
                        //identityCoAppPanel.assignValues(quotation);
                    }
                }
            }
            if (QuotationProfileUtils.isLoanTabAvailable(quotation)) {
                identityPanelsKFP.assignValues(quotation);
                identityPanelsMFP.assignValues(quotation);
                employeePanelMFP.setQuotation(quotation);
                employeePanelMFP.assignValues(individual);
                equipmentRentalPanel.assignValues(individual);
                liabilityPanel.assignValues(individual);
                familyAndExpensePanel.assignValues(individual);
                otherInfoPanel.assignValues(quotation.getApplicant());
                if (QuotationProfileUtils.isLoanTabAvailable(quotation)) {
                    applicantTabSheet.addTab(loanTabPanel, I18N.message("loan.account"));
                    loanTabPanel.assignLoanAccountValue(quotation.getApplicant());
                } else {
                    applicantTabSheet.removeFormPanel(loanTabPanel);
                }
                loanTabPanel.assignLoanAccountValue(quotation.getApplicant());
            }

            // Set Enable ApplicantDetail
            boolean isEnabled = QuotationProfileUtils.isEnabled(quotation);
            identityPanelsKFP.setEnabled(isEnabled);
            otherInfoPanel.setEnabled(isEnabled);
            identityPanelsKFP.setEnableIdentityPanel(isEnabled);
            identityPanelsMFP.setEnableIdentityPanel(isEnabled);
            employeePanelMFP.setEnableEmployee(isEnabled);
            loanTabPanel.setEnabled(quotation.getWkfStatus().equals(QuotationWkfStatus.APV) || quotation.getWkfStatus().equals(QuotationWkfStatus.AWT) || ProfileUtil.isAdmin());
        }
    }
    private void displayTabs() {
        if (quotation != null) {
            if (PRODUCT_LINE_SERVICE.getEProductLineCodeByQuotation(quotation).getId() != null) {
                if (PRODUCT_LINE_SERVICE.getEProductLineCodeByQuotation(quotation).getId().equals(EProductLineCode.MFP.getId())) {
                    applicantTabSheet.addTab(identityPanelsMFP, I18N.message("identity"));
                    applicantTabSheet.addTab(employeePanelMFP, I18N.message("employment"));
                    applicantTabSheet.addTab(otherInfoPanel, I18N.message("other.info"));
                } else if (PRODUCT_LINE_SERVICE.getEProductLineCodeByQuotation(quotation).getId().equals(EProductLineCode.KFP.getId())) {
                    applicantTabSheet.addTab(identityPanelsKFP, I18N.message("identity"));
                    applicantTabSheet.addTab(employmentPanelKFP, I18N.message("employment"));
                    applicantTabSheet.addTab(equipmentRentalPanel, I18N.message("equipment.rental"));
                    applicantTabSheet.addTab(liabilityPanel, I18N.message("liability"));
                    applicantTabSheet.addTab(familyAndExpensePanel, I18N.message("family.expense"));
                }
            }
        }
    }

    public List<String> isValid() {
        List<String> errr = new ArrayList<>();
        if (quotation != null) {
            if (PRODUCT_LINE_SERVICE.getEProductLineCodeByQuotation(quotation) != null) {
                if (PRODUCT_LINE_SERVICE.getEProductLineCodeByQuotation(quotation).getId().equals(EProductLineCode.MFP.getId())) {
                    if (applicantTabSheet.getSelectedTab() == identityPanelsKFP) {
                        errr = identityPanelsKFP.isValid();
                    } else if (applicantTabSheet.getSelectedTab() == otherInfoPanel) {
                        errr = otherInfoPanel.isValid();
                    } else {
                        //errr = loanTabPanel.isValid();
                    }
                } else if (PRODUCT_LINE_SERVICE.getEProductLineCodeByQuotation(quotation).getId().equals(EProductLineCode.KFP.getId())) {
                    if (applicantTabSheet.getSelectedTab() == identityPanelsKFP) {
                        errr = identityPanelsKFP.isValid();
                    } else if (applicantTabSheet.getSelectedTab() == familyAndExpensePanel){
                        errr = familyAndExpensePanel.fullValidate();
                    } else if (applicantTabSheet.getSelectedTab() == liabilityPanel){
                        errr = liabilityPanel.fullValidate();
                    } else if (applicantTabSheet.getSelectedTab() == equipmentRentalPanel){
                        errr = equipmentRentalPanel.fullValidate();
                    }
                }
            }
        }
        return errr;
    }

    public Applicant getApplicant(Applicant applicant) {
        individual = applicant.getIndividual() == null ? new Individual() : applicant.getIndividual();
        if (quotation != null) {
            if (PRODUCT_LINE_SERVICE.getEProductLineCodeByQuotation(quotation) != null) {
                if (PRODUCT_LINE_SERVICE.getEProductLineCodeByQuotation(quotation).getId().equals(EProductLineCode.MFP.getId())) {
                    employeePanelMFP.getEmployments(individual);
                    otherInfoPanel.getOtherInfo(individual);
                    loanTabPanel.getLoanAccount(applicant);
                } else if (PRODUCT_LINE_SERVICE.getEProductLineCodeByQuotation(quotation).getId().equals(EProductLineCode.KFP.getId())) {
                    employmentPanelKFP.getEmployments(individual);
                    identityPanelsKFP.getIdentityPanel(individual);
                    //identityCoAppPanel.getIdentityPanel(individualCoApp);
                }
            }
        }

        employeePanelMFP.getEmployments(individual);
        otherInfoPanel.getOtherInfo(individual);
        identityPanelsKFP.getIdentityPanel(individual);
        identityPanelsMFP.getIdentityPanel(individual);
        equipmentRentalPanel.getEquipmentRentalPanel(individual);
        loanTabPanel.getLoanAccount(applicant);
        liabilityPanel.getLiabilityPanel(individual);
        familyAndExpensePanel.getFamilyAndExpensePanel(individual);

        applicant.setIndividual(individual);
        applicant.setApplicantCategory(EApplicantCategory.INDIVIDUAL);
        return applicant;
    }

    public void reset() {
        identityPanelsKFP.reset();
        employeePanelMFP.reset();
        otherInfoPanel.reset();
        loanTabPanel.reset();
    }

    public IdentityPanelsKFP getIdentityPanels() {
        return identityPanelsKFP;
    }

    public IdentityPanelsMFP getIdentityPanelsMFP(){
        return identityPanelsMFP;
    }

    public EmployeePanelMFP getEmployeePanels() {
        return employeePanelMFP;
    }

    public OtherInfoPanel getOtherInfoPanel() {
        return otherInfoPanel;
    }

    public LoanTabPanel getLoanTabPanel() {
        return loanTabPanel;
    }

    @Override
    public void selectedTabChange(com.vaadin.ui.TabSheet.SelectedTabChangeEvent selectedTabChangeEvent) {
        if (quotation != null) {
            if (PRODUCT_LINE_SERVICE.getEProductLineCodeByQuotation(quotation) != null) {
                if (PRODUCT_LINE_SERVICE.getEProductLineCodeByQuotation(quotation).getId().equals(EProductLineCode.MFP.getId())) {
                    employeePanelMFP.getCurrentEmploymentPanel().setAddress(identityPanelsMFP.getAddress());
                } else if (PRODUCT_LINE_SERVICE.getEProductLineCodeByQuotation(quotation).getId().equals(EProductLineCode.KFP.getId())) {
                    employeePanelMFP.getCurrentEmploymentPanel().setAddress(identityPanelsKFP.getAddress());
                }
            }
        }
    }
}
