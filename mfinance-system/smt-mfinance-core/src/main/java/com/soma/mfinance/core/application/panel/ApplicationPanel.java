package com.soma.mfinance.core.application.panel;

import com.soma.mfinance.core.applicant.panel.IdentificationPanel;
import com.soma.mfinance.core.asset.panel.AssetPanel;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.report.ReportUtils;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.soma.mfinance.core.applicant.panel.IdentificationPanel;
import com.soma.mfinance.core.asset.panel.AssetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import org.apache.commons.lang.StringUtils;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.service.EntityService;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;


@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(ApplicationPanel.NAME)
public class ApplicationPanel extends AbstractTabsheetPanel implements View, FinServicesHelper {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public static final String NAME = "applications";
    @Autowired
    private ApplicationFormPanel applicationFormPanel;
    @Autowired
    private ApplicationTablePanel applicationTablePanel;

    @Autowired
    private IdentificationPanel identificationPanel;

    private EntityService entityService = SpringUtils.getBean(EntityService.class);

    private Quotation quotation;

    private Boolean isAdd = false;

    public Boolean getAdd() {
        return isAdd;
    }

    public void setAdd(Boolean add) {
        isAdd = add;
    }

    @PostConstruct
    public void PostConstruct() {
        super.init();
        applicationTablePanel.setMainPanel(this);
        applicationFormPanel.setCaption(I18N.message("application"));
        applicationFormPanel.setApplicationPanel(this);
        getTabSheet().setTablePanel(applicationTablePanel);
    }

    @Override
    public void enter(ViewChangeEvent event) {
        String quotaId = event.getParameters();
        if (StringUtils.isNotEmpty(quotaId)) {
            getTabSheet().addFormPanel(applicationFormPanel);
            applicationFormPanel.assignValues(new Long(quotaId), true);
            getTabSheet().setForceSelected(true);
            getTabSheet().setSelectedTab(applicationFormPanel);
        }
    }

    @Override
    public void onAddEventClick() {
        isAdd = true;
        identificationPanel.reset();
        identificationPanel.setApplicationPanel(this);
        //getTabSheet().removeFormsPanel();
        getTabSheet().addFormPanel(identificationPanel, I18N.message("applicant.identification"));
        getTabSheet().setSelectedTab(identificationPanel);

    }

    public void addApplicationFormPanel() {
        applicationFormPanel.reset();
        getTabSheet().removeComponent(identificationPanel);
        getTabSheet().addFormPanel(applicationFormPanel);
        initSelectedTab(applicationFormPanel);
    }

    @Override
    public void onEditEventClick() {
        isAdd = false;
        applicationFormPanel.reset();
        quotation = entityService.getById(Quotation.class, applicationTablePanel.getItemSelectedId());
        getTabSheet().addFormPanel(applicationFormPanel);
        initSelectedTab(applicationFormPanel);

        if (!ProfileUtil.isAdmin()) {
            ReportUtils.buildMenuReport(quotation, applicationFormPanel.reportMenu);
        }
    }

    @Override
    public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
        if (selectedTab == applicationFormPanel) {
            applicationFormPanel.assignValues(this.quotation, true);
        } else if (selectedTab == applicationTablePanel && getTabSheet().isNeedRefresh()) {
            applicationTablePanel.refresh();
        } else if (selectedTab == applicationTablePanel) {
            applicationTablePanel.refresh();
        }
        getTabSheet().setSelectedTab(selectedTab);

    }

    public void displayApplicationTablePanel() {
        applicationTablePanel.refresh();
        getTabSheet().setSelectedTab(applicationTablePanel);
    }

    public ApplicationFormPanel getApplicationFormPanel() {
        return applicationFormPanel;
    }

    public Quotation getQuotation() {
        return quotation;
    }

    public void setQuotation(Quotation quotation) {
        this.quotation = quotation;
    }

    public AssetPanel getAssetPanel(){
        return applicationFormPanel.getAssetPanel();
    }

    public ApplicationTablePanel getApplicationTablePanel() {
        return applicationTablePanel;
    }
}
