package com.soma.mfinance.core.shared.exception.quotation;

import com.soma.mfinance.core.shared.exception.BusinessException;

/**
 * Expire quotation exception
 * @author kimsuor.seang
 *
 */
public class ExpireQuotationException extends BusinessException {

	private static final long serialVersionUID = -3131217462805735240L;

}
