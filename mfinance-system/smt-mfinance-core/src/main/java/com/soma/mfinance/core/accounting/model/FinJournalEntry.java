package com.soma.mfinance.core.accounting.model;

import javax.persistence.DiscriminatorValue;

import com.soma.ersys.finance.accounting.model.JournalEntry;

/**
 * Journal entry
 * 
 * @author kimsuor.seang
 *
 */
@DiscriminatorValue(value="E")
public class FinJournalEntry extends JournalEntry {
	/** */
	private static final long serialVersionUID = -4124346139519232502L;

	
}
