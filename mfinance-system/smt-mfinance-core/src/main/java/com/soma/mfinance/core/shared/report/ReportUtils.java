package com.soma.mfinance.core.shared.report;

import com.soma.mfinance.core.application.ReportCommand;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.financialproduct.FinancialProductEntityField;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.mfinance.glf.report.xls.*;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.MenuBar;
import org.seuksa.frmk.i18n.I18N;

/**
 * Created by Dang Dim
 * Date     : 31-Aug-17, 4:19 PM
 * Email    : d.dim@gl-f.com
 */
public class ReportUtils {

    public static void buildMenuReport(Quotation quotation, MenuBar menuBar) {

        MenuBar reportMenu = menuBar;
        reportMenu.removeItems();
        final MenuBar.MenuItem reports = reportMenu.addItem(I18N.message("reports"), null);
        reports.setIcon(FontAwesome.BAR_CHART_O);

        final MenuBar.MenuItem applicantItem = reports.addItem(I18N.message("applicant"), null);
        final MenuBar.MenuItem guarantorItem = reports.addItem(I18N.message("guarantor"), null);
        final MenuBar.MenuItem certifyItem = reports.addItem(I18N.message("certify.letter"), null);
        final MenuBar.MenuItem ownerTransferAgreement = reports.addItem(I18N.message("ownership.transference"), null);
        final MenuBar.MenuItem returnRegistration = reports.addItem(I18N.message("return.original.registration.card"), null);
        final MenuBar.MenuItem salePurchaseAgreement = reports.addItem(I18N.message("sale.purchase.agreement"), null);

        applicantItem.addItem(I18N.message("payment.schedule"), new ThemeResource("../smt-default/icons/16/csv.png"), new ReportCommand(PaymentScheduleMfp.class, quotation));
        applicantItem.addItem(I18N.message("contract"), new ThemeResource("../smt-default/icons/16/rtf.png"), new ReportCommand(LeasingContractMfp.class, quotation));
        applicantItem.addItem(I18N.message("annex"), new ThemeResource("../smt-default/icons/16/rtf.png"), new ReportCommand(LeasingContractLegalMfp.class, quotation));

        guarantorItem.addItem(I18N.message("guarantor"), new ThemeResource("../smt-default/icons/16/rtf.png"), new ReportCommand(GuaranteeContractMfp.class, quotation));
        guarantorItem.addItem(I18N.message("annex"), new ThemeResource("../smt-default/icons/16/rtf.png"), new ReportCommand(GLFGuaranteeContractLegalKH.class, quotation));

        certifyItem.addItem(I18N.message("certify.letter"), new ThemeResource("../smt-default/icons/16/rtf.png"), new ReportCommand(GLFCertifyLetterKH.class, quotation));
        ownerTransferAgreement.addItem(I18N.message("ownership.transference"), new ThemeResource("../smt-default/icons/16/rtf.png"), new ReportCommand(OwnershipTransference.class, quotation));
        returnRegistration.addItem(I18N.message("return.original.registration.card"), new ThemeResource("../smt-default/icons/16/rtf.png"), new ReportCommand(ReturnOriginalRegistrationCard.class, quotation));
        salePurchaseAgreement.addItem(I18N.message("sale.purchase.agreement"), new ThemeResource("../smt-default/icons/16/rtf.png"), new ReportCommand(SalePurchaseAgreement.class, quotation));

        if (ProfileUtil.isUW()) {
            if (quotation != null) {
                if (quotation.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_EXISTING)) {
                    applicantItem.getChildren().get(0).setVisible(false);
                    applicantItem.getChildren().get(2).setVisible(false);
                    guarantorItem.setVisible(false);
                    certifyItem.setVisible(false);
                    ownerTransferAgreement.setVisible(false);
                    salePurchaseAgreement.setVisible(false);
                    returnRegistration.setVisible(false);
                } else {
                    applicantItem.getChildren().get(0).setVisible(false);
                    applicantItem.getChildren().get(2).setVisible(false);
                    guarantorItem.getChildren().get(1).setVisible(false);
                    certifyItem.setVisible(false);
                    ownerTransferAgreement.setVisible(false);
                    salePurchaseAgreement.setVisible(false);
                    returnRegistration.setVisible(false);
                }
            }

        } else if (quotation != null) {
            if (quotation.getWkfStatus().equals(QuotationWkfStatus.APV)) {
                applicantItem.setVisible(false);
                guarantorItem.setVisible(false);
                certifyItem.setVisible(false);
                ownerTransferAgreement.setVisible(false);
                returnRegistration.setVisible(false);
            } else if ((ProfileUtil.isCreditOfficer() || ProfileUtil.isProductionOfficer()) && (quotation.getWkfStatus().equals(QuotationWkfStatus.ACT))) {
                salePurchaseAgreement.setVisible(false);
                if (quotation.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_EXISTING)) {
                    guarantorItem.setVisible(false);
                }
            }
        }

        if (quotation != null && ProfileUtil.isDocumentController()) {
            salePurchaseAgreement.setVisible(false);
            if (quotation.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_EXISTING)) {
                guarantorItem.setVisible(false);
            }
        }
    }
}
