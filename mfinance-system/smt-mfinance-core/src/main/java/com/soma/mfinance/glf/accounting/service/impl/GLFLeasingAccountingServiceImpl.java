package com.soma.mfinance.glf.accounting.service.impl;

import com.soma.common.app.history.model.EHistoReason;
import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.common.app.workflow.model.WkfHistoryItem;
import com.soma.common.app.workflow.service.WkfHistoryItemRestriction;
import com.soma.mfinance.core.accounting.InstallmentVO;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.applicant.model.Employment;
import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.mfinance.core.application.model.EmploymentOccupation;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.ContractAdjustment;
import com.soma.mfinance.core.contract.model.ContractApplicant;
import com.soma.mfinance.core.contract.model.ContractWkfHistoryItem;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowType;
import com.soma.mfinance.core.contract.service.ContractService;
import com.soma.mfinance.core.contract.service.cashflow.CashflowService;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.financial.model.EServiceType;
import com.soma.mfinance.core.financial.service.FinanceCalculationService;
import com.soma.mfinance.core.payment.model.Payment;
import com.soma.mfinance.core.quotation.model.EInsuranceYear;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationService;
import com.soma.mfinance.core.quotation.model.QuotationWkfHistoryItem;
import com.soma.mfinance.core.registrations.model.ProfileContractStatus;
import com.soma.mfinance.core.shared.accounting.*;
import com.soma.mfinance.core.shared.cashflow.CashflowEntityField;
import com.soma.mfinance.core.shared.service.ServiceEntityField;
import com.soma.mfinance.core.workflow.ContractHistoReason;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.mfinance.glf.accounting.service.GLFLeasingAccountingService;
import com.soma.ersys.core.hr.model.address.Address;
import com.soma.finance.services.shared.AmortizationSchedules;
import com.soma.finance.services.shared.CalculationParameter;
import com.soma.finance.services.shared.Schedule;
import com.soma.finance.services.shared.system.EFrequency;
import com.soma.finance.services.tools.LoanUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.*;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.model.meta.NativeColumn;
import org.seuksa.frmk.model.meta.NativeRow;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyMathUtils;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.amount.Amount;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.seuksa.frmk.tools.exception.NativeQueryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.soma.mfinance.core.helper.FinServicesHelper.INSTALLMENT_SERVICE_MFP;
import static com.soma.frmk.helper.SeuksaServicesHelper.ENTITY_SRV;
import static com.soma.frmk.helper.SeuksaServicesHelper.WKF_SRV;


/**
 * Contract service
 *
 * @author kimsuor.seang
 */
@Service("gLFLeasingAccountingService")
public class GLFLeasingAccountingServiceImpl extends BaseEntityServiceImpl implements GLFLeasingAccountingService, CashflowEntityField {

    private static final long serialVersionUID = 8800816886214543276L;

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private EntityDao dao;

    @Autowired
    private FinanceCalculationService financeCalculationService;

    @Autowired
    private ContractService contractService;

    @Autowired
    private CashflowService cashflowService;

    @Override
    public EntityDao getDao() {
        return dao;
    }

    private EHistoReason[] getHistoReasons() {
        return new EHistoReason[]{
                ContractHistoReason.CONTRACT_0003,
                ContractHistoReason.CONTRACT_ACC,
                ContractHistoReason.CONTRACT_FRA,
                ContractHistoReason.CONTRACT_LOSS,
                ContractHistoReason.CONTRACT_REP,
                ContractHistoReason.CONTRACT_THE,
                ContractHistoReason.CONTRACT_WRI,
        };
    }

    @Override
    public List<LeaseAdjustment> getLeaseAdjustments(EDealerType dealerType,
                                                     Dealer dealer, EWkfStatus contractStatus, String reference, Date startDate, Date endDate) {
        List<LeaseAdjustment> leaseAdjustments = new ArrayList<>();
        BaseRestrictions<Contract> restrictions = new BaseRestrictions<>(Contract.class);
        restrictions.addCriterion(Restrictions.in(CONTRACT_STATUS, new EWkfStatus[]{
                ContractWkfStatus.LOS, ContractWkfStatus.THE, ContractWkfStatus.EAR, ContractWkfStatus.FRA,
                ContractWkfStatus.ACC, ContractWkfStatus.REP, ContractWkfStatus.WRI}));

        DetachedCriteria historySubCriteria = DetachedCriteria.forClass(WkfHistoryItem.class, "history");
        historySubCriteria.add(Restrictions.in("history.hisReason", getHistoReasons()));
        if (startDate != null) {
            historySubCriteria.add(Restrictions.ge("history.historyDate", DateUtils.getDateAtBeginningOfDay(startDate)));
        }
        if (endDate != null) {
            historySubCriteria.add(Restrictions.le("history.historyDate", DateUtils.getDateAtEndOfDay(endDate)));
        }

        historySubCriteria.setProjection(Projections.projectionList().add(Projections.property("history.contract_old.id")));
        restrictions.addCriterion(Property.forName("id").in(historySubCriteria));

        if (dealer != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        }

        if (StringUtils.isNotEmpty(reference)) {
            restrictions.addCriterion(Restrictions.ilike(REFERENCE, reference, MatchMode.ANYWHERE));
        }

        if (contractStatus != null) {
            restrictions.addCriterion(Restrictions.eq(CONTRACT_STATUS, contractStatus));
        }

        restrictions.addOrder(Order.desc("startDate"));

        List<Contract> contracts = list(restrictions);
        for (Contract contract : contracts) {
            LeaseAdjustment leaseAdjustment = new LeaseAdjustment();
            leaseAdjustment.setId(contract.getId());
            leaseAdjustment.setReference(contract.getReference());
            leaseAdjustment.setWkfStatus(contract.getWkfStatus());
            leaseAdjustment.setContractStartDate(contract.getStartDate());
            leaseAdjustment.setFirstInstallmentDate(contract.getFirstDueDate());
            leaseAdjustment.setChangeStatusDate(getEventDate(contract.getHistories(), contract.getWkfStatus()));
            leaseAdjustment.setFirstNameEn(contract.getApplicant().getIndividual().getFirstNameEn());
            leaseAdjustment.setLastNameEn(contract.getApplicant().getIndividual().getLastNameEn());
            ContractAdjustment contractAdjustment = contract.getContractAdjustment();
            if (contractAdjustment != null) {
                leaseAdjustment.setBalanceInterestInSuspend(new Amount(contractAdjustment.getTeBalanceInterestInSuspendUsd(),
                        contractAdjustment.getVatBalanceInterestInSuspendUsd(), contractAdjustment.getTiBalanceInterestInSuspendUsd()));
                leaseAdjustment.setUnpaidAccruedInterestReceivable(new Amount(contractAdjustment.getTeUnpaidAccruedInterestReceivableUsd(),
                        contractAdjustment.getVatUnpaidAccruedInterestReceivableUsd(),
                        contractAdjustment.getTiUnpaidAccruedInterestReceivableUsd()));
                leaseAdjustment.setUnpaidInterestBalance(new Amount(contractAdjustment.getTeAdjustmentInterest(), contractAdjustment.getVatAdjustmentInterest(), contractAdjustment.getTiAdjustmentInterest()));
                leaseAdjustment.setUnpaidPrincipalBalance(new Amount(contractAdjustment.getTeAdjustmentPrincipal(), contractAdjustment.getVatAdjustmentPrincipal(), contractAdjustment.getTiAdjustmentPrincipal()));
            }
            leaseAdjustments.add(leaseAdjustment);
        }
        return leaseAdjustments;
    }

    @Override
    public List<LeaseAdjustment> getLeaseAdjustments(EDealerType dealerType, Dealer dealer, ProfileContractStatus contractStatus, String reference, Date startDate, Date endDate, Date payStartDate, EInsuranceYear insuranceYear) {
        List<LeaseAdjustment> leaseAdjustments = new ArrayList<>();

        BaseRestrictions<Contract> restrictions = new BaseRestrictions<>(Contract.class);

        restrictions.addCriterion(Restrictions.in(WKF_STATUS, new EWkfStatus[]{
                ContractWkfStatus.LOS,
                ContractWkfStatus.THE,
                ContractWkfStatus.EAR,
                ContractWkfStatus.FRA,
                ContractWkfStatus.ACC,
                ContractWkfStatus.REP,
                ContractWkfStatus.WRI,
                ContractWkfStatus.FIN}));
        restrictions.addAssociation("quotations", "quo", JoinType.INNER_JOIN);
        if (startDate != null) {
            restrictions.addCriterion(Restrictions.ge("quo.insuranceStartDate", DateUtils.getDateAtBeginningOfDay(startDate)));
        }
        if (endDate != null) {
            restrictions.addCriterion(Restrictions.le("quo.insuranceStartDate", DateUtils.getDateAtEndOfDay(endDate)));
        }

        Disjunction orJunction = Restrictions.or();

        if (dealer != null) {
            restrictions.addAssociation("quo.dealer", "dea", JoinType.INNER_JOIN);
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
            if (dealerType != null) {
                restrictions.addCriterion(Restrictions.eq("dea.dealerType", dealerType));
            }
        }
        if (StringUtils.isNotEmpty(reference)) {
            restrictions.addCriterion(Restrictions.ilike(REFERENCE, reference, MatchMode.ANYWHERE));
        }
        if (contractStatus != null) {
            if (contractStatus.getWkfStatus() != null) {
                restrictions.addCriterion(Restrictions.eq(WKF_STATUS, contractStatus.getWkfStatus()));
            }
        }

        if (insuranceYear != null && payStartDate != null) {
            int numberInsurance = Integer.parseInt(insuranceYear.getCode().toString()) - 1;
            restrictions.addCriterion(Restrictions.between("quo.insuranceStartDate", DateUtils.getDateAtBeginningOfDay(DateUtils.addYearsDate(payStartDate, -numberInsurance)), DateUtils.getDateAtEndOfDay(DateUtils.addYearsDate(payStartDate, -numberInsurance))));
        }

        if (insuranceYear == null && payStartDate != null) {
            orJunction.add(Restrictions.between("quo.insuranceStartDate", DateUtils.getDateAtBeginningOfDay(payStartDate), DateUtils.getDateAtEndOfDay(payStartDate)));
            orJunction.add(Restrictions.between("quo.insuranceStartDate", DateUtils.getDateAtBeginningOfDay(DateUtils.addYearsDate(payStartDate, -1)), DateUtils.getDateAtEndOfDay(DateUtils.addYearsDate(payStartDate, -1))));
            orJunction.add(Restrictions.between("quo.insuranceStartDate", DateUtils.getDateAtBeginningOfDay(DateUtils.addYearsDate(payStartDate, -2)), DateUtils.getDateAtEndOfDay(DateUtils.addYearsDate(payStartDate, -2))));
            orJunction.add(Restrictions.between("quo.insuranceStartDate", DateUtils.getDateAtBeginningOfDay(DateUtils.addYearsDate(payStartDate, -3)), DateUtils.getDateAtEndOfDay(DateUtils.addYearsDate(payStartDate, -3))));
            orJunction.add(Restrictions.between("quo.insuranceStartDate", DateUtils.getDateAtBeginningOfDay(DateUtils.addYearsDate(payStartDate, -4)), DateUtils.getDateAtEndOfDay(DateUtils.addYearsDate(payStartDate, -4))));
        } else if (payStartDate != null) {
            orJunction.add(Restrictions.eq("quo.insuranceStartDate", payStartDate));
        }
        restrictions.addCriterion(orJunction);
        restrictions.addOrder(Order.desc("startDate"));

        List<Contract> contracts = ENTITY_SRV.list(restrictions);
        for (Contract contract : contracts) {
            LeaseAdjustment leaseAdjustment = new LeaseAdjustment();
            Quotation quotation = contract.getQuotation();
            leaseAdjustment.setTerm(contract.getTerm());
            leaseAdjustment.setTiAdvancePaymentUsd(contract.getTiAdvancePaymentAmount() == null ? 0.0d : contract.getTiAdvancePaymentAmount());
            leaseAdjustment.setTeAdvancePaymentUsd(contract.getTiAdvancePaymentAmount() == null ? 0.0d : contract.getTiAdvancePaymentAmount());
            leaseAdjustment.setId(contract.getId());
            leaseAdjustment.setQuotationID(contract.getQuotation().getId());
            leaseAdjustment.setReference(contract.getReference());
            leaseAdjustment.setContractStatus(contract.getWkfStatus());
            leaseAdjustment.setContractStartDate(contract.getStartDate());
            leaseAdjustment.setAssetPrice(quotation.getAsset().getTiAssetPrice() == null ? 0.0d : quotation.getAsset().getTiAssetPrice());
            leaseAdjustment.setInsuranceStartDate(contract.getQuotation().getInsuranceStartDate());
            if (quotation.getInsuranceEndYear() == null) {
                getCalculationInsuranceEndYear();
                leaseAdjustment.setInsuranceEndYear(quotation.getInsuranceEndYear());
            } else {
                leaseAdjustment.setInsuranceEndYear(quotation.getInsuranceEndYear());
            }

            if (quotation.getInsuranceStartDate() != null) {
                leaseAdjustment.setNoYearInsurance(getNumberYearOfTwoDates(DateUtils.todayDate(), quotation.getInsuranceStartDate()));
            } else if (quotation.getInsuranceStartDate() != null && quotation.getInsuranceEndYear() != null) {
                leaseAdjustment.setNoYearInsurance(getNumberYearOfTwoDates(quotation.getInsuranceEndYear(), quotation.getInsuranceStartDate()));
            }
            leaseAdjustment.setFirstInstallmentDate(contract.getQuotation().getContractStartDate());
            leaseAdjustment.setChangeStatusDate(getEventChangeDate(quotation));
            leaseAdjustment.setFirstNameEn(contract.getApplicant().getFirstNameEn());
            leaseAdjustment.setLastNameEn(contract.getApplicant().getLastNameEn());

            if (quotation.getAsset() != null) {
                Asset asset = quotation.getAsset();
                if (asset.getChassisNumber() != null) {
                    leaseAdjustment.setChassisNumber(asset.getChassisNumber());
                }
                if (asset.getEngineNumber() != null) {
                    leaseAdjustment.setEngineNumber(asset.getEngineNumber());
                }
            }
            ContractAdjustment contractAdjustment = contract.getContractAdjustment();
            if (contractAdjustment != null) {
                if (contract.getWkfStatus() == ContractWkfStatus.EAR) {
                    leaseAdjustment.setBalanceInterestInSuspend(new Amount(0d, 0d, 0d));
                    leaseAdjustment.setUnpaidAccruedInterestReceivable(new Amount(0d, 0d, 0d));
                } else {
                    leaseAdjustment.setBalanceInterestInSuspend(
                            new Amount(contractAdjustment.getTeBalanceInterestInSuspendUsd(),
                                    contractAdjustment.getVatBalanceInterestInSuspendUsd(),
                                    contractAdjustment.getTiBalanceInterestInSuspendUsd()));
                    leaseAdjustment.setUnpaidAccruedInterestReceivable(
                            new Amount(contractAdjustment.getTeUnpaidAccruedInterestReceivableUsd(),
                                    contractAdjustment.getVatUnpaidAccruedInterestReceivableUsd(),
                                    contractAdjustment.getTiUnpaidAccruedInterestReceivableUsd()));
                }
            }
            /*if (contract.getWkfStatus() == ContractWkfStatus.WAIT_TRANSACTION_SETTLEMENT) {
                double unpaidRate = cashflowService.getAmountRemainingByCashflow(contract.getId(), ECashflowType.IAP);
                double unpaidPrincipal = cashflowService.getAmountRemainingByCashflow(contract.getId(),
                        ECashflowType.CAP);
                leaseAdjustment.setUnpaidInterestBalance(new Amount(unpaidRate, 0d, unpaidRate));
                leaseAdjustment.setUnpaidPrincipalBalance(new Amount(unpaidPrincipal, 0d, unpaidPrincipal));
                leaseAdjustment.setUnpaidAccruedInterestReceivable(getAccruedInterestIncome(contract));
            } else {
                leaseAdjustment.setUnpaidInterestBalance(
                        new Amount(MyNumberUtils.getDouble(contract.getTeAdjustmentInterest()),
                                MyNumberUtils.getDouble(contract.getVatAdjustmentInterestUsd()),
                                MyNumberUtils.getDouble(contract.getTiAdjustmentInterestUsd())));
                leaseAdjustment.setUnpaidPrincipalBalance(new Amount(contract.getTeAdjustmentPrincipalUsd(),
                        contract.getVatAdjustmentPrincipalUsd(), contract.getTiAdjustmentPrincipalUsd()));
                leaseAdjustment.setUnpaidInterestBalance(
                        new Amount(MyNumberUtils.getDouble(contract.getTmFinancedAmount()),
                                MyNumberUtils.getDouble(contract.getVatFinancedAmount()),
                                MyNumberUtils.getDouble(contract.getTiFinancedAmount())));
                leaseAdjustment.setUnpaidPrincipalBalance(new Amount(contract.get(),
                        contract.getVatAdjustmentPrincipalUsd(), contract.getTiAdjustmentPrincipalUsd()));
            }*/
            leaseAdjustments.add(leaseAdjustment);
        }
        return leaseAdjustments;
    }

    public List<ReferalFeeAdjustment> getReferalFeeAdjustments(EDealerType dealerType, Dealer dealer, String reference, Date startDate, Date endDate) {
        List<ReferalFeeAdjustment> referalFeeAdjustments = new ArrayList<>();
        BaseRestrictions<Contract> restrictions = new BaseRestrictions<>(Contract.class);
        restrictions.addCriterion(Restrictions.in(CONTRACT_STATUS, new EWkfStatus[]{
                ContractWkfStatus.LOS, ContractWkfStatus.THE, ContractWkfStatus.EAR, ContractWkfStatus.FRA, ContractWkfStatus.ACC, ContractWkfStatus.REP}));

        DetachedCriteria historySubCriteria = DetachedCriteria.forClass(WkfHistoryItem.class, "history");
        historySubCriteria.add(Restrictions.in("history.hisReason", getHistoReasons()));
        if (startDate != null) {
            historySubCriteria.add(Restrictions.ge("history.historyDate", DateUtils.getDateAtBeginningOfDay(startDate)));
        }
        if (endDate != null) {
            historySubCriteria.add(Restrictions.le("history.historyDate", DateUtils.getDateAtEndOfDay(endDate)));
        }

        historySubCriteria.setProjection(Projections.projectionList().add(Projections.property("history.contract_old.id")));
        restrictions.addCriterion(Property.forName("id").in(historySubCriteria));

        if (dealer != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        }

        if (StringUtils.isNotEmpty(reference)) {
            restrictions.addCriterion(Restrictions.ilike(REFERENCE, reference, MatchMode.ANYWHERE));
        }

        restrictions.addOrder(Order.desc("startDate"));

        List<Contract> contracts = list(restrictions);
        for (Contract contract : contracts) {
            ReferalFeeAdjustment referalFeeAdjustment = new ReferalFeeAdjustment();
            referalFeeAdjustment.setId(contract.getId());
            referalFeeAdjustment.setReference(contract.getReference());
            referalFeeAdjustment.setWkfStatus(contract.getWkfStatus());
            referalFeeAdjustment.setContractStartDate(contract.getStartDate());
            referalFeeAdjustment.setFirstInstallmentDate(contract.getFirstDueDate());
            referalFeeAdjustment.setFirstNameEn(contract.getApplicant().getIndividual().getFirstNameEn());
            referalFeeAdjustment.setLastNameEn(contract.getApplicant().getIndividual().getLastNameEn());
            if (contract.getContractAdjustment() != null) {
            } else {
                referalFeeAdjustment.setUnpaidDeferredCommissionReferalFee(new Amount(0d, 0d, 0d));
                referalFeeAdjustment.setUnpaidAcrrualExpenses(new Amount(0d, 0d, 0d));
            }
            referalFeeAdjustments.add(referalFeeAdjustment);
        }
        return referalFeeAdjustments;
    }

    public List<InsuranceIncomeAdjustment> getInsuranceIncomeAdjustments(EDealerType dealerType, Dealer dealer, String reference, Date startDate, Date endDate) {
        List<InsuranceIncomeAdjustment> insuranceIncomeAdjustments = new ArrayList<>();
        BaseRestrictions<Contract> restrictions = new BaseRestrictions<>(Contract.class);
        restrictions.addCriterion(Restrictions.in(CONTRACT_STATUS, new EWkfStatus[]{
                ContractWkfStatus.LOS, ContractWkfStatus.THE, ContractWkfStatus.EAR, ContractWkfStatus.FRA, ContractWkfStatus.ACC, ContractWkfStatus.REP}));

        DetachedCriteria historySubCriteria = DetachedCriteria.forClass(WkfHistoryItem.class, "history");
        historySubCriteria.add(Restrictions.in("history.hisReason", getHistoReasons()));
        if (startDate != null) {
            historySubCriteria.add(Restrictions.ge("history.historyDate", DateUtils.getDateAtBeginningOfDay(startDate)));
        }
        if (endDate != null) {
            historySubCriteria.add(Restrictions.le("history.historyDate", DateUtils.getDateAtEndOfDay(endDate)));
        }

        historySubCriteria.setProjection(Projections.projectionList().add(Projections.property("history.contract_old.id")));
        restrictions.addCriterion(Property.forName("id").in(historySubCriteria));

        if (dealer != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        }

        if (StringUtils.isNotEmpty(reference)) {
            restrictions.addCriterion(Restrictions.ilike(REFERENCE, reference, MatchMode.ANYWHERE));
        }

        restrictions.addOrder(Order.desc("startDate"));

        List<Contract> contracts = list(restrictions);
        for (Contract contract : contracts) {
            InsuranceIncomeAdjustment insuranceIncomeAdjustment = new InsuranceIncomeAdjustment();
            insuranceIncomeAdjustment.setId(contract.getId());
            insuranceIncomeAdjustment.setReference(contract.getReference());
            insuranceIncomeAdjustment.setWkfStatus(contract.getWkfStatus());
            insuranceIncomeAdjustment.setContractStartDate(contract.getStartDate());
            insuranceIncomeAdjustment.setFirstInstallmentDate(contract.getFirstDueDate());
            insuranceIncomeAdjustment.setFirstNameEn(contract.getApplicant().getIndividual().getFirstNameEn());
            insuranceIncomeAdjustment.setLastNameEn(contract.getApplicant().getIndividual().getLastNameEn());
            if (contract.getContractAdjustment() != null) {
            } else {
                insuranceIncomeAdjustment.setBalanceInsuranceIncomeInSuspend(new Amount(0d, 0d, 0d));
                insuranceIncomeAdjustment.setUnpaidUnearnedInsuranceIncome(new Amount(0d, 0d, 0d));
                insuranceIncomeAdjustment.setUnpaidAccrualReceivable(new Amount(0d, 0d, 0d));
            }
            insuranceIncomeAdjustments.add(insuranceIncomeAdjustment);
        }
        return insuranceIncomeAdjustments;
    }

    public List<ServicingIncomeAdjustment> getServicingIncomeAdjustments(EDealerType dealerType, Dealer dealer, String reference, Date startDate, Date endDate) {
        List<ServicingIncomeAdjustment> servicingIncomeAdjustments = new ArrayList<>();
        BaseRestrictions<Contract> restrictions = new BaseRestrictions<>(Contract.class);
        restrictions.addCriterion(Restrictions.in(CONTRACT_STATUS, new EWkfStatus[]{
                ContractWkfStatus.LOS, ContractWkfStatus.THE, ContractWkfStatus.EAR, ContractWkfStatus.FRA, ContractWkfStatus.ACC, ContractWkfStatus.REP}));

        DetachedCriteria historySubCriteria = DetachedCriteria.forClass(WkfHistoryItem.class, "history");
        historySubCriteria.add(Restrictions.in("history.hisReason", getHistoReasons()));
        if (startDate != null) {
            historySubCriteria.add(Restrictions.ge("history.historyDate", DateUtils.getDateAtBeginningOfDay(startDate)));
        }
        if (endDate != null) {
            historySubCriteria.add(Restrictions.le("history.historyDate", DateUtils.getDateAtEndOfDay(endDate)));
        }

        historySubCriteria.setProjection(Projections.projectionList().add(Projections.property("history.contract_old.id")));
        restrictions.addCriterion(Property.forName("id").in(historySubCriteria));

        if (dealer != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        }

        if (StringUtils.isNotEmpty(reference)) {
            restrictions.addCriterion(Restrictions.ilike(REFERENCE, reference, MatchMode.ANYWHERE));
        }

        restrictions.addOrder(Order.desc("startDate"));

        List<Contract> contracts = list(restrictions);
        for (Contract contract : contracts) {
            ServicingIncomeAdjustment servicingIncomeAdjustment = new ServicingIncomeAdjustment();
            servicingIncomeAdjustment.setId(contract.getId());
            servicingIncomeAdjustment.setReference(contract.getReference());
            servicingIncomeAdjustment.setWkfStatus(contract.getWkfStatus());
            servicingIncomeAdjustment.setContractStartDate(contract.getStartDate());
            servicingIncomeAdjustment.setFirstInstallmentDate(contract.getFirstDueDate());
            servicingIncomeAdjustment.setFirstNameEn(contract.getApplicant().getIndividual().getFirstNameEn());
            servicingIncomeAdjustment.setLastNameEn(contract.getApplicant().getIndividual().getLastNameEn());
            if (contract.getContractAdjustment() != null) {
            } else {
                servicingIncomeAdjustment.setBalanceServicingIncomeInSuspend(new Amount(0d, 0d, 0d));
                servicingIncomeAdjustment.setUnpaidUnearnedServicingIncome(new Amount(0d, 0d, 0d));
                servicingIncomeAdjustment.setUnpaidAccrualReceivable(new Amount(0d, 0d, 0d));
            }
            servicingIncomeAdjustments.add(servicingIncomeAdjustment);
        }
        return servicingIncomeAdjustments;
    }

    private Date getEventDate(List<ContractWkfHistoryItem> histories, EWkfStatus contractStatus) {
        Date earlySettlementDate = null;
        for (ContractWkfHistoryItem history : histories) {
           if(contractStatus.getCode().equals(history.getNewValue())) {
               earlySettlementDate = DateUtils.getDateAtBeginningOfDay(history.getChangeDate());
           }
        }
        return earlySettlementDate;
    }

    public Date getEventChangeDate(Quotation quotation) {
        Date changeStatusDate = null;
        WkfHistoryItemRestriction restrictions = new WkfHistoryItemRestriction(QuotationWkfHistoryItem.class);
        restrictions.setEntityId(quotation.getId());
        restrictions.addOrder(Order.desc("changeDate"));
        restrictions.setPropertyName("wkfStatus");
        List<QuotationWkfHistoryItem> wkfBaseHistoryItems = WKF_SRV.getHistories(restrictions);
        for (QuotationWkfHistoryItem historyItem : wkfBaseHistoryItems) {
            if (quotation.getWkfStatus().getCode().equals(historyItem.getNewValue())) {
                changeStatusDate = historyItem.getChangeDate();
                break;
            }
        }
        return changeStatusDate;
    }

    public Amount getAccountingInterestUnearnedBalance(Date calculDate, List<Cashflow> cashflows) {
        Amount interestBalance = new Amount(0d, 0d, 0d);
        for (Cashflow cashflow : cashflows) {
            if (cashflow.getCashflowType().equals(ECashflowType.IAP)
                    && !cashflow.isCancel()
                    && !cashflow.isUnpaid()
                    && (!cashflow.isPaid() || cashflow.getInstallmentDate().compareTo(calculDate) > 0 || (cashflow.getPayment() != null && cashflow.getPayment().getPaymentDate().compareTo(calculDate) > 0))) {
                interestBalance.plusTiAmount(cashflow.getTiInstallmentAmount());
                interestBalance.plusTeAmount(cashflow.getTeInstallmentAmount());
                interestBalance.plusVatAmount(cashflow.getVatInstallmentAmount());
            }
        }
        return interestBalance;
    }

    private Amount getTheoricalInterestUnearnedBalance(Date calculDate, List<Cashflow> cashflows) {
        Amount interestBalance = new Amount(0d, 0d, 0d);
        for (Cashflow cashflow : cashflows) {
            if (cashflow.getCashflowType().equals(ECashflowType.IAP)
                    && !cashflow.isCancel()
                    && (cashflow.getInstallmentDate().compareTo(calculDate) >= 0 || !cashflow.isPaid() || (cashflow.getPayment() != null && cashflow.getPayment().getPaymentDate().compareTo(calculDate) > 0))
                    && !cashflow.isUnpaid()) {
                interestBalance.plusTiAmount(cashflow.getTiInstallmentAmount());
                interestBalance.plusTeAmount(cashflow.getTeInstallmentAmount());
                interestBalance.plusVatAmount(cashflow.getVatInstallmentAmount());
            }
        }
        return interestBalance;
    }

    /**
     * Get interest unearned balance
     *
     * @param calculDate
     * @param cashflows
     * @return
     */
    public Amount getInterestUnearnedBalance(Date calculDate, List<Cashflow> cashflows) {
        Amount interestBalance = new Amount(0d, 0d, 0d);
        for (Cashflow cashflow : cashflows) {
            if (cashflow.getCashflowType().equals(ECashflowType.IAP)
                    && !cashflow.isCancel()
                    && (!cashflow.isPaid() || (cashflow.getPayment() != null && cashflow.getPayment().getPaymentDate().compareTo(calculDate) > 0))
                    && !cashflow.isUnpaid()) {
                interestBalance.plusTiAmount(cashflow.getTiInstallmentAmount());
                interestBalance.plusTeAmount(cashflow.getTeInstallmentAmount());
                interestBalance.plusVatAmount(cashflow.getVatInstallmentAmount());
            }
        }
        return interestBalance;
    }

    /**
     * Get principal balance
     *
     * @param calculDate
     * @param cashflows
     * @return
     */
    public Amount getAccountingPrincipalBalance(Date calculDate, List<Cashflow> cashflows) {
        Amount principalBalance = new Amount(0d, 0d, 0d);
        for (Cashflow cashflow : cashflows) {
            if (cashflow.getCashflowType().equals(ECashflowType.CAP)
                    && !cashflow.isCancel()
                    && !cashflow.isUnpaid()
                    && (!cashflow.isPaid() || cashflow.getInstallmentDate().compareTo(calculDate) > 0 || (cashflow.getPayment() != null && cashflow.getPayment().getPaymentDate().compareTo(calculDate) > 0))) {
                principalBalance.plusTiAmount(cashflow.getTiInstallmentAmount());
                principalBalance.plusTeAmount(cashflow.getTeInstallmentAmount());
                principalBalance.plusVatAmount(cashflow.getVatInstallmentAmount());
            }
        }
        return principalBalance;
    }

    /**
     * Get principal balance
     *
     * @param calculDate
     * @param cashflows
     * @return
     */
    public Amount getAccountingServiceBalance(Date calculDate, List<Cashflow> cashflows) {
        Amount principalBalance = new Amount(0d, 0d, 0d);
        for (Cashflow cashflow : cashflows) {
            if (!cashflow.isCancel()
                    && !cashflow.isUnpaid()
                    && (!cashflow.isPaid() || cashflow.getInstallmentDate().compareTo(calculDate) > 0 || (cashflow.getPayment() != null && cashflow.getPayment().getPaymentDate().compareTo(calculDate) > 0))) {
                principalBalance.plusTiAmount(cashflow.getTiInstallmentAmount());
                principalBalance.plusTeAmount(cashflow.getTeInstallmentAmount());
                principalBalance.plusVatAmount(cashflow.getVatInstallmentAmount());
            }
        }
        return principalBalance;
    }

    /**
     * @param calculDate
     * @param cashflows
     * @return
     */
    private Amount getTheoricalPrincipalBalance(Date calculDate, List<Cashflow> cashflows) {
        Amount outstanding = new Amount(0d, 0d, 0d);
        for (Cashflow cashflow : cashflows) {
            if (cashflow.getCashflowType().equals(ECashflowType.CAP)
                    && !cashflow.isCancel()
                    && (cashflow.getInstallmentDate().compareTo(calculDate) >= 0 || !cashflow.isPaid() || (cashflow.getPayment() != null && cashflow.getPayment().getPaymentDate().compareTo(calculDate) > 0))
                    && !cashflow.isUnpaid()) {
                outstanding.plusTiAmount(cashflow.getTiInstallmentAmount());
                outstanding.plusTeAmount(cashflow.getTeInstallmentAmount());
                outstanding.plusVatAmount(cashflow.getVatInstallmentAmount());
            }
        }
        return outstanding;
    }

    /**
     * Get principal balance
     *
     * @param calculDate
     * @param cashflows
     * @return
     */
    public Amount getPrincipalBalance(Date calculDate, List<Cashflow> cashflows) {
        Amount principalBalance = new Amount(0d, 0d, 0d);
        for (Cashflow cashflow : cashflows) {
            if (cashflow.getCashflowType().equals(ECashflowType.CAP)
                    && !cashflow.isCancel()
                    && (!cashflow.isPaid() || (cashflow.getPayment() != null && cashflow.getPayment().getPaymentDate().compareTo(calculDate) > 0))
                    && !cashflow.isUnpaid()) {
                principalBalance.plusTiAmount(cashflow.getTiInstallmentAmount());
                principalBalance.plusTeAmount(cashflow.getTeInstallmentAmount());
                principalBalance.plusVatAmount(cashflow.getVatInstallmentAmount());
            }
        }
        return principalBalance;
    }

    /**
     * Get referal fees
     *
     * @param dealer
     * @param reference
     * @param startDate
     * @param endDate
     * @return
     */
    public List<ReferalFee> getReferalFees(EDealerType dealerType, Dealer dealer, String reference, Date startDate, Date endDate) {
        List<ReferalFee> referalFees = new ArrayList<>();

        startDate = DateUtils.getDateAtBeginningOfDay(startDate);
        endDate = DateUtils.getDateAtBeginningOfDay(endDate);

        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.in("wkfStatus",
                new EWkfStatus[]{QuotationWkfStatus.ACT, QuotationWkfStatus.ACG, QuotationWkfStatus.RVG, QuotationWkfStatus.RCG, QuotationWkfStatus.LCG}));

        restrictions.addCriterion(Restrictions.le("contractStartDate", endDate));

        if (dealer != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        }

        if (StringUtils.isNotEmpty(reference)) {
            restrictions.addCriterion(Restrictions.ilike(REFERENCE, reference, MatchMode.ANYWHERE));
        }

        List<Quotation> quotations = list(restrictions);
        for (Quotation quotation : quotations) {
            CalculationParameter calculationParameter = new CalculationParameter();
            calculationParameter.setInitialPrincipal(quotation.getTiFinanceAmount());
            calculationParameter.setNumberOfPeriods(LoanUtils.getNumberOfPeriods(quotation.getTerm(), quotation.getFrequency()));
            calculationParameter.setPeriodicInterestRate(quotation.getInterestRate() / 100d);
            calculationParameter.setFrequency(quotation.getFrequency());

            GLFReferalFeeCalculatorImpl calculator = new GLFReferalFeeCalculatorImpl();
            ReferalFeeSchedules referalFeeSchedules = calculator.getSchedules(quotation.getContractStartDate(), quotation.getFirstDueDate(), calculationParameter);

            ReferalFee referalFee = new ReferalFee();
            referalFee.setId(quotation.getId());
            referalFee.setReference(quotation.getReference());
            referalFee.setContractStartDate(quotation.getContractStartDate());
            referalFee.setFirstInstallmentDate(quotation.getFirstDueDate());
            referalFee.setFirstNameEn(quotation.getApplicant().getIndividual().getFirstNameEn());
            referalFee.setLastNameEn(quotation.getApplicant().getIndividual().getLastNameEn());
            for (ReferalFeeSchedule referalFeeSchedule : referalFeeSchedules.getSchedules()) {
                if (startDate.compareTo(referalFeeSchedule.getPeriodStartDate()) <= 0
                        && endDate.compareTo(referalFeeSchedule.getPeriodEndDate()) >= 0) {
                    referalFee.getReferalFeeDistribution2().plus(new Amount(referalFeeSchedule.getReferalFeeDistribution2(), 0d, referalFeeSchedule.getReferalFeeDistribution2()));
                    referalFee.getReferalFeeDistribution3().plus(new Amount(referalFeeSchedule.getReferalFeeDistribution3(), 0d, referalFeeSchedule.getReferalFeeDistribution3()));
                    referalFee.getCumulativeBalance().plus(new Amount(referalFeeSchedule.getCumulativeBalance(), 0d, referalFeeSchedule.getCumulativeBalance()));
                    referalFee.getPaymentToDealer().plus(new Amount(referalFeeSchedule.getPaymentToDealer(), 0d, referalFeeSchedule.getPaymentToDealer()));
                }
                if (referalFeeSchedule.getPeriodEndDate().compareTo(endDate) >= 0
                        && referalFee.getDeferredCommissionReferalFee().getTiAmount() == null) {
                    referalFee.setDeferredCommissionReferalFee(new Amount(referalFeeSchedule.getDeferredCommissionReferalFee(), 0d, referalFeeSchedule.getDeferredCommissionReferalFee()));
                    referalFee.setAcrrualExpenses(new Amount(referalFeeSchedule.getAcrrualExpenses(), 0d, referalFeeSchedule.getAcrrualExpenses()));
                }
            }

            if (referalFee.getRealReferalFeeDistributed().getTiAmount() != null
                    && referalFee.getRealReferalFeeDistributed().getTiAmount() > 0d) {
                referalFees.add(referalFee);
            }

        }

        return referalFees;
    }

    /**
     * Get insurance incomes
     *
     * @param dealer
     * @param reference
     * @param startDate
     * @param endDate
     * @return
     */
    public List<InsuranceExpense> getInsuranceExpenses(EDealerType dealerType, Dealer dealer, String reference, Date startDate, Date endDate) {
        List<InsuranceExpense> insuranceExpenses = new ArrayList<>();

        startDate = DateUtils.getDateAtBeginningOfDay(startDate);
        endDate = DateUtils.getDateAtBeginningOfDay(endDate);

        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.in("wkfStatus",
                new EWkfStatus[]{QuotationWkfStatus.ACT, QuotationWkfStatus.ACG, QuotationWkfStatus.RVG, QuotationWkfStatus.RCG, QuotationWkfStatus.LCG}));

        restrictions.addCriterion(Restrictions.le("contractStartDate", endDate));

        if (dealer != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        }

        if (StringUtils.isNotEmpty(reference)) {
            restrictions.addCriterion(Restrictions.ilike(REFERENCE, reference, MatchMode.ANYWHERE));
        }

        List<Quotation> quotations = list(restrictions);
        for (Quotation quotation : quotations) {
            CalculationParameter calculationParameter = new CalculationParameter();
            calculationParameter.setAssetPrice(quotation.getAsset().getTiAssetPrice());
            calculationParameter.setInitialPrincipal(quotation.getTiFinanceAmount());
            calculationParameter.setNumberOfPeriods(LoanUtils.getNumberOfPeriods(quotation.getTerm(), quotation.getFrequency()));
            calculationParameter.setPeriodicInterestRate(quotation.getInterestRate() / 100d);
            calculationParameter.setFrequency(quotation.getFrequency());

            GLFInsuranceExpenseCalculatorImpl calculator = new GLFInsuranceExpenseCalculatorImpl();
            InsuranceExpenseSchedules insuranceExpenseSchedules = calculator.getSchedules(quotation.getContractStartDate(), quotation.getFirstDueDate(), calculationParameter);

            InsuranceExpense insuranceExpense = new InsuranceExpense();
            insuranceExpense.setId(quotation.getId());
            insuranceExpense.setReference(quotation.getReference());
            insuranceExpense.setContractStartDate(quotation.getContractStartDate());
            insuranceExpense.setFirstInstallmentDate(quotation.getFirstDueDate());
            insuranceExpense.setFirstNameEn(quotation.getApplicant().getIndividual().getFirstNameEn());
            insuranceExpense.setLastNameEn(quotation.getApplicant().getIndividual().getLastNameEn());
            for (InsuranceExpenseSchedule insuranceExpenseSchedule : insuranceExpenseSchedules.getSchedules()) {
                if (startDate.compareTo(insuranceExpenseSchedule.getPeriodStartDate()) <= 0
                        && endDate.compareTo(insuranceExpenseSchedule.getPeriodEndDate()) >= 0) {
                    insuranceExpense.getInsuranceExpenseDistribution2().plus(new Amount(insuranceExpenseSchedule.getInsuranceExpenseDistribution2(), 0d, insuranceExpenseSchedule.getInsuranceExpenseDistribution2()));
                    insuranceExpense.getInsuranceExpenseDistribution3().plus(new Amount(insuranceExpenseSchedule.getInsuranceExpenseDistribution3(), 0d, insuranceExpenseSchedule.getInsuranceExpenseDistribution3()));
                    insuranceExpense.getCumulativeBalance().plus(new Amount(insuranceExpenseSchedule.getCumulativeBalance(), 0d, insuranceExpenseSchedule.getCumulativeBalance()));
                    insuranceExpense.getInsuranceExpensePaid().plus(new Amount(insuranceExpenseSchedule.getInsuranceExpensePaid(), 0d, insuranceExpenseSchedule.getInsuranceExpensePaid()));
                }
                if (insuranceExpenseSchedule.getPeriodEndDate().compareTo(endDate) >= 0
                        && insuranceExpense.getBalanceInsuranceExpense().getTiAmount() == null) {
                    insuranceExpense.setBalanceInsuranceExpense(new Amount(insuranceExpenseSchedule.getBalanceInsuranceExpense(), 0d, insuranceExpenseSchedule.getBalanceInsuranceExpense()));
                }
            }

            if (insuranceExpense.getRealInsuranceExpenseDistributed().getTiAmount() != null
                    && insuranceExpense.getRealInsuranceExpenseDistributed().getTiAmount() > 0d) {
                insuranceExpenses.add(insuranceExpense);
            }
        }
        return insuranceExpenses;
    }

    public List<RegistrationExpense> getRegistrationExpenses(EDealerType dealerType, Dealer dealer, String reference, Date startDate, Date endDate) {
        List<RegistrationExpense> registrationExpenses = new ArrayList<>();

        startDate = DateUtils.getDateAtBeginningOfDay(startDate);
        endDate = DateUtils.getDateAtBeginningOfDay(endDate);

        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.in("wkfStatus",
                new EWkfStatus[]{QuotationWkfStatus.ACT, QuotationWkfStatus.ACG, QuotationWkfStatus.RVG, QuotationWkfStatus.RCG, QuotationWkfStatus.LCG}));

        restrictions.addCriterion(Restrictions.le("contractStartDate", endDate));

        if (dealer != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        }

        if (StringUtils.isNotEmpty(reference)) {
            restrictions.addCriterion(Restrictions.ilike(REFERENCE, reference, MatchMode.ANYWHERE));
        }

        List<Quotation> quotations = list(restrictions);
        for (Quotation quotation : quotations) {

            // double registrationPlateNumberFee = quotation.getDealer().getRegistrationPlateNumberFee() == null ? 30d : quotation.getDealer().getRegistrationPlateNumberFee();

            double registrationPlateNumberFee = 30d;
            CalculationParameter calculationParameter = new CalculationParameter();
            calculationParameter.setInitialPrincipal(quotation.getTiFinanceAmount());
            calculationParameter.setNumberOfPeriods(LoanUtils.getNumberOfPeriods(quotation.getTerm(), quotation.getFrequency()));
            calculationParameter.setPeriodicInterestRate(quotation.getInterestRate() / 100d);
            calculationParameter.setFrequency(quotation.getFrequency());
            calculationParameter.setRegistrationFee(registrationPlateNumberFee);

            GLFRegistrationExpenseCalculatorImpl calculator = new GLFRegistrationExpenseCalculatorImpl();
            RegistrationExpenseSchedules registrationExpenseSchedules = calculator.getSchedules(quotation.getContractStartDate(), quotation.getFirstDueDate(), calculationParameter, registrationPlateNumberFee);

            RegistrationExpense registrationExpense = new RegistrationExpense();
            registrationExpense.setId(quotation.getId());
            registrationExpense.setReference(quotation.getReference());
            registrationExpense.setContractStartDate(quotation.getContractStartDate());
            registrationExpense.setFirstInstallmentDate(quotation.getFirstDueDate());
            registrationExpense.setFirstNameEn(quotation.getApplicant().getIndividual().getFirstNameEn());
            registrationExpense.setLastNameEn(quotation.getApplicant().getIndividual().getLastNameEn());
            for (RegistrationExpenseSchedule registrationExpenseSchedule : registrationExpenseSchedules.getSchedules()) {
                if (startDate.compareTo(registrationExpenseSchedule.getPeriodStartDate()) <= 0
                        && endDate.compareTo(registrationExpenseSchedule.getPeriodEndDate()) >= 0) {
                    registrationExpense.getRegistrationExpenseDistribution2().plus(new Amount(registrationExpenseSchedule.getRegistrationExpenseDistribution2(), 0d, registrationExpenseSchedule.getRegistrationExpenseDistribution2()));
                    registrationExpense.getRegistrationExpenseDistribution3().plus(new Amount(registrationExpenseSchedule.getRegistrationExpenseDistribution3(), 0d, registrationExpenseSchedule.getRegistrationExpenseDistribution3()));
                    registrationExpense.getCumulativeBalance().plus(new Amount(registrationExpenseSchedule.getCumulativeBalance(), 0d, registrationExpenseSchedule.getCumulativeBalance()));
                    registrationExpense.getRegistrationPlateNumberFee().plus(new Amount(registrationExpenseSchedule.getRegistrationPlateNumberFee(), 0d, registrationExpenseSchedule.getRegistrationPlateNumberFee()));
                }
                if (registrationExpenseSchedule.getPeriodEndDate().compareTo(endDate) >= 0
                        && registrationExpense.getBalanceRegistrationExpense().getTiAmount() == null) {
                    registrationExpense.setBalanceRegistrationExpense(new Amount(registrationExpenseSchedule.getBalanceRegistrationExpense(), 0d, registrationExpenseSchedule.getBalanceRegistrationExpense()));
                }
            }
            if (registrationExpense.getRealRegistrationExpenseDistributed().getTiAmount() != null
                    && registrationExpense.getRealRegistrationExpenseDistributed().getTiAmount() > 0d) {
                registrationExpenses.add(registrationExpense);
            }

        }
        return registrationExpenses;
    }

    @Override
    public List<ServicingIncome> getServicingIncomes(EDealerType dealerType, Dealer dealer, String reference, Date startDate, Date endDate) {
        List<ServicingIncome> servicingIncomes = new ArrayList<>();

        startDate = DateUtils.getDateAtBeginningOfDay(startDate);
        endDate = DateUtils.getDateAtBeginningOfDay(endDate);

        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.in("wkfStatus",
                new EWkfStatus[]{QuotationWkfStatus.ACT, QuotationWkfStatus.ACG, QuotationWkfStatus.RVG, QuotationWkfStatus.RCG, QuotationWkfStatus.LCG}));

        restrictions.addCriterion(Restrictions.le("contractStartDate", endDate));

        if (dealer != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        }

        if (StringUtils.isNotEmpty(reference)) {
            restrictions.addCriterion(Restrictions.ilike(REFERENCE, reference, MatchMode.ANYWHERE));
        }

        List<Quotation> quotations = list(restrictions);
        for (Quotation quotation : quotations) {

            QuotationService servicingService = quotation.getQuotationService("SERFEE");

            if (servicingService != null) {
                CalculationParameter calculationParameter = new CalculationParameter();
                calculationParameter.setInitialPrincipal(quotation.getTiFinanceAmount());
                calculationParameter.setNumberOfPeriods(LoanUtils.getNumberOfPeriods(quotation.getTerm(), quotation.getFrequency()));
                calculationParameter.setPeriodicInterestRate(quotation.getInterestRate() / 100d);
                calculationParameter.setFrequency(quotation.getFrequency());
                calculationParameter.setServicingFee(servicingService.getTiPrice());

                GLFServicingIncomeCalculatorImpl calculator = new GLFServicingIncomeCalculatorImpl();
                Long cotraId = quotation.getContract().getId();
                Map<Integer, Cashflow> servicingClashflow = getServicingCashflow(cotraId);
                ServicingIncomeSchedules servicingIncomeSchedules = calculator.getSchedules(quotation.getContractStartDate(), quotation.getFirstDueDate(), calculationParameter, servicingClashflow);
                System.out.println(servicingIncomeSchedules.toString());
                ServicingIncome servicingIncome = new ServicingIncome();
                servicingIncome.setId(quotation.getId());
                servicingIncome.setReference(quotation.getReference());
                servicingIncome.setContractStartDate(quotation.getContractStartDate());
                servicingIncome.setFirstInstallmentDate(quotation.getFirstDueDate());
                servicingIncome.setFirstNameEn(quotation.getApplicant().getIndividual().getFirstNameEn());
                servicingIncome.setLastNameEn(quotation.getApplicant().getIndividual().getLastNameEn());
                for (ServicingIncomeSchedule servicingIncomeSchedule : servicingIncomeSchedules.getSchedules()) {

                    Date calculEndDate = DateUtils.getDateAtEndOfMonth(endDate);
                    Date calculStartDate = DateUtils.getDateAtBeginningOfMonth(startDate);
                    if (calculStartDate.compareTo(servicingIncomeSchedule.getPeriodStartDate()) <= 0
                            && calculEndDate.compareTo(servicingIncomeSchedule.getPeriodEndDate()) >= 0) {
//					if (startDate.compareTo(servicingIncomeSchedule.getPeriodStartDate()) <= 0
//							&& endDate.compareTo(servicingIncomeSchedule.getPeriodEndDate()) >= 0) {
                        servicingIncome.getServicingIncomeDistribution2().plus(new Amount(servicingIncomeSchedule.getServicingIncomeDistribution2(), 0d, servicingIncomeSchedule.getServicingIncomeDistribution2()));
                        servicingIncome.getServicingIncomeDistribution3().plus(new Amount(servicingIncomeSchedule.getServicingIncomeDistribution3(), 0d, servicingIncomeSchedule.getServicingIncomeDistribution3()));
                        servicingIncome.getCumulativeBalance().plus(new Amount(servicingIncomeSchedule.getCumulativeBalance(), 0d, servicingIncomeSchedule.getCumulativeBalance()));
                        servicingIncome.getAccountReceivable().plus(new Amount(servicingIncomeSchedule.getAccountReceivable(), 0d, servicingIncomeSchedule.getAccountReceivable()));
                        servicingIncome.getServicingIncomeReceived().plus(new Amount(servicingIncomeSchedule.getServicingIncomeReceived(), 0d, servicingIncomeSchedule.getServicingIncomeReceived()));


                        //Insurance income calculate as daily base
                        Date calculEndDate2 = DateUtils.addDaysDate(servicingIncomeSchedule.getPeriodEndDate(), 1);
                        Date calculStartDate2 = servicingIncomeSchedule.getPeriodStartDate();
                        if (DateUtils.isBeforeDay(calculStartDate2, startDate)) {
                            calculStartDate2 = startDate;
                        }
                        if (DateUtils.isBeforeDay(endDate, calculEndDate2)) {
                            calculEndDate2 = endDate;
                        }

                        long coeff = DateUtils.getDiffInDaysPlusOneDay(servicingIncomeSchedule.getPeriodEndDate(), servicingIncomeSchedule.getPeriodStartDate()); // DateUtils.getDiffInDaysPlusOneDay(periodEndDate, periodStartDate);
                        long nbDays = 0;
                        if (DateUtils.isAfterDay(servicingIncomeSchedule.getPeriodEndDate(), calculEndDate2)) {
                            nbDays = DateUtils.getDiffInDaysPlusOneDay(calculEndDate2, calculStartDate2);
                        } else {
                            nbDays = coeff - DateUtils.getDiffInDays(calculStartDate2, servicingIncomeSchedule.getPeriodStartDate());
                        }

                        logger.info("nbDays - [" + nbDays + "]");
                        logger.info("coeff - [" + coeff + "]");

                        double realServicingIncomeDistributedInNDays = 0d;
                        double servicingIncomeInSuspendInNDays = 0d;

                        realServicingIncomeDistributedInNDays = MyMathUtils.roundAmountTo((servicingIncomeSchedule.getRealServicingIncomeDistributed() / coeff) * nbDays);
                        servicingIncomeInSuspendInNDays = MyMathUtils.roundAmountTo((servicingIncomeSchedule.getServicingIncomeInSuspend() / coeff) * nbDays);

                        servicingIncome.getServicingIncomeInSuspend().plus(new Amount(servicingIncomeInSuspendInNDays, 0d, servicingIncomeInSuspendInNDays));
                        servicingIncome.getServicingIncomeInSuspendCumulated().plus(new Amount(servicingIncomeSchedule.getServicingIncomeInSuspendCumulated(), 0d, servicingIncomeSchedule.getServicingIncomeInSuspendCumulated()));
                        servicingIncome.getRealServicingIncomeDistributed().plus(new Amount(realServicingIncomeDistributedInNDays, 0d, realServicingIncomeDistributedInNDays));

                    }

                    if (servicingIncomeSchedule.getPeriodEndDate().compareTo(endDate) >= 0
                            && servicingIncome.getUnearnedServicingIncome().getTiAmount() == null) {
                        servicingIncome.setUnearnedServicingIncome(new Amount(servicingIncomeSchedule.getUnearnedServicingIncome(), 0d, servicingIncomeSchedule.getUnearnedServicingIncome()));
                    }
                }
                servicingIncomes.add(servicingIncome);
                /*if (servicingIncome.getRealServicingIncomeDistributed().getTiAmountUsd() != null
                        && servicingIncome.getRealServicingIncomeDistributed().getTiAmountUsd() > 0d) {
					servicingIncomes.add(servicingIncome);
				}*/
            }
        }

        return servicingIncomes;
    }

    private List<Cashflow> getCashflowsNoCancel(Contract contract) {
        BaseRestrictions<Cashflow> restrictions = new BaseRestrictions<>(Cashflow.class);
        restrictions.addCriterion(Restrictions.eq("contract", contract));
        restrictions.addCriterion(Restrictions.eq(CANCEL, false));
        List<Object> cashflowTypes =  Arrays.asList(new Object[]{
                                        ECashflowType.CAP,
                                        ECashflowType.PEN,
                                        ECashflowType.IAP});
        restrictions.addCriterion(Restrictions.in(CASHFLOW_TYPE, cashflowTypes));
        restrictions.addOrder(Order.asc(NUM_INSTALLMENT));

        List<Cashflow> cashflows = ENTITY_SRV.list(restrictions);

        return cashflows;
    }

    /**
     * @param cotraId
     * @return
     */
    private List<Cashflow> getFeesAndDirectCosts(Long cotraId) {
        List<Cashflow> cashflows = new ArrayList<>();
        String query =
                "SELECT "
                        + " c.cfw_id, "
                        + " c.catyp_code, "
                        + " c.cfw_dt_installment, "
                        + " c.cfw_bl_cancel, "
                        + " c.cfw_bl_paid, "
                        + " c.cfw_am_te_installment_usd, "
                        + " c.cfw_am_vat_installment_usd, "
                        + " c.cfw_am_ti_installment_usd, "
                        + " c.cfw_nu_num_installment, "
                        + " s.servi_id, "
                        + " s.servi_code, "
                        + " s.servi_desc_en, "
                        + " s.setyp_code, "
                        + " c.pay_id, "
                        + " p.pay_dt_payment"
                        + " FROM td_cashflow c"
                        + " inner join tu_service s on c.fin_srv_id = s.fin_srv_id"
                        + " left join td_payment p on p.pay_id = c.pay_id"
                        + " WHERE c.cotra_id = " + cotraId
                        + " AND (c.cfw_bl_cancel is false or (c.cacode_code is not null and c.cfw_bl_paid is false))"
                        + " AND c.cfw_typ_id in(4, 7)"
                        + " ORDER BY c.cfw_nu_num_installment asc";

        try {
            List<NativeRow> cashflowRows = executeSQLNativeQuery(query);
            for (NativeRow row : cashflowRows) {
                List<NativeColumn> columns = row.getColumns();
                int i = 0;
                Cashflow cashflow = new Cashflow();
                cashflow.setId((Long) columns.get(i++).getValue());
                cashflow.setCashflowType(ECashflowType.getByCode(columns.get(i++).getValue().toString()));
                cashflow.setInstallmentDate((Date) columns.get(i++).getValue());
                cashflow.setCancel((Boolean) columns.get(i++).getValue());
                cashflow.setPaid((Boolean) columns.get(i++).getValue());
                cashflow.setTeInstallmentAmount(Math.abs((Double) columns.get(i++).getValue()));
                cashflow.setVatInstallmentAmount(Math.abs((Double) columns.get(i++).getValue()));
                cashflow.setTiInstallmentAmount(Math.abs((Double) columns.get(i++).getValue()));
                cashflow.setNumInstallment((Integer) columns.get(i++).getValue());
                Long serviId = (Long) columns.get(i++).getValue();
                if (serviId != null && serviId.longValue() > 0) {
                    com.soma.mfinance.core.financial.model.FinService service = new com.soma.mfinance.core.financial.model.FinService();
                    service.setId(serviId);
                    service.setCode((String) columns.get(i++).getValue());
                    service.setDescEn((String) columns.get(i++).getValue());
                    String setypCode = (String) columns.get(i++).getValue();
                    service.setServiceType(EServiceType.getByCode(setypCode));
                    cashflow.setService(service);
                }
                Long paymnId = (Long) columns.get(i++).getValue();
                if (paymnId != null && paymnId.longValue() > 0) {
                    Payment payment = new Payment();
                    payment.setId(paymnId);
                    payment.setPaymentDate((Date) columns.get(i++).getValue());
                    cashflow.setPayment(payment);
                }
                cashflows.add(cashflow);
            }
        } catch (NativeQueryException e) {
            logger.error(e.getMessage(), e);
        }

        return cashflows;
    }

    private Map<Integer, Cashflow> getInsuranceCashflow(Long cotraId) {
        Map<Integer, Cashflow> mapCashflow = new HashMap<Integer, Cashflow>();
        List<Cashflow> cashflows = getServiceCashflowByContractId(cotraId);
        for (Cashflow cashflow : cashflows) {
            if (cashflow.getService() != null && ServiceEntityField.INSFEE.equals(cashflow.getService().getCode())) {
                mapCashflow.put(cashflow.getNumInstallment(), cashflow);
            }
        }
        return mapCashflow;
    }

    private Map<Integer, Cashflow> getServicingCashflow(Long cotraId) {
        Map<Integer, Cashflow> mapCashflow = new HashMap<Integer, Cashflow>();
        List<Cashflow> cashflows = getServiceCashflowByContractId(cotraId);
        for (Cashflow cashflow : cashflows) {
            if (cashflow.getService() != null && ServiceEntityField.SERFEE.equals(cashflow.getService().getCode())) {
                mapCashflow.put(cashflow.getNumInstallment(), cashflow);
            }
        }
        return mapCashflow;
    }

    private List<Cashflow> getServiceCashflowByContractId(Long cotraId) {
        BaseRestrictions<Cashflow> restrictions = new BaseRestrictions<Cashflow>(Cashflow.class);
        restrictions.addCriterion(Restrictions.eq(CASHFLOW_TYPE, ECashflowType.FEE));
        restrictions.addCriterion(Restrictions.eq(CANCEL, Boolean.FALSE));
        restrictions.addCriterion(Restrictions.eq(CONTRACT + "." + ID, cotraId));
        restrictions.addOrder(Order.asc(NUM_INSTALLMENT));
        List<Cashflow> cashflows = list(restrictions);
        return cashflows;
    }

    protected List<Contract> getRemainingBalanceContracts(EDealerType dealerType, Dealer dealer, String reference, Date endDate) {
        List<Contract> contracts = new ArrayList<>();
        String query =
                "SELECT "
                        + " c.con_id, "
                        + " c.con_va_reference, "
                        + " c.con_dt_start, "
                        + " c.con_dt_first_due, "
                        + " c.con_am_ti_financed_amount, "
                        + " c.con_nu_term, "
                        + " c.fre_id, "
                        + " c.con_rt_interest_rate, "
                        + " c.con_rt_irr_rate, "
                        + " c.wkf_sta_id, "
                        + " appl.app_id, "
                        + " ind.per_lastname_en, "
                        + " ind.per_firstname_en "
                        + " FROM td_contract c"
                        + " inner join ts_wkf_status wkf on wkf.wkf_sta_id = c.wkf_sta_id"
                        + " inner join tu_dealer dea on dea.dea_id = c.dea_id"
                        + " inner join td_contract_applicant capp on capp.con_id = c.con_id and capp.app_typ_id = '1'"
                        + " inner join td_applicant appl on appl.app_id = c.app_id"
                        + " inner join td_individual ind on ind.ind_id = appl.ind_id"
                        + " WHERE wkf.ref_desc_en not in ('" + ContractWkfStatus.PEN + "')"
                        + " AND c.con_dt_start <= '" + DateUtils.getDateLabel(DateUtils.getDateAtEndOfDay(endDate), "yyyy-MM-dd HH:mm:ss") + "'";

        if (dealerType != null) {
            query += " AND dea.detyp_code = '" + dealerType + "'";
        }

        if (dealer != null) {
            query += " AND dea.dea_id = " + dealer.getId();
        }

        if (StringUtils.isNotEmpty(reference)) {
            query += " AND c.con_va_reference ilike '%" + reference + "%'";
        }

        query += " ORDER BY c.con_dt_start desc";

        try {
            List<NativeRow> contractRows = executeSQLNativeQuery(query);
            for (NativeRow row : contractRows) {
                List<NativeColumn> columns = row.getColumns();
                int i = 0;
                Contract contract = new Contract();
                contract.setId((Long) columns.get(i++).getValue());
                contract.setReference((String) columns.get(i++).getValue());
                contract.setStartDate((Date) columns.get(i++).getValue());
                contract.setFirstDueDate((Date) columns.get(i++).getValue());
                contract.setTiFinancedAmount((Double) columns.get(i++).getValue());
                contract.setTerm((Integer) columns.get(i++).getValue());
                contract.setFrequency(EFrequency.getById((Long) columns.get(i++).getValue()));
                contract.setInterestRate((Double) columns.get(i++).getValue());
                contract.setIrrRate((Double) columns.get(i++).getValue());
                contract.setWkfStatus(EWkfStatus.getById((Long) columns.get(i++).getValue()));

                List<ContractApplicant> contractApplicants = new ArrayList<>();

                Applicant applicant = new Applicant();
                applicant.setId((Long) columns.get(i++).getValue());

                Individual individual = new Individual();
                individual.setLastNameEn((String) columns.get(i++).getValue());
                individual.setFirstNameEn((String) columns.get(i++).getValue());

                applicant.setIndividual(individual);

                ContractApplicant contractApplicant = new ContractApplicant();
                contractApplicant.setApplicant(applicant);
                contractApplicant.setContract(contract);
                contractApplicant.setApplicantType(EApplicantType.C);

                contractApplicants.add(contractApplicant);
                contract.setContractApplicants(contractApplicants);

                contract.setApplicant(applicant);
                contract.getApplicant().setIndividual(individual);

                contracts.add(contract);
            }
        } catch (NativeQueryException e) {
            logger.error(e.getMessage(), e);
        }
        return contracts;
    }

    protected List<Contract> getLeaseTransactionContracts(EDealerType dealerType, Dealer dealer, String reference, Date endDate) {
        BaseRestrictions<Contract> restrictions = new BaseRestrictions<>(Contract.class);
        restrictions.addCriterion(Restrictions.ne(WKF_STATUS, ContractWkfStatus.WTD));
        if (StringUtils.isNotEmpty(reference)) {
            restrictions.addCriterion(Restrictions.like(REFERENCE, reference, MatchMode.ANYWHERE));
        }
        if (dealerType != null) {
            restrictions.addAssociation("dealer", "quodeal", JoinType.INNER_JOIN);
            restrictions.addCriterion(Restrictions.eq("quodeal.dealerType", dealerType));
        }
        if (dealer != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        }
        if (endDate != null) {
            restrictions.addCriterion(Restrictions.le(START_DATE, endDate));
        }
        restrictions.addOrder(Order.desc(START_DATE));

        List<Contract> contracts = ENTITY_SRV.list(restrictions);
        return contracts;
    }

    @Override
    public List<LeaseTransaction> getRemainingBalance(EDealerType dealerType, Dealer dealer, String reference, Date endDate) {
        List<LeaseTransaction> leaseTransactions = new ArrayList<>();
        List<Contract> contracts = getRemainingBalanceContracts(dealerType, dealer, reference, endDate);

        int totalContracts = contracts.size();
        int i = 1;

        for (Contract contract : contracts) {

            if (i % 100 == 0 || i == contracts.size()) {
                System.out.println(i + "/" + totalContracts);
            }
            i++;

            List<Cashflow> cashflows = getCashflowsNoCancel(contract);
            LeaseTransaction leaseTransaction = new LeaseTransaction();
            leaseTransaction.setId(contract.getId());
            leaseTransaction.setReference(contract.getReference());
            leaseTransaction.setContractStartDate(contract.getStartDate());
            leaseTransaction.setFirstInstallmentDate(contract.getFirstDueDate());

            if (contract.getApplicant() != null && contract.getApplicant().getIndividual() != null) {
                leaseTransaction.setFirstNameEn(contract.getApplicant().getIndividual().getFirstNameEn());
                leaseTransaction.setLastNameEn(contract.getApplicant().getIndividual().getLastNameEn());
            }

            leaseTransaction.setInterestRate(contract.getInterestRate());
            leaseTransaction.setIrrRate(MyNumberUtils.getDouble(contract.getIrrRate()) * 100);

            CalculationParameter calculationParameter = new CalculationParameter();
            calculationParameter.setInitialPrincipal(contract.getTiFinancedAmount());
            if (contract.getTerm() != null && contract.getFrequency() != null) {
                calculationParameter.setNumberOfPeriods(LoanUtils.getNumberOfPeriods(contract.getTerm(), contract.getFrequency()));
            }

            calculationParameter.setPeriodicInterestRate(contract.getInterestRate() / 100d);
            calculationParameter.setFrequency(contract.getFrequency());

            Amount principalBalance = getAccountingPrincipalBalance(endDate, cashflows);
            Amount unearnedInterestBalance = getAccountingInterestUnearnedBalance(endDate, cashflows);
            leaseTransaction.getPrincipalBalance().plus(principalBalance);
            leaseTransaction.getUnearnedInterestBalance().plus(unearnedInterestBalance);
            leaseTransactions.add(leaseTransaction);
        }
        return leaseTransactions;
    }

    @Override
    public List<LeaseTransaction> getLeaseTransactions(EDealerType dealerType, Dealer dealer,
                                                       String reference, Date startDate, Date endDate) {
        List<LeaseTransaction> leaseTransactions = new ArrayList<>();
        startDate = DateUtils.getDateAtBeginningOfDay(startDate);
        endDate = DateUtils.getDateAtEndOfDay(endDate);
        List<Contract> contracts = getLeaseTransactionContracts(dealerType, dealer, reference, endDate);
        for (Contract contract : contracts) {
            List<Cashflow> cashflows = getCashflowsNoCancel(contract);
            CalculationParameter calculationParameter = new CalculationParameter();
            calculationParameter.setInitialPrincipal(contract.getTiFinancedAmount());
            calculationParameter.setNumberOfPeriods(LoanUtils.getNumberOfPeriods(contract.getTerm(), contract.getFrequency()));
            calculationParameter.setPeriodicInterestRate(contract.getInterestRate() / 100d);
            calculationParameter.setFrequency(contract.getFrequency());
            leaseTransactions.add(getLeaseTransaction(contract, cashflows, startDate, endDate, calculationParameter, null));
        }
        return leaseTransactions;
    }


    /**
     * @param dealerType
     * @param dealer
     * @param reference
     * @param startDate
     * @param endDate
     * @return
     */
    public List<LeaseTransaction> getNetLeasings(EDealerType dealerType, Dealer dealer,
                                                 String reference, Date startDate, Date endDate) {
        List<LeaseTransaction> leaseTransactions = new ArrayList<>();
        startDate = DateUtils.getDateAtBeginningOfDay(startDate);
        endDate = DateUtils.getDateAtEndOfDay(endDate);
        List<Contract> contracts = getLeaseTransactionContracts(dealerType, dealer, reference, endDate);
        int totalContracts = contracts.size();
        int i = 1;
        for (Contract contract : contracts) {
            if (i % 100 == 0 || i == contracts.size()) {
                logger.debug(i + "/" + totalContracts);
            }
            i++;
            List<Cashflow> feesAndDirectCosts = getFeesAndDirectCosts(contract.getId());
            double feesAndDirectCostsAmountUsd = 0d;
            for (Cashflow cashflow : feesAndDirectCosts) {
                if (EServiceType.listDirectCosts().contains(cashflow.getService().getServiceType())) {
                    feesAndDirectCostsAmountUsd += Math.abs(cashflow.getTiInstallmentAmount());
                } else {
                    feesAndDirectCostsAmountUsd -= Math.abs(cashflow.getTiInstallmentAmount());
                }
            }
            List<Cashflow> cashflows = getCashflowsNoCancel(contract);

            CalculationParameter calculationParameter = new CalculationParameter();
            calculationParameter.setInitialPrincipal(contract.getTiFinancedAmount());
            calculationParameter.setNumberOfPeriods(LoanUtils.getNumberOfPeriods(contract.getTerm(), contract.getFrequency()));
            calculationParameter.setPeriodicInterestRate(contract.getInterestRate() / 100d);
            calculationParameter.setFrequency(contract.getFrequency());
            calculationParameter.setNumberOfPrincipalGracePeriods(MyNumberUtils.getInteger(contract.getNumberOfPrincipalGracePeriods()));

            CalculationParameter calculationParameter2 = new CalculationParameter();
            calculationParameter2.setInitialPrincipal(contract.getTiFinancedAmount() + feesAndDirectCostsAmountUsd);
            calculationParameter2.setNumberOfPeriods(LoanUtils.getNumberOfPeriods(contract.getTerm(), contract.getFrequency()));
            calculationParameter2.setPeriodicInterestRate(contract.getInterestRate() / 100d);
            calculationParameter2.setFrequency(contract.getFrequency());
            calculationParameter2.setNumberOfPrincipalGracePeriods(MyNumberUtils.getInteger(contract.getNumberOfPrincipalGracePeriods()));

            AmortizationSchedules amortizationSchedules = financeCalculationService.getAmortizationSchedules(contract.getStartDate(),
                    contract.getFirstDueDate(), calculationParameter2);
            List<Schedule> schedules = amortizationSchedules.getSchedules();
            for (Cashflow cashflow : cashflows) {
                for (Schedule schedule : schedules) {
                    if (DateUtils.isSameDay(cashflow.getInstallmentDate(), schedule.getInstallmentDate())) {
                        if (cashflow.getCashflowType().equals(ECashflowType.CAP)) {
                            cashflow.setTiInstallmentAmount(schedule.getPrincipalAmount());
                            cashflow.setTeInstallmentAmount(cashflow.getTiInstallmentAmount());
                        } else if (cashflow.getCashflowType().equals(ECashflowType.IAP)) {
                            cashflow.setTiInstallmentAmount(schedule.getInterestAmount());
                            cashflow.setTeInstallmentAmount(cashflow.getTiInstallmentAmount());
                        }
                        break;
                    }
                }
            }

            leaseTransactions.add(getLeaseTransaction(contract, cashflows, startDate, endDate, calculationParameter, calculationParameter2));
        }
        return leaseTransactions;
    }

    public List<ServiceTransaction> getServiceTransactions(EDealerType dealerType, Dealer dealer,
                                                           String reference, Date startDate, Date endDate) {
        List<ServiceTransaction> serviceTransactions = new ArrayList<>();
        List<Contract> contracts = getLeaseTransactionContracts(dealerType, dealer, reference, endDate);
        for (Contract contract : contracts) {


            List<Cashflow> feesAndDirectCosts = getFeesAndDirectCosts(contract.getId());
            double feesAndDirectCostsAmountUsd = 0d;
            for (Cashflow cashflow : feesAndDirectCosts) {
                if (EServiceType.listDirectCosts().contains(cashflow.getService().getServiceType())) {
                    feesAndDirectCostsAmountUsd += Math.abs(cashflow.getTiInstallmentAmount());
                } else {
                    feesAndDirectCostsAmountUsd -= Math.abs(cashflow.getTiInstallmentAmount());
                }
            }
            List<Cashflow> cashflows = getCashflowsNoCancel(contract);

            CalculationParameter calculationParameter = new CalculationParameter();
            calculationParameter.setInitialPrincipal(contract.getTiFinancedAmount());
            calculationParameter.setNumberOfPeriods(LoanUtils.getNumberOfPeriods(contract.getTerm(), contract.getFrequency()));
            calculationParameter.setPeriodicInterestRate(contract.getInterestRate() / 100d);
            calculationParameter.setFrequency(contract.getFrequency());
            calculationParameter.setNumberOfPrincipalGracePeriods(MyNumberUtils.getInteger(contract.getNumberOfPrincipalGracePeriods()));

            CalculationParameter calculationParameter2 = new CalculationParameter();
            calculationParameter2.setInitialPrincipal(contract.getTiFinancedAmount() + feesAndDirectCostsAmountUsd);
            calculationParameter2.setNumberOfPeriods(LoanUtils.getNumberOfPeriods(contract.getTerm(), contract.getFrequency()));
            calculationParameter2.setPeriodicInterestRate(contract.getInterestRate() / 100d);
            calculationParameter2.setFrequency(contract.getFrequency());
            calculationParameter2.setNumberOfPrincipalGracePeriods(MyNumberUtils.getInteger(contract.getNumberOfPrincipalGracePeriods()));


            Date eventDate = null;
            if (contract.getWkfStatus() != ContractWkfStatus.FIN) {
                eventDate = getEventDate(contract.getHistories(), contract.getWkfStatus());
            }
            Date lastInstallmentDate = getLastInstallmentDate(cashflows);
            Date calculStartDate = startDate;
            Date calculEndDate = endDate;

            if (calculStartDate.before(contract.getStartDate())) {
                calculStartDate = contract.getStartDate();
            }
            if (calculEndDate.after(lastInstallmentDate)) {
                calculEndDate = lastInstallmentDate;
            }

            Map<String, ServiceCalculation> services = getServices(feesAndDirectCosts);

            for (Iterator<String> iter = services.keySet().iterator(); iter.hasNext(); ) {

                ServiceCalculation serviceCalculation = services.get(iter.next());

                ServiceTransaction serviceTransaction = new ServiceTransaction();
                serviceTransaction.setId(contract.getId() * serviceCalculation.getService().getId());
                serviceTransaction.setReference(contract.getReference());
                serviceTransaction.setContractStartDate(contract.getStartDate());
                serviceTransaction.setFirstInstallmentDate(contract.getFirstDueDate());
                serviceTransaction.setServiceDescEn(serviceCalculation.getService().getDescEn());

                double serviceAmountUsd = serviceCalculation.getServiceAmountUsd();

                System.out.println(serviceCalculation.getService().getCode() + " : " + serviceAmountUsd);

                GLFServiceDailyCalculatorImpl calculator = new GLFServiceDailyCalculatorImpl();
                ServiceIncomeSchedules schedules2 = calculator.getSchedules(contract, DateUtils.getDateAtBeginningOfDay(contract.getStartDate()),
                        DateUtils.getDateAtBeginningOfDay(contract.getFirstDueDate()), serviceAmountUsd, calculationParameter, calculationParameter2,
                        serviceCalculation.getCashflows(), DateUtils.getDateAtBeginningOfDay(calculStartDate), DateUtils.getDateAtBeginningOfDay(calculEndDate),
                        contract.getWkfStatus(), eventDate);

                System.out.println(schedules2);

                Amount revenue = new Amount(0d, 0d, 0d, 8);
                Amount accruedIncome = new Amount(0d, 0d, 0d, 8);
                Amount principalRepayment = new Amount(0d, 0d, 0d);

                for (int j = 0; j < schedules2.getSchedules().size(); j++) {
                    ServiceIncomeSchedule dailySchedule = schedules2.getSchedules().get(j);
                    if (DateUtils.getDateAtBeginningOfDay(startDate).compareTo(DateUtils.getDateAtBeginningOfDay(dailySchedule.getPeriodStartDate())) <= 0
                            && DateUtils.getDateAtBeginningOfDay(endDate).compareTo(DateUtils.getDateAtBeginningOfDay(dailySchedule.getPeriodEndDate())) >= 0) {

                        double principal = dailySchedule.getPrincipalRepayment();
                        principalRepayment.plus(new Amount(principal, 0d, principal));
                        revenue.plus(new Amount(dailySchedule.getRevenue(), 0d, dailySchedule.getRevenue()));
                        if (!dailySchedule.isContractInOverdueMoreThanOneMonth() && DateUtils.isSameDay(dailySchedule.getInstallmentDate(), lastInstallmentDate)) {
                            revenue.plus(new Amount(-1 * dailySchedule.getAccruedIncome(), 0d, -1 * dailySchedule.getAccruedIncome()));
                            accruedIncome = new Amount(0d, 0d, 0d, 8);
                        } else {
                            accruedIncome = new Amount(dailySchedule.getAccruedIncome(), 0d, dailySchedule.getAccruedIncome(), 8);
                        }
                    }
                }
                serviceTransaction.setRevenue(revenue);
                serviceTransaction.setPrincipalRepayment(principalRepayment);
                serviceTransaction.setAccruedIncome(accruedIncome);
                if (eventDate == null || endDate.compareTo(eventDate) < 0) {
                    serviceTransaction.getPrincipalBalance().plus(getAccountingServiceBalance(endDate, serviceCalculation.getCashflows()));
                } else {
                    serviceTransaction.getPrincipalBalance().plus(new Amount(0d, 0d, 0d));
                }

                serviceTransactions.add(serviceTransaction);
            }
        }

        return serviceTransactions;
    }

    private Map<String, ServiceCalculation> getServices(List<Cashflow> feesAndDirectCosts) {
        Map<String, ServiceCalculation> servicesMap = new HashMap<>();
        for (Cashflow cashflow : feesAndDirectCosts) {
            ServiceCalculation serviceCalculation = servicesMap.get(cashflow.getService().getCode());
            if (serviceCalculation == null) {
                serviceCalculation = new ServiceCalculation();
                serviceCalculation.setService(cashflow.getService());
                serviceCalculation.setCashflows(new ArrayList<Cashflow>());
                servicesMap.put(cashflow.getService().getCode(), serviceCalculation);
            }
            serviceCalculation.getCashflows().add(cashflow);
            serviceCalculation.setServiceAmountUsd(serviceCalculation.getServiceAmountUsd() + Math.abs(cashflow.getTiInstallmentAmount()));
        }

        return servicesMap;
    }

    private LeaseTransaction getLeaseTransaction(Contract contract, List<Cashflow> cashflows, Date startDate, Date endDate,
                                                 CalculationParameter calculationParameter, CalculationParameter calculationParameter2) {
        LeaseTransaction leaseTransaction = new LeaseTransaction();
        leaseTransaction.setId(contract.getId());
        leaseTransaction.setReference(contract.getReference());
        leaseTransaction.setContractStartDate(contract.getStartDate());
        leaseTransaction.setFirstInstallmentDate(contract.getFirstDueDate());
        leaseTransaction.setInterestRate(contract.getInterestRate());
        leaseTransaction.setIrrRate(contract.getIrrRate());

        leaseTransaction.setContractStatus(contract.getWkfStatus());
        leaseTransaction.setLastPaidInstallmentDate(contract.getLastPaidDateInstallment());
        leaseTransaction.setLastPaidNumInstallment(contract.getLastPaidNumInstallment());
        leaseTransaction.setContractEndDate(contract.getEndDate());
        leaseTransaction.setTerm(contract.getTerm());


        Date lastInstallmentDate = contract.getEndDate();

        Date calculStartDate = startDate;
        Date calculEndDate = endDate;
        Date lastPaidDate = contract.getLastPaidDateInstallment();

        if (calculStartDate.before(contract.getStartDate())) {
            calculStartDate = contract.getStartDate();
        }

        if(lastInstallmentDate!=null){
            if (calculEndDate.after(lastInstallmentDate)) {
                calculEndDate = lastInstallmentDate;
            }
        }

        if(lastPaidDate == null) {
            lastPaidDate = DateUtils.getDateAtBeginningOfDay(contract.getFirstDueDate());
        }

        //Contract Change Date
        Date eventDate = null;
        if (contract.getWkfStatus() != ContractWkfStatus.FIN) {
            eventDate = getEventDate(contract.getHistories(), contract.getWkfStatus());
        }

        leaseTransaction.setEventDate(eventDate);

        //To fix when Contract Wkf History is not worked.
        if(eventDate == null) {
            eventDate = contract.getLastPaidDateInstallment();
        }


        //TODO: Sreyrath's code
        boolean isLossContractStatus = contract.getWkfStatus().equals(ContractWkfStatus.ACC)
                                    || contract.getWkfStatus().equals(ContractWkfStatus.THE)
                                    || contract.getWkfStatus().equals(ContractWkfStatus.REP);
        Amount interestRevenue = new Amount(0d, 0d, 0d, 2);
        Amount interestIncomeReceivable = new Amount(0d, 0d, 0d, 2);
        Date prevMonth = null, dueDateOfTransaction = null;
        double totalReceivePrev = 0, totalReceiveNext = 0;
        double intInOneDayPrev = 0, intIncomePrev = 0;
        int numOfDayInNextMonth = 0;
        int lastPaidNumInstallment = contract.getLastPaidNumInstallment();


        //TODO: Early Paid-OFF Contract
        if(contract.getWkfStatus().equals(ContractWkfStatus.EAR)) {
            int numInstallmentForPaidOff = contract.getPenaltyTermPayoff();
            int prevNumInstallment = contract.getPreviousPaidNumInstallment();
            for (int i = prevNumInstallment; i <= contract.getTerm(); i++) {
                List<InstallmentVO> installmentByNumInstallments = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(contract, i);
                if(eventDate.after(calculStartDate)) {
                    for (InstallmentVO installmentVO : installmentByNumInstallments) {
                        if (installmentVO.getCashflowType().equals(ECashflowType.IAP)) {
                            if (i == prevNumInstallment) {
                                prevMonth = installmentVO.getInstallmentDate();
                            } else if (numInstallmentForPaidOff >= i) {
                                totalReceivePrev += installmentVO.getTiamount();

                            } else if (i == prevNumInstallment + 1) {
                                long nbDiff = DateUtils.getDiffInDays(eventDate, calculStartDate);
                                long nbDaysOfMonth = DateUtils.getNbDaysInMonth(prevMonth);

                                if (nbDiff <= 31)
                                    totalReceivePrev = MyMathUtils.roundAmountTo(nbDiff * (installmentVO.getTiamount() / nbDaysOfMonth));
                            }
                        }
                    }
                }
            }
        }else if((isLossContractStatus || contract.getWkfStatus().equals(ContractWkfStatus.CLO)) && eventDate.before(calculStartDate)) {
            //TODO: All isLossContractStatus Or Maturity Paid-off and Change Status date before Transaction Start Date

            interestIncomeReceivable = new Amount(0d, 0d, 0d, 2);
            interestRevenue = new Amount(0d, 0d, 0d, 2);
        } else if(lastPaidDate.before(calculStartDate)) {
            //TODO: Get Num Installment In Overdue
            int numInsInOverdue = DateUtils.getNumberMonthOfTwoDates(calculStartDate,lastPaidDate) - 1;
            int totalNumInstallmentInOverdue =  numInsInOverdue + lastPaidNumInstallment;
            int j = 1;
            if(totalNumInstallmentInOverdue > contract.getTerm())
                j = contract.getTerm();
            else
                j = totalNumInstallmentInOverdue +1;

            for (int i = lastPaidNumInstallment+1; i <= j; i++) {
                List<InstallmentVO> installmentVOs = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(contract, i);

                try {
                    for(InstallmentVO installmentVO : installmentVOs) {
                        if (installmentVO.getCashflowType().equals(ECashflowType.IAP)) {
                            //AIR
                            if(i > lastPaidNumInstallment && i <= totalNumInstallmentInOverdue) {
                                if(i== contract.getTerm())
                                    calculEndDate = installmentVO.getInstallmentDate();

                                if(calculEndDate.after(installmentVO.getInstallmentDate()) || installmentVO.getInstallmentDate() == calculEndDate)
                                    intIncomePrev += installmentVO.getTiamount();


                                if(i == totalNumInstallmentInOverdue) {
                                    long totalNumPrev;

                                    //isLossContractStatus: Repossess, Thief and Accidence
                                    if (isLossContractStatus)
                                        totalNumPrev = DateUtils.getDiffInDays(leaseTransaction.getEventDate(), calculStartDate);
                                    else
                                        totalNumPrev = DateUtils.getDiffInDays(installmentVO.getInstallmentDate(), calculStartDate) + 1;

                                    int numOfDayInPrev = DateUtils.getNbDaysInMonth(DateUtils.addMonthsDate(installmentVO.getInstallmentDate(), -1));
                                    intInOneDayPrev = installmentVO.getTiamount() / numOfDayInPrev;
                                    totalReceivePrev = intInOneDayPrev * totalNumPrev;
                                }
                            } else if(i == totalNumInstallmentInOverdue+1) {
                                dueDateOfTransaction = DateUtils.addMonthsDate(installmentVO.getInstallmentDate(), -1);

                                numOfDayInNextMonth = DateUtils.getNbDaysInMonth(dueDateOfTransaction);
                                double intInOneDayNext = installmentVO.getTiamount() / numOfDayInNextMonth;
                                long totalNumNext = DateUtils.getDiffInDays(calculEndDate, dueDateOfTransaction) - 1;

                                if(isLossContractStatus){
                                    totalReceiveNext = 0d;
                                } else {
                                    totalReceiveNext = intInOneDayNext * totalNumNext;
                                }
                            }

                            if(leaseTransaction.getEventDate() != null && leaseTransaction.getEventDate().before(calculEndDate)) {
                                intIncomePrev = 0d;
                                totalReceiveNext = 0d;
                            }
                        }
                    }
                }catch(NullPointerException e) {
                    System.out.println("Contract : " + contract.getReference() + " ID: " + contract.getId());
                    //throw new NullPointerException();
                }

            }
        } else if (contract.getWkfStatus().equals(ContractWkfStatus.FIN)){
            //TODO: 1. condition Normal Contract with Last Installment Date and First Installment.
            //AIR and Revenue on Normal installment with paid and First Installment
            for (int i = lastPaidNumInstallment; i <= lastPaidNumInstallment+1; i++) {
                List<InstallmentVO> installmentVOs = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(contract, (i<=0 ? 1 : i));

                for(InstallmentVO installmentVO : installmentVOs) {
                    if (installmentVO.getCashflowType().equals(ECashflowType.IAP)) {
                        if (i==lastPaidNumInstallment) {
                            //Revenue
                            int numOfDayInPrevMonth = DateUtils.getNbDaysInMonth(DateUtils.addMonthsDate(installmentVO.getInstallmentDate(), -1));
                            intInOneDayPrev = installmentVO.getTiamount() / numOfDayInPrevMonth;
                            long totalNumPrev = DateUtils.getDiffInDays(lastPaidDate, calculStartDate)+1;
                            totalReceivePrev = intInOneDayPrev * totalNumPrev;

                            numOfDayInNextMonth = DateUtils.getNbDaysInMonth(installmentVO.getInstallmentDate());
                        }else if(contract.getLastPaidNumInstallment() > 0 && i==lastPaidNumInstallment+1) {
                            //AIR Normal
                            double intInOneDayNext = installmentVO.getTiamount() / numOfDayInNextMonth;
                            long totalNumNext = DateUtils.getDiffInDays(calculEndDate, lastPaidDate) - 1;
                            totalReceiveNext = intInOneDayNext * totalNumNext;
                        } else if(contract.getLastPaidNumInstallment() == 0 && i==lastPaidNumInstallment+2) {
                            //TODO: 2. First Installment
                            double intInOneDayNext = installmentVO.getTiamount() / numOfDayInNextMonth;
                            long totalNumNext = DateUtils.getDiffInDays(calculEndDate, lastPaidDate) - 1;
                            totalReceiveNext = intInOneDayNext * totalNumNext;
                        }

                        if(contract.getLastPaidNumInstallment() == 0 && i == lastPaidNumInstallment) {
                            //TODO: 2. First Installment
                            long totalDatePrev = DateUtils.getDiffInDays(contract.getFirstDueDate(),contract.getStartDate()) + 1;
                            intIncomePrev = intInOneDayPrev * totalDatePrev;
                        }
                    }
                }
            }
        }

        //TODO: Revenue (Interest Income) & AIR
        interestRevenue.plus(AmountUtils.convertToAmount(totalReceivePrev));
        interestRevenue.plus(AmountUtils.convertToAmount(totalReceiveNext));
        leaseTransaction.setInterestRevenue(interestRevenue);

        interestIncomeReceivable.plus(AmountUtils.convertToAmount(intIncomePrev));
        interestIncomeReceivable.plus(AmountUtils.convertToAmount(totalReceiveNext));
        leaseTransaction.setInterestReceivable(interestIncomeReceivable);

        return leaseTransaction;
    }

    @Override
    public List<InsuranceIncome> getInsuranceIncomes(EDealerType dealerTyep, Dealer dealer, String reference, Date startDate, Date endDate) {
        List<InsuranceIncome> insuranceIncomes = new ArrayList<>();

        startDate = DateUtils.getDateAtBeginningOfDay(startDate);
        endDate = DateUtils.getDateAtBeginningOfDay(endDate);

        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.in("wkfStatus",
                new EWkfStatus[]{QuotationWkfStatus.ACT, QuotationWkfStatus.ACG, QuotationWkfStatus.RVG, QuotationWkfStatus.RCG, QuotationWkfStatus.LCG}));

        restrictions.addCriterion(Restrictions.le("contractStartDate", endDate));

        if (dealer != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        }

        if (StringUtils.isNotEmpty(reference)) {
            restrictions.addCriterion(Restrictions.ilike(REFERENCE, reference, MatchMode.ANYWHERE));
        }

        List<Quotation> quotations = list(restrictions);
        for (Quotation quotation : quotations) {

            QuotationService insuranceService = quotation.getQuotationService("INSFEE");
            if (insuranceService != null) {

                CalculationParameter calculationParameter = new CalculationParameter();
                calculationParameter.setInitialPrincipal(quotation.getTiFinanceAmount());
                calculationParameter.setNumberOfPeriods(LoanUtils.getNumberOfPeriods(quotation.getTerm(), quotation.getFrequency()));
                calculationParameter.setPeriodicInterestRate(quotation.getInterestRate() / 100d);
                calculationParameter.setFrequency(quotation.getFrequency());
                calculationParameter.setInsuranceFee(insuranceService.getTiPrice());

                GLFInsuranceIncomeCalculatorImpl calculator = new GLFInsuranceIncomeCalculatorImpl();
                Long cotraId = quotation.getContract().getId();
                InsuranceIncomeSchedules insuranceIncomeSchedules = null;
                if (insuranceService.isSplitWithInstallment()) {
                    Map<Integer, Cashflow> insuranceClashflow = getInsuranceCashflow(cotraId);
                    insuranceIncomeSchedules = calculator.getSchedules(quotation.getContractStartDate(), quotation.getFirstDueDate(), calculationParameter, insuranceClashflow);
                    System.out.println(insuranceIncomeSchedules.toString());
                } else {
                    insuranceIncomeSchedules = calculator.getSchedules(quotation.getContractStartDate(), quotation.getFirstDueDate(), calculationParameter);
                }
                InsuranceIncome insuranceIncome = new InsuranceIncome();
                insuranceIncome.setId(quotation.getId());
                insuranceIncome.setReference(quotation.getReference());
                insuranceIncome.setContractStartDate(quotation.getContractStartDate());
                insuranceIncome.setFirstInstallmentDate(quotation.getFirstDueDate());
                insuranceIncome.setFirstNameEn(quotation.getApplicant().getIndividual().getFirstNameEn());
                insuranceIncome.setLastNameEn(quotation.getApplicant().getIndividual().getLastNameEn());

                for (InsuranceIncomeSchedule insuranceIncomeSchedule : insuranceIncomeSchedules.getSchedules()) {

                    Date calculEndDate = DateUtils.getDateAtEndOfMonth(endDate);
                    Date calculStartDate = DateUtils.getDateAtBeginningOfMonth(startDate);
                    if (calculStartDate.compareTo(insuranceIncomeSchedule.getPeriodStartDate()) <= 0
                            && calculEndDate.compareTo(insuranceIncomeSchedule.getPeriodEndDate()) >= 0) {
                    /*if (startDate.compareTo(insuranceIncomeSchedule.getPeriodStartDate()) <= 0
							&& endDate.compareTo(insuranceIncomeSchedule.getPeriodEndDate()) >= 0) {*/
                        insuranceIncome.getInsuranceIncomeDistribution2().plus(new Amount(insuranceIncomeSchedule.getInsuranceIncomeDistribution2(), 0d, insuranceIncomeSchedule.getInsuranceIncomeDistribution2()));
                        insuranceIncome.getInsuranceIncomeDistribution3().plus(new Amount(insuranceIncomeSchedule.getInsuranceIncomeDistribution3(), 0d, insuranceIncomeSchedule.getInsuranceIncomeDistribution3()));
                        insuranceIncome.getCumulativeBalance().plus(new Amount(insuranceIncomeSchedule.getCumulativeBalance(), 0d, insuranceIncomeSchedule.getCumulativeBalance()));
                        insuranceIncome.getAccountReceivable().plus(new Amount(insuranceIncomeSchedule.getAccountReceivable(), 0d, insuranceIncomeSchedule.getAccountReceivable()));
                        insuranceIncome.getInsuranceIncomeReceived().plus(new Amount(insuranceIncomeSchedule.getInsuranceIncomeReceived(), 0d, insuranceIncomeSchedule.getInsuranceIncomeReceived()));

                        insuranceIncome.getInsuranceIncomeInSuspendCumulated().plus(new Amount(insuranceIncomeSchedule.getInsuranceIncomeInSuspendCumulated(), 0d, insuranceIncomeSchedule.getInsuranceIncomeInSuspendCumulated()));

                        //Insurance income calculate as daily base
                        Date calculEndDate2 = DateUtils.addDaysDate(insuranceIncomeSchedule.getPeriodEndDate(), 1);
                        Date calculStartDate2 = insuranceIncomeSchedule.getPeriodStartDate();
                        if (DateUtils.isBeforeDay(calculStartDate2, startDate)) {
                            calculStartDate2 = startDate;
                        }
                        if (DateUtils.isBeforeDay(endDate, calculEndDate2)) {
                            calculEndDate2 = endDate;
                        }

                        long coeff = DateUtils.getDiffInDaysPlusOneDay(insuranceIncomeSchedule.getPeriodEndDate(), insuranceIncomeSchedule.getPeriodStartDate()); // DateUtils.getDiffInDaysPlusOneDay(periodEndDate, periodStartDate);
                        long nbDays = 0;
                        if (DateUtils.isAfterDay(insuranceIncomeSchedule.getPeriodEndDate(), calculEndDate2)) {
                            nbDays = DateUtils.getDiffInDaysPlusOneDay(calculEndDate2, calculStartDate2);
                        } else {
                            nbDays = coeff - DateUtils.getDiffInDays(calculStartDate2, insuranceIncomeSchedule.getPeriodStartDate());
                        }

                        logger.info("nbDays - [" + nbDays + "]");
                        logger.info("coeff - [" + coeff + "]");

                        double realInsuranceIncomeDistributedInNDays = 0d;
                        double insuranceIncomeInSuspendInNDays = 0d;
                        double insuranceIncomeInSuspendCumulatedInNDays = 0d;
                        double insuranceIncomeInSuspendCumulatd = insuranceIncomeSchedule.getInsuranceIncomeInSuspendCumulated() - insuranceIncomeSchedule.getInsuranceIncomeInSuspend();

                        double insuranceIncomeLatePayment = insuranceIncomeSchedule.getInsuranceIncomeRevenueLatePayment();
                        double insuranceIncomeCal = insuranceIncomeSchedule.getRealInsuranceIncomeDistributed() - insuranceIncomeLatePayment;

                        realInsuranceIncomeDistributedInNDays = MyMathUtils.roundAmountTo((insuranceIncomeCal / coeff) * nbDays) + insuranceIncomeLatePayment;
                        insuranceIncomeInSuspendInNDays = MyMathUtils.roundAmountTo((insuranceIncomeSchedule.getInsuranceIncomeInSuspend() / coeff) * nbDays);
                        insuranceIncomeInSuspendCumulatedInNDays = insuranceIncomeInSuspendCumulatd + insuranceIncomeInSuspendInNDays;

                        insuranceIncome.getRealInsuranceIncomeDistributed().plus(new Amount(realInsuranceIncomeDistributedInNDays, 0d, realInsuranceIncomeDistributedInNDays));
                        insuranceIncome.getInsuranceIncomeInSuspend().plus(new Amount(insuranceIncomeInSuspendInNDays, 0d, insuranceIncomeInSuspendInNDays));
                        insuranceIncome.getInsuranceIncomeInSuspendCumulated().plus(new Amount(insuranceIncomeInSuspendCumulatedInNDays, 0d, insuranceIncomeInSuspendCumulatedInNDays));
                    }
                    if (insuranceIncomeSchedule.getPeriodEndDate().compareTo(endDate) >= 0
                            && insuranceIncome.getUnearnedInsuranceIncome().getTiAmount() == null) {
                        insuranceIncome.setUnearnedInsuranceIncome(new Amount(insuranceIncomeSchedule.getUnearnedInsuranceIncome(), 0d, insuranceIncomeSchedule.getUnearnedInsuranceIncome()));
                    }
                }
                insuranceIncomes.add(insuranceIncome);
            }
        }

        return insuranceIncomes;
    }

    public List<LeasesReport> getLeaseReports(Date calculDate, BaseRestrictions<Contract> restrictions) {

        List<LeasesReport> leasesReports = new ArrayList<>();
        List<Contract> contracts = list(restrictions);
        calculDate = DateUtils.getDateAtEndOfDay(calculDate);

        int i = 0;
        int nbContract = contracts.size();

        for (Contract contract : contracts) {
            i++;
            // if (i % 500 == 0 || i == nbContract) {
            System.out.println("LeasesReport === " + i + "/" + nbContract);
            //}

            LeasesReport lease = new LeasesReport();
            Applicant applicant = contract.getApplicant();
            Individual individual = applicant.getIndividual();
            Asset asset = contract.getAsset();
            Address address = individual.getMainAddress();
            Dealer dealer = contract.getDealer();

            List<Cashflow> cashflows = cashflowService.getNativeCashflowsNoCancel(contract.getId());

            Employment emp = individual.getCurrentEmployment();

            lease.setId(contract.getId());
            lease.setPoNo("");
            lease.setLidNo(contract.getReference());
            lease.setWkfStatus(contract.getWkfStatus());
            lease.setFullName(individual.getLastNameEn() + " " + individual.getFirstNameEn());

            if (emp != null) {
                lease.setBusinessType(emp.getEmploymentStatus().getDescEn());
                lease.setBusinessIndustry(emp.getEmploymentIndustry().getDescEn());
            }

            Quotation quotation = contract.getQuotation();
            Applicant applicantEmployment = quotation.getMainApplicant();
            Employment employment = applicantEmployment.getIndividual().getEmployments().get(0);
            EmploymentOccupation employmentOccupation = employment.getEmploymentOccupation();

			/* added new lease field for analysis report*/

            lease.setQuotationID(quotation.getId());
            lease.setOccupation(employmentOccupation == null ? " " : employmentOccupation.getDescEn());
            lease.setPosition(employment != null && employment.getEmploymentPosition() != null ? employment.getEmploymentPosition().getDescEn() : " ");
            lease.setMaritalStatus(applicant.getIndividual().getMaritalStatus().getDesc());
            lease.setAge(DateUtils.getAge(applicant.getIndividual().getBirthDate()));
            lease.setPropertyAddressType(individual.getIndividualAddresses().get(0).getHousing().getDescEn());
            lease.setTel(individual.getMobilePerso());

            lease.setHouseNo(StringUtils.defaultString(address.getHouseNo() != null ? address.getHouseNo() : " "));
            lease.setStreet(StringUtils.defaultString(address.getStreet() != null ? address.getStreet() : " "));
            lease.setVillage(address.getVillage().getDescEn());
            lease.setVillageKh(StringUtils.defaultString(address.getVillage().getDesc()));
            lease.setCommune(address.getCommune().getDescEn());
            lease.setCommuneKh(StringUtils.defaultString(address.getCommune().getDesc()));
            lease.setDistrict(address.getDistrict().getDescEn());
            lease.setDistrictKh(StringUtils.defaultString(address.getDistrict().getDesc()));
            lease.setProvince(address.getProvince().getDescEn());
            lease.setProvinceKh(StringUtils.defaultString(address.getProvince().getDesc()));

            lease.setSex(individual.getGender().getDescEn());
            lease.setDealerName(dealer.getNameEn());
            lease.setAssetModel(asset.getModel().getDescEn());
            lease.setCoName(quotation.getCreditOfficer().getDesc());
            lease.setPoNo(quotation.getProductionOfficer() != null ? quotation.getProductionOfficer().getDesc() : "");
            lease.setApplicationType(quotation.getFinancialProduct().getCode());
            lease.setLeasAmount(quotation.getTiFinanceAmount());
            lease.setLeaseAmountPercentage(quotation.getLeaseAmountPercentage());
            lease.setUnderwriter(quotation.getUnderwriter() != null ? quotation.getUnderwriter().getDesc() : " ");
            lease.setUnderwriterSupervisor(quotation.getUnderwriterSupervisor() != null ? quotation.getUnderwriterSupervisor().getDesc() : " ");

            /* appinacn income from field check*/
            lease.setRatio(quotation.getUwNetIncomeEstimation() != null ? quotation.getUwNetIncomeEstimation() : 0d);
            lease.setSalary(quotation.getUwRevenuEstimation() != null ? quotation.getUwRevenuEstimation() : 0d);
            lease.setAllowance(quotation.getUwAllowanceEstimation() != null ? quotation.getUwAllowanceEstimation() : 0d);
            lease.setBusinessExpense(quotation.getUwBusinessExpensesEstimation() != null ? quotation.getUwBusinessExpensesEstimation() : 0d);
            lease.setPersonalExpense(quotation.getUwPersonalExpensesEstimation() != null ? quotation.getUwPersonalExpensesEstimation() : 0d);
            lease.setFamilyExpense(quotation.getUwFamilyExpensesEstimation() != null ? quotation.getUwFamilyExpensesEstimation() : 0d);
            lease.setLiabilities(quotation.getUwLiabilityEstimation() != null ? quotation.getUwLiabilityEstimation() : 0d);

            /*Ratio*/

            double coRevenuEstimation = employment.getRevenue() != null ? employment.getRevenue() : 0d;
            double coAllowance = employment.getAllowance() != null ? employment.getAllowance() : 0d;
            double coBizExpance = employment.getBusinessExpense() != null ? employment.getBusinessExpense() : 0d;
            double coPersonalExpence = employment.getIndividual().getMonthlyPersonalExpenses() != null ? employment.getIndividual().getMonthlyPersonalExpenses() : 0d;
            double coFamilyExpence = employment.getIndividual().getMonthlyFamilyExpenses() != null ? employment.getIndividual().getMonthlyFamilyExpenses() : 0d;
            double coLiability = employment.getIndividual().getTotalDebtInstallment() != null ? employment.getIndividual().getTotalDebtInstallment() : 0d;
            double coTotalInstallment = quotation.getTotalInstallmentAmount() != null ? quotation.getTotalInstallmentAmount() : 0d;
            double totalIncome = (coRevenuEstimation + coAllowance);
            double totalExpence = (coBizExpance + coPersonalExpence + coFamilyExpence + coLiability);

            double applicantRatio = (totalIncome - totalExpence) / coTotalInstallment;
            lease.setRatio(applicantRatio);

            /* Ratio for wu*/
            double uwRevenuEstimation = quotation.getUwRevenuEstimation() != null ? quotation.getUwRevenuEstimation() : 0d;
            double uwAllowance = quotation.getUwAllowanceEstimation() != null ? quotation.getUwAllowanceEstimation() : 0d;
            double uwBizExpance = quotation.getUwBusinessExpensesEstimation() != null ? quotation.getUwBusinessExpensesEstimation() : 0d;
            double uwPersonalExpence = quotation.getUwPersonalExpensesEstimation() != null ? quotation.getUwPersonalExpensesEstimation() : 0d;
            double uwFamilyExpence = quotation.getUwFamilyExpensesEstimation() != null ? quotation.getUwFamilyExpensesEstimation() : 0d;
            double uwLiability = quotation.getUwLiabilityEstimation() != null ? quotation.getUwLiabilityEstimation() : 0d;
            double uwTotalInstallment = quotation.getTotalInstallmentAmount() != null ? quotation.getTotalInstallmentAmount() : 0d;
            double uwtalIncome = (uwRevenuEstimation + uwAllowance);
            double uwTotaltalExpence = (uwBizExpance + uwPersonalExpence + uwFamilyExpence + uwLiability);

            double uwRatio = (uwtalIncome - uwTotaltalExpence) / uwTotalInstallment;
            lease.setRatioUw(uwRatio);

            /* ----------------- end Ratio -----------*/

            lease.setAuthoritiesConfirmation(quotation.getCoVOpinionApplicant() != null ? quotation.getCoVOpinionApplicant().getDescEn() : " ");

            if (quotation.getContract() != null) {
                if (contract.getLastPaidNumInstallment() > 0) {
                    List<InstallmentVO> nextInstallmentVOList = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(contract, contract.getLastPaidNumInstallment());
                    lease.setRealPrinBalanceAnaly(nextInstallmentVOList.get(0).getBalance());
                } else {
                    lease.setRealPrinBalanceAnaly(quotation.getTiFinanceAmount() != null ? quotation.getTiFinanceAmount() : 0d);
                }
            }
            if (employment != null && employment.isAllowCallToWorkPlace()) {
                lease.setWorkPlaceConfirmation("Working place contactable");
            } else
                lease.setWorkPlaceConfirmation(" ");

            lease.setDateOfContract(contract.getSigatureDate());
            lease.setAssetPrice(asset.getTiAssetPrice() == null ? 0.0d : asset.getTiAssetPrice());
            lease.setTerm(contract.getTerm());
            lease.setFirstInstallmentDate(contract.getFirstDueDate());
            lease.setRate(contract.getInterestRate() == null ? 0.0d : contract.getInterestRate());
            lease.setIrrMonth(contract.getIrrRate() == null ? 0.0d : contract.getIrrRate() * 100);
            if (lease.getIrrMonth() != null) {
                lease.setIrrYear(lease.getIrrMonth() * 12);
            }
            lease.setLoanAmount(contract.getTiFinancedAmount() == null ? 0.0d : contract.getTiFinancedAmount());
            lease.setNbOverdueInDays(contractService.getNbOverdueInDays(calculDate, cashflows));
            lease.setDownPay(contract.getTiAdvancePaymentAmount() == null ? 0.0d : contract.getTiAdvancePaymentAmount());
            lease.setAdvPaymentPer(contract.getAdvancePaymentPercentage() == null ? 0.0d : contract.getAdvancePaymentPercentage());

            lease.setInsurance(getServiceFee(ServiceEntityField.INSFEE, cashflows));
            lease.setRegistration(getServiceFee(ServiceEntityField.REGFEE, cashflows));
            lease.setServiceFee(getServiceFee(ServiceEntityField.SERFEE, cashflows));

            lease.setAdvPayment(MyMathUtils.roundAmountTo(lease.getDownPay()
                    + getFirstInstallmentServiceFee(ServiceEntityField.INSFEE, cashflows)
                    + getFirstInstallmentServiceFee(ServiceEntityField.REGFEE, cashflows)
                    + getFirstInstallmentServiceFee(ServiceEntityField.SERFEE, cashflows)));
            lease.setSecondPay(MyMathUtils.roundAmountTo(lease.getAssetPrice() - lease.getAdvPayment()));

            lease.setTotalInt(MyMathUtils.roundAmountTo(contract.getTiFinancedAmount() * contract.getTerm() * (contract.getInterestRate() / 100)));
            if (contract.getWkfStatus().equals(ContractWkfStatus.EAR)) {
                Date earlySettlementDate = getEventDate(contract.getHistories(), ContractWkfStatus.EAR);
                if (earlySettlementDate.compareTo(calculDate) > 0) {
                    ContractAdjustment contractAdjustment = contract.getContractAdjustment();
                    lease.setPrinBalance(getTheoricalPrincipalBalance(calculDate, cashflows).getTiAmount());
                    lease.setRealPrinBalance(getPrincipalBalance(calculDate, cashflows).getTiAmount());
                    lease.setIntBalance(MyMathUtils.roundAmountTo(MyNumberUtils.getDouble(contractAdjustment.getTiAdjustmentInterest()) + MyNumberUtils.getDouble(getTheoricalInterestUnearnedBalance(calculDate, cashflows).getTiAmount())));
                    lease.setRealIntBalance(MyMathUtils.roundAmountTo(MyNumberUtils.getDouble(contractAdjustment.getTiAdjustmentInterest()) + MyNumberUtils.getDouble(getInterestUnearnedBalance(calculDate, cashflows).getTiAmount())));
                    lease.setTotalReceive(MyMathUtils.roundAmountTo(MyNumberUtils.getDouble(lease.getIntBalance()) + MyNumberUtils.getDouble(lease.getPrinBalance())));
                    lease.setRealTotalReceive(MyMathUtils.roundAmountTo(MyNumberUtils.getDouble(lease.getRealIntBalance()) + MyNumberUtils.getDouble(lease.getRealPrinBalance())));
                } else {
                    lease.setPrinBalance(0d);
                    lease.setRealPrinBalance(0d);
                    lease.setIntBalance(0d);
                    lease.setRealIntBalance(0d);
                    lease.setTotalReceive(0d);
                    lease.setRealTotalReceive(0d);
                }
            } else if (contract.getWkfStatus().equals(ContractWkfStatus.LOS)
                    || contract.getWkfStatus().equals(ContractWkfStatus.REP)
                    || contract.getWkfStatus().equals(ContractWkfStatus.THE)
                    || contract.getWkfStatus().equals(ContractWkfStatus.ACC)
                    || contract.getWkfStatus().equals(ContractWkfStatus.FRA)
                    || contract.getWkfStatus().equals(ContractWkfStatus.WRI)) {
                Date lossDate = getEventDate(contract.getHistories(), contract.getWkfStatus());

                if (lossDate.compareTo(calculDate) > 0) {
                    ContractAdjustment contractAdjustment = contract.getContractAdjustment();
                    lease.setPrinBalance(getTheoricalPrincipalBalance(calculDate, cashflows).getTiAmount());
                    lease.setRealPrinBalance(contractAdjustment.getTiAdjustmentPrincipal() + getPrincipalBalance(calculDate, cashflows).getTiAmount());
                    lease.setIntBalance(MyMathUtils.roundAmountTo(getTheoricalInterestUnearnedBalance(calculDate, cashflows).getTiAmount()));
                    lease.setRealIntBalance(contractAdjustment.getTiAdjustmentInterest() + MyMathUtils.roundAmountTo(getInterestUnearnedBalance(calculDate, cashflows).getTiAmount()));
                    lease.setTotalReceive(MyMathUtils.roundAmountTo(MyNumberUtils.getDouble(lease.getIntBalance()) + MyNumberUtils.getDouble(lease.getPrinBalance())));
                    lease.setRealTotalReceive(MyMathUtils.roundAmountTo(MyNumberUtils.getDouble(lease.getRealIntBalance()) + MyNumberUtils.getDouble(lease.getRealPrinBalance())));
                } else {
                    lease.setPrinBalance(0d);
                    lease.setRealPrinBalance(0d);
                    lease.setIntBalance(0d);
                    lease.setRealIntBalance(0d);
                    lease.setTotalReceive(0d);
                    lease.setRealTotalReceive(0d);
                }
            } else if (contract.getWkfStatus().equals(ContractWkfStatus.CLO)) {
                lease.setPrinBalance(0d);
                lease.setRealPrinBalance(0d);
                lease.setIntBalance(0d);
                lease.setRealIntBalance(0d);
                lease.setTotalReceive(0d);
                lease.setRealTotalReceive(0d);
            } else {
                lease.setPrinBalance(getTheoricalPrincipalBalance(calculDate, cashflows).getTiAmount());
                lease.setRealPrinBalance(getPrincipalBalance(calculDate, cashflows).getTiAmount());
                lease.setIntBalance(MyMathUtils.roundAmountTo(getTheoricalInterestUnearnedBalance(calculDate, cashflows).getTiAmount()));
                lease.setRealIntBalance(MyMathUtils.roundAmountTo(getInterestUnearnedBalance(calculDate, cashflows).getTiAmount()));
                lease.setTotalReceive(MyMathUtils.roundAmountTo(MyNumberUtils.getDouble(lease.getIntBalance()) + MyNumberUtils.getDouble(lease.getPrinBalance())));
                lease.setRealTotalReceive(MyMathUtils.roundAmountTo(MyNumberUtils.getDouble(lease.getRealIntBalance()) + MyNumberUtils.getDouble(lease.getRealPrinBalance())));
            }
            lease.setInstallmentAmount(MyMathUtils.roundAmountTo(MyNumberUtils.getDouble(contract.getTiInstallmentAmount())));
            //lease.setPoNumber(cashflows.get(0).getPayment().getReference().replaceAll("-OR", ""));

            if (contract != null && contract.getLastPaidDateInstallment() != null) {
                lease.setLastPaidInstallmentDate(contract.getLastPaidDateInstallment());
                lease.setNumInstallment(contract.getLastPaidNumInstallment());
            } else {
                lease.setLastPaidInstallmentDate(null);
                lease.setNumInstallment(null);
            }

            leasesReports.add(lease);
        }
        return leasesReports;
    }

    private Double getFirstInstallmentServiceFee(String code, List<Cashflow> cashflows) {
        double tiServiceAmount = 0d;
        /*for (Cashflow cashflow : cashflows) {
            if (cashflow.getCashflowType().equals(ECashflowType.FEE) && code.equals(cashflow.getService().getCode()) && cashflow.getNumInstallment() == 0) {
                tiServiceAmount += cashflow.getTiInstallmentAmount();
            }
        }*/
        return tiServiceAmount;
    }


    private Double getServiceFee(String code, List<Cashflow> cashflows) {
        double tiServiceAmount = 0d;
        /*for (Cashflow cashflow : cashflows) {
            if (cashflow.getCashflowType().equals(ECashflowType.FEE) && code.equals(cashflow.getService().getCode())) {
				tiServiceAmount += cashflow.getTiInstallmentAmount();
			}
		}*/
        return tiServiceAmount;
    }

    private Date getLastInstallmentDate(List<Cashflow> cashflows) {
        Date lastInstallmentDate = null;
        for (Cashflow cashflow : cashflows) {
            if (!cashflow.isCancel()
                    && (lastInstallmentDate == null || lastInstallmentDate.compareTo(cashflow.getInstallmentDate()) < 0)) {
                lastInstallmentDate = cashflow.getInstallmentDate();
            }
        }
        return lastInstallmentDate;
    }

   /* private Amount getAccruedInterestIncome(Contract contract) {
        List<Cashflow> cashflows = getCashflowsNoCancel(contract.getId());
        CalculationParameter calculationParameter = new CalculationParameter();
        calculationParameter.setInitialPrincipal(contract.getTiFinancedAmount());
        calculationParameter
                .setNumberOfPeriods(LoanUtils.getNumberOfPeriods(contract.getTerm(), contract.getFrequency()));
        calculationParameter.setPeriodicInterestRate(contract.getInterestRate() / 100d);
        calculationParameter.setFrequency(contract.getFrequency());
        Date changeDate = getDateContractChangeStatus(contract.getHistories(), contract.getWkfStatus());
        LeaseTransaction leaseTransaction = getLeaseTransaction(contract, cashflows, contract.getStartDate(),
                DateUtils.addDaysDate(DateUtils.getDateAtEndOfDay(changeDate), -1), calculationParameter, null);
        Amount accreudInterestRateAmount = new Amount(0d, 0d, 0d);
        accreudInterestRateAmount.setTiAmount(leaseTransaction.getInterestRevenue().getTiAmount() - leaseTransaction.getInterestIncome().getTiAmount());
        accreudInterestRateAmount.setTeAmount(leaseTransaction.getInterestRevenue().getTeAmount() - leaseTransaction.getInterestIncome().getTeAmount());
        accreudInterestRateAmount.setVatAmount(leaseTransaction.getInterestRevenue().getVatAmount() - leaseTransaction.getInterestIncome().getVatAmount());
        return accreudInterestRateAmount;
    }

    private Date getDateContractChangeStatus(List<ContractWkfHistoryItem> histories, EWkfStatus contractStatus) {
        Date earlySettlementDate = null;
        ContractWkfHistoryItem historyType = null;
        EHistoReason eHistoReason = null;
        if (contractStatus.equals(ContractWkfStatus.EAR)) {
            eHistoReason = ContractHistoReason.CONTRACT_0003;
        } else if (contractStatus.equals(ContractWkfStatus.LOS)) {
            eHistoReason = ContractHistoReason.CONTRACT_LOSS;
        } else if (contractStatus.equals(ContractWkfStatus.REP)) {
            eHistoReason = ContractHistoReason.CONTRACT_REP;
        } else if (contractStatus.equals(ContractWkfStatus.THE)) {
            eHistoReason = ContractHistoReason.CONTRACT_THE;
        } else if (contractStatus.equals(ContractWkfStatus.ACC)) {
            eHistoReason = ContractHistoReason.CONTRACT_ACC;
        } else if (contractStatus.equals(ContractWkfStatus.FRA)) {
            eHistoReason = ContractHistoReason.CONTRACT_FRA;
        } else if (contractStatus.equals(ContractWkfStatus.WRI)) {
            eHistoReason = ContractHistoReason.CONTRACT_WRI;
        } else if (contractStatus.equals(ContractWkfStatus.WAIT_TRANSACTION_SETTLEMENT)) {
           *//* eHistoReason = ContractHistoReason.TRANSFERRED;*//*
            eHistoReason = ContractHistoReason.CONTRACT_SIMUL_EARLY_SETL;
        } else if (contractStatus.equals(ContractWkfStatus.FIN)) {
            eHistoReason = ContractHistoReason.CONTRACT_0001;
        } else if (contractStatus.equals(ContractWkfStatus.CLO)) {
            eHistoReason = ContractHistoReason.CONTRACT_0002;
        }

        for (ContractWkfHistoryItem history : histories) {
            if (history.getWkfHistory().equals(historyType)) {
                earlySettlementDate = DateUtils.getDateAtBeginningOfDay(history.getWkfHistory().getChangeDate());
            }
        }
        return earlySettlementDate;
    }*/

    public Integer getNumberYearOfTwoDates(Date date1, Date date2) {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(date1);
        int year1 = calendar1.get(1);
        int month1 = calendar1.get(2);
        int day1 = calendar1.get(5);
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(date2);
        int year2 = calendar2.get(1);
        int month2 = calendar2.get(2);
        int day2 = calendar2.get(5);
        int numberYear = year1 - year2;
        if (month1 < month2 || month2 == month1 && day1 < day2) {
            --numberYear;
        }

        return numberYear;
    }

    private void getCalculationInsuranceEndYear() {

        List<Quotation> quotationsStart = getInsuranceStartRestrictions();
        List<Quotation> quotationEnd = getInsuranceEndRestrictions();

        for (Quotation quotation : quotationsStart) {
            Date insuranceDate = quotation.getInsuranceStartDate();

            if (quotation.getInsuranceEndYear() == null) {
                if (quotation.getTerm() == 18) {

                    Date oneYear = DateUtils.addYearsDate(insuranceDate, 1);
                    Date Year18 = DateUtils.addMonthsDate(oneYear, 6);
                    quotation.setInsuranceEndYear(Year18);
                }
                if (quotation.getTerm() == 30) {
                    Date twoYear = DateUtils.addYearsDate(insuranceDate, 2);
                    Date Year30 = DateUtils.addMonthsDate(twoYear, 6);
                    quotation.setInsuranceEndYear(Year30);
                }

                if (quotation.getTerm() == 12) {
                    quotation.setInsuranceEndYear(DateUtils.addYearsDate(insuranceDate, 1));
                }
                if (quotation.getTerm() == 24) {
                    quotation.setInsuranceEndYear(DateUtils.addYearsDate(insuranceDate, 2));
                }

                if (quotation.getTerm() == 36) {
                    quotation.setInsuranceEndYear(DateUtils.addYearsDate(insuranceDate, 3));
                }
                if (quotation.getTerm() == 48) {
                    quotation.setInsuranceEndYear(DateUtils.addYearsDate(insuranceDate, 4));
                }

                if (quotation.getTerm() == 60) {
                    quotation.setInsuranceEndYear(DateUtils.addYearsDate(insuranceDate, 5));
                }
            }
            ENTITY_SRV.saveOrUpdate(quotation);
        }

        for (Quotation quotation : quotationEnd) {
            Date today = DateUtils.todayDate();
            Date InsuranceEndDate = quotation.getInsuranceEndYear();
            int monthEnd = DateUtils.getNumberMonthOfTwoDates(today, InsuranceEndDate);
            if (quotation.getTerm() == 18 || quotation.getTerm() == 30) {
                if (monthEnd >= 18) {
                    Date oneYear = DateUtils.addYearsDate(InsuranceEndDate, 1);
                    Date Year18 = DateUtils.addMonthsDate(oneYear, 6);
                    quotation.setInsuranceEndYear(Year18);
                }
                if (monthEnd >= 30) {
                    Date twoYear = DateUtils.addYearsDate(InsuranceEndDate, 2);
                    Date Year30 = DateUtils.addMonthsDate(twoYear, 6);
                    quotation.setInsuranceEndYear(Year30);
                }
            }
            if (quotation.getTerm() == 12 || quotation.getTerm() == 24) {
                if (monthEnd >= 12) {
                    quotation.setInsuranceEndYear(DateUtils.addYearsDate(InsuranceEndDate, 1));
                }
                if (monthEnd >= 24) {
                    quotation.setInsuranceEndYear(DateUtils.addYearsDate(InsuranceEndDate, 2));
                }
            }
            if (quotation.getTerm() == 36 || quotation.getTerm() == 48) {
                if (monthEnd >= 36) {
                    quotation.setInsuranceEndYear(DateUtils.addYearsDate(InsuranceEndDate, 3));
                }
                if (monthEnd >= 48) {
                    quotation.setInsuranceEndYear(DateUtils.addYearsDate(InsuranceEndDate, 4));
                }
            }
            if (quotation.getTerm() == 60) {
                if (monthEnd >= 60) {
                    quotation.setInsuranceEndYear(DateUtils.addYearsDate(InsuranceEndDate, 5));
                }
            }

            ENTITY_SRV.saveOrUpdate(quotation);
        }
    }

    private List<Quotation> getInsuranceStartRestrictions() {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.isNotNull("insuranceStartDate"));
        return ENTITY_SRV.list(restrictions);
    }

    private List<Quotation> getInsuranceEndRestrictions() {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.isNotNull("insuranceEndYear"));
        return ENTITY_SRV.list(restrictions);
    }
}
