package com.soma.mfinance.core.auction.panel.stock;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * Stock Panel
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(AssetStockPanel.NAME)
public class AssetStockPanel extends AbstractTabsheetPanel implements View {

	private static final long serialVersionUID = -4292057602033442775L;
	public static final String NAME = "asset.stock";
	
	@Autowired
	private AssetStockTablePanel tablePanel;

	@Autowired
	private AssetStockFormPanel formPanel;

	@PostConstruct
	public void PostConstruct() {
		super.init();
		tablePanel.setMainPanel(this);
		formPanel.setCaption(I18N.message("stock"));
		getTabSheet().setTablePanel(tablePanel);
	}

	@Override
	public void enter(ViewChangeEvent event) {
	}

	@Override
	public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
		if (selectedTab == formPanel) {
			formPanel.assignValues(tablePanel.getItemSelectedId());
		} else if (selectedTab == tablePanel && getTabSheet().isNeedRefresh()) {
			tablePanel.refresh();
		}
		getTabSheet().setSelectedTab(selectedTab);
	}

	@Override
	public void onAddEventClick() {
	}

	@Override
	public void onEditEventClick() {
		formPanel.reset();
		getTabSheet().addFormPanel(formPanel);
		initSelectedTab(formPanel);
	}

}
