package com.soma.mfinance.core.financial.model;

import org.seuksa.frmk.model.entity.MEntityRefA;


/**
 * Meta data of com.soma.mfinance.core.financial.model.CampaignAssetCategory
 * @author kimsuor.seang
 */
public interface MCampaignAssetCategory extends MEntityRefA {

	public final static String ASSETCATEGORY = "assetCategory";
	public final static String CAMPAIGN = "campaign";
	
}
