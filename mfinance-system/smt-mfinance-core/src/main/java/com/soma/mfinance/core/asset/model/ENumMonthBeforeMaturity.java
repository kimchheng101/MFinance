package com.soma.mfinance.core.asset.model;

import java.util.List;

import javax.persistence.AttributeConverter;

import org.seuksa.frmk.model.eref.BaseERefData;

/**
 * 
 * @author p.ly
 *
 */
public class ENumMonthBeforeMaturity extends BaseERefData implements AttributeConverter<ENumMonthBeforeMaturity, Long> {
	/** */
	private static final long serialVersionUID = 8800717908072047609L;

	/**
	 */
	public ENumMonthBeforeMaturity() {
	}
	
	/**
	 * @param code
	 * @param id
	 */
	public ENumMonthBeforeMaturity(String code, long id) {
		super(code, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public ENumMonthBeforeMaturity convertToEntityAttribute(Long id) {
		return super.convertToEntityAttribute(id);
	}
	
	@Override
	public Long convertToDatabaseColumn(ENumMonthBeforeMaturity arg0) {
		return super.convertToDatabaseColumn(arg0);
	}

	/** 
	 * @return
	 */
	public static List<ENumMonthBeforeMaturity> values() {
		return getValues(ENumMonthBeforeMaturity.class);
	}
	
	/**
	 * @param code
	 * @return
	 */
	public static ENumMonthBeforeMaturity getByCode(String code) {
		return getByCode(ENumMonthBeforeMaturity.class, code);
	}
	
	/**
	 * @param id
	 * @return
	 */
	public static ENumMonthBeforeMaturity getById(long id) {
		return getById(ENumMonthBeforeMaturity.class, id);
	}
}
