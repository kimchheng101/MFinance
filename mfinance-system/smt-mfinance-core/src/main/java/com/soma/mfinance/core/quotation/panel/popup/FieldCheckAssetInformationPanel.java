package com.soma.mfinance.core.quotation.panel.popup;

import com.soma.mfinance.core.document.panel.DisplayDocumentPanel;
import com.soma.mfinance.core.document.panel.DocumentUploader;
import com.soma.mfinance.core.quotation.model.AssetInformationDocument;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.system.AmountBigUtils;
import com.soma.frmk.security.context.SecApplicationContextHolder;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Runo;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.service.EntityService;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author th.seng
 *
 */
public class FieldCheckAssetInformationPanel {
	private EntityService entityService = (EntityService) SecApplicationContextHolder.getContext().getBean("entityService");
	private List<Upload> uploadDocuments;
	private List<TextField> txtAssetInfos ;
	private List<TextField> txtSizeUnits ;
	private List<TextField> txtEstimatedPrices ;
	private List<CheckBox> cbChiefVillages ;
	private List<CheckBox> cbEvidences ;
	private List<CheckBox> cbOriginals ;
	private List<Button> btnPaths;

	public GridLayout getFieldCheckAssetInfo () {
		btnPaths = new ArrayList<Button>();
		uploadDocuments = new ArrayList<Upload>();
		txtAssetInfos = new ArrayList<TextField>();
		txtSizeUnits = new ArrayList<TextField>();
		txtEstimatedPrices = new ArrayList<TextField>();
		cbChiefVillages = new ArrayList<CheckBox>();
		cbEvidences = new ArrayList<CheckBox>();
		cbOriginals = new ArrayList<CheckBox>();
	    
	    GridLayout gridLayout = new GridLayout(8, 11);
    	gridLayout.setSpacing(true);
    	gridLayout.setMargin(true);
    	
    	gridLayout.addComponent(new Label(I18N.message("asset.info")), 0, 0);
    	gridLayout.addComponent(new Label(I18N.message("size.unit")), 1, 0);
    	gridLayout.addComponent(new Label(I18N.message("estimated.price")), 2, 0);
    	
    	for (int i = 1; i < 11; i++) {  
    		TextField txtAssetInfo = ComponentFactory.getTextField(false, 30, 100);
    		txtAssetInfos.add(txtAssetInfo);
    		
    		TextField txtSizeUnit = ComponentFactory.getTextField(false, 30, 100);
    		txtSizeUnits.add(txtSizeUnit);
    		
    		TextField txtEstimatedPrice = ComponentFactory.getTextField(false, 30, 100);
    		txtEstimatedPrices.add(txtEstimatedPrice);
    		
    		CheckBox cbChiefVillage = new CheckBox();
    		cbChiefVillage.setCaption(I18N.message("chief.village.confirm"));
    		cbChiefVillages.add(cbChiefVillage);
    		
    		CheckBox cbEvidence = new CheckBox();
    		cbEvidence.setCaption(I18N.message("evidence"));
    		cbEvidences.add(cbEvidence);
    		
    		CheckBox cbOriginal = new CheckBox();
    		cbOriginal.setCaption(I18N.message("original"));
    		cbOriginals.add(cbOriginal);
    		
    		Button btnPath = new Button();
    		btnPath.setIcon(new ThemeResource("../smt-default/icons/16/pdf.png"));
    		btnPath.setVisible(false);
    		btnPath.setStyleName(Runo.BUTTON_LINK);
    		btnPaths.add(btnPath);

    		btnPath.addClickListener(new ClickListener() {
				private static final long serialVersionUID = 1762740731111758211L;
				@Override
				public void buttonClick(ClickEvent event) {
					new DisplayDocumentPanel((String) ((Button) event.getSource()).getData()).display();
				}
			});
    		
    		Upload uploadDocument = new Upload();
    		uploadDocument.setButtonCaption(I18N.message("upload"));
    		uploadDocuments.add(uploadDocument);
    		
    		final DocumentUploader uploader = new DocumentUploader(cbOriginal, btnPath, uploadDocument);
    		uploadDocument.setReceiver(uploader);
    		uploadDocument.addSucceededListener(uploader);
    		gridLayout.addComponent(txtAssetInfo, 0, i);
    		gridLayout.addComponent(txtSizeUnit, 1, i);
    		gridLayout.addComponent(txtEstimatedPrice, 2, i);
    		gridLayout.addComponent(cbChiefVillage, 3, i);
    		gridLayout.addComponent(cbEvidence, 4, i);
    		gridLayout.addComponent(cbOriginal, 5, i);
    		gridLayout.addComponent(btnPath, 6, i);
    		gridLayout.addComponent(uploadDocument, 7, i);
		}
		return gridLayout;
	}

	public void assignValue(Quotation quotation) {
		reset();
		List<AssetInformationDocument> assetInformationDocuments = getAssetDocumentInfo(quotation);
		if (assetInformationDocuments != null
				&& !assetInformationDocuments.isEmpty()) {
			int i = 0;
			for(AssetInformationDocument assetInformationDocument : assetInformationDocuments) {
				txtAssetInfos.get(i).setValue(getDefaultStringValue(assetInformationDocument.getAssetInfo()));
				txtSizeUnits.get(i).setValue(getDefaultStringValue(assetInformationDocument.getAssetSizeUnit()));
				txtEstimatedPrices.get(i).setValue(AmountBigUtils.format(assetInformationDocument.getAssetEstPrice()));
				cbChiefVillages.get(i).setValue(assetInformationDocument.isCvConfirm());
				cbEvidences.get(i).setValue(assetInformationDocument.isEvidence());
				cbOriginals.get(i).setValue(assetInformationDocument.isOriginal());
				if (StringUtils.isNotEmpty(assetInformationDocument.getPath())) {
					btnPaths.get(i).setVisible(true);
					btnPaths.get(i).setData(assetInformationDocument.getPath());
				} else {
					btnPaths.get(i).setVisible(false);
					btnPaths.get(i).setData(null);
				}
				i++;
			}
		}

	}

	public void getAssetInformationDocument(Quotation quotation) {
		List<AssetInformationDocument> assetInformationDocuments = getAssetDocumentInfo(quotation);

		if (assetInformationDocuments != null && !assetInformationDocuments.isEmpty()) {
			int i = 0;
			for (AssetInformationDocument assetInformationDocument : assetInformationDocuments) {
				assetInfoSetEntity(assetInformationDocument, i);
				i++;
			}
		} else {
			assetInformationDocuments = new ArrayList<AssetInformationDocument>();
			for (int i = 0; i < 10; i++) {
				AssetInformationDocument assetInformationDocument = new AssetInformationDocument();
				assetInformationDocument.setQuotation(quotation);
				assetInfoSetEntity(assetInformationDocument, i);
				assetInformationDocuments.add(assetInformationDocument);
			}
		}
		quotation.setAssetInformationDocument(assetInformationDocuments);
		entityService.saveOrUpdate(quotation);

	}

	private List <AssetInformationDocument> getAssetDocumentInfo(Quotation quotation){
		if(quotation.getId() == null){
			return null;
		}
		BaseRestrictions<AssetInformationDocument> assetInformationDocumentRestrictions = new BaseRestrictions<AssetInformationDocument>(AssetInformationDocument.class);
		assetInformationDocumentRestrictions.addCriterion(Restrictions.eq("quotation", quotation));
		assetInformationDocumentRestrictions.addOrder(Order.asc("id"));
		List<AssetInformationDocument> assetInformations = entityService.list(assetInformationDocumentRestrictions);
		return assetInformations;
	}

	private static String getDefaultStringValue(String value) {
		return value == null ? "" : value;
	}

	private void assetInfoSetEntity(AssetInformationDocument assetInformationDocument, int i) {
		assetInformationDocument.setAssetInfo(getDefaultStringValue(txtAssetInfos.get(i).getValue()));
		assetInformationDocument.setAssetSizeUnit(getDefaultStringValue(txtSizeUnits.get(i).getValue()));
		assetInformationDocument.setAssetEstPrice(AmountBigUtils.getValueDouble(txtEstimatedPrices.get(i)));
		assetInformationDocument.setCvConfirm(cbChiefVillages.get(i).getValue());
		assetInformationDocument.setEvidence(cbEvidences.get(i).getValue());
		assetInformationDocument.setOriginal(cbOriginals.get(i).getValue());
		if(assetInformationDocument.isCvConfirm() || assetInformationDocument.isEvidence() || assetInformationDocument.isOriginal()){
			if (btnPaths.get(i).getData() != null) {
				assetInformationDocument.setPath(getDefaultStringValue((String) btnPaths.get(i).getData()));
			}
		}else{
			assetInformationDocument.setPath("");
		}
		if(StringUtils.isEmpty(assetInformationDocument.getPath())){
			assetInformationDocument.setCvConfirm(false);
			assetInformationDocument.setEvidence(false);
			assetInformationDocument.setOriginal(false);
		}
		entityService.saveOrUpdate(assetInformationDocument);
	}

	private void reset(){
		for (int i = 0; i < 10; i++) {
			txtAssetInfos.get(i).setValue("");
			txtSizeUnits.get(i).setValue("");
			txtEstimatedPrices.get(i).setValue("");
			cbChiefVillages.get(i).setValue(false);
			cbEvidences.get(i).setValue(false);
			cbOriginals.get(i).setValue(false);
			btnPaths.get(i).setVisible(false);
		}
	}

}
