package com.soma.mfinance.core.contract.model;

import org.seuksa.frmk.model.entity.MEntityA;


/**
 * Meta data of com.soma.mfinance.core.contract_old.model.ContractFinService
 * @author kimsuor.seang
 */
public interface MContractFinService extends MEntityA {
	
	public final static String CONTRACT = "contract";
	public final static String SERVICE = "service";

}
