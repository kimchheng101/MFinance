package com.soma.mfinance.core.shared.exception.quotation;

import com.soma.mfinance.core.shared.exception.BusinessException;

/**
 * Invalid quotation exception
 * @author kimsuor.seang
 *
 */
public class InvalidQuotationException extends BusinessException {

	private static final long serialVersionUID = -3131217462805735240L;

}
