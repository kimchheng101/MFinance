package com.soma.mfinance.core.asset.model;

import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;

/**
 * 
 * @author tha.bunsath
 * 
 */
@Entity
@Table(name = "tu_asset_component")
public class AssetComponent extends EntityRefA {

    private static final long serialVersionUID = -8954187823136477015L;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ass_com_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Override
    @Column(name = "ass_com_code", nullable = false, length=20, unique = true)
    public String getCode() {
        return super.getCode();
    }

    /**
     * Get the asset make's description in locale language.
     * 
     * @return <String>
     */
    @Override
    @Column(name = "ass_com_desc", nullable = true, length=255)
    public String getDesc() {
        return super.getDesc();
    }

    /**
     * Get the asset make's name in English.
     * 
     * @return <String>
     */
    @Override
    @Column(name = "ass_com_desc_en", nullable = false, length=255)
    public String getDescEn() {
        return super.getDescEn();
    }

}
