package com.soma.mfinance.glf.report.xls;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.conf.AppConfig;
import com.soma.mfinance.core.shared.financialproduct.FinancialProductEntityField;
import com.soma.mfinance.core.shared.report.ReportParameter;
import com.soma.mfinance.tools.report.Report;
import net.sf.jxls.transformer.XLSTransformer;
import org.seuksa.frmk.tools.DateUtils;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/28/2017
 * Name  : 9:41 AM
 * Email : k.seang@gl-f.com
 */
public class LeasingContractLegalMfp implements Report, FinServicesHelper {
    @Override
    public String generate(ReportParameter reportParameter) throws Exception {

        Long quotaId = (Long) reportParameter.getParameters().get("quotaId");
        Assert.notNull(quotaId, "Quotation Id can not be null. !");
        Quotation quotation = QUO_SRV.getById(Quotation.class, quotaId);
        Assert.notNull(quotation, "Quotation can not be null. !");
        Applicant applicant = quotation.getApplicant();

        Map<String, String> beans = new HashMap<String, String>();

        if (quotation != null && quotation.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_PUBLIC)) {
            beans.put("lesseeName", applicant.getIndividual().getLastName() + " " + applicant.getIndividual().getFirstName());
            beans.put("lesseeGuarantee", quotation.getGuarantor().getIndividual().getLastName() + " " + quotation.getGuarantor().getIndividual().getFirstName());
        } else {
            beans.put("lesseeName", applicant.getIndividual().getLastName() + " " + applicant.getIndividual().getFirstName());
        }
        beans.put("contractNumber", quotation.getReference());
        beans.put("leaseAgreeDate", DateUtils.getDateLabel(quotation.getContractStartDate(), DateUtils.FORMAT_DDMMYYYY_SLASH));
        beans.put("providerName", quotation.getCreditOfficer().getDesc());
        Date printDate = new Date();
        beans.put("date", DateUtils.getDateLabel(printDate, DateUtils.FORMAT_DDMMYYYY_SLASH));

        String templatePath = AppConfig.getInstance().getConfiguration().getString("specific.templatedir");
        String templateFileName = "";
        String prefixOutputName = "";

        if (quotation != null && quotation.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_PUBLIC)){
            templateFileName = templatePath +"/LeasingContractLegalPub.xlsx";
            prefixOutputName = "LeasingContractLegalPub";
        } else {
            templateFileName = templatePath + "/LeasingContractLegalEx.xlsx";
            prefixOutputName = "LeasingContractLegalEx";
        }

        String outputPath = AppConfig.getInstance().getConfiguration().getString("specific.tmpdir");
        String sufixOutputName = "xlsx";

        String uuid = UUID.randomUUID().toString().replace("-", "");
        String xlsFileName = outputPath + "/" + prefixOutputName + uuid + "." + sufixOutputName;

        XLSTransformer transformer = new XLSTransformer();
        transformer.transformXLS(templateFileName, beans, xlsFileName);

        return prefixOutputName + uuid + "." + sufixOutputName;
    }
}
