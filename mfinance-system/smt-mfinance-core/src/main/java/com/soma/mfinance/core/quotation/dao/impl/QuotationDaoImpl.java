package com.soma.mfinance.core.quotation.dao.impl;

import com.soma.mfinance.core.quotation.dao.QuotationDao;
import com.soma.mfinance.core.quotation.model.Quotation;
import org.hibernate.Query;
import org.seuksa.frmk.dao.impl.BaseEntityDaoImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Quotation data access implementation
 *
 * @author kimsuor.seang
 */
@Repository
public class QuotationDaoImpl extends BaseEntityDaoImpl implements QuotationDao {

    @SuppressWarnings("unchecked")
    @Override
    public List<Quotation> getQuotationByContractStatus() {
        final String queryString = "Select qua FROM Contract con INNER JOIN con.quotation qua "
                + " WHERE con.contractStatus in ('FIN','EAR','CLO') ";
        final Query query = createQuery(queryString);
        return query.list();
    }

}
