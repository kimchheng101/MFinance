package com.soma.mfinance.core.shared.referencial;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.application.model.EmploymentOccupation;
import com.soma.mfinance.core.application.model.EmploymentPosition;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.document.model.Document;
import com.soma.mfinance.core.document.model.DocumentGroup;
import com.soma.mfinance.core.financial.model.ProductLine;
import com.soma.mfinance.core.quotation.model.ProfileDefaultQuotationStatus;
import com.soma.mfinance.core.quotation.model.SupportDecision;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.ersys.core.hr.model.PublicHoliday;
import com.soma.frmk.config.model.SettingConfig;
import com.soma.frmk.security.model.SecUser;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.service.EntityService;
import org.seuksa.frmk.tools.spring.SpringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Data Reference
 * @author kimsuor.seang
 *
 */
public final class DataReference {
	private List<SettingConfig> settingConfigs;
	private List<Document> documents  = new ArrayList<>();
	private List<DocumentGroup> documentGroups = new ArrayList<>();
	private List<Dealer> dealers = new ArrayList<>();
	private List<SupportDecision> supportDecisions = new ArrayList<>();	
	private List<SecUser> users = new ArrayList<>();
	private List<PublicHoliday> publicHoliday = new ArrayList<>();
	private List<ProductLine> productLines = new ArrayList<>();
	private List<ProfileDefaultQuotationStatus> profileDefaultQuotationStatus = new ArrayList<>();
	private List<EmploymentOccupation> employmentOccupation;
	private List<EmploymentPosition> employmentPossitions;
			
	private DataReference() {}
	
	/** Holder */
	private static class SingletonHolder {
		private final static DataReference instance = new DataReference();
	}
	 
	/**
	 * @return
	 */
	public static DataReference getInstance() {
		return SingletonHolder.instance;
	}

	/**
	 * @return the documents
	 */
	public List<Document> getDocuments() {
		return documents;
	}

	/**
	 * @param documents the documents to set
	 */
	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	
	/**
	 * @return the documentGroups
	 */
	public List<DocumentGroup> getDocumentGroups() {
		return documentGroups;
	}

	/**
	 * @param documentGroups the documentGroups to set
	 */
	public void setDocumentGroups(List<DocumentGroup> documentGroups) {
		this.documentGroups = documentGroups;
	}

	


	/**
	 * @return the dealers
	 */
	public List<Dealer> getDealers() {
		return dealers;
	}

	/**
	 * @param dealers the dealers to set
	 */
	public void setDealers(List<Dealer> dealers) {
		this.dealers = dealers;
	}

	

	/**
	 * @return the supportDecisions
	 */
	public List<SupportDecision> getSupportDecisions() {
		return supportDecisions;
	}

	/**
	 * @param supportDecisions the supportDecisions to set
	 */
	public void setSupportDecisions(
			List<SupportDecision> supportDecisions) {
		this.supportDecisions = supportDecisions;
	}
	
	/**
	 * Get support decisions
	 * @param quotationStatus
	 * @return
	 */
	public List<SupportDecision> getSupportDecisions(EWkfStatus quotationStatus) {
		List<SupportDecision> resultList = new ArrayList<SupportDecision>();
		for (SupportDecision supportDecision : supportDecisions) {
			if (supportDecision.getQuotationStatus() == quotationStatus) {
				resultList.add(supportDecision);
			}
		}
		return resultList;
	}
	

	/**
	 * @return the users
	 */
	public List<SecUser> getUsers() {
		return users;
	}

	/**
	 * @param users the users to set
	 */
	public void setUsers(List<SecUser> users) {
		this.users = users;
	}
	
	/**
	 * @param profileId
	 * @return
	 */
	public List<SecUser> getUsers(Long profileId, EStatusRecord statusRecord) {
		EntityService ENTITY_SRV = SpringUtils.getBean(EntityService.class);
		List<SecUser> usersByProfile = new ArrayList<SecUser>();
		BaseRestrictions<SecUser> restrictions = new BaseRestrictions<SecUser>(SecUser.class);
		List<SecUser> list = new ArrayList<>(ENTITY_SRV.list(restrictions));
//		for (SecUser user : this.users) {
		for (SecUser user : list) {
			if (ProfileUtil.isProfileExist(profileId, user.getProfiles())) {
				if (statusRecord == null) {
					usersByProfile.add(user);
				} else if (user.getStatusRecord() == statusRecord){
					usersByProfile.add(user);
				}
			}
		}
		return usersByProfile;
	}

	/**
	 * 
	 * @param profileId
	 * @return
	 */
	public List<SecUser> getUsers(Long profileId) {
		return getUsers(profileId, null);
	}
	
	/**
	 * @return the publicHoliday
	 */
	public List<PublicHoliday> getPublicHoliday() {
		return publicHoliday;
	}

	/**
	 * @param publicHoliday the publicHoliday to set
	 */
	public void setPublicHoliday(List<PublicHoliday> publicHoliday) {
		this.publicHoliday = publicHoliday;
	}

	

	/**
	 * @return the profileDefaultQuotationStatus
	 */
	public List<ProfileDefaultQuotationStatus> getProfileDefaultQuotationStatus() {
		return profileDefaultQuotationStatus;
	}
	
	/**
	 * @param profileDefaultQuotationStatus the profileDefaultQuotationStatus to set
	 */
	public void setProfileDefaultQuotationStatus(
			List<ProfileDefaultQuotationStatus> profileDefaultQuotationStatus) {
		this.profileDefaultQuotationStatus = profileDefaultQuotationStatus;
	}

	/**
	 * @return the productLines
	 */
	public List<ProductLine> getProductLines() {
		return productLines;
	}

	/**
	 * @param productLines the productLines to set
	 */
	public void setProductLines(List<ProductLine> productLines) {
		this.productLines = productLines;
	}

	public List<EmploymentOccupation> getEmploymentOccupation() {
		return employmentOccupation;
	}


	public void setEmploymentOccupation(
			List<EmploymentOccupation> employmentOccupation) {
		this.employmentOccupation = employmentOccupation;
	}

	public List<EmploymentPosition> getEmploymentPossitions() {
		return employmentPossitions;
	}


	public void setEmploymentPossitions(
			List<EmploymentPosition> employmentPossitions) {
		this.employmentPossitions = employmentPossitions;
	}
	public SettingConfig getSettingConfigByCode(String code) {
		SettingConfig values = null;
		try{
			for (SettingConfig settingConfig : settingConfigs) {
				if (code.equals(settingConfig.getCode())) {
					if(settingConfig != null){
						values = settingConfig;
						return values;
					}
				}
			}
		}catch (Exception e){
			values = null;
		}
		return values;
	}
}
