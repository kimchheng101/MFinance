package com.soma.mfinance.core.asset.model;

import org.seuksa.frmk.model.eref.BaseERefData;

import javax.persistence.AttributeConverter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dang Dim
 * Date     : 10-Oct-17, 11:07 AM
 * Email    : d.dim@gl-f.com
 */

public class EPowerTillerModel extends BaseERefData implements AttributeConverter<EPowerTillerModel, Long> {

    public final static EPowerTillerModel NC_PLUS_X = new EPowerTillerModel("NC_PLUS_X", 1);

    public EPowerTillerModel() {
    }

    public EPowerTillerModel(String code, long id) {
        super(code, id);
    }

    @SuppressWarnings("unchecked")
    @Override
    public EPowerTillerModel convertToEntityAttribute(Long id) {
        return super.convertToEntityAttribute(id);
    }

    @Override
    public Long convertToDatabaseColumn(EPowerTillerModel arg0) {
        return super.convertToDatabaseColumn(arg0);
    }


    public static List<EPowerTillerModel> values() {
        return getValues(EPowerTillerModel.class);
    }


    public static EPowerTillerModel getByCode(String code) {
        return getByCode(EPowerTillerModel.class, code);
    }

    public static EPowerTillerModel getById(long id) {
        return getById(EPowerTillerModel.class, id);
    }

    public static List<EPowerTillerModel> list() {
        List<EPowerTillerModel> list  = new ArrayList<>();
        list.add(NC_PLUS_X);
        return list;
    }
}
