package com.soma.mfinance.core.asset.model;

import com.soma.mfinance.core.quotation.model.Quotation;
import org.seuksa.frmk.model.entity.EntityA;
import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;

/**
 * @author tha.bunsath
 * 
 */
@Entity
@Table(name = "td_multi_asset_ingine_doc")
public class AddMultiAssetEngineDoc extends EntityA {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1199015204914472442L;
	/** */
	private Quotation quotation;
	private String assetEngineDc;
	/**
	 * @return the
	 */
	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "amti_eng_id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}
	/* 
	 * @return
	 */
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "quota_id")
	public Quotation getQuotation() {
		return quotation;
	}
	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}
	/**
	 * 
	 * @return
	 */
	@Column(name = "amti_engine_dc", nullable = true, length = 100)
	public String getAssetEngineDc() {
		return assetEngineDc;
	}
	public void setAssetEngineDc(String assetEngineDc) {
		this.assetEngineDc = assetEngineDc;
	}
}
