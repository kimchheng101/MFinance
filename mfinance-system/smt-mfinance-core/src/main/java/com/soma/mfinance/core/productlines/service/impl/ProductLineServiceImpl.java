package com.soma.mfinance.core.productlines.service.impl;

import com.soma.common.app.eref.EProductLineCode;
import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.productlines.service.ProductLineService;
import com.soma.mfinance.core.quotation.model.Quotation;
import org.springframework.stereotype.Service;

/**
 * @author by kimsuor.seang  on 10/26/2017.
 */

@Service
public class ProductLineServiceImpl implements ProductLineService {

    @Override
    public EProductLineCode getEProductLineCode(AssetRange assetRange) {
        EProductLineCode  eProductLineCode = null;
        if(assetRange != null){
            if(assetRange.getProductLine() != null){
                if(assetRange.getProductLine().getProductLineCode() != null){
                    if(assetRange.getProductLine().getProductLineCode().equals(EProductLineCode.KFP)){
                        eProductLineCode = EProductLineCode.KFP;
                    }else if(assetRange.getProductLine().getProductLineCode().equals(EProductLineCode.MFP)){
                        eProductLineCode = EProductLineCode.MFP;
                    }
                }
            }
        }
        return eProductLineCode;
    }

    @Override
    public EProductLineCode getEProductLineCodeByQuotation(Quotation quotation) {
        EProductLineCode eProductLineCode  = new EProductLineCode();
        if (quotation.getAsset() != null) {
            if (quotation.getAsset().getAssetRange() != null) {
                if (quotation.getAsset().getAssetRange().getAssetMake() != null) {
                    if (quotation.getAsset().getAssetRange().getAssetMake().getId().equals(EProductLineCode.MFP.getId())) {
                        eProductLineCode = EProductLineCode.MFP;
                    } else if (quotation.getAsset().getAssetRange().getAssetMake().getId().equals(EProductLineCode.KFP.getId())) {
                        eProductLineCode = EProductLineCode.KFP;
                    }
                }
            }
        }
        return eProductLineCode;
    }
}
