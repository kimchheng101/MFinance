package com.soma.mfinance.core.Enum;

import org.seuksa.frmk.model.eref.BaseERefData;

import javax.persistence.AttributeConverter;
import java.util.List;

/**
 * Created by b.chea on 2/25/2017.
 */
public class ECorrespondence extends BaseERefData implements AttributeConverter<ECorrespondence, Long> {

    public static final ECorrespondence CURRENT_ADDRESS = new ECorrespondence("CURRENT_ADDRESS", 1L);

    public ECorrespondence(){}

    public ECorrespondence(String code, long id){ super(code, id);}
    @Override
    public Long convertToDatabaseColumn(ECorrespondence attribute) {
        return super.convertToDatabaseColumn(attribute);
    }

    @Override
    public ECorrespondence convertToEntityAttribute(Long id) {
        return getById(ECorrespondence.class, id);
    }

    public static List<ECorrespondence> values() {
        return getValues(ECorrespondence.class);
    }

    public static ECorrespondence getByCode(String code) {
        return getByCode(ECorrespondence.class, code);
    }

    public static ECorrespondence getById(long id) {
        return (ECorrespondence) getById(ECorrespondence.class, Long.valueOf(id));
    }
}
