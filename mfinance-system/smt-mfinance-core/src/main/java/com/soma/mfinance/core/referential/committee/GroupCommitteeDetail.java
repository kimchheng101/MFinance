package com.soma.mfinance.core.referential.committee;

import com.soma.mfinance.core.asset.model.AssetRange;
import org.seuksa.frmk.model.EntityFactory;
import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;

/**
 * Created by Dang Dim
 * Date     : 31-Oct-17, 2:01 PM
 * Email    : d.dim@gl-f.com
 */

@Entity
@Table(name = "tu_group_committee_detail")
public class GroupCommitteeDetail extends EntityA {

    private GroupCommittee groupCommittee;
    private AssetRange assetRange;
    private Committee committee;

    public static GroupCommittee createInstance() {
        GroupCommittee instance = EntityFactory.createInstance(GroupCommittee.class);
        return instance;
    }

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "gro_com_det_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gro_com_id", nullable = false)
    public GroupCommittee getGroupCommittee() {
        return groupCommittee;
    }

    public void setGroupCommittee(GroupCommittee groupCommittee) {
        this.groupCommittee = groupCommittee;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ass_ran_id", nullable = false)
    public AssetRange getAssetRange() {
        return assetRange;
    }

    public void setAssetRange(AssetRange assetRange) {
        this.assetRange = assetRange;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "com_mit_id", nullable = false)
    public Committee getCommittee() {
        return committee;
    }

    public void setCommittee(Committee committee) {
        this.committee = committee;
    }
}
