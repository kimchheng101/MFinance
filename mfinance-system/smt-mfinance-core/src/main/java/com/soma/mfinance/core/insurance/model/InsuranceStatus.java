package com.soma.mfinance.core.insurance.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.seuksa.frmk.model.entity.EntityRefA;

/**
 * 
 * @author Riya.Pov
 *
 */
@Entity
@Table(name = "tu_insurance_status")
public class InsuranceStatus extends EntityRefA{

	/**
	 * 
	 */
	private static final long serialVersionUID = 537563216751421728L;
	
	private boolean defaultStatus;
	
	/**
	 * @see org.seuksa.frmk.model.entity.EntityA#getId()
	 */
	@Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ins_sta_id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}
	
	/**
	 * @see EntityRefA#getCode()
	 */
	@Column(name = "ins_sta_code", nullable = false, length=10)
	@Override
	public String getCode() {
		return super.getCode();
	}

	/**
	 * @see EntityRefA#getDesc()
	 */
	@Column(name = "ins_sta_desc", nullable = false, length=100)
	@Override
    public String getDesc() {
        return super.getDesc();
    }
	
	/**
	 * @see EntityRefA#getDescEn()
	 */
    @Override
    @Column(name = "ins_sta_desc_en", nullable = false, length=100)
    public String getDescEn() {
        return super.getDescEn();
    }
    
    /**
	 * @return the defaultStatus
	 */
    @Column(name = "ins_sta_bl_default_status", nullable = false, columnDefinition = "boolean default false")
	public boolean isDefaultStatus() {
		return defaultStatus;
	}

	/**
	 * @param defaultStatus the defaultStatus to set
	 */
	public void setDefaultStatus(boolean defaultStatus) {
		this.defaultStatus = defaultStatus;
	}

}
