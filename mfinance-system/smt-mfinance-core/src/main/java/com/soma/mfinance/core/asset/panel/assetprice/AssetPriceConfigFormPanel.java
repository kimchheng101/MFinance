package com.soma.mfinance.core.asset.panel.assetprice;

import com.soma.mfinance.core.asset.model.AssetMake;
import com.soma.mfinance.core.asset.model.AssetModel;
import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.auction.model.AssetPrice;
import com.soma.mfinance.core.custom.component.CustomEntityComboBox;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.soma.mfinance.core.asset.model.AssetMake;
import com.soma.mfinance.core.asset.model.AssetModel;
import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.auction.model.AssetPrice;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.Entity;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by cheasocheat on 3/23/17.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AssetPriceConfigFormPanel extends AbstractFormPanel{
    private AssetPrice assetPrice;
    private TextField txtCode;
    private TextField txtDesc;
    private TextField txtDescEn;
    private TextField txtAssetPrice;
    private TextField txtAssetSecHandPrice;
    private EntityComboBox<AssetMake> cbxAssetMake;
    private EntityComboBox<AssetRange> cbxAssetRange;
    private EntityComboBox<AssetModel> cbxAssetModel;

    @PostConstruct
    public void PostConstruct(){
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addSaveClickListener(this);
    }

    public void assignValues(Long type_id) {
        super.reset();
        if(type_id != null){
            assetPrice = ENTITY_SRV.getById(AssetPrice.class, type_id);
            txtCode.setValue(assetPrice.getCode());
            txtDesc.setValue(assetPrice.getDesc());
            txtDescEn.setValue(assetPrice.getDescEn());
            txtAssetPrice.setValue(MyNumberUtils.formatDoubleToString(assetPrice.getPrice()));
            cbxAssetMake.setSelectedEntity(assetPrice.getAssetMake());
            cbxAssetRange.setSelectedEntity(assetPrice.getAssetRange());
            cbxAssetModel.setSelectedEntity(assetPrice.getAssetModel());
            txtAssetSecHandPrice.setValue(MyNumberUtils.formatDoubleToString(MyNumberUtils.getDouble(assetPrice.getSecondHandPrice())));
        }
    }

    @Override
    protected Entity getEntity() {
        assetPrice.setCode(txtCode.getValue());
        assetPrice.setDesc(txtDesc.getValue());
        assetPrice.setDescEn(txtDescEn.getValue());
        assetPrice.setPrice(getDouble(txtAssetPrice,0));
        assetPrice.setSecondHandPrice(getDouble(txtAssetSecHandPrice,0));
        assetPrice.setAssetModel(cbxAssetModel.getSelectedEntity());
        assetPrice.setAssetMake(cbxAssetMake.getSelectedEntity());
        assetPrice.setAssetRange(cbxAssetRange.getSelectedEntity());
        return assetPrice;
    }

    @Override
    protected com.vaadin.ui.Component createForm() {
        FormLayout layout = new FormLayout();

        txtCode = ComponentFactory.getTextField(I18N.message("code"), true, 60, 200);
        txtDesc = ComponentFactory.getTextField(I18N.message("desc"), false, 60, 200);
        txtDescEn = ComponentFactory.getTextField(I18N.message("desc.en"), true, 60, 200);
        txtAssetPrice = ComponentFactory.getTextField(I18N.message("asset.price"), true, 60, 200);
        txtAssetSecHandPrice = ComponentFactory.getTextField(I18N.message("asset.secondhand.price"), true, 60, 200);

        cbxAssetMake = new CustomEntityComboBox<>(AssetMake.class , I18N.message("asset.make"),"descEn" , "");
        cbxAssetMake.setWidth(200, Unit.PIXELS);
        cbxAssetMake.setRequired(true);
        cbxAssetMake.renderer();

        cbxAssetRange = new CustomEntityComboBox<>(AssetRange.class, I18N.message("asset.range"),"descEn" , "");
        cbxAssetRange.setWidth(200, Unit.PIXELS);
        cbxAssetRange.setRequired(true);

        cbxAssetModel = new CustomEntityComboBox<>(AssetModel.class, I18N.message("asset.model"), "descEn", "");
        cbxAssetModel.setWidth(200, Unit.PIXELS);
        cbxAssetModel.setRequired(true);

        ((CustomEntityComboBox)cbxAssetMake).setFilteringComboBox(cbxAssetRange, AssetRange.class);
        ((CustomEntityComboBox)cbxAssetRange).setFilteringComboBox(cbxAssetModel, AssetModel.class);

        layout.addComponent(txtCode);
        layout.addComponent(cbxAssetMake);
        layout.addComponent(cbxAssetRange);
        layout.addComponent(cbxAssetModel);
        layout.addComponent(txtDesc);
        layout.addComponent(txtDescEn);
        layout.addComponent(txtAssetPrice);
        layout.addComponent(txtAssetSecHandPrice);

        return layout;
    }

    @Override
    public void reset() {
        super.reset();
        assetPrice = new AssetPrice();
        txtCode.setValue("");
        cbxAssetMake.setSelectedEntity(null);
        cbxAssetRange.setSelectedEntity(null);
        cbxAssetModel.setSelectedEntity(null);
        txtAssetSecHandPrice.setValue("");
        txtDesc.setValue("");
        txtDescEn.setValue("");
        txtAssetPrice.setValue("");
        markAsDirty();
    }

    @Override
    protected boolean validate() {
        checkMandatoryField(txtCode, I18N.message("code"));
        checkMandatorySelectField(cbxAssetMake, I18N.message("asset.make"));
        checkMandatorySelectField(cbxAssetRange, I18N.message("asset.range"));
        checkMandatorySelectField(cbxAssetModel, I18N.message("asset.model"));
        checkMandatoryField(txtDescEn, I18N.message("desc.en"));
        checkMandatoryField(txtAssetPrice, I18N.message("asset.price"));
        return errors.isEmpty();
    }
}
