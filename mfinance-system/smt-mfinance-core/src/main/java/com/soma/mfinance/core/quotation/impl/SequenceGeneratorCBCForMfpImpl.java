package com.soma.mfinance.core.quotation.impl;

import com.soma.mfinance.core.quotation.SequenceGenerator;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/23/2017
 * Name  : 2:37 PM
 * Email : k.seang@gl-f.com
 */
public class SequenceGeneratorCBCForMfpImpl implements SequenceGenerator {
    private Long sequence;
    private String prefix;

    /**
     * @param prefix
     * @param sequence
     */
    public SequenceGeneratorCBCForMfpImpl(String prefix,Long sequence) {
        this.prefix = prefix;
        this.sequence = sequence;
    }

    @Override
    public String generate() {
        String sequenceNumber = "0000000" + sequence;
        return prefix  + sequenceNumber.substring(sequenceNumber.length() - 7);
    }
}
