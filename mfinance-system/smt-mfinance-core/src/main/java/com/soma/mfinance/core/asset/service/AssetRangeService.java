package com.soma.mfinance.core.asset.service;

import com.soma.mfinance.core.asset.model.AssetMake;
import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.financial.model.ProductLine;
import com.soma.mfinance.core.asset.model.AssetMake;
import com.soma.mfinance.core.asset.model.AssetRange;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.service.BaseEntityService;

import java.util.List;

/**
 * Asset range service interface
 *
 * @author kimsuor.seang
 */
public interface AssetRangeService extends BaseEntityService {

    /**
     * saveOrUpdate Asset Range
     *
     * @param assetRange
     */
    void saveOrUpdateAssetRange(AssetRange assetRange);

    /**
     * @param assetMake
     * @return
     */
    List<AssetRange> getAssetRangesByAssetMake(AssetMake assetMake);


    BaseRestrictions getBaseRestrictionsAssetRangesByProductLine(ProductLine productLine);

}
