package com.soma.mfinance.core.shared.system;

import java.text.DecimalFormat;

import org.apache.commons.lang3.StringUtils;

import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.ui.TextField;

/**
 *
 * @author Sok.vina
 *
 */
public class AutoTextField extends TextField implements BlurListener {
    /**
     *
     */
    private static final long serialVersionUID = -9014412239844723353L;

    public AutoTextField() {
        super();
        this.addBlurListener(this);
        this.setImmediate(true);
        this.setInputPrompt("000,000,000,000.00");
    }
    public AutoTextField(String width) {
        super();
        this.addBlurListener(this);
        this.setImmediate(true);
        this.setInputPrompt("000,000,000,000.00");
        this.setWidth(width);
    }
    /**
     * @param value
     * @return
     */
    private static Double getDoubleUSD(String value) {
        if (value != null && StringUtils.isNotEmpty(value)) {
            return Double.valueOf(value.replaceAll(",", ""));
        }
        return 0d;
    }
    @Override
    public void blur(BlurEvent event) {
        // TODO Auto-generated method stub
        String value = this.getValue();
        DecimalFormat stringValue = new DecimalFormat("###,###,###,###.00");
        if (StringUtils.isEmpty(value)) {
            this.setValue("");
        } else if(getDoubleUSD(value) == 0d || getDoubleUSD(value) == 0.00){
            this.setValue(getDoubleUSD(value).toString());
        } else {
            this.setValue(stringValue.format(getDoubleUSD(value)));
        }

    }
}
