package com.soma.mfinance.core.shared.quotation;

import com.soma.mfinance.core.shared.FMEntityField;

/**
 * @author kimsuor.seang
 */
public interface QuotationEntityField extends FMEntityField {

    String SEQUENCE = "sequence";
    String REFERENCE = "reference";
    String SUBMISSION_DATE = "submissionDate";
    String ACCEPTATION_DATE = "acceptationDate";
    String ACTIVATION_DATE = "activationDate";
    String REJECTION_DATE = "rejectionDate";
    String CONTRACT_START_DATE = "contractStartDate";
    String FIRST_PAYMENT_DATE = "firstPaymentDate";
    String USER_LOCKED = "userLocked";
    String FIRST_SUBMISSION_DATE = "firstSubmissionDate";
    String UNDERWRITER = "underwriter";
    String UNDERWRITER_SUPERVISOR = "underwriterSupervisor";
    String QUOTATION_VA_ID = "migrationID";
    String CHECKER_ID = "checkerID";

    //String ID = "id";
    String START_DATE = "START DATE";
    String END_DATE = "END DATE";

    String PERFORM_USER = "performUser";
    String EXECUTED_DATE = "executedDate";
    String EXECUTED = "executed";

}
