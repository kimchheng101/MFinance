package com.soma.mfinance.core.financial.service;

import java.util.Date;

import com.soma.finance.services.shared.AmortizationSchedules;
import com.soma.finance.services.shared.CalculationParameter;

/**
 * Calculation Service
 * @author kimsuor.seang
 */
public interface FinanceCalculationService {

	/**
	 * Calculate installment amount
	 * @return
	 */
	double getInstallmentPayment(CalculationParameter calculationParameter);
	
	/**
	 * Calculate total interest
	 * @return
	 */
	double getTotalInterest(CalculationParameter calculationParameter);
	
	/**
	 * Get amortization schedule
	 * @param startDate start date
	 * @param firstInstallmentDate
	 * @param calculationParameter
	 * @return
	 */
	AmortizationSchedules getAmortizationSchedules(Date startDate, Date firstInstallmentDate, CalculationParameter calculationParameter);
}
