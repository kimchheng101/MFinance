package com.soma.mfinance.core.applicant.model;

import com.soma.mfinance.core.address.model.ScaleOfBusiness;
import com.soma.mfinance.core.shared.system.BusinessTypes;
import com.soma.mfinance.core.shared.system.WhoseJob;
import com.soma.ersys.core.hr.model.address.Address;
import com.soma.ersys.core.hr.model.address.BaseAddress;

import javax.persistence.*;

/**
 * @author kimsuor.seang
 * @modify kimsuor.seang on 20/10/2018
 */
@Entity
@Table(name = "td_employment")
public class Employment extends AbstractEmployment {

    private static final long serialVersionUID = -911299500876813917L;

    private Individual individual;
    private Address address;
    private String workPlaceName;
    private Double basicSalary;
    private WhoseJob whoseJob;
    private BusinessTypes businessTypes;
    private ScaleOfBusiness scaleOfBusiness;
    private Double netIncome;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "emp_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ind_id")
    public Individual getIndividual() {
        return individual;
    }
    public void setIndividual(Individual individual) {
        this.individual = individual;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "add_id")
    public Address getAddress() {
        return address;
    }
    public void setAddress(Address address) {
        this.address = address;
    }

    @Transient
    @Override
    public BaseAddress getBaseAddress() {
        return address;
    }

    @Column(name = "work_place_name", nullable = true)
    public String getWorkPlaceName() {
        return this.workPlaceName;
    }
    public void setWorkPlaceName(String workPlaceName) {
        this.workPlaceName = workPlaceName;
    }

    @Column(name = "basic_salary", nullable = true, length = 30)
    public Double getBasicSalary() {
        return this.basicSalary;
    }
    public void setBasicSalary(Double basicSalary) {
        this.basicSalary = basicSalary;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "whojob_id")
    public WhoseJob getWhoseJob() {
        return whoseJob;
    }
    public void setWhoseJob(WhoseJob whoseJob) {
        this.whoseJob = whoseJob;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "busty_id")
    public BusinessTypes getBusinessTypes() {
        return businessTypes;
    }
    public void setBusinessTypes(BusinessTypes businessTypes) {
        this.businessTypes = businessTypes;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sclbus_id")
    public ScaleOfBusiness getScaleOfBusiness() {
        return scaleOfBusiness;
    }
    public void setScaleOfBusiness(ScaleOfBusiness scaleOfBusiness) {
        this.scaleOfBusiness = scaleOfBusiness;
    }

    @Column(name = "emplo_am_netincome", nullable = true)
    public Double getNetIncome() {
        return netIncome;
    }
    public void setNetIncome(Double netIncome) {
        this.netIncome = netIncome;
    }
}
