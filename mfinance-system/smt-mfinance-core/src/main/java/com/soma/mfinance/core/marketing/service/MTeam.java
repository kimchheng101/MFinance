package com.soma.mfinance.core.marketing.service;

import org.seuksa.frmk.model.entity.MEntityA;


/**
 * Meta data of com.soma.mfinance.core.marketing.model.Team
 * @author kimsuor.seang
 */
public interface MTeam extends MEntityA {
	
	public final static String DESCRIPTION = "description";
	public final static String EMPLOYEES = "employees";

}
