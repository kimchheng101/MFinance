package com.soma.mfinance.core.actor.service;

import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.actor.model.Actor;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.dealer.model.Dealer;

/**
 * Actor service interface
 * @author kimsuor.seang
 */
public interface ActorService extends BaseEntityService {

	Actor getFinancialCompany();
	Actor getActorByDealer(Dealer dealer);
	Actor getActorByApplicant(Applicant applicant);
}
