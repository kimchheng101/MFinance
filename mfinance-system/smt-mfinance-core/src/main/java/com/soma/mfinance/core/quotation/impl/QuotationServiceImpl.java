package com.soma.mfinance.core.quotation.impl;

import com.soma.common.app.tools.UserSessionManager;
import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.applicant.model.Employment;
import com.soma.mfinance.core.applicant.service.ApplicantService;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.asset.service.AssetService;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.service.ActivationContractMfpService;
import com.soma.mfinance.core.contract.service.ContractService;
import com.soma.mfinance.core.contract.service.SequenceManager;
import com.soma.mfinance.core.document.service.DocumentService;
import com.soma.mfinance.core.quotation.SequenceGenerator;
import com.soma.mfinance.core.quotation.dao.QuotationDao;
import com.soma.mfinance.core.quotation.model.*;
import com.soma.mfinance.core.shared.conf.AppConfig;
import com.soma.mfinance.core.shared.conf.UWScoreConfig;
import com.soma.mfinance.core.shared.exception.ValidationFieldsException;
import com.soma.mfinance.core.shared.exception.quotation.ExpireQuotationException;
import com.soma.mfinance.core.shared.exception.quotation.InvalidQuotationException;
import com.soma.mfinance.core.shared.system.FMProfile;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.mfinance.third.creditbureau.CreditBureauService;
import com.soma.mfinance.third.creditbureau.exception.ErrorCreditBureauException;
import com.soma.mfinance.third.creditbureau.exception.InvokedCreditBureauException;
import com.soma.mfinance.third.creditbureau.exception.ParserCreditBureauException;
import com.soma.ersys.core.hr.model.address.Address;
import com.soma.ersys.core.hr.model.eref.EOptionality;
import com.soma.ersys.core.hr.model.eref.ETypeAddress;
import com.soma.frmk.security.model.SecProfile;
import com.soma.frmk.security.model.SecUser;
import com.vaadin.ui.Notification;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.model.entity.CrudAction;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.exception.DaoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;

import static com.soma.mfinance.core.helper.FinServicesHelper.QUO_SRV;
import static com.soma.frmk.helper.SeuksaServicesHelper.ENTITY_SRV;

/**
 * Quotation service implementation
 *
 * @author kimsuor.seang
 * @author kimsuor.seang (modified) Date: 06/09/2017
 */
@Service("quotationService")
public class QuotationServiceImpl extends BaseEntityServiceImpl implements com.soma.mfinance.core.quotation.QuotationService {
    /**  */
    private static final long serialVersionUID = 553476005407096093L;

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private QuotationDao quotationDao;

    @Autowired
    private ApplicantService applicantService;

    @Autowired
    private AssetService assetService;

    @Autowired
    private DocumentService documentService;

    @Autowired
    private CreditBureauService creditBureauService;

    @Autowired
    private ContractService contractService;

    @Autowired
    private ActivationContractMfpService activationContractMfpService;


    public QuotationServiceImpl() {
        super();
    }

    /**
     * Save or Update quotation
     *
     * @param quotation
     */
    @Override
    @Transactional(rollbackFor = {Exception.class})
    public Quotation saveOrUpdateQuotation(Quotation quotation) {
        logger.debug("[>> saveOrUpdateQuotation]");
        Assert.notNull(quotation, "Quotation could not be null.");
        try {
            Asset asset = assetService.saveOrUpdateAsset(quotation.getAsset());
            if (asset.getId() != null) {
                if (ProfileUtil.isAuctionController() && asset.getAssetAppraisal() != null) {
                    //To implements
                    quotation.setTeAppraisalEstimateAmount(asset.getTeAssetApprPrice());
                    quotation.setTiAppraisalEstimateAmount(asset.getTiAssetApprPrice());
                }
                if (QuotationWkfStatus.QUO.equals(quotation.getWkfStatus()) && quotation.getQuotationDate() == null) {
                    quotation.setQuotationDate(DateUtils.today());
                }
                saveOrUpdate(quotation);
                saveOrUpdateQuotationApplicants(quotation);
                saveOrUpdateQuotationServices(quotation);
                saveOrUpdateQuotationDocuments(quotation);
            }

        } catch (Exception e) {
            logger.error("Error during save quotation .... !");
        }
        logger.debug("[<< saveOrUpdateQuotation]");

        return quotation;
    }

    /**
     * @param quotation
     * @return
     */
    @Override
    public Quotation saveChangeGuarantor(Quotation quotation) {
        try {
            List<QuotationApplicant> quotationApplicants = quotation.getQuotationApplicants();
            for (QuotationApplicant quotationApplicant : quotationApplicants) {
                if (quotationApplicant.getApplicantType().equals(EApplicantType.G)) {
                    Applicant applicant = quotationApplicant.getApplicant();
                    applicantService.saveOrUpdateApplicant(applicant);
                    quotationApplicant.setQuotation(quotation);
                    saveOrUpdate(quotationApplicant);
                }
            }
            QuotationApplicant oldQuotationGuarantor = quotation.getQuotationApplicant(EApplicantType.O);
            if (oldQuotationGuarantor != null) {
                saveOrUpdate(oldQuotationGuarantor);
            }
            saveOrUpdateGuarantorQuotationDocuments(quotation);
        } catch (DaoException e) {
            e.printStackTrace();
        } finally {
            clear();
        }
        return quotation;
    }

    /**
     * Save or update quotation applicants
     *
     * @param quotation
     */
    private void saveOrUpdateQuotationApplicants(Quotation quotation) {
        logger.debug("[>> saveOrUpdateQuotationApplicants]");
        List<QuotationApplicant> quotationApplicants = quotation.getQuotationApplicants();
        if (quotationApplicants != null && !quotationApplicants.isEmpty()) {
            for (QuotationApplicant quotationApplicant : quotationApplicants) {
                if (CrudAction.DELETE.equals(quotationApplicant.getCrudAction())) {
                    delete(quotationApplicant);
                } else {
                    Applicant applicant = quotationApplicant.getApplicant();
                    if (quotationApplicant.isSameApplicantAddress()) {
                        Address addressApplicant = quotation.getApplicant().getIndividual().getMainAddress();
                        Address addressGuarantor = quotationApplicant.getApplicant().getIndividual().getMainAddress();
                        if (addressApplicant != null && addressGuarantor != null) {
                            addressGuarantor.setProvince(addressApplicant.getProvince());
                            addressGuarantor.setDistrict(addressApplicant.getDistrict());
                            addressGuarantor.setCommune(addressApplicant.getCommune());
                            addressGuarantor.setVillage(addressApplicant.getVillage());
                            addressGuarantor.setHouseNo(addressApplicant.getHouseNo());
                            addressGuarantor.setStreet(addressApplicant.getStreet());
                        }
                    }
                    quotationApplicant.setApplicant(applicantService.saveOrUpdateApplicant(applicant));
                    quotationApplicant.setQuotation(quotation);
                    saveOrUpdate(quotationApplicant);
                    if (quotationApplicant.getApplicantType().equals(EApplicantType.C) && quotation.getApplicant() == null) {
                        quotation.setApplicant(applicant);
                        saveOrUpdate(quotation);
                    }
                }
            }
        }
        logger.debug("[<< saveOrUpdateQuotationApplicants]");
    }

    /**
     * Save or update quotation Services
     *
     * @param quotation
     */
    private void saveOrUpdateQuotationServices(Quotation quotation) {
        logger.debug("[>> saveOrUpdateQuotationServices]");
        List<QuotationService> quotationServices = quotation.getQuotationServices();
        if (quotationServices != null && !quotationServices.isEmpty()) {
            for (QuotationService quotationService : quotationServices) {
                if (CrudAction.DELETE.equals(quotationService.getCrudAction())) {
                    delete(quotationService);
                } else {
                    quotationService.setQuotation(quotation);
                    saveOrUpdate(quotationService);
                }
            }
        }
        logger.debug("[<< saveOrUpdateQuotationServices]");
    }

    /**
     * Save or update quotation documents
     *
     * @param quotation
     */
    private void saveOrUpdateGuarantorQuotationDocuments(Quotation quotation) {
        List<QuotationDocument> quotationDocuments = new ArrayList<>();
        for (QuotationDocument quotationDocument : quotation.getQuotationDocuments()) {
            if (quotationDocument.getDocument().getApplicantType().equals(EApplicantType.G)) {
                quotationDocuments.add(quotationDocument);
            }
        }
        saveOrUpdateQuotationDocuments(quotation, quotationDocuments);
    }

    /**
     * Save or update quotation documents
     *
     * @param quotation
     */
    @Override
    public void saveOrUpdateQuotationDocuments(Quotation quotation) {
        saveOrUpdateQuotationDocuments(quotation, quotation.getQuotationDocuments());
    }

    /**
     * Save or update quotation documents
     *
     * @param quotation
     */
    private void saveOrUpdateQuotationDocuments(Quotation quotation, List<QuotationDocument> quotationDocuments) {
        logger.debug("[>> saveOrUpdateQuotationDocuments]");
        if (quotationDocuments != null && !quotationDocuments.isEmpty()) {
            String tmpDir = AppConfig.getInstance().getConfiguration().getString("specific.tmpdir");
            String documentDir = AppConfig.getInstance().getConfiguration().getString("document.path");
            for (QuotationDocument quotationDocument : quotationDocuments) {
                if (CrudAction.DELETE.equals(quotationDocument.getCrudAction())) {
                    if (StringUtils.isNotEmpty(quotationDocument.getPath())) {
                        File documentFile = new File(documentDir + "/" + quotationDocument.getPath());
                        if (documentFile.exists()) {
                            documentFile.delete();
                        }
                    }
                    delete(quotationDocument);
                } else {
                    quotationDocument.setQuotation(quotation);
                    String pathFileName = quotationDocument.getPath();
                    if (pathFileName != null && pathFileName.indexOf("tmp_") != -1) {
                        String fileName = quotationDocument.getDocument().getApplicantType().getCode() + "_" + quotationDocument.getDocument().getId() + "_" + pathFileName.substring(pathFileName.indexOf("/") + 1);
                        File tmpDocumentFile = new File(tmpDir + "/" + pathFileName);
                        File documentFileDir = new File(documentDir + "/" + quotation.getId());
                        if (!documentFileDir.exists()) {
                            documentFileDir.mkdirs();
                        }
                        File f = new File(documentDir + "/" + quotation.getId() + "/" + fileName);
                        if (f.exists()) {
                            f.delete();
                        }
                        //tmpDocumentFile.renameTo(new File(documentDir + "/" + quotation.getId() + "/" + fileName));
                        try {
                            FileCopyUtils.copy(tmpDocumentFile, new File(documentDir + "/" + quotation.getId() + "/" + fileName));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        quotationDocument.setPath(quotation.getId() + "/" + fileName);
                    }
                    documentService.saveOrUpdate(quotationDocument);

                }
            }
        }
        logger.debug("[<< saveOrUpdateQuotationDocuments]");
    }

    /**
     * Submit quotation to underwriter
     *
     * @return
     * @throws ValidationFieldsException
     * @throws InvalidQuotationException
     * @throws ExpireQuotationException
     */
    @Override
    public boolean submitQuotation(Quotation quotation) {
        if (quotation != null) {
            if (quotation.getWkfStatus().equals(QuotationWkfStatus.ACS)) {
                List<SecUser> secUsers = getSecUserByProfile(FMProfile.UW);
                SecUser secUser = getUnderwriterAndSupervisorToAssign(secUsers);
                if (quotation.getUnderwriter() == null) {
                    quotation.setUnderwriter(secUser);
                }
            }
            quotation = saveOrUpdateQuotation(quotation);
            quotation = changeQuotationNextStatus(quotation);
            Notification.show("", "Application has been submitted to " + quotation.getWkfStatus().getDescEn(), Notification.Type.HUMANIZED_MESSAGE);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get Quotation object by it's reference
     *
     * @param reference
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public Quotation getByReference(String reference) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.or(Restrictions.eq("reference", reference), Restrictions.eq("externalReference", reference)));
        List<Quotation> quotations = list(restrictions);
        if (quotations != null && !quotations.isEmpty()) {
            return quotations.get(0);
        }
        return null;
    }

    /**
     * Change quotation status
     *
     * @param quotation
     * @param newQuotationStatus
     * @return
     */
    @Override
    public EWkfStatus changeQuotationStatus(Quotation quotation, EWkfStatus newQuotationStatus) {
        logger.debug("[>> changeQuotationStatus]");
        EWkfStatus previousWkfStatus = quotation.getWkfStatus();
        quotation.setPreviousWkfStatus(previousWkfStatus);
        quotation.setWkfStatus(newQuotationStatus);
        if (ProfileUtil.isUnderwriter()) {
            quotation.setUnderwriter(UserSessionManager.getCurrentUser());
            quotation.setUnderwriterSubmitDate(DateUtils.today());
        }
        saveOrUpdate(quotation);
        logger.debug("[<< changeQuotationStatus]");
        return newQuotationStatus;
    }

    /**
     * Change quotation status
     *
     * @param quotaId
     * @param newQuotationStatus
     * @return
     */
    @Override
    public EWkfStatus changeQuotationStatus(Long quotaId, EWkfStatus newQuotationStatus) {
        Quotation quotation = getById(Quotation.class, quotaId);
        return changeQuotationStatus(quotation, newQuotationStatus);
    }

    /**
     * @param quotation
     * @param comments
     */
    @Override
    @Transactional(rollbackFor = {Exception.class})
    public void saveUnderwriter(Quotation quotation, List<Comment> comments) {
        logger.debug(">> Save Underwriter ..... ! ");
        saveOrUpdateQuotationDocuments(quotation);
        saveOrUpdate(quotation);
        if (comments != null && !comments.isEmpty()) {
            saveOrUpdateBulk(comments);
        }
        logger.debug("<< Finish Save Underwriter ..... ");
    }

    /**
     * @param quotation
     * @param newStatus
     * @param comments
     */

    @Override
    public void saveUnderwriterDecision(Quotation quotation, EWkfStatus newStatus, List<Comment> comments) {
        logger.debug("[>> saveUnderwriterDecision]");
        saveOrUpdateQuotationDocuments(quotation);
        if (newStatus.equals(QuotationWkfStatus.APU) || newStatus.equals(QuotationWkfStatus.APS)) {
            quotation.setAcceptationDate(DateUtils.today());
        } else if (newStatus.equals(QuotationWkfStatus.REJ)) {
            quotation.setRejectDate(DateUtils.today());
        }
        changeQuotationStatus(quotation, newStatus);
        if (comments != null && !comments.isEmpty()) {
            saveOrUpdateBulk(comments);
        }

        logger.debug("[<< saveUnderwriterDecision]");
    }

    /**
     * @param quotation
     * @param newStatus
     */
    @Override
    public void saveManagementDecision(Quotation quotation, EWkfStatus newStatus) {
        logger.debug("[>> saveUnderwriterDecision]");
        if (newStatus.equals(QuotationWkfStatus.APV) || newStatus.equals(QuotationWkfStatus.REJ) ||
                newStatus.equals(QuotationWkfStatus.AWT)) {
            SecUser manager = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (newStatus.equals(QuotationWkfStatus.APV) || newStatus.equals(QuotationWkfStatus.AWT)) {
                quotation.setAcceptationDate(DateUtils.today());
            } else if (newStatus.equals(QuotationWkfStatus.APU) || newStatus.equals(QuotationWkfStatus.APS) ||
                    newStatus.equals(QuotationWkfStatus.REJ)) {
                quotation.setRejectDate(DateUtils.today());
            }
            quotation.setManager(manager);
            changeQuotationStatus(quotation, newStatus);
        }
        logger.debug("[<< saveUnderwriterDecision]");
    }

    /**
     * @param quotation
     */
    @Override
    public void saveDocumentControllerDecision(Quotation quotation) {
        SecUser documentController = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        quotation.setDocumentController(documentController);
        saveOrUpdate(quotation.getAsset());
        saveOrUpdate(quotation);
    }

    /**
     * @param quotaId
     */
    @Override
    public void activateQuotation(Long quotaId, Date contractStartDate, Date firstPaymentDate) {
        logger.debug("[>> Starting activate quotation ..... [ " + quotaId + "] ");
        Quotation quotation = getById(Quotation.class, quotaId);
        Assert.notNull(quotation, "Quotation object could be not null ...! ...");

        logger.debug("[>> activate Quotation .....]");

        quotation.setActivationDate(DateUtils.today());
        quotation.setContractStartDate(contractStartDate);
        quotation.setFirstDueDate(firstPaymentDate);

        if (StringUtils.isEmpty(quotation.getReference())) {
            Long sequence = SequenceManager.getInstance().getSequenceQuotationMfp();
          /*  if (quotation.getFinancialProduct() != null && FIN_CODE_PUBLIC.equals(quotation.getFinancialProduct().getCode())) {
                sequence = SequenceManager.getInstance().getSequenceQuotationMfpPub();
            }*/
            String yearLabel = DateUtils.getDateLabel(DateUtils.today(), "yy");
            SequenceGenerator sequenceGenerator = new QuotationReferenceGeneratorImpl(quotation.getDealer(), quotation.getFinancialProduct(), yearLabel, sequence);
            quotation.setReference(sequenceGenerator.generate());
        }
//        quotation.setContract(activationContractMfpService.activateContract(quotation));
        changeQuotationNextStatus(quotation);
        logger.debug("[<< activate contract .....]");
    }

    /**
     * @param quotation
     */
    @Override
    public void decline(Quotation quotation) {
        logger.debug("[>> decline]");
        quotation.setDecline(true);
        quotation.setDeclineDate(DateUtils.today());
        changeQuotationStatus(quotation, QuotationWkfStatus.DEC);
        logger.debug("[<< decline]");
    }

    /**
     * @param quotation
     */
    @Override
    public void reject(Quotation quotation) {
        logger.debug("[>> reject]");
        quotation.setRejectDate(DateUtils.today());
        changeQuotationStatus(quotation, QuotationWkfStatus.REJ);
        logger.debug("[<< reject]");
    }

    /**
     * @param quotation
     */
    @Override
    public void changeAsset(Quotation quotation) {
        logger.debug("[>> changeAsset]");
        quotation.setValid(false);
        changeQuotationStatus(quotation, QuotationWkfStatus.QUO);
        logger.debug("[<< changeAsset]");
    }

    /**
     * Save or update quotation support decisions
     *
     * @param quotation
     */
    @Override
    public void saveOrUpdateQuotationSupportDecisions(Quotation quotation) {
        logger.debug("[>> saveOrUpdateQuotationSupportDecisions]");
        List<QuotationSupportDecision> quotationSupportDecisions = quotation.getQuotationSupportDecisions();
        if (quotationSupportDecisions != null && !quotationSupportDecisions.isEmpty()) {
            for (QuotationSupportDecision quotationSupportDecision : quotationSupportDecisions) {
                if (CrudAction.DELETE.equals(quotationSupportDecision.getCrudAction())) {
                    delete(quotationSupportDecision);
                } else {
                    logger.debug("Add/update support decision [" + quotationSupportDecision.getSupportDecision().getCode() + "]");
                    quotationSupportDecision.setQuotation(quotation);
                    saveOrUpdate(quotationSupportDecision);
                }
            }
        }
        logger.debug("[<< saveOrUpdateQuotationSupportDecisions]");
    }

    /**
     * @param quotation
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public boolean isGuarantorRequired(Quotation quotation) {
        boolean guarantorRequired = false;
        if (quotation.getFinancialProduct() != null && quotation.getFinancialProduct().getGuarantor() != null) {
            guarantorRequired = EOptionality.MANDATORY.equals(quotation.getFinancialProduct().getGuarantor());
        }
        return guarantorRequired;
    }

    @Override
    public BaseEntityDao getDao() {
        return quotationDao;
    }

    /**
     * Call credit bureau interface
     *
     * @param quotaId
     * @param parameters
     */
    @Override
    public Boolean invokeCreditBureau(Long quotaId, Map<String, Object> parameters) throws InvokedCreditBureauException,
            ErrorCreditBureauException, ParserCreditBureauException {

        Quotation quotation = getById(Quotation.class, quotaId);

        String response = null;
        Boolean status = Boolean.TRUE;
        try {
            response = creditBureauService.enquiry(quotation, parameters);
        } catch (InvokedCreditBureauException e) {
            logger.error("[Credit Bureau]", e);
            throw e;
        }

        if (StringUtils.isNotEmpty(response)) {
            QuotationExtModule quotationExtModule = new QuotationExtModule();
            quotationExtModule.setQuotation(quotation);
            quotationExtModule.setExtModule(EExtModule.CREDIT_BURO);
            quotationExtModule.setStatus(status);
            quotationExtModule.setProcessDate(DateUtils.today());
            quotationExtModule.setResult(response);
            quotationExtModule.setReference(creditBureauService.getReference(response));
            quotationExtModule.setApplicantType((EApplicantType) parameters.get(EApplicantType.class.toString()));
            SecUser processByUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            quotationExtModule.setProcessByUser(processByUser);
            saveOrUpdate(quotationExtModule);

        } else {
            status = Boolean.FALSE;
        }

        return status;
    }

    /**
     * Get quotations list by commune address
     */
    @Transactional(readOnly = true)
    public List<Quotation> getQuotationsByCommune(Long commuId) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addAssociation("quotationApplicants", "quoapp", JoinType.INNER_JOIN);
        restrictions.addAssociation("quoapp.applicant", "app", JoinType.INNER_JOIN);
        restrictions.addAssociation("app.applicantAddresses", "appaddr", JoinType.INNER_JOIN);
        restrictions.addAssociation("appaddr.address", "addr", JoinType.INNER_JOIN);

        restrictions.addCriterion("appaddr.addressType", ETypeAddress.MAIN);
        restrictions.addCriterion("addr.commune.id", commuId);

        List<Quotation> quotations = list(restrictions);
        return quotations;
    }

    /**
     * @param quotaId
     * @return
     */
    @Transactional(readOnly = true)
    public List<QuotationExtModule> getQuotationExtModules(Long quotaId, EApplicantType applicantType) {
        BaseRestrictions<QuotationExtModule> restrictions = new BaseRestrictions<QuotationExtModule>(QuotationExtModule.class);
        restrictions.addCriterion(Restrictions.eq("quotation.id", quotaId));
        if (applicantType != null) {
            restrictions.addCriterion(Restrictions.eq("applicantType", applicantType));
        }
        return list(restrictions);
    }

    /**
     * @param quotation
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public boolean isPrintedPurchaseOrder(Quotation quotation) {
        boolean printPurchaseOrder = false;
        Contract contract = quotation.getApplicant().getContract();
        if (quotation.isIssueDownPayment() && contract != null) {
            return contractService.isPrintedPurchaseOrder(contract.getId());
        }
        return printPurchaseOrder;
    }

    @Override
    public Quotation getQuoatationByContractReference(Long contractId) {
        Contract contract = getById(Contract.class, contractId);
        return contract.getQuotation();
    }

    @Override
    public List<Quotation> getQuotationByContractStatus() {
        return quotationDao.getQuotationByContractStatus();
    }


    @Override
    public List<Quotation> getQuotationByDealer(Long dealerId, Date startDate, Date endDate) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);

        restrictions.addCriterion("dealer.id", dealerId);
        restrictions.addCriterion(Restrictions.ge("quotationDate", startDate));
        restrictions.addCriterion(Restrictions.le("quotationDate", endDate));
        List<Quotation> quotations = list(restrictions);
        return quotations;
    }

    @Override
    public boolean backToPOS(Quotation quotation) {
        Assert.notNull(quotation, "Quotation could not be null");
        changeQuotationStatus(quotation, quotation.getPreviousWkfStatus());
        Notification.show("", "Application has been change status to " + quotation.getWkfStatus().getDescEn(), Notification.Type.HUMANIZED_MESSAGE);
        return true;
    }

    @Override
    public Quotation changeQuotationNextStatus(Quotation quotation) {
        if (quotation.getNextWkfStatuses() != null && !quotation.getNextWkfStatuses().isEmpty())
            changeQuotationStatus(quotation, quotation.getNextWkfStatuses().get(0));
        return quotation;
    }

    @Override
    public Comment getLastComment(Quotation quotation, boolean isUnderWriter, boolean isUnderWriterSupervisor) {
        Comment comment = null;
        BaseRestrictions<Comment> restrictions = new BaseRestrictions<>(Comment.class);
        restrictions.addCriterion(Restrictions.eq("quotation.id", quotation.getId()));
        restrictions.addCriterion(Restrictions.eq("onlyForUW", isUnderWriter));
        restrictions.addCriterion(Restrictions.eq("onlyForUS", isUnderWriterSupervisor));
        restrictions.addCriterion(Restrictions.eq("forManager", false));
        restrictions.addOrder(Order.desc(Comment.ID_FIELD));
        List<Comment> comments = list(restrictions);
        if (!comments.isEmpty()) {
            comment = comments.get(0);
        }
        return comment;
    }


    @Override
    @Transactional(readOnly = true)
    public List<Quotation> getListQuotation(BaseRestrictions<Quotation> criteria) {
        return list(criteria);
    }

    @Override
    public SecUser getUnderwriterAndSupervisorToAssign(List<SecUser> secUsers) {
        if (secUsers != null && !secUsers.isEmpty()) {
            Random random = new Random();
            return secUsers.get(random.nextInt(secUsers.size()));
        }
        return null;
    }

    @Override
    public List<SecUser> getSecUserByProfile(long profile) {
        List<SecUser> secUsers = new ArrayList<>();
        SecProfile secProfile = ENTITY_SRV.getById(SecProfile.class, profile);
        if (secProfile != null) {
            if (secProfile.getUsers() != null) {
                for (SecUser secUser : secProfile.getUsers()) {
                    if (secUser.getStatusRecord().equals(EStatusRecord.ACTIV)) {
                        secUsers.add(secUser);
                    }
                }
                return secUsers;
            }
        }
        return null;
    }

    @Override
    public List<Quotation> getCapacityUWandUS(SecUser secUser, long profile, Date startDate, Date endDate) {
        System.out.println("Start Date :" + startDate + " End Date :" + endDate);
        List<Quotation> quotations = new ArrayList<>();
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        if (FMProfile.UW.equals(profile)) {
            restrictions.addCriterion("underwriter.id", secUser.getId());
            restrictions.addCriterion(Restrictions.eq("underwriterApprove", true));
            restrictions.addCriterion(Restrictions.ge("underwriterSubmitDate", DateUtils.getDateAtBeginningOfDay(startDate)));
            restrictions.addCriterion(Restrictions.le("underwriterSubmitDate", DateUtils.getDateAtEndOfDay(endDate)));
        } else if (FMProfile.US.equals(profile)) {
            restrictions.addCriterion("underwriterSupervisor.id", secUser.getId());
            restrictions.addCriterion(Restrictions.eq("underwriterSupervisorApprove", true));
            restrictions.addCriterion(Restrictions.ge("underwriterSupervisorSubmitDate", DateUtils.getDateAtBeginningOfDay(startDate)));
            restrictions.addCriterion(Restrictions.le("underwriterSupervisorSubmitDate", DateUtils.getDateAtEndOfDay(endDate)));
        }
        quotations = ENTITY_SRV.list(restrictions);
        if (!quotations.isEmpty()) {
            return quotations;
        }
        return null;
    }

    @Override
    public void autoAssignOrUpdateQuotationTOUWAndUS(Quotation quotation) {
        if (ProfileUtil.isUnderwriter()) {
            //under writer submit and assign underwriter supervisor
            List<SecUser> secUsers = QUO_SRV.getSecUserByProfile(FMProfile.US);
            SecUser secUser = QUO_SRV.getUnderwriterAndSupervisorToAssign(secUsers);
            if (quotation.getUnderwriterSupervisor() == null) {
                quotation.setUnderwriterSupervisor(secUser);
                quotation.setUnderwriterSubmitDate(DateUtils.today());
                quotation.setUnderwriterApprove(true);
            }
        } else if (ProfileUtil.isUnderwriterSupervisor()) {
            //underwriter supervisor submit
            if (quotation.getUnderwriterSupervisorSubmitDate() == null) {
                quotation.setUnderwriterSupervisorSubmitDate(DateUtils.today());
                quotation.setUnderwriterSupervisorApprove(true);
            }
        } else if (ProfileUtil.isUnderwriterSupervisor2()) {
            if (quotation.getWkfStatus().equals(QuotationWkfStatus.PRO)) {
                quotation.setUnderwriter(null);
                quotation.setUnderwriterSupervisor((SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
            }
        }
    }

    @Override
    public Double calculateApplicantRatio(Quotation quotation) {
        Applicant customer = quotation.getMainApplicant();
        if (customer != null) {
            double totalNetIncome = 0d;
            double totalRevenus = 0d;
            double totalAllowance = 0d;
            double totalBusinessExpenses = 0d;
            double totalDebtInstallment = 0d;
            if (customer.getIndividual() != null) {
                totalDebtInstallment = customer.getIndividual().getTotalDebtInstallment() != null ? customer.getIndividual().getTotalDebtInstallment() : 0.0;
            }

            List<Employment> employments = customer.getIndividual().getEmployments();
            for (Employment employment : employments) {
                totalRevenus += MyNumberUtils.getDouble(employment.getRevenue());
                totalAllowance += MyNumberUtils.getDouble(employment.getAllowance());
                totalBusinessExpenses += MyNumberUtils.getDouble(employment.getBusinessExpense());
            }
            totalNetIncome = totalRevenus + totalAllowance - totalBusinessExpenses;

            double totalExpenses = MyNumberUtils.getDouble(customer.getIndividual().getMonthlyPersonalExpenses())
                    + MyNumberUtils.getDouble(customer.getIndividual().getMonthlyFamilyExpenses())
                    + totalDebtInstallment;

            double disposableIncome = totalNetIncome - totalExpenses;

            Double totalInstallment = quotation.getTotalInstallmentAmount() != null ? quotation.getTotalInstallmentAmount() : 0.0d;

            double customerRatio = disposableIncome / totalInstallment;

            if (customerRatio > 0.0)
                return customerRatio;
        }
        return 0.0;
    }

    @Override
    public String getRatioColor(double applicantRatio) {
        String color = "";
        Double applicantCoRatio = applicantRatio;
        Configuration config = UWScoreConfig.getInstance().getConfiguration();

        double maxRedGrossIncomeRatio = config
                .getDouble("score.liability.gross_income_ratio[@maxred]");
        if (applicantCoRatio < maxRedGrossIncomeRatio) {
            color = "red";
        } else {
            double maxOrangeGrossIncomeRatio = config
                    .getDouble("score.liability.gross_income_ratio[@maxorange]");
            if (applicantCoRatio < maxOrangeGrossIncomeRatio) {
                color = "orange";
            } else {
                color = "green";
            }
        }
        return color;
    }

    @Override
    public boolean isAlreadyApproved(Quotation quotation){
        return QuotationWkfStatus.getQuotationStatusAfterApproved().contains(quotation.getWkfStatus());
    }

}
