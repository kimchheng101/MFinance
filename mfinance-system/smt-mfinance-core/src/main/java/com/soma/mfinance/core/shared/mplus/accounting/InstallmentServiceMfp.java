package com.soma.mfinance.core.shared.mplus.accounting;

import com.soma.mfinance.core.accounting.InstallmentVO;
import com.soma.mfinance.core.accounting.MultiAmountTypeVO;
import com.soma.mfinance.core.accounting.PayOffVO;
import com.soma.mfinance.core.accounting.ServiceFeeVO;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.financial.model.EServiceType;
import com.soma.mfinance.core.payment.model.EPaymentMethod;
import com.soma.mfinance.core.quotation.model.Quotation;
import org.seuksa.frmk.service.BaseEntityService;
import org.seuksa.frmk.tools.amount.Amount;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 
 * @author vi.sok
 * @since 05/05/2017
 */
public interface InstallmentServiceMfp extends BaseEntityService {
	/**
	 * 
	 * @param quotation
	 * @return
	 */
	HashMap<String, ServiceFeeVO> getServiceFeeInstallments(Quotation quotation);
	/**
	 * 
	 * @param quotation
	 * @param serviceType
	 * @return
	 */
	ServiceFeeVO getServiceInstallmentByCode(Quotation quotation, EServiceType serviceType);
	/**
	 * 
	 * @param services
	 * @param serviceType
	 * @return
	 */
	ServiceFeeVO getServiceInstallmentByCode(HashMap<String, ServiceFeeVO> services, EServiceType serviceType);
	
	
	//----------------------------------installmentVO calculate----------------------------------------------------
	/**
	 * 
	 * @param contract
	 * @return
	 */
	HashMap<Integer, List<InstallmentVO>> getInstallmentVOs(Contract contract);
	/**
	 * 
	 * @param quotation
	 * @return
	 */
	HashMap<Integer, List<InstallmentVO>> getInstallmentVOs(Quotation quotation, Date firstPaymentDate);
	/**
	 * 
	 * @param contract
	 * @param numInstallment
	 * @return
	 */
	List<InstallmentVO> getInstallmentVOs(Contract contract, int numInstallment);
	/**
	 * 
	 * @param contract
	 * @param dueDate
	 * @return
	 */
	List<InstallmentVO> getInstallmentVOs(Contract contract, Date dueDate);
	/**
	 * 
	 * @param cashflows
	 * @return
	 */
	List<InstallmentVO> getInstallmentVOs(List<Cashflow> cashflows);
	/**
	 * 
	 * @param contract
	 * @return
	 */
	PayOffVO getInstallmentPayOffVos(Contract contract, Date paymentDate, EPaymentMethod ePaymentMethod);


	Double getOutStanding(Quotation quotation);

	/***
	 * @param installmentByNumInstallments
	 * @return
	 */
	public MultiAmountTypeVO getEachAmountTypes(Contract contract,List<InstallmentVO> installmentByNumInstallments);

	/***
	 *
	 * @param contract
	 * @param paymentDate
	 * @return
	 */
	public Amount getUnearnInterestOfReprocess(Contract contract, Date paymentDate);
}
