package com.soma.mfinance.core.contract.model;

/**
 * Meta data of com.soma.mfinance.core.contract_old.model.ContractUserSimulInbox
 * @author kimsuor.seang
 *
 */
public interface MContractUserSimulInbox {
	
	public final static String CONTRACT = "contract";

}
