package com.soma.mfinance.core.applicant.service;

import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.applicant.model.CompanyContactInfo;

/**
 * Individual service interface
 * @author kimsuor.seang
 */
public interface CompanyService extends BaseEntityService {

	/**
	 * Save or update company contact info
	 * @param companyContactInfo
	 */
	void saveOrUpdateCompanyContactInfo(CompanyContactInfo companyContactInfo);
	
}
