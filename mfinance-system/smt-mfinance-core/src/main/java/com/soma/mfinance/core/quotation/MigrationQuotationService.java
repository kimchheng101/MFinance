package com.soma.mfinance.core.quotation;

import java.util.Date;

import org.seuksa.frmk.service.BaseEntityService;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.exception.ValidationFieldsException;
import com.soma.mfinance.core.shared.exception.quotation.ExpireQuotationException;
import com.soma.mfinance.core.shared.exception.quotation.InvalidQuotationException;
import com.soma.frmk.security.model.SecUser;

/**
 * Quotation service interface
 * @author kimsuor.seang
 *
 */
public interface MigrationQuotationService extends BaseEntityService {

	/**
	 * Create a new quotation
	 * @param quotation
	 * @return
	 */
	Quotation saveOrUpdateQuotation(Quotation quotation);
	
	/**
	 * Submit quotation to underwriter
	 * @param quotation
	 * @throws ValidationFieldsException
	 * @throws InvalidQuotationException
	 * @throws ExpireQuotationException
	 * @return
	 */
	boolean submitQuotation(Long quotaId, SecUser changeUser);
	
	/**
	 * Change quotation status
	 * @param quotation
	 * @param newStatus
	 * @return
	 */
	EWkfStatus changeQuotationStatus(Quotation quotation, EWkfStatus newStatus, SecUser changeUser);
	
	/**
	 * Change quotation status
	 * @param quotaId
	 * @param newStatus
	 * @return
	 */
	EWkfStatus changeQuotationStatus(Long quotaId, EWkfStatus newStatus, SecUser changeUser);
	
	/**
	 * @param quotation
	 * @param newStatus
	 */
	void saveUnderwriterDecision(Quotation quotation, EWkfStatus newStatus, SecUser changeUser);
	
	/**
	 * @param quotation
	 * @param newStatus
	 */
	void saveManagementDecision(Quotation quotation, EWkfStatus newStatus, SecUser changeUser);
	
	/**
	 * @param quotation
	 */
	void saveDocumentControllerDecision(Quotation quotation, SecUser changeUser);
	
	/**
	 * @param quotation
	 */
	void activate(Long quotaId, Date contractStartDate, Date firstPaymentDate, SecUser changeUser);
	
	
	/**
	 * Save or update quotation documents
	 * @param quotation
	 */
	void saveOrUpdateQuotationDocuments(Quotation quotation);
	
	/**
	 * 
	 * @param templateFileName
	 */
	void updateMigrationContract(String templateFileName, boolean isMessage);
	
	/**
	 * @param reference
	 */
	void migrateDirectCost(String reference);
	
	/**
	 */
	void migrateDirectCosts();
	
}
