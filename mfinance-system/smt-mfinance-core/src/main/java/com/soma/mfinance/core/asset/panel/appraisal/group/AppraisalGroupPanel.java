package com.soma.mfinance.core.asset.panel.appraisal.group;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Component;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * Created by Kimsuor SEANG
 * Date  : 7/19/2017
 * Name  : 1:48 PM
 * Email : k.seang@gl-f.com
 */
@org.springframework.stereotype.Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(AppraisalGroupPanel.NAME)
public class AppraisalGroupPanel extends AbstractTabsheetPanel implements View {

    /** */
    private static final long serialVersionUID = -2963062593873628481L;

    public static final String NAME = "groupdetail.group";

    @Autowired
    private AppraisalGroupTablePanel appraisalGroupTablePanel;
    @Autowired
    private AppraisalGroupFormPanel appraisalGroupFormPanel;


    @PostConstruct
    public void PostConstruct() {
        super.init();
        appraisalGroupTablePanel.setMainPanel(this);
        appraisalGroupFormPanel.setCaption(I18N.message("groupdetail.groups"));
        getTabSheet().setTablePanel(appraisalGroupTablePanel);
    }

    @Override
    public void onAddEventClick() {
        appraisalGroupFormPanel.reset();
        getTabSheet().addFormPanel(appraisalGroupFormPanel);
        getTabSheet().setSelectedTab(appraisalGroupFormPanel);
    }

    @Override
    public void onEditEventClick() {
        getTabSheet().addFormPanel(appraisalGroupFormPanel);
        initSelectedTab(appraisalGroupFormPanel);
    }

    @Override
    public void initSelectedTab(Component selectedTab) {
        if (selectedTab == appraisalGroupFormPanel) {
            appraisalGroupFormPanel.assignValues(appraisalGroupTablePanel.getItemSelectedId());
        } else if (selectedTab == appraisalGroupTablePanel && getTabSheet().isNeedRefresh()) {
            appraisalGroupTablePanel.refresh();
        }
        getTabSheet().setSelectedTab(selectedTab);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }
}
