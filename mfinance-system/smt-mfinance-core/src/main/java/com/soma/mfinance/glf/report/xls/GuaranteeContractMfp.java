package com.soma.mfinance.glf.report.xls;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.applicant.model.Employment;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationApplicant;
import com.soma.mfinance.core.quotation.model.QuotationDocument;
import com.soma.mfinance.core.shared.conf.AppConfig;
import com.soma.mfinance.core.shared.report.ReportParameter;
import com.soma.mfinance.tools.report.Report;
import com.soma.ersys.core.hr.model.address.Address;
import com.soma.ersys.core.hr.model.eref.EGender;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.joda.time.Years;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.springframework.util.Assert;

import java.util.*;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/27/2017
 * Name  : 10:07 AM
 * Email : k.seang@gl-f.com
 */
public class GuaranteeContractMfp implements Report, FinServicesHelper, GuaranteeContractFields {

    Map<String, Object> mapGuaranteeContractValue = null;

    public GuaranteeContractMfp() {
    }

    @Override
    public String generate(ReportParameter reportParameter) throws Exception {

        Long quotaId = (Long) reportParameter.getParameters().get("quotaId");
        boolean stamp = (Boolean) reportParameter.getParameters().get("stamp");
        Assert.notNull(quotaId, "Quotation Id can not be null. !");

        Map<String, String> beans = new HashMap<String, String>();
        Quotation quotation = QUO_SRV.getById(Quotation.class, quotaId);
        Assert.notNull(quotaId, "Quotation Id can not be null. !");
        org.springframework.util.Assert.notNull(quotation, "Quotation could not be null.");

        Applicant guarantor = quotation.getGuarantor();
        Asset asset = quotation.getAsset();
        this.initailMapGuaranteeContractValue(quotation);
        beans.put(GUARANTEE_NAME, (String) mapGuaranteeContractValue.get(GUARANTEE_NAME));
        beans.put(GUARANTEE_GENDER, (String) mapGuaranteeContractValue.get(GUARANTEE_GENDER));
        beans.put(GUARANTEE_DATE_BIRTHDAY, (String) mapGuaranteeContractValue.get(GUARANTEE_DATE_BIRTHDAY));

        // Guarantor info
        this.getGuarantorIndentityInfo(beans, quotation);

        //Detail 1//
        beans.put(GUARANTEE_AGREENUM, quotation.getReference());
        beans.put(DATE, (String) mapGuaranteeContractValue.get(DATE));
        beans.put(GUARANTEE_RENTING, (String) mapGuaranteeContractValue.get(GUARANTEE_RENTING));
        beans.put("guaranteeAgreeDate", DateUtils.getDateLabel(quotation.getContractStartDate(), DateUtils.FORMAT_DDMMYYYY_SLASH));

        //Detail 3//
        beans.put(GUARANTEE_VEHICLEMODEL, quotation.getApplicant().getQuotation().getAsset().getModel().getDesc());
        beans.put(GUARANTEE_VEHICLECOLOR, (String) mapGuaranteeContractValue.get(GUARANTEE_VEHICLECOLOR));
        beans.put(GUARANTEE_VEHICLEMANUFACTUREDATE, mapGuaranteeContractValue.get(GUARANTEE_VEHICLEMANUFACTUREDATE).toString());
        beans.put(GUARANTEE_VEHICLEGENDER, mapGuaranteeContractValue.get(GUARANTEE_VEHICLEGENDER).toString());
        beans.put(GUARANTEE_VEHICLEBRAND, mapGuaranteeContractValue.get(GUARANTEE_VEHICLEBRAND).toString());

        beans.put(GUARANTEE_MARK, "Honda");
        beans.put(GUARANTEE_ASSETPRODUCT, mapGuaranteeContractValue.get(GUARANTEE_ASSETPRODUCT).toString());
        beans.put(GUARANTEE_VEHICLECC, (String) mapGuaranteeContractValue.get(GUARANTEE_VEHICLECC));
        beans.put(GUARANTEE_VEHICLEENGINENUM, StringUtils.defaultString(asset.getEngineNumber()));
        beans.put(GUARANTEE_VEHICLESERIENUM, StringUtils.defaultString(asset.getChassisNumber()));

        //current address
        beans.put(GUARANTEE_HOUSENUM, (String) mapGuaranteeContractValue.get(GUARANTEE_HOUSENUM));
        beans.put(GUARANTEE_STREETNUM, (String) mapGuaranteeContractValue.get(GUARANTEE_STREETNUM));
        beans.put(GUARANTEE_VILLAGE, (String) mapGuaranteeContractValue.get(GUARANTEE_VILLAGE));
        beans.put(GUARANTEE_COMMUNE, (String) mapGuaranteeContractValue.get(GUARANTEE_COMMUNE));
        beans.put(GUARANTEE_DISTRICT, (String) mapGuaranteeContractValue.get(GUARANTEE_DISTRICT));
        beans.put(GUARANTEE_PROVINCE, (String) mapGuaranteeContractValue.get(GUARANTEE_PROVINCE));

        //work place
        beans.put(GUARANTEE_COMPANYNAME, (String) mapGuaranteeContractValue.get(GUARANTEE_COMPANYNAME));
        beans.put(GUARANTEE_COMPANYTYPEBUSINESS, (String) mapGuaranteeContractValue.get(GUARANTEE_COMPANYTYPEBUSINESS));
        beans.put(GUARANTEE_COMPANYHOUSENUM, (String) mapGuaranteeContractValue.get(GUARANTEE_COMPANYHOUSENUM));
        beans.put(GUARANTEE_COMPANYSTREETNUM, (String) mapGuaranteeContractValue.get(GUARANTEE_COMPANYSTREETNUM));
        beans.put(GUARANTEE_COMPANYVILLAGE, (String) mapGuaranteeContractValue.get(GUARANTEE_COMPANYVILLAGE));
        beans.put(GUARANTEE_COMPANYCOMMUNE, (String) mapGuaranteeContractValue.get(GUARANTEE_COMPANYCOMMUNE));
        beans.put(GUARANTEE_COMPANYDISTRICT, (String) mapGuaranteeContractValue.get(GUARANTEE_COMPANYDISTRICT));
        beans.put(GUARANTEE_COMPANYPROVINCE, (String) mapGuaranteeContractValue.get(GUARANTEE_COMPANYPROVINCE));
        beans.put(GUARANTEE_COMPANYPHONENUM, (String) mapGuaranteeContractValue.get(GUARANTEE_COMPANYPHONENUM));
        beans.put(GUARANTEE_POSITION, (String) mapGuaranteeContractValue.get(GUARANTEE_POSITION));
        beans.put(GUARANTEE_PROFESSIONAL, (String) mapGuaranteeContractValue.get(GUARANTEE_PROFESSIONAL));

        beans.put(GUARANTEE_TIMEATADDRESSINMONTH, mapGuaranteeContractValue.get(GUARANTEE_TIMEATADDRESSINMONTH).toString());
        beans.put(GUARANTEE_TIMEATADDRESSINYEAR, mapGuaranteeContractValue.get(GUARANTEE_TIMEATADDRESSINYEAR).toString());
        beans.put(GUARANTEE_AGE, mapGuaranteeContractValue.get(GUARANTEE_AGE).toString());

        if (guarantor != null) {
            Applicant applicant = quotation.getApplicant();
            double totalNetIncome = 0d;
            double totalRevenus = 0d;
            double totalAllowance = 0d;
            double totalBusinessExpenses = 0d;

            List<Employment> employments = guarantor.getIndividual().getEmployments();
            if (employments == null) {
                employments = new ArrayList<Employment>();
            }

            for (Employment employment : employments) {
                totalRevenus += MyNumberUtils.getDouble(employment.getRevenue());
                totalAllowance += MyNumberUtils.getDouble(employment.getAllowance());
                totalBusinessExpenses += MyNumberUtils.getDouble(employment.getBusinessExpense());
            }
            totalNetIncome = totalRevenus + totalAllowance - totalBusinessExpenses;
            beans.put(GUARANTEE_PHONENUM1, guarantor.getIndividual().getMobilePerso());
            beans.put(GUARANTEE_PHONENUM2, guarantor.getIndividual().getMobilePerso2() != null ? guarantor.getIndividual().getMobilePerso2() : " ");
            beans.put(GUARANTEE_INCOME, "" + AmountUtils.format(totalNetIncome));
            beans.put(GUARANTEE_CONTRACTPAYMENT, "" + AmountUtils.format(quotation.getTiFinanceAmount()));
            beans.put(GUARANTEE_RELATIONSHIP_CONTACT, mapGuaranteeContractValue.get(GUARANTEE_RELATIONSHIP_CONTACT).toString());

            beans.put(GUARANTEE_AVALABLEDATETIME, guarantor.getIndividual() != null ? StringUtils.defaultString(guarantor.getIndividual().getConvenientVisitTime()) : "");
            beans.put(GUARANTEE_APPLICANTNAME, applicant.getLastName() + "  " + applicant.getFirstName());
            beans.put(GUARANTEE_APPLICANTGENDER, applicant.getIndividual().getGender().toString());
            beans.put(GUARANTEE_APPLICANTBIRTHDATE, DateUtils.getDateLabel(applicant.getIndividual().getBirthDate(), DateUtils.FORMAT_DDMMYYYY_SLASH).toString());
            beans.put(GUARANTEE_APPLICANTAGE, getAgeByBirthDate(applicant.getIndividual().getBirthDate()));

            this.getApplicantIdentityInfo(beans, quotation);

        } else {
            Applicant applicant = quotation.getApplicant();

            double totalNetIncome = 0d;
            double totalRevenus = 0d;
            double totalAllowance = 0d;
            double totalBusinessExpenses = 0d;
            totalNetIncome = totalRevenus + totalAllowance - totalBusinessExpenses;
            beans.put(GUARANTEE_PHONENUM1, "");
            beans.put(GUARANTEE_INCOME, "" + AmountUtils.format(totalNetIncome));
            beans.put(GUARANTEE_CONTRACTPAYMENT, "" + AmountUtils.format(quotation.getTiFinanceAmount()));
        }

        //=-===================
        String templatePath = AppConfig.getInstance().getConfiguration().getString("specific.templatedir");

        String templateFileName = templatePath + ("/GuaranteeContractMfp.xlsx");
        //String templateFileName = templatePath + (stamp ? "/GLFGuaranteeContractKHStamp.xlsx" : "/GLFGuaranteeContractKH.xlsx");

        String outputPath = AppConfig.getInstance().getConfiguration().getString("specific.tmpdir");

        String prefixOutputName = "GuaranteeContractMfp";
        String sufixOutputName = "xlsx";
        String uuid = UUID.randomUUID().toString().replace("-", "");
        String xlsFileName = outputPath + "/" + prefixOutputName + uuid + "." + sufixOutputName;

        XLSTransformer transformer = new XLSTransformer();
        transformer.transformXLS(templateFileName, beans, xlsFileName);

        /*Edit excel file to add photo*/
        /*InputStream inp = new FileInputStream(destFileName);
        Workbook wb = WorkbookFactory.create(inp);
        Sheet sheet = wb.getSheetAt(0);

        CreationHelper helper = wb.getCreationHelper();
        Drawing drawing = sheet.createDrawingPatriarch();

            InputStream is = new FileInputStream("D:/Work_local/glf/glf_report/glf/in/client.JPG");
            byte[] bytes = IOUtils.toByteArray(is);
            int pictureIdx = wb.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
            is.close();

            ClientAnchor anchor = helper.createClientAnchor();
            anchor.setRow1(6);
            anchor.setCol1(23);
            Picture pict = drawing.createPicture(anchor, pictureIdx);
            pict.resize();

        FileOutputStream fileOut1 = new FileOutputStream(destFileName);
        wb.write(fileOut1);
        fileOut1.close(); */

        return prefixOutputName + uuid + "." + sufixOutputName;
    }

    /**
     * @param beans
     * @param quotation
     */
    private void getGuarantorIndentityInfo(Map<String, String> beans, Quotation quotation) {
        List<QuotationDocument> quotationDocuments = quotation.getQuotationDocuments(EApplicantType.G);
        QuotationDocument idNationCardDocument = DOC_SRV.getQuotationDocumentByDocCode(NATION_ID_DOC_CODE, quotationDocuments);
        beans.put(IS_LESSEEI_DCARD, idNationCardDocument != null ? CHECKING_SYMBOL : StringUtils.EMPTY);
        beans.put(GUARANTEE_CARD, idNationCardDocument != null ? StringUtils.defaultString(idNationCardDocument.getReference()) : "");

        QuotationDocument reseidentBookDocument = DOC_SRV.getQuotationDocumentByDocCode(RES_IND_BOOK_DOC_CODE, quotationDocuments);
        beans.put(ISLESSEERESIDENT_BOOKNUM, reseidentBookDocument != null ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(GUARANTEE_RESIDENTBOOKNUM, reseidentBookDocument != null ? StringUtils.defaultString(reseidentBookDocument.getReference()) : "");

        /*QuotationDocument villageCertifiedDocument = DOC_SRV.getQuotationDocumentByDocCode(VILLAGE_CERTI_DOC_CODE, quotationDocuments);
        beans.put(ISLESSEE_CERTIFYLETTERNUM, villageCertifiedDocument != null ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(GUARANTEE_CERTIFYLETTERNUM, villageCertifiedDocument != null ? StringUtils.defaultString(villageCertifiedDocument.getReference()) : "");
        */
        QuotationDocument familyBookDocument = DOC_SRV.getQuotationDocumentByDocCode(FAMILY_BOOK_DOC_CODE, quotationDocuments);
        beans.put(ISLESSEE_FAMILYBOOKNUM, familyBookDocument != null ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(GUARANTEE_FAMILYBOOKNUM, familyBookDocument != null ? StringUtils.defaultString(familyBookDocument.getReference()) : "");

        QuotationDocument birthCertificateDocument = DOC_SRV.getQuotationDocumentByDocCode(BIRTH_CERTIFICATE_DOC_CODE, quotationDocuments);
        beans.put(ISLESSEE_BIRTHCERTIFICATENUM, birthCertificateDocument != null ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(GUARANTEE_BIRTHCERTIFICATENUM, birthCertificateDocument != null ? StringUtils.defaultString(birthCertificateDocument.getReference()) : "");

        Applicant guarantor = quotation.getGuarantor();
        if (guarantor.getIndividual() != null){

            String houseCode = guarantor.getIndividual().getIndividualAddresses().get(0).getHousing().getCode();
            beans.put(GUARANTEE_IS_OWNHOUSE, houseCode.equals("owner") ? CHECKING_SYMBOL : CHECKING_SPACE);
            beans.put(GUARANTEE_IS_PARENTHOUSE, houseCode.equals("parent") ? CHECKING_SYMBOL : CHECKING_SPACE);
            beans.put(GUARANTEE_IS_RELATIVEHOUSE, houseCode.equals("relative") ? CHECKING_SYMBOL : CHECKING_SPACE);
            beans.put(GUARANTEE_IS_RENTINGHOUSE, houseCode.equals("rental") ? CHECKING_SYMBOL : CHECKING_SPACE);
            beans.put(GUARANTEE_IS_OTHER, houseCode.equals("other") ? CHECKING_SYMBOL : CHECKING_SPACE);
        }
    }

    /**
     * @param beans
     * @param quotation
     */
    private void getApplicantIdentityInfo(Map<String, String> beans, Quotation quotation) {
        // applicant Docs
        List<QuotationDocument> applicantDocs = quotation.getQuotationDocuments(EApplicantType.C);
        QuotationDocument applicantNatIDoc = DOC_SRV.getQuotationDocumentByDocCode(NATION_ID_DOC_CODE, applicantDocs);
        beans.put(IS_APPLICANT_NATIONAL_ID, applicantNatIDoc != null ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(GUARANTEE_APPLICANT_NATIONAL_ID, applicantNatIDoc != null ? StringUtils.defaultString(applicantNatIDoc.getReference()) : "");

        QuotationDocument applicantResidentBookDoc = DOC_SRV.getQuotationDocumentByDocCode(RES_IND_BOOK_DOC_CODE, applicantDocs);
        beans.put(IS_APPLICANT_RESIDENTBOOK_ID, applicantResidentBookDoc != null ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(GUARANTEE_APPLICANT_RESIDENTBOOK_ID, applicantResidentBookDoc != null ? StringUtils.defaultString(applicantResidentBookDoc.getReference()) : "");

        QuotationDocument villageCertifyLetter = DOC_SRV.getQuotationDocumentByDocCode(VILLAGE_CERTI_DOC_CODE, applicantDocs);
        beans.put(IS_APPLICANT_CERTIFICATE, villageCertifyLetter != null ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(GUARANTEE_APPLICANT_CERTIFICATE, villageCertifyLetter != null ? StringUtils.defaultString(villageCertifyLetter.getReference()) : "");

        QuotationDocument applicantFamilyBookDoc = DOC_SRV.getQuotationDocumentByDocCode(FAMILY_BOOK_DOC_CODE, applicantDocs);
        beans.put(IS_APPLICANT_FAMILYBOOK, applicantFamilyBookDoc != null ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(GUARANTEE_APPLICANT_FAMILYBOOK, applicantFamilyBookDoc != null ? StringUtils.defaultString(applicantFamilyBookDoc.getReference()) : "");

        QuotationDocument applicantBirthCertificateDoc = DOC_SRV.getQuotationDocumentByDocCode(BIRTH_CERTIFICATE_DOC_CODE, applicantDocs);
        beans.put(IS_APPLICANT_BIRTHCERTIFICATE, applicantBirthCertificateDoc != null ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(GUARANTEE_APPLICANT_BIRTHCERTIFICATE, applicantBirthCertificateDoc != null ? StringUtils.defaultString(applicantBirthCertificateDoc.getReference()) : "");
        beans.put("leasAmount", AmountUtils.format(quotation.getTiFinanceAmount() != null ? quotation.getTiFinanceAmount() : 0d));
    }

    /**
     * @param quotation
     */
    public void initailMapGuaranteeContractValue(Quotation quotation) {
        mapGuaranteeContractValue = new HashedMap();

        Applicant guarantor = quotation.getGuarantor();
        Asset asset = quotation.getAsset();
        Applicant applicant = quotation.getApplicant();
        QuotationApplicant quotationApplicant = quotation.getQuotationApplicant(EApplicantType.G);

        if (applicant.getIndividual() != null) {
            mapGuaranteeContractValue.put(GUARANTEE_RENTING, StringUtils.defaultString(applicant.getIndividual().getLastName()) + " " + StringUtils.defaultString(applicant.getIndividual().getFirstName()));
        } else {
            mapGuaranteeContractValue.put(GUARANTEE_RENTING, StringUtils.EMPTY);
        }

        if (guarantor != null) {
            Employment guarantorEmployment = guarantor.getIndividual().getCurrentEmployment();
            Address guarantorAddress = guarantor.getIndividual() != null ? guarantor.getIndividual().getMainAddress() : null;
            Address employmentAddress = guarantorEmployment.getAddress();
            if (guarantor.getIndividual() != null) {
                mapGuaranteeContractValue.put(GUARANTEE_NAME, guarantor.getIndividual().getLastName() + "  " + guarantor.getIndividual().getFirstName());
                if (guarantor.getIndividual().getGender() != null) {
                    mapGuaranteeContractValue.put(GUARANTEE_GENDER, StringUtils.defaultString(guarantor.getIndividual().getGender().getDesc()));
                } else {
                    mapGuaranteeContractValue.put(GUARANTEE_GENDER, StringUtils.EMPTY);
                }
                mapGuaranteeContractValue.put(GUARANTEE_DATE_BIRTHDAY, DateUtils.getDateLabel(guarantor.getIndividual().getBirthDate(), DateUtils.FORMAT_DDMMYYYY_SLASH));

            } else {
                mapGuaranteeContractValue.put(GUARANTEE_NAME, StringUtils.EMPTY);
                mapGuaranteeContractValue.put(GUARANTEE_GENDER, StringUtils.EMPTY);
                mapGuaranteeContractValue.put(GUARANTEE_DATE_BIRTHDAY, StringUtils.EMPTY);
            }

            if (quotationApplicant.getRelationship().toString().equals("n/a")) {
                mapGuaranteeContractValue.put(GUARANTEE_RELATIONSHIP_CONTACT, quotationApplicant.getRelationship().getDescEn());
            } else {
                mapGuaranteeContractValue.put(GUARANTEE_RELATIONSHIP_CONTACT, quotationApplicant.getRelationship());
            }

            if (asset != null && asset.getColor() != null) {
                if (StringUtils.isNotEmpty(asset.getColor().getDesc())) {
                    mapGuaranteeContractValue.put(GUARANTEE_VEHICLECOLOR, asset.getColor().getDesc());
                } else {
                    mapGuaranteeContractValue.put(GUARANTEE_VEHICLECOLOR, StringUtils.defaultString(asset.getColor().getDescEn()));
                }
            } else {
                mapGuaranteeContractValue.put(GUARANTEE_VEHICLECOLOR, StringUtils.EMPTY);
            }

            if (asset != null && asset.getEngine() != null) {
                if (StringUtils.isNotEmpty(asset.getEngine().getDesc()) && !asset.getEngine().getDesc().equalsIgnoreCase("n/a ")) {
                    mapGuaranteeContractValue.put(GUARANTEE_VEHICLECC, asset.getEngine().getDescEn());
                } else {
                    mapGuaranteeContractValue.put(GUARANTEE_VEHICLECC, StringUtils.defaultString(asset.getEngine().getDescEn()));
                }

                mapGuaranteeContractValue.put(GUARANTEE_VEHICLEGENDER, getGenderDesc(asset.getAssetGender().getCode()));
                mapGuaranteeContractValue.put(GUARANTEE_VEHICLEMANUFACTUREDATE, quotation.getApplicant().getQuotation().getAsset().getAssetYear().getCode());
                mapGuaranteeContractValue.put(GUARANTEE_VEHICLEBRAND, applicant.getQuotation().getAsset().getBrandDescLocale());
                mapGuaranteeContractValue.put(GUARANTEE_ASSETPRODUCT, applicant.getQuotation().getAsset().getAssetMake().getDesc());

            } else {
                mapGuaranteeContractValue.put(GUARANTEE_VEHICLECC, StringUtils.EMPTY);
            }
            if (guarantorAddress != null) {
                mapGuaranteeContractValue.put(GUARANTEE_HOUSENUM, StringUtils.defaultString(guarantorAddress.getHouseNo()));
                mapGuaranteeContractValue.put(GUARANTEE_STREETNUM, StringUtils.defaultString(guarantorAddress.getStreet()));
                mapGuaranteeContractValue.put(GUARANTEE_VILLAGE, guarantorAddress.getVillage() == null ? "" : StringUtils.defaultString(guarantorAddress.getVillage().getDesc()));
                mapGuaranteeContractValue.put(GUARANTEE_COMMUNE, guarantorAddress.getCommune() == null ? "" : StringUtils.defaultString(guarantorAddress.getCommune().getDesc()));
                mapGuaranteeContractValue.put(GUARANTEE_DISTRICT, guarantorAddress.getDistrict() == null ? "" : StringUtils.defaultString(guarantorAddress.getDistrict().getDesc()));
                mapGuaranteeContractValue.put(GUARANTEE_PROVINCE, guarantorAddress.getProvince() == null ? "" : StringUtils.defaultString(guarantorAddress.getProvince().getDesc()));
                mapGuaranteeContractValue.put(GUARANTEE_TIMEATADDRESSINYEAR, guarantor.getIndividual().getIndividualAddresses().get(0).getTimeAtAddressInYear());
                mapGuaranteeContractValue.put(GUARANTEE_TIMEATADDRESSINMONTH, guarantor.getIndividual().getIndividualAddresses().get(0).getTimeAtAddressInMonth());
                mapGuaranteeContractValue.put(GUARANTEE_AGE, getAgeByBirthDate(guarantor.getIndividual().getBirthDate()));

            } else {
                mapGuaranteeContractValue.put(GUARANTEE_HOUSENUM, StringUtils.EMPTY);
                mapGuaranteeContractValue.put(GUARANTEE_STREETNUM, StringUtils.EMPTY);
                mapGuaranteeContractValue.put(GUARANTEE_VILLAGE, StringUtils.EMPTY);
                mapGuaranteeContractValue.put(GUARANTEE_COMMUNE, StringUtils.EMPTY);
                mapGuaranteeContractValue.put(GUARANTEE_DISTRICT, StringUtils.EMPTY);
                mapGuaranteeContractValue.put(GUARANTEE_PROVINCE, StringUtils.EMPTY);
            }

            if (guarantorEmployment != null && guarantorEmployment.getEmploymentIndustry() != null) {
                if (StringUtils.isNotEmpty(guarantorEmployment.getEmploymentIndustry().getDesc()) && !guarantorEmployment.getEmploymentIndustry().getDesc().equalsIgnoreCase("n/a")) {
                    mapGuaranteeContractValue.put(GUARANTEE_COMPANYTYPEBUSINESS, guarantorEmployment.getEmploymentIndustry().getDesc());
                } else {
                    mapGuaranteeContractValue.put(GUARANTEE_COMPANYTYPEBUSINESS, StringUtils.defaultString(guarantorEmployment.getEmploymentIndustry().getDescEn()));
                }
            } else {
                mapGuaranteeContractValue.put(GUARANTEE_COMPANYTYPEBUSINESS, StringUtils.EMPTY);
            }
            if (employmentAddress != null) {
                mapGuaranteeContractValue.put(GUARANTEE_COMPANYNAME, StringUtils.defaultString(guarantorEmployment.getWorkPlaceName()));
                mapGuaranteeContractValue.put(GUARANTEE_COMPANYHOUSENUM, StringUtils.defaultString(employmentAddress.getHouseNo()));
                mapGuaranteeContractValue.put(GUARANTEE_COMPANYSTREETNUM, StringUtils.defaultString(employmentAddress.getStreet()));
                mapGuaranteeContractValue.put(GUARANTEE_COMPANYVILLAGE, employmentAddress.getVillage() != null ? StringUtils.defaultString(employmentAddress.getVillage().getDesc()) : "");
                mapGuaranteeContractValue.put(GUARANTEE_COMPANYCOMMUNE, employmentAddress.getVillage() != null ? StringUtils.defaultString(employmentAddress.getCommune().getDesc()) : "");
                mapGuaranteeContractValue.put(GUARANTEE_COMPANYDISTRICT, employmentAddress.getVillage() != null ? StringUtils.defaultString(employmentAddress.getDistrict().getDesc()) : "");
                mapGuaranteeContractValue.put(GUARANTEE_COMPANYPROVINCE, employmentAddress.getVillage() != null ? StringUtils.defaultString(employmentAddress.getProvince().getDesc()) : "");
                mapGuaranteeContractValue.put(GUARANTEE_COMPANYPHONENUM, StringUtils.defaultString(guarantorEmployment.getWorkPhone()));
                mapGuaranteeContractValue.put(GUARANTEE_POSITION, StringUtils.defaultString(guarantorEmployment.getEmploymentPosition().getDesc()));
                mapGuaranteeContractValue.put(GUARANTEE_PROFESSIONAL, guarantorEmployment.getSeniorityLevel() != null ? StringUtils.defaultString(guarantorEmployment.getSeniorityLevel().getDesc()) : "");

            } else {
                mapGuaranteeContractValue.put(GUARANTEE_COMPANYNAME, StringUtils.EMPTY);
                mapGuaranteeContractValue.put(GUARANTEE_COMPANYHOUSENUM, StringUtils.EMPTY);
                mapGuaranteeContractValue.put(GUARANTEE_COMPANYSTREETNUM, StringUtils.EMPTY);
                mapGuaranteeContractValue.put(GUARANTEE_COMPANYVILLAGE, StringUtils.EMPTY);
                mapGuaranteeContractValue.put(GUARANTEE_COMPANYCOMMUNE, StringUtils.EMPTY);
                mapGuaranteeContractValue.put(GUARANTEE_COMPANYDISTRICT, StringUtils.EMPTY);
                mapGuaranteeContractValue.put(GUARANTEE_COMPANYPROVINCE, StringUtils.EMPTY);
                mapGuaranteeContractValue.put(GUARANTEE_COMPANYPHONENUM, StringUtils.EMPTY);
                mapGuaranteeContractValue.put(GUARANTEE_POSITION, StringUtils.EMPTY);
            }
        } else {
            mapGuaranteeContractValue.put(GUARANTEE_NAME, StringUtils.EMPTY);
            mapGuaranteeContractValue.put(GUARANTEE_GENDER, StringUtils.EMPTY);
            mapGuaranteeContractValue.put(GUARANTEE_DATE_BIRTHDAY, StringUtils.EMPTY);
            mapGuaranteeContractValue.put(GUARANTEE_HOUSENUM, StringUtils.EMPTY);
            mapGuaranteeContractValue.put(GUARANTEE_STREETNUM, StringUtils.EMPTY);
            mapGuaranteeContractValue.put(GUARANTEE_VILLAGE, StringUtils.EMPTY);
            mapGuaranteeContractValue.put(GUARANTEE_COMMUNE, StringUtils.EMPTY);
            mapGuaranteeContractValue.put(GUARANTEE_DISTRICT, StringUtils.EMPTY);
            mapGuaranteeContractValue.put(GUARANTEE_PROVINCE, StringUtils.EMPTY);
            mapGuaranteeContractValue.put(GUARANTEE_COMPANYNAME, StringUtils.EMPTY);
            mapGuaranteeContractValue.put(GUARANTEE_COMPANYHOUSENUM, StringUtils.EMPTY);
            mapGuaranteeContractValue.put(GUARANTEE_COMPANYSTREETNUM, StringUtils.EMPTY);
            mapGuaranteeContractValue.put(GUARANTEE_COMPANYVILLAGE, StringUtils.EMPTY);
            mapGuaranteeContractValue.put(GUARANTEE_COMPANYCOMMUNE, StringUtils.EMPTY);
            mapGuaranteeContractValue.put(GUARANTEE_COMPANYDISTRICT, StringUtils.EMPTY);
            mapGuaranteeContractValue.put(GUARANTEE_COMPANYPROVINCE, StringUtils.EMPTY);
            mapGuaranteeContractValue.put(GUARANTEE_COMPANYPHONENUM, StringUtils.EMPTY);
            mapGuaranteeContractValue.put(GUARANTEE_POSITION, StringUtils.EMPTY);
        }
    }

    /**
     * @param birthDate
     * @return
     */
    private String getAgeByBirthDate(Date birthDate) {
        long timestamp = birthDate.getTime();
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);
        Integer y = cal.get(Calendar.YEAR);
        Integer moy = cal.get(Calendar.MONTH) + 1;
        Integer dom = cal.get(Calendar.DAY_OF_MONTH);
        org.joda.time.LocalDate birthdate = new org.joda.time.LocalDate(y, moy, dom);
        Years age = Years.yearsBetween(birthdate, org.joda.time.LocalDate.now());
        return String.format("%s", age.getYears());
    }

    /*retrurn code*/
    private String getGenderDesc(String code) {
        if (EGender.M.getCode().equals(code)) {
            return "ប្រុស";
        } else if (EGender.F.getCode().equals(code)) {
            return "ស្រី";
        } else if (EGender.U.getCode().equals(code)) {
            return "មិនដឹង";
        }
        return "";
    }

}
