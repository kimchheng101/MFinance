package com.soma.mfinance.core.financial.service;

import com.soma.mfinance.core.financial.model.EServiceType;
import com.soma.mfinance.core.financial.model.FinProduct;
import com.soma.mfinance.core.financial.model.FinService;
import com.soma.mfinance.core.financial.model.Term;
import com.soma.mfinance.core.quotation.model.Quotation;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.service.BaseEntityService;

import java.util.Map;

/**
 * Financial product service interface
 *
 * @author kimsuor.seang
 */
public interface FinancialProductService extends BaseEntityService {

	FinProduct createFinProduct(FinProduct finProduct);

	void deleteFinProduct(FinProduct finProduct);

	FinService getFinServiceByType(EServiceType serviceType);

	Map<String, Double> calculationInstallmentAmount(Quotation quotation);

	Term getNumberOfTermByNbTerm(Integer nbTerm);

	Double getInsuranceFee(Double leaseAmount);

	Double getVatInsurance(Double leaseAmount);

	Double getVatService(String serviceType);

	BaseRestrictions<FinProduct> getFinProductRestrictions();
}
