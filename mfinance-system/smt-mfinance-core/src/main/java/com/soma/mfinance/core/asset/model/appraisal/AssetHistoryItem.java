package com.soma.mfinance.core.asset.model.appraisal;

import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;

/**
 * @author by kimsuor.seang  on 27/10/2017.
 */

@Entity
@Table(name = "td_asset_history_item")
public class AssetHistoryItem extends EntityRefA {

	private static final long serialVersionUID = -6266316613037520987L;
    private Integer percent;
    private AssetHistoryCategory assetHistoryCategory;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ass_his_item_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Override
    @Transient
    public String getCode() {
        return code;
    }

    @Column(name = "ass_his_item_desc", nullable = false, length = 50)
    @Override
    public String getDesc() {
        return super.getDesc();
    }

    @Override
    @Column(name = "ass_his_item_desc_en", nullable = false, length=50)
    public String getDescEn() {
        return super.getDescEn();
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ass_his_cate_id")
    public AssetHistoryCategory getAssetHistoryCategory() {
        return assetHistoryCategory;
    }

    public void setAssetHistoryCategory(AssetHistoryCategory assetHistoryCategory) {
        this.assetHistoryCategory = assetHistoryCategory;
    }

    @Column(name = "ass_app_item_percent", nullable = true)
    public Integer getPercent() {
        return percent;
    }

    public void setPercent(Integer percent) {
        this.percent = percent;
    }


}
