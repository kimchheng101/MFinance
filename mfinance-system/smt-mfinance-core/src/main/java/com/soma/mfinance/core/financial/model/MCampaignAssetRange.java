package com.soma.mfinance.core.financial.model;

import org.seuksa.frmk.model.entity.MEntityRefA;

/**
 * Meta data of com.soma.mfinance.core.financial.model.CampaignAssetRange
 * @author kimsuor.seang
 */
public interface MCampaignAssetRange extends MEntityRefA {
	
	public final static String ASSETRANGE = "assetRange";
	public final static String CAMPAIGN = "campaign";

}
