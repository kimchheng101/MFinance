package com.soma.mfinance.core.application.model;

import com.soma.ersys.core.hr.model.eref.EEmploymentIndustry;
import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;

/**
 * Created by Kimsuor SEANG
 * Date  : 6/21/2017
 * Name  : 9:02 AM
 * Email : k.seang@gl-f.com
 */
@Entity
@Table(
        name = "tu_employment_position",
        indexes = {
                @Index( name = "idx_emp_pos_ind_id", columnList = "emp_ind_id"
        )}
)
public class EmploymentPosition extends EntityRefA {

    private EEmploymentIndustry employmentIndustry;
    private EmploymentOccupation employmentOccupation;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "emp_pos_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name = "emp_ind_id",nullable = false)
    @Convert(
            converter = EEmploymentIndustry.class
    )
    public EEmploymentIndustry getEmploymentIndustry() {
        return employmentIndustry;
    }

    public void setEmploymentIndustry(EEmploymentIndustry employmentIndustry) {
        this.employmentIndustry = employmentIndustry;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "emp_occ_id", referencedColumnName = "emp_occ_id")
    public EmploymentOccupation getEmploymentOccupation() {
        return employmentOccupation;
    }

    public void setEmploymentOccupation(EmploymentOccupation employmentOccupation) {
        this.employmentOccupation = employmentOccupation;
    }

    /**
     * @see
     */
    @Column(name = "emp_pos_code", nullable = false, unique = true)
    @Override
    public String getCode() {
        return super.getCode();
    }

    @Override
    @Column(name = "emp_pos_desc", nullable = true)
    public String getDesc() {
        return super.getDesc();
    }


    @Override
    @Column(name = "emp_pos_desc_en", nullable = true)
    public String getDescEn() {
        return super.getDescEn();
    }
}
