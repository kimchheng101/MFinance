package com.soma.mfinance.core.contract.service.impl;

import com.soma.common.app.tools.UserSessionManager;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.ApplicantArc;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.applicant.service.ApplicantService;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.asset.model.AssetArc;
import com.soma.mfinance.core.contract.dao.ContractDao;
import com.soma.mfinance.core.contract.handle.ContractServiceHelper;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.ContractApplicant;
import com.soma.mfinance.core.contract.model.ContractFinService;
import com.soma.mfinance.core.contract.model.MContract;
import com.soma.mfinance.core.contract.service.ActivationContractMfpService;
import com.soma.mfinance.core.contract.service.DealerPaymentException;
import com.soma.mfinance.core.contract.service.JournalEntryException;
import com.soma.mfinance.core.contract.service.SequenceManager;
import com.soma.mfinance.core.financial.model.EProductLineType;
import com.soma.mfinance.core.financial.model.FinProduct;
import com.soma.mfinance.core.financial.model.ProductLine;
import com.soma.mfinance.core.financial.model.Term;
import com.soma.mfinance.core.financial.service.FinancialProductService;
import com.soma.mfinance.core.quotation.SequenceGenerator;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationApplicant;
import com.soma.mfinance.core.quotation.model.QuotationService;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.frmk.vaadin.util.i18n.I18N;
import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyMathUtils;
import org.seuksa.frmk.tools.exception.EntityCreationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/21/2017
 * Name  : 2:37 PM
 * Email : k.seang@gl-f.com
 */
@Service("activationContractMotoForFlusService")
public class ActivationContractMfpServiceImpl extends BaseEntityServiceImpl implements ActivationContractMfpService, MContract {

    protected Logger LOG = LoggerFactory.getLogger(ActivationContractMfpServiceImpl.class);


    @Autowired
    private ContractDao dao;

    @Autowired
    private ApplicantService applicantService;

    @Autowired
    private FinancialProductService financialProductService;

    @Override
    public BaseEntityDao getDao() {
        return dao;
    }

    @Override
    public Contract createContract(Quotation quotation) throws EntityCreationException {
        LOG.debug(">> Create contract - Quotation ID. [" + quotation.getId() + "]");

        Contract contract = Contract.createInstance();
        FinProduct financialProduct = quotation.getFinancialProduct();
        ProductLine productLine = financialProduct.getProductLine();
        contract.setProductLineType(EProductLineType.FNC);
        contract.setProductLine(productLine);
        contract.setFinancialProduct(financialProduct);
        contract.setCampaign(quotation.getCampaign());
        contract.setExternalReference(quotation.getReference());
        contract.setOriginBranch(quotation.getOriginBranch());
        contract.setBranchInCharge(quotation.getOriginBranch());

        Long sequence = SequenceManager.getInstance().getSequenceContractMfp();
        String yearLabel = DateUtils.getDateLabel(DateUtils.today(), "yy");
        SequenceGenerator sequenceGenerator = new ContractSequenceMfpImpl(contract.getFinancialProduct().getCode(), yearLabel, sequence);
        //contract.setReference(sequenceGenerator.generate());
        contract.setReference(quotation.getReference());

        Applicant applicant = quotation.getApplicant();
        if (applicant != null && applicant.getId() == null) {
            applicantService.saveOrUpdateApplicant(applicant);
        }
        contract.setApplicant(applicant);

        Asset asset = quotation.getAsset();
        saveOrUpdate(asset);

        contract.setDealer(quotation.getDealer());
        contract.setAsset(asset);

        contract.setStartDate(quotation.getContractStartDate());
        contract.setApprovalDate(quotation.getAcceptationDate());
        contract.setQuotationDate(quotation.getQuotationDate());
        contract.setCreationDate(quotation.getActivationDate());
        contract.setInitialEndDate(contract.getEndDate());
        contract.setWkfStatus(ContractWkfStatus.WTD);
        contract.setVatValue(quotation.getVatValue());
        contract.setTiAdvancePaymentAmount(quotation.getTiAdvancePaymentAmount());
        contract.setTeAdvancePaymentAmount(quotation.getTeAdvancePaymentAmount());
        contract.setVatAdvancePaymentAmount(quotation.getVatAdvancePaymentAmount());
        contract.setAdvancePaymentPercentage(quotation.getAdvancePaymentPercentage());
        contract.setTiFinancedAmount(quotation.getTiFinanceAmount());
        contract.setTmFinancedAmount(quotation.getTmFinanceAmount());
        contract.setVatFinancedAmount(quotation.getVatFinanceAmount());
        contract.setPenaltyRule(financialProduct.getPenaltyRule());
        contract.setTerm(quotation.getTerm());
        contract.setInterestRate(quotation.getInterestRate());
        contract.setFrequency(quotation.getFrequency());
        contract.setNumberOfPrincipalGracePeriods(quotation.getNumberOfPrincipalGracePeriods());
        contract.setTeInstallmentAmount(MyMathUtils.roundAmountTo(quotation.getTeInstallmentAmount()));
        contract.setVatInstallmentAmount(MyMathUtils.roundAmountTo(quotation.getVatInstallmentAmount()));
        contract.setTiInstallmentAmount(MyMathUtils.roundAmountTo(quotation.getTiInstallmentAmount()));
       /* Double irrRate = 100 * Rate.calculateIRR(LoanUtils.getNumberOfPeriods(quotation.getTerm(), quotation.getFrequency()),
                quotation.getTeInstallmentAmount(), quotation.getTiFinanceAmount());
        contract.setIrrRate(MyMathUtils.roundTo(irrRate,2));*/
        contract.setIrrRate(MyMathUtils.roundTo(quotation.getIrrRate(),2));
        contract.setCheckerID(quotation.getCheckerID());
        contract.setCheckerName(quotation.getCheckerName());
        contract.setCheckerPhoneNumber(quotation.getCheckerPhoneNumber());
        contract.setNumberPrepaidTerm(0);
        contract.setLastPaidNumInstallment(0);
        contract.setFirstDueDate(quotation.getFirstDueDate());
        contract.setQuotation(quotation);
        Term term = financialProductService.getNumberOfTermByNbTerm(quotation.getTerm());
        if(term != null ) {
            contract.setPenaltyTermPayoff(term.getTermPayOff());
        }
        int numberGuarantors = 0;
        List<ContractApplicant> contractApplicants = new ArrayList<ContractApplicant>();
        if (quotation.getQuotationApplicants() != null) {
            for (QuotationApplicant quotationApplicant : quotation.getQuotationApplicants()) {
                Applicant guarantor = quotationApplicant.getApplicant();
                ContractApplicant contractApplicant = new ContractApplicant();
                contractApplicant.setApplicant(guarantor);
                contractApplicant.setContract(contract);
                contractApplicant.setApplicantType(quotationApplicant.getApplicantType());
                if (guarantor != null && guarantor.getId() == null) {
                    applicantService.saveOrUpdateApplicant(guarantor);
                }
                saveOrUpdate(contractApplicant);
                if (quotationApplicant.getApplicantType().equals(EApplicantType.G)) {
                    numberGuarantors++;
                }
                contractApplicants.add(contractApplicant);
            }
        }
        contract.setContractApplicants(contractApplicants);
        contract.setNumberGuarantors(numberGuarantors);

        contract.setStartDate(quotation.getContractStartDate());
        contract.setSigatureDate(contract.getStartDate());
        contract.setInitialStartDate(contract.getStartDate());
        contract.setEndDate(DateUtils.addDaysDate(DateUtils.addMonthsDate(contract.getStartDate(), contract.getTerm() * contract.getFrequency().getNbMonths()), -1));
        contract.setInitialEndDate(contract.getEndDate());

        LOG.debug(">> saveOrUpdate contract - Ref. [" + contract.getReference() + "]");
        saveOrUpdate(contract);
        LOG.debug("<< saveOrUpdate contract");

        List<QuotationService> quotationServices = quotation.getQuotationServices();

        if (quotationServices != null && !quotationServices.isEmpty()) {
            for (QuotationService quotationService : quotationServices) {
                ContractFinService contractFinService = new ContractFinService();
                contractFinService.setService(quotationService.getService());
                contractFinService.setContract(contract);
                contractFinService.setVatValue(quotationService.getVatValue());
                contractFinService.setTePrice(quotationService.getTePrice());
                contractFinService.setVatPrice(quotationService.getVatPrice());
                contractFinService.setTiPrice(quotationService.getTiPrice());
                contractFinService.setChargePoint(quotationService.getChargePoint());
                saveOrUpdate(contractFinService);
            }
        }

        // Not to copy Quotation Document to Contract Document
        /*List<QuotationDocument> quotationDocuments = quotation.getQuotationDocuments();

        if (quotationDocuments != null && !quotationDocuments.isEmpty()) {
            for (QuotationDocument quotationDocument : quotationDocuments) {
                ContractDocument contractDocument = new ContractDocument();
                contractDocument.setDocument(quotationDocument.getDocument());
                contractDocument.setContract(contract);
                contractDocument.setReference(quotationDocument.getReference());
                contractDocument.setPath(quotationDocument.getPath());
                contractDocument.setExpireDate(quotationDocument.getExpireDate());
                contractDocument.setIssueDate(quotationDocument.getIssueDate());
                saveOrUpdate(contractDocument);
            }
        }*/


        LOG.debug("<< End Created contract");
        return contract;
    }

    @Override
    public Contract activateContract(Quotation quotation) throws EntityCreationException {
        Assert.notNull(quotation,"Quotation could not be null... ! ");
        Contract contract = createContract(quotation);
        contract = complete(contract,true,true);
        return contract;
    }

    @Override
    public Contract complete(Contract contract, boolean isForced, boolean disburse) throws DealerPaymentException, JournalEntryException {
        LOG.debug(">> activate contract");

       /* contract.setForceActivated(isForced);
        if (disburse) {
            contract.setWkfStatus(ContractWkfStatus.HOLD_PAY);
        } else {
            contract.setWkfStatus(ContractWkfStatus.FIN);
        }*/

        Applicant applicant = contract.getApplicant();
        ApplicantArc applicantArc = ContractServiceHelper.copyApplicant(applicant);
        applicantService.saveOrUpdateApplicantArc(applicantArc);

        if (contract.getStartDate() == null) {
            contract.setStartDate(DateUtils.todayH00M00S00());
        }
        contract.setApplicantArc(applicantArc);
        contract.setApplicant(applicant);

        AssetArc assetArc = ContractServiceHelper.copyAsset(contract.getAsset());
        saveOrUpdate(assetArc);
        contract.setAsset(contract.getAsset());
        contract.setAssetArc(assetArc);

       /* DealerAttribute dealerAttribute = ContractServiceHelper.getDealerAttribute(contract.getDealer(), contract.getAsset().getAssetMake(), contract.getAsset().getModel().getAssetCategory());
        contract.setInsuranceCompany(dealerAttribute.getInsuranceCompany());*/

        saveOrUpdate(contract);
        String loginUser = UserSessionManager.getCurrentUser() == null ? "test" : UserSessionManager.getCurrentUser().getLogin();
        String desc = I18N.message("msg.contract.activated", new String[] {contract.getReference(), loginUser});
        //finHistoryService.addFinHistory(contract, FinHistoryType.FIN_HIS_SYS, desc);
        LOG.debug("<< activate contract");
        return contract;
    }

    @Override
    public List<String> validation(Contract contract, Date firstDueDate, String chassisNo, String engineNo, String taxInvoiceNumber) {
        return null;
    }
}
