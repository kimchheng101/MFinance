package com.soma.mfinance.core.history.service.impl;

import java.util.List;

import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.history.FinHistory;
import com.soma.mfinance.core.history.FinHistoryType;
import com.soma.mfinance.core.history.service.FinHistoryRestriction;
import com.soma.mfinance.core.history.service.FinHistoryService;

/**
 * 
 * @author kimsuor.seang
 */
@Service("finHistoryService")
public class FinHistoryServiceImpl extends BaseEntityServiceImpl implements FinHistoryService {

	/** */
	private static final long serialVersionUID = -989132212019489930L;

	@Autowired
    private EntityDao dao;
	
	/**
	 * @see org.seuksa.frmk.service.impl.BaseEntityServiceImpl#getDao()
	 */
	@Override
	public BaseEntityDao getDao() {
		return dao;
	}
	
	/**
	 * @see com.soma.mfinance.core.history.service.FinHistoryService#addFinHistory(Contract, com.soma.mfinance.core.history.FinHistoryType, java.lang.String)
	 */
	@Override
	public void addFinHistory(Contract contract, FinHistoryType historyType, String comment) {
		FinHistory history = FinHistory.createInstance();
		history.setContract(contract);
		history.setType(historyType);
		history.setComment(comment);
		create(history);
	}
	
	/**
	 * @see com.soma.mfinance.core.history.service.FinHistoryService#getFinHistories(java.lang.Long, com.soma.mfinance.core.history.FinHistoryType[])
	 */
	@Override
	public List<FinHistory> getFinHistories(Long conId, FinHistoryType[] historyTypes) {
		FinHistoryRestriction restrictions = new FinHistoryRestriction();
		restrictions.setConId(conId);
		restrictions.setFinHistoryTypes(historyTypes);
		return list(restrictions);
	}
	
}
