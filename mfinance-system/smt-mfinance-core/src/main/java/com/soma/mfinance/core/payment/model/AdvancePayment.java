package com.soma.mfinance.core.payment.model;

import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;

/**
 * Created by b.chea on 2/22/2017.
 */
@Entity
@Table(name = "tu_advance_payment")
public class AdvancePayment extends EntityRefA {

    String advancePaymentPercent;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "adv_pay_sta_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Override
    @Transient
    public String getCode() {
        return "Null";
    }

    @Column(name = "adv_pay_sta_desc", nullable = false, length = 50)
    @Override
    public String getDesc() {
        return super.getDesc();
    }

    @Override
    @Column(name = "adv_pay_desc_en", nullable = false, length=50)
    public String getDescEn() {
        return super.getDescEn();
    }

    @Column(name = "adv_pay_percent", nullable = false, length=50)
    public String getAdvancePaymentPercent() {
        return advancePaymentPercent;
    }

    public void setAdvancePaymentPercent(String advancePaymentPercent){
        this.advancePaymentPercent= advancePaymentPercent;
    }
}
