package com.soma.mfinance.core.asset.panel.appraisal.group;

import com.soma.mfinance.core.asset.model.appraisal.AppraisalGroup;
import com.soma.mfinance.core.shared.component.ComponentLayoutFactory;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by Kimsuor SEANG
 * Date  : 7/19/2017
 * Name  : 1:48 PM
 * Email : k.seang@gl-f.com
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AppraisalGroupFormPanel extends AbstractFormPanel {

    private CheckBox cbActive;
    private TextField txtCode;
    private TextField txtDescEn;
    private TextField txtSortIndex;
    private TextField txtCoefficient;

    private AppraisalGroup appraisalGroup ;
    /**
     *
     */
    @PostConstruct
    public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addSaveClickListener(this);
    }

    @Override
    protected Entity getEntity() {
        appraisalGroup.setCoefficient(getDouble(txtCoefficient));
        appraisalGroup.setDescEn(txtDescEn.getValue());
        appraisalGroup.setCode(txtCode.getValue());
        appraisalGroup.setStatusRecord(cbActive.getValue() ? EStatusRecord.ACTIV : EStatusRecord.INACT);
        appraisalGroup.setSortIndex(getInteger(txtSortIndex));
        return appraisalGroup;
    }

    /**
     * @param appraisalGroupId
     */
    public void assignValues(Long appraisalGroupId) {
        super.reset();
        if (appraisalGroupId != null) {
            appraisalGroup = ENTITY_SRV.getById(AppraisalGroup.class,appraisalGroupId);
            txtCode.setValue(appraisalGroup.getCode());
            txtDescEn.setValue(appraisalGroup.getDescEn());
            txtSortIndex.setValue(getDefaultString(appraisalGroup.getSortIndex()));
            txtCoefficient.setValue(getDefaultString(appraisalGroup.getCoefficient()));
        }
    }

    @Override
    public void reset() {
        super.reset();
        appraisalGroup = AppraisalGroup.createInstance();
        txtCoefficient.setValue("");
        txtDescEn.setValue("");
        txtCode.setValue("");
        cbActive.setValue(true);
        txtSortIndex.setValue("");
    }

    @Override
    protected com.vaadin.ui.Component createForm() {
        txtSortIndex = ComponentFactory.getTextField("sortIndex", false, 60, 180);
        txtCode =  ComponentFactory.getTextField("code", true, 60, 180);
        txtDescEn = ComponentFactory.getTextField("name", true, 60, 180);
        cbActive = new CheckBox(I18N.message("active"));
        cbActive.setValue(true);

        txtCoefficient = ComponentFactory.getTextField("coefficient", true, 60, 180);

        FormLayout frmLayout = ComponentLayoutFactory.getFormLayoutCaptionAlignLeft();
        frmLayout.setMargin(new MarginInfo(false, false, false, true));
        frmLayout.addComponent(txtCode);
        frmLayout.addComponent(txtDescEn);
        frmLayout.addComponent(txtCoefficient);
        frmLayout.addComponent(txtSortIndex);
        frmLayout.addComponent(cbActive);
        return frmLayout;
    }
}
