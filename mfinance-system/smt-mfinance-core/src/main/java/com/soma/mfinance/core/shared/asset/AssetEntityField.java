package com.soma.mfinance.core.shared.asset;

import com.soma.mfinance.core.shared.FMEntityField;

/**
 * @author kimsuor.seang
 */
public interface AssetEntityField extends FMEntityField {

}
