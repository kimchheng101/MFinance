package com.soma.mfinance.core.application.model;

import com.soma.ersys.core.hr.model.eref.EEmploymentIndustry;
import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;

/**
 * Created by Kimsuor SEANG
 * Date  : 6/21/2017
 * Name  : 9:06 AM
 * Email : k.seang@gl-f.com
 */
@Entity
@Table(name = "tu_employment_occupation",
        indexes = {@Index(
                name = "idx_emp_ind_id",columnList = "emp_ind_id")
        })
public class EmploymentOccupation extends EntityRefA {

    private static final long serialVersionUID = -950028282910683142L;
    private EEmploymentIndustry employmentIndustry;

    /**
     * @see org.seuksa.frmk.mvc.model.entity.EntityA#getId()
     */
    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "emp_occ_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name = "emp_ind_id",nullable = false)
    @Convert(
            converter = EEmploymentIndustry.class
    )
    public EEmploymentIndustry getEmploymentIndustry() {
        return employmentIndustry;
    }

    public void setEmploymentIndustry(EEmploymentIndustry employmentIndustry) {
        this.employmentIndustry = employmentIndustry;
    }

    /**
     * @see
     */
    @Column(name = "emp_occ_code", nullable = false, unique = true)
    @Override
    public String getCode() {
        return super.getCode();
    }

    /**
     * @see
     */
    @Column(name = "emp_occ_desc", nullable = true)
    @Override
    public String getDesc() {
        return super.getDesc();
    }

    @Column(name = "emp_occ_desc_en", nullable = true)
    @Override
    public String getDescEn() {
        return super.getDescEn();
    }
}
