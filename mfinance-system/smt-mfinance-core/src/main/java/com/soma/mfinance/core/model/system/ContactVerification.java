package com.soma.mfinance.core.model.system;

import com.soma.frmk.security.model.SecProfile;
import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;

/**
 * Contact Evidence
 * @author kimsuor.seang
 */
@Entity
@Table(name = "tu_contact_verification")
public class ContactVerification extends EntityRefA {
	private static final long serialVersionUID = -5906890512077766966L;

	private SecProfile profile;
	
	/**
     * @see org.seuksa.frmk.mvc.model.entity.EntityA#getId()
     */
    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ctevi_id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	/**
	 * @see org.seuksa.frmk.mvc.model.entity.AuditEntityRef#getCode()
	 */
	@Override
	@Transient
	public String getCode() {
		return "code";
	}

	/**
	 * @see org.seuksa.frmk.mvc.model.entity.AuditEntityRef#getDesc()
	 */
	@Column(name = "ctevi_desc", nullable = false, length=50)
	@Override
    public String getDesc() {
        return super.getDesc();
    }
	
	/**
     * @return <String>
     */
    @Override
    @Column(name = "ctevi_desc_en", nullable = false, length=50)
    public String getDescEn() {
        return super.getDescEn();
    }

	/**
	 * @return the profile
	 */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_pro_id")
	public SecProfile getProfile() {
		return profile;
	}

	/**
	 * @param profile the profile to set
	 */
	public void setProfile(SecProfile profile) {
		this.profile = profile;
	}
    
}
