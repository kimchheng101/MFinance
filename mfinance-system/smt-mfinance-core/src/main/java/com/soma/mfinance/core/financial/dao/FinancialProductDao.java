package com.soma.mfinance.core.financial.dao;

import org.seuksa.frmk.dao.BaseEntityDao;

/**
 * Financial product data model access
 * @author kimsuor.seang
 */
public interface FinancialProductDao extends BaseEntityDao {

}
