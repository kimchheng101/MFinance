package com.soma.mfinance.core.contract.model.schedule;

import com.soma.mfinance.core.contract.model.MContract;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.frmk.security.model.SecUser;
import org.seuksa.frmk.model.EntityFactory;
import org.seuksa.frmk.model.entity.EntityA;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by kimsuor.seang
 * USER   c.chhai
 * DATE on 6/8/2017.
 * TIME at 11:10 AM
 * EMIAL   c.chhai@gl-f.com
 */

@Transactional
@Entity
@Table(name  = "td_contract_activate_schedule")
public class ContractActivateSchedule extends EntityA implements MContract {

	private Quotation quotation;
	private SecUser performUser;
	private SecUser executedUser;
    private Date performDate;
	private boolean executed;
	private Date executedDate;


	public static ContractActivateSchedule createInstance() {
		ContractActivateSchedule contractActivateSchedule = EntityFactory.createInstance(ContractActivateSchedule.class);
		return contractActivateSchedule;
	}

	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "con_act_sch_id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "quo_id", nullable = false)
	public Quotation getQuotation() {
		return quotation;
	}

	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sec_usr_id")
	public SecUser getPerformUser() {
		return performUser;
	}

	public void setPerformUser(SecUser performUser) {
		this.performUser = performUser;
	}

	@Column(name = "con_act_sch_usr_executed")
	public SecUser getExecutedUser() {
		return executedUser;
	}

	public void setExecutedUser(SecUser executedUser) {
		this.executedUser = executedUser;
	}

	@Column(name = "con_act_sch_dt_perform")
	public Date getPerformDate() {
		return performDate;
	}

	public void setPerformDate(Date performDate) {
		this.performDate = performDate;
	}

	@Column(name = "con_act_sch_bl_executed")
	public boolean getExecuted() {
		return executed;
	}

	public void setExecuted(boolean executed) {
		this.executed = executed;
	}

	@Column(name = "con_act_sch_dt_executed")
	public Date getExecutedDate() {
		return executedDate;
	}

	public void setExecutedDate(Date executedDate) {
		this.executedDate = executedDate;
	}
}
