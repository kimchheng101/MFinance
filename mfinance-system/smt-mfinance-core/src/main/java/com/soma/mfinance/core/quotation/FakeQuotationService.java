package com.soma.mfinance.core.quotation;

import java.util.Date;

import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.quotation.model.Quotation;

public interface FakeQuotationService extends BaseEntityService {

	Quotation simulateCreateQuotation(Long dealerId, Date startDate, Date firstInstallamentDate);
}
