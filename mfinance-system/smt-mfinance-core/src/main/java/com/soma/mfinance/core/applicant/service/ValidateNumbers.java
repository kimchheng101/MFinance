package com.soma.mfinance.core.applicant.service;

import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.TextField;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by d.dim on 19-May-17.
 * modified by th.seng
 */
public class ValidateNumbers {

    public static void validateNumberDot(List<TextField> textFields) {
        textFields.forEach(ValidateNumbers::validateNumberDot);
    }
    public static void validateNumberDot(TextField textField){
        textField.addTextChangeListener(event -> {
            String text = event.getText().replaceAll("[^\\d.]", "");
            textField.setValue(text);
        });
        textField.setTextChangeEventMode(AbstractTextField.TextChangeEventMode.EAGER);
    }

    public static void validateNumber(List<TextField> textFields) {
        textFields.forEach(ValidateNumbers::validateNumber);
    }

    public static void validateNumber(TextField textField){
        textField.addTextChangeListener(event -> {
            String text = event.getText().replaceAll("[^\\d]", "");
            textField.setValue(text);
        });
        textField.setTextChangeEventMode(AbstractTextField.TextChangeEventMode.EAGER);
    }

    public static Boolean isValidOnlyNumber(String number) {

        if(!StringUtils.isEmpty(number)) {
            Pattern pattern = Pattern.compile("[0-9 ]*");
            Matcher matcher = pattern.matcher(number);
            if (matcher.matches())
                return true;
        }else if(StringUtils.isEmpty(number)){
            return true;
        }
        return false;
    }

    public static Boolean isValidPhoneNumber(String phone) {

        if(!StringUtils.isEmpty(phone)) {
            Pattern pattern = Pattern.compile("(^1800|1900|1902\\d{6}$)|(^0[1|2|3|7|6|8|9]{1}[0-9]{8}$)|(^0[1|2|3|7|6|8|9]{1}[0-9]{7}$)|(^04\\d{2,3}\\d{6}$)");
            Matcher matcher = pattern.matcher(phone);
            if (matcher.matches())
                return true;
        }else if(StringUtils.isEmpty(phone)){
            return true;
        }
        return false;
    }
}
