package com.soma.mfinance.core.shared.system;

import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;

/**
 * @author by kimsuor.seang  on 10/18/2017.
 */
@Entity
@Table(name = "tu_types_of_business")
public class BusinessTypes extends EntityRefA {

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "busty_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Override
    @Transient
    public String getCode() {
        return desc;
    }


    @Column(name = "busty_desc", nullable = false, length = 50)
    @Override
    public String getDesc() {
        return super.getDesc();
    }


    @Override
    @Column(name = "busty_desc_en", nullable = false, length = 50)
    public String getDescEn() {
        return super.getDescEn();
    }
}
