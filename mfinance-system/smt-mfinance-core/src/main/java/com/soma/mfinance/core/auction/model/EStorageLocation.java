package com.soma.mfinance.core.auction.model;

import org.seuksa.frmk.model.eref.BaseERefData;

import javax.persistence.AttributeConverter;
import java.util.List;

/**
 * 
 * @author th.seng
 *
 */
public class EStorageLocation extends BaseERefData implements AttributeConverter<EStorageLocation, Long> {

	private static final long serialVersionUID = 8800717903072047607L;

	public EStorageLocation() {
	}

	public EStorageLocation(String code, long id) {
		super(code, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public EStorageLocation convertToEntityAttribute(Long id) {
		return super.convertToEntityAttribute(id);
	}
	
	@Override
	public Long convertToDatabaseColumn(EStorageLocation arg0) {
		return super.convertToDatabaseColumn(arg0);
	}

	public static List<EStorageLocation> values() {
		return getValues(EStorageLocation.class);
	}
	
	public static EStorageLocation getByCode(String code) {
		return getByCode(EStorageLocation.class, code);
	}
	
	public static EStorageLocation getById(long id) {
		return getById(EStorageLocation.class, id);
	}
}
