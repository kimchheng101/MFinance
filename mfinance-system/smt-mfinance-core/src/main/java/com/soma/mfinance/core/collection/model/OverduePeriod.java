package com.soma.mfinance.core.collection.model;

import org.seuksa.frmk.model.entity.EntityA;
import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by s.torn on 6/28/2017.
 */
@Entity
@Table(name = "tu_overdue_period")
public class OverduePeriod extends EntityRefA {

    private String overdueFrom;
    private String overdueTo;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ovd_prd_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name = "ovd_prd_from", nullable = false)
    public String getOverdueFrom() {
        return overdueFrom;
    }

    public void setOverdueFrom(String overdueFrom) {
        this.overdueFrom = overdueFrom;
    }

    @Column(name = "ovd_prd_to", nullable = false)
    public String getOverdueTo() {
        return overdueTo;
    }

    public void setOverdueTo(String overdueTo) {
        this.overdueTo = overdueTo;
    }

    /**
     * @see org.seuksa.frmk.model.entity.AuditEntityRef#getDesc()
     */
    @Column(name = "ovd_prd_desc", nullable = true, length = 255)
    @Override
    public String getDesc() {
        return super.getDesc();
    }
    /**
     * @see org.seuksa.frmk.model.entity.EntityRefA#getDescEn()
     */
    @Column(name = "ovd_prd_full_desc_en", nullable = true, length = 255)
    @Override
    public String getDescEn() {
        return getOverdueFrom()+ " - "+getOverdueTo();
    }
}
