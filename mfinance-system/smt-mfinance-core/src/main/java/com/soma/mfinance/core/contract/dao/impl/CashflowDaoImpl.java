package com.soma.mfinance.core.contract.dao.impl;

import org.seuksa.frmk.dao.impl.BaseEntityDaoImpl;
import org.springframework.stereotype.Repository;

import com.soma.mfinance.core.contract.dao.CashflowDao;

/**
 * Cashflow data access implementation
 * @author kimsuor.seang
 *
 */
@Repository
public class CashflowDaoImpl extends BaseEntityDaoImpl implements CashflowDao {

}
