package com.soma.mfinance.core.accounting;

import com.soma.mfinance.core.financial.model.FinService;

/**
 * 
 * @author vi.sok
 *
 */
public class ServiceFeeVO {
	private double tiAmount;
	private FinService service;
	private double vat;
	
	public ServiceFeeVO(double amount,double vat,FinService service){
		this.tiAmount = amount;
		this.service = service;
		this.vat = vat;
	}
	
	/**
	 * @return the tiAmount
	 */
	public double getTiAmount() {
		return tiAmount;
	}

	/**
	 * @param tiAmount the tiAmount to set
	 */
	public void setTiAmount(double tiAmount) {
		this.tiAmount = tiAmount;
	}
	/**
	 * @return the service
	 */
	public FinService getService() {
		return service;
	}

	/**
	 * @param service the service to set
	 */
	public void setService(FinService service) {
		this.service = service;
	}

	/**
	 * @return the vat
	 */
	public double getVat() {
		return vat;
	}

	/**
	 * @param vat the vat to set
	 */
	public void setVat(double vat) {
		this.vat = vat;
	}
	

}
