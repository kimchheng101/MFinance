package com.soma.mfinance.core.applicant.panel.otherInformation;

/**
 * Created by b.chea on 2/2/2017.
 */

import com.soma.mfinance.core.Enum.ECertifyCurrentAddress;
import com.soma.mfinance.core.Enum.ECertifyIncome;
import com.soma.mfinance.core.Enum.ECorrespondence;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.mfinance.core.applicant.service.ValidateNumbers;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.ersys.core.hr.model.eref.ERelationship;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.mfinance.core.applicant.service.ValidateNumbers;
import com.vaadin.data.Property;
import com.vaadin.ui.*;
import org.apache.commons.lang.BooleanUtils;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.amount.AmountUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class OtherInfoPanel extends AbstractTabPanel{

    private TextField txtMonthlyPersonalExpenses;
    private TextField txtMonthlyFamilyExpenses;
    private TextField txtTotalExpenses;
    private OptionGroup optDebtOtherSource;
    private TextField txtTotalMonthlyInstallment;
    private TextField txtConvenientTimeforVisit;
    private ERefDataComboBox<ECorrespondence> cbxPlaceSendingCorrespondence;
    private ERefDataComboBox<ECertifyCurrentAddress> cbxCertifyCurrentAddress;
    private ERefDataComboBox<ECertifyIncome> cbxCertifyIncome;
    private TextField txtChiefName;
    private TextField txtChiefNumber;
    private TextField txtDeputyChiefName;
    private TextField txtDeputyChiefNumber;
    private ERefDataComboBox<ERelationship> cbxMember1;
    private ERefDataComboBox<ERelationship> cbxMember2;
    private ERefDataComboBox<ERelationship> cbxMember3;
    private ERefDataComboBox<ERelationship> cbxMember4;
    private ERefDataComboBox<ERelationship> cbxMember5;
    private ERefDataComboBox<ERelationship> cbxMember6;
    private TextField txtIncome1;
    private TextField txtIncome2;
    private TextField txtIncome3;
    private TextField txtIncome4;
    private TextField txtIncome5;
    private TextField txtIncome6;
    private TextField txtContract1;
    private TextField txtContract2;
    private TextField txtContract3;
    private TextField txtContract4;
    private TextField txtContract5;
    private TextField txtContract6;
    private TextField txtTotalIncome;
    private TextField txtTotalFamilyMembers;

    public OtherInfoPanel() {
        super();
        setMargin(true);
        setSizeFull();
    }

    @Override
    protected Component createForm() {
        ValidateNumbers applcInfo = new ValidateNumbers();
        ArrayList validTextInfo = new ArrayList();
        VerticalLayout otherLayout = new VerticalLayout();
        otherLayout.setMargin(true);
        otherLayout.setSpacing(true);

        cbxCertifyCurrentAddress = new ERefDataComboBox<ECertifyCurrentAddress>(ECertifyCurrentAddress.values());
        cbxCertifyCurrentAddress.setImmediate(true);
        cbxCertifyCurrentAddress.setWidth("220px");

        cbxPlaceSendingCorrespondence = new ERefDataComboBox<ECorrespondence>(ECorrespondence.values());
        cbxPlaceSendingCorrespondence.setImmediate(true);
        cbxPlaceSendingCorrespondence.setWidth("200px");

        cbxCertifyIncome = new ERefDataComboBox<ECertifyIncome>(ECertifyIncome.values());
        cbxCertifyIncome.setImmediate(true);
        cbxCertifyIncome.setWidth("200px");

        cbxMember1 = new ERefDataComboBox<ERelationship>(ERelationship.values());
        cbxMember1.setImmediate(true);
        cbxMember1.setWidth("100px");

        cbxMember4 = new ERefDataComboBox<ERelationship>(ERelationship.values());
        cbxMember4.setImmediate(true);
        cbxMember4.setWidth("100px");

        cbxMember2 = new ERefDataComboBox<ERelationship>(ERelationship.values());
        cbxMember2.setImmediate(true);
        cbxMember2.setWidth("100px");

        cbxMember5 = new ERefDataComboBox<ERelationship>(ERelationship.values());
        cbxMember5.setImmediate(true);
        cbxMember5.setWidth("100px");

        cbxMember3 = new ERefDataComboBox<ERelationship>(ERelationship.values());
        cbxMember3.setImmediate(true);
        cbxMember3.setWidth("100px");

        cbxMember6 = new ERefDataComboBox<ERelationship>(ERelationship.values());
        cbxMember6.setImmediate(true);
        cbxMember6.setWidth("100px");

        txtMonthlyPersonalExpenses = ComponentFactory.getTextField(40, 140);
        validTextInfo.add(txtMonthlyPersonalExpenses);
        txtMonthlyPersonalExpenses.addValueChangeListener(new Property.ValueChangeListener() {
            private static final long serialVersionUID = 1522323948219241644L;
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                txtTotalExpenses.setValue(getDefaultString(getTotalMonthlyExpenses()));
            }
        });
        txtMonthlyFamilyExpenses = ComponentFactory.getTextField(40, 140);
        validTextInfo.add(txtMonthlyFamilyExpenses);
        txtMonthlyFamilyExpenses.addValueChangeListener(new Property.ValueChangeListener() {
            private static final long serialVersionUID = 1522323948219241644L;
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                txtTotalExpenses.setValue(getDefaultString(getTotalMonthlyExpenses()));
            }
        });
        txtTotalExpenses = ComponentFactory.getTextField(40, 140);
        txtTotalExpenses.setEnabled(false);
        txtTotalExpenses.addStyleName("blackdisabled");

        txtChiefName = ComponentFactory.getTextField(40, 140);
        txtChiefNumber = ComponentFactory.getTextField(40, 140);
        txtDeputyChiefName = ComponentFactory.getTextField(40, 140);
        txtDeputyChiefNumber = ComponentFactory.getTextField(40, 140);
        txtIncome1 = ComponentFactory.getTextField(40, 100);
        validTextInfo.add(txtIncome1);
        txtIncome1.addValueChangeListener(new Property.ValueChangeListener() {
            private static final long serialVersionUID = 1522323948219241644L;
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                txtTotalIncome.setValue(getDefaultString(getTotalFamilyIncome()));
            }
        });
        txtContract1 = ComponentFactory.getTextField(40, 120);
        validTextInfo.add(txtContract1);
        txtIncome4 = ComponentFactory.getTextField(40, 100);
        validTextInfo.add(txtIncome4);
        txtIncome4.addValueChangeListener(new Property.ValueChangeListener() {
            private static final long serialVersionUID = 1522323948219241644L;
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                txtTotalIncome.setValue(getDefaultString(getTotalFamilyIncome()));
            }
        });
        txtContract4 = ComponentFactory.getTextField(40, 120);
        validTextInfo.add(txtContract4);
        txtIncome2 = ComponentFactory.getTextField(40, 100);
        validTextInfo.add(txtIncome2);
        txtIncome2.addValueChangeListener(new Property.ValueChangeListener() {
            private static final long serialVersionUID = 1522323948219241644L;
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                txtTotalIncome.setValue(getDefaultString(getTotalFamilyIncome()));
            }
        });
        txtContract2 = ComponentFactory.getTextField(40, 120);
        validTextInfo.add(txtContract2);
        txtIncome5 = ComponentFactory.getTextField(40, 100);
        validTextInfo.add(txtIncome5);
        txtIncome5.addValueChangeListener(new Property.ValueChangeListener() {
            private static final long serialVersionUID = 1522323948219241644L;
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                txtTotalIncome.setValue(getDefaultString(getTotalFamilyIncome()));
            }
        });
        txtContract5 = ComponentFactory.getTextField(40, 120);
        validTextInfo.add(txtContract5);
        txtIncome3 = ComponentFactory.getTextField(40, 100);
        validTextInfo.add(txtIncome3);
        txtIncome3.addValueChangeListener(new Property.ValueChangeListener() {
            private static final long serialVersionUID = 1522323948219241644L;
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                txtTotalIncome.setValue(getDefaultString(getTotalFamilyIncome()));
            }
        });
        txtContract3 = ComponentFactory.getTextField(40, 120);
        validTextInfo.add(txtContract3);
        txtIncome6 = ComponentFactory.getTextField(40, 100);
        validTextInfo.add(txtIncome6);
        txtIncome6.addValueChangeListener(new Property.ValueChangeListener() {
            private static final long serialVersionUID = 1522323948219241644L;
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                txtTotalIncome.setValue(getDefaultString(getTotalFamilyIncome()));
            }
        });
        txtContract6 = ComponentFactory.getTextField(40, 120);
        validTextInfo.add(txtContract6);

        optDebtOtherSource = new OptionGroup();
        optDebtOtherSource.addItem(1);
        optDebtOtherSource.setItemCaption(1, I18N.message("yes"));
        optDebtOtherSource.addItem(0);
        optDebtOtherSource.setItemCaption(0, I18N.message("no"));
        optDebtOtherSource.select(0);
        optDebtOtherSource.addStyleName("horizontal");

        txtTotalMonthlyInstallment = ComponentFactory.getTextField(40, 140);
        txtTotalMonthlyInstallment.setEnabled(false);
        txtTotalMonthlyInstallment.setStyleName("blackdisabled");

        txtConvenientTimeforVisit = ComponentFactory.getTextField(40, 280);
        txtTotalIncome = ComponentFactory.getTextField(40, 140);
        validTextInfo.add(txtTotalIncome);
        txtTotalIncome.setEnabled(false);
        txtTotalIncome.addStyleName("blackdisabled");

        txtTotalFamilyMembers = ComponentFactory.getTextField(40, 140);
        validTextInfo.add(txtTotalFamilyMembers);
        CustomLayout customLayout = createCustomLayout("otherInfo/otherInfoLayout");

        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("monthly.personal.expenses")), "lblPersonalExpenses");
        customLayout.addComponent(txtMonthlyPersonalExpenses, "txtPersonalExpenses");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("monthly.family.expenses")), "lblFamilyExpenses");
        customLayout.addComponent(txtMonthlyFamilyExpenses, "txtFamilyExpenses");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("total.expenses")), "lblExpenses");
        customLayout.addComponent(txtTotalExpenses, "txtExpenses");

        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("debt.from.other.source")), "lblOtherSource");
        customLayout.addComponent(optDebtOtherSource, "optDebtOtherSource");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("total.debt.monthly.installment")), "lblTotalMonthlyInstallment");
        customLayout.addComponent(txtTotalMonthlyInstallment, "txtTotalMonthlyInstallment");

        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("convenient.time.for.visit")), "lblConvenientTimeForVisit");
        customLayout.addComponent(txtConvenientTimeforVisit, "txtConvenientTimeForVisit");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("name")), "lblName");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("telephone.number")), "lblTelephoneNumber");

        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("place.sending.correspondence")), "lblPlaceSendingCorrespondence");
        customLayout.addComponent(cbxPlaceSendingCorrespondence, "cbxPlaceSendingCorrespondence");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("chief.village")), "lblChiefVillage");
        customLayout.addComponent(txtChiefName, "txtChiefName");
        customLayout.addComponent(txtChiefNumber, "txtChiefPhone");

        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("certify.current.address")), "lblCertifyCurrentAddress");
        customLayout.addComponent(cbxCertifyCurrentAddress, "cbxCertifyCurrentAddress");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("deputy.chief.village")), "lblDeputyChiefVillage");
        customLayout.addComponent(txtDeputyChiefName, "txtDeputyChiefName");
        customLayout.addComponent(txtDeputyChiefNumber, "txtDeputyChiefPhone");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("certify.income")), "lblCertifyIncome");
        customLayout.addComponent(cbxCertifyIncome, "cbxCertifyIncome");

        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("income")), "lblIncome");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("contract.number")), "lblContactNumber");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("income")), "lblIncome2");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("contract.number")), "lblContactNumber2");

        customLayout.addComponent(ComponentFactory.getLabel("1"), "lbl1");
        customLayout.addComponent(cbxMember1, "cbxMember1");
        customLayout.addComponent(txtIncome1, "txtIncome1");
        customLayout.addComponent(txtContract1, "txtContract1");
        customLayout.addComponent(ComponentFactory.getLabel("4"), "lbl4");
        customLayout.addComponent(cbxMember4, "cbxMember4");
        customLayout.addComponent(txtIncome4, "txtIncome4");
        customLayout.addComponent(txtContract4, "txtContract4");

        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("reference.member.info")), "lblReferenceMember");
        customLayout.addComponent(ComponentFactory.getLabel("2"), "lbl2");
        customLayout.addComponent(cbxMember2, "cbxMember2");
        customLayout.addComponent(txtIncome2, "txtIncome2");
        customLayout.addComponent(txtContract2, "txtContract2");
        customLayout.addComponent(ComponentFactory.getLabel("5"), "lbl5");
        customLayout.addComponent(cbxMember5, "cbxMember5");
        customLayout.addComponent(txtIncome5, "txtIncome5");
        customLayout.addComponent(txtContract5, "txtContract5");

        customLayout.addComponent(ComponentFactory.getLabel("3"), "lbl3");
        customLayout.addComponent(cbxMember3, "cbxMember3");
        customLayout.addComponent(txtIncome3, "txtIncome3");
        customLayout.addComponent(txtContract3, "txtContract3");
        customLayout.addComponent(ComponentFactory.getLabel("6"), "lbl6");
        customLayout.addComponent(cbxMember6, "cbxMember6");
        customLayout.addComponent(txtIncome6, "txtIncome6");
        customLayout.addComponent(txtContract6, "txtContract6");

        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("total.accumulate.Household.income")), "lblTotalIncome");
        customLayout.addComponent(ComponentFactory.getLabel(":"), "lblLast");
        customLayout.addComponent(txtTotalIncome, "txtTotalIncome");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("total.family.member")), "lblTotalNumberOfFamily");
        customLayout.addComponent(txtTotalFamilyMembers, "txtTotalMember");

        optDebtOtherSource.addValueChangeListener(event -> {
            if (optDebtOtherSource.getValue().equals(1)) {
                txtTotalMonthlyInstallment.setEnabled(true);
                validTextInfo.add(txtTotalMonthlyInstallment);
            } else if (optDebtOtherSource.getValue().equals(0)) {
                txtTotalMonthlyInstallment.setEnabled(false);
                txtTotalMonthlyInstallment.setStyleName("blackdisabled");
            }
        });

        if (ProfileUtil.isAdmin()){
            txtMonthlyPersonalExpenses.setEnabled(true);
            txtMonthlyFamilyExpenses.setEnabled(true);
        }

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setMargin(true);
        horizontalLayout.addComponent(customLayout);
        applcInfo.validateNumberDot(validTextInfo);

        return horizontalLayout;
    }

    private CustomLayout createCustomLayout(String template) {
        InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
        CustomLayout customLayout = null;
        try {
            customLayout = new CustomLayout(layoutFile);
        } catch (IOException e) {
            Notification.show("Could not locate template " + template, e.getMessage(), Notification.Type.ERROR_MESSAGE);
        }
        return customLayout;
    }

    public void getOtherInfo(Individual individual) {
        individual.setMonthlyPersonalExpenses(getDouble(txtMonthlyPersonalExpenses, 0));
        individual.setMonthlyFamilyExpenses(getDouble(txtMonthlyFamilyExpenses, 0));
        individual.setMonthlyExpenses(getDouble(txtTotalExpenses, 0));
        individual.setConvenientVisitTime(txtConvenientTimeforVisit.getValue());
        if (optDebtOtherSource.getValue() != null) {
            individual.setDebtFromOtherSource(Integer.parseInt(optDebtOtherSource.getValue().toString()) == 1);
            individual.setTotalDebtInstallment(getDouble(txtTotalMonthlyInstallment, 0d));
        }
        individual.setChiefVillageName(txtChiefName.getValue());
        individual.setChiefVillagePhoneNumber(txtChiefNumber.getValue());
        individual.setDeputyChiefVillageName(txtDeputyChiefName.getValue());
        individual.setDeputyChiefVillagePhoneNumber(txtDeputyChiefNumber.getValue());

        if (cbxCertifyIncome.getSelectedEntity() == null) {
            individual.setECertifyIncome(ECertifyIncome.TEST);
        } else {
            individual.setECertifyIncome(cbxCertifyIncome.getSelectedEntity());
        }

        if (cbxCertifyCurrentAddress.getSelectedEntity() == null) {
            individual.setECertifyCurrentAddress(ECertifyCurrentAddress.TEST);
        } else {
            individual.setECertifyCurrentAddress(cbxCertifyCurrentAddress.getSelectedEntity());
        }

        if (cbxPlaceSendingCorrespondence.getSelectedEntity() == null) {
            individual.setECorrespondence(ECorrespondence.CURRENT_ADDRESS);
        } else {
            individual.setECorrespondence(cbxPlaceSendingCorrespondence.getSelectedEntity());
        }

        individual.setMonthlyIncomeMember1(getDouble(txtIncome1, 0));
        individual.setMonthlyIncomeMember2(getDouble(txtIncome2, 0));
        individual.setMonthlyIncomeMember3(getDouble(txtIncome3, 0));
        individual.setMonthlyIncomeMember4(getDouble(txtIncome4, 0));
        individual.setMonthlyIncomeMember5(getDouble(txtIncome5, 0));
        individual.setMonthlyIncomeMember6(getDouble(txtIncome6, 0));

        individual.setContactMember1(txtContract1.getValue());
        individual.setContactMember2(txtContract2.getValue());
        individual.setContactMember3(txtContract3.getValue());
        individual.setContactMember4(txtContract4.getValue());
        individual.setContactMember5(txtContract5.getValue());
        individual.setContactMember6(txtContract6.getValue());

        individual.setRelationship1(cbxMember1.getSelectedEntity());
        individual.setRelationship2(cbxMember2.getSelectedEntity());
        individual.setRelationship3(cbxMember3.getSelectedEntity());
        individual.setRelationship4(cbxMember4.getSelectedEntity());
        individual.setRelationship5(cbxMember5.getSelectedEntity());
        individual.setRelationship6(cbxMember6.getSelectedEntity());

        individual.setTotalFamilyMember(getInteger(txtTotalFamilyMembers, 0));
        individual.setTotalAllFamilyMemberIncome(getDouble(txtTotalIncome, 0));

    }

    public void assignValues(Applicant applicant) {

        Individual individual = applicant.getIndividual();

        if (individual != null) {

            txtMonthlyPersonalExpenses.setValue(AmountUtils.format(individual.getMonthlyPersonalExpenses()));
            txtMonthlyFamilyExpenses.setValue(AmountUtils.format(individual.getMonthlyFamilyExpenses()));
            txtTotalExpenses.setValue(AmountUtils.format(individual.getMonthlyExpenses()));
            txtConvenientTimeforVisit.setValue(getDefaultString(individual.getConvenientVisitTime()));
            optDebtOtherSource.setValue(BooleanUtils.toBoolean(individual.isDebtFromOtherSource()) ? 1 : 0);

            if (individual.getECertifyIncome() != null) {
                cbxCertifyIncome.setSelectedEntity(individual.getECertifyIncome());
            }

            if (individual.getECertifyCurrentAddress() != null) {
                cbxCertifyCurrentAddress.setSelectedEntity(individual.getECertifyCurrentAddress());
            }

            if (individual.getECorrespondence() != null) {
                cbxPlaceSendingCorrespondence.setSelectedEntity(individual.getECorrespondence());
            }

            txtChiefName.setValue(getDefaultString(individual.getChiefVillageName()));
            txtChiefNumber.setValue(getDefaultString(individual.getChiefVillagePhoneNumber()));
            txtDeputyChiefName.setValue(getDefaultString(individual.getDeputyChiefVillageName()));
            txtDeputyChiefNumber.setValue(getDefaultString(individual.getDeputyChiefVillagePhoneNumber()));

            txtTotalMonthlyInstallment.setValue(AmountUtils.format(individual.getTotalDebtInstallment()));

            txtIncome1.setValue(AmountUtils.format(individual.getMonthlyIncomeMember1()));
            txtIncome2.setValue(AmountUtils.format(individual.getMonthlyIncomeMember2()));
            txtIncome3.setValue(AmountUtils.format(individual.getMonthlyIncomeMember3()));
            txtIncome4.setValue(AmountUtils.format(individual.getMonthlyIncomeMember4()));
            txtIncome5.setValue(AmountUtils.format(individual.getMonthlyIncomeMember5()));
            txtIncome6.setValue(AmountUtils.format(individual.getMonthlyIncomeMember6()));
            txtContract1.setValue(getDefaultString(individual.getContactMember1()));
            txtContract2.setValue(getDefaultString(individual.getContactMember2()));
            txtContract3.setValue(getDefaultString(individual.getContactMember3()));
            txtContract4.setValue(getDefaultString(individual.getContactMember4()));
            txtContract5.setValue(getDefaultString(individual.getContactMember5()));
            txtContract6.setValue(getDefaultString(individual.getContactMember6()));
            cbxMember1.setSelectedEntity(individual.getRelationship1());
            cbxMember2.setSelectedEntity(individual.getRelationship2());
            cbxMember3.setSelectedEntity(individual.getRelationship3());
            cbxMember4.setSelectedEntity(individual.getRelationship4());
            cbxMember5.setSelectedEntity(individual.getRelationship5());
            cbxMember6.setSelectedEntity(individual.getRelationship6());
            txtTotalFamilyMembers.setValue(getDefaultString(individual.getTotalFamilyMember()));
            txtTotalIncome.setValue(getDefaultString(individual.getTotalAllFamilyMemberIncome()));
        }
    }

    public void reset() {
        txtMonthlyPersonalExpenses.setValue("");
        txtMonthlyFamilyExpenses.setValue("");
        txtTotalExpenses.setValue("");
        txtConvenientTimeforVisit.setValue("");
        optDebtOtherSource.setValue("");
        txtChiefName.setValue("");
        txtChiefNumber.setValue("");
        txtDeputyChiefName.setValue("");
        txtDeputyChiefNumber.setValue("");
        cbxCertifyIncome.setSelectedEntity(null);
        cbxCertifyCurrentAddress.setSelectedEntity(null);
        cbxPlaceSendingCorrespondence.setSelectedEntity(null);
        txtIncome1.setValue("");
        txtIncome2.setValue("");
        txtIncome3.setValue("");
        txtIncome4.setValue("");
        txtIncome5.setValue("");
        txtIncome6.setValue("");
        txtContract1.setValue("");
        txtContract2.setValue("");
        txtContract3.setValue("");
        txtContract4.setValue("");
        txtContract5.setValue("");
        txtContract6.setValue("");
        cbxMember1.setSelectedEntity(null);
        cbxMember2.setSelectedEntity(null);
        cbxMember3.setSelectedEntity(null);
        cbxMember4.setSelectedEntity(null);
        cbxMember5.setSelectedEntity(null);
        cbxMember6.setSelectedEntity(null);
        txtTotalFamilyMembers.setValue("");
        txtTotalIncome.setValue("");

    }

    public List<String> isValid() {
        super.reset();
        //checkMandatoryField(txtMonthlyPersonalExpenses, "monthly.personal.expenses");
        if (MyNumberUtils.getDouble(txtMonthlyPersonalExpenses.getValue(),0) == 0){
            errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("Monthly personal expenses must be more than 0"));
        }
        checkMandatoryField(txtConvenientTimeforVisit, "convenient.time.for.visit");
        checkMandatorySelectField(cbxPlaceSendingCorrespondence, "place.sending.correspondence");
        checkMandatoryField(txtMonthlyFamilyExpenses, "monthly.family.expenses");
        /*if (MyNumberUtils.getDouble(txtMonthlyFamilyExpenses.getValue(),0) == 0){
            errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("Monthly family expenses must be more than 0"));
        }*/
        if(optDebtOtherSource.getValue() != null && Integer.parseInt(optDebtOtherSource.getValue().toString()) == 1){
            checkMandatoryField(txtTotalMonthlyInstallment, "total.debt.monthly.installment");
            if (MyNumberUtils.getDouble(txtTotalMonthlyInstallment.getValue(),0) == 0){
                errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("Total monthly installment expenses must be more than 0"));
            }
        }
        checkMandatoryField(txtTotalExpenses, "total.expenses");
        checkMandatoryField(txtTotalFamilyMembers, "total.family.member");
       /* if(!isValidOnlyNumber(txtChiefNumber.getValue()))
            errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("chief telephone contain character"));
        if(!isValidOnlyNumber(txtDeputyChiefNumber.getValue()))
            errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("chief telephone contain character"));
          *//*
        Check Khmer Phone Number
         *//*
        if (!isValidPhoneNumber(txtChiefNumber.getValue())) {
            errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("chief.telephone.is.invalid!"));
        }
        if (!isValidPhoneNumber(txtDeputyChiefNumber.getValue())) {
            errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("chief.telephone.is.invalid!"));
        }*/

        return errors;
    }
    /**
     * Get total monthly expenses
     *
     * @return
     */
    private double getTotalMonthlyExpenses() {
        double monthlyPersonalExpenses = getDouble(txtMonthlyPersonalExpenses, 0.0);
        double monthlyFamilyExpenses = getDouble(txtMonthlyFamilyExpenses, 0.0);
        return monthlyPersonalExpenses + monthlyFamilyExpenses;
    }

    private double getTotalFamilyIncome() {
        double income1 = getDouble(txtIncome1, 0.0);
        double income2 = getDouble(txtIncome2, 0.0);
        double income3 = getDouble(txtIncome3, 0.0);
        double income4 = getDouble(txtIncome4, 0.0);
        double income5 = getDouble(txtIncome5, 0.0);
        double income6 = getDouble(txtIncome6, 0.0);
        return income6 + income1 + income2 + income3 + income4 + income5;
    }

}
