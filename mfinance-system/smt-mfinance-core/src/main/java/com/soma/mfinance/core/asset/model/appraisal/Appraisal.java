package com.soma.mfinance.core.asset.model.appraisal;

import org.seuksa.frmk.model.EntityFactory;

import javax.persistence.*;

/**
 * Created by Kimsuor SEANG
 * Date  : 6/12/2017
 * Name  : 1:20 PM
 * Email : k.seang@gl-f.com
 */
@Entity
@Table(name = "td_appraisal")
public class Appraisal extends AbstractAppraisal {

    private Double value;
    private Double adjustmentValue;
    private Integer condition;
    private String remarkCO;
    private String remarkAu;
    private AssetAppraisal assetAppraisal;
    private Long itemId;
    private Double coefficient;
    private String freeFieldValue;
    private Double wieght;



    public static Appraisal createInstance() {
        Appraisal instance = EntityFactory.createInstance(Appraisal.class);
        return instance;
    }

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "apr_id", unique = true, nullable = false)
    public Long getId() {
        return this.id;
    }

    @Column(name = "apr_value", nullable = true)
    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Column(name = "apr_adjustment_value", nullable = true)
    public Double getAdjustmentValue() {
        return adjustmentValue;
    }

    public void setAdjustmentValue(Double adjustmentValue) {
        this.adjustmentValue = adjustmentValue;
    }

    @Column(name = "apr_remark_co", nullable = true, length = 1000)
    public String getRemarkCO() {
        return remarkCO;
    }

    public void setRemarkCO(String remarkCO) {
        this.remarkCO = remarkCO;
    }

    @Column(name = "apr_remark_au", nullable = true, length = 1000)
    public String getRemarkAu() {
        return remarkAu;
    }

    public void setRemarkAu(String remarkAu) {
        this.remarkAu = remarkAu;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ass_app_id", nullable = false)
    public AssetAppraisal getAssetAppraisal() {
        return assetAppraisal;
    }

    public void setAssetAppraisal(AssetAppraisal assetAppraisal) {
        this.assetAppraisal = assetAppraisal;
    }

    @Column(name = "apr_condition", nullable = true)
    public Integer getCondition() {
        return condition;
    }

    public void setCondition(Integer condition) {
        this.condition = condition;
    }

    @Column(name = "apr_itm_id", nullable = true)
    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    @Column(name = "apr_coefficient", nullable = true)
    public Double getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(Double coefficient) {
        this.coefficient = coefficient;
    }

    @Column(name = "apr_free_field_val", nullable = true, length = 30)
    public String getFreeFieldValue() {
        return freeFieldValue;
    }

    public void setFreeFieldValue(String freeFieldValue) {
        this.freeFieldValue = freeFieldValue;
    }

    @Column(name = "apr_wieght", nullable = true, length = 30)
    public Double getWieght() {
        return wieght;
    }

    public void setWieght(Double wieght) {
        this.wieght = wieght;
    }


}
