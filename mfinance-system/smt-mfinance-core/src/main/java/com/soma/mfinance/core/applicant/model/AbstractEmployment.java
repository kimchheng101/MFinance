package com.soma.mfinance.core.applicant.model;

import com.soma.mfinance.core.Enum.EEmployeeOccupation;
import com.soma.mfinance.core.application.model.EmploymentOccupation;
import com.soma.mfinance.core.application.model.EmploymentPosition;
import com.soma.ersys.core.hr.model.address.BaseAddress;
import com.soma.ersys.core.hr.model.eref.*;
import org.apache.commons.lang.StringUtils;
import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;
import java.util.Date;

/**
 * 
 * @author kimsuor.seang
 * @modify kimsuor.seang on 20/10/2018
 */
@MappedSuperclass
public abstract class AbstractEmployment extends EntityA {

	private static final long serialVersionUID = 4288524795479334115L;

	private String position;
	private EJobPosition position1;
	private EJobPosition position2;
	private EJobPosition position3;
	private String licenceNo;
	private String employerName;
	private Date since;
	private Integer timeWithEmployerInYear;
	private Integer timeWithEmployerInMonth;
	private Double revenue;
	private Double revenue2;
	private Double allowance;
	private Double businessExpense;
	private Double businessExpense2;
	private Double businessExpense3;
	private Double businessIncomes;
	private Integer noMonthInYear;
	private Integer noMonthInYear2;
	private EEmploymentStatus employmentStatus;
	private EEmploymentStatus employmentStatus2;
	private EEmploymentStatus employmentStatus3;
	private EEmploymentIndustry employmentIndustry;
	private EEmploymentIndustry employmentIndustry2;
	private EEmploymentIndustry employmentIndustry3;
	private EEmploymentIndustryCategory employmentIndustryCategory;
	private EEmploymentType employmentType;
	private EEmploymentCategory employmentCategory;
	private ELegalForm legalForm;
	private Boolean allowCallToWorkPlace;
	private Boolean sameApplicantAddress;
	private String workPhone;
	private String managerPhone;
	private String managerName;
	private ECompanySize companySize;
	private ESeniorityLevel seniorityLevel;
	private String empRemarks;
	private String departmentPhone;
	private EEmployeeOccupation eEmployeeOccupation2;
	private EEmployeeOccupation eEmployeeOccupation3;
	private EmploymentOccupation employmentOccupation;
	private EmploymentPosition employmentPosition;

	private Boolean jan;
	private Boolean feb;
	private Boolean mar;
	private Boolean apr;
	private Boolean may;
	private Boolean jun;
	private Boolean jul;
	private Boolean aug;
	private Boolean sep;
	private Boolean oct;
	private Boolean nov;
	private Boolean dec;

	@Column(name = "emp_va_position", nullable = true, length = 100)
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}

	@Column(name = "emp_va_position_id", nullable = true)
	@Convert(converter = EJobPosition.class)
	public EJobPosition getPosition1() {
		return position1;
	}

	public void setPosition1(EJobPosition position1) {
		this.position1 = position1;
	}

	@Column(name = "emp_va_position2_id", nullable = true)
	@Convert(converter = EJobPosition.class)
	public EJobPosition getPosition2() {
		return position2;
	}
	public void setPosition2(EJobPosition position2) {
		this.position2 = position2;
	}

	@Column(name = "emp_va_position3_id", nullable = true)
	@Convert(converter = EJobPosition.class)
	public EJobPosition getPosition3() {
		return position3;
	}
	public void setPosition3(EJobPosition position3) {
		this.position3 = position3;
	}

	@Column(name = "emp_va_licence_no", nullable = true, length = 50)
	public String getLicenceNo() {
		return licenceNo;
	}
	public void setLicenceNo(String licenceNo) {
		this.licenceNo = licenceNo;
	}

	@Column(name = "emp_va_employer_name", nullable = true, length = 100)
	public String getEmployerName() {
		return employerName;
	}
	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}

	@Column(name = "emp_dt_since", nullable = true)
	public Date getSince() {
		return since;
	}
	public void setSince(Date since) {
		this.since = since;
	}

	@Column(name = "emp_nu_time_with_employer_year", nullable = true)
	public Integer getTimeWithEmployerInYear() {
		return timeWithEmployerInYear;
	}
	public void setTimeWithEmployerInYear(Integer timeWithEmployerInYear) {
		this.timeWithEmployerInYear = timeWithEmployerInYear;
	}

	@Column(name = "emp_nu_time_with_employer_month", nullable = true)
	public Integer getTimeWithEmployerInMonth() {
		return timeWithEmployerInMonth;
	}
	public void setTimeWithEmployerInMonth(Integer timeWithEmployerInMonth) {
		this.timeWithEmployerInMonth = timeWithEmployerInMonth;
	}

	@Column(name = "emp_am_revenu", nullable = true)
	public Double getRevenue() {
		return revenue;
	}
	public void setRevenue(Double revenue) {
		this.revenue = revenue;
	}

	@Column(name = "emp_am_revenu2", nullable = true)
	public Double getRevenue2() {
		return revenue2;
	}
	public void setRevenue2(Double revenue2) {
		this.revenue2 = revenue2;
	}

	@Column(name = "emp_am_allowance", nullable = true)
	public Double getAllowance() {
		return allowance;
	}
	public void setAllowance(Double allowance) {
		this.allowance = allowance;
	}

	@Column(name = "emp_am_business_expense", nullable = true)
	public Double getBusinessExpense() {
		return businessExpense;
	}
	public void setBusinessExpense(Double businessExpense) {
		this.businessExpense = businessExpense;
	}

	@Column(name = "emp_am_business_expense2", nullable = true)
	public Double getBusinessExpense2() {
		return businessExpense2;
	}
	public void setBusinessExpense2(Double businessExpense2) {
		this.businessExpense2 = businessExpense2;
	}

	@Column(name = "emp_am_business_expense3", nullable = true)
	public Double getBusinessExpense3() {
		return businessExpense3;
	}
	public void setBusinessExpense3(Double businessExpense3) {
		this.businessExpense3 = businessExpense3;
	}

	@Column(name = "emp_nu_no_month_in_year", nullable = false)
	public Integer getNoMonthInYear() {
		return noMonthInYear;
	}
	public void setNoMonthInYear(Integer noMonthInYear) {
		this.noMonthInYear = noMonthInYear;
	}

	@Column(name = "emp_nu_no_month_in_year2", nullable = true)
	public Integer getNoMonthInYear2() {
		return noMonthInYear2;
	}
	public void setNoMonthInYear2(Integer noMonthInYear2) {
		this.noMonthInYear2 = noMonthInYear2;
	}

    @Column(name = "emp_sta_id", nullable = true)
    @Convert(converter = EEmploymentStatus.class)
	public EEmploymentStatus getEmploymentStatus() {
		return employmentStatus;
	}
	public void setEmploymentStatus(EEmploymentStatus employmentStatus) {
		this.employmentStatus = employmentStatus;
	}

	@Column(name = "emp_sta2_id", nullable = true)
	@Convert(converter = EEmploymentStatus.class)
	public EEmploymentStatus getEmploymentStatus2() {
		return employmentStatus2;
	}
	public void setEmploymentStatus2(EEmploymentStatus employmentStatus2) {
		this.employmentStatus2 = employmentStatus2;
	}

	@Column(name = "emp_sta3_id", nullable = true)
	@Convert(converter = EEmploymentStatus.class)
	public EEmploymentStatus getEmploymentStatus3() {
		return employmentStatus3;
	}
	public void setEmploymentStatus3(EEmploymentStatus employmentStatus3) {
		this.employmentStatus3 = employmentStatus3;
	}

    @Column(name = "emp_ind_id", nullable = true)
    @Convert(converter = EEmploymentIndustry.class)
	public EEmploymentIndustry getEmploymentIndustry() {
		return employmentIndustry;
	}
	public void setEmploymentIndustry(EEmploymentIndustry employmentIndustry) {
		this.employmentIndustry = employmentIndustry;
	}

	@Column(name = "emp_ind2_id", nullable = true)
	@Convert(converter = EEmploymentIndustry.class)
	public EEmploymentIndustry getEmploymentIndustry2() {
		return employmentIndustry2;
	}
	public void setEmploymentIndustry2(EEmploymentIndustry employmentIndustry2) {
		this.employmentIndustry2 = employmentIndustry2;
	}

	@Column(name = "emp_ind3_id", nullable = true)
	@Convert(converter = EEmploymentIndustry.class)
	public EEmploymentIndustry getEmploymentIndustry3() {
		return employmentIndustry3;
	}
	public void setEmploymentIndustry3(EEmploymentIndustry employmentIndustry3) {
		this.employmentIndustry3 = employmentIndustry3;
	}

	@Column(name = "emp_ind_cat_id", nullable = true)
    @Convert(converter = EEmploymentIndustryCategory.class)
	public EEmploymentIndustryCategory getEmploymentIndustryCategory() {
		return employmentIndustryCategory;
	}
	public void setEmploymentIndustryCategory(EEmploymentIndustryCategory employmentIndustryCategory) {
		this.employmentIndustryCategory = employmentIndustryCategory;
	}

    @Column(name = "emp_typ_id", nullable = true)
    @Convert(converter = EEmploymentType.class)
	public EEmploymentType getEmploymentType() {
		return employmentType;
	}
	public void setEmploymentType(EEmploymentType employmentType) {
		this.employmentType = employmentType;
	}	

	@Column(name = "emp_cat_id", nullable = true)
    @Convert(converter = EEmploymentCategory.class)
	public EEmploymentCategory getEmploymentCategory() {
		return employmentCategory;
	}
	public void setEmploymentCategory(EEmploymentCategory employmentCategory) {
		this.employmentCategory = employmentCategory;
	}

	@Column(name = "leg_for_id", nullable = true)
    @Convert(converter = ELegalForm.class)
	public ELegalForm getLegalForm() {
		return legalForm;
	}
	public void setLegalForm(ELegalForm legalForm) {
		this.legalForm = legalForm;
	}

	@Column(name = "com_siz_id", nullable = true)
    @Convert(converter = ECompanySize.class)
	public ECompanySize getCompanySize() {
		return companySize;
	}
	public void setCompanySize(ECompanySize companySize) {
		this.companySize = companySize;
	}

	@Column(name = "emp_bl_allow_call_work_place", nullable = true)
	public Boolean isAllowCallToWorkPlace() {
		return allowCallToWorkPlace;
	}
	public void setAllowCallToWorkPlace(Boolean allowCallToWorkPlace) {
		this.allowCallToWorkPlace = allowCallToWorkPlace;
	}

	@Column(name = "emp_bl_same_applicant_address", nullable = true)
	public Boolean isSameApplicantAddress() {
		return sameApplicantAddress;
	}
	public void setSameApplicantAddress(Boolean sameApplicantAddress) {
		this.sameApplicantAddress = sameApplicantAddress;
	}

	@Column(name = "emp_va_work_phone", nullable = true, length = 100)
	public String getWorkPhone() {
		return workPhone;
	}
	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}	

	@Column(name = "emp_va_manager_phone", nullable = true, length = 30)
	public String getManagerPhone() {
		return managerPhone;
	}
	public void setManagerPhone(String managerPhone) {
		this.managerPhone = managerPhone;
	}

	@Column(name = "emp_va_manager_name", nullable = true, length = 100)
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

    @Column(name = "sen_lev_id", nullable = true)
    @Convert(converter = ESeniorityLevel.class)
	public ESeniorityLevel getSeniorityLevel() {
		return seniorityLevel;
	}
	public void setSeniorityLevel(ESeniorityLevel seniorityLevel) {
		this.seniorityLevel = seniorityLevel;
	}

	@Column(name = "emp_remarks", nullable = true, length = 1000)
	public String getEmpRemarks() {
		return empRemarks;
	}
	public void setEmpRemarks(String empRemarks) {
		this.empRemarks = empRemarks;
	}
	
	@Transient
	public abstract BaseAddress getBaseAddress();
	
	@Transient
	public Boolean isPersistent() {
		return getId() != null
//				|| StringUtils.isNotEmpty(position)
				|| employmentStatus != null
				|| employmentIndustry != null
				|| StringUtils.isNotEmpty(employerName)
				|| (revenue != null && revenue > 0)
				|| (businessExpense != null && businessExpense > 0) 
				|| (noMonthInYear != null && noMonthInYear > 0)
				|| (getBaseAddress() != null && getBaseAddress().isPersistent());
	}

	@Column(name = "emp_va_department_phone", nullable = true, length = 30)
	public String getDepartmentPhone() {
		return departmentPhone;
	}
	public void setDepartmentPhone(String departmentPhone) {
		this.departmentPhone = departmentPhone;
	}

	@Column(name = "emp_occ2_id", nullable = true)
	@Convert(converter = EEmployeeOccupation.class)
	public EEmployeeOccupation getEmploymentOccupation2() {
		return eEmployeeOccupation2;
	}
	public void setEmploymentOccupation2(EEmployeeOccupation eEmployeeOccupation2) {
		this.eEmployeeOccupation2 = eEmployeeOccupation2;
	}

	@Column(name = "emp_occ3_id", nullable = true)
	@Convert(converter = EEmployeeOccupation.class)
	public EEmployeeOccupation getEmploymentOccupation3() {
		return eEmployeeOccupation3;
	}
	public void setEmploymentOccupation3(EEmployeeOccupation eEmployeeOccupation3) {
		this.eEmployeeOccupation3 = eEmployeeOccupation3;
	}

	@Column(name = "emp_am_business_incomes", nullable = true)
	public Double getBusinessIncomes() {
		return businessIncomes;
	}
	public void setBusinessIncomes(Double businessIncomes) {
		this.businessIncomes = businessIncomes;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "emp_occ_id", referencedColumnName = "emp_occ_id")
	public EmploymentOccupation getEmploymentOccupation() {
		return employmentOccupation;
	}
	public void setEmploymentOccupation(EmploymentOccupation employmentOccupation) {
		this.employmentOccupation = employmentOccupation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "emp_pos_id", referencedColumnName = "emp_pos_id")
	public EmploymentPosition getEmploymentPosition() {
		return employmentPosition;
	}
	public void setEmploymentPosition(EmploymentPosition employmentPosition) {
		this.employmentPosition = employmentPosition;
	}

	@Column(name = "emplo_bl_jan", nullable = true)
	public Boolean isJan() {
		return jan;
	}
	public void setJan(Boolean jan) {
		this.jan = jan;
	}

	@Column(name = "emplo_bl_feb", nullable = true)
	public Boolean isFeb() {
		return feb;
	}
	public void setFeb(Boolean feb) {
		this.feb = feb;
	}

	@Column(name = "emplo_bl_mar", nullable = true)
	public Boolean isMar() {
		return mar;
	}
	public void setMar(Boolean mar) {
		this.mar = mar;
	}

	@Column(name = "emplo_bl_apr", nullable = true)
	public Boolean isApr() {
		return apr;
	}
	public void setApr(Boolean apr) {
		this.apr = apr;
	}

	@Column(name = "emplo_bl_may", nullable = true)
	public Boolean isMay() {
		return may;
	}
	public void setMay(Boolean may) {
		this.may = may;
	}

	@Column(name = "emplo_bl_jun", nullable = true)
	public Boolean isJun() {
		return jun;
	}
	public void setJun(Boolean jun) {
		this.jun = jun;
	}

	@Column(name = "emplo_bl_jul", nullable = true)
	public Boolean isJul() {
		return jul;
	}
	public void setJul(Boolean jul) {
		this.jul = jul;
	}

	@Column(name = "emplo_bl_aug", nullable = true)
	public Boolean isAug() {
		return aug;
	}
	public void setAug(Boolean aug) {
		this.aug = aug;
	}

	@Column(name = "emplo_bl_sep", nullable = true)
	public Boolean isSep() {
		return sep;
	}
	public void setSep(Boolean sep) {
		this.sep = sep;
	}

	@Column(name = "emplo_bl_oct", nullable = true)
	public Boolean isOct() {
		return oct;
	}
	public void setOct(Boolean oct) {
		this.oct = oct;
	}

	@Column(name = "emplo_bl_nov", nullable = true)
	public Boolean isNov() {
		return nov;
	}
	public void setNov(Boolean nov) {
		this.nov = nov;
	}

	@Column(name = "emplo_bl_dec", nullable = true)
	public Boolean isDec() {
		return dec;
	}
	public void setDec(Boolean dec) {
		this.dec = dec;
	}
}
