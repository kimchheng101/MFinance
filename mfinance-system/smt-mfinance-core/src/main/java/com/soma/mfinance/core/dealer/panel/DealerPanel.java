package com.soma.mfinance.core.dealer.panel;

import com.soma.mfinance.core.common.security.model.SecUserDetail;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.quotation.model.EPlaceInstallment;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.dealer.DealerEntityField;
import com.soma.mfinance.core.shared.quotation.QuotationProfileUtils;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.shared.system.FMProfile;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.mfinance.core.widget.SecUserComboBox;
import com.soma.ersys.core.hr.model.eref.EMediaPromoting;
import com.soma.frmk.helper.FrmkServicesHelper;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.*;
import com.vaadin.ui.Notification.Type;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.service.EntityService;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Dealer panel
 * @author kimsuor.seang
 */
public class DealerPanel extends AbstractTabPanel implements DealerEntityField, FrmkServicesHelper {
	
	private static final long serialVersionUID = 6849476207051876799L;
		
	private ERefDataComboBox<EDealerType> cbxDealerType;
	private DealerComboBox cbxDealer;
	private DateField dfQuotationDate;
	private SecUserComboBox cbxCreditOfficer;
	private SecUserComboBox cbxProductionOfficer;
	private ERefDataComboBox<EPlaceInstallment> cbxPlaceInstallment;
	private ERefDataComboBox<EMediaPromoting> cbxWayOfKnowing;	
	private ERefDataComboBox<EMediaPromoting> cbxWayOfKnowing1;
	private ERefDataComboBox<EMediaPromoting> cbxWayOfKnowing2;
	private HorizontalLayout contentLayout;
	private ValueChangeListener valueChangeListener;

	public DealerPanel() {
		super();
		setSizeFull();
	}

	@Override
	protected Component createForm() {	
		BaseRestrictions<Dealer> res = new BaseRestrictions<Dealer>(Dealer.class);
		res.getStatusRecordList().add(EStatusRecord.ACTIV);
		cbxDealer = new DealerComboBox(ENTITY_SRV.list(res));
		cbxDealer.setWidth("220px");
		cbxDealer.setImmediate(true);
		cbxDealer.setSelectedEntity(null);
		cbxDealer.setEnabled(false);
		List<EDealerType> dealerTypes = EDealerType.values();
		cbxDealerType = new ERefDataComboBox<EDealerType>(dealerTypes);
		cbxDealerType.setImmediate(true);
		cbxDealerType.setWidth("220px");
		cbxDealerType.setEnabled(false);
		valueChangeListener = new ValueChangeListener() {
			/** */
			private static final long serialVersionUID = -6789482796826178900L;
			@Override
			public void valueChange(ValueChangeEvent event) {
				BaseRestrictions<Dealer> restrictions = new BaseRestrictions<Dealer>(Dealer.class);
				restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
				//restrictions.addCriterion(Restrictions.eq("dealerType", DealerType.OTH));
				if (cbxDealerType.getSelectedEntity() != null) {
					restrictions.addCriterion(Restrictions.eq("dealerType", cbxDealerType.getSelectedEntity()));
				}
				cbxDealer.setDealers(ENTITY_SRV.list(restrictions));
				cbxDealer.setSelectedEntity(null);
			}
		};
		cbxDealerType.addValueChangeListener(valueChangeListener);
		
		SecUser secUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		EntityService entityService = SpringUtils.getBean(EntityService.class);
		
		List<SecUser> usersByProfileCO = new ArrayList<SecUser>();
		List<SecUser> usersByProfilePO = new ArrayList<SecUser>();
		 if (ProfileUtil.isPOS()) {
			SecUserDetail secUserDetail = entityService.getByField(SecUserDetail.class, "secUser.id", secUser.getId());
			if (secUserDetail != null && secUserDetail.getDealer() != null) {
				BaseRestrictions<SecUserDetail> restrictions = new BaseRestrictions<>(SecUserDetail.class);			
				restrictions.addCriterion(Restrictions.eq("dealer.id", secUserDetail.getDealer().getId()));
				List<SecUserDetail> secUserDetails = entityService.list(restrictions);					
				if (secUserDetails != null) {
					for (SecUserDetail userDetail : secUserDetails) {
						if (ProfileUtil.isProfileExist(FMProfile.CO, userDetail.getSecUser().getProfiles())) {
							usersByProfileCO.add(userDetail.getSecUser());
						}
						if (ProfileUtil.isProfileExist(FMProfile.PO, userDetail.getSecUser().getProfiles())) {
							usersByProfilePO.add(userDetail.getSecUser());
						}
					}
				}
			}
		}
		
		if (ProfileUtil.isProfileExist(FMProfile.CO, secUser.getProfiles())) {
			addUser(usersByProfileCO, secUser);
		}
		
		if (ProfileUtil.isProfileExist(FMProfile.PO, secUser.getProfiles())) {
			addUser(usersByProfilePO, secUser);
		}
				
		if (usersByProfileCO.isEmpty()) {
			usersByProfileCO = DataReference.getInstance().getUsers(FMProfile.CO);
		}
		if (usersByProfilePO.isEmpty()) {
			usersByProfilePO = DataReference.getInstance().getUsers(FMProfile.PO);
		}
		
		cbxCreditOfficer = new SecUserComboBox(usersByProfileCO);
		cbxProductionOfficer = new SecUserComboBox(usersByProfilePO);
		cbxProductionOfficer.setRequired(false);
		
		dfQuotationDate = ComponentFactory.getAutoDateField();
		dfQuotationDate.setWidth(95,Unit.PIXELS);
		cbxPlaceInstallment = new ERefDataComboBox<EPlaceInstallment>(EPlaceInstallment.class);
		cbxWayOfKnowing = new ERefDataComboBox<EMediaPromoting>(EMediaPromoting.class);
		cbxWayOfKnowing1 = new ERefDataComboBox<EMediaPromoting>(EMediaPromoting.class);
		cbxWayOfKnowing2 = new ERefDataComboBox<EMediaPromoting>(EMediaPromoting.class);
		String template = "quotationDealer";
		InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
		CustomLayout customLayout = null;
		try {
			customLayout = new CustomLayout(layoutFile);
		} catch (IOException e) {
			Notification.show("Could not locate template " + template, e.getMessage(), Type.ERROR_MESSAGE);
		}
		
		
		customLayout.addComponent(new Label(I18N.message("dealer.type")), "lblDealerType");
		customLayout.addComponent(cbxDealerType, "cbxDealerType");
		customLayout.addComponent(new Label(I18N.message("dealer")), "lblDealer");
		customLayout.addComponent(cbxDealer, "cbxDealer");
		customLayout.addComponent(new Label(I18N.message("quotation.date")), "lblQuotationDate");
		customLayout.addComponent(dfQuotationDate, "dfQuotationDate");
		customLayout.addComponent(new Label(I18N.message("credit.officer")), "lblCreditOfficer");
		customLayout.addComponent(cbxCreditOfficer, "cbxCreditOfficer");
		customLayout.addComponent(new Label(I18N.message("production.officer")), "lblProductionOfficer");
		customLayout.addComponent(cbxProductionOfficer, "cbxProductionOfficer");
		customLayout.addComponent(new Label(I18N.message("place.installment")), "lblPlaceInstallment");
		customLayout.addComponent(cbxPlaceInstallment, "cbxPlaceInstallment");
		customLayout.addComponent(new Label(I18N.message("way.of.knowing")), "lblWayOfKnowing");
		customLayout.addComponent(cbxWayOfKnowing, "cbxWayOfKnowing");
		customLayout.addComponent(new Label(I18N.message("way.of.knowing1")), "lblWayOfKnowing1");
		customLayout.addComponent(cbxWayOfKnowing1, "cbxWayOfKnowing1");
		customLayout.addComponent(new Label(I18N.message("way.of.knowing2")), "lblWayOfKnowing2");
		customLayout.addComponent(cbxWayOfKnowing2, "cbxWayOfKnowing2");

		if (ProfileUtil.isAdmin()){
			cbxDealer.setEnabled(true);
			cbxDealerType.setEnabled(true);
		}

		contentLayout = new HorizontalLayout();
		contentLayout.setMargin(true);
		contentLayout.addComponent(customLayout);
		return contentLayout;
	}
	
	/**
	 * @param users
	 * @param user
	 */
	private void addUser(List<SecUser> users, SecUser user) {
		boolean found = false;
		for (SecUser secUser : users) {
			if (secUser.getId().equals(user.getId())) {
				found = true;
				break;
			}
		}
		if (!found) {
			users.add(user);
		}
	}
	
	/**
	 * Assign value to quotation 
	 * @param quotation
	 */
	public void assignValues(Quotation quotation) {
		cbxDealer.setSelectedEntity(quotation.getDealer());
		cbxDealerType.removeValueChangeListener(valueChangeListener);
		cbxDealerType.setSelectedEntity(cbxDealer.getSelectedEntity() != null ? cbxDealer.getSelectedEntity().getDealerType() : null);
		cbxDealerType.addValueChangeListener(valueChangeListener);
		if (quotation.getQuotationDate() == null) {
			dfQuotationDate.setValue(DateUtils.today());
		} else {
			dfQuotationDate.setValue(quotation.getQuotationDate());
		}
		
		cbxCreditOfficer.addValue(quotation.getCreditOfficer());
		cbxProductionOfficer.addValue(quotation.getProductionOfficer());
		
		cbxCreditOfficer.setSelectedEntity(quotation.getCreditOfficer());
		cbxProductionOfficer.setSelectedEntity(quotation.getProductionOfficer());
		cbxPlaceInstallment.setSelectedEntity(quotation.getPlaceInstallment());
		
		cbxWayOfKnowing.setSelectedEntity(quotation.getWayOfKnowing());
		cbxWayOfKnowing1.setSelectedEntity(quotation.getWayOfKnowing1());
		cbxWayOfKnowing2.setSelectedEntity(quotation.getWayOfKnowing2());


		/**
		 * Enable Field On Dealer Tab
		 */
		setEnabledDealer(QuotationProfileUtils.isEnabled(quotation));
	}
	
	/**
	 * Get quotation
	 * @param quotation
	 * @return
	 */
	public Quotation getQuotation(Quotation quotation) {
		quotation.setDealer(cbxDealer.getSelectedEntity());
		quotation.setQuotationDate(dfQuotationDate.getValue());
		quotation.setCreditOfficer(cbxCreditOfficer.getSelectedEntity());
		quotation.setProductionOfficer(cbxProductionOfficer.getSelectedEntity());
		quotation.setPlaceInstallment(cbxPlaceInstallment.getSelectedEntity());
		quotation.setWayOfKnowing(cbxWayOfKnowing.getSelectedEntity());
		quotation.setWayOfKnowing1(cbxWayOfKnowing1.getSelectedEntity());
		quotation.setWayOfKnowing2(cbxWayOfKnowing2.getSelectedEntity());
		// check sub dealer
		if (quotation.getDealer().getDealerType() != EDealerType.HEAD) {
			if (quotation.getAsset() != null) {
				quotation.getAsset().setModelUsed(false);
			}
		}
		return quotation;
	}
	
	/**
	 * @param enabled
	 */
	public void setEnabledDealer(boolean enabled) {
		cbxCreditOfficer.setEnabled(enabled);
		cbxPlaceInstallment.setEnabled(enabled);
		cbxProductionOfficer.setEnabled(enabled);
		cbxWayOfKnowing.setEnabled(enabled);
		cbxWayOfKnowing1.setEnabled(enabled);
		cbxWayOfKnowing2.setEnabled(enabled);
		dfQuotationDate.setEnabled(enabled);
	}
	
	/**
	 * @return
	 */
	public boolean isValid() {
		super.removeErrorsPanel();
		checkMandatorySelectField(cbxDealer, "dealer");
		checkMandatoryDateField(dfQuotationDate, "quotation.date");
		checkMandatorySelectField(cbxCreditOfficer, "credit.officer");
		if (!errors.isEmpty()) {
			super.displayErrorsPanel();
		}
		return errors.isEmpty();
	}
	
	/**
	 * @return
	 */
	public List<String> fullValidate() {		
		super.removeErrorsPanel();
		checkMandatorySelectField(cbxDealer, "dealer");
		checkMandatoryDateField(dfQuotationDate, "quotation.date");
		checkMandatorySelectField(cbxCreditOfficer, "credit.officer");
		checkMandatorySelectField(cbxPlaceInstallment, "place.installment");
		checkMandatorySelectField(cbxWayOfKnowing, "way.of.knowing");
		return errors;
	}

	/**
	 * Reset 
	 */
	public void reset() {
		assignValues(new Quotation());
	}
}
