package com.soma.mfinance.core.financial.service;

import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.asset.model.AssetModel;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.financial.model.AssetMatrixPrice;
import com.soma.mfinance.core.financial.model.FinService;
import com.soma.ersys.core.hr.model.eref.EColor;

/**
 * @author kimsuor.seang
 *
 */
public interface MatrixPricingService  extends BaseEntityService {

	/**
	 * @param assetModel
	 * @param dealer
	 * @param color
	 * @param year
	 * @return
	 */
	AssetMatrixPrice getAssetModelMatrixPrice(AssetModel assetModel, Dealer dealer, EColor color, Integer year);
	
	/**
	 * @param assetModel
	 * @param service
	 * @return
	 */
	AssetMatrixPrice getServiceMatrixPrice(AssetModel assetModel, FinService service);
}
