package com.soma.mfinance.core.asset.dao;

import org.seuksa.frmk.dao.BaseEntityDao;

/**
 * Asset data model access
 * @author kimsuor.seang
 *
 */
public interface AssetDao extends BaseEntityDao {

}
