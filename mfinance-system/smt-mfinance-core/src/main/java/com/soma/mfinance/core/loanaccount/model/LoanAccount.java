package com.soma.mfinance.core.loanaccount.model;

import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;

/**
 * Created by cheasocheat on 5/4/17.
 */
@Entity
@Table(name = "td_loan_account")
public class LoanAccount extends EntityA{
    private String accountId;
    private String accountName;
    private String phoneNumber;
    private String leaseAmount;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(
            name = "loan_id",
            unique = true,
            nullable = false
    )
    public Long getId() {
        return id;
    }

    @Column(
            name = "loan_acc_id",
            unique = false,
            nullable = true
    )
    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    @Column(
            name = "loan_acc_name",
            unique = false,
            nullable = true
    )
    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    @Column(
            name = "loan_acc_phone_num",
            unique = false,
            nullable = true
    )
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    @Column(
            name = "loan_acc_lease_amount",
            unique = false,
            nullable = true
    )
    public String getLeaseAmount() {
        return leaseAmount;
    }

    public void setLeaseAmount(String leaseAmount) {
        this.leaseAmount = leaseAmount;
    }
}
