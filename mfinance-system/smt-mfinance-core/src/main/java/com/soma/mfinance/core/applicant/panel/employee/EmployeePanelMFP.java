package com.soma.mfinance.core.applicant.panel.employee;

/**
 * Created by b.chea on 2/2/2017.
 * Updated by kimsuor.seang on 2/2/2017.
 */

import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.applicant.model.Employment;
import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.mfinance.core.applicant.panel.CurrentEmploymentPanelMFP;
import com.soma.mfinance.core.applicant.panel.SecondaryEmploymentPanelMFP;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.ersys.core.hr.model.eref.EEmploymentType;
import com.soma.frmk.vaadin.ui.panel.AbstractControlPanel;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.CrudAction;

import java.util.List;

public class EmployeePanelMFP extends AbstractControlPanel {
    private static final long serialVersionUID = 8485010240363814186L;

    //private OccupationPanel occupationPanel;
    private CurrentEmploymentPanelMFP currentEmploymentPanelMFP;
    private SecondaryEmploymentPanelMFP secondaryEmploymentPanel1;
    private SecondaryEmploymentPanelMFP secondaryEmploymentPanel2;

    private Quotation quotation;


    public EmployeePanelMFP() {
        setSizeFull();
        setMargin(true);

        //occupationPanel = new OccupationPanel();

/*
        final Panel occupationLayout = new Panel(I18N.message("occupation.info"));
        occupationLayout.setContent(occupationPanel); */

        init();
    }

    public void init() {
        currentEmploymentPanelMFP = new CurrentEmploymentPanelMFP();
        secondaryEmploymentPanel1 = new SecondaryEmploymentPanelMFP();
        secondaryEmploymentPanel2 = new SecondaryEmploymentPanelMFP();

        final Panel currentEmploymentLayout = new Panel(I18N.message("current.employment"));
        currentEmploymentLayout.setContent(currentEmploymentPanelMFP);

        final HorizontalLayout secondaryEmploymentLayout = new HorizontalLayout();
        secondaryEmploymentLayout.setSpacing(true);

        final HorizontalLayout secondaryEmploymentLayout2 = new HorizontalLayout();
        secondaryEmploymentLayout.setSpacing(true);

        final Panel secondaryEmploymentLayout1 = new Panel(I18N.message("secondary.employment") + " 1");
        secondaryEmploymentLayout1.setContent(secondaryEmploymentPanel1);

        final Panel secondaryEmploymentPanelLayout2 = new Panel(I18N.message("secondary.employment") + " 2");
        secondaryEmploymentPanelLayout2.setContent(secondaryEmploymentPanel2);


        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        verticalLayout.setSpacing(true);
        //verticalLayout.addComponent(occupationLayout);
        verticalLayout.addComponent(currentEmploymentLayout);
        secondaryEmploymentLayout.addComponent(secondaryEmploymentLayout1);
        secondaryEmploymentLayout.addComponent(secondaryEmploymentPanelLayout2);
        verticalLayout.addComponent(secondaryEmploymentLayout);
        addComponent(verticalLayout);
    }

    public void assignValues(Individual individual) {
        super.reset();
        this.removeAllComponents();
        this.init();

        Employment currentEmployment = individual.getCurrentEmployment();
        currentEmploymentPanelMFP.setApplicant(individual, EApplicantType.C);
        if (currentEmployment != null) {
            if (quotation != null) {
                currentEmploymentPanelMFP.setCurrentEmploymentQuotation(quotation);
            }
            currentEmploymentPanelMFP.assignValues(currentEmployment);
        } else {
            currentEmploymentPanelMFP.reset();
        }

        List<Employment> secondaryEmployments = individual.getEmployments(EEmploymentType.CURR.SECO);
        if (secondaryEmployments != null && !secondaryEmployments.isEmpty()) {
            secondaryEmploymentPanel1.assignValues(secondaryEmployments.get(0));
            if (secondaryEmployments.size() > 1) {
                secondaryEmploymentPanel2.assignValues(secondaryEmployments.get(1));
            } else if (secondaryEmployments.size() > 2) {
                secondaryEmploymentPanel2.assignValues(secondaryEmployments.get(2));
            } else if (secondaryEmployments.size() > 3) {
                secondaryEmploymentPanel2.assignValues(secondaryEmployments.get(3));
            }

        } else {
            secondaryEmploymentPanel1.reset();
            secondaryEmploymentPanel2.reset();
        }
    }


    public Individual getEmployments(Individual individual) {
        Employment currentEmployment = individual.getCurrentEmployment();
        if (currentEmployment != null) {
            currentEmploymentPanelMFP.getEmployment(currentEmployment);
        } else {
            currentEmployment = currentEmploymentPanelMFP.getEmployment(new Employment());
            if (currentEmployment.isPersistent()) {
                individual.addEmployment(currentEmployment);
            }
        }
        List<Employment> secondaryEmployments = individual.getEmployments(EEmploymentType.SECO);
        if (secondaryEmployments != null && !secondaryEmployments.isEmpty()) {
            Employment secondaryEmployment1 = secondaryEmploymentPanel1.getEmployment(secondaryEmployments.get(0));
            if (secondaryEmployment1 == null) {
                secondaryEmployments.get(0).setCrudAction(CrudAction.DELETE);
            }
        } else {
            Employment prevEmployment1 = secondaryEmploymentPanel1.getEmployment(new Employment());
            if (prevEmployment1 != null) {
                individual.addEmployment(prevEmployment1);
            }
        }

        if (secondaryEmployments != null && secondaryEmployments.size() > 1) {
            Employment secondaryEmployment2 = secondaryEmploymentPanel2.getEmployment(secondaryEmployments.get(1));
            if (secondaryEmployment2 == null) {
                secondaryEmployments.get(1).setCrudAction(CrudAction.DELETE);
            }
        } else {
            Employment secondaryEmployment2 = secondaryEmploymentPanel2.getEmployment(new Employment());
            if (secondaryEmployment2 != null) {
                individual.addEmployment(secondaryEmployment2);
            }
        }

        return individual;
    }

    public List<String> isValid() {
        super.reset();
        errors.addAll(currentEmploymentPanelMFP.isValid());
        return errors;
    }

    public void reset() {
        currentEmploymentPanelMFP.reset();
        secondaryEmploymentPanel1.reset();
        secondaryEmploymentPanel2.reset();
    }

    public CurrentEmploymentPanelMFP getCurrentEmploymentPanel() {
        return currentEmploymentPanelMFP;
    }

    public void setEnableEmployee(boolean isEnabled) {
        currentEmploymentPanelMFP.setEnabled(isEnabled);
        secondaryEmploymentPanel1.setEnabled(isEnabled);
        secondaryEmploymentPanel2.setEnabled(isEnabled);
    }

    public void setQuotation(Quotation quotation) {
        this.quotation = quotation;
    }
}

