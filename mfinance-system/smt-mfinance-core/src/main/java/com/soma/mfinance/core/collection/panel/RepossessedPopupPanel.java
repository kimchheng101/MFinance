package com.soma.mfinance.core.collection.panel;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.auction.model.ContractAuctionData;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.service.aftersales.LossService;
import com.soma.mfinance.core.contract.service.aftersales.LossSimulateRequest;
import com.soma.mfinance.core.contract.service.aftersales.LossSimulateResponse;
import com.soma.mfinance.core.contract.service.aftersales.LossValidateRequest;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.workflow.AuctionWkfStatus;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox.ButtonType;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.soma.mfinance.core.auction.model.ContractAuctionData;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification.Type;
import org.apache.commons.lang.StringUtils;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.Entity;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;

import java.util.Date;
import java.util.List;

import static com.soma.frmk.helper.SeuksaServicesHelper.ENTITY_SRV;

/**
 * @author Calvin
 */
public class RepossessedPopupPanel extends Window {

    private static final long serialVersionUID = -3257822869747884791L;

    private LossService lossService = SpringUtils.getBean(LossService.class);

    private AutoDateField dfDateRequestRepossessed;
    private TextField txtRepossessionFee;
    private TextField txtCollectionFee;
    private Contract contract;
    private ComboBox cbxWhoRepossess;
    private ComboBox cbxSupport1;
    private ComboBox cbxSupport2;
    private ComboBox cbxWhoBringToWareHouse;

    /**
     * @param contractStatus
     * @param contract
     */
    public RepossessedPopupPanel(final EWkfStatus contractStatus, final Contract contract, AbstractTabsheetPanel tabsheetPanel, AbstractTablePanel tablePanel) {
        setModal(true);
        setCaption(contractStatus.getDesc());
        this.contract = contract;

        dfDateRequestRepossessed = ComponentFactory.getAutoDateField();
        dfDateRequestRepossessed.setRequired(true);
        dfDateRequestRepossessed.setValue(DateUtils.today());
        dfDateRequestRepossessed.setImmediate(true);

        txtRepossessionFee = ComponentFactory.getTextField(100, 150);
        txtRepossessionFee.setEnabled(true);
        txtCollectionFee = ComponentFactory.getTextField(100, 150);
        txtCollectionFee.setEnabled(true);


        cbxWhoRepossess = new ComboBox();
        cbxWhoRepossess.setRequired(true);
        cbxSupport1 = new ComboBox();
        cbxSupport2 = new ComboBox();
        cbxWhoBringToWareHouse = new ComboBox();
        cbxWhoBringToWareHouse.setRequired(true);

        String sql = "select t1.sec_usr_desc, t1.sec_usr_id  from tu_sec_user t1 "
                + "INNER JOIN tu_sec_user_profile t2 ON t1.sec_usr_id = t2.sec_usr_id"
                + " where t1.sta_rec_id=1 AND t2.sec_pro_id = 9 OR  t2.sec_pro_id = 10;";

        List<Object[]> secUsersCollection = ENTITY_SRV.createSqlQuery(sql).list();

        for (Object[] userProfile : secUsersCollection) {
            cbxWhoRepossess.addItem(userProfile[0]);
            cbxSupport1.addItem(userProfile[0]);
            cbxSupport2.addItem(userProfile[0]);
            cbxWhoBringToWareHouse.addItem(userProfile[0]);
        }


        final GridLayout gridLayout = new GridLayout(9, 7);
        gridLayout.setMargin(true);
        gridLayout.setSpacing(true);

        int iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("date.repossessed")), iCol++, 0);
        gridLayout.addComponent(dfDateRequestRepossessed, iCol++, 0);

        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("repossession.fee")), iCol++, 1);
        gridLayout.addComponent(txtRepossessionFee, iCol++, 1);

        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("collection.fee")), iCol++, 2);
        gridLayout.addComponent(txtCollectionFee, iCol++, 2);

        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("who.repossess")), iCol++, 3);
        gridLayout.addComponent(cbxWhoRepossess, iCol++, 3);
        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("support1")), iCol++, 4);
        gridLayout.addComponent(cbxSupport1, iCol++, 4);

        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("support2")), iCol++, 5);
        gridLayout.addComponent(cbxSupport2, iCol++, 5);

        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("who.bring.to.ware.house")), iCol++, 6);
        gridLayout.addComponent(cbxWhoBringToWareHouse, iCol++, 6);

        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setSpacing(true);
        Button btnSave = new NativeButton(I18N.message("validate"), new Button.ClickListener() {
            private static final long serialVersionUID = 7657693632881547084L;

            public void buttonClick(ClickEvent event) {
                doRepossess(contractStatus,tabsheetPanel,tablePanel);

            }

        });

        btnSave.setIcon(new ThemeResource("../smt-default/icons/16/save.png"));

        Button btnCancel = new NativeButton(I18N.message("close"), new Button.ClickListener() {
            private static final long serialVersionUID = 3975121141565713259L;

            public void buttonClick(ClickEvent event) {
                close();
            }
        });

        btnCancel.setIcon(new ThemeResource("../smt-default/icons/16/delete.png"));
        NavigationPanel navigationPanel = new NavigationPanel();
        navigationPanel.addButton(btnSave);
        navigationPanel.addButton(btnCancel);
        contentLayout.addComponent(navigationPanel);
        contentLayout.addComponent(gridLayout);
        setContent(contentLayout);
    }

    private void doRepossess(EWkfStatus contractStatus,AbstractTabsheetPanel tabsheetPanel, AbstractTablePanel tablePanel) {
        Date lossDate = dfDateRequestRepossessed.getValue();

        if (lossDate != null
                && cbxWhoRepossess.getValue() != null
                && !cbxWhoRepossess.getValue().toString().isEmpty()
                && cbxWhoBringToWareHouse.getValue() != null
                && !cbxWhoBringToWareHouse.getValue().toString().isEmpty()) {


            // Save Basic Informtaion for Repossess
            contract.setRequestRepossess(true);
            contract.setWhoRepossess(cbxWhoRepossess.getValue().toString());

            contract.setSupport1(cbxSupport1.getValue() != null ? cbxSupport1.getValue().toString() : "");
            contract.setSupport2(cbxSupport2.getValue() != null ? cbxSupport2.getValue().toString() : "");
            contract.setWhoBringToWareHouse(cbxWhoBringToWareHouse.getValue().toString());
            contract.setWkfStatus(ContractWkfStatus.REP);
            contract.setAuctionStatus(AuctionWkfStatus.EVA);
            ENTITY_SRV.saveOrUpdate(contract);

            Quotation quotation = contract.getQuotation();
            quotation.setWkfStatus(ContractWkfStatus.REP);
            ENTITY_SRV.saveOrUpdate(quotation);

            // End of Update Contract and Quotation
            Entity contractAuctionData=getContractAuctionDataEntity();
            // Save Contract AuctionData
            lossService.saveOrUpdate(contractAuctionData);

            // Request and Response Loss Simulation
            LossSimulateRequest lossSimulateRequest = new LossSimulateRequest();
            lossSimulateRequest.setCotraId(contract.getId());
            lossSimulateRequest.setEventDate(lossDate);

            LossSimulateResponse lossSimulateResponse = lossService.simulate(lossSimulateRequest);

            LossValidateRequest request = new LossValidateRequest();

            request.setCotraId(contract.getId());
            request.setWkfStatus(contractStatus);
            request.setEventDate(lossSimulateResponse.getEventDate());
            request.setTotalInterest(lossSimulateResponse.getTotalInterest());
            request.setTotalPrincipal(lossSimulateResponse.getTotalPrincipal());
            request.setTotalOther(lossSimulateResponse.getTotalOther());
            request.setInsuranceFee(lossSimulateResponse.getInsuranceFee());
            request.setServicingFee(lossSimulateResponse.getServicingFee());
            request.setTransferFee(lossSimulateResponse.getTransferFee());
            request.setCashflows(lossSimulateResponse.getCashflows());

            lossService.validate(request);


            Notification notification = new Notification("", Type.HUMANIZED_MESSAGE);
            notification.setDescription(I18N.message("success.change.status.contract"));
            notification.setDelayMsec(3000);
            notification.show(Page.getCurrent());
            close();
            displayTablePanel(tabsheetPanel,tablePanel);

        } else if (lossDate == null) {
            MessageBox mb = new MessageBox(UI.getCurrent(), "320px", "160px", I18N.message("information"),
                    MessageBox.Icon.ERROR, I18N.message("please.select.date"), Alignment.MIDDLE_RIGHT,
                    new MessageBox.ButtonConfig(ButtonType.OK, I18N.message("ok")));
            mb.show();
        } else if (cbxWhoRepossess.getValue() == null || cbxWhoRepossess.getValue().toString().isEmpty()) {
            MessageBox mb = new MessageBox(UI.getCurrent(), "320px", "160px", I18N.message("information"),
                    MessageBox.Icon.ERROR, I18N.message("who.repossess") + "field required", Alignment.MIDDLE_RIGHT,
                    new MessageBox.ButtonConfig(ButtonType.OK, I18N.message("ok")));
            mb.show();
        } else if (cbxWhoBringToWareHouse.getValue() == null || cbxWhoBringToWareHouse.getValue().toString().isEmpty()) {
            MessageBox mb = new MessageBox(UI.getCurrent(), "320px", "160px", I18N.message("information"),
                    MessageBox.Icon.ERROR, I18N.message("who.bring.to.ware.house") + "field required", Alignment.MIDDLE_RIGHT,
                    new MessageBox.ButtonConfig(ButtonType.OK, I18N.message("ok")));
            mb.show();
        }
    }

    protected Entity getContractAuctionDataEntity() {
        ContractAuctionData contractAuctionData = null;
        if (contract != null) {
            contractAuctionData = contract.getContractAuctionData();
            if (contractAuctionData == null) {
                contractAuctionData = new ContractAuctionData();
                contractAuctionData.setContract(contract);
            }
        }
        contractAuctionData.setRequestRepossessedDate(dfDateRequestRepossessed.getValue());
        contractAuctionData.setRepossessionFeeUsd(getDataFromTextField(txtRepossessionFee.getValue()));
        contractAuctionData.setCollectionFeeUsd(getDataFromTextField(txtCollectionFee.getValue()));

        return contractAuctionData;
    }

    public Double getDataFromTextField(String data) {
        if (data == null || StringUtils.isEmpty(data)) {
            return 0d;
        }
        return Double.valueOf(data.replaceAll(",", "").toString());
    }

    /***
     * For redirect to its original
     * @param tabsheetPanel
     * @param tablePanel
     */
    public void displayTablePanel(AbstractTabsheetPanel tabsheetPanel, AbstractTablePanel tablePanel) {
        tablePanel.refresh();
        tabsheetPanel.getTabSheet().setSelectedTab(tablePanel);
    }
}



