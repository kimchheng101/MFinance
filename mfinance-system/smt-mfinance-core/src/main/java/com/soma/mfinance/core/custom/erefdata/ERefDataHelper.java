package com.soma.mfinance.core.custom.erefdata;

import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.frmk.config.service.RefDataService;
import org.seuksa.frmk.model.entity.RefDataId;
import org.seuksa.frmk.model.eref.ColERefData;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * Created by cheasocheat on 4/20/17.
 */
public final class ERefDataHelper implements FinServicesHelper {

    public static Map<String, Map<Long, ? extends RefDataId>> refData = null;

    static {
        try {
            Field field = ColERefData.class.getDeclaredField("mainMapERefData");
            field.setAccessible(true);
            refData = (Map<String, Map<Long, ? extends RefDataId>>) field.get(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*private static <T> T getTargetObject(Object proxy) throws Exception {
        while ((AopUtils.isJdkDynamicProxy(proxy))) {
            return (T) getTargetObject(((Advised) proxy).getTargetSource().getTarget());
        }
        return (T) proxy; // expected to be cglib proxy then, which is simply a specialized class
    }*/

}
