package com.soma.mfinance.core.dealer.dao;

import org.seuksa.frmk.dao.BaseEntityDao;

/**
 * Dealer data model access
 * @author kimsuor.seang
 *
 */
public interface DealerDao extends BaseEntityDao {

}
