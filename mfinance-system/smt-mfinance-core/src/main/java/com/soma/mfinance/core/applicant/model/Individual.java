package com.soma.mfinance.core.applicant.model;

import com.soma.mfinance.core.actor.model.Liability;
import com.soma.mfinance.core.model.actor.SourcePayment;
import com.soma.mfinance.core.panel.applicant.FamilyAndExpense;
import com.soma.ersys.core.hr.model.address.Address;
import com.soma.ersys.core.hr.model.eref.EEmploymentType;
import com.soma.ersys.core.hr.model.eref.EJobPosition;
import com.soma.ersys.core.hr.model.eref.ETypeAddress;
import com.soma.ersys.core.hr.model.eref.ETypeContactInfo;
import com.soma.ersys.core.hr.model.organization.ContactInfo;
import org.apache.commons.lang3.StringUtils;
import org.seuksa.frmk.model.EntityFactory;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author kimsuor.seang
 *
 */
@Entity
@Table(name = "td_individual")
public class Individual extends AbstractIndividual {

	private static final long serialVersionUID = 6949885917223461002L;

	private List<IndividualAddress> individualAddresses;
	private List<Employment> employments;
	private List<IndividualContactInfo> individualContactInfos;
	private List<IndividualReferenceInfo> individualReferenceInfos;
	private List<IndividualSpouse> individualSpouses;
	private SourcePayment sourcePayment;
	private List<Liability> liabilities;
	private FamilyAndExpense familyAndExpense;
	private EJobPosition applicantEquipmentPosition;
	private String equipmentRentalName;
	private String equipmentRentalReason;
	private Integer equipmentRentalInYear;
	private Integer equipmentRentalInMonth;

    public static Individual createInstance() {
        Individual individual = EntityFactory.createInstance(Individual.class);
        return individual;
    }

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ind_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @OneToMany(mappedBy = "individual", fetch = FetchType.LAZY)
    public List<IndividualAddress> getIndividualAddresses() {
        if (individualAddresses == null) {
            individualAddresses = new ArrayList<>();
        }
        return individualAddresses;
    }
    public void setIndividualAddresses(List<IndividualAddress> individualAddresses) {
        this.individualAddresses = individualAddresses;
    }

    @OneToMany(mappedBy = "individual", fetch = FetchType.LAZY)
    public List<Employment> getEmployments() {
        if (employments == null) {
            employments = new ArrayList<>();
        }
        return employments;
    }
    public void setEmployments(List<Employment> employments) {
        this.employments = employments;
    }

    @OneToMany(mappedBy = "individual", fetch = FetchType.LAZY)
    public List<IndividualContactInfo> getIndividualContactInfos() {
        return individualContactInfos;
    }
	public void setIndividualContactInfos(
			List<IndividualContactInfo> individualContactInfos) {
		this.individualContactInfos = individualContactInfos;
	}

	@OneToMany(mappedBy="individual", fetch = FetchType.LAZY)
	public List<IndividualReferenceInfo> getIndividualReferenceInfos() {
		return individualReferenceInfos;
	}
    public void setIndividualReferenceInfos(
            List<IndividualReferenceInfo> individualReferenceInfos) {
        this.individualReferenceInfos = individualReferenceInfos;
    }

    @OneToMany(mappedBy = "individual", fetch = FetchType.LAZY)
    public List<IndividualSpouse> getIndividualSpouses() {
        if (individualSpouses == null) {
            individualSpouses = new ArrayList<IndividualSpouse>();
        }
        return individualSpouses;
    }
	public void setIndividualSpouses(List<IndividualSpouse> individualSpouses) {
		this.individualSpouses = individualSpouses;
	}

	@Transient
	public IndividualSpouse getIndividualSpouse() {
		if (individualSpouses != null && !individualSpouses.isEmpty()) {
			return individualSpouses.get(0);
		}
		return null;
	}

	@Transient
	public void addEmployment(Employment employment) {
		if (employments == null) {
			employments = new ArrayList<>();
		}
		employments.add(employment);
	}

	@Transient
	public Employment getCurrentEmployment() {
		if (employments != null && !employments.isEmpty()) {
			for (Employment employment : employments) {
				if(employment.getEmploymentType() != null){
					if (employment.getEmploymentType().equals(EEmploymentType.CURR)) {
						return employment;
					}
				}
			}
		}
		return null;
	}

	@Transient
	public List<Employment> getEmployments(EEmploymentType employmentType) {
		List<Employment> prevEmployments = new ArrayList<>();
		if (employments != null && !employments.isEmpty()) {
			for (Employment employment : employments) {
				if(employment.getEmploymentType() != null){
					if (employment.getEmploymentType().equals(employmentType)) {
						if(employment != null){
							prevEmployments.add(employment);
						}
					}
				}
			}
		}
		return prevEmployments;
	}

	@Transient
	public void setAddress(Address address, ETypeAddress addressType) {
		if (individualAddresses == null) {
			individualAddresses = new ArrayList<>();
		}

		IndividualAddress individualAddress = getIndividualAddress(addressType);
		if (individualAddress == null) {
			individualAddress = new IndividualAddress();
			individualAddresses.add(individualAddress);
		}
		address.setType(addressType);
		individualAddress.setIndividual(this);
		individualAddress.setAddress(address);
	}

	@Transient
	public IndividualAddress getIndividualAddress(ETypeAddress addressType) {
		if (individualAddresses != null && !individualAddresses.isEmpty()) {
			for (IndividualAddress individualAddress : individualAddresses) {
				if (addressType.equals(individualAddress.getAddress().getType())) {
					return individualAddress;
				}
			}
		}
		return null;
	}

	@Transient
	public String getIndividualPrimaryContactInfo() {
		if (individualContactInfos != null && !individualContactInfos.isEmpty()) {
			for (IndividualContactInfo indContactInfo : individualContactInfos) {
				String primaryContact = getPrimaryContactInfo(indContactInfo.getContactInfo());
				if (StringUtils.isNotEmpty(primaryContact)) {
					return primaryContact;
				}
			}
		}
		return StringUtils.EMPTY;
	}

	@Transient
	public String getPrimaryContactInfo(ContactInfo info) {
		if (info.isPrimary()) {
			if (ETypeContactInfo.LANDLINE.equals(info.getTypeInfo())
					|| ETypeContactInfo.MOBILE.equals(info.getTypeInfo())) {
				return info.getValue() == null ? "" : info.getValue();
			}
		}
		return StringUtils.EMPTY;
	}

	@Transient
	public String getIndividualSecondaryContactInfo() {
		if (individualContactInfos != null && !individualContactInfos.isEmpty()) {
			for (IndividualContactInfo indContactInfo : individualContactInfos) {
				ContactInfo contactInfo = indContactInfo.getContactInfo();
				if (ETypeContactInfo.LANDLINE.equals(contactInfo.getTypeInfo())
						|| ETypeContactInfo.MOBILE.equals(contactInfo.getTypeInfo())) {
					if (!contactInfo.isPrimary()) {
						return contactInfo.getValue() == null ? "" : contactInfo.getValue();
					}
				}
			}
		}
		return StringUtils.EMPTY;
	}

	@Transient
	public Address getMainAddress() {
		IndividualAddress individualAddress = getIndividualAddress(ETypeAddress.MAIN);
		return individualAddress == null ? null : individualAddress.getAddress();
	}


	@Column(name = "ind_pos_id", nullable = true)
	@Convert(converter = EJobPosition.class)
	public EJobPosition getApplicantEquipmentPosition() {
		return applicantEquipmentPosition;
	}

	public void setApplicantEquipmentPosition(EJobPosition applicantEquipmentPosition) {
		this.applicantEquipmentPosition = applicantEquipmentPosition;
	}

	@Column(name = "appli_equipment_name", nullable = true)
	public String getEquipmentRentalName() {
		return equipmentRentalName;
	}

	public void setEquipmentRentalName(String equipmentRentalName) {
		this.equipmentRentalName = equipmentRentalName;
	}

	@Column(name = "appli_equipment_reason", nullable = true)
	public String getEquipmentRentalReason() {
		return equipmentRentalReason;
	}

	public void setEquipmentRentalReason(String equipmentRentalReason) {
		this.equipmentRentalReason = equipmentRentalReason;
	}

	@Column(name = "appli_equipment_year", nullable = true)
	public Integer getEquipmentRentalInYear() {
		return equipmentRentalInYear;
	}

	public void setEquipmentRentalInYear(Integer equipmentRentalInYear) {
		this.equipmentRentalInYear = equipmentRentalInYear;
	}

	@Column(name = "appli_equipment_month", nullable = true)
	public Integer getEquipmentRentalInMonth() {
		return equipmentRentalInMonth;
	}

	public void setEquipmentRentalInMonth(Integer equipmentRentalInMonth) {
		this.equipmentRentalInMonth = equipmentRentalInMonth;
	}

	@OneToOne(mappedBy = "individual", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
	public SourcePayment getSourcePayment() {
		return sourcePayment;
	}

	public void setSourcePayment(SourcePayment sourcePayment) {
		this.sourcePayment = sourcePayment;
	}

	@OneToMany(mappedBy="individual", fetch = FetchType.LAZY)
	public List<Liability> getLiabilities() {
		return liabilities;
	}

	public void setLiabilities(List<Liability> liabilities) {
		this.liabilities = liabilities;
	}

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "individual")
	public FamilyAndExpense getFamilyAndExpense() {
		return familyAndExpense;
	}

	public void setFamilyAndExpense(FamilyAndExpense familyAndExpense) {
		this.familyAndExpense = familyAndExpense;
	}
}

