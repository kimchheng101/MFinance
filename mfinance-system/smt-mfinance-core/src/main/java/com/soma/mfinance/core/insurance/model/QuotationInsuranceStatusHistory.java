package com.soma.mfinance.core.insurance.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.soma.mfinance.core.quotation.model.Quotation;

import org.seuksa.frmk.model.entity.EntityA;

@SuppressWarnings("deprecation")
@Entity
@Table(name = "td_quotation_insurance_status_history")
public class QuotationInsuranceStatusHistory extends EntityA {

	private static final long serialVersionUID = 8878165531840138458L;
	
	private InsuranceStatus insuranceStatus;
	private Quotation quotation;
	
	/**
	 * @return id
	 * @see org.seuksa.frmk.model.entity.EntityA#getId()
	 */
    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "quo_ins_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }
    
    /**
	 * @return the insuranceStatus
	 */
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ins_sta_id", nullable = true)
	public InsuranceStatus getInsuranceStatus() {
		return insuranceStatus;
	}

	/**
	 * @param insuranceStatus the insuranceStatus to set
	 */
	public void setInsuranceStatus(InsuranceStatus insuranceStatus) {
		this.insuranceStatus = insuranceStatus;
	}
    
    /**
	 * @return the quotation
	 */
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "quo_id")
	public Quotation getQuotation() {
		return quotation;
	}

	/**
	 * @param quotation the quotation to set
	 */
	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}

}
