package com.soma.mfinance.core.contract.service.aftersales.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.soma.mfinance.core.accounting.InstallmentVO;
import com.soma.mfinance.core.contract.model.LockSplit;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowCode;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowType;
import com.soma.mfinance.core.contract.model.cashflow.ETreasuryType;
import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyMathUtils;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.amount.Amount;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soma.mfinance.core.aftersale.EAfterSaleEventType;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.service.ContractService;
import com.soma.mfinance.core.contract.service.aftersales.EarlySettlementSaveRequest;
import com.soma.mfinance.core.contract.service.aftersales.EarlySettlementSaveResponse;
import com.soma.mfinance.core.contract.service.aftersales.EarlySettlementService;
import com.soma.mfinance.core.contract.service.aftersales.EarlySettlementSimulateRequest;
import com.soma.mfinance.core.contract.service.aftersales.EarlySettlementSimulateResponse;
import com.soma.mfinance.core.contract.service.aftersales.EarlySettlementValidateRequest;
import com.soma.mfinance.core.contract.service.aftersales.EarlySettlementValidateResponse;
import com.soma.mfinance.core.contract.service.cashflow.impl.CashflowUtils;
import com.soma.mfinance.core.financial.model.EServiceType;
import com.soma.mfinance.core.payment.service.LockSplitService;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.ersys.finance.accounting.model.JournalEvent;

import static com.soma.mfinance.core.helper.FinServicesHelper.INSTALLMENT_SERVICE_MFP;

/**
 * 
 * @author kimsuor.seang
 *
 */
@Service("earlySettlementService")
public class EarlySettlementServiceImpl extends BaseEntityServiceImpl implements EarlySettlementService, FMEntityField {
	/** */
	private static final long serialVersionUID = 7726232467770673141L;
	
	@Autowired
    private EntityDao dao;
	
	@Autowired
	private ContractService contractService;
	
	@Autowired
	private LockSplitService lockSplitService;
			
	/**
	 * @see org.seuksa.frmk.service.impl.BaseEntityServiceImpl#getDao()
	 */
	@Override
	public EntityDao getDao() {
		return dao;
	}
	
	/**
	 * @param request
	 * @return
	 */
	public EarlySettlementSimulateResponse simulate(EarlySettlementSimulateRequest request) {
		EarlySettlementSimulateResponse response = new EarlySettlementSimulateResponse();

		//TODO: Sreyrath's code
		Amount tiPrincipal = new Amount(0d, 0d, 0d);
		Amount tiInterestRate = new Amount(0d, 0d, 0d);
		Amount tiInsurance = new Amount(0d, 0d, 0d);
		Amount tiServiceFee = new Amount(0d, 0d, 0d);
		Amount tiTranfer = new Amount(0d, 0d, 0d);
		Amount otherAmount = new Amount(0d, 0d, 0d);
		//Date installmentDate = null;
		Amount adjustmentInterest = new Amount();
		Double insFeePaidOff = null, tiInsuranceAll = null;
		Date lostDate = request.getEventDate();

		int firstInstallmentNotPaid = 1;
		Contract contract = getById(Contract.class, request.getCotraId());
		if (contract.getLastPaidNumInstallment() != null) {
			firstInstallmentNotPaid = contract.getLastPaidNumInstallment() + 1;
		}
		int lastNumInstallment = contract.getTerm();
		for (int i = firstInstallmentNotPaid; i <= lastNumInstallment; i++) {
			List<InstallmentVO> installmentByNumInstallments = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(contract, i);
			for (InstallmentVO installmentVO : installmentByNumInstallments) {
				if (installmentVO.getCashflowType().equals(ECashflowType.CAP)) {
					tiPrincipal.plusTiAmount(installmentVO.getTiamount());
					tiPrincipal.plusTeAmount(installmentVO.getTiamount());
					tiPrincipal.plusVatAmount(installmentVO.getVatAmount());
				} else if(installmentVO.getCashflowType().equals(ECashflowType.IAP)) {
					if(contract.getPenaltyTermPayoff() >= i){
						tiInterestRate.plusTiAmount(installmentVO.getTiamount());
						tiInterestRate.plusTeAmount(installmentVO.getTiamount());
					} else {

						long nbDiff = DateUtils.getDiffInDays(installmentVO.getInstallmentDate(),DateUtils.getDateAtEndOfDay(lostDate));
						long nbDaysOfMonth = DateUtils.getNbDaysInMonth(DateUtils.today());

						if(lostDate.after(installmentVO.getInstallmentDate())){
							tiInterestRate.plusTiAmount(installmentVO.getTiamount());
							tiInterestRate.plusTeAmount(installmentVO.getTiamount());
						} else if(nbDiff <= 30){
							tiInterestRate.plusTiAmount(MyMathUtils.roundAmountTo((nbDaysOfMonth-nbDiff) * installmentVO.getTiamount() / nbDaysOfMonth));
						}
					}
				} else if(installmentVO.getService() != null && EServiceType.INSFEE.getCode().equals(installmentVO.getService().getCode())){
					tiInsurance.plusTiAmount(installmentVO.getTiamount());
					tiInsurance.plusTeAmount(installmentVO.getTiamount());
					tiInsurance.plusVatAmount(installmentVO.getVatAmount());

					tiInsuranceAll = tiInsurance.getTiAmount() + tiInsurance.getVatAmount();
					Double tiInsOneMonth = installmentVO.getTiamount() + installmentVO.getVatAmount();

					if(lastNumInstallment == 12
							|| (lastNumInstallment == 24 && firstInstallmentNotPaid > 12)
							|| (lastNumInstallment == 36 && firstInstallmentNotPaid > 24) )
						insFeePaidOff = tiInsuranceAll;
					else if (lastNumInstallment == 24 && firstInstallmentNotPaid <= 12)
						insFeePaidOff = tiInsuranceAll - (tiInsOneMonth * 12);
					else if (lastNumInstallment == 36) {
						if (firstInstallmentNotPaid <= 12)
							insFeePaidOff = tiInsuranceAll - (tiInsOneMonth * 24);
						else if (firstInstallmentNotPaid > 12 && firstInstallmentNotPaid <= 24)
							insFeePaidOff = tiInsuranceAll - (tiInsOneMonth * 12);
					}
				} else if(installmentVO.getService() != null
						&& (EServiceType.SRVFEE.getCode().equals(installmentVO.getService().getCode())
						||EServiceType.SRVFEEEXT.getCode().equals(installmentVO.getService().getCode()))){
					tiServiceFee.plusTiAmount(installmentVO.getTiamount());
					tiServiceFee.plusTeAmount(installmentVO.getTiamount());
					tiServiceFee.plusVatAmount(installmentVO.getVatAmount());
				} else if(installmentVO.getService() != null && EServiceType.TRANSFEE.getCode().equals(installmentVO.getService().getCode())) {
					tiTranfer.plusTiAmount(installmentVO.getTiamount());
					tiTranfer.plusTeAmount(installmentVO.getTiamount());
				} else {
					otherAmount.plusTiAmount(installmentVO.getTiamount());
					otherAmount.plusTeAmount(installmentVO.getTiamount());
					otherAmount.plusVatAmount(installmentVO.getVatAmount());
				}
				/*if(i == lastNumInstallment){
					installmentDate = installmentVO.getInstallmentDate();
				}*/
			}
		}

		response.setCotraId(request.getCotraId());
		response.setEventDate(lostDate);
		response.setBalanceCapital(tiPrincipal);
		response.setBalanceInterest(tiInterestRate);
		response.setInsuranceFee(AmountUtils.convertToAmount(insFeePaidOff));
		response.setBalanceTransferFee(tiTranfer);
		response.setServicingFee(tiServiceFee);
		response.setTotalOther(otherAmount);
		response.setAdjustmentInterest(tiInterestRate);
		/*List<Cashflow> cashflows = getCashflows(payOffVO,contract);
		response.setCashflows(cashflows);*/

		//TODO: END
				
		/*balanceInterest = contractService.getRealInterestBalance(request.getEventDate(), cashflows);
		
		if (balanceInterest != null && balanceInterest.getTiAmount() > 0) {
			
			discountInterest.plusTiAmount(MyMathUtils.roundAmountTo(balanceInterest.getTiAmount() / 2));
			discountInterest.plusTeAmount(MyMathUtils.roundAmountTo(balanceInterest.getTeAmount() / 2));
			discountInterest.plusVatAmount(MyMathUtils.roundAmountTo(balanceInterest.getVatAmount() / 2));
			
						
			adjustmentInterest.plusTiAmount(MyMathUtils.roundAmountTo(balanceInterest.getTiAmount() - discountInterest.getTiAmount()));
			adjustmentInterest.plusTeAmount(MyMathUtils.roundAmountTo(balanceInterest.getTeAmount() - discountInterest.getTeAmount()));
			adjustmentInterest.plusVatAmount(MyMathUtils.roundAmountTo(balanceInterest.getVatAmount() - discountInterest.getVatAmount()));
		}
		*/
		

		return response;
	}
	
	/**
	 * @param request
	 * @return
	 */
	public EarlySettlementSaveResponse save(EarlySettlementSaveRequest request) {
		EarlySettlementSaveResponse earlySettlementSaveResponse = new EarlySettlementSaveResponse();
		
		LockSplit lockSplit = new LockSplit();
		lockSplit.setContract(getById(Contract.class, request.getCotraId()));
		lockSplit.setFrom(request.getEarlySettlementDate());
		lockSplit.setTo(request.getEarlySettlementDate());
		lockSplit.setWhiteClearance(true);
		lockSplit.setAfterSaleEventType(EAfterSaleEventType.EARLYSETTLEMENT);
				
/*		List<LockSplitItem> items = new ArrayList<>();
		lockSplitService.addLockSplitItem(lockSplit, items, ECashflowType.CAP, request.getTotalPrincipal());
		lockSplitService.addLockSplitItem(lockSplit, items, ECashflowType.IAP, request.getTotalInterest());
		if (MyNumberUtils.getDouble(request.getTotalPenalty().getTiAmount()) > 0) {
			lockSplitService.addLockSplitItem(lockSplit, items, ECashflowType.PEN, request.getTotalPenalty());
		}
		
		if (MyNumberUtils.getDouble(request.getInsuranceFee().getTiAmount()) > 0) {
			lockSplitService.addLockSplitItem(lockSplit, items, ECashflowType.FEE, getByCode(FinService.class, EServiceType.INSFEE.getCode()), request.getInsuranceFee());
		}
		
		if (MyNumberUtils.getDouble(request.getServicingFee().getTiAmount()) > 0) {
			lockSplitService.addLockSplitItem(lockSplit, items, ECashflowType.FEE, getByCode(FinService.class, EServiceType.SRVFEE.getCode()), request.getServicingFee());
		}*/
		
		// lockSplit.setItems(items);
		
		lockSplitService.saveLockSplit(lockSplit);
		
		earlySettlementSaveResponse.setLockSplit(lockSplit);
		return earlySettlementSaveResponse;
	}
	
		
	/**
	 * @param request
	 */
	public EarlySettlementValidateResponse validate(EarlySettlementValidateRequest request) {
		
		Contract contract = getById(Contract.class, request.getCotraId());
		Integer term = contract.getTerm();
		
		List<Cashflow> earlySettlementCashflows = new ArrayList<>();
		
		Cashflow cashflowCAP = CashflowUtils.createCashflow(contract.getProductLine(), null,
				contract, contract.getVatValue(), ECashflowType.CAP, ETreasuryType.APP, getByCode(JournalEvent.class, ECashflowType.CAP_JOURNAL_EVENT),
				contract.getProductLine().getPaymentConditionCap(), request.getBalanceCapital().getTeAmount(), request.getBalanceCapital().getVatAmount(), 
				request.getBalanceCapital().getTiAmount(), request.getEarlySettlementDate(), request.getEarlySettlementDate(), request.getEarlySettlementDate(), term);
		cashflowCAP.setCashflowCode(ECashflowCode.EAR);
		saveOrUpdate(cashflowCAP);
		earlySettlementCashflows.add(cashflowCAP);
		
		if (MyNumberUtils.getDouble(request.getBalanceInterest().getTeAmount()) > 0) {
			// Discount interest 50%
			request.setBalanceInterest(discount50Percent(request.getBalanceInterest()));
			Cashflow cashflowIAP = CashflowUtils.createCashflow(contract.getProductLine(), null,
						contract, contract.getVatValue(), ECashflowType.IAP, ETreasuryType.APP, getByCode(JournalEvent.class, ECashflowType.IAP_JOURNAL_EVENT),
						contract.getProductLine().getPaymentConditionIap(), request.getBalanceInterest().getTeAmount(), request.getBalanceInterest().getVatAmount(),
						request.getBalanceInterest().getTiAmount(), request.getEarlySettlementDate(), request.getEarlySettlementDate(), request.getEarlySettlementDate(), term);
			cashflowIAP.setCashflowCode(ECashflowCode.EAR);
			saveOrUpdate(cashflowIAP);
			earlySettlementCashflows.add(cashflowIAP);
		}
		
		if (MyNumberUtils.getDouble(request.getBalancePenalty().getTeAmount()) > 0) {
			Cashflow cashflowPER = CashflowUtils.createCashflow(contract.getProductLine(), null,
					contract, contract.getVatValue(), ECashflowType.PEN, ETreasuryType.APP, getByCode(JournalEvent.class, ECashflowType.PEN_JOURNAL_EVENT),
					contract.getProductLine().getPaymentConditionIap(), request.getBalancePenalty().getTeAmount(), request.getBalancePenalty().getVatAmount(),
					request.getBalancePenalty().getTiAmount(), request.getEarlySettlementDate(), request.getEarlySettlementDate(), request.getEarlySettlementDate(), term);
			cashflowPER.setCashflowCode(ECashflowCode.EAR);
			saveOrUpdate(cashflowPER);
			earlySettlementCashflows.add(cashflowPER);
		}
		
		for (Cashflow cashflow : request.getCashflows()) {
			if (!cashflow.isCancel() && !cashflow.isPaid() && !cashflow.isUnpaid()) {
				cashflow.setCancel(true);
				cashflow.setCancelationDate(DateUtils.today());
				cashflow.setCashflowCode(ECashflowCode.EAR);
				saveOrUpdate(cashflow);
			}
		}
				
		contractService.changeContractStatus(contract, ContractWkfStatus.EAR, request.getEarlySettlementDate());
		
		EarlySettlementValidateResponse response = new EarlySettlementValidateResponse();
		return response;
	}
	
	/**
	 * Discount amount 50%
	 * @param value
	 * @return
	 */
	private Amount discount50Percent(Amount value) {
		if (value == null) {
			return null;
		} else {
			Amount amount = new Amount();
			amount.setNbDecimal(value.getNbDecimal());
			if (value.getTiAmount() != null) {
				amount.setTiAmount(value.getTiAmount() / 2);
			}
			if (value.getTeAmount() != null) {
				amount.setTeAmount(value.getTeAmount() / 2);
			}
			if (value.getVatAmount() != null) {
				amount.setVatAmount(value.getVatAmount() / 2);
			}
			return amount;
		}
	}

}
