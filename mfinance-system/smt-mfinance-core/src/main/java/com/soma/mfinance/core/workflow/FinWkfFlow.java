package com.soma.mfinance.core.workflow;

import com.soma.common.app.workflow.model.EWkfFlow;
import com.soma.mfinance.core.auction.model.Auction;
import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.payment.model.Payment;
import com.soma.mfinance.core.quotation.model.Quotation;

/**
 * List of WkfFlow 
 * 
 * @author kimsuor.seang
 *
 */
public class FinWkfFlow {
	public static final EWkfFlow QUOTATION = EWkfFlow.getByClass(Quotation.class);
	public static final EWkfFlow CONTRACT = EWkfFlow.getByClass(Contract.class);
	public static final EWkfFlow PAYMENT = EWkfFlow.getByClass(Payment.class);
	public static final EWkfFlow COLLECTION = EWkfFlow.getByClass(Collection.class);
	public static final EWkfFlow AUCTION = EWkfFlow.getByClass(Auction.class);
}