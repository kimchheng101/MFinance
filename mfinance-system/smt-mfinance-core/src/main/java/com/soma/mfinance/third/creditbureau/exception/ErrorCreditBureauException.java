package com.soma.mfinance.third.creditbureau.exception;

import com.soma.mfinance.core.shared.exception.BusinessException;

/**
 * Error Credit Bureau Exception
 * @author kimsuor.seang
 */
public class ErrorCreditBureauException extends BusinessException {

	private static final long serialVersionUID = -1876516919210851235L;

	public ErrorCreditBureauException(String message) {
		this(message, null);
	}
	
	public ErrorCreditBureauException(String message, Throwable e) {
		super(message, e);
	}
}
