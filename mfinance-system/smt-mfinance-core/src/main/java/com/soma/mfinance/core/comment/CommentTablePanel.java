package com.soma.mfinance.core.comment;

import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.QuotationService;
import com.soma.mfinance.core.quotation.model.Comment;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.soma.frmk.vaadin.ui.widget.toolbar.event.SaveClickListener;
import com.vaadin.data.Item;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import org.seuksa.frmk.i18n.I18N;

import java.util.ArrayList;
import java.util.List;
import com.vaadin.data.util.IndexedContainer;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Order;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.exception.DaoException;
import org.apache.commons.lang.StringUtils;
import com.soma.frmk.security.context.SecApplicationContextHolder;
import org.springframework.security.core.context.SecurityContextHolder;
import com.soma.frmk.security.model.SecUser;

/**
 * Created by cheasocheat on 1/27/17.
 */
public class CommentTablePanel extends AbstractTabPanel implements SaveClickListener,FinServicesHelper,FMEntityField,Button.ClickListener,ItemClickEvent.ItemClickListener {
    private List<ColumnDefinition> columnDefinitions;
    private SimplePagedTable<Comment> pagedTable;
    private Item selectedItem;
    private Button btnAdd;
    private NavigationPanel navigationPanel;
    private TextArea txtComment;
    private Window winComment;
    private Quotation quotation;
    private Comment comment;
    protected QuotationService quotationService = (QuotationService) SecApplicationContextHolder.getContext().getBean("quotationService");

//    public MCommentTablePanel(ApplicationFormPanel delegate){
//        super();
//        setSizeFull();
//        setMargin(true);
//        setSpacing(true);
//    }

    @Override
    protected Component createForm() {
        //Create Popup

        navigationPanel = new NavigationPanel();
        Button btnSave = new NativeButton(I18N.message("save"), new Button.ClickListener() {
            private static final long serialVersionUID = -4024064977917270885L;
            public void buttonClick(Button.ClickEvent event) {
                if (StringUtils.isNotEmpty(txtComment.getValue())) {
                    SecUser secUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                    comment.setDesc(txtComment.getValue());
                    comment.setForManager(true);
                    comment.setOnlyForUW(true);
                    comment.setQuotation(quotation);
                    comment.setUser(secUser);
                    quotationService.saveOrUpdate(comment);
                    pagedTable.removeAllItems();
                    pagedTable.setContainerDataSource(getIndexedContainer(quotation.getId()));
                }
                winComment.close();
            }
        });
        btnSave.setIcon(new ThemeResource("../smt-default/icons/16/save.png"));
        Button btnCancel = new NativeButton(I18N.message("close"), new Button.ClickListener() {
            private static final long serialVersionUID = 3975121141565713259L;
            public void buttonClick(Button.ClickEvent event) {
                winComment.close();
            }
        });
        btnCancel.setIcon(new ThemeResource("../smt-default/icons/16/delete.png"));

        navigationPanel.addButton(btnSave);
        navigationPanel.addButton(btnCancel);
        this.columnDefinitions = createColumnDefinitions();
        pagedTable = new SimplePagedTable<>(this.columnDefinitions);
        pagedTable.addItemClickListener(this);

        btnAdd = new NativeButton(I18N.message("add"));
        btnAdd.setIcon(FontAwesome.PLUS);
        btnAdd.addClickListener(this);

        NavigationPanel navigationPanel = new NavigationPanel();
        navigationPanel.addButton(btnAdd);

        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setSpacing(true);
        contentLayout.addComponent(navigationPanel);
        contentLayout.addComponent(pagedTable);
        contentLayout.addComponent(pagedTable.createControls());
        return contentLayout;
    }

    /**
     * Get Paged definition
     * @return
     */
    protected List<ColumnDefinition> createColumnDefinitions() {
        List<ColumnDefinition> columnDefinitions = new ArrayList<>();
        columnDefinitions.add(new ColumnDefinition(ID, I18N.message("id"), Long.class, Table.Align.LEFT, 70));
        columnDefinitions.add(new ColumnDefinition(DESC_EN, I18N.message("desc"), String.class, Table.Align.LEFT, 500));
        columnDefinitions.add(new ColumnDefinition(CREATE_USER, I18N.message("user"), String.class, Table.Align.LEFT, 200));
        columnDefinitions.add(new ColumnDefinition(UPDATE_DATE, I18N.message("date"), String.class, Table.Align.LEFT, 200));
        return columnDefinitions;
    }

    @Override
    public void saveButtonClick(Button.ClickEvent clickEvent) {

    }

    @Override
    public void itemClick(ItemClickEvent itemClickEvent) {
        this.selectedItem = itemClickEvent.getItem();
        if (itemClickEvent.isDoubleClick()) {
            //this.delegate.addForm(this.assetMakeId, getItemSelectedId());
            Long id = Long.parseLong(itemClickEvent.getItemId().toString());
            this.getCommentWindow(id);
//          Notification.show(pagedTable.getId());
        }
    }

    @Override
    public void buttonClick(Button.ClickEvent clickEvent) {
        comment = new Comment();
        this.getCommentWindow(0l);
    }

    public void assignValues(Quotation quotation) {
        this.selectedItem = null;
        this.quotation = quotation;
        Long quota_id = quotation.getId();
        if(quota_id != null){
            pagedTable.setContainerDataSource(getIndexedContainer(quota_id));
        }else{
            pagedTable.removeAllItems();
        }
    }

    private void getCommentWindow(Long id){
        winComment = new Window();
        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setSpacing(true);
        contentLayout.setWidth("100%");
        contentLayout.addComponent(navigationPanel);
        txtComment = new TextArea(I18N.message("comment"));
        txtComment.setSizeFull();
        txtComment.setWidth("100%");
        txtComment.setRows(5);
        txtComment.setMaxLength(1000);
        VerticalLayout verticalLayoutComment = new VerticalLayout();
        verticalLayoutComment.setSizeFull();
        verticalLayoutComment.setMargin(true);
        verticalLayoutComment.setSpacing(true);
        verticalLayoutComment.addComponent(txtComment);
        contentLayout.addComponent(verticalLayoutComment);

        winComment.setModal(true);
        winComment.setWidth(430, Unit.PIXELS);
        winComment.setHeight(250, Unit.PIXELS);
        winComment.setCaption(I18N.message("add.comment"));
        winComment.setContent(contentLayout);
        UI.getCurrent().addWindow(winComment);

        if(id != 0){
            BaseRestrictions<Comment> restrictions = new BaseRestrictions<Comment>(Comment.class);
            restrictions.addCriterion(Restrictions.eq(ID, id));
            List<Comment> comments = ENTITY_SRV.list(restrictions);
            comment = comments.get(0);
            txtComment.setValue(comment.getDesc());
        }
    }

    private IndexedContainer getIndexedContainer(Long quo_id) {
        IndexedContainer indexedContainer = new IndexedContainer();
        try {

            for (ColumnDefinition column : this.columnDefinitions) {
                indexedContainer.addContainerProperty(column.getPropertyId(), column.getPropertyType(), null);
            }
            indexedContainer.addContainerProperty(DESC_EN, String.class, null);
            indexedContainer.addContainerProperty(CREATE_USER, String.class, null);

            BaseRestrictions<Comment> restrictions = new BaseRestrictions<Comment>(Comment.class);
            List<Criterion> criterions = new ArrayList<Criterion>();
            criterions.add(Restrictions.eq("quotation.id", quo_id));
            restrictions.setCriterions(criterions);
            restrictions.addOrder(Order.asc(UPDATE_DATE));
//            restrictions.addOrder(Order.desc(ID));

            List<Comment> comments = ENTITY_SRV.list(restrictions);

            for (Comment comment : comments) {
                Item item = indexedContainer.addItem(comment.getId());
                item.getItemProperty(ID).setValue(comment.getId());
                item.getItemProperty(DESC_EN).setValue(comment.getDesc());
                item.getItemProperty(CREATE_USER).setValue(comment.getUser().getDesc());
                item.getItemProperty(UPDATE_DATE).setValue(DateUtils.formatDate(comment.getUpdateDate(), DateUtils.FORMAT_YYYYMMDD_HHMMSS_SLASH));
            }

        } catch (DaoException e) {
            logger.error("DaoException", e);
        }
        return  indexedContainer;
    }
}
