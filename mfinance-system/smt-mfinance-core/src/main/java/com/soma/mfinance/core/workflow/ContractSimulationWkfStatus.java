
package com.soma.mfinance.core.workflow;

import com.soma.common.app.workflow.model.EWkfStatus;

/**
 * Contract status
 * 
 * @author kimsuor.seang
 *
 */
public class ContractSimulationWkfStatus {
	
	public final static EWkfStatus SIMULATED = EWkfStatus.getById(5000);														
	public final static EWkfStatus VALIDATED = EWkfStatus.getById(5001);
	public final static EWkfStatus CANCELLED = EWkfStatus.getById(5002);
}