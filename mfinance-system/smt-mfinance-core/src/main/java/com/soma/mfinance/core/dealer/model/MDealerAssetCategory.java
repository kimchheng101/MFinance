package com.soma.mfinance.core.dealer.model;

import org.seuksa.frmk.model.entity.MEntityRefA;


/**
 * Meta data of DealerAssetCategory
 * @author kimsuor.seang
 */
public interface MDealerAssetCategory extends MEntityRefA {

	public final static String ASSETCATEGORY = "assetCategory";
	public final static String DEALER = "dealer";
	
}
