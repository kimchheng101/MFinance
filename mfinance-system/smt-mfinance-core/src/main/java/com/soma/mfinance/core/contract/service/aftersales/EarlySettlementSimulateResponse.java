package com.soma.mfinance.core.contract.service.aftersales;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import org.seuksa.frmk.tools.amount.Amount;

public class EarlySettlementSimulateResponse implements Serializable {
	
	private static final long serialVersionUID = 4146157822425914186L;
	
	private Long cotraId;
	private Date eventDate;
	
	private Amount balanceCapital;
	private Amount balanceInterest;
	private Amount balancePenalty;
	private Amount balanceFollowingFee;
	private Amount balanceRepossessionFee;
	private Amount balanceCollectionFee;
	private Amount balanceOperationFee;
	private Amount balancePressingFee;
	private Amount balanceTransferFee;
	private Amount adjustmentInterest;
	private List<Cashflow> cashflows;

	private Amount totalOther;
	private Amount totalPenalty;
	private Amount insuranceFee;
	private Amount servicingFee;
	private Amount vatInsurance;
	private Amount vatService;
	private Amount vatPenalty;

	public Amount getTotalOther() {
		return totalOther;
	}

	public void setTotalOther(Amount totalOther) {
		this.totalOther = totalOther;
	}

	public Amount getTotalPenalty() {
		return totalPenalty;
	}

	public void setTotalPenalty(Amount totalPenalty) {
		this.totalPenalty = totalPenalty;
	}

	public Amount getInsuranceFee() {
		return insuranceFee;
	}

	public void setInsuranceFee(Amount insuranceFee) {
		this.insuranceFee = insuranceFee;
	}

	public Amount getServicingFee() {
		return servicingFee;
	}

	public void setServicingFee(Amount servicingFee) {
		this.servicingFee = servicingFee;
	}

	public Amount getVatInsurance() {
		return vatInsurance;
	}

	public void setVatInsurance(Amount vatInsurance) {
		this.vatInsurance = vatInsurance;
	}

	public Amount getVatService() {
		return vatService;
	}

	public void setVatService(Amount vatService) {
		this.vatService = vatService;
	}

	public Amount getVatPenalty() {
		return vatPenalty;
	}

	public void setVatPenalty(Amount vatPenalty) {
		this.vatPenalty = vatPenalty;
	}

	/**
	 * @return the cotraId
	 */
	public Long getCotraId() {
		return cotraId;
	}
	
	/**
	 * @param cotraId the cotraId to set
	 */
	public void setCotraId(Long cotraId) {
		this.cotraId = cotraId;
	}

	/**
	 * @return the eventDate
	 */
	public Date getEventDate() {
		return eventDate;
	}

	/**
	 * @param eventDate the eventDate to set
	 */
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	/**
	 * @return the balanceCapital
	 */
	public Amount getBalanceCapital() {
		return balanceCapital;
	}

	/**
	 * @param balanceCapital the balanceCapital to set
	 */
	public void setBalanceCapital(Amount balanceCapital) {
		this.balanceCapital = balanceCapital;
	}

	/**
	 * @return the balanceInterest
	 */
	public Amount getBalanceInterest() {
		return balanceInterest;
	}

	/**
	 * @param balanceInterest the balanceInterest to set
	 */
	public void setBalanceInterest(Amount balanceInterest) {
		this.balanceInterest = balanceInterest;
	}

	/**
	 * @return the balancePenalty
	 */
	public Amount getBalancePenalty() {
		return balancePenalty;
	}

	/**
	 * @param balancePenalty the balancePenalty to set
	 */
	public void setBalancePenalty(Amount balancePenalty) {
		this.balancePenalty = balancePenalty;
	}

	/**
	 * @return the balanceFollowingFee
	 */
	public Amount getBalanceFollowingFee() {
		return balanceFollowingFee;
	}

	/**
	 * @param balanceFollowingFee the balanceFollowingFee to set
	 */
	public void setBalanceFollowingFee(Amount balanceFollowingFee) {
		this.balanceFollowingFee = balanceFollowingFee;
	}

	/**
	 * @return the balanceRepossessionFee
	 */
	public Amount getBalanceRepossessionFee() {
		return balanceRepossessionFee;
	}

	/**
	 * @param balanceRepossessionFee the balanceRepossessionFee to set
	 */
	public void setBalanceRepossessionFee(Amount balanceRepossessionFee) {
		this.balanceRepossessionFee = balanceRepossessionFee;
	}

	/**
	 * @return the balanceCollectionFee
	 */
	public Amount getBalanceCollectionFee() {
		return balanceCollectionFee;
	}

	/**
	 * @param balanceCollectionFee the balanceCollectionFee to set
	 */
	public void setBalanceCollectionFee(Amount balanceCollectionFee) {
		this.balanceCollectionFee = balanceCollectionFee;
	}

	/**
	 * @return the balanceOperationFee
	 */
	public Amount getBalanceOperationFee() {
		return balanceOperationFee;
	}

	/**
	 * @param balanceOperationFee the balanceOperationFee to set
	 */
	public void setBalanceOperationFee(Amount balanceOperationFee) {
		this.balanceOperationFee = balanceOperationFee;
	}

	/**
	 * @return the balancePressingFee
	 */
	public Amount getBalancePressingFee() {
		return balancePressingFee;
	}

	/**
	 * @param balancePressingFee the balancePressingFee to set
	 */
	public void setBalancePressingFee(Amount balancePressingFee) {
		this.balancePressingFee = balancePressingFee;
	}

	/**
	 * @return the balanceTransferFee
	 */
	public Amount getBalanceTransferFee() {
		return balanceTransferFee;
	}

	/**
	 * @param balanceTransferFee the balanceTransferFee to set
	 */
	public void setBalanceTransferFee(Amount balanceTransferFee) {
		this.balanceTransferFee = balanceTransferFee;
	}

	/**
	 * @return the adjustmentInterest
	 */
	public Amount getAdjustmentInterest() {
		return adjustmentInterest;
	}

	/**
	 * @param adjustmentInterest the adjustmentInterest to set
	 */
	public void setAdjustmentInterest(Amount adjustmentInterest) {
		this.adjustmentInterest = adjustmentInterest;
	}

	/**
	 * @return the cashflows
	 */
	public List<Cashflow> getCashflows() {
		return cashflows;
	}

	/**
	 * @param cashflows the cashflows to set
	 */
	public void setCashflows(List<Cashflow> cashflows) {
		this.cashflows = cashflows;
	}

}
