package com.soma.mfinance.core.financial.service;

import com.soma.mfinance.core.financial.model.ManufacturerCompensation;


/**
 * 
 * @author seanglay.chhoeurn
 *
 */
public interface CompensationService {

	/**
	 * 
	 * @param manufacturerCompensation
	 */
	void saveOrUpdateCompensation(ManufacturerCompensation manufacturerCompensation);
}
