package com.soma.mfinance.core.asset.panel.appraisal.item;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * 
 * @author vi.sok
 *
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(AppraisalItemsPanel.NAME)
public class AppraisalItemsPanel extends AbstractTabsheetPanel implements View {
	
	/** */
	private static final long serialVersionUID = -2963062593873628481L;

	public static final String NAME = "appraisal.items";
	
	@Autowired
	private AppraisalItemTablePanel appraisalItemTablePanel;
	@Autowired
	private AppraisalItemFormPanel appraisalItemFormPanel;
	
	@PostConstruct
	public void PostConstruct() {
		super.init();
		appraisalItemTablePanel.setMainPanel(this);
		appraisalItemFormPanel.setCaption(I18N.message("appraisal.items"));
		getTabSheet().setTablePanel(appraisalItemTablePanel);
	}

	/**
	 * @see View#enter(ViewChangeEvent)
	 */
	@Override
	public void enter(ViewChangeEvent event) {
	}

	/**
	 * @see AbstractTabsheetPanel#onAddEventClick()
	 */
	@Override
	public void onAddEventClick() {
		appraisalItemFormPanel.reset();
		getTabSheet().addFormPanel(appraisalItemFormPanel);
		getTabSheet().setSelectedTab(appraisalItemFormPanel);
	}

	/**
	 * @see AbstractTabsheetPanel#onEditEventClick()
	 */
	@Override
	public void onEditEventClick() {
		getTabSheet().addFormPanel(appraisalItemFormPanel);
		initSelectedTab(appraisalItemFormPanel);
	}

	/**
	 * @see AbstractTabsheetPanel#initSelectedTab(com.vaadin.ui.Component)
	 */
	@Override
	public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
		if (selectedTab == appraisalItemFormPanel) {
			appraisalItemFormPanel.assignValues(appraisalItemTablePanel.getItemSelectedId());
		} else if (selectedTab == appraisalItemTablePanel && getTabSheet().isNeedRefresh()) {
			appraisalItemTablePanel.refresh();
		}
		getTabSheet().setSelectedTab(selectedTab);
	}
}
