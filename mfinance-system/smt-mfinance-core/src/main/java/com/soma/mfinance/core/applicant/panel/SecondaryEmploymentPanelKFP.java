package com.soma.mfinance.core.applicant.panel;


import com.soma.mfinance.core.applicant.model.Employment;
import com.soma.mfinance.core.applicant.panel.address.BusinessScaleIncomePanel;
import com.soma.mfinance.core.application.model.EmploymentOccupation;
import com.soma.mfinance.core.application.model.EmploymentPosition;
import com.soma.mfinance.core.shared.system.AmountBigUtils;
import com.soma.mfinance.core.shared.system.AutoTextField;
import com.soma.mfinance.core.shared.system.BusinessTypes;
import com.soma.mfinance.core.shared.system.WhoseJob;
import com.soma.ersys.core.hr.model.eref.EEmploymentIndustry;
import com.soma.ersys.core.hr.model.eref.EEmploymentStatus;
import com.soma.ersys.core.hr.model.eref.EEmploymentType;
import com.soma.frmk.vaadin.ui.panel.AbstractControlPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.mfinance.core.applicant.panel.address.BusinessScaleIncomePanel;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.ui.*;
import com.vaadin.ui.Notification.Type;
import org.apache.commons.lang.StringUtils;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.MyNumberUtils;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author by kimsuor.seang  on 10/14/2017.
 */
public class SecondaryEmploymentPanelKFP extends AbstractControlPanel {

    private static final long serialVersionUID = 2542460101873663919L;
    private TextField txtPosition;
    private TextField txtNoMonthInYear;
    private AutoTextField txtRevenue;
    private TextField txtTimeWithEmployerInYear;
    private TextField txtTimeWithEmployerInMonth;
    private AutoTextField txtBusinessExpense;
    private ERefDataComboBox<EEmploymentStatus> cbxEmploymentStatus;
    private ERefDataComboBox<EEmploymentIndustry> cbxEmploymentIndustry;
    private EntityRefComboBox<EmploymentOccupation> cbxEmploymentOccupation;
    private EntityRefComboBox<EmploymentPosition> cbxEmploymentPosition;
    private EntityRefComboBox<BusinessTypes> cbxBusinessTypes;
    private EntityRefComboBox<WhoseJob> cbxWhoseJob;
    private BusinessScaleIncomePanel businessScaleIncomePanel;
    private AutoTextField txtNetIncome;

    public SecondaryEmploymentPanelKFP() {
        this("secondaryEmployment");
    }

    public SecondaryEmploymentPanelKFP(String template) {

        cbxBusinessTypes = new EntityRefComboBox();
        cbxBusinessTypes.setRestrictions(new BaseRestrictions<>(BusinessTypes.class));
        cbxBusinessTypes.renderer();
        cbxWhoseJob = new EntityRefComboBox<>();
        cbxWhoseJob.setRestrictions(new BaseRestrictions<>(WhoseJob.class));
        cbxWhoseJob.renderer();

        txtPosition = ComponentFactory.getTextField(false, 150, 250);
        txtTimeWithEmployerInYear = ComponentFactory.getTextField(false, 20, 50);
        txtTimeWithEmployerInMonth = ComponentFactory.getTextField(false, 20, 50);

        cbxEmploymentOccupation = new EntityRefComboBox<>();
        cbxEmploymentOccupation.setRestrictions(new BaseRestrictions<>(EmploymentOccupation.class));
        cbxEmploymentOccupation.renderer();
        cbxEmploymentOccupation.setImmediate(true);
        cbxEmploymentOccupation.setWidth("200px");

        cbxEmploymentPosition = new EntityRefComboBox<>();
        cbxEmploymentPosition.setRestrictions(new BaseRestrictions<>(EmploymentPosition.class));
        cbxEmploymentPosition.renderer();
        cbxEmploymentPosition.setImmediate(true);
        cbxEmploymentOccupation.setWidth("200px");

        txtRevenue = new AutoTextField();
        txtRevenue.setWidth("150");
        txtRevenue.setImmediate(true);
        txtRevenue.addBlurListener(new BlurListener() {
            private static final long serialVersionUID = 8401761385543992446L;

            @Override
            public void blur(BlurEvent event) {
                autoCalculateNetIncome();

            }
        });
        txtNoMonthInYear = ComponentFactory.getTextField(false, 50, 150);
        txtBusinessExpense = new AutoTextField();
        txtBusinessExpense.setWidth("150");
        txtBusinessExpense.setEnabled(true);
        txtBusinessExpense.setValue(AmountBigUtils.format(0d));
        txtBusinessExpense.setImmediate(true);
        txtBusinessExpense.addBlurListener(new BlurListener() {
            private static final long serialVersionUID = 8763877374593000842L;

            @Override
            public void blur(BlurEvent event) {
                autoCalculateNetIncome();

            }
        });
        cbxEmploymentStatus = new ERefDataComboBox<>(EEmploymentStatus.values());
        cbxEmploymentIndustry = new ERefDataComboBox<>(EEmploymentIndustry.class);

        InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
        CustomLayout customLayout = null;
        try {
            customLayout = new CustomLayout(layoutFile);
        } catch (IOException e) {
            Notification.show("Could not locate template " + template, e.getMessage(), Type.ERROR_MESSAGE);
        }
        customLayout.addComponent(new Label(I18N.message("per.year")), "lblPerYear");
        customLayout.addComponent(new Label(I18N.message("per.year")), "lblPerYear1");
        customLayout.addComponent(new Label(I18N.message("per.year")), "lblPerYear2");

        customLayout.addComponent(new Label(I18N.message("occupation")), "lblEmploymentOccupation");
        customLayout.addComponent(cbxEmploymentOccupation, "cbxEmploymentOccupation");

        customLayout.addComponent(new Label(I18N.message("position")), "lblEmploymentPosition");
        customLayout.addComponent(cbxEmploymentPosition, "cbxEmploymentPosition");

        customLayout.addComponent(new Label(I18N.message("no.month.in.year")), "lblNoMonthInYear");
        customLayout.addComponent(txtNoMonthInYear, "txtNoMonthInYear");

        customLayout.addComponent(new Label(I18N.message("total.sales")), "lblRevenue");
        customLayout.addComponent(txtRevenue, "txtRevenue");

        customLayout.addComponent(new Label(I18N.message("time.with.employer")), "lblTimeWithEmployer");
        customLayout.addComponent(new Label(I18N.message("years")), "lblYears");
        customLayout.addComponent(new Label(I18N.message("months")), "lblMonths");

        customLayout.addComponent(txtTimeWithEmployerInYear, "txtTimeWithEmployerInYear");
        customLayout.addComponent(txtTimeWithEmployerInMonth, "txtTimeWithEmployerInMonth");

        customLayout.addComponent(new Label(I18N.message("business.expense")), "lblBusinessExpense");
        customLayout.addComponent(txtBusinessExpense, "txtBusinessExpense");

        customLayout.addComponent(new Label(I18N.message("employment.status")), "lblEmploymentStatus");
        customLayout.addComponent(cbxEmploymentStatus, "cbxEmploymentStatus");

        customLayout.addComponent(new Label(I18N.message("employment.industry")), "lblEmploymentIndustry");
        customLayout.addComponent(cbxEmploymentIndustry, "cbxEmploymentIndustry");

        customLayout.addComponent(new Label(I18N.message("business.types")), "lblBusinessTypes");
        customLayout.addComponent(cbxBusinessTypes, "cbxBusinessTypes");

        customLayout.addComponent(new Label(I18N.message("whose.job")), "lblWhoseJob");
        customLayout.addComponent(cbxWhoseJob, "cbxWhoseJob");

        businessScaleIncomePanel = new BusinessScaleIncomePanel(true, "businessScaleIncomeNoRequriedField");
        txtNetIncome = businessScaleIncomePanel.getTxtNetIncome();
        txtNetIncome.setImmediate(true);
        txtNetIncome.addBlurListener(new BlurListener() {
            private static final long serialVersionUID = -5023781933968810733L;

            @Override
            public void blur(BlurEvent event) {
                autoCalculateTextNetIncome();
            }
        });
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addComponent(customLayout);
        verticalLayout.addComponent(businessScaleIncomePanel);
        verticalLayout.setSpacing(false);
        addComponent(verticalLayout);
    }

    private void autoCalculateNetIncome() {
        double revenue = AmountBigUtils.getValueDouble(txtRevenue);
        double businessExpense = AmountBigUtils.getValueDouble(txtBusinessExpense);
        txtNetIncome.setValue(AmountBigUtils.format(revenue - businessExpense));
        double netIncome = AmountBigUtils.getValueDouble(txtNetIncome);
        double total = revenue - netIncome;
        if (total < 0) {
            total = -total;
        }
        txtBusinessExpense.setValue(AmountBigUtils.format(total));
    }

    private void autoCalculateTextNetIncome() {
        double revenue = AmountBigUtils.getValueDouble(txtRevenue);
        double netIncome = AmountBigUtils.getValueDouble(txtNetIncome);
        double total = revenue - netIncome;
        if (total < 0) {
            total = -total;
        }
        txtBusinessExpense.setValue(AmountBigUtils.format(total));
    }

    public void assignValues(Employment employment) {
        reset();
        cbxEmploymentPosition.setSelectedEntity(employment.getEmploymentPosition());
        txtRevenue.setValue(AmountBigUtils.format(employment.getRevenue()));
        txtTimeWithEmployerInYear.setValue(getDefaultString(employment.getTimeWithEmployerInYear()));
        txtTimeWithEmployerInMonth.setValue(getDefaultString(employment.getTimeWithEmployerInMonth()));
        txtBusinessExpense.setValue(AmountBigUtils.format(employment.getBusinessExpense()));
        if(employment.getRevenue()!=null && employment.getNetIncome() != null){
			txtBusinessExpense.setValue(AmountBigUtils.format(employment.getRevenue()-employment.getNetIncome()));
		}
        cbxEmploymentStatus.setSelectedEntity(employment.getEmploymentStatus());
		cbxEmploymentIndustry.setSelectedEntity(employment.getEmploymentIndustry());
		cbxBusinessTypes.setSelectedEntity(employment.getBusinessTypes());
		cbxWhoseJob.setSelectedEntity(employment.getWhoseJob());
        txtNoMonthInYear.setValue(getDefaultString(employment.getNoMonthInYear()));
        businessScaleIncomePanel.assignValues(employment);
    }

    public Employment getEmployment(Employment employment) {
        if(employment  != null){
            employment.setEmploymentPosition(cbxEmploymentPosition.getSelectedEntity());
            employment.setTimeWithEmployerInYear(MyNumberUtils.getInteger(getInteger(txtTimeWithEmployerInYear)));
            employment.setTimeWithEmployerInMonth(MyNumberUtils.getInteger(getInteger(txtTimeWithEmployerInMonth)));
            employment.setRevenue(MyNumberUtils.getDouble(getDouble(txtRevenue)));
            employment.setBusinessExpense(getDouble(txtBusinessExpense));
            employment.setEmploymentStatus(cbxEmploymentStatus.getSelectedEntity());
            employment.setEmploymentIndustry(cbxEmploymentIndustry.getSelectedEntity());
            employment.setEmploymentOccupation(cbxEmploymentOccupation.getSelectedEntity());
            employment.setEmploymentPosition(cbxEmploymentPosition.getSelectedEntity());
            employment.setEmploymentType(EEmploymentType.SECO);
			employment.setBusinessTypes(cbxBusinessTypes.getSelectedEntity());
			employment.setWhoseJob(cbxWhoseJob.getSelectedEntity());
            employment.setNoMonthInYear(getInteger(txtNoMonthInYear));
            businessScaleIncomePanel.getBusinessScaleIncome(employment);
            return employment;
        }
        return null;
    }

    public void reset() {
        txtPosition.setValue("");
        txtRevenue.setValue("");
        txtTimeWithEmployerInYear.setValue("");
        txtTimeWithEmployerInMonth.setValue("");
        txtBusinessExpense.setValue("");
        txtBusinessExpense.setValue(null);
        cbxEmploymentStatus.setSelectedEntity(null);
        cbxEmploymentIndustry.setSelectedEntity(null);
        cbxBusinessTypes.setSelectedEntity(null);
        cbxWhoseJob.setSelectedEntity(null);
        txtNoMonthInYear.setValue("");
        businessScaleIncomePanel.reset();
    }
}
