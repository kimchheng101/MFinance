package com.soma.mfinance.core.asset.model;

import org.seuksa.frmk.model.eref.BaseERefData;

import javax.persistence.AttributeConverter;
import java.util.List;

/**
 * Created by cheasocheat on 2/25/17.
 */
public class EAssetYear extends BaseERefData implements AttributeConverter<EAssetYear, Long> {

    public final static EAssetYear Y2011 = new EAssetYear("2011",1);
    public final static EAssetYear Y2012 = new EAssetYear("2012",2);
    public final static EAssetYear Y2013 = new EAssetYear("2013",3);
    public final static EAssetYear Y2014 = new EAssetYear("2014",4);
    public final static EAssetYear Y2015 = new EAssetYear("2015",5);
    public final static EAssetYear Y2016 = new EAssetYear("2016",6);
    public final static EAssetYear Y2017 = new EAssetYear("2017",7);

    public EAssetYear(){super();}

    /*
        * @param code
        * @param id
        */

    public EAssetYear(String code, long id){
        super(code,id);
    }

    @Override
    public Long convertToDatabaseColumn(EAssetYear attribute) {
        return super.convertToDatabaseColumn(attribute);
    }

    @Override
    public EAssetYear convertToEntityAttribute(Long dbData) {
        return super.convertToEntityAttribute(dbData);
    }

    /**
     *
     * @return
     */
    public static List<EAssetYear> values() {
        return getValues(EAssetYear.class);
    }

    /**
     *
     * @param code
     * @return
     */
    public static EAssetYear getByCode(String code) {
        return getByCode(EAssetYear.class, code);
    }

    /**
     *
     * @param id
     * @return
     */
    public static EAssetYear getById(long id) {
        return getById(EAssetYear.class, id);
    }

}