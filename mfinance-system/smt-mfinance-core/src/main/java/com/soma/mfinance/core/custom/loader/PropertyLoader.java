package com.soma.mfinance.core.custom.loader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * Created by cheasocheat on 4/26/17.
 */
@Component
//@PropertySource({"classpath:ws-config.properties"})
@PropertySource({"file:${user.home}/mfinance/config/web-service-config.properties"})
public class PropertyLoader {
    @Autowired
    private Environment environment;

    public Environment getEnvironment() {
        return environment;
    }
}
