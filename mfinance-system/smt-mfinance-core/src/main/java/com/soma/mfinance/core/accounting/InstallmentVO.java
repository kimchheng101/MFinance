package com.soma.mfinance.core.accounting;

import java.util.Date;

import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowType;
import com.soma.mfinance.core.financial.model.FinService;

/**
 * 
 * @author vi.sok
 *
 */
public class InstallmentVO {
	private int numInstallment;
	private Date installmentDate;
	private ECashflowType cashflowType;
	private double tiamount;
	private double vatAmount;
	private Contract contract;
	private Date periodStartDate;
	private Date periodEndDate;
	private FinService service;
	private double balance;
	/**
	 * @return the numInstallment
	 */
	public int getNumInstallment() {
		return numInstallment;
	}
	/**
	 * @param numInstallment the numInstallment to set
	 */
	public void setNumInstallment(int numInstallment) {
		this.numInstallment = numInstallment;
	}
	/**
	 * @return the installmentDate
	 */
	public Date getInstallmentDate() {
		return installmentDate;
	}
	/**
	 * @param installmentDate the installmentDate to set
	 */
	public void setInstallmentDate(Date installmentDate) {
		this.installmentDate = installmentDate;
	}
	/**
	 * @return the cashflowType
	 */
	public ECashflowType getCashflowType() {
		return cashflowType;
	}
	/**
	 * @param cashflowType the cashflowType to set
	 */
	public void setCashflowType(ECashflowType cashflowType) {
		this.cashflowType = cashflowType;
	}
	/**
	 * @return the tiamount
	 */
	public double getTiamount() {
		return tiamount;
	}
	/**
	 * @param tiamount the tiamount to set
	 */
	public void setTiamount(double tiamount) {
		this.tiamount = tiamount;
	}
	/**
	 * @return the vatAmount
	 */
	public double getVatAmount() {
		return vatAmount;
	}
	/**
	 * @param vatAmount the vatAmount to set
	 */
	public void setVatAmount(double vatAmount) {
		this.vatAmount = vatAmount;
	}
	/**
	 * @return the contract_old
	 */
	public Contract getContract() {
		return contract;
	}
	/**
	 * @param contract the contract_old to set
	 */
	public void setContract(Contract contract) {
		this.contract = contract;
	}
	/**
	 * @return the periodStartDate
	 */
	public Date getPeriodStartDate() {
		return periodStartDate;
	}
	/**
	 * @param periodStartDate the periodStartDate to set
	 */
	public void setPeriodStartDate(Date periodStartDate) {
		this.periodStartDate = periodStartDate;
	}
	/**
	 * @return the periodEndDate
	 */
	public Date getPeriodEndDate() {
		return periodEndDate;
	}
	/**
	 * @param periodEndDate the periodEndDate to set
	 */
	public void setPeriodEndDate(Date periodEndDate) {
		this.periodEndDate = periodEndDate;
	}
	/**
	 * @return the service
	 */
	public FinService getService() {
		return service;
	}
	/**
	 * @param service the service to set
	 */
	public void setService(FinService service) {
		this.service = service;
	}
	/**
	 * @return the balance
	 */
	public double getBalance() {
		return balance;
	}
	/**
	 * @param balance the balance to set
	 */
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
}
