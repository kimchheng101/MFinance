package com.soma.mfinance.core.address.panel.area;

import com.soma.mfinance.core.address.model.Area;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.ersys.core.hr.model.address.Commune;
import com.soma.ersys.core.hr.model.address.District;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.EntityColumnRenderer;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Area code table panel in collection
 *
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AreaAddressTablePanel extends AbstractTablePanel<Area> implements FMEntityField {

    /** */
    private static final long serialVersionUID = 2777943503384227541L;

    @PostConstruct
    public void PostConstruct() {
        setCaption(I18N.message("areas"));
        setSizeFull();
        setMargin(true);
        setSpacing(true);

        super.init(I18N.message("areas"));
        addDefaultNavigation();
    }

    /**
     * Get Paged definition
     *
     * @return
     */
    @Override
    protected PagedDataProvider<Area> createPagedDataProvider() {
        PagedDefinition<Area> pagedDefinition = new PagedDefinition<Area>(searchPanel.getRestrictions());

        pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition(CODE, I18N.message("code"), String.class, Align.LEFT, 120);
        pagedDefinition.addColumnDefinition(DESC_EN, I18N.message("desc.en"), String.class, Align.LEFT, 120);
        pagedDefinition.addColumnDefinition(DESC, I18N.message("desc"), String.class, Align.LEFT, 120);
        pagedDefinition.addColumnDefinition(PROVINCE + "." + DESC_EN, I18N.message("province"), String.class, Align.LEFT, 200);
        pagedDefinition.addColumnDefinition(DISTRICT, I18N.message("district"), String.class, Align.LEFT, 400, new DistrictColumnRenderer());
        pagedDefinition.addColumnDefinition(COMMUNE, I18N.message("commune"), String.class, Align.LEFT, 400, new CommuneColumnRenderer());
        pagedDefinition.addColumnDefinition(REMARK, I18N.message("remark"), String.class, Align.LEFT, 100);

        EntityPagedDataProvider<Area> pagedDataProvider = new EntityPagedDataProvider<Area>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    /**
     * @see AbstractTablePanel#getEntity()
     */
    @Override
    protected Area getEntity() {
        final Long id = getItemSelectedId();
        if (id != null) {
            return ENTITY_SRV.getById(Area.class, id);
        }
        return null;
    }

    /**
     * @see AbstractTablePanel#createSearchPanel()
     */
    @Override
    protected AreaAddressSearchPanel createSearchPanel() {
        return new AreaAddressSearchPanel(this);
    }

    private class DistrictColumnRenderer extends EntityColumnRenderer {

        @Override
        public Object getValue() {
            List<District> list = ((Area) getEntity()).getDistricts();
            StringBuilder sb = new StringBuilder();
            int i = 0;
            for (District district : list) {
                i++;
                sb.append(district.getDescEn());
                if(i < list.size())
                    sb.append(", ");
            }
            return sb.toString();
        }

    }


    private class CommuneColumnRenderer extends EntityColumnRenderer {

        @Override
        public Object getValue() {
            List<Commune> list = ((Area) getEntity()).getCommunes();
            StringBuilder sb = new StringBuilder();
            int i = 0;
            for (Commune commune : list) {
                i++;
                sb.append(commune.getDescEn());
                if(i < list.size())
                    sb.append(", ");
            }
            return sb.toString();
        }

    }
}
