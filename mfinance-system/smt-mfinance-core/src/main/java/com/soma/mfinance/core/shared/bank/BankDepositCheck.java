package com.soma.mfinance.core.shared.bank;

import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.service.EntityService;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;

import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.payment.model.BankDepositChecked;
import com.soma.mfinance.core.shared.payment.PaymentEntityField;

/**
 * @author kimsuor.seang
 */
public class BankDepositCheck implements PaymentEntityField {
	
	private static EntityService entityService = SpringUtils.getBean(EntityService.class);

	public static List<BankDepositChecked> getBankDepositChecked(Dealer dealer, Date startDate, Date endDate) {
		BaseRestrictions<BankDepositChecked> restrictions = new BaseRestrictions<BankDepositChecked>(BankDepositChecked.class);
		restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
		restrictions.addCriterion(Restrictions.ge("checkedDate", DateUtils.getDateAtBeginningOfDay(startDate)));
		restrictions.addCriterion(Restrictions.le("checkedDate", DateUtils.getDateAtEndOfDay(endDate)));
		return entityService.list(restrictions);
	}
}
