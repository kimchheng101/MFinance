package com.soma.mfinance.core.asset.panel.appraisal.item;

import com.soma.mfinance.core.asset.model.appraisal.AppraisalCategory;
import com.soma.mfinance.core.asset.model.appraisal.AppraisalGroup;
import com.soma.mfinance.core.asset.model.appraisal.AppraisalItem;
import com.soma.mfinance.core.asset.model.appraisal.EAppraisalRange;
import com.soma.mfinance.core.common.reference.model.EComponetType;
import com.soma.mfinance.core.shared.component.ComponentLayoutFactory;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.model.entity.Entity;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author vi.sok
 * @mofity kimsuor.seang date 06/10/2017
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AppraisalItemFormPanel extends AbstractFormPanel {

    private static final long serialVersionUID = -4276155516661087487L;

    private AppraisalItem appraisalItem;

    private CheckBox cbActive;
    private TextField txtCode;
    private TextField txtDescEn;
    private TextField txtValue;
    private TextField txtSortIndex;
    private EntityRefComboBox<AppraisalCategory> cbxAppraisalCategory;
    private CheckBox cbMandetory;
    private ERefDataComboBox<EComponetType> cbxEComponetType;
    private EntityComboBox<AppraisalGroup> cbxAppraisalGroup;
    private CheckBox cbIsVisible;
    private TextField txtCoefficient;
    private TextField txtWieghtCoefficient;
    private ERefDataComboBox<EAppraisalRange> cbxEAppraisalRange;
    private TextField txtFixPrice;
    private TextField txtChangePrice;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addSaveClickListener(this);
    }

    @Override
    protected Entity getEntity() {
        appraisalItem.setSortIndex(getInteger(txtSortIndex));
        appraisalItem.setCode(txtCode.getValue());
        appraisalItem.setDesc(txtDescEn.getValue());
        appraisalItem.setDescEn(txtDescEn.getValue());
        appraisalItem.setAppraisalCategory(cbxAppraisalCategory.getSelectedEntity());
        appraisalItem.setValue(getDouble(txtValue));
        appraisalItem.setStatusRecord(cbActive.getValue() ? EStatusRecord.ACTIV : EStatusRecord.INACT);
        appraisalItem.setMandetory(cbMandetory.getValue());
        appraisalItem.setComponetType(cbxEComponetType.getSelectedEntity());
        appraisalItem.setVisible(cbIsVisible.getValue() ? true : false);
        appraisalItem.setAppraisalGroup(cbxAppraisalGroup.getSelectedEntity());
        appraisalItem.setCoefficient(MyNumberUtils.getDouble(txtCoefficient.getValue(), 0));
        appraisalItem.setWeightCoefficient(MyNumberUtils.getDouble(txtWieghtCoefficient.getValue(), 1));
        appraisalItem.seteAppraisalRange(cbxEAppraisalRange.getSelectedEntity());
        if (txtFixPrice.getValue() != null || txtChangePrice.getValue() != null) {
            appraisalItem.setFixPrice(Double.parseDouble(txtFixPrice.getValue()));
            appraisalItem.setChangePrice(Double.parseDouble(txtChangePrice.getValue()));
        }
        return appraisalItem;
    }

    @Override
    protected com.vaadin.ui.Component createForm() {
        txtSortIndex = ComponentFactory.getTextField("sortIndex", false, 60, 180);
        txtValue = ComponentFactory.getTextField("value", false, 60, 180);
        txtCode = ComponentFactory.getTextField("code", true, 60, 180);
        txtDescEn = ComponentFactory.getTextField("name", true, 60, 180);
        txtWieghtCoefficient = ComponentFactory.getTextField("wieght.coefficient", false, 60, 180);
        cbActive = new CheckBox(I18N.message("active"));
        cbActive.setValue(true);
        cbMandetory = new CheckBox(I18N.message("mandetory"));
        cbMandetory.setValue(true);

        cbIsVisible = new CheckBox(I18N.message("visible"));
        cbIsVisible.setValue(true);

        cbxEAppraisalRange = new ERefDataComboBox<EAppraisalRange>(EAppraisalRange.class);
        cbxEAppraisalRange.setCaption(I18N.message("groupdetail.range"));
        cbxEAppraisalRange.setRequired(true);

        cbxAppraisalCategory = new EntityRefComboBox<>();
        cbxAppraisalCategory.setCaption(I18N.message("appraisal.category"));
        cbxAppraisalCategory.setWidth(170, Unit.PIXELS);
        cbxAppraisalCategory.setRequired(true);

        cbxAppraisalGroup = new EntityComboBox<AppraisalGroup>(AppraisalGroup.class, "descEn");
        cbxAppraisalGroup.setCaption(I18N.message("groupdetail.group"));
        cbxAppraisalGroup.setWidth(170, Unit.PIXELS);
        cbxAppraisalGroup.renderer();

        cbxEComponetType = new ERefDataComboBox<>(EComponetType.class);
        cbxEComponetType.setCaption(I18N.message("componet.type"));
        cbxEComponetType.setRequired(true);

        txtCoefficient = ComponentFactory.getTextField("coefficient", false, 60, 180);

        cbxEAppraisalRange.addValueChangeListener(new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                if (cbxEAppraisalRange.getSelectedEntity() != null) {
                    if (cbxEAppraisalRange.getSelectedEntity().getCode() != null) {
                        BaseRestrictions<AppraisalCategory> restrictions = new BaseRestrictions<>(AppraisalCategory.class);
                        restrictions.addCriterion(Restrictions.eq("appraisalRange", EAppraisalRange.getByCode(cbxEAppraisalRange.getSelectedEntity().getCode())));
                        cbxAppraisalCategory.setRestrictions(restrictions);
                        cbxAppraisalCategory.renderer();
                        txtFixPrice.setValue("");
                        txtFixPrice.setEnabled(false);
                        txtChangePrice.setValue("");
                        txtChangePrice.setEnabled(false);

                        txtValue.setEnabled(true);
                        txtCoefficient.setEnabled(true);
                        txtWieghtCoefficient.setEnabled(true);

                        if (cbxEAppraisalRange.getSelectedEntity().equals(EAppraisalRange.TRACTOR) ||
                                cbxEAppraisalRange.getSelectedEntity().equals(EAppraisalRange.COMBINE_HARVESTER) ||
                                cbxEAppraisalRange.getSelectedEntity().equals(EAppraisalRange.WALKING_TRACTOR)) {
                            cbxAppraisalGroup.setSelectedEntity(null);
                            cbxAppraisalGroup.setEnabled(false);
                            txtFixPrice.setEnabled(true);
                            txtChangePrice.setEnabled(true);

                            txtValue.setValue("");
                            txtValue.setEnabled(false);
                            txtCoefficient.setValue("");
                            txtCoefficient.setEnabled(false);
                            txtWieghtCoefficient.setValue("");
                            txtWieghtCoefficient.setEnabled(false);
                        }
                    }

                }
            }
        });

        cbxAppraisalGroup.addValueChangeListener(new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent valueChangeEvent) {
                if (cbxAppraisalGroup.getSelectedEntity() != null) {
                    txtCoefficient.setValue("0");
                    txtCoefficient.setEnabled(false);
                    txtCoefficient.setRequired(false);
                } else {
                    txtCoefficient.setEnabled(true);
                    txtCoefficient.setRequired(true);
                }
            }
        });
        this.txtFixPrice = ComponentFactory.getTextField("fix.price", false, 60, 180);
        this.txtChangePrice = ComponentFactory.getTextField("change.price", false, 60, 180);

        FormLayout frmLayout = ComponentLayoutFactory.getFormLayoutCaptionAlignLeft();
        frmLayout.setMargin(new MarginInfo(false, false, false, true));
        frmLayout.addComponent(txtCode);
        frmLayout.addComponent(txtDescEn);
        frmLayout.addComponent(cbxEAppraisalRange);
        frmLayout.addComponent(cbxAppraisalCategory);
        frmLayout.addComponent(cbxAppraisalGroup);
        frmLayout.addComponent(txtFixPrice);
        frmLayout.addComponent(txtChangePrice);
        frmLayout.addComponent(txtValue);
        frmLayout.addComponent(txtCoefficient);
        frmLayout.addComponent(txtWieghtCoefficient);
        frmLayout.addComponent(cbxEComponetType);
        frmLayout.addComponent(cbMandetory);
        frmLayout.addComponent(cbIsVisible);
        frmLayout.addComponent(txtSortIndex);
        frmLayout.addComponent(cbActive);

        return frmLayout;
    }

    public void assignValues(Long appraisalItemId) {
        super.reset();
        if (appraisalItemId != null) {
            appraisalItem = ENTITY_SRV.getById(AppraisalItem.class, appraisalItemId);
            txtCode.setValue(appraisalItem.getCode());
            txtDescEn.setValue(appraisalItem.getDescEn());
            AppraisalCategory appraisalCategory = appraisalItem.getAppraisalCategory();
            /*if(appraisalCategory != null){
                AppraisalCategory appraisalCategorySelected = ENTITY_SRV.getById(ApprhaisalCategory.class,appraisalCategory.getId());
				if(appraisalCategorySelected != null){
					cbxAppraisalCategory.setSelectedEntity(appraisalCategorySelected);
				}
			}*/
            cbxEComponetType.setSelectedEntity(appraisalItem.getComponetType());
            cbxAppraisalCategory.setSelectedEntity(appraisalCategory);
            txtValue.setValue(appraisalItem.getValue().toString());
            txtSortIndex.setValue(getDefaultString(appraisalItem.getSortIndex()));
            cbActive.setValue(appraisalItem.getStatusRecord().equals(EStatusRecord.ACTIV));
            cbMandetory.setValue(appraisalItem.getMandetory());
            cbIsVisible.setValue(appraisalItem.getVisible());
            txtCoefficient.setValue(getDefaultString(appraisalItem.getCoefficient()));
            cbxAppraisalGroup.setSelectedEntity(appraisalItem.getAppraisalGroup());
            txtWieghtCoefficient.setValue(getDefaultString(appraisalItem.getWeightCoefficient()));
            if (appraisalItem.getFixPrice() != null) {
                txtFixPrice.setValue(String.valueOf(appraisalItem.getFixPrice()));
            }
            if (appraisalItem.getChangePrice() != null) {
                txtChangePrice.setValue(String.valueOf(appraisalItem.getChangePrice()));
            }
        }
    }

    @Override
    public void reset() {
        super.reset();
        appraisalItem = AppraisalItem.createInstance();
        txtCode.setValue("");
        txtDescEn.setValue("");
        cbxAppraisalCategory.setSelectedEntity(null);
        cbxEComponetType.setSelectedEntity(null);
        txtValue.setValue("0");
        txtCoefficient.setValue("");
        txtSortIndex.setValue("");
        cbActive.setValue(true);
        cbMandetory.setValue(true);
        cbIsVisible.setValue(true);
        cbxAppraisalGroup.setSelectedEntity(null);
        txtWieghtCoefficient.setValue("1");
        cbxEAppraisalRange.setSelectedEntity(null);
        txtFixPrice.setValue("");
        txtChangePrice.setValue("");
    }

    @Override
    protected boolean validate() {
        checkMandatoryField(txtCode, "code");
        checkMandatoryField(txtDescEn, "name.en");
        checkMandatorySelectField(cbxAppraisalCategory, "appraisal.category");
        checkMandatorySelectField(cbxEComponetType, "componet.category");
        checkMandatorySelectField(cbxEAppraisalRange, "groupdetail.range");

        if (cbxEAppraisalRange.getSelectedEntity().equals(EAppraisalRange.TRACTOR) ||
                cbxEAppraisalRange.getSelectedEntity().equals(EAppraisalRange.COMBINE_HARVESTER) ||
                cbxEAppraisalRange.getSelectedEntity().equals(EAppraisalRange.WALKING_TRACTOR)) {
            txtFixPrice.setRequired(true);
            txtChangePrice.setRequired(true);
            txtCoefficient.setRequired(false);
            checkMandatoryField(txtFixPrice, "fix.price");
            checkMandatoryField(txtChangePrice, "change.price");

        }
        if (cbxEAppraisalRange.getSelectedEntity().equals(EAppraisalRange.DREAM) ||
                cbxEAppraisalRange.getSelectedEntity().equals(EAppraisalRange.NONE_DREAM)) {
            txtCoefficient.setRequired(true);
            txtFixPrice.setRequired(false);
            txtChangePrice.setRequired(false);
            checkMandatoryField(txtCoefficient, "coefficient");
        }
        return errors.isEmpty();
    }
}
