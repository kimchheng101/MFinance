package com.soma.mfinance.core.contract.service.aftersales;

import org.seuksa.frmk.service.BaseEntityService;

public interface LossService extends BaseEntityService {

	/**
	 * @param request
	 * @return
	 */
	LossSimulateResponse simulate(LossSimulateRequest request);
	
	/**
	 * @param request
	 * @return
	 */
	LossSaveResponse save(LossSaveRequest request);
	
	/**
	 * @param request
	 * @return
	 */
	void validate(LossValidateRequest request);
	
}
