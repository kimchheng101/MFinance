package com.soma.mfinance.core.applicant.service;

import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.applicant.model.Driver;

/**
 * 
 * @author kimsuor.seang
 */
public interface DriverInformationService extends BaseEntityService {

	/**
	 * 
	 * @param driver
	 */
	void saveOrUpdateDriverAddress(Driver driver);
}
