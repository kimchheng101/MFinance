package com.soma.mfinance.core.auction.panel.sold;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.auction.model.ContractAuctionData;
import com.soma.mfinance.core.auction.service.AuctionService;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.document.panel.DocumentFileUploader;
import com.soma.mfinance.core.workflow.AuctionWkfStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.soma.frmk.vaadin.util.i18n.I18N;
import com.vaadin.ui.*;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.vaadin.dialogs.ConfirmDialog;

import javax.annotation.PostConstruct;

/**
 * AUTHOR : kimsuor.seang
 * EMAIL  : chhaichivon1995@gmail.com
 * DATE   : 8/29/2017
 * TIME   : 10:30 AM
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SoldMotorcycleFormPanel extends AbstractFormPanel implements Button.ClickListener, ConfirmDialog.Listener{

    private Upload upload;
    private DocumentFileUploader uploader;
    private Contract contract;

    private Button btnCancel;
    private EWkfStatus auctionStatus;
    private SoldMotorcyclePanel panel;

    @Autowired
    private AuctionService auctionService;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addSaveClickListener(this);
        navigationPanel.addButton(btnCancel);
    }


    @Override
    protected com.vaadin.ui.Component createForm() {
        btnCancel = new NativeButton(I18N.message("Cancel"));
        btnCancel.addClickListener(this);
        uploader = new DocumentFileUploader("PurchaseAgreement");
        upload = new Upload();
        upload.setReceiver(uploader);
        upload.setCaption(I18N.message("purchase.agreement"));
        upload.addSucceededListener(uploader);

        FormLayout formLayout = new FormLayout();
        formLayout.addComponent(upload);

        return formLayout;
    }

    @Override
    public void buttonClick(Button.ClickEvent event) {
        super.reset();
        Boolean validate = true;
        String statusName = "";

        if (event.getButton() == btnCancel) {
            statusName = I18N.message("cancel");
            auctionStatus = AuctionWkfStatus.WRE;
        }
        if (validate == true) {
            ConfirmDialog confirmDialog = ConfirmDialog.show(
                    UI.getCurrent(),
                    I18N.message("confirm"),
                    I18N.message("confirm.auction", statusName),
                    I18N.message("ok"),
                    I18N.message("cancel"),
                    this);
            confirmDialog.setWidth("400px");
            confirmDialog.setHeight("150px");
            confirmDialog.setImmediate(true);
        } else {
            displayErrors();
        }
    }

    @Override
    protected Entity getEntity() {
        ContractAuctionData contractAuctionData = contract.getContractAuctionData();
        if (contractAuctionData == null) {
            contractAuctionData = new ContractAuctionData();
            contractAuctionData.setContract(contract);
        }

        contractAuctionData.setPurchaseAgreementFile(uploader.getFileNameDoc());

        return contractAuctionData;
    }

    @Override
    public void saveEntity() {
        ENTITY_SRV.saveOrUpdate(getEntity());
        contract = ENTITY_SRV.getById(Contract.class, contract.getId());
    }


    public void assignValues (Long id) {
        reset();
        this.contract = ENTITY_SRV.getById(Contract.class, id);
    }

    @Override
    public void reset() {
        super.reset();

        this.contract = null;
        uploader.setFileNameDoc(null);
    }

    @Override
    public void onClose(ConfirmDialog result) {
        if (result.isConfirmed()) {
            auctionService.changeAuctionStatus(contract, auctionStatus);
            panel.displayTablePanel();
        }
    }

    public void setMainPanel (SoldMotorcyclePanel panel) {
        this.panel = panel;
    }

}
