package com.soma.mfinance.core.asset.model;

import java.util.List;

import javax.persistence.AttributeConverter;

import org.seuksa.frmk.model.eref.BaseERefData;

/**
 * 
 * @author kimsuor.seang
 *
 */
public class EEngineStatus extends BaseERefData implements AttributeConverter<EEngineStatus, Long> {
	private static final long serialVersionUID = 8800717908072047609L;

	public EEngineStatus() {
	}
	
	public EEngineStatus(String code, long id) {
		super(code, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public EEngineStatus convertToEntityAttribute(Long id) {
		return super.convertToEntityAttribute(id);
	}
	
	@Override
	public Long convertToDatabaseColumn(EEngineStatus arg0) {
		return super.convertToDatabaseColumn(arg0);
	}

	public static List<EEngineStatus> values() {
		return getValues(EEngineStatus.class);
	}
	
	public static EEngineStatus getByCode(String code) {
		return getByCode(EEngineStatus.class, code);
	}
	
	public static EEngineStatus getById(long id) {
		return getById(EEngineStatus.class, id);
	}
}
