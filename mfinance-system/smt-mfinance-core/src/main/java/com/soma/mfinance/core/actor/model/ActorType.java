package com.soma.mfinance.core.actor.model;

/**
 * @author kimsuor.seang
 */
public enum ActorType {
	APP,
	DEA,
	FIN,
	BID;
}
