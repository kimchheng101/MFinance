package com.soma.mfinance.core.auction.model;

import com.soma.mfinance.core.asset.model.AssetMake;
import com.soma.mfinance.core.asset.model.AssetModel;
import com.soma.mfinance.core.asset.model.AssetRange;
import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;

/**
 * Created by cheasocheat on 3/23/17.
 */
@Entity
@Table(name = "td_asset_price")
public class AssetPrice extends EntityA{
    private String code;
    private String desc;
    private String descEn;
    private Double price;
    private Double secondHandPrice;
    private AssetMake assetMake;
    private AssetRange assetRange;
    private AssetModel assetModel;


    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ass_pric_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name = "ass_pric_code", unique = false, nullable = true)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "ass_pric_desc", unique = false, nullable = true)
    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Column(name = "ass_pric_desc_en", unique = false, nullable = true)
    public String getDescEn() {
        return descEn;
    }

    public void setDescEn(String descEn) {
        this.descEn = descEn;
    }

    @Column(name = "ass_price", unique = false, nullable = true)
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ass_mak_id")
    public AssetMake getAssetMake() {
        return assetMake;
    }

    public void setAssetMake(AssetMake assetMake) {
        this.assetMake = assetMake;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ass_ran_id")
    public AssetRange getAssetRange() {
        return assetRange;
    }

    public void setAssetRange(AssetRange assetRange) {
        this.assetRange = assetRange;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ass_mod_id")
    public AssetModel getAssetModel() {
        return assetModel;
    }

    public void setAssetModel(AssetModel assetModel) {
        this.assetModel = assetModel;
    }

    @Column(name = "ass_second_hand_price", nullable = true)
    public Double getSecondHandPrice() {
        return secondHandPrice;
    }

    public void setSecondHandPrice(Double secondHandPrice) {
        this.secondHandPrice = secondHandPrice;
    }
}
