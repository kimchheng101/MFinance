package com.soma.mfinance.core.shared.system;

import com.soma.frmk.security.model.ESecPrivilege;
import com.soma.frmk.security.model.SecControlProfilePrivilege;
import com.soma.frmk.security.model.SecProfile;
import org.seuksa.frmk.model.entity.EntityRefA;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ki.kao on 2/25/2017.
 */
public class CustomProfilePrivilege extends EntityRefA {

    private List<SecControlProfilePrivilege> value = null;
    private List<SecControlProfilePrivilege> toDelete = null;

    public CustomProfilePrivilege() {
        value = new ArrayList<>(3);
        toDelete = new ArrayList<>(3);
    }

    @Override
    public Long getId() {
        return value.get(0).getProfile().getId();
    }

    @Override
    public String getDesc() {
        return value.get(0).getProfile().getDesc();
    }

    @Override
    public String getDescEn() {
        return value.get(0).getProfile().getDescEn();
    }


    public void addControlProfilePrivilege(SecProfile secProfile, ESecPrivilege privilege) {
        if (secProfile != null && privilege != null) {
            SecControlProfilePrivilege entity = new SecControlProfilePrivilege();
            entity.setProfile(secProfile);
            entity.setPrivilege(privilege);
            value.add(entity);
        }
    }

    public void addControlProfilePrivilege(SecControlProfilePrivilege privilege) {
        value.add(privilege);
    }

    public void addControlProfilePrivilege(List<SecControlProfilePrivilege> privileges) {
        value = privileges;
    }

    public void removeControlProfilePrivilege(ESecPrivilege privilege) {
        removeControlProfilePrivilege(privilege.getId());
    }

    public void removeControlProfilePrivilege(Long key) {
        for (int i = 0; i < value.size(); i++) {
            if (value.get(i).getPrivilege().getId() == key) {
                toDelete.add(value.get(i));
                value.remove(i);
            }
        }
    }

    public SecControlProfilePrivilege getByPrivilegeId(Long key) {
        for (SecControlProfilePrivilege obj : value)
            if (obj.getPrivilege().getId() == key)
                return obj;
        return null;
    }

    public boolean isExist(Long id) {
        if (getByPrivilegeId(id) != null)
            return true;
        return false;
    }

    public SecProfile getSecProfile() {
        return value.get(0).getProfile();
    }

    public List<SecControlProfilePrivilege> getListPrivilege() {
        return value;
    }

    public List<SecControlProfilePrivilege> getCloneListPrivilege() {
        return new ArrayList<>(value);
    }

    public List<SecControlProfilePrivilege> getListToDelete() {
        return toDelete;
    }
}
