package com.soma.mfinance.core.document.panel;

import com.soma.mfinance.core.custom.vaadin.CustomDownloadStream;
import com.soma.mfinance.core.shared.conf.AppConfig;
import com.vaadin.server.DownloadStream;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * @author kimsuor.seang
 */
public class DisplayDocumentPanel extends Window {

    private static final long serialVersionUID = 7212903972039140830L;
    private static final Logger LOGGER = LoggerFactory.getLogger(DisplayDocumentPanel.class);

    private String caption;
    private File file;

    public DisplayDocumentPanel(String filePathName) {
        this("", filePathName);
    }

    public DisplayDocumentPanel(String caption, String filePathName) {
        super(caption);
        setModal(true);
        setResizable(true);
        setWidth("800");
        setHeight("600");
        center();
        this.caption = caption;
        String tmpDir = "";

        if (filePathName.indexOf("tmp_") != -1)
            tmpDir = AppConfig.getInstance().getConfiguration().getString("specific.tmpdir");
        else
            tmpDir = AppConfig.getInstance().getConfiguration().getString("document.path");
        file = new File(tmpDir + "/" + filePathName);

    }

    /**
     * Display document
     */
    public void display() {
        StreamSource source = null;
        if (source == null) {
            source = new StreamSource() {
                @Override
                public InputStream getStream() {
                    FileInputStream is;
                    try {
                        is = new FileInputStream(file);
                    } catch (FileNotFoundException e) {
                        LOGGER.error("=================> FileNotFoundException : file ", e);
                        is = null;
                    }
                    return is;
                }
            };
        }

        StreamResource streamResource = new StreamResource(source, file.getName()) {
            @Override
            public DownloadStream getStream() {
                final StreamSource ss = getStreamSource();
                if (ss == null) {
                    return null;
                }
                final DownloadStream ds = new CustomDownloadStream(ss.getStream(),
                        getMIMEType(), getFilename(), (int) file.length());
                ds.setBufferSize(getBufferSize());
                ds.setCacheTime(DownloadStream.DEFAULT_CACHETIME);
                return ds;
            }
        };

        BrowserFrame browserFrame = new BrowserFrame(caption);
        browserFrame.setSource(streamResource);
        browserFrame.setSizeFull();
        browserFrame.setImmediate(true);
        this.setContent(browserFrame);
        UI.getCurrent().addWindow(this);
    }

}
