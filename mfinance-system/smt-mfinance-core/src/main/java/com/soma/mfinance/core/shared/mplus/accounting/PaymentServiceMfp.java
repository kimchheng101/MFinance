/**
 * 
 */
package com.soma.mfinance.core.shared.mplus.accounting;

import java.util.Date;
import java.util.List;

import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.payment.model.EPaymentMethod;
import com.soma.mfinance.core.payment.model.Payment;

/**
 * @author vi.sok
 *
 */
public interface PaymentServiceMfp extends BaseEntityService {
	/**
	 * 
	 * @param cashflows
	 * @param paymentDate
	 * @param penaltyDay
	 * @param dealer
	 * @param paymentMethod
	 * @return
	 */
	Payment createPayment(List<Cashflow> cashflows, Date paymentDate, int penaltyDay, Dealer dealer, EPaymentMethod paymentMethod);
	/**
	 * 
	 * @param cashflows
	 * @param paymentDate
	 * @param penaltyDay
	 * @param dealer
	 * @param paymentMethod
	 * @return
	 */
	Payment createPaidOffPayment(List<Cashflow> cashflows, Date paymentDate, int penaltyDay, Dealer dealer, EPaymentMethod paymentMethod);
	/**
	 * 
	 * @param payment
	 */
	void cancelInstallment(Payment payment);
}
