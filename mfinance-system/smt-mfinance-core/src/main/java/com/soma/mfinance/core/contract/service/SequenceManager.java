package com.soma.mfinance.core.contract.service;

import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.tools.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.frmk.config.model.SettingConfig;

/**
 * Sequence manager
 * @author kimsuor.seang
 *
 */
public final class SequenceManager implements FinServicesHelper {

	private final static String CONREF = "CONREF";
	private final static String LOCKSPLITREF = "CONREF";
	private final static String APPLICANTREF = "APPREF";
	private final static String CONREF_MFP = "CONREF_MFP";
	private final static String QUOREF_MFP = "QUOREF_MFP";
	private final static String QUOREF_MFP_PUB = "QUOREF_MFP_PUB";

	private Logger LOG = LoggerFactory.getLogger(SequenceManager.class);
	
	private Long sequenceContract = 0l;
	private Long sequenceLockSplit = 0l;
	private Long sequenceApplicant = 0l;
	private Long sequenceContractMfp = 0l;
	private Long sequenceQuotationMfp = 0l;
	private Long sequenceQuotationMfpPub = 0l;

	private SequenceManager() {}
	
	/** Holder */
	private static class SingletonHolder {
		private final static SequenceManager instance = new SequenceManager();
	}
	 
	/**
	 * @return
	 */
	public static SequenceManager getInstance() {
		return SingletonHolder.instance;
	}

	/**
	 * @return the sequenceContract
	 */
	public Long getSequenceContract() {
		synchronized (sequenceContract) {			
			String yearLabel = DateUtils.getDateLabel(DateUtils.today(), "yy");
			SettingConfig setting = CONT_SRV.getByCode(SettingConfig.class, CONREF + yearLabel);
			if (setting == null) {
				setting = new SettingConfig();
				setting.setStatusRecord(EStatusRecord.ACTIV);
				setting.setDesc("Reference contract_old running number");
				setting.setReadOnly(false);
				setting.setCode(CONREF + yearLabel);
				sequenceContract = 0l;
			} else {
				sequenceContract = Long.parseLong(setting.getValue());
			}
			sequenceContract++;
			setting.setValue(String.valueOf(sequenceContract));
			LOG.debug(">> saveOrUpdate SettingConfig contract_old running number");
			CONT_SRV.saveOrUpdate(setting);
			LOG.debug("<< saveOrUpdate SettingConfig contract_old running number");
			
		}
		return sequenceContract;
	}
	
	/**
	 * @return the sequenceContract
	 */
	public Long getSequenceLockSplit() {
		synchronized (this.sequenceLockSplit) {
			SettingConfig setting = CONT_SRV.getByCode(SettingConfig.class, LOCKSPLITREF);
			if (setting == null) {
				setting = new SettingConfig();
				setting.setStatusRecord(EStatusRecord.ACTIV);
				setting.setDesc("Lock split running number");
				setting.setReadOnly(false);
				setting.setCode(LOCKSPLITREF);
				setting.setValue(String.valueOf(1));
				sequenceLockSplit = 0l;
			} else {
				sequenceLockSplit = Long.parseLong(setting.getValue());
			}
			sequenceLockSplit++;
			setting.setValue(String.valueOf(sequenceLockSplit));
			LOG.debug(">> saveOrUpdate SettingConfig Lock Split");
			CONT_SRV.saveOrUpdate(setting);
			LOG.debug("<< saveOrUpdate SettingConfig Lock Split");
		}
		return sequenceLockSplit;
	}
	
	/**
	 * @return
	 */
	public Long getSequenceApplicant() {
		synchronized (this.sequenceApplicant) {
			SettingConfig setting = CONT_SRV.getByCode(SettingConfig.class, APPLICANTREF);
			if (setting == null) {
				setting = new SettingConfig();
				setting.setStatusRecord(EStatusRecord.ACTIV);
				setting.setDesc("Applicant running number");
				setting.setReadOnly(false);
				setting.setCode(APPLICANTREF);
				setting.setValue(String.valueOf(1));
				sequenceApplicant = 0l;
			} else {
				sequenceApplicant = Long.parseLong(setting.getValue());
			}
			sequenceApplicant++;
			setting.setValue(String.valueOf(sequenceApplicant));
			LOG.debug(">> saveOrUpdate SettingConfig Applicant");
			CONT_SRV.saveOrUpdate(setting);
			LOG.debug("<< saveOrUpdate SettingConfig Applicant");
		}
		return sequenceApplicant;
	}

	/**
	 * @return the sequenceContract
	 */
	public Long getSequenceContractMfp() {
		synchronized (sequenceContractMfp) {
			sequenceContractMfp = getSequenceNumByCode(CONREF_MFP,"Reference contract_old running number (MFP)",sequenceContractMfp);
		}
		return sequenceContractMfp;
	}

	/**
	 * @return the sequenceContract
	 */
	public Long getSequenceQuotationMfp() {
		synchronized (sequenceQuotationMfp) {
			sequenceQuotationMfp = getSequenceNumByCode(QUOREF_MFP,"Reference quotation running number (MFP)",sequenceQuotationMfp);
		}
		return sequenceQuotationMfp;
	}

	/**
	 * @return the sequenceContract
	 */
	public Long getSequenceQuotationMfpPub() {
		synchronized (sequenceQuotationMfpPub) {
			sequenceQuotationMfpPub = getSequenceNumByCode(QUOREF_MFP_PUB,"Reference quotation running number (MFP)",sequenceQuotationMfpPub);
		}
		return sequenceQuotationMfpPub;
	}

	public Long  getSequenceNumByCode(String code,String desc,Long seqNum) {
		SettingConfig setting = CONT_SRV.getByCode(SettingConfig.class, code);
		if (setting == null) {
			setting = new SettingConfig();
			setting.setStatusRecord(EStatusRecord.ACTIV);
			setting.setDesc(desc);
			setting.setReadOnly(false);
			setting.setCode(code);
			setting.setValue(String.valueOf(1));
			seqNum = 0l;
		} else {
			seqNum = Long.parseLong(setting.getValue());
		}
		seqNum++;
		setting.setValue(String.valueOf(seqNum));
		LOG.debug(">> saveOrUpdate SettingConfig Lock Split");
		CONT_SRV.saveOrUpdate(setting);
		LOG.debug("<< saveOrUpdate SettingConfig Lock Split");

		return  seqNum;
	}

}
