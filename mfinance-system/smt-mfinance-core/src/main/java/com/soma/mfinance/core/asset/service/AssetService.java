package com.soma.mfinance.core.asset.service;

import java.util.List;

import com.soma.mfinance.core.asset.model.appraisal.Appraisal;
import com.soma.mfinance.core.asset.model.appraisal.AssetAppraisal;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.asset.model.appraisal.AssetAppraisal;
import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.shared.exception.ValidationFieldsException;

/**
 * Customer service interface
 * @author kimsuor.seang
 *
 */
public interface AssetService extends BaseEntityService {

	/**
	 * Save or update an masset
	 * @param asset
	 * @return
	 */
	Asset saveOrUpdateAsset(Asset asset);
	
	/**
	 * Control masset
	 * @param asset
	 */
	void checkAsset(Asset asset) throws ValidationFieldsException;
	
	/**
	 * @param chassisNumber
	 * @return
	 */
	List<Asset> getAssetsByChassisNumber(String chassisNumber);
	
	/**
	 * @param enginNumber
	 * @return
	 */
	List<Asset> getAssetsByEnginNumber(String enginNumber);
	
	/**
	 * @param enginNumber
	 * @param asset
	 * @return
	 */
	boolean isChassisNumberExist(String chassisNumber, Asset asset);
	
	/**
	 * @param enginNumber
	 * @param asset
	 * @return
	 */
	boolean isEnginNumberExist(String enginNumber, Asset asset);
	
	/**
	 * 
	 * @param plateNumber
	 * @return
	 */
	Asset getAssetByPlateNumber(String plateNumber);
	
	/**
	 * 
	 * @param contract
	 * @return
	 */
	String getAssetStatus(Contract contract);

	void saveOrUpdateAppraisal(AssetAppraisal assetAppraisal) ;
	
}
