package com.soma.mfinance.core.contract.service.impl;

import com.soma.mfinance.core.quotation.SequenceGenerator;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/24/2017
 * Name  : 11:52 AM
 * Email : k.seang@gl-f.com
 */
public class ContractSequenceMfpImpl implements SequenceGenerator {

    private String year;
    private Long sequence;
    private String prefix;

    /**
     *
     * @param prefix
     * @param type
     * @param sequence
     */
    public ContractSequenceMfpImpl(String prefix, String type, Long sequence) {
        this.prefix = prefix;
        this.year = type;
        this.sequence = sequence;
    }

    /**
     * @see com.soma.mfinance.core.quotation.SequenceGenerator#generate()
     */
    @Override
    public String generate() {
        String sequenceNumber = "70000000" + sequence;
        return prefix + "" + year + "" + sequenceNumber.substring(sequenceNumber.length() - 8);
    }
}
