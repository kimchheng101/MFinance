package com.soma.mfinance.core.quotation;

import java.util.List;

import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.service.BaseEntityService;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationStatusHistory;

/**
 * Quotation service interface
 * @author kimsuor.seang
 *
 */
public interface ProcessTimeService extends BaseEntityService {

	/**
	 * @param from
	 * @param to
	 * @param restrictions
	 * @return
	 */
	List<QuotationStatusHistory> getConsolidateQuotationStatusHistories(EWkfStatus from, EWkfStatus to, 
			BaseRestrictions<Quotation> restrictions);	
}
