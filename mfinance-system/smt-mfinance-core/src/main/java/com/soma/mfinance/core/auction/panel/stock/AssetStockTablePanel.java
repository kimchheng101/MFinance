package com.soma.mfinance.core.auction.panel.stock;

import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.EntityColumnRenderer;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Stock Table Panel
 *
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AssetStockTablePanel extends AbstractTablePanel<Contract> implements FMEntityField {

    private static final long serialVersionUID = 3730996332444515248L;

    @PostConstruct
    public void PostConstruct() {

        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addEditClickListener(this);
        navigationPanel.addRefreshClickListener(this);

        setCaption(I18N.message("stocks"));
        setSizeFull();
        setMargin(true);
        setSpacing(true);

        super.init("");
    }

    @Override
    protected PagedDataProvider<Contract> createPagedDataProvider() {
        PagedDefinition<Contract> pagedDefinition = new PagedDefinition<Contract>(searchPanel.getRestrictions());

        pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("auctionStatus.desc", I18N.message("auction.status"), String.class, Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("reference", I18N.message("contract.reference"), String.class, Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("financialProduct" + "." + DESC_EN, I18N.message("financial.product"), String.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("storageLocation", I18N.message("storage.location"), String.class, Align.LEFT, 130, new EStoreLocationColumnRenderer());
        pagedDefinition.addColumnDefinition("stockStatus", I18N.message("collection.stock.status"), String.class, Align.LEFT, 150, new EStockStatusColumnRenderer());

        EntityPagedDataProvider<Contract> pagedDataProvider = new EntityPagedDataProvider<Contract>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    @Override
    protected Contract getEntity() {
        final Long id = getItemSelectedId();
        if (id != null) {
            return ENTITY_SRV.getById(Contract.class, id);
        }
        return null;
    }

    @Override
    protected AbstractSearchPanel<Contract> createSearchPanel() {

        return new AssetStockSearchPanel(this);

    }

    private class EStoreLocationColumnRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            return ((Contract) getEntity()).getStorageLocation().getDesc();
        }
    }

    private class EStockStatusColumnRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            return ((Contract) getEntity()).getStockStatus().getDescEn();
        }
    }

}
