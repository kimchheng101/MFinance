package com.soma.mfinance.core.collection.service.impl;

import com.soma.mfinance.core.collection.model.ContractFlag;
import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soma.mfinance.core.collection.model.ContractFlag;
import com.soma.mfinance.core.collection.service.ContractFlagService;
import com.soma.mfinance.core.contract.model.Contract;

/**
 * Contract Flag Service
 * @author kimsuor.seang
 */       
@Service("contractFlagService")
public class ContractFlagServiceImpl extends BaseEntityServiceImpl implements ContractFlagService {
	
	/** */
	private static final long serialVersionUID = -8289733967836096833L;
	
	@Autowired
    private EntityDao dao;
	
	/**
	 * @see org.seuksa.frmk.service.impl.BaseEntityServiceImpl#getDao()
	 */
	@Override
	public BaseEntityDao getDao() {
		return dao;
	}
	
	/**
	 * @see com.soma.mfinance.core.collection.service.ContractFlagService#saveOrUpdateLegalCase(ContractFlag)
	 */
	@Override
	public void saveOrUpdateLegalCase(ContractFlag contractFlag) {
		update(contractFlag.getContract());
		saveOrUpdate(contractFlag);
	}
	
	/**
	 * @see com.soma.mfinance.core.collection.service.ContractFlagService#withdrawLegalCase(ContractFlag)
	 */
	@Override
	public void withdrawLegalCase(ContractFlag contractFlag) {
		Contract contract = contractFlag.getContract();
		if (contract != null) {
			contract.setWkfSubStatus(null);
			update(contract);
		}
		delete(contractFlag);
	}
	
}
