package com.soma.mfinance.core.asset.service;

import java.util.List;

import com.soma.mfinance.core.asset.model.AssetModel;
import com.soma.mfinance.core.asset.model.AssetRange;
import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.asset.model.AssetModel;
import com.soma.mfinance.core.asset.model.AssetRange;

/**
 * Asset model service interface
 * @author kimsuor.seang
 */
public interface AssetModelService extends BaseEntityService {

	/**
	 * saveOrUpdate Asset Model
	 * @param assetModel
	 */
	void saveOrUpdateAssetModel(AssetModel assetModel);
	
	/**
	 * 
	 * @param assetRange
	 * @return
	 */
	List<AssetModel> getAssetModelsByAssetRange(AssetRange assetRange);
	
}
