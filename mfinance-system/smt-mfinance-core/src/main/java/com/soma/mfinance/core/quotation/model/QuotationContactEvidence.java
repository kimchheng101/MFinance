package com.soma.mfinance.core.quotation.model;

import com.soma.mfinance.core.document.model.DocumentConfirmEvidence;
import com.soma.mfinance.core.document.model.DocumentUwGroup;
import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;

/**
 * @author kimsuor.seang
 */
@Entity
@Table(name = "td_quotation_contact_evidence")
public class QuotationContactEvidence extends EntityA {

	private static final long serialVersionUID = -6559919455976817015L;
	
	private Quotation quotation;
	private DocumentUwGroup documentUwGroup;
	private DocumentConfirmEvidence documentConfirmEvidence;
		
	/**
     * @return id.
     */
    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "qucev_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

	/**
	 * @return the documentConfirmEvidence
	 */
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doc_con_evi_id")
	public DocumentConfirmEvidence getDocumentConfirmEvidence() {
		return documentConfirmEvidence;
	}

	/**
	 * @param documentConfirmEvidence the confirmEvidence to set
	 */
	public void setDocumentConfirmEvidence(DocumentConfirmEvidence documentConfirmEvidence) {
		this.documentConfirmEvidence = documentConfirmEvidence;
	}

	/**
	 * @return the quotation
	 */
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "quo_id")
	public Quotation getQuotation() {
		return quotation;
	}

	/**
	 * @param quotation the quotation to set
	 */
	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}

	/**
	 * @return the documentUwGroup
	 */
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doc_uw_grp_id")
	public DocumentUwGroup getDocumentUwGroup() {
		return documentUwGroup;
	}

	/**
	 * @param documentUwGroup the documentUwGroup to set
	 */
	public void setDocumentUwGroup(DocumentUwGroup documentUwGroup) {
		this.documentUwGroup = documentUwGroup;
	}

}
