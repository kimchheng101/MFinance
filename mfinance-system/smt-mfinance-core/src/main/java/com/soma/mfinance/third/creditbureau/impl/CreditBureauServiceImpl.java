package com.soma.mfinance.third.creditbureau.impl;

import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.third.conf.ModuleConfig;
import com.soma.mfinance.third.creditbureau.CreditBureauService;
import com.soma.mfinance.third.creditbureau.cbc.service.CBCService;
import com.soma.mfinance.third.creditbureau.exception.ErrorCreditBureauException;
import com.soma.mfinance.third.creditbureau.exception.InvokedCreditBureauException;
import com.soma.mfinance.third.creditbureau.exception.ParserCreditBureauException;

/**
 * CBC Service
 * @author kimsuor.seang
 */
@Service("creditBureauService")
public class CreditBureauServiceImpl implements CreditBureauService {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private CBCService cbcService;
	
	/**
	 * Call credit bureau enquiry service
	 * @param quotation
	 * @param parameters
	 * @return
	 */
	public String enquiry(Quotation quotation, Map<String, Object> parameters) throws InvokedCreditBureauException {
		Configuration config = ModuleConfig.getInstance().getConfiguration();
		String strategy = config.getString("creditbureau.strategy");
		logger.debug("Strategy used by Credit Bureau [" + strategy + "]");
		String response = "";
		if ("CBC".equals(strategy)) {
			response = cbcService.enquiry(quotation, parameters);
		}
		return response;
	}	
	
	/**
	 * Validate the result of credit bureau
	 * @param response
	 * @throws InvokedCreditBureauException
	 * @throws ErrorCreditBureauException
	 * @throws ParserCreditBureauException
	 */
	public boolean validateResponse(String response) throws InvokedCreditBureauException, ErrorCreditBureauException, ParserCreditBureauException {
		return cbcService.validateResponse(response);
	}

	@Override
	public String getReference(String response) throws InvokedCreditBureauException {
		return cbcService.getReference(response);
	}
}
