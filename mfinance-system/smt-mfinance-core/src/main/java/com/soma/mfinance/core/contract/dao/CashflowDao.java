package com.soma.mfinance.core.contract.dao;

import org.seuksa.frmk.dao.BaseEntityDao;

/**
 * Cashflow data model access
 * @author kimsuor.seang
 *
 */
public interface CashflowDao extends BaseEntityDao {

}
