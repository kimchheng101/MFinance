package com.soma.mfinance.glf.report.xls;

import com.soma.mfinance.core.accounting.InstallmentVO;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowType;
import com.soma.mfinance.core.financial.model.EServiceType;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationService;
import com.soma.mfinance.core.shared.conf.AppConfig;
import com.soma.mfinance.core.shared.report.ReportParameter;
import com.soma.ersys.core.finance.model.eref.ECurrency;
import com.soma.ersys.core.hr.model.address.Address;
import com.soma.finance.services.shared.AmortizationSchedules;
import com.soma.finance.services.shared.CalculationParameter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyMathUtils;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.amount.Amount;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/24/2017
 * Name  : 4:54 PM
 * Email : k.seang@gl-f.com
 */
public class PaymentScheduleMfp extends PaymentSchedule implements PaymentScheduleMfpFields, FinServicesHelper {

    private XSSFSheet sheet = null;

    /** */
    public PaymentScheduleMfp() {
        try {
            createWorkbook(null);
            sheet = wb.createSheet("Payment Schedule");
            // Lock excel sheet
            sheet.protectSheet("abcd56abcd");
            sheet.lockDeleteColumns();
            sheet.lockDeleteRows();
            sheet.lockFormatCells();
            sheet.lockFormatColumns();
            sheet.lockFormatRows();
            sheet.lockInsertColumns();
            sheet.lockInsertRows();
            sheet.lockSelectLockedCells();
            sheet.lockSelectUnlockedCells();

            sheet.setFitToPage(true);
            sheet.setColumnBreak(11);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String generate(ReportParameter reportParameter) throws Exception {
        String fileName = "";
        Long quotaId = (Long) reportParameter.getParameters().get("quotaId");
        Quotation quotation = quotationService.getById(Quotation.class, quotaId);

        term = quotation.getTerm();
        QuotationService quotationServiceInsurance = quotation.getQuotationServiceByServiceType(EServiceType.INSFEE);
        QuotationService quotationServiceFee = quotation.getQuotationServiceByServiceType(EServiceType.SRVFEE);
        insuranceFee = getServiceFeeAmount(quotationServiceInsurance);
        // servicingFee = getServiceFeeAmount(quotationServiceFee);
        vatInstallAmount = MyMathUtils.roundAmountTo(quotation.getVatInstallmentAmount());
        CalculationParameter calculationParameter = new CalculationParameter();
        calculationParameter.setFrequency(quotation.getFrequency());
        calculationParameter.setInitialPrincipal(quotation.getTiFinanceAmount());
        calculationParameter.setNumberOfPeriods(term);
        calculationParameter.setPeriodicInterestRate(quotation.getInterestRate() / 100);
        calculationParameter.setNumberOfPrincipalGracePeriods(MyNumberUtils.getInteger(quotation.getNumberOfPrincipalGracePeriods()));
        Amount installmentAmount = MyMathUtils.calculateFromAmountIncl(financeCalculationService.getInstallmentPayment(calculationParameter), quotation.getVatValue(), 2);
        calculationParameter.setInstallmentAmount(installmentAmount.getTiAmount());

        Date contractStartDate = quotation.getContractStartDate();
        if (contractStartDate == null) {
            contractStartDate = DateUtils.today();
        }

        Date firstPaymentDate = quotation.getFirstDueDate();
        if (firstPaymentDate == null) {
            firstPaymentDate = DateUtils.today();
        }

        AmortizationSchedules amortizationSchedules = financeCalculationService.getAmortizationSchedules(contractStartDate, firstPaymentDate, calculationParameter);
        Applicant applicant = quotation.getApplicant();
        createHeader();
        CellStyle style = wb.createCellStyle();
        int iRow = 6;
        iRow = createCustomerInfo(quotation, iRow);
        iRow = createVerticalInfo(iRow, style, quotation);

        Map<Integer, List<InstallmentVO>> mapInstallmentVOs = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(quotation, firstPaymentDate);
        iRow++;

        iRow = createPaymentTableHeader(sheet, iRow, style);
        for (Integer nbInstallment : mapInstallmentVOs.keySet()) {
            if (nbInstallment == 13 || nbInstallment == 25) {
                iRow = createPaymentTableHeader(sheet, iRow, style);
            }
            iRow = createPaymentTable(sheet, iRow, style, mapInstallmentVOs.get(nbInstallment));
        }
        iRow = iRow + 3;
        iRow = footer(sheet, iRow, style, quotation);
        fileName = writeXLSData(PAYMENT_FILENAME + ((quotation.getDealer() == null) ? "" : quotation.getDealer().getCode()) + "_" + DateUtils.getDateLabel(DateUtils.today(), "yyyyMMddHHmmssSSS") + ".xlsx");

        return fileName;
    }

    private int createCustomerInfo(Quotation quotation, int iRow) {

        int iCol = 0;
        Applicant applicant = quotation.getApplicant();
        Address applicantAddress = applicant.getIndividual().getMainAddress();
        final Row rspCusNameRow = sheet.createRow(iRow++);

        iCol = iCol + 1;
        createCell(rspCusNameRow, iCol, RSP_CUS_NAME, 12, false, false, CellStyle.ALIGN_LEFT, false, BG_WHITE, FC_BLACK);

        iCol = iCol + 2;
        createCell(rspCusNameRow, iCol, applicant.getIndividual().getLastName() + " " + applicant.getIndividual().getFirstName(), 11, false, false, CellStyle.ALIGN_LEFT, false, BG_WHITE, FC_BLUE);

        String address1 = this.buildFirstLineAddress(applicantAddress);
        String address2 = this.buildSecondLineAddress(applicantAddress);

        iCol = 1;
        final Row rspCusAddress1Row = sheet.createRow(iRow++);
        createCell(rspCusAddress1Row, iCol, RSP_CUS_ADDRESS, 12, false, false, CellStyle.ALIGN_LEFT, false, BG_WHITE, FC_BLACK);

        iCol = iCol + 2;
        createCell(rspCusAddress1Row, iCol, address1, 11, false, false, CellStyle.ALIGN_LEFT, false, BG_WHITE, FC_BLUE);

        final Row rspCusAddress2Row = sheet.createRow(iRow++);
        iCol = 3;
        createCell(rspCusAddress2Row, iCol, address2, 11, false, false, CellStyle.ALIGN_LEFT, false, BG_WHITE, FC_BLUE);

        iCol = iCol + 3;
        createCell(rspCusAddress2Row, iCol, RSP_DEALER_NUMBER, 11, false, false, CellStyle.ALIGN_LEFT, false, BG_WHITE, FC_BLACK);
        iCol = iCol + 1;
        createNumericCell(rspCusAddress2Row, iCol, (quotation.getDealer() == null) ? "" : quotation.getReference().substring(4, 10), false, CellStyle.ALIGN_LEFT, 11, false, true, BG_WHITE, FC_BLUE);

        iCol = 1;
        final Row rspCusPhoneNumberRow = sheet.createRow(iRow++);
        createCell(rspCusPhoneNumberRow, iCol, RSP_CUS_PHONENUMBER, 12, false, false, CellStyle.ALIGN_LEFT, false, BG_WHITE, FC_BLACK);
        iCol = iCol + 2;
        // Phone number value
        createCell(rspCusPhoneNumberRow, iCol, applicant.getIndividual().getMobilePerso(), 11, false, false, CellStyle.ALIGN_LEFT, false, BG_WHITE, FC_BLUE);

        iCol = iCol + 3;
        createCell(rspCusPhoneNumberRow, iCol, RSP_LID_NUMBER, 11, false, false, CellStyle.ALIGN_LEFT, false, BG_WHITE, FC_BLACK);
        iCol = iCol + 1;
        createNumericCell(rspCusPhoneNumberRow, iCol, quotation.getReference(), false, CellStyle.ALIGN_LEFT, 11, false, true, BG_WHITE, FC_BLUE);

        iCol = 1;
        final Row rspCompanyCodeRow = sheet.createRow(iRow++);
        createNumericCell(rspCompanyCodeRow, iCol, RSP_COM_CODE, false, CellStyle.ALIGN_LEFT, 11, false, true, BG_WHITE, FC_BLACK);
       /* value of company code*/
        iCol = iCol + 2;
        createNumericCell(rspCompanyCodeRow, iCol, "6777 (បង់តាមវីង ទ្រូម៉ាន់នី ឬអ៊ីម៉ាន់នី)", false, CellStyle.ALIGN_LEFT, 11, false, true, BG_WHITE, FC_BLUE);

        iCol = 6;
        createNumericCell(rspCompanyCodeRow, iCol, RSP_CUS_CODE, false, CellStyle.ALIGN_LEFT, 11, false, true, BG_WHITE, FC_BLACK);
        /*value of customer code*/
        iCol = iCol + 1;
        createNumericCell(rspCompanyCodeRow, iCol, quotation.getReference().substring(11), false, CellStyle.ALIGN_LEFT, 11, false, true, BG_WHITE, FC_BLUE);

        return iRow;
    }

    /**
     * @param applicantAddress
     * @return
     */
    private String buildSecondLineAddress(Address applicantAddress) {
        String address2 = "";
        if (applicantAddress.getDistrict() != null) {
            address2 = concat(address2, applicantAddress.getDistrict().getDesc());
        }

        if (applicantAddress.getProvince() != null) {
            address2 = concat(address2, applicantAddress.getProvince().getDesc());
        }
        return address2;
    }

    /**
     * @param applicantAddress
     * @return
     */
    private String buildFirstLineAddress(Address applicantAddress) {
        String address1 = "";

        if (StringUtils.isNotEmpty(applicantAddress.getHouseNo())) {
            address1 += HOUSE_NO + " " + applicantAddress.getHouseNo();
        }

        if (StringUtils.isNotEmpty(applicantAddress.getStreet())) {
            address1 = concat(address1, STREET + " " + applicantAddress.getStreet());
        }

        if (applicantAddress.getVillage() != null) {
            address1 = concat(address1, VILLAGE + " " + applicantAddress.getVillage().getDesc());
        }

        if (applicantAddress.getCommune() != null) {
            address1 = concat(address1, SANGKAT + " " + applicantAddress.getCommune().getDesc());
        }
        return address1;
    }

    private int createVerticalInfo(int iRow, CellStyle style, Quotation quotation) {

        final int startColumnPosition = 1;
        /* vertical */
        Asset asset = quotation.getAsset();
        Date contractStartDate = quotation.getContractStartDate();
        if (contractStartDate == null) {
            contractStartDate = DateUtils.today();
        }
        Date firstPaymentDate = quotation.getFirstDueDate();
        if (firstPaymentDate == null) {
            firstPaymentDate = DateUtils.today();
        }

        int iCol = startColumnPosition;
        iRow = iRow + 1;
        final Row borderLeftTop = sheet.createRow(iRow++);
        borderLeftTop.setRowStyle(style);
        createCell(borderLeftTop, iCol, "", getCellStyle(TOP_LEFT_BORDER));
        //top border
        for (int i = 1; i < 8; i++) {
            createCell(borderLeftTop, i, "", getCellStyle(TOP_BORDER));
            if (i == 1) {
                createCell(borderLeftTop, i, "", getCellStyle(TOP_LEFT_BORDER));
            }
        }
        iCol = 8;
        createCell(borderLeftTop, iCol, "", getCellStyle(TOP_RIGHT_BORDER));
        int fontsize = 9;
        // 1
        Row rowModel = sheet.createRow(iRow++);
        iCol = 0;
        createCell(rowModel, iCol, "", getCellStyle(RIGHT_BORDER));
        iCol = 2;
        createRichCell(rowModel, iCol, RSP_VHE_MOTORCYCLE_MODEL, 13, RSP_VHE_MOTORCYCLE_MODEL.length(), CellStyle.ALIGN_RIGHT, fontsize);

        iCol = 3;
        createCell(rowModel, iCol, asset.getModel().getDesc(), 11, false, true, CellStyle.ALIGN_CENTER, false, BG_WHITE, FC_BLUE);
        iCol = 6;
        createRichCell(rowModel, iCol, RSP_VHE_INTEREST_RATE, 13, RSP_VHE_INTEREST_RATE.length(), CellStyle.ALIGN_RIGHT, fontsize);
        iCol = 7;
        createPercentageValueCell(rowModel, iCol, quotation.getInterestRate() / 100, false, true, CellStyle.ALIGN_CENTER, 11, 2, true, true, BG_WHITE, FC_BLUE);
        iCol = 8;
        createCell(rowModel, iCol, "", getCellStyle(RIGHT_BORDER));

        // 2
        Row rowYear = sheet.createRow(iRow++);
        iCol = startColumnPosition;
        createCell(rowYear, iCol, "", getCellStyle(LEFT_BORDER));
        iCol = 2;
        createRichCell(rowYear, iCol, RSP_VHE_YEAR, 10, RSP_VHE_YEAR.length(), CellStyle.ALIGN_RIGHT, fontsize);
        iCol = 3;
        createNumericCell(rowYear, iCol, asset.getAssetYear().getCode(), true, CellStyle.ALIGN_CENTER, 11, false, true, BG_WHITE, FC_BLUE);
        iCol = 6;
        createRichCell(rowYear, iCol, RSP_NUMBER_OF_PAYMENTS, 23, RSP_NUMBER_OF_PAYMENTS.length(), CellStyle.ALIGN_RIGHT, fontsize);
        iCol = 7;
        createNumericCell(rowYear, iCol, quotation.getTerm(), true, CellStyle.ALIGN_CENTER, 11, false, true, BG_WHITE, FC_BLUE);
        iCol = 8;
        createCell(rowYear, iCol, "", getCellStyle(RIGHT_BORDER));

        // 3
        Row rowCc = sheet.createRow(iRow++);
        iCol = 0;
        createCell(rowCc, iCol, "", getCellStyle(RIGHT_BORDER));
        iCol = 2;
        createRichCell(rowCc, iCol, RSP_VHE_CC, 13, RSP_VHE_CC.length(), CellStyle.ALIGN_RIGHT, fontsize);
        iCol = 3;
        createNumericCell(rowCc, iCol, StringUtils.defaultString(asset.getEngine().getDescEn(), asset.getEngine().getDesc()), true, CellStyle.ALIGN_CENTER, 11, false, true, BG_WHITE, FC_BLUE);
        iCol = 6;
        createRichCell(rowCc, iCol, RSP_MONTHLY_INSTALMENT_AMOUNT, 18, RSP_MONTHLY_INSTALMENT_AMOUNT.length(), CellStyle.ALIGN_RIGHT, fontsize);
        iCol = 7;

        createPriceGeneral(rowCc, iCol, MyNumberUtils.getDouble(quotation.getTotalInstallmentAmount()), true, false, true, CellStyle.ALIGN_CENTER, 11, false, 2, true, BG_WHITE, FC_GREEN);
        iCol = 8;
        createCell(rowCc, iCol, "", getCellStyle(RIGHT_BORDER));

        // 4
        Row rowVehCashPice = sheet.createRow(iRow++);
        iCol = startColumnPosition;
        createCell(rowVehCashPice, iCol, "", getCellStyle(LEFT_BORDER));
        iCol = 2;
        createRichCell(rowVehCashPice, iCol, RSP_VHE_PRICE, 11, RSP_VHE_PRICE.length(), CellStyle.ALIGN_RIGHT, fontsize);
        iCol = 3;

        createPriceGeneral(rowVehCashPice, iCol, MyNumberUtils.getDouble(quotation.getTiFinanceAmount()), true, false, true, CellStyle.ALIGN_CENTER, 11, false, 2, true, BG_WHITE, FC_BLUE);
        iCol = 6;
        createRichCell(rowVehCashPice, iCol, RSP_VHE_CONTRACT_DATE, 13, RSP_VHE_CONTRACT_DATE.length(), CellStyle.ALIGN_RIGHT, fontsize);
        iCol = 7;
        createDateCell(rowVehCashPice, iCol, contractStartDate, true, CellStyle.ALIGN_CENTER, 11, false, true, BG_WHITE, FC_BLUE);
        iCol = 8;
        createCell(rowVehCashPice, iCol, "", getCellStyle(RIGHT_BORDER));

        //Row 5
        Row rowLeaseAmount = sheet.createRow(iRow++);
        iCol = 0;
        createCell(rowLeaseAmount, iCol, "", getCellStyle(RIGHT_BORDER));
        iCol = 2;
        createRichCell(rowLeaseAmount, iCol, RSP_VHE_LEASE_AMOUNT, 13, RSP_VHE_LEASE_AMOUNT.length(), CellStyle.ALIGN_RIGHT, fontsize);
        iCol = 3;
        createPriceGeneral(rowLeaseAmount, iCol, MyNumberUtils.getDouble(quotation.getTiFinanceAmount()), true, false, true, CellStyle.ALIGN_CENTER, 11, false, 2, true, BG_WHITE, FC_GREEN);
        iCol = 6;
        createRichCell(rowLeaseAmount, iCol, RSP_VHE_FIRST_PAYMENT_DATE, 13, RSP_VHE_FIRST_PAYMENT_DATE.length(), CellStyle.ALIGN_RIGHT, fontsize);

        iCol = 7;
        createDateCell(rowLeaseAmount, iCol, firstPaymentDate, true, CellStyle.ALIGN_CENTER, 11, false, true, BG_WHITE, FC_BLUE);
        iCol = 8;
        createCell(rowLeaseAmount, iCol, "", getCellStyle(RIGHT_BORDER));

        final Row buttom_border = sheet.createRow(iRow++);
        buttom_border.setRowStyle(style);

        // bottom border
        for (int i = 1; i < 8; i++) {
            createCell(buttom_border, i, "", getCellStyle(BUTTOM_BORDER));
            if (i == 1) {
                createCell(buttom_border, i, "", getCellStyle(BUTTOM_LEFT_BORDER));
            }
        }

        iCol = 8;
        createCell(buttom_border, iCol, "", getCellStyle(BUTTOM_RIGHT_BORDER));
        /* end vertical */
        return iRow;
    }

    private void createHeader() throws IOException {
        CellStyle style = wb.createCellStyle();
        styles = new HashMap<String, CellStyle>();
        createStyles();
        sheet.setColumnWidth(1, 3000);
        sheet.setColumnWidth(2, 3500);
        sheet.setColumnWidth(3, 3500);
        sheet.setColumnWidth(4, 4000);
        sheet.setColumnWidth(5, 4000);
        sheet.setColumnWidth(6, 4000);
        sheet.setColumnWidth(7, 4200);
        sheet.setColumnWidth(8, 4500);
        sheet.setColumnWidth(9, 3500);
        sheet.setZoom(7, 10);
        //Setup the Page margins - Left, Right, Top and Bottom
        sheet.setMargin(Sheet.LeftMargin, 0.25);
        sheet.setMargin(Sheet.RightMargin, 0.15);
        sheet.setMargin(Sheet.TopMargin, 0.25);
        sheet.setMargin(Sheet.BottomMargin, 0.25);
        sheet.setMargin(Sheet.HeaderMargin, 1.25);
        sheet.setMargin(Sheet.FooterMargin, 0.25);
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BIG_SPOTS);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        final PrintSetup printSetup = sheet.getPrintSetup();
        printSetup.setPaperSize(HSSFPrintSetup.A4_PAPERSIZE);
        printSetup.setScale((short) 75);
        InputStream logoInputStream = null;
        byte[] logoBytes = new byte[0];
        String templatePath = AppConfig.getInstance().getConfiguration().getString("specific.templatedir");
        try {
            logoInputStream = new FileInputStream(templatePath + "/GLF-logo.png");
            logoBytes = IOUtils.toByteArray(logoInputStream);
            int logoIdx = wb.addPicture(logoBytes, Workbook.PICTURE_TYPE_PNG);
            ClientAnchor clientAnchor = wb.getCreationHelper().createClientAnchor();
            clientAnchor.setAnchorType(ClientAnchor.MOVE_DONT_RESIZE);
            clientAnchor.setCol1(0);
            clientAnchor.setCol2(3);
            clientAnchor.setRow1(1);
            clientAnchor.setRow2(5);
            Drawing drawing = sheet.createDrawingPatriarch();
            Picture logo = drawing.createPicture(clientAnchor, logoIdx);
            logo.resize();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            logoInputStream.close();
        }
        final Row headerRow = sheet.createRow(2);
        createCell(headerRow, 3, RSP_HEADER, 14, true, false, CellStyle.ALIGN_LEFT, false, BG_WHITE, FC_BLACK);

    }

    /**
     * @param sheet
     * @param iRow
     * @param style
     * @return
     * @throws Exception
     */
    private int createPaymentTableHeader(final Sheet sheet, int iRow, final CellStyle style) throws Exception {
        /* Create total data header */

        int iCol = 0;
        Row tmpRow = sheet.createRow(iRow++);
        int fontSize = 10;
        createCell(tmpRow, iCol++, RSP_INDEX, fontSize, false, false, CellStyle.ALIGN_CENTER, true, BG_GREEN, FC_WHITE);
        createCell(tmpRow, iCol++, RSP_DATE_PAYMENT, fontSize, false, false, CellStyle.ALIGN_RIGHT, true, BG_GREEN, FC_WHITE);
        createCell(tmpRow, iCol++, RSP_SCHEDULE_PAYMENT, fontSize, false, false, CellStyle.ALIGN_RIGHT, true, BG_GREEN, FC_WHITE);
        createCell(tmpRow, iCol++, SERVICING_FEE, fontSize, false, false, CellStyle.ALIGN_RIGHT, true, BG_GREEN, FC_WHITE);
        createCell(tmpRow, iCol++, INSURANCE_FEE, fontSize, false, false, CellStyle.ALIGN_RIGHT, true, BG_GREEN, FC_WHITE);
        createCell(tmpRow, iCol++, TRANSFER_RIGHT_FEE, fontSize, false, false, CellStyle.ALIGN_RIGHT, true, BG_GREEN, FC_WHITE);
        createCell(tmpRow, iCol++, RSP_PRICIPAL_PAYMENT, fontSize, false, false, CellStyle.ALIGN_RIGHT, true, BG_GREEN, FC_WHITE);
        createCell(tmpRow, iCol++, RSP_VAT_PRICIPAL_PAYMENT, fontSize, false, false, CellStyle.ALIGN_RIGHT, true, BG_GREEN, FC_WHITE);
        createCell(tmpRow, iCol++, RSP_INTEREST_PAYMENT, fontSize, false, false, CellStyle.ALIGN_RIGHT, true, BG_GREEN, FC_WHITE);
        createCell(tmpRow, iCol, RSP_BALANCE_PAYMENT, fontSize, false, false, CellStyle.ALIGN_RIGHT, true, BG_GREEN, FC_WHITE);
        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, iCol, iCol + 1));
        iCol = iCol + 1;
        createCell(tmpRow, iCol++, "", getCellStyle(HEADER));
        tmpRow.setRowStyle(style);

        return iRow;

    }

    /**
     * @param sheet
     * @param iRow
     * @param style
     * @param listInstallmentVOs
     * @return
     * @throws Exception
     */
    private int createPaymentTable(final Sheet sheet, int iRow, final CellStyle style, List<InstallmentVO> listInstallmentVOs) throws Exception {

        /* Create total data header */
        Row tmpRow = sheet.createRow(iRow++);
        int fontSize = 12;
        tmpRow.setRowStyle(style);
        Double totalInstallmentAmount = 0d;
        int iCol = 0;
        double installmentAmount = 0d;
        double tiPrincipal = 0d;
        double tiInterestRate = 0d;
        double tiInsurance = 0d;
        double tiTranferRightFee = 0d;
        double tiServiceFee = 0d;
        double otherAmount = 0d;
        double vatAmount = 0d;
        double balance = 0d;
        Date paymentDate = null;
        double vatPrincipal = 0d;
        int nbPayment = 0;
        for (InstallmentVO installmentVO : listInstallmentVOs) {
            paymentDate = installmentVO.getInstallmentDate();
            nbPayment = installmentVO.getNumInstallment();
            installmentAmount += installmentVO.getTiamount() + installmentVO.getVatAmount();
            if (ECashflowType.CAP.equals(installmentVO.getCashflowType())) {
                tiPrincipal = installmentVO.getTiamount();
                vatPrincipal = installmentVO.getVatAmount();
                balance = installmentVO.getBalance();
            } else if (ECashflowType.IAP.equals(installmentVO.getCashflowType())) {
                tiInterestRate = installmentVO.getTiamount();
            } else if (installmentVO.getService() != null && EServiceType.INSFEE.getCode().equals(installmentVO.getService().getCode())) {
                tiInsurance = installmentVO.getTiamount() + installmentVO.getVatAmount();
            } else if (installmentVO.getService() != null) {
                if (EServiceType.SRVFEE.getCode().equals(installmentVO.getService().getCode())){
                    tiServiceFee = installmentVO.getTiamount() + installmentVO.getVatAmount();
                }else if (EServiceType.SRVFEEEXT.getCode().equals(installmentVO.getService().getCode())){
                    tiServiceFee = installmentVO.getTiamount() + installmentVO.getVatAmount();
                }
            } else {
                otherAmount += installmentVO.getTiamount() + installmentVO.getVatAmount();
            }

            if (installmentVO.getService() != null && EServiceType.TRANSFEE.getCode().equals(installmentVO.getService().getCode())) {
                tiTranferRightFee = installmentVO.getTiamount() + installmentVO.getVatAmount();
            }
        }

        createNumericCell(tmpRow, iCol++, nbPayment, false, CellStyle.ALIGN_CENTER, fontSize, false, true, BG_WHITE, FC_BLACK);
        createDateCell(tmpRow, iCol++, paymentDate, false, CellStyle.ALIGN_RIGHT, fontSize, false, true, BG_WHITE, FC_BLACK);
        createPriceGeneral(tmpRow, iCol++, installmentAmount, true, false, false, CellStyle.ALIGN_RIGHT, fontSize, false, 2, true, BG_WHITE, FC_BLACK);
        createPriceGeneral(tmpRow, iCol++, tiServiceFee, true, false, false, CellStyle.ALIGN_RIGHT, fontSize, false, 2, true, BG_WHITE, FC_BLACK);
        createPriceGeneral(tmpRow, iCol++, tiInsurance, true, false, false, CellStyle.ALIGN_RIGHT, fontSize, false, 2, true, BG_WHITE, FC_BLACK);
        createPriceGeneral(tmpRow, iCol++, tiTranferRightFee, true, false, false, CellStyle.ALIGN_RIGHT, fontSize, false, 2, true, BG_WHITE, FC_BLACK);
        createPriceGeneral(tmpRow, iCol++, tiPrincipal, true, false, false, CellStyle.ALIGN_RIGHT, fontSize, false, 2, true, BG_WHITE, FC_BLACK);
        createPriceGeneral(tmpRow, iCol++, vatPrincipal, true, false, false, CellStyle.ALIGN_RIGHT, fontSize, false, 2, true, BG_WHITE, FC_BLACK);
        createPriceGeneral(tmpRow, iCol++, tiInterestRate, true, false, false, CellStyle.ALIGN_RIGHT, fontSize, false, 2, true, BG_WHITE, FC_BLACK);
        createPriceGeneral(tmpRow, iCol, balance, true, false, false, CellStyle.ALIGN_RIGHT, fontSize, false, 2, true, BG_WHITE, FC_BLACK);
        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, iCol, iCol + 1));
        tmpRow.setRowStyle(style);
        return iRow;

    }

    /**
     * @param sheet
     * @param iRow
     * @param style
     * @return
     * @throws Exception
     */
    public int footer(final Sheet sheet, int iRow, final CellStyle style, Quotation quotation) throws Exception {
        Applicant applicant = quotation.getMainApplicant();
        String lesseeName = applicant.getLastName() + " " + applicant.getFirstName();

        Date contractStartDate = quotation.getContractStartDate();
        if (contractStartDate == null) {
            contractStartDate = DateUtils.today();
        }

		/* Create total data header */
        int setFontsize = 12;
        int iCol = 1;
        Row tmpRow1 = sheet.createRow(iRow++);
        createRichCell(tmpRow1, iCol, RSP_FOOTER_GL_FINANCE_PLC, 14, RSP_FOOTER_GL_FINANCE_PLC.length(), CellStyle.ALIGN_LEFT, setFontsize);
        iCol = 6;
        createRichCell(tmpRow1, iCol, RSP_FOOTER_CUS_LESSEE, 15, RSP_FOOTER_CUS_LESSEE.length(), CellStyle.ALIGN_LEFT, setFontsize);
        iRow = iRow + 6;
        iCol = 1;
        Row tmpRow2 = sheet.createRow(iRow++);
        createRichCell(tmpRow2, iCol, RSP_FOOTER_GLF_NAME, 5, RSP_FOOTER_GLF_NAME.length(), CellStyle.ALIGN_LEFT, setFontsize);
        iCol = 6;
        createRichCell(tmpRow2, iCol, I18N.message(RSP_FOOTER_CUS_NAME, lesseeName), 5, I18N.message(RSP_FOOTER_CUS_NAME, lesseeName).length(), CellStyle.ALIGN_LEFT, setFontsize);

        iCol = 1;
        Row tmpRow3 = sheet.createRow(iRow++);
        createRichCell(tmpRow3, iCol, I18N.message(RSP_FOOTER_GLF_DATE, DateUtils.date2StringDDMMYYYY_SLASH(contractStartDate)), 11, I18N.message(RSP_FOOTER_GLF_DATE, DateUtils.date2StringDDMMYYYY_SLASH(contractStartDate)).length(), CellStyle.ALIGN_LEFT, setFontsize);

        iCol = 0;
        Row tmpRow3_2 = sheet.createRow(iRow++);
        createRichCell(tmpRow3_2, iCol, I18N.message(RSP_FOOTER_GLF_NOTE), 15, RSP_FOOTER_GLF_NOTE.length(), CellStyle.ALIGN_LEFT, 10);

        iCol = 6;
        createRichCell(tmpRow3, iCol, I18N.message(RSP_FOOTER_CUS_DATE, DateUtils.date2StringDDMMYYYY_SLASH(contractStartDate)), 11, I18N.message(RSP_FOOTER_CUS_DATE, DateUtils.date2StringDDMMYYYY_SLASH(contractStartDate)).length(), CellStyle.ALIGN_LEFT, setFontsize);

        return iRow;
    }

    /**
     * @param richStringRow
     * @param iCol
     * @param value
     * @param start
     * @param end
     * @param alignment
     * @param fontsize
     * @return
     */
    protected Cell createRichCell(final Row richStringRow, final int iCol, final String value, int start, int end, final short alignment, final int fontsize) {
        final CellStyle style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BIG_SPOTS);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setLocked(true);
        style.setAlignment(alignment);

        Font font1 = wb.createFont();
        font1.setFontHeightInPoints((short) fontsize);

        font1.setFontName(KHMER_OS_BATTAMBANG);
        font1.setColor(FC_BLACK);

        Font font2 = wb.createFont();
        font2.setFontHeightInPoints((short) fontsize);
        font2.setFontName(KHMER_OS_BATTAMBANG);
        font2.setColor(FC_GREY);

        final Cell cell = richStringRow.createCell(iCol);
        RichTextString richString = new XSSFRichTextString(value);
        richString.applyFont(0, start, font1);
        richString.applyFont(start, end, font2);
        cell.setCellValue(richString);
        style.setFont(font1);
        style.setFont(font2);
        cell.setCellStyle(style);

        richStringRow.setRowStyle(style);

        return cell;
    }

    /**
     *
     */
    protected Cell createCell(final Row row, final int iCol, final String value, final CellStyle style) {
        final Cell cell = row.createCell(iCol);
        cell.setCellValue((value == null ? "" : value));
        cell.setCellStyle(style);
        return cell;
    }

    /**
     * @param row
     * @param iCol
     * @param value
     * @param fontsize
     * @param isBold
     * @param hasBorder
     * @param alignment
     * @param setBgColor
     * @param bgColor
     * @param fonCorlor
     * @return
     */
    protected Cell createCell(final Row row, final int iCol, final String value, final int fontsize, final boolean isBold, final boolean hasBorder,
                              final short alignment, final boolean setBgColor, final short bgColor, final short fonCorlor) {
        final Cell cell = row.createCell(iCol);
        final Font itemFont = wb.createFont();
        itemFont.setFontHeightInPoints((short) fontsize);

        if (isBold) {
            itemFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
        }
        itemFont.setFontName(KHMER_OS_BATTAMBANG);

        final CellStyle style = wb.createCellStyle();
        style.setAlignment(alignment);
        style.setFont(itemFont);
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BIG_SPOTS);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setLocked(true);
        itemFont.setColor(fonCorlor);
        style.setFont(itemFont);
        if (hasBorder) {
            style.setBorderTop(CellStyle.BORDER_DOTTED);
            style.setBorderLeft(CellStyle.BORDER_DOTTED);
            style.setBorderRight(CellStyle.BORDER_DOTTED);
            style.setBorderBottom(CellStyle.BORDER_DOTTED);
        }
        if (setBgColor) {
            style.setFillForegroundColor(bgColor);
            style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        }
        cell.setCellValue((value == null ? "" : value));
        cell.setCellStyle(style);
        row.setRowStyle(style);
        return cell;
    }

    /**
     * @param row
     * @param iCol
     * @param price
     * @param setCurrentcy
     * @param isKhr
     * @param hasBorder
     * @param alignment
     * @param fontsize
     * @param isBold
     * @param decimalFormatNumber
     * @param setBgColor
     * @param bgColor
     * @param fontColor
     * @return
     */
    protected Cell createPriceGeneral(final Row row, final int iCol,
                                      final Double price, final boolean setCurrentcy,
                                      final boolean isKhr, final boolean hasBorder,
                                      final short alignment, final int fontsize, final boolean isBold,
                                      final int decimalFormatNumber, final boolean setBgColor,
                                      final short bgColor, final short fontColor) {

        final Cell cell = row.createCell(iCol);
        final DataFormat df = wb.createDataFormat();
        final Font itemRightFont = wb.createFont();
        itemRightFont.setFontHeightInPoints((short) fontsize);
        itemRightFont.setFontName(KHMER_OS_BATTAMBANG);
        itemRightFont.setColor(fontColor);
        if (isBold) {
            itemRightFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
        }
        final CellStyle style = wb.createCellStyle();
        style.setFont(itemRightFont);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setLocked(true);

        if (setBgColor) {
            style.setFillForegroundColor(bgColor);
            style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        }
        if (hasBorder) {
            style.setBorderTop(CellStyle.BORDER_DOTTED);
            style.setBorderLeft(CellStyle.BORDER_DOTTED);
            style.setBorderRight(CellStyle.BORDER_DOTTED);
            style.setBorderBottom(CellStyle.BORDER_DOTTED);
        }
        if (setCurrentcy && price != 0) {
            ECurrency currency = ECurrency.getDefault();
            if (currency != null && StringUtils.isNotEmpty(currency.getSymbol())) {
                style.setDataFormat(df.getFormat(currency.getSymbol() + " " + FORMAT_DECIMAL2));
            } else {
                style.setDataFormat(df.getFormat("$ " + FORMAT_DECIMAL2));
            }
        }
        if (price != 0 && !setCurrentcy) {
            if (decimalFormatNumber == 1) {
                style.setDataFormat(df.getFormat(FORMAT_DECIMAL1));
            } else if (decimalFormatNumber == 2) {
                style.setDataFormat(df.getFormat(FORMAT_DECIMAL2));
            } else if (decimalFormatNumber == 3) {
                style.setDataFormat(df.getFormat(FORMAT_DECIMAL3));
            }
        }
        style.setAlignment(alignment);
        if (price == 0 || price == null) {
            cell.setCellValue("-");
        } else {
            cell.setCellValue((price == null ? 0 : price));
        }

        cell.setCellStyle(style);
        return cell;
    }

    /**
     * @param row
     * @param iCol
     * @param value
     * @param hasBorder
     * @param alignment
     * @param fontsize
     * @param isBold
     * @param setBgColor
     * @param bgColor
     * @param fontColor
     * @return
     */
    protected Cell createNumericCell(final Row row, final int iCol,
                                     final Object value, final boolean hasBorder, final short alignment,
                                     final int fontsize, final boolean isBold, final boolean setBgColor,
                                     final short bgColor, final short fontColor) {

        final Cell cell = row.createCell(iCol);
        final Font itemRightFont = wb.createFont();
        itemRightFont.setFontHeightInPoints((short) fontsize);
        itemRightFont.setFontName(KHMER_OS_BATTAMBANG);
        itemRightFont.setColor(fontColor);
        if (isBold) {
            itemRightFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
        }
        final CellStyle style = wb.createCellStyle();
        style.setFont(itemRightFont);
        style.setLocked(true);

        if (setBgColor) {
            style.setFillForegroundColor(bgColor);
            style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        }
        if (hasBorder) {
            style.setBorderTop(CellStyle.BORDER_DOTTED);
            style.setBorderLeft(CellStyle.BORDER_DOTTED);
            style.setBorderRight(CellStyle.BORDER_DOTTED);
            style.setBorderBottom(CellStyle.BORDER_DOTTED);
        }

        style.setAlignment(alignment);
        if (value == null) {
            cell.setCellValue("");
        } else if (value instanceof Integer) {
            cell.setCellValue(Integer.valueOf(value.toString()));
        } else if (value instanceof Long) {
            cell.setCellValue(Long.valueOf(value.toString()));
        } else if (value instanceof String) {
            cell.setCellValue(value.toString());
        }
        cell.setCellStyle(style);
        return cell;
    }

    /**
     * @param row
     * @param iCol
     * @param value
     * @param isBold
     * @param hasBorder
     * @param alignment
     * @param fontsize
     * @param decimalFormatNumber
     * @param percentageSymbol
     * @param setBgColor
     * @param bgColor
     * @param fontColor
     * @return
     */
    protected Cell createPercentageValueCell(final Row row,
                                             final int iCol, final Double value, final boolean isBold,
                                             final boolean hasBorder, final short alignment, final int fontsize,
                                             final int decimalFormatNumber, final boolean percentageSymbol,
                                             final boolean setBgColor, final short bgColor, final short fontColor) {
        final Cell cell = row.createCell(iCol);
        final DataFormat df = wb.createDataFormat();

        final Font itemRightFont = wb.createFont();
        itemRightFont.setFontHeightInPoints((short) fontsize);
        itemRightFont.setFontName(KHMER_OS_BATTAMBANG);
        itemRightFont.setColor(fontColor);

        final CellStyle style = wb.createCellStyle();
        style.setFont(itemRightFont);
        style.setLocked(true);

        if (isBold) {
            itemRightFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
        }
        if (setBgColor) {
            style.setFillForegroundColor(bgColor);
            style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        }
        if (hasBorder) {
            style.setBorderTop(CellStyle.BORDER_DOTTED);
            style.setBorderLeft(CellStyle.BORDER_DOTTED);
            style.setBorderRight(CellStyle.BORDER_DOTTED);
            style.setBorderBottom(CellStyle.BORDER_DOTTED);
        }

        if (percentageSymbol) {
            if (decimalFormatNumber == 1) {
                style.setDataFormat(df.getFormat(FORMAT_PERCENTAGE1));
            } else if (decimalFormatNumber == 2) {
                style.setDataFormat(df.getFormat(FORMAT_PERCENTAGE2));
            } else if (decimalFormatNumber == 3) {
                style.setDataFormat(df.getFormat(FORMAT_PERCENTAGE3));
            }
        } else {
            if (decimalFormatNumber == 1) {
                style.setDataFormat(df.getFormat(FORMAT_DECIMAL1));
            } else if (decimalFormatNumber == 2) {
                style.setDataFormat(df.getFormat(FORMAT_DECIMAL2));
            } else if (decimalFormatNumber == 3) {
                style.setDataFormat(df.getFormat(FORMAT_DECIMAL3));
            }
        }
        style.setAlignment(alignment);
        cell.setCellValue((value == null ? 0 : value));
        cell.setCellStyle(style);
        return cell;
    }

    /**
     * @param row
     * @param iCol
     * @param value
     * @param hasBorder
     * @param alignment
     * @param fontsize
     * @param isBold
     * @param setBgColor
     * @param bgColor
     * @param fontColor
     * @return
     */
    protected Cell createDateCell(final Row row, final int iCol,
                                  final Date value, final boolean hasBorder, final short alignment,
                                  final int fontsize, final boolean isBold, final boolean setBgColor,
                                  final short bgColor, final short fontColor) {

        final Cell cell = row.createCell(iCol);

        final Font itemRightFont = wb.createFont();
        itemRightFont.setFontHeightInPoints((short) fontsize);
        itemRightFont.setFontName(KHMER_OS_BATTAMBANG);
        itemRightFont.setColor(fontColor);
        if (isBold) {
            itemRightFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
        }
        final CellStyle style = wb.createCellStyle();
        style.setFont(itemRightFont);
        style.setLocked(true);

        if (setBgColor) {
            style.setFillForegroundColor(bgColor);
            style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        }
        if (hasBorder) {
            style.setBorderTop(CellStyle.BORDER_DOTTED);
            style.setBorderLeft(CellStyle.BORDER_DOTTED);
            style.setBorderRight(CellStyle.BORDER_DOTTED);
            style.setBorderBottom(CellStyle.BORDER_DOTTED);
        }
        style.setAlignment(alignment);
        cell.setCellValue((value == null ? "" : getDateLabel(value)));
        cell.setCellStyle(style);
        return cell;
    }

    /**
     * @param date
     * @param formatPattern
     * @return
     */
    public String getDateLabel(final Date date,
                               final String formatPattern) {
        if (date != null && formatPattern != null) {
            return DateFormatUtils.format(date, formatPattern);
        }
        return null;
    }

    /**
     * @param date
     * @return
     */
    public String getDateLabel(final Date date) {
        return getDateLabel(date, DEFAULT_DATE_FORMAT);
    }

    /**
     * @return
     */
    private Map<String, CellStyle> createStyles() {
        CellStyle style;
        style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BORDER_DOUBLE);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderLeft(CellStyle.BORDER_MEDIUM);
        style.setBorderTop(CellStyle.BORDER_MEDIUM);
        style.setLocked(true);
        styles.put(TOP_LEFT_BORDER, style);

        style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BORDER_DOUBLE);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderTop(CellStyle.BORDER_MEDIUM);
        style.setLocked(true);
        styles.put(TOP_BORDER, style);

        style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BORDER_DOUBLE);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderLeft(CellStyle.BORDER_MEDIUM);
        style.setLocked(true);
        styles.put(LEFT_BORDER, style);

        style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BORDER_DOUBLE);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderRight(CellStyle.BORDER_MEDIUM);
        style.setLocked(true);
        styles.put(RIGHT_BORDER, style);

        style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BORDER_DOUBLE);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderBottom(CellStyle.BORDER_MEDIUM);
        style.setLocked(true);
        styles.put(BUTTOM_BORDER, style);

        style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BORDER_DOUBLE);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderRight(CellStyle.BORDER_MEDIUM);
        style.setBorderTop(CellStyle.BORDER_MEDIUM);
        style.setLocked(true);
        styles.put(TOP_RIGHT_BORDER, style);

        style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BORDER_DOUBLE);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderRight(CellStyle.BORDER_MEDIUM);
        style.setBorderBottom(CellStyle.BORDER_MEDIUM);
        style.setLocked(true);
        styles.put(BUTTOM_RIGHT_BORDER, style);

        style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BORDER_DOUBLE);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderLeft(CellStyle.BORDER_MEDIUM);
        style.setBorderBottom(CellStyle.BORDER_MEDIUM);
        style.setLocked(true);
        styles.put(BUTTOM_LEFT_BORDER, style);

        style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.GREEN.getIndex());
        style.setFillPattern(HSSFCellStyle.BORDER_DOUBLE);
        style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setLocked(true);
        styles.put(HEADER, style);

        return styles;
    }

    protected CellStyle getCellStyle(final String styleName) {
        return styles.get(styleName);
    }

    /**
     * @param value
     * @param defaultValue
     * @return
     */
    private String getDefaultString(String value, String defaultValue) {
        if (StringUtils.isNotEmpty(value)) {
            return value;
        } else {
            return StringUtils.defaultString(defaultValue);
        }
    }

    /**
     * @param resumeAddress
     * @param value
     * @return
     */
    private String concat(String resumeAddress, String value) {
        if (StringUtils.isNotEmpty(value)) {
            if (StringUtils.isNotEmpty(resumeAddress)) {
                resumeAddress += " ";
            }
            resumeAddress += value;
        }
        return resumeAddress;
    }

    /**
     * @param service
     * @return
     */
    private Double getServiceFeeAmount(QuotationService service) {
        if (service.getService().getFrequency() != null) {
            int nbMonths = service.getService().getFrequency().getNbMonths();
            return MyMathUtils.roundAmountTo(((term / nbMonths) * service.getTiPrice()) / term);
        } else {
            return MyMathUtils.roundAmountTo(service.getTiPrice() / term);
        }
    }
}
