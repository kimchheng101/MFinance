package com.soma.mfinance.core.registrations.model;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.frmk.security.model.SecProfile;
import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;

/**
 * 
 * @author Riya.Pov
 *
 */
@Entity
@Table(name = "tu_profile_contract_status")
public class ProfileContractStatus extends EntityA {

	private static final long serialVersionUID = 3773416307610244314L;
	
	private SecProfile profile;
	private EWkfStatus wkfStatus;
	
    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "prcontst_id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}
	/**
	 * @return the profile
	 */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_pro_id")
	public SecProfile getProfile() {
		return profile;
	}
	/**
	 * @param profile the profile to set
	 */
	public void setProfile(SecProfile profile) {
		this.profile = profile;
	}

	@Column(name = "wkf_sta_id", nullable = false)
	@Convert(converter = EWkfStatus.class)
	public EWkfStatus getWkfStatus() {
		return wkfStatus;
	}
	public void setWkfStatus(EWkfStatus wkfStatus) {
		this.wkfStatus = wkfStatus;
	}
	
	
}
