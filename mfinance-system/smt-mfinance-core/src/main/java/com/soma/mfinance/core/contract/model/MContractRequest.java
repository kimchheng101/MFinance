package com.soma.mfinance.core.contract.model;

import org.seuksa.frmk.model.entity.MEntityA;


/**
 * Meta data of com.soma.mfinance.core.contract_old.model.ContractRequest
 * @author kimsuor.seang
 */
public interface MContractRequest extends MEntityA {
	
	public final static String CONTRACT = "contract";
	public final static String REQUESTTYPE = "requestType";
	public final static String OTHERSVALUE = "othersValue";
	public final static String COMMENT = "comment";
	public final static String USER = "user";
	public final static String PROCESSED = "processed";
	public final static String ACTIONS = "actions";
	
}
