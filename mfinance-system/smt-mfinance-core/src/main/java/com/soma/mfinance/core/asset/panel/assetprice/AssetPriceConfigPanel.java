package com.soma.mfinance.core.asset.panel.assetprice;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * Created by cheasocheat on 3/23/17.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(AssetPriceConfigPanel.NAME)
public class AssetPriceConfigPanel extends AbstractTabsheetPanel implements View{
    public static final String NAME = "asset.prices";

    @Autowired
    private AssetPriceConfigFormPanel formPanel;
    @Autowired
    private AssetPriceConfigTablePanel tablePanel;

    @PostConstruct
    public void PostConstruct(){
        super.init();
        tablePanel.setMainPanel(this);
        formPanel.setCaption(I18N.message("asset.price"));
        getTabSheet().setTablePanel(tablePanel);
    }
    @Override
    public void onAddEventClick() {
        formPanel.reset();
        getTabSheet().addFormPanel(formPanel);
        getTabSheet().setSelectedTab(formPanel);
    }

    @Override
    public void onEditEventClick() {
        getTabSheet().addFormPanel(formPanel);
        initSelectedTab(formPanel);
    }

    @Override
    public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
        if (selectedTab == formPanel) {
            formPanel.assignValues(tablePanel.getItemSelectedId());
        } else if (selectedTab == tablePanel && getTabSheet().isNeedRefresh()) {
            tablePanel.refresh();
        }
        getTabSheet().setSelectedTab(selectedTab);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}
