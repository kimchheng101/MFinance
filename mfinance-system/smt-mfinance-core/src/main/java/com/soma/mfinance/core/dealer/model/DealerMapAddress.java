package com.soma.mfinance.core.dealer.model;

import com.soma.ersys.core.hr.model.address.Commune;
import com.soma.ersys.core.hr.model.address.District;
import com.soma.ersys.core.hr.model.address.Province;
import org.seuksa.frmk.model.EntityFactory;
import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;

/**
 * @author by kimsuor.seang  on 11/11/2017.
 */
@Entity
@Table(name = "tu_dealer_map")
public class DealerMapAddress extends EntityA {

    private Province province;
    private District district;
    private Commune commune;
    //private Village village;
    private Dealer dealer;

    public static DealerMapAddress createInstance() {
        DealerMapAddress dealerMapAddress = EntityFactory.createInstance(DealerMapAddress.class);
        return dealerMapAddress;
    }

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "deler_map_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "prvin_id")
    public Province getProvince() {
        return province;
    }
    public void setProvince(Province province) {
        this.province = province;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "distr_id")
    public District getDistrict() {
        return district;
    }
    public void setDistrict(District district) {
        this.district = district;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "commu_id")
    public Commune getCommune() {
        return commune;
    }
    public void setCommune(Commune commune) {
        this.commune = commune;
    }

	/*@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "villa_id")
	public Village getVillage() {
		return village;
	}
	public void setVillage(Village village) {
		this.village = village;
	}*/

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "deler_id")
    public Dealer getDealer() {
        return dealer;
    }
    public void setDealer(Dealer dealer) {
        this.dealer = dealer;
    }

}
