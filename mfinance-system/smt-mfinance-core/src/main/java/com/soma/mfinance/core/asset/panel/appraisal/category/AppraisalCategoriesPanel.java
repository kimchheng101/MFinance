package com.soma.mfinance.core.asset.panel.appraisal.category;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * 
 * @author vi.sok
 *
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(AppraisalCategoriesPanel.NAME)
public class AppraisalCategoriesPanel extends AbstractTabsheetPanel implements View {
	
	/** */
	private static final long serialVersionUID = -2963062593873628481L;

	public static final String NAME = "appraisal.categories";
	
	@Autowired
	private AppraisalCategoryTablePanel appraisalCategoryTablePanel;
	@Autowired
	private AppraisalCategoryFormPanel appraisalCategoryFormPanel;
	
	@PostConstruct
	public void PostConstruct() {
		super.init();
		appraisalCategoryTablePanel.setMainPanel(this);
		appraisalCategoryFormPanel.setCaption(I18N.message("appraisal.category"));
		getTabSheet().setTablePanel(appraisalCategoryTablePanel);
	}

	/**
	 * @see View#enter(ViewChangeEvent)
	 */
	@Override
	public void enter(ViewChangeEvent event) {
	}

	/**
	 * @see AbstractTabsheetPanel#onAddEventClick()
	 */
	@Override
	public void onAddEventClick() {
		appraisalCategoryFormPanel.reset();
		getTabSheet().addFormPanel(appraisalCategoryFormPanel);
		getTabSheet().setSelectedTab(appraisalCategoryFormPanel);
	}

	/**
	 * @see AbstractTabsheetPanel#onEditEventClick()
	 */
	@Override
	public void onEditEventClick() {
		getTabSheet().addFormPanel(appraisalCategoryFormPanel);
		initSelectedTab(appraisalCategoryFormPanel);
	}

	/**
	 * @see AbstractTabsheetPanel#initSelectedTab(com.vaadin.ui.Component)
	 */
	@Override
	public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
		if (selectedTab == appraisalCategoryFormPanel) {
			appraisalCategoryFormPanel.assignValues(appraisalCategoryTablePanel.getItemSelectedId());
		} else if (selectedTab == appraisalCategoryTablePanel && getTabSheet().isNeedRefresh()) {
			appraisalCategoryTablePanel.refresh();
		}
		getTabSheet().setSelectedTab(selectedTab);
	}
}
