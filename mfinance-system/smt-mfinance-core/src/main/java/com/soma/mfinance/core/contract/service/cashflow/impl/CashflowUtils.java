package com.soma.mfinance.core.contract.service.cashflow.impl;

import java.util.Date;
import java.util.List;

import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowType;
import com.soma.mfinance.core.contract.model.cashflow.ETreasuryType;
import org.seuksa.frmk.service.EntityService;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;

import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.financial.model.CreditLine;
import com.soma.mfinance.core.financial.model.EProductLineType;
import com.soma.mfinance.core.financial.model.ProductLine;
import com.soma.mfinance.core.payment.model.EPaymentCondition;
import com.soma.mfinance.core.payment.model.EPaymentMethod;
import com.soma.ersys.finance.accounting.model.JournalEvent;

public class CashflowUtils {

	/**
	 * @param productLine
	 * @param creditLine
	 * @param contract
	 * @param cashfolwType
	 * @param treasuryType
	 * @param paymentCondition
	 * @param teAmount
	 * @param vatAmount
	 * @param tiAmount
	 * @param intallmentDate
	 * @param numInstallment
	 * @return
	 */
	public static Cashflow createCashflow(ProductLine productLine,
                                          CreditLine creditLine, Contract contract, double vatValue,
                                          ECashflowType cashfolwType, ETreasuryType treasuryType, JournalEvent journalEvent, EPaymentCondition paymentCondition,
                                          double teAmount, double vatAmount, double tiAmount,
                                          Date intallmentDate, Date periodStartDate, Date periodEndDate, Integer numInstallment) {
		EntityService entityService = SpringUtils.getBean(EntityService.class);
		EPaymentMethod paymentMethod = (paymentCondition == null || paymentCondition.getPaymentMethod() == null) ? entityService.getByCode(EPaymentMethod.class, "CASH") : paymentCondition.getPaymentMethod();
		return createCashflow(productLine, 
				creditLine, contract, vatValue, cashfolwType, treasuryType, journalEvent, paymentMethod, 
				teAmount, vatAmount, tiAmount, intallmentDate, periodStartDate, periodEndDate, numInstallment);
	}
	
	/**
	 * @param productLine
	 * @param creditLine
	 * @param contract
	 * @param cashfolwType
	 * @param treasuryType
	 * @param paymentMethod
	 * @param teAmount
	 * @param vatAmount
	 * @param tiAmount
	 * @param intallmentDate
	 * @param numInstallment
	 * @return
	 */
	public static Cashflow createCashflow(ProductLine productLine,
			CreditLine creditLine, Contract contract, double vatValue,
			ECashflowType cashfolwType, ETreasuryType treasuryType, JournalEvent journalEvent, EPaymentMethod paymentMethod,
			double teAmount, double vatAmount, double tiAmount,
			Date intallmentDate, Date periodStartDate, Date periodEndDate, Integer numInstallment) {
		return createCashflow(EProductLineType.FNC, productLine, 
				creditLine, contract, vatValue,
				cashfolwType, treasuryType, journalEvent, paymentMethod, 
				teAmount, vatAmount, tiAmount, intallmentDate, periodStartDate, periodEndDate, numInstallment);
	}
	
	/**
	 * @param productLineType
	 * @param productLine
	 * @param creditLine
	 * @param contract
	 * @param cashfolwType
	 * @param treasuryType
	 * @param paymentMethod
	 * @param teAmount
	 * @param vatAmount
	 * @param tiAmount
	 * @param intallmentDate
	 * @param numInstallment
	 * @return
	 */
	public static Cashflow createCashflow(EProductLineType productLineType, ProductLine productLine,
			CreditLine creditLine, Contract contract, double vatValue,
			ECashflowType cashfolwType, ETreasuryType treasuryType, JournalEvent journalEvent, EPaymentMethod paymentMethod,
			double teAmount, double vatAmount, double tiAmount,
			Date intallmentDate, Date periodStartDate, Date periodEndDate, Integer numInstallment) {
		Cashflow cashflow = new Cashflow();
		cashflow.setCashflowType(cashfolwType);
		cashflow.setTreasuryType(treasuryType);
		cashflow.setProductLineType(productLineType);
		cashflow.setProductLine(productLine);
		cashflow.setContract(contract);
		cashflow.setPaymentMethod(paymentMethod);
		cashflow.setVatValue(vatValue);
		cashflow.setTiInstallmentAmount(tiAmount);
		cashflow.setTeInstallmentAmount(teAmount);
		cashflow.setVatInstallmentAmount(vatAmount);
		cashflow.setInstallmentDate(DateUtils.getDateAtBeginningOfDay(intallmentDate));
		cashflow.setPeriodStartDate(DateUtils.getDateAtBeginningOfDay(periodStartDate));
		cashflow.setPeriodEndDate(DateUtils.getDateAtBeginningOfDay(periodEndDate));
		cashflow.setNumInstallment(numInstallment);
		cashflow.setJournalEvent(journalEvent);
		return cashflow;
	}
	
	/**
	 * check the duplicate cashflow if exist 
	 * 
	 * @param cashflows
	 * @param cashflow
	 * @return
	 */
	public static boolean isCashflowDuplicate(List<Cashflow> cashflows, Cashflow cashflow) {
		boolean result = false;
		if (cashflow == null || cashflows == null || cashflows.isEmpty()) {
			return result;
		}
		
		for (Cashflow caflw : cashflows) {
			boolean isNumInstallment = caflw.getNumInstallment() == cashflow.getNumInstallment();
			boolean isCashflowType = caflw.getCashflowType() == cashflow.getCashflowType();
			boolean isCashflowDuplicateNumInstallmentType = isNumInstallment && isCashflowType;
			if (isCashflowDuplicateNumInstallmentType) {
				if (cashflow.getCashflowType().equals(ECashflowType.FEE)) {
					if (caflw.getService().getId() == cashflow.getService().getId()) {
						result = true;
						break;
					}
				} else {
					result = true;
					break;
				}
			}
		}
		return result;
	}
}
