/**
 * Auto-generated on Tue Jun 09 11:27:49 ICT 2015
 */
package com.soma.mfinance.core.common.security.model;

import org.seuksa.frmk.model.entity.MEntityA;

/**
 * Meta data of com.soma.mfinance.core.common.security.model.SecUserBackup
 * @author 
 */
public interface MSecUserBackup extends MEntityA {

	public final static String EXPIRATIONDATE = "expirationDate";
	public final static String STATUSRECORD = "statusRecord";
	public final static String CRUDACTION = "crudAction";

}
