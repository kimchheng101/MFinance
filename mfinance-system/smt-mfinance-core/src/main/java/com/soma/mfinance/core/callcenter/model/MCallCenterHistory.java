package com.soma.mfinance.core.callcenter.model;

import org.seuksa.frmk.model.entity.MEntityA;

/**
 * Meta data of com.soma.mfinance.core.callcenter.model.CallCenterHistory
 * @author kimsuor.seang
 */
public interface MCallCenterHistory extends MEntityA {
	
	// For Vaadin Grid
	public final static String ACTION = "action";
	
	public final static String CONTRACT = "contract";
	public final static String RESULT = "result";
	public final static String COMMENT = "comment";

}
