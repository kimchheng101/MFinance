package com.soma.mfinance.third.creditbureau.exception;

import com.soma.mfinance.core.shared.exception.BusinessException;

/**
 * Parser Credit Bureau Exception
 * @author kimsuor.seang
 */
public class ParserCreditBureauException extends BusinessException {

	private static final long serialVersionUID = -1876516919210851235L;

	public ParserCreditBureauException(String message) {
		this(message, null);
	}
	
	public ParserCreditBureauException(String message, Throwable e) {
		super(message, e);
	}
}
