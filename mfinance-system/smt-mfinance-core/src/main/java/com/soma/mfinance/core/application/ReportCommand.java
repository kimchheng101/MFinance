package com.soma.mfinance.core.application;

import com.soma.mfinance.core.application.panel.ApplicationFormPanel;
import com.soma.mfinance.core.contract.model.schedule.ContractActivateSchedule;
import com.soma.mfinance.core.document.panel.DocumentViewver;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.conf.AppConfig;
import com.soma.mfinance.core.shared.report.ReportParameter;
import com.soma.mfinance.third.finwiz.client.ap.ClientBankAccount;
import com.soma.mfinance.core.application.panel.ApplicationFormPanel;
import com.soma.mfinance.tools.report.Report;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.UI;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.util.List;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/24/2017
 * Name  : 3:48 PM
 * Email : k.seang@gl-f.com
 */
public class ReportCommand implements MenuBar.Command, FinServicesHelper {

    protected final static Logger LOG = LoggerFactory.getLogger(ClientBankAccount.class);
    private static final long serialVersionUID = -1436430428258990868L;
    private Class<? extends Report> reportClass;
    private Quotation quotation;
    private ApplicationFormPanel applicationFormPanel;

    public ReportCommand(Class<? extends Report> reportClass) {
        this.reportClass = reportClass;
    }


    public ReportCommand(Class<? extends Report> reportClass, Quotation quotation) {
        this.reportClass = reportClass;
        this.quotation = quotation;
    }

    public ReportCommand(Class<? extends Report> reportClass, ApplicationFormPanel applicationFormPanel) {
        this.reportClass = reportClass;
        this.applicationFormPanel = applicationFormPanel;
    }

    @Override
    public void menuSelected(MenuBar.MenuItem selectedItem) {
        try {
            Assert.notNull(quotation, "Quotation could not be null.");
            ReportParameter reportParameter = new ReportParameter();
            reportParameter.addParameter("quotaId", quotation.getId());
            reportParameter.addParameter("stamp", true);

            String fileName = REPORT_SRV.extract(reportClass, reportParameter);
            String fileDir = "";
            fileDir = AppConfig.getInstance().getConfiguration().getString("specific.tmpdir");
            DocumentViewver documentViewver = new DocumentViewver(I18N.message(""), fileDir + "/" + fileName);
            UI.getCurrent().addWindow(documentViewver);
        } catch (Exception e) {
            LOG.error("", e);
        }
    }

    public static boolean isQuotationHasInScheduleTable(Long id) {
        BaseRestrictions<ContractActivateSchedule> contractActivateScheduleBaseRestrictions = new BaseRestrictions<>(ContractActivateSchedule.class);
        contractActivateScheduleBaseRestrictions.addAssociation("quotation", JoinType.LEFT_OUTER_JOIN);
        List<ContractActivateSchedule> contractActivateSchedulesList = ENTITY_SRV.list(contractActivateScheduleBaseRestrictions);
        for (ContractActivateSchedule contractActivateSchedule : contractActivateSchedulesList) {
            Long quotationId = contractActivateSchedule.getQuotation().getId();
            if (quotationId == id) {
                return false;
            }
        }
        return true;
    }

}
