package com.soma.mfinance.core.asset.model.appraisal;

import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * @author by kimsuor.seang  on 11/6/2017.
 */
@MappedSuperclass
public abstract class AbstractAssetAppraisal extends EntityA {

    private List<HistoryItem> historyItems;
    private Long monthsOfUse;
    private Long hoursOfUse;
    private Double priceOfBrandNew;
    private Double priceOfSecondhand;
    private Double depreciatePerMonth;
    private Double depreciate;
    private Double costOfRepair;
    private Double costOfChange;
    private Double priceBeforeAdjustment;
    private Double priceAfterAdjustmentAppraisal;
    private Double priceAfterAdjustment;
    private Double adjustment;
    private Double leaseAmount;
    private Double leaseAmountAppraisal;
    private Double leaseAmountCommittee;

    @OneToMany(mappedBy="assetAppraisal", fetch = FetchType.LAZY)
    public List<HistoryItem> getHistoryItems() {
        return historyItems;
    }
    public void setHistoryItems(List<HistoryItem> historyItems) {
        this.historyItems = historyItems;
    }

    @Column(name = "app_month_use", nullable = true)
    public Long getMonthsOfUse() {
        return monthsOfUse;
    }
    public void setMonthsOfUse(Long monthsOfUse) {
        this.monthsOfUse = monthsOfUse;
    }

    @Column(name = "app_hours_use", nullable = true)
    public Long getHoursOfUse() {
        return hoursOfUse;
    }

    public void setHoursOfUse(Long hoursOfUse) {
        this.hoursOfUse = hoursOfUse;
    }

    @Column(name = "app_price_brand_new", nullable = true)
    public Double getPriceOfBrandNew() {
        return priceOfBrandNew;
    }

    public void setPriceOfBrandNew(Double priceOfBrandNew) {
        this.priceOfBrandNew = priceOfBrandNew;
    }

    @Column(name = "app_price_secondhand", nullable = true)
    public Double getPriceOfSecondhand() {
        return priceOfSecondhand;
    }

    public void setPriceOfSecondhand(Double priceOfSecondhand) {
        this.priceOfSecondhand = priceOfSecondhand;
    }

    @Column(name = "app_depreciate_month", nullable = true)
    public Double getDepreciatePerMonth() {
        return depreciatePerMonth;
    }

    public void setDepreciatePerMonth(Double depreciatePerMonth) {
        this.depreciatePerMonth = depreciatePerMonth;
    }

    @Column(name = "app_depreciate", nullable = true)
    public Double getDepreciate() {
        return depreciate;
    }

    public void setDepreciate(Double depreciate) {
        this.depreciate = depreciate;
    }

    @Column(name = "app_cost_repair", nullable = true)
    public Double getCostOfRepair() {
        return costOfRepair;
    }

    public void setCostOfRepair(Double costOfRepair) {
        this.costOfRepair = costOfRepair;
    }

    @Column(name = "app_cost_change", nullable = true)
    public Double getCostOfChange() {
        return costOfChange;
    }

    public void setCostOfChange(Double costOfChange) {
        this.costOfChange = costOfChange;
    }

    @Column(name = "app_price_before_adjustment", nullable = true)
    public Double getPriceBeforeAdjustment() {
        return priceBeforeAdjustment;
    }

    public void setPriceBeforeAdjustment(Double priceBeforeAdjustment) {
        this.priceBeforeAdjustment = priceBeforeAdjustment;
    }

    @Column(name = "app_price_before_adjustment_appraisal", nullable = true)
    public Double getPriceAfterAdjustmentAppraisal() {
        return priceAfterAdjustmentAppraisal;
    }

    public void setPriceAfterAdjustmentAppraisal(Double priceAfterAdjustmentAppraisal) {
        this.priceAfterAdjustmentAppraisal = priceAfterAdjustmentAppraisal;
    }

    @Column(name = "app_price_after_adjustment", nullable = true)
    public Double getPriceAfterAdjustment() {
        return priceAfterAdjustment;
    }

    public void setPriceAfterAdjustment(Double priceAfterAdjustment) {
        this.priceAfterAdjustment = priceAfterAdjustment;
    }

    @Column(name = "app_adjustment", nullable = true)
    public Double getAdjustment() {
        return adjustment;
    }

    public void setAdjustment(Double adjustment) {
        this.adjustment = adjustment;
    }

    @Column(name = "app_lease_amount", nullable = true)
    public Double getLeaseAmount() {
        return leaseAmount;
    }

    public void setLeaseAmount(Double leaseAmount) {
        this.leaseAmount = leaseAmount;
    }

    @Column(name = "app_lease_amount_appraisal", nullable = true)
    public Double getLeaseAmountAppraisal() {
        return leaseAmountAppraisal;
    }

    public void setLeaseAmountAppraisal(Double leaseAmountAppraisal) {
        this.leaseAmountAppraisal = leaseAmountAppraisal;
    }

    @Column(name = "app_lease_amount_committee", nullable = true)
    public Double getLeaseAmountCommittee() {
        return leaseAmountCommittee;
    }

    public void setLeaseAmountCommittee(Double leaseAmountCommittee) {
        this.leaseAmountCommittee = leaseAmountCommittee;
    }
}
