package com.soma.mfinance.core.stock.service;

import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.stock.model.EStockReason;
import com.soma.mfinance.core.stock.model.Product;
import com.soma.mfinance.core.stock.model.ProductStock;

/**
 * @author kimsuor.seang
 */
public interface StockService extends BaseEntityService {
	
	ProductStock getProductStock(Dealer dealer, String code);
	void saveStock(Dealer dealer, String code, Integer initialQty, Integer qty);
	void saveStock(Dealer dealer, String code, Integer qty, EStockReason stockReason);	
	void saveStock(Dealer dealer, Product product, Integer qty, EStockReason stockReason);
}