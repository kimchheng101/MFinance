package com.soma.mfinance.core.asset.panel.assetprice;

import com.soma.mfinance.core.auction.model.AssetPrice;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.soma.mfinance.core.auction.model.AssetPrice;
import com.vaadin.ui.Table;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by cheasocheat on 3/23/17.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AssetPriceConfigTablePanel extends AbstractTablePanel<AssetPrice> implements FMEntityField{

    @PostConstruct
    public void PostConstruct(){
        super.init(I18N.message("asset.prices"));
        setCaption(I18N.message("asset.prices"));
        setSizeFull();
        setMargin(true);
        setSpacing(true);
        addDefaultNavigation();
    }
    @Override
    protected AssetPrice getEntity() {
        final Long id = getItemSelectedId();
        return id != null ? ENTITY_SRV.getById(AssetPrice.class, id) : null;
    }

    @Override
    protected PagedDataProvider<AssetPrice> createPagedDataProvider() {
        PagedDefinition<AssetPrice> pagedDefinition = new PagedDefinition<>(searchPanel.getRestrictions());
        pagedDefinition.addColumnDefinition(ID, I18N.message("id").toUpperCase(), Long.class, Table.Align.LEFT, 100);
        pagedDefinition.addColumnDefinition(CODE, I18N.message("code"), String.class, Table.Align.LEFT, 100);
        pagedDefinition.addColumnDefinition(ASSET_MAKE + "." + DESC_EN, I18N.message("asset.make"), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition(ASSET_RANGE + "." + DESC_EN, I18N.message("asset.range"), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition(ASSET_MODEL + "." + DESC_EN, I18N.message("asset.model"), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition(DESC, I18N.message("desc"), String.class, Table.Align.LEFT, 180);
        pagedDefinition.addColumnDefinition(DESC_EN, I18N.message("desc.en"), String.class, Table.Align.LEFT, 180);
        pagedDefinition.addColumnDefinition("price", I18N.message("asset.price"), Double.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("secondHandPrice", I18N.message("asset.secondhand.price"), Double.class, Table.Align.LEFT, 150);
        EntityPagedDataProvider<AssetPrice> pagedDataProvider = new EntityPagedDataProvider<>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    @Override
    protected AbstractSearchPanel<AssetPrice> createSearchPanel() {
        return new AssetPriceConfigSearchPanel(this);
    }
}

