package com.soma.mfinance.core.asset.service.impl;

import java.util.List;

import com.soma.mfinance.core.asset.model.AssetModel;
import com.soma.mfinance.core.asset.model.AssetRange;
import org.apache.commons.lang3.StringUtils;
import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soma.mfinance.core.asset.model.AssetModel;
import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.asset.service.AssetModelRestriction;
import com.soma.mfinance.core.asset.service.AssetModelSequenceImpl;
import com.soma.mfinance.core.asset.service.AssetModelService;
import com.soma.mfinance.core.asset.service.AssetSequenceManager;
import com.soma.mfinance.core.quotation.SequenceGenerator;

/**
 * Asset model service
 * @author kimsuor.seang
 */       
@Service("assetModelService")
public class AssetModelServiceImpl extends BaseEntityServiceImpl implements AssetModelService {
	
	/** */
	private static final long serialVersionUID = -6293684761226832807L;
	
	@Autowired
    private EntityDao dao;
	
	/**
	 * @see org.seuksa.frmk.service.impl.BaseEntityServiceImpl#getDao()
	 */
	@Override
	public BaseEntityDao getDao() {
		return dao;
	}
	
	/**
	 * @see com.soma.mfinance.core.asset.service.AssetModelService#saveOrUpdateAssetModel(AssetModel)
	 */
	@Override
	public void saveOrUpdateAssetModel(AssetModel assetModel) {
		if (assetModel.getCode().length() < 9) {
			String assRangeCode = StringUtils.EMPTY;
			if (assetModel.getAssetRange() != null) {
				assRangeCode = assetModel.getAssetRange().getCode();
			}
			Integer sequence = AssetSequenceManager.getInstance().getSequenceAssetModel(assRangeCode);
			SequenceGenerator sequenceGenerator = new AssetModelSequenceImpl(assRangeCode, sequence);
			assetModel.setCode(sequenceGenerator.generate());
		}
		saveOrUpdate(assetModel);
	}
	
	/**
	 * @see com.soma.mfinance.core.asset.service.AssetModelService#getAssetModelsByAssetRange(AssetRange)
	 */
	@Override
	public List<AssetModel> getAssetModelsByAssetRange(AssetRange assetRange) {
		AssetModelRestriction restrictions = new AssetModelRestriction();
		restrictions.setAssetRange(assetRange);
		return list(restrictions);
	}
	
}
