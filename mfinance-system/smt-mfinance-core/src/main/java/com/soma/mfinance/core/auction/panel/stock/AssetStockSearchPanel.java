package com.soma.mfinance.core.auction.panel.stock;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.auction.model.EStorageLocation;
import com.soma.mfinance.core.collection.model.EStockStatus;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.financial.model.FinProduct;
import com.soma.mfinance.core.shared.contract.ContractEntityField;
import com.soma.mfinance.core.workflow.AuctionWkfStatus;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.mfinance.core.collection.model.EStockStatus;
import com.vaadin.ui.*;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;

import java.util.List;

/**
 * Search panel for auction panel
 * @author kimsuor.seang
 */
public class AssetStockSearchPanel extends AbstractSearchPanel<Contract> implements ContractEntityField {

    private static final long serialVersionUID = 8765174204972444569L;

    private TextField txtReference;
    private EntityRefComboBox<EWkfStatus> cbxAuctionStatus;
    private ERefDataComboBox<EStorageLocation> cbxEStorageLocation;
    private ERefDataComboBox<EStockStatus> cbxEStockStatus;
    private EntityRefComboBox<FinProduct> cbxFinancialProduct;
    private HorizontalLayout horizontalLayout;

    /**
     * @param tablePanel
     */
    public AssetStockSearchPanel(AbstractTablePanel<Contract> tablePanel) {
        super(I18N.message("search"), tablePanel);
    }

    @Override
    protected Component createForm() {

        cbxFinancialProduct = new EntityRefComboBox<FinProduct>(I18N.message("financial.product"));
        BaseRestrictions<FinProduct> restrictionsFinancial = new BaseRestrictions<>(FinProduct.class);
        restrictionsFinancial.getStatusRecordList().add(EStatusRecord.ACTIV);
        cbxFinancialProduct.setRestrictions(restrictionsFinancial);
        cbxFinancialProduct.setImmediate(true);
        cbxFinancialProduct.renderer();
        cbxFinancialProduct.setSelectedEntity(null);
        txtReference = ComponentFactory.getTextField("reference", false, 50, 200);
        cbxEStorageLocation = new ERefDataComboBox<>(I18N.message("storage.location"), EStorageLocation.class);
        cbxEStockStatus = new ERefDataComboBox<EStockStatus>(I18N.message("collection.stock.status"), EStockStatus.class);

        List<EWkfStatus> eWkfStatusList = AuctionWkfStatus.values();
        eWkfStatusList.remove(AuctionWkfStatus.SOL);
        cbxAuctionStatus = new EntityRefComboBox<EWkfStatus>(AuctionWkfStatus.convertAuctionStatus(eWkfStatusList));
        cbxAuctionStatus.setCaption(I18N.message("auction.status"));
        horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);

        horizontalLayout.addComponent(new FormLayout(txtReference));
        horizontalLayout.addComponent(new FormLayout(cbxEStorageLocation));
        horizontalLayout.addComponent(new FormLayout(cbxEStockStatus));
        horizontalLayout.addComponent(new FormLayout(cbxAuctionStatus));

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addComponent(horizontalLayout);
        verticalLayout.addComponent(new FormLayout(cbxFinancialProduct));

        return verticalLayout;
    }

    @Override
    public BaseRestrictions<Contract> getRestrictions() {

        BaseRestrictions<Contract> restrictions = new BaseRestrictions<>(Contract.class);
        restrictions.addCriterion(Restrictions.eq(CONTRACT_STATUS, ContractWkfStatus.REP));

        if (cbxFinancialProduct.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq("financialProduct", cbxFinancialProduct.getSelectedEntity()));
        }
        if (cbxAuctionStatus.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq("auctionStatus", cbxAuctionStatus.getSelectedEntity()));
        }

        if (cbxEStorageLocation.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq("storageLocation", cbxEStorageLocation.getSelectedEntity()));
        }

        if (StringUtils.isNotEmpty(txtReference.getValue())) {
            restrictions.addCriterion(Restrictions.ilike(REFERENCE, txtReference.getValue(), MatchMode.ANYWHERE));
        }

        if (cbxEStockStatus.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq("stockStatus", cbxEStockStatus.getSelectedEntity()));
        }

        return restrictions;
    }

    @Override
    protected void reset() {
        txtReference.setValue("");
        cbxAuctionStatus.setSelectedEntity(null);
        cbxEStorageLocation.setSelectedEntity(null);
        cbxEStockStatus.setSelectedEntity(null);
        cbxFinancialProduct.setSelectedEntity(null);
    }

}
