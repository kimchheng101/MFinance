package com.soma.mfinance.core.applicant.service.impl;

import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soma.mfinance.core.applicant.model.Driver;
import com.soma.mfinance.core.applicant.service.DriverInformationService;
import com.soma.mfinance.core.shared.FMEntityField;

/**
 * 
 * @author kimsuor.seang
 */
@Service("driverInformationService")
public class DriverInformationServiceImpl extends BaseEntityServiceImpl implements DriverInformationService, FMEntityField {

	/** */
	private static final long serialVersionUID = 4269670409353088052L;
	
	@Autowired
    private EntityDao dao;
	
	/**
     * @see org.seuksa.frmk.mvc.service.impl.BaseEntityServiceImpl#getDao()
     */
	@Override
	public EntityDao getDao() {
		return dao;
	}
	
	/**
	 * @see com.soma.mfinance.core.applicant.service.DriverInformationService#saveOrUpdateDriverAddress(com.soma.mfinance.core.applicant.model.Driver)
	 */
	@Override
	public void saveOrUpdateDriverAddress(Driver driver) {
		if (driver.getAddress() != null) {
			saveOrUpdate(driver.getAddress());
		}
		saveOrUpdate(driver);
	}
	
}
