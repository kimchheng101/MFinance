package com.soma.mfinance.glf.report.xls;

import com.soma.mfinance.core.financial.service.FinanceCalculationService;
import com.soma.mfinance.core.quotation.QuotationService;
import com.soma.mfinance.tools.report.XLSAbstractReportExtractor;
import com.soma.frmk.security.context.SecApplicationContextHolder;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;

import java.util.Map;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/24/2017
 * Name  : 4:59 PM
 * Email : k.seang@gl-f.com
 */
public abstract class PaymentSchedule extends XLSAbstractReportExtractor implements GLFPaymentScheduleFields{
    protected Map<String, CellStyle> styles = null;
    protected static String FORMAT_PERCENTAGE1 = "###,###,###,##0.0%";
    protected static String FORMAT_PERCENTAGE2 = "###,###,##0.00%";
    protected static String FORMAT_PERCENTAGE3 = "###,###,###,##0.000%";

    protected static String FORMAT_DECIMAL1 = "###,###,###,##0.0";
    protected static String FORMAT_DECIMAL2 = "###,###,###,##0.00";
    protected static String FORMAT_DECIMAL3 = "###,###,###,##0.000";
    protected static String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
    /** Background color format */
    static short BG_WHITE =  IndexedColors.WHITE.getIndex();
    static short BG_GREEN = IndexedColors.GREEN.getIndex();

    /** Font color */
    static short FC_WHITE = IndexedColors.WHITE.getIndex();
    static short FC_BLACK = IndexedColors.BLACK.getIndex();
    static short FC_BLUE = 48;
    static short FC_GREY = IndexedColors.GREY_80_PERCENT.getIndex();
    static short FC_GREEN = IndexedColors.GREEN.getIndex();

    protected QuotationService quotationService = (QuotationService) SecApplicationContextHolder.getContext().getBean("quotationService");
    protected FinanceCalculationService financeCalculationService = (FinanceCalculationService) SecApplicationContextHolder.getContext().getBean("financeCalculationService");
    protected int term;
    protected Double insuranceFee;
    protected Double servicingFee;
    protected Double vatInstallAmount;
    protected static String PAYMENT_FILENAME = "Payment_Schedule_";
}
