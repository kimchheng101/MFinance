package com.soma.mfinance.core.applicant.panel;

import com.soma.mfinance.core.actor.model.Liability;
import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.mfinance.core.model.actor.SourcePayment;
import com.soma.mfinance.core.shared.system.AmountBigUtils;
import com.soma.mfinance.core.shared.system.AutoTextField;
import com.soma.frmk.security.context.SecApplicationContextHolder;
import com.soma.frmk.vaadin.ui.panel.AbstractControlPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.*;
import com.vaadin.ui.Notification.Type;
import org.apache.commons.lang.BooleanUtils;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.service.EntityService;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author by th.seng  on 10/19/2017.
 */
public class LiabilityPanel extends AbstractControlPanel {
	private static final long serialVersionUID = 8341162309347917428L;

	private EntityService entityService = (EntityService) SecApplicationContextHolder.getContext().getBean("entityService");
	private List<Liability> liabilities;
	private OptionGroup optDebtOtherSource;
	private TextField txtAdvancePaymentPecentage;
	private AutoTextField txtAdvancePayment;
	private TextField txtSourceOfMoney1;
	private TextField txtSourceOfMoney2;
	private TextField txtSourceOfMoney3;
	private TextField txtSourceOfMoneyAmount1;
	private TextField txtSourceOfMoneyAmount2;
	private TextField txtSourceOfMoneyAmount3;
	private AutoTextField txtPersonalMoney;
	private AutoTextField txtOtherMoney;
	private TextField txtRelationship1;
	private TextField txtRelationship2;
	private TextField txtRelationshipAmount1;
	private TextField txtRelationshipAmount2;
	private TextField txtContactNumber1;
	private TextField txtContactNumber2;

	private TextField txtPurposeOfUse1;
	private TextField txtCreditor1;
	private TextField txtTimingOfLoan1;
	private AutoTextField txtRemaininLoan1;
	private AutoTextField txtAnualRepayment1;

	private TextField txtPurposeOfUse2;
	private TextField txtCreditor2;
	private TextField txtTimingOfLoan2;
	private AutoTextField txtRemaininLoan2;
	private AutoTextField txtAnualRepayment2;

	private TextField txtPurposeOfUse3;
	private TextField txtCreditor3;
	private TextField txtTimingOfLoan3;
	private AutoTextField txtRemaininLoan3;
	private AutoTextField txtAnualRepayment3;

	private TextField txtPurposeOfUse4;
	private TextField txtCreditor4;
	private TextField txtTimingOfLoan4;
	private AutoTextField txtRemaininLoan4;
	private AutoTextField txtAnualRepayment4;

	private Liability liability1;
	private Liability liability2;
	private Liability liability3;
	private Liability liability4;

	private Label lblTotalRemainingLoan;
	private Label lblTotalAnualRepayment;

	private SourcePayment sourcePayment;

	public LiabilityPanel() {
		setMargin(true);
		setSpacing(true);
		setSizeFull();
		optDebtOtherSource = new OptionGroup();
		optDebtOtherSource.addItem(1);
		optDebtOtherSource.setItemCaption(1, I18N.message("yes"));
		optDebtOtherSource.addItem(0);
		optDebtOtherSource.setItemCaption(0, I18N.message("no"));
		optDebtOtherSource.select(0);
		optDebtOtherSource.addStyleName("horizontal");


		txtPurposeOfUse1 = ComponentFactory.getTextField(false, 50, 250);
		txtCreditor1 = ComponentFactory.getTextField(false, 50, 180);
		txtTimingOfLoan1 = ComponentFactory.getTextField(false, 50, 80);
		txtRemaininLoan1 = new AutoTextField();
		txtRemaininLoan1.setWidth("80");
		txtAnualRepayment1 = new AutoTextField();
		txtAnualRepayment1.setWidth("80");

		txtPurposeOfUse2 = ComponentFactory.getTextField(false, 50, 250);
		txtCreditor2 = ComponentFactory.getTextField(false, 50, 180);
		txtTimingOfLoan2 = ComponentFactory.getTextField(false, 50, 80);
		txtRemaininLoan2 = new AutoTextField();
		txtRemaininLoan2.setWidth("80");

		txtAnualRepayment2 = new AutoTextField();
		txtAnualRepayment2.setWidth("80");

		txtPurposeOfUse3 = ComponentFactory.getTextField(false, 50, 250);
		txtCreditor3 = ComponentFactory.getTextField(false, 50, 180);
		txtTimingOfLoan3 = ComponentFactory.getTextField(false, 50, 80);
		txtRemaininLoan3 = new AutoTextField();
		txtRemaininLoan3.setWidth("80");

		txtAnualRepayment3 = new AutoTextField();
		txtAnualRepayment3.setWidth("80");

		txtPurposeOfUse4 = ComponentFactory.getTextField(false, 50, 250);
		txtCreditor4 = ComponentFactory.getTextField(false, 50, 180);
		txtTimingOfLoan4 = ComponentFactory.getTextField(false, 50, 80);
		txtRemaininLoan4 =new AutoTextField();
		txtRemaininLoan4.setWidth("80");
		txtAnualRepayment4 = new AutoTextField();
		txtAnualRepayment4.setWidth("80");

		txtAdvancePaymentPecentage = ComponentFactory.getTextField(false, 50, 170);
		txtAdvancePaymentPecentage.setEnabled(false);
		txtAdvancePayment = new AutoTextField();
		txtAdvancePayment.setWidth("120");
		txtAdvancePayment.setEnabled(false);
		txtSourceOfMoney1 = ComponentFactory.getTextField(false, 50, 290);
		txtSourceOfMoney2 = ComponentFactory.getTextField(false, 50, 290);
		txtSourceOfMoney3 = ComponentFactory.getTextField(false, 50, 290);

		txtSourceOfMoneyAmount1 = ComponentFactory.getTextField(false, 50, 60);
		txtSourceOfMoneyAmount2 = ComponentFactory.getTextField(false, 50, 60);
		txtSourceOfMoneyAmount3 = ComponentFactory.getTextField(false, 50, 60);

		txtRelationship1 = ComponentFactory.getTextField(false, 50, 170);
		txtRelationship2 = ComponentFactory.getTextField(false, 50, 170);
		txtRelationshipAmount1 = ComponentFactory.getTextField(false, 50, 60);
		txtRelationshipAmount2 = ComponentFactory.getTextField(false, 50, 60);

		txtPersonalMoney = new AutoTextField();
		txtPersonalMoney.setWidth("170");
		txtOtherMoney = new AutoTextField();
		txtOtherMoney.setWidth("170");

		txtContactNumber1 = ComponentFactory.getTextField(false, 50, 170);
		txtContactNumber2 = ComponentFactory.getTextField(false, 50, 170);

		HorizontalLayout horizontalRelationship1 = new HorizontalLayout();
		HorizontalLayout horizontalRelationship2 = new HorizontalLayout();
		horizontalRelationship1.addComponent(txtRelationship1);
		horizontalRelationship1.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS));
		horizontalRelationship1.addComponent(txtRelationshipAmount1);
		horizontalRelationship1.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS));
		horizontalRelationship1.addComponent(new Label(I18N.message("contact.number")));
		//horizontalRelationship1.addComponent(getSymbolRequired());

		horizontalRelationship2.addComponent(txtRelationship2);
		horizontalRelationship2.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS));
		horizontalRelationship2.addComponent(txtRelationshipAmount2);
		VerticalLayout verticalLayoutRelationship = new VerticalLayout();
		verticalLayoutRelationship.addComponent(horizontalRelationship1);
		verticalLayoutRelationship.addComponent(getSpace("10"));
		verticalLayoutRelationship.addComponent(horizontalRelationship2);

		VerticalLayout verticalLayoutSourceOfMoney = new VerticalLayout();
		verticalLayoutSourceOfMoney.addComponent(txtSourceOfMoney1);
		verticalLayoutSourceOfMoney.addComponent(getSpace("10"));
		verticalLayoutSourceOfMoney.addComponent(txtSourceOfMoney2);
		verticalLayoutSourceOfMoney.addComponent(getSpace("10"));
		verticalLayoutSourceOfMoney.addComponent(txtSourceOfMoney3);

		VerticalLayout verticalSourceOfMoneyAmount = new VerticalLayout();
		verticalSourceOfMoneyAmount.addComponent(txtSourceOfMoneyAmount1);
		verticalSourceOfMoneyAmount.addComponent(getSpace("10"));
		verticalSourceOfMoneyAmount.addComponent(txtSourceOfMoneyAmount2);
		verticalSourceOfMoneyAmount.addComponent(getSpace("10"));
		verticalSourceOfMoneyAmount.addComponent(txtSourceOfMoneyAmount3);

		VerticalLayout verticalLayoutContractNumber = new VerticalLayout();
		verticalLayoutContractNumber.addComponent(txtContactNumber1);
		verticalLayoutContractNumber.addComponent(getSpace("10"));
		verticalLayoutContractNumber.addComponent(txtContactNumber2);

		final GridLayout gridLayout = new GridLayout(10, 10);
		gridLayout.setMargin(true);
		gridLayout.setSpacing(true);
		gridLayout.setSizeFull();

		int iCol = 0;
		int iRow = 0;
		/*gridLayout.addComponent(ComponentFactory.getLabel(I18N.message("advance.payment.percentage"), 120), iCol++, iRow);
		gridLayout.addComponent(txtAdvancePaymentPecentage, iCol++, iRow);
		gridLayout.addComponent(ComponentFactory.getLabel(I18N.message("advance.payment"), 110), iCol++, iRow);
		gridLayout.addComponent(txtAdvancePayment, iCol++, iRow);
		gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, iRow);*/

		iCol = 0;
		iRow = 1;
		//gridLayout.addComponent(getLabelWithRequiredSymbol(ComponentFactory.getLabel(I18N.message("personal.money"),120), true), iCol++, iRow);
		gridLayout.addComponent(ComponentFactory.getLabel(I18N.message("personal.money"),120), iCol++, iRow);
		gridLayout.addComponent(txtPersonalMoney, iCol++, iRow);
		gridLayout.addComponent(ComponentFactory.getLabel(I18N.message("source.of.payment"),110), iCol++, iRow);
		gridLayout.addComponent(verticalLayoutSourceOfMoney, iCol++, iRow);
		gridLayout.addComponent(ComponentFactory.getSpaceLayout(100, Unit.PIXELS), iCol++, iRow);
		gridLayout.addComponent(verticalSourceOfMoneyAmount, iCol++, iRow);

		iCol = 0;
		iRow = 2;
		gridLayout.addComponent(ComponentFactory.getLabel(I18N.message("other.money"),120), iCol++, iRow);
		gridLayout.addComponent(txtOtherMoney, iCol++, iRow);
		gridLayout.addComponent(ComponentFactory.getLabel(I18N.message("relationship"),110), iCol++, iRow);
		gridLayout.addComponent(verticalLayoutRelationship, iCol++, iRow);
		HorizontalLayout horizontalSpace = new HorizontalLayout();
		horizontalSpace.setWidth("150");
		gridLayout.addComponent(horizontalSpace, iCol++, iRow);
		gridLayout.addComponent(verticalLayoutContractNumber, iCol++, iRow);

		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setSizeFull();
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.addComponent(new Label(I18N.message("debt.from.other.source")));
		horizontalLayout.addComponent(getSymbolRequired());
		horizontalLayout.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS));
		horizontalLayout.addComponent(optDebtOtherSource);
		verticalLayout.addComponent(horizontalLayout);

		verticalLayout.addComponents(getSpace("15"));
		verticalLayout.addComponent(getTableLiability());
		verticalLayout.setMargin(true);
		Panel panel = new Panel();
		panel.setContent(verticalLayout);
		Panel panel1 = new Panel();
		panel1.setCaption(I18N.message("source.of.payment"));
		panel1.setContent(gridLayout);
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.addComponent(panel);
		mainLayout.addComponent(getSpace("15"));
		mainLayout.addComponent(panel1);
		addComponent(mainLayout);
	}

	private HorizontalLayout getLabelWithRequiredSymbol(Label label, Boolean isLeftMarging30) {
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.addComponent(label);
		if (isLeftMarging30) {
			horizontalLayout.addComponent(getSymbolRequiredMargingLeft30());
		} else {
			horizontalLayout.addComponent(getSymbolRequired());
		}

		return horizontalLayout;
	}
	/**
	 *
	 * @param space
	 * @return
	 */
	private VerticalLayout getSpace(String space){
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setHeight(space);
		return verticalLayout;
	}
	/**
	 *
	 * @return
	 */
	private Label getSymbolRequired() {
		Label lblSymbolRequired = new Label("*");
		lblSymbolRequired.setStyleName("symbol-require-field");
		return lblSymbolRequired;
	}

	/**
	 *
	 * @return
	 */
	private Label getSymbolRequiredMargingLeft30() {
		Label lblSymbolRequired = new Label("*");
		lblSymbolRequired.setStyleName("marging-left30");
		return lblSymbolRequired;
	}
	/**
	 *
	 * @return table
	 */
	private CustomLayout getTableLiability() {
		CustomLayout customLayout = null;
		InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/liability.html");
		try {
			customLayout = new CustomLayout(layoutFile);
			customLayout.setCaption(I18N.message("liability"));
		} catch (IOException e) {
			Notification.show("Could not locate template liability.html", e.getMessage(), Type.ERROR_MESSAGE);
		}

		customLayout.setSizeFull();
		//Header
		customLayout.addComponent(new Label(I18N.message("creditor")), "lblCreditor");
		customLayout.addComponent(new Label(I18N.message("purpose.of.use")), "lblPurposeOfUse");
		customLayout.addComponent(new Label(I18N.message("timing.of.loan")), "lblTimingOfLoan");
		customLayout.addComponent(new Label(I18N.message("remaining.loan.amount")), "lblRemaininLoan");
		customLayout.addComponent(new Label(I18N.message("anual.repayment.amount")), "lblAnualRepayment");
		//Body
		customLayout.addComponent(txtCreditor1, "txtCreditor1");
		customLayout.addComponent(txtPurposeOfUse1, "txtPurposeOfUse1");
		customLayout.addComponent(txtTimingOfLoan1, "txtTimingOfLoan1");
		customLayout.addComponent(txtRemaininLoan1, "txtRemaininLoan1");
		customLayout.addComponent(txtAnualRepayment1, "txtAnualRepayment1");

		customLayout.addComponent(txtCreditor2, "txtCreditor2");
		customLayout.addComponent(txtPurposeOfUse2, "txtPurposeOfUse2");
		customLayout.addComponent(txtTimingOfLoan2, "txtTimingOfLoan2");
		customLayout.addComponent(txtRemaininLoan2, "txtRemaininLoan2");
		customLayout.addComponent(txtAnualRepayment2, "txtAnualRepayment2");

		customLayout.addComponent(txtCreditor3, "txtCreditor3");
		customLayout.addComponent(txtPurposeOfUse3, "txtPurposeOfUse3");
		customLayout.addComponent(txtTimingOfLoan3, "txtTimingOfLoan3");
		customLayout.addComponent(txtRemaininLoan3, "txtRemaininLoan3");
		customLayout.addComponent(txtAnualRepayment3, "txtAnualRepayment3");

		customLayout.addComponent(txtCreditor4, "txtCreditor4");
		customLayout.addComponent(txtPurposeOfUse4, "txtPurposeOfUse4");
		customLayout.addComponent(txtTimingOfLoan4, "txtTimingOfLoan4");
		customLayout.addComponent(txtRemaininLoan4, "txtRemaininLoan4");
		customLayout.addComponent(txtAnualRepayment4, "txtAnualRepayment4");
		lblTotalRemainingLoan = new Label("0.0");
		lblTotalAnualRepayment = new Label("0.0");
		customLayout.addComponent(lblTotalRemainingLoan, "lblTotalRemaininLoan");
		customLayout.addComponent(lblTotalAnualRepayment, "lblTotalAnualRepayment");

		return customLayout;
	}

	public Individual getLiabilityPanel(Individual individual) {
		if (individual != null) {
			if (liabilities != null && !liabilities.isEmpty()) {
				liability1 = liabilities.get(0);
				liability2 = liabilities.get(1);
				liability3 = liabilities.get(2);
				liability4 = liabilities.get(3);
			} else {
				liability1 = new Liability();
				liability2 = new Liability();
				liability3 = new Liability();
				liability4 = new Liability();
			}
			liabilities = getListLiabilities(liability1,liability2,liability3,liability4,individual);
			saveLiability(liabilities);
			individual.setLiabilities(liabilities);

			SourcePayment sourcePayment = getSourcePayment(individual);
			sourcePayment.setIndividual(individual);
			entityService.saveOrUpdate(sourcePayment);
			individual.setSourcePayment(sourcePayment);
			individual.setDebtFromOtherSource(Integer.parseInt(optDebtOtherSource.getValue().toString()) == 1);
		}

		return individual;
	}
	/**
	 *
	 * @return sourcePayment
	 */
	private SourcePayment getSourcePayment(Individual individual) {
		sourcePayment = individual.getSourcePayment();
		if (sourcePayment == null) {
			sourcePayment = new SourcePayment();
		}
		sourcePayment.setPersonalMoney(getDoubleValue(AmountBigUtils.getValueDouble(txtPersonalMoney)));
		sourcePayment.setOtherMoney(getDoubleValue(AmountBigUtils.getValueDouble(txtOtherMoney)));
		sourcePayment.setSourceOfMoneyLabel1(txtSourceOfMoney1.getValue());
		sourcePayment.setSourceOfMoneyLabel2(txtSourceOfMoney2.getValue());
		sourcePayment.setSourceOfMoneyLabel3(txtSourceOfMoney3.getValue());
		sourcePayment.setSourceOfMoney1(getDoubleValue(getDouble(txtSourceOfMoneyAmount1)));
		sourcePayment.setSourceOfMoney2(getDoubleValue(getDouble(txtSourceOfMoneyAmount2)));
		sourcePayment.setSourceOfMoney3(getDoubleValue(getDouble(txtSourceOfMoneyAmount3)));
		sourcePayment.setRelationshipLabel1(txtRelationship1.getValue());
		sourcePayment.setRelationshipLabel2(txtRelationship2.getValue());
		sourcePayment.setRelationship1(getDoubleValue(getDouble(txtRelationshipAmount1)));
		sourcePayment.setRelationship2(getDoubleValue(getDouble(txtRelationshipAmount2)));
		sourcePayment.setContactNumber1(txtContactNumber1.getValue());
		sourcePayment.setContactNumber2(txtContactNumber2.getValue());
		entityService.saveOrUpdate(sourcePayment);
		return sourcePayment;
	}
	private void saveLiability(List<Liability> liabilities) {
		for(Liability liability : liabilities){
			entityService.saveOrUpdate(liability);
		}
	}
	/**
	 *
	 * @param liability1
	 * @param liability2
	 * @param liability3
	 * @param liability4
	 * @return
	 */
	private List<Liability> getListLiabilities(Liability liability1,
											   Liability liability2, Liability liability3, Liability liability4, Individual individual) {
		List<Liability> lstLiabilities = new ArrayList<Liability>();
		entityService.saveOrUpdate(individual);
		liability1.setCreditor(getDefaultString(txtCreditor1.getValue()));
		liability1.setPurposeOfUse(getDefaultString(txtPurposeOfUse1.getValue()));
		liability1.setTimingLoan(getIntValue(getInteger(txtTimingOfLoan1)));
		liability1.setRemainingLoan(getDoubleValue(AmountBigUtils.getValueDouble(txtRemaininLoan1)));
		liability1.setAnnualPayment(getDoubleValue(AmountBigUtils.getValueDouble(txtAnualRepayment1)));
		liability1.setIndividual(individual);

		liability2.setCreditor(getDefaultString(txtCreditor2.getValue()));
		liability2.setPurposeOfUse(getDefaultString(txtPurposeOfUse2.getValue()));
		liability2.setTimingLoan(getIntValue(getInteger(txtTimingOfLoan2)));
		liability2.setRemainingLoan(getDoubleValue(AmountBigUtils.getValueDouble(txtRemaininLoan2)));
		liability2.setAnnualPayment(getDoubleValue(AmountBigUtils.getValueDouble(txtAnualRepayment2)));
		liability2.setIndividual(individual);

		liability3.setCreditor(getDefaultString(txtCreditor3.getValue()));
		liability3.setPurposeOfUse(getDefaultString(txtPurposeOfUse3.getValue()));
		liability3.setTimingLoan(getIntValue(getInteger(txtTimingOfLoan3)));
		liability3.setRemainingLoan(getDoubleValue(AmountBigUtils.getValueDouble(txtRemaininLoan3)));
		liability3.setAnnualPayment(getDoubleValue(AmountBigUtils.getValueDouble(txtAnualRepayment3)));
		liability3.setIndividual(individual);

		liability4.setCreditor(getDefaultString(txtCreditor4.getValue()));
		liability4.setPurposeOfUse(getDefaultString(txtPurposeOfUse4.getValue()));
		liability4.setTimingLoan(getIntValue(getInteger(txtTimingOfLoan4)));
		liability4.setRemainingLoan(getDoubleValue(AmountBigUtils.getValueDouble(txtRemaininLoan4)));
		liability4.setAnnualPayment(getDoubleValue(AmountBigUtils.getValueDouble(txtAnualRepayment4)));
		liability4.setIndividual(individual);


		lstLiabilities.add(liability1);
		lstLiabilities.add(liability2);
		lstLiabilities.add(liability3);
		lstLiabilities.add(liability4);
		return lstLiabilities;
	}

	public void assignValues(Individual individual) {
		reset();
		if (individual != null) {
			this.sourcePayment = individual.getSourcePayment();
			if (individual.getLiabilities() != null
					&& !individual.getLiabilities().isEmpty()) {
				this.liabilities = individual.getLiabilities();
				setValueToLiabilityForm(liabilities);
			}
			if (sourcePayment == null) {
				sourcePayment = new SourcePayment();
			} else {
				assignValueToSourcePayment(sourcePayment);
			}
			getValueAdvancePayment(individual);
			optDebtOtherSource.setValue(BooleanUtils.toBoolean(individual.isDebtFromOtherSource()) ? 1 : 0);
		}
	}

	private void getValueAdvancePayment(Individual individual) {
		if (individual.getQuotationApplicants() != null
				&& !individual.getQuotationApplicants().isEmpty()) {
			//Quotation quotation = applicant.getQuotationApplicants().get(0).getQuotation();
			/*if (individual != null) {
				txtAdvancePayment.setValue(AmountBigUtils.format(individual.getTiAdvancePaymentUsd()));
				txtAdvancePaymentPecentage.setValue(AmountUtils.format(individual.getAdvancePaymentPercentage()));
			}*/
		}
	}
	/**
	 *
	 * @param sourcePayment
	 */
	private void assignValueToSourcePayment(SourcePayment sourcePayment) {
		txtPersonalMoney.setValue(getDefaultString(AmountBigUtils.format(sourcePayment.getPersonalMoney())));
		txtOtherMoney.setValue(getDefaultString(AmountBigUtils.format(sourcePayment.getOtherMoney())));
		txtRelationshipAmount1.setValue(getDouble(sourcePayment.getRelationship1()));
		txtRelationshipAmount2.setValue(getDouble(sourcePayment.getRelationship2()));
		txtRelationship1.setValue(sourcePayment.getRelationshipLabel1());
		txtRelationship2.setValue(sourcePayment.getRelationshipLabel2());
		txtSourceOfMoney1.setValue(sourcePayment.getSourceOfMoneyLabel1());
		txtSourceOfMoney2.setValue(sourcePayment.getSourceOfMoneyLabel2());
		txtSourceOfMoney3.setValue(sourcePayment.getSourceOfMoneyLabel3());
		txtSourceOfMoneyAmount1.setValue(getDouble(sourcePayment.getSourceOfMoney1()));
		txtSourceOfMoneyAmount2.setValue(getDouble(sourcePayment.getSourceOfMoney2()));
		txtSourceOfMoneyAmount3.setValue(getDouble(sourcePayment.getSourceOfMoney3()));
		txtContactNumber1.setValue(sourcePayment.getContactNumber1());
		txtContactNumber2.setValue(sourcePayment.getContactNumber2());
	}
	/**
	 *
	 * @param value
	 * @return
	 */
	private String getDouble(Double value) {
		if (value != null && !value.equals(0d)) {
			return value.toString();
		} else {
			return "";
		}
	}
	/**
	 *
	 * @param liabilities
	 */
	private void setValueToLiabilityForm(List<Liability> liabilities) {
		Liability liability1 = liabilities.get(0);
		Liability liability2 = liabilities.get(1);
		Liability liability3 = liabilities.get(2);
		Liability liability4 = liabilities.get(3);

		txtCreditor1.setValue(getDefaultString(liability1.getCreditor()));
		txtPurposeOfUse1.setValue(getDefaultString(liability1.getPurposeOfUse()));
		txtTimingOfLoan1.setValue(getDefaultString(liability1.getTimingLoan()));
		txtRemaininLoan1.setValue(getDefaultString(AmountBigUtils.format(liability1.getRemainingLoan())));
		txtAnualRepayment1.setValue(getDefaultString(AmountBigUtils.format(liability1.getAnnualPayment())));

		txtCreditor2.setValue(getDefaultString(liability2.getCreditor()));
		txtPurposeOfUse2.setValue(getDefaultString(liability2.getPurposeOfUse()));
		txtTimingOfLoan2.setValue(getDefaultString(liability2.getTimingLoan()));
		txtRemaininLoan2.setValue(getDefaultString(AmountBigUtils.format(liability2.getRemainingLoan())));
		txtAnualRepayment2.setValue(getDefaultString(AmountBigUtils.format(liability2.getAnnualPayment())));

		txtCreditor3.setValue(getDefaultString(liability3.getCreditor()));
		txtPurposeOfUse3.setValue(getDefaultString(liability3.getPurposeOfUse()));
		txtTimingOfLoan3.setValue(getDefaultString(liability3.getTimingLoan()));
		txtRemaininLoan3.setValue(getDefaultString(AmountBigUtils.format(liability3.getRemainingLoan())));
		txtAnualRepayment3.setValue(getDefaultString(AmountBigUtils.format(liability3.getAnnualPayment())));

		txtCreditor4.setValue(getDefaultString(liability4.getCreditor()));
		txtPurposeOfUse4.setValue(getDefaultString(liability4.getPurposeOfUse()));
		txtTimingOfLoan4.setValue(getDefaultString(liability4.getTimingLoan()));
		txtRemaininLoan4.setValue(getDefaultString(AmountBigUtils.format(liability4.getRemainingLoan())));
		txtAnualRepayment4.setValue(getDefaultString(AmountBigUtils.format(liability4.getAnnualPayment())));
		Double totalRemainingLoan = liability1.getRemainingLoan() + liability2.getRemainingLoan() + liability3.getRemainingLoan() + liability4.getRemainingLoan();
		Double totalAnualRepayment = liability1.getAnnualPayment() + liability2.getAnnualPayment() + liability3.getAnnualPayment() + liability4.getAnnualPayment();
		lblTotalRemainingLoan.setValue(AmountBigUtils.format(totalRemainingLoan));
		lblTotalAnualRepayment.setValue(AmountBigUtils.format(totalAnualRepayment));

	}
	/**
	 * Reset panel
	 */
	public void reset() {
		optDebtOtherSource.setValue(0);
		txtCreditor1.setValue("");
		txtPurposeOfUse1.setValue("");
		txtTimingOfLoan1.setValue("");
		txtRemaininLoan1.setValue("");
		txtAnualRepayment1.setValue("");

		txtCreditor2.setValue("");
		txtPurposeOfUse2.setValue("");
		txtTimingOfLoan2.setValue("");
		txtRemaininLoan2.setValue("");
		txtAnualRepayment2.setValue("");

		txtCreditor3.setValue("");
		txtPurposeOfUse3.setValue("");
		txtTimingOfLoan3.setValue("");
		txtRemaininLoan3.setValue("");
		txtAnualRepayment3.setValue("");

		txtCreditor4.setValue("");
		txtPurposeOfUse4.setValue("");
		txtTimingOfLoan4.setValue("");
		txtRemaininLoan4.setValue("");
		txtAnualRepayment4.setValue("");

		lblTotalRemainingLoan.setValue("");
		lblTotalAnualRepayment.setValue("");


		resetSourcePayment();
	}

	private void resetSourcePayment() {
		txtPersonalMoney.setValue("");
		txtOtherMoney.setValue("");
		txtRelationship1.setValue("");
		txtRelationship2.setValue("");
		txtSourceOfMoney1.setValue("");
		txtSourceOfMoney2.setValue("");
		txtSourceOfMoney3.setValue("");
		txtContactNumber1.setValue("");
		txtContactNumber2.setValue("");
		txtAdvancePayment.setValue("");
		txtAdvancePaymentPecentage.setValue("");
		txtSourceOfMoneyAmount1.setValue("");
		txtSourceOfMoneyAmount2.setValue("");
		txtSourceOfMoneyAmount3.setValue("");
		txtRelationshipAmount1.setValue("");
		txtRelationshipAmount2.setValue("");
	}
	/**
	 * @return
	 */
	public List<String> validate() {
		super.reset();

		return errors;
	}

	/**
	 * @return
	 */
	public List<String> fullValidate() {
		super.reset();
/*		checkMandatoryField(txtPersonalMoney, "personal.money");
		checkMandatoryField(txtOtherMoney, "other.money");
		checkMandatoryField(txtSourceOfMoneyAmount1, "source.of.payment");
		checkMandatoryField(txtRelationship1, "relationship");
		checkMandatoryField(txtContactNumber1, "contact.number");*/

		return errors;
	}

	private Integer getIntValue(Integer value) {
		if(value == null){
			return 0;
		} else {
			return value;
		}
	}

	private Double getDoubleValue(Double value) {
		if(value == null){
			return 0d;
		} else {
			return value;
		}
	}

}
