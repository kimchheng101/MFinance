package com.soma.mfinance.core.custom.component;

import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedTable;
import com.soma.frmk.vaadin.util.exporter.CSVExporter;
import com.soma.frmk.vaadin.util.exporter.ExcelExporter;
import com.soma.frmk.vaadin.util.filebuilder.FileBuilder;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.themes.Reindeer;
import org.seuksa.frmk.model.entity.Entity;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Created by kaokimchheng on 5/18/17.
 */
public class ApplicationEntityPagedTable<T extends Entity> extends EntityPagedTable<T> {

    public ApplicationEntityPagedTable(PagedDataProvider<T> dataProvider) {
        super(dataProvider);
    }

    public ApplicationEntityPagedTable(String caption, PagedDataProvider<T> dataProvider) {
        super(caption, dataProvider);
    }

    public HorizontalLayout createExportBar() {
        HorizontalLayout exportBar = new HorizontalLayout();
        final Button excelExporter = new Button("", new Button.ClickListener() {
            private static final long serialVersionUID = -345128294252780849L;
            public void buttonClick(Button.ClickEvent event) {
                try {
                    final ExcelExporter excelExp = new ExcelExporter();
                    IndexedContainer tmpContainer = getDataProvider().getIndexedContainer(0, (int) getDataProvider().getTotalRecords());
                    excelExp.setContainerToBeExported(tmpContainer);
                    Field field = ExcelExporter.class.getSuperclass().getDeclaredField("fileBuilder");
                    if(field!=null){
                        field.setAccessible(true);
                        FileBuilder fileBuilder= (FileBuilder) field.get(excelExp);
                        fileBuilder.setDescriptions(new ArrayList<>());
                    }
                    excelExp.sendConvertedFileToUser(getUI());
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });
        excelExporter.setIcon(new ThemeResource("../smt-default/icons/16/excel.png"));
        excelExporter.setStyleName(Reindeer.BUTTON_LINK);

        final Button csvExporter = new Button("", new Button.ClickListener() {
            private static final long serialVersionUID = 1544211032887048179L;
            public void buttonClick(Button.ClickEvent event) {
                final CSVExporter csvlExp = new CSVExporter();
                IndexedContainer tmpContainer = getDataProvider().getIndexedContainer(0, (int) getDataProvider().getTotalRecords());
                csvlExp.setContainerToBeExported(tmpContainer);
                csvlExp.sendConvertedFileToUser(getUI());
            }
        });
        csvExporter.setIcon(new ThemeResource("../smt-default/icons/16/csv.png"));
        csvExporter.setStyleName(Reindeer.BUTTON_LINK);

        exportBar.addComponent(excelExporter);
        exportBar.addComponent(csvExporter);
        exportBar.setSpacing(true);
        exportBar.setWidth(null);

        return exportBar;
    }
}
