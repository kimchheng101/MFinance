package com.soma.mfinance.core.quotation.model;

import com.soma.common.app.workflow.model.WkfBaseHistory;

import javax.persistence.*;
import java.util.List;

/**
 * 
 * @author kimsuor.seang
 *
 */
@Entity
@Table(name = "td_quotation_wkf_history")
public class QuotationWkfHistory extends WkfBaseHistory {
	/** */
	private static final long serialVersionUID = -3148816956742357734L;

	@Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "wkf_his_id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}
	
	@OneToMany(mappedBy = "wkfHistory", fetch = FetchType.LAZY)
	@Override
	public List<QuotationWkfHistoryItem> getHistItems() {
		return (List<QuotationWkfHistoryItem>) super.getHistItems();
	}

}
