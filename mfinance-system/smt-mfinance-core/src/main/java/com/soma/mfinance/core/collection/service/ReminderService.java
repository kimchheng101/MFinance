package com.soma.mfinance.core.collection.service;

import java.util.List;

import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.collection.model.Reminder;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.quotation.model.Comment;

/**
 * Reminder service interface
 * @author kimsuor.seang
 */
public interface ReminderService extends BaseEntityService {
	
	/**
	 * saveOrUpdate Reminder
	 * @param reminder
	 */
	void saveOrUpdateReminder(Reminder reminder);
	
	/**
	 * saveOrUpdate Comment
	 * @param comment
	 */
	void saveOrUpdateComment(Comment comment);
	
	/**
	 * 
	 * @param contract
	 * @return
	 */
	List<Reminder> getReminderByContract(Contract contract);
}
