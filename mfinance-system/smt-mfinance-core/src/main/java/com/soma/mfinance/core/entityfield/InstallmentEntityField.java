package com.soma.mfinance.core.entityfield;

import com.soma.mfinance.core.shared.FMEntityField;
/**
 * 
 * @author vi.sok
 *
 */
public interface InstallmentEntityField extends FMEntityField {
	String RECEIVER = "receiver";
	String NUM_INSTALLMENT = "installment.number";
	String INSTALLMENT_AMOUNT = "installment.amount";
	String PRINCIPAL_AMOUNT = "principal.amount";
	String INTEREST_AMOUNT = "interest.amount";
	String INSURANCE_FEE = "insurance.fee";
	String SERVICING_FEE = "servicing.fee";
	String TRANSFER_FEE = "transfer.fee";
	String TRANSFER_AMOUNT = "transfer.amount";
	String TOTAL_FEE_VAT = "total.fee.vat";
	String OTHER_AMOUNT = "other.amount";
	String VAT = "vat";
	String NUM_PENALTY_DAY = "no.penalty.days";
	String NUM_OVERDUE_DAY = "no.overdue.days";
	String PENALTY_AMOUNT = "penalty.amount";
	String TOTAL_PAYMENT = "total.payment";
	String CONTRACT = "contract";
	String DEALER = "dealer";
	String PAYMENT_METHOD = "payment.method";
	String COMMISSION = "commission";
	String TOTAL_INSTALLMENT_AMOUNT = "total.installment.amount";
	String REMAINING_BALANCE = "remaining.balance";
	String TERM="term";
	String CONTRACT_STATUS="contract.status";
}
