package com.soma.mfinance.core.referential.creditcommitteeappraisal.model;

import com.soma.mfinance.core.asset.model.AssetModel;
import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.frmk.security.model.SecUser;
import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;

/**
 * @author by kimsuor.seang  on 9/27/2017.
 */
@Entity
@Table(name = "tu_appraisal_credit_committee")
public class AppraisalCreditCommittee extends EntityA {

    private SecUser secUser;
    private AssetModel assetModel;
    private AssetRange assetRange;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "app_cre_com_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_uer_id")
    public SecUser getSecUser() {
        return secUser;
    }

    public void setSecUser(SecUser secUser) {
        this.secUser = secUser;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ass_mod_id", nullable = false)
    public AssetModel getAssetModel() {
        return assetModel;
    }

    public void setAssetModel(AssetModel assetModel) {
        this.assetModel = assetModel;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ass_ran_id")
    public AssetRange getAssetRange() {
        return assetRange;
    }

    public void setAssetRange(AssetRange assetRange) {
        this.assetRange = assetRange;
    }
}
