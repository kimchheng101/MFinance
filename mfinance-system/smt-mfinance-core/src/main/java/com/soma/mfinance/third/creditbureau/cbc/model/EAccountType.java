package com.soma.mfinance.third.creditbureau.cbc.model;

import org.seuksa.frmk.model.eref.BaseERefData;

import javax.persistence.AttributeConverter;
import java.util.List;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/11/2017
 * Name  : 4:56 PM
 * Email : k.seang@gl-f.com
 */
public class EAccountType extends BaseERefData implements AttributeConverter<EProductType, Long> {



    public final static EAccountType J = new EAccountType("J", 1);
    public final static EAccountType S = new EAccountType("S", 2);
    public final static EAccountType G = new EAccountType("G", 3);

    /**
     *
     */
    public EAccountType() {
    }

    /**
     * @param code
     * @param id
     */
    public EAccountType(String code, long id) {
        super(code, id);
    }

    @Override
    public Long convertToDatabaseColumn(EProductType attribute) {
        return super.convertToDatabaseColumn(attribute);
    }

    @Override
    public EProductType convertToEntityAttribute(Long dbData) {
        return super.convertToEntityAttribute(dbData);
    }


    /**
     *
     * @return
     */
    public static List<EAccountType> values() {
        return getValues(EAccountType.class);
    }

    /**
     *
     * @param code
     * @return
     */
    public static EAccountType getByCode(String code) {
        return getByCode(EAccountType.class, code);
    }

    /**
     *
     * @param id
     * @return
     */
    public static EProductType getById(long id) {
        return getById(EProductType.class, id);
    }
}
