package com.soma.mfinance.tools.barcode;

import java.io.File;

import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.shared.conf.AppConfig;

/**
 * @author kimsuor.seang
 */
public class ContractBarcode {
	
	private static final String BARCODE_PATH = "/barcode/contract_old";
	
	/**
	 * @param contract
	 * @return
	 */
	public static File create(Contract contract) {
		String pathName = AppConfig.getInstance().getConfiguration().getString("document.path") + BARCODE_PATH;
		File path = new File(pathName);
		if (!path.exists()) {
			path.mkdirs();
    	}
		
		String fileName = path + "/" + contract.getReference() + ".png";
		File file = new File(fileName);
		if (!file.exists()) {
			Barcode.create(contract.getReference(), file);
		}
		return file;
	}

}
