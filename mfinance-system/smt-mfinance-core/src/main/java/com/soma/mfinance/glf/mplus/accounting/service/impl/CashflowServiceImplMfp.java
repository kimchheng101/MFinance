package com.soma.mfinance.glf.mplus.accounting.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyMathUtils;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.amount.Amount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.financial.service.FinanceCalculationService;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.mplus.accounting.CashflowServiceMfp;
import com.soma.finance.services.shared.AmortizationSchedules;
import com.soma.finance.services.shared.CalculationParameter;
import com.soma.finance.services.shared.Schedule;

/**
 * 
 * @author vi.sok
 * @since 05/05/2017
 */
@Service("cashflowServiceMfp")
@Transactional
public class CashflowServiceImplMfp extends BaseEntityServiceImpl implements CashflowServiceMfp {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2395938011631250310L;
	protected Logger LOG = LoggerFactory.getLogger(getClass());
	
	@Autowired
    private EntityDao dao;
	@Autowired
	private FinanceCalculationService financeCalculationService;

	@Override
	public List<Schedule> getCashflows(Contract contract) {

		Date contractStartDate = contract.getStartDate();
		if(contractStartDate == null){
			contractStartDate = DateUtils.today();
		}
		Date firstPaymentDate = contract.getFirstDueDate();
		if (firstPaymentDate == null) {
			firstPaymentDate = DateUtils.today();
		}
        CalculationParameter calculationParameter = new CalculationParameter();
        calculationParameter.setFrequency(contract.getFrequency());
        calculationParameter.setInitialPrincipal(contract.getTiFinancedAmount());
        calculationParameter.setNumberOfPeriods(contract.getTerm());
        calculationParameter.setPeriodicInterestRate(contract.getInterestRate() / 100);
        calculationParameter.setNumberOfPrincipalGracePeriods(MyNumberUtils.getInteger(contract.getNumberOfPrincipalGracePeriods()));
        Amount installmentAmount = MyMathUtils.calculateFromAmountIncl(financeCalculationService.getInstallmentPayment(calculationParameter), contract.getVatValue(), 2);
        calculationParameter.setInstallmentAmount(installmentAmount.getTiAmount());
		AmortizationSchedules amortizationSchedules = financeCalculationService.getAmortizationSchedules(contractStartDate, firstPaymentDate, calculationParameter);
		int nbSchedulePerYear = contract.getFrequency().getNbSchedulePerYear();
		double nbYear = amortizationSchedules.getSchedules().size() / (double) nbSchedulePerYear;
		int indexValue = 0;
		List<Schedule> schedules = new ArrayList<Schedule>();
		for (int i = 0; i < nbYear; i++) {
	            indexValue = i + 1;
	            int lastIndex = indexValue * nbSchedulePerYear;
	            if (lastIndex > amortizationSchedules.getSchedules().size()) {
	                lastIndex = amortizationSchedules.getSchedules().size();
	            }
	            for (int j = (indexValue - 1) * nbSchedulePerYear; j < lastIndex; j++) {
	                schedules.add(amortizationSchedules.getSchedules().get(j));
	            }
	        }
		return schedules;
	}

	@Override
	public List<Schedule> getCashflows(Quotation quotation, Date firstPaymentDate) {
		Date contractStartDate = quotation.getActivationDate();
		if(contractStartDate == null){
			contractStartDate = DateUtils.today();
		}
        CalculationParameter calculationParameter = new CalculationParameter();
        calculationParameter.setFrequency(quotation.getFrequency());
        calculationParameter.setInitialPrincipal(quotation.getTiFinanceAmount());
        calculationParameter.setNumberOfPeriods(quotation.getTerm());
        calculationParameter.setPeriodicInterestRate(quotation.getInterestRate() / 100);
        calculationParameter.setNumberOfPrincipalGracePeriods(MyNumberUtils.getInteger(quotation.getNumberOfPrincipalGracePeriods()));
        Amount installmentAmount = MyMathUtils.calculateFromAmountIncl(financeCalculationService.getInstallmentPayment(calculationParameter), quotation.getVatValue(), 2);
        calculationParameter.setInstallmentAmount(installmentAmount.getTiAmount());
		AmortizationSchedules amortizationSchedules = financeCalculationService.getAmortizationSchedules(contractStartDate, firstPaymentDate, calculationParameter);
		int nbSchedulePerYear = quotation.getFrequency().getNbSchedulePerYear();
		double nbYear = amortizationSchedules.getSchedules().size() / (double) nbSchedulePerYear;
		int indexValue = 0;
		List<Schedule> schedules = new ArrayList<Schedule>();
		for (int i = 0; i < nbYear; i++) {
	            indexValue = i + 1;
	            int lastIndex = indexValue * nbSchedulePerYear;
	            if (lastIndex > amortizationSchedules.getSchedules().size()) {
	                lastIndex = amortizationSchedules.getSchedules().size();
	            }
	            for (int j = (indexValue - 1) * nbSchedulePerYear; j < lastIndex; j++) {
	                schedules.add(amortizationSchedules.getSchedules().get(j));
	            }
	        }
		return schedules;
	}
	
	@Override
	public BaseEntityDao getDao() {
		// TODO Auto-generated method stub
		return dao;
	}

}
