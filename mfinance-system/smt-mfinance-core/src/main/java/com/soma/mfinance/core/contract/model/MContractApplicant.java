package com.soma.mfinance.core.contract.model;


/**
 * Meta data of com.soma.mfinance.core.contract_old.model.ContractApplicant
 * @author kimsuor.seang
 *
 */
public interface MContractApplicant {
	
	public final static String APPLICANT = "applicant";
	public final static String CONTRACT = "contract";
	public final static String APPLICANTTYPE = "applicantType";

}
