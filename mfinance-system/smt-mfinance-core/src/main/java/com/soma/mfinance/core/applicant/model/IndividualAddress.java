package com.soma.mfinance.core.applicant.model;

import javax.persistence.*;

import com.soma.mfinance.core.Enum.EHousing;
import org.seuksa.frmk.model.EntityFactory;
import org.seuksa.frmk.model.entity.EntityA;

import com.soma.ersys.core.hr.model.address.Address;

/**
 * 
 * @author kimsuor.seang
 *
 */
@Entity
@Table(name = "td_individual_address")
public class IndividualAddress extends EntityA {
	/** */
	private static final long serialVersionUID = 7556450149731798268L;

	private Individual individual;
	private Address address;
	private EHousing housing;
	private String zipCode;
	private Integer timeAtAddressInYear;
	private Integer timeAtAddressInMonth;
	
	/**
     * 
     * @return
     */
    public static IndividualAddress createInstance() {
    	IndividualAddress instance = EntityFactory.createInstance(IndividualAddress.class);
        return instance;
    }
	
	@Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ind_add_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

	/**
	 * @return the individual
	 */
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ind_id")
	public Individual getIndividual() {
		return individual;
	}

	/**
	 * @param individual the individual to set
	 */
	public void setIndividual(Individual individual) {
		this.individual = individual;
	}

	/**
	 * @return the address
	 */
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "add_id")
	public Address getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}

	@Column(
			name = "hos_id",
			nullable = true
	)
	@Convert(
			converter = EHousing.class
	)
	public EHousing getHousing() {
		return this.housing;
	}

	public void setHousing(EHousing housing) {
		this.housing = housing;
	}

	@Column(
			name = "zip_code",
			nullable = true,
			length = 10
	)
	public String getZipCode() {
		return this.zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@Column(
			name = "tim_address_year",
			nullable = true,
			length = 10
	)
	public Integer getTimeAtAddressInYear() {
		return this.timeAtAddressInYear;
	}

	public void setTimeAtAddressInYear(Integer timeAtAddressInYear) {
		this.timeAtAddressInYear = timeAtAddressInYear;
	}

	@Column(
			name = "tim_address_month",
			nullable = true,
			length = 10
	)
	public Integer getTimeAtAddressInMonth() {
		return this.timeAtAddressInMonth;
	}

	public void setTimeAtAddressInMonth(Integer timeAtAddressInMonth) {
		this.timeAtAddressInMonth = timeAtAddressInMonth;
	}
	
}
