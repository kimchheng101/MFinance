package com.soma.mfinance.core.financial.model;

import com.soma.finance.services.shared.system.EFrequency;
import org.seuksa.frmk.model.eref.BaseERefData;

import javax.persistence.AttributeConverter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sr.soth on 10/31/2017.
 */
public class EAdjustLeaseAmount extends BaseERefData implements AttributeConverter<EAdjustLeaseAmount, Long> {

    public final static EAdjustLeaseAmount P0 = new EAdjustLeaseAmount("P0", 3851l); // 0$
    public final static EAdjustLeaseAmount P10 = new EAdjustLeaseAmount("P10", 3852l); // 10$
    public final static EAdjustLeaseAmount P20 = new EAdjustLeaseAmount("P20", 3853l); // 20$
    public final static EAdjustLeaseAmount P30 = new EAdjustLeaseAmount("P30", 3854l); // 30$
    public final static EAdjustLeaseAmount P40 = new EAdjustLeaseAmount("P40", 3855l); // 40$
    public final static EAdjustLeaseAmount P50 = new EAdjustLeaseAmount("P50", 3856l); // 50$
    public final static EAdjustLeaseAmount P60 = new EAdjustLeaseAmount("P50", 3857l); // 60$
    public final static EAdjustLeaseAmount P70 = new EAdjustLeaseAmount("P50", 3858l); // 70$
    public final static EAdjustLeaseAmount P80 = new EAdjustLeaseAmount("P50", 3859l); // 80$
    public final static EAdjustLeaseAmount P90 = new EAdjustLeaseAmount("P50", 3860l); // 90$
    public final static EAdjustLeaseAmount P100 = new EAdjustLeaseAmount("P50", 3861l); // 100$

    public EAdjustLeaseAmount() {

    }

    public EAdjustLeaseAmount(String code, long id) {
        super(code, id);
    }

    public static EAdjustLeaseAmount getByCode(String code) {
        return getByCode(EAdjustLeaseAmount.class, code);
    }

    public static EAdjustLeaseAmount getById(long id) {
        return getById(EAdjustLeaseAmount.class, id);
    }

    public static EAdjustLeaseAmount getDefault() {
        return getDefault(EAdjustLeaseAmount.class);
    }

    public static List<EAdjustLeaseAmount> values() {
        return getValues(EAdjustLeaseAmount.class);
    }

    @SuppressWarnings("unchecked")
    @Override
    public EAdjustLeaseAmount convertToEntityAttribute(Long id) {
        return super.convertToEntityAttribute(id);
    }

    @Override
    public Long convertToDatabaseColumn(EAdjustLeaseAmount arg0) {
        return super.convertToDatabaseColumn(arg0);
    }
}
