package com.soma.mfinance.glf.report.pdf;

import com.soma.mfinance.tools.report.Report;
import com.soma.mfinance.core.shared.report.ReportParameter;

/**
 * @author kimsuor.seang
 *
 */
public class GLFLeasingContractLegalKH implements Report {

	public GLFLeasingContractLegalKH() {
	}

	@Override
	public String generate(ReportParameter reportParameter) throws Exception {
		boolean stamp = (Boolean) reportParameter.getParameters().get("stamp"); 
        return stamp ? "GLFLeasingContractLegalStamp.pdf" : "GLFLeasingContractLegal.pdf";
	}
	
}
