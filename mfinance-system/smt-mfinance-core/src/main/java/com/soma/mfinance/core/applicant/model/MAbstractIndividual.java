package com.soma.mfinance.core.applicant.model;

/**
 * Meta data of com.soma.mfinance.core.applicant.model.AbstractIndividual
 * @author kimsuor.seang
 */
public interface MAbstractIndividual {
	
	public final static String REFERENCE = "reference";

}
