package com.soma.mfinance.core.asset.panel;

import com.soma.common.app.eref.EProductLineCode;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.asset.model.appraisal.AssetAppraisal;
import com.soma.mfinance.core.asset.panel.appraisal.AppraisalKFPPanel;
import com.soma.mfinance.core.asset.panel.appraisal.AppraisalPanel;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.quotation.QuotationProfileUtils;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.mfinance.core.asset.panel.appraisal.AppraisalKFPPanel;
import com.soma.mfinance.core.asset.panel.appraisal.AppraisalPanel;
import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import org.seuksa.frmk.i18n.I18N;

import java.util.ArrayList;
import java.util.List;

import static com.soma.mfinance.core.helper.FinServicesHelper.ASSET_APPRAISAL_SERVICE;
import static com.soma.mfinance.core.helper.FinServicesHelper.PRODUCT_LINE_SERVICE;

/**
 * Created by cheasocheat on 3/1/17.
 */
public class AssetTabPanel extends AbstractTabPanel implements TabSheet.SelectedTabChangeListener {

    private TabSheet assetTabSheet;
    private AssetPanel assetPanel;
    private AppraisalPanel appraisalPanel;
    private Asset asset;
    private Quotation quotation;
    private AssetAppraisal assetAppraisal;
    private AppraisalKFPPanel appraisalKFPPanel; //for kubota
    private AssetRange assetRange;

    @Override
    protected Component createForm() {
        assetAppraisal = new AssetAppraisal();
        assetTabSheet = new TabSheet();
        assetPanel = new AssetPanel();
        assetPanel.setAsset(Asset.createInstance());
        appraisalPanel = new AppraisalPanel();
        appraisalKFPPanel = new AppraisalKFPPanel();
        displayTabs();
        assetTabSheet.setSelectedTab(assetPanel);
        assetTabSheet.addSelectedTabChangeListener(this);
        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setSpacing(true);
        contentLayout.addComponent(assetTabSheet);
        assetRange = new AssetRange();
        return contentLayout;
    }

    public void assignValues(Quotation quotation) {
        reset();
        if (quotation != null) {
            this.quotation = quotation;
            if(quotation.getAsset() != null) {
                asset = quotation.getAsset();

                assetPanel.reset();
                assetPanel.assignValues(quotation);

                if(appraisalPanel != null){
                    appraisalPanel.reset();
                    appraisalPanel.assignValue(quotation);
                }

                if(appraisalKFPPanel != null){
                    appraisalKFPPanel.reset();
                    appraisalKFPPanel.assignValues(quotation);
                }

            } else {
                assetPanel.setAsset(Asset.createInstance());
                appraisalPanel.setAssetAppraisal(AssetAppraisal.createInstance());
                appraisalKFPPanel.setAssetAppraisal(AssetAppraisal.createInstance());
            }
            assetTabSheet.setSelectedTab(assetPanel);
            /*Set Enable asset for ADMIN*/
            if (!ProfileUtil.isAdmin()){
                appraisalPanel.setEnabled(QuotationProfileUtils.isEnabledAppraisalTab(quotation));
            }
        }
    }

    /* Display Tab Sheet */
    private void displayTabs() {
        assetTabSheet.addTab(assetPanel, I18N.message("asset"));
        if (assetRange != null) {
            if (PRODUCT_LINE_SERVICE.getEProductLineCode(assetRange).equals(EProductLineCode.KFP)) {
                assetTabSheet.addTab(appraisalPanel, I18N.message("asset.appraisal"));
            } else if (PRODUCT_LINE_SERVICE.getEProductLineCode(assetRange).equals(EProductLineCode.MFP)) {
                assetTabSheet.addTab(appraisalKFPPanel, I18N.message("asset.appraisal"));
            }
        }
        assetPanel.setAssetTabPanel(this);
    }

    public Quotation getAsset() {
        asset = assetPanel.getAsset();
        if (this.assetRange != null) {
            if(PRODUCT_LINE_SERVICE.getEProductLineCode(this.assetRange) != null){
                if (PRODUCT_LINE_SERVICE.getEProductLineCode(this.assetRange).equals(EProductLineCode.MFP)) {
                    //In case there are have asset groupdetail have been fill
                    if (appraisalPanel.getAssetAppraisal() != null) {
                        assetAppraisal = appraisalPanel.getAssetAppraisal();

                        asset.setAssetAppraisal(assetAppraisal);
                        if (assetAppraisal.getAssetSecondPrice() != null && assetAppraisal.getAssetSecondPrice() != 0) {
                            asset.setTeAssetSecondHandPrice(assetAppraisal.getAssetSecondPrice());
                        }
                        if (assetAppraisal.getAssetOrigPrice() != null && assetAppraisal.getAssetOrigPrice() != 0) {
                            asset.setTeAssetPrice(assetAppraisal.getAssetOrigPrice());
                        }
                        if (assetAppraisal.getOrigAppraisalPrice() != null && assetAppraisal.getOrigAppraisalPrice() != 0) {
                            asset.setTeAssetApprPrice(assetAppraisal.getOrigAppraisalPrice());
                            asset.setTiAssetApprPrice(assetAppraisal.getOrigAppraisalPrice());
                        }

                    }
                } else if (PRODUCT_LINE_SERVICE.getEProductLineCode(this.assetRange).equals(EProductLineCode.KFP)) {
                    assetAppraisal = appraisalKFPPanel.getAssetAppraisal();
                    if (assetAppraisal != null) {
                        asset.setAssetAppraisal(assetAppraisal);
                    }
                }
            }
            this.quotation.setAsset(this.asset);
        }


        return this.quotation;
    }

    public List<String> isValid() {
        List<String> error = new ArrayList<>();
        if (assetTabSheet.getSelectedTab() == assetPanel) {
            error = assetPanel.isValid();
        }
        if (assetTabSheet.getSelectedTab() == appraisalPanel) {
            error = appraisalPanel.isValid();
        }
        if (assetTabSheet.getSelectedTab() == appraisalKFPPanel) {
            error = appraisalKFPPanel.isValid();
        }
        return error;
    }

    public void reset() {
        super.reset();
        assetPanel.assetModelId = 0;
        assetPanel.reset();
        if (appraisalPanel != null) {
            appraisalPanel.reset();
        }
        if (appraisalKFPPanel != null) {
            appraisalKFPPanel.reset();
        }
        if (getAppraisalPanel() != null) {
            removeAppraisalPanel();
        }
        assetTabSheet.setSelectedTab(assetPanel);
    }

    public AssetPanel getAssetPanel() {
        return assetPanel;
    }

    public AppraisalPanel getAppraisalPanel() {
        return appraisalPanel;
    }

    @Override
    public void selectedTabChange(TabSheet.SelectedTabChangeEvent event) {
        AbstractTabPanel selectedTab = (AbstractTabPanel) event.getTabSheet().getSelectedTab();
        if (selectedTab == appraisalPanel) {
            asset = assetPanel.getAsset();
            quotation.setAsset(asset);
            appraisalPanel.reset();
            appraisalPanel.assignValue(this.quotation);
            appraisalPanel.setClickTrigger(true);
        } else if (selectedTab == assetPanel) {
            assetPanel.setHashError(appraisalPanel.getHashError());
            assetPanel.assignValues(quotation);
            appraisalPanel.setClickTrigger(false);
        } else if (selectedTab == appraisalKFPPanel) {
            appraisalKFPPanel.reset();
            appraisalKFPPanel.assignValues(this.quotation);
        }
    }

    public void removeAppraisalPanel() {
        assetTabSheet.removeComponent(appraisalPanel);
        assetTabSheet.removeComponent(appraisalKFPPanel);
    }

    public void addAppraisalPanel(AssetRange assetRange) {
        this.assetRange = assetRange;
        EProductLineCode eProductLineCode = null;
        if (assetRange != null) {
            if(PRODUCT_LINE_SERVICE.getEProductLineCode(assetRange) !=  null){
                eProductLineCode = PRODUCT_LINE_SERVICE.getEProductLineCode(assetRange);
                if(eProductLineCode != null){
                    if(eProductLineCode.getId() != null){
                        if (PRODUCT_LINE_SERVICE.getEProductLineCode(assetRange).equals(EProductLineCode.MFP)) {
                            if (assetTabSheet.getTab(appraisalPanel) == null) {
                                assetTabSheet.addTab(appraisalPanel, I18N.message("asset.appraisal"));
                                if (assetRange != null) {
                                    appraisalPanel.buildMainLayoutByAprraisalCatecory(ASSET_APPRAISAL_SERVICE.getAppraisalCategoriesByAssetRang(assetRange));
                                }
                            }
                        } else if (PRODUCT_LINE_SERVICE.getEProductLineCode(assetRange).equals(EProductLineCode.KFP)) {
                            eProductLineCode = PRODUCT_LINE_SERVICE.getEProductLineCode(assetRange);
                            if(eProductLineCode != null){
                                if(eProductLineCode.getId() != null){
                                    if (assetTabSheet.getTab(appraisalKFPPanel) == null) {
                                        assetTabSheet.addTab(appraisalKFPPanel, I18N.message("asset.appraisal"));
                                    }
                                    if (assetRange != null) {
                                        appraisalKFPPanel.getAppraisalCategories(ASSET_APPRAISAL_SERVICE.getAppraisalCategoriesByAssetRang(assetRange));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }
}
