package com.soma.mfinance.core.custom.component;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.seuksa.frmk.i18n.I18N;
import org.vaadin.dialogs.ConfirmDialog;

import java.util.List;
import java.util.Map;

/**
 * Created by ki.kao on 4/10/2017.
 */
public class CustomPopUp {

    public CustomPopUp(Map<String, List<String>> map) {
        ConfirmDialog cd = ConfirmDialog.getFactory().create("Title", "Message", "okCaption",
                "cancelCaption", "notOkCaption");
        cd.setHeight("240px");
        cd.setWidth("400px");
        VerticalLayout content = new VerticalLayout();
        for (Map.Entry<String, List<String>> entry : map.entrySet()) {
            VerticalLayout panel = new VerticalLayout();
            Label title = new Label("<div style='width:100%;background-color:#C0C0C0'><h4 style='margin-left:10px;display:inline;color:red;'>" + I18N.message(entry.getKey()) + "</h4></div>");
            title.setContentMode(ContentMode.HTML);
            panel.addComponent(title);
            VerticalLayout verticalLayout = new VerticalLayout();
            for (String message : entry.getValue()) {
                Label label = new Label("<p style='margin-left:10px;display: inline'>" + message + "</p>");
                label.setContentMode(ContentMode.HTML);
                verticalLayout.addComponent(label);
            }
            panel.addComponent(verticalLayout);
            content.addComponent(panel);
        }
        //content.setComponentAlignment(buttons, Alignment.BOTTOM_RIGHT);
        //content.setSizeFull();
        cd.setContent(content);
        cd.show(UI.getCurrent(), null, true);
    }
}
