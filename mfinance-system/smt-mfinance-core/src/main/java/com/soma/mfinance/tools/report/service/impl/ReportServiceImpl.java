package com.soma.mfinance.tools.report.service.impl;

import org.springframework.stereotype.Service;

import com.soma.mfinance.core.shared.report.ReportParameter;
import com.soma.mfinance.tools.report.Report;
import com.soma.mfinance.tools.report.service.ReportService;

/**
 * Report service
 * @author kimsuor.seang
 */
@Service("reportService")
public class ReportServiceImpl implements ReportService {

	@Override
	public String extract(Class<? extends Report> reportClass, ReportParameter reportParameter) throws Exception {
		Report report = reportClass.newInstance();
		return report.generate(reportParameter);
	}
}
