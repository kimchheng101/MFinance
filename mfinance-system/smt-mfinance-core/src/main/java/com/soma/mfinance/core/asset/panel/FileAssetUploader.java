package com.soma.mfinance.core.asset.panel;

import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.shared.conf.AppConfig;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;
import org.seuksa.frmk.service.EntityService;
import org.seuksa.frmk.tools.spring.SpringUtils;

import java.io.*;
import java.util.Date;

public class FileAssetUploader implements Receiver, SucceededListener, Upload.FailedListener {

    private EntityService entityService = SpringUtils.getBean(EntityService.class);

    private String path;
    private File file;
    private Button btnPath;
    private AssetPanel assetPanel;
    private FileUploadHandler handler;

    private static final long serialVersionUID = 1L;


    public FileAssetUploader(AssetPanel assetPanel, Button btnPath, FileUploadHandler handler) {
        this.btnPath = btnPath;
        this.assetPanel = assetPanel;
        this.handler = handler;
    }

    @Override
    public void uploadSucceeded(SucceededEvent event) {
        btnPath.setData(path);
        btnPath.setVisible(true);
        if (handler != null) {
            handler.onSuccess(file, path, btnPath);
        }
    }

    @Override
    public void uploadFailed(Upload.FailedEvent event) {
        if (handler != null)
            handler.onFailed(event.getReason().getMessage());
    }

    @Override
    public OutputStream receiveUpload(String filename, String mimeType) {

        OutputStream fos = new ByteArrayOutputStream();
        try {
            Asset asset = assetPanel.getAsset();
            if (filename.isEmpty() || filename == null || filename == ""){
                this.assetPanel.messageEmpty();
            }else {
                String documentDir = AppConfig.getInstance().getConfiguration().getString("document.path");
                String dirPath = "AssetDocuments/" + asset.getId() + "/" + (new Date().getTime());
                path = dirPath + "/" + filename;
                File tmpDirPath = new File(documentDir + "/" + dirPath);
                if (!tmpDirPath.exists()) {
                    tmpDirPath.mkdirs();
                }
                file = new File(documentDir + "/" + path);
                fos = new FileOutputStream(file);
            }

        } catch (FileNotFoundException e) {
            Notification.show("Could not open file<br/>", e.getMessage(), Type.ERROR_MESSAGE);
            return null;
        }
        return fos;
    }


    public interface FileUploadHandler {
        void onSuccess(File file, String path, Button button);

        void onFailed(String message);
    }
}
