
package com.soma.mfinance.core.workflow;

import com.soma.common.app.workflow.model.EWkfStatus;
import org.seuksa.frmk.tools.reflection.MyClassUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Auction status
 * 
 * @author kimsuor.seang
 *
 */
public class AuctionWkfStatus {
	public final static EWkfStatus EVA = new EWkfStatus("EVA", 40); // evaluation
	public final static EWkfStatus VAL = new EWkfStatus("VAL", 41); // manager.validation
	public final static EWkfStatus WRE = new EWkfStatus("WRE", 42); // waiting.for.result
	public final static EWkfStatus SOL = new EWkfStatus("SOL", 43); // sold
	public final static EWkfStatus CNS = new EWkfStatus("CNS", 44); // cannot.sell

	public static List<EWkfStatus> values() {
		List<EWkfStatus> statuses = (List<EWkfStatus>) MyClassUtils.getStaticValues(AuctionWkfStatus.class, EWkfStatus.class);
		return statuses;
	}

	public static List<EWkfStatus> auctionStatus() {
		List<EWkfStatus> values = new ArrayList<>();
		values.add(EVA);
		values.add(CNS);
		return values;
	}

	public static List<EWkfStatus> convertAuctionStatus(List<EWkfStatus> auctionStatus){
		List<EWkfStatus> eWkfStatuses  = new ArrayList<>();
		for (EWkfStatus eWkfStatus : auctionStatus){
			eWkfStatuses.add(EWkfStatus.getById(EWkfStatus.class, eWkfStatus.getId()));
		}
		return eWkfStatuses;
	}

}