package com.soma.mfinance.core.quotation.model;

import com.soma.common.app.eref.EProductLineCode;
import com.soma.common.app.workflow.WorkflowException;
import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.common.app.workflow.model.EntityWkf;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.ApplicantArc;
import com.soma.mfinance.core.applicant.model.CoVOpinion;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.asset.model.AssetArc;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.document.model.DocumentUwGroup;
import com.soma.mfinance.core.document.model.EConfirmEvidence;
import com.soma.mfinance.core.financial.model.Campaign;
import com.soma.mfinance.core.financial.model.EServiceType;
import com.soma.mfinance.core.financial.model.FinProduct;
import com.soma.mfinance.core.insurance.model.InsuranceStatus;
import com.soma.mfinance.core.insurance.model.QuotationInsuranceStatusHistory;
import com.soma.mfinance.core.model.system.ContactVerification;
import com.soma.mfinance.core.referential.committee.GroupCommittee;
import com.soma.mfinance.core.registrations.model.QuotationRegistrationLocationHistory;
import com.soma.mfinance.core.registrations.model.QuotationRegistrationStatusHistory;
import com.soma.mfinance.core.registrations.model.RegistrationStatus;
import com.soma.mfinance.core.registrations.model.RegistrationStorageLocation;
import com.soma.ersys.core.hr.model.eref.EMediaPromoting;
import com.soma.ersys.core.hr.model.organization.OrgStructure;
import com.soma.finance.services.shared.system.EFrequency;
import com.soma.frmk.security.model.SecUser;
import org.hibernate.annotations.Cascade;
import org.seuksa.frmk.model.EntityFactory;
import org.seuksa.frmk.model.entity.CrudAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Quotation class
 *
 * @author kimsuor.seang
 * @author kimsuor.seang (modified) Date: 06/09/2018
 */
@Entity
@Table(name = "td_quotation", indexes = {
        @Index(name = "idx_quo_va_reference", columnList = "quo_va_reference"),
        @Index(name = "idx_quo_va_external_reference", columnList = "quo_va_external_reference"),
        @Index(name = "inx_quo_va_migration_id", columnList = "quo_va_migration_id"),
        @Index(name = "idx_quo_dea_id", columnList = "dea_id"),
        @Index(name = "idx_quo_fpd_id", columnList = "fpd_id"),
        @Index(name = "idx_quo_cam_id", columnList = "cam_id")
})
public class Quotation extends EntityWkf implements MQuotation {

    private static final long serialVersionUID = 8132370206805536749L;

    private Contract contract;
    private Applicant applicant;
    private ApplicantArc applicantArc;
    private List<AssetInformationDocument> assetInformationDocument;

    private String reference;
    private String externalReference;
    private String transferReference;

    private Dealer dealer;
    private SecUser secUser;
    private SecUser creditOfficer;
    private SecUser productionOfficer;
    private SecUser underwriter;
    private SecUser underwriterSupervisor;
    private SecUser manager;
    private SecUser documentController;
    private SecUser fieldCheck;

    private Asset asset;
    private AssetArc assetArc;

    private FinProduct financialProduct;
    private Campaign campaign;

    private Double tiDefaultFinanceAmount;
    private Double teDefaultFinanceAmount;
    private Double vatDefaultFinanceAmount;

    private Double tiFinanceAmount; //equal Lease Amount
    private Double tmFinanceAmount;
    private Double vatFinanceAmount;

    private Double tiAdvancePaymentAmount;
    private Double teAdvancePaymentAmount;
    private Double vatAdvancePaymentAmount;
    private Double advancePaymentPercentage;
    private Double tiInstallmentAmount;
    private Double teInstallmentAmount;
    private Double vatInstallmentAmount;
    private Double totalInstallmentAmount;
    private Boolean valid;

    private Date startCreationDate;
    private Date quotationDate;
    private Date submissionDate;
    private Date firstSubmissionDate;
    private Date acceptationDate;
    private Date rejectDate;
    private Date declineDate;
    private Date activationDate;
    private Date contractStartDate;
    private Date firstDueDate;
    private Date lastDueDate;
    private Date bookingDate;
    private Date insuranceStartDate;
    private Date insuranceEndYear;

    private Integer term;
    private Double interestRate;
    private Double irrRate;
    private EFrequency frequency;
    private Integer numberOfPrincipalGracePeriods;

    private EPlaceInstallment placeInstallment;
    private EMediaPromoting wayOfKnowing;
    private EMediaPromoting wayOfKnowing1;
    private EMediaPromoting wayOfKnowing2;

    private Double uwRevenuEstimation;
    private Double uwAllowanceEstimation;
    private Double uwNetIncomeEstimation;
    private Double uwBusinessExpensesEstimation;
    private Double uwBusinessIncomesEstimation;
    private Double uwPersonalExpensesEstimation;
    private Double uwFamilyExpensesEstimation;
    private Double uwLiabilityEstimation;

    private Double coRevenuEstimation;
    private Double coAllowanceEstimation;
    private Double coNetIncomeEstimation;
    private Double coBusinessExpensesEstimation;
    private Double coPersonalExpensesEstimation;
    private Double coFamilyExpensesEstimation;
    private Double coLiabilityEstimation;

    private Double coGuarantorRevenuEstimation;
    private Double coGuarantorAllowanceEstimation;
    private Double coGuarantorNetIncomeEstimation;
    private Double coGuarantorBusinessExpensesEstimation;
    private Double coGuarantorPersonalExpensesEstimation;
    private Double coGuarantorFamilyExpensesEstimation;
    private Double coGuarantorLiabilityEstimation;

    //Guarantor
    private Double uwGuarantorRevenuEstimation;
    private Double uwGuarantorAllowanceEstimation;
    private Double uwGuarantorNetIncomeEstimation;
    private Double uwGuarantorBusinessExpensesEstimation;
    private Double uwGuarantorPersonalExpensesEstimation;
    private Double uwGuarantorFamilyExpensesEstimation;
    private Double uwGuarantorLiabilityEstimation;

    private Boolean fieldCheckPerformed = false;
    private Boolean locked;
    private SecUser userLocked;

    private Boolean issueDownPayment;

    private Double totalAR;
    private Double totalUE;
    private Double totalVAT;
    private Double tiPrepaidInstallment;
    private Integer numberPrepaidTerm;
    private Double vatValue;

    private String checkerID;
    private String checkerName;
    private String checkerPhoneNumber;

    private List<Comment> comments;
    private List<QuotationApplicant> quotationApplicants;
    private List<QuotationService> quotationServices;
    private List<QuotationDocument> quotationDocuments;
    private List<QuotationExtModule> quotationExtModules;
    private List<QuotationSupportDecision> quotationSupportDecisions;

    private Integer timestamp;

    private OrgStructure originBranch;
    private String migrationID;
    private Date migrationDate;
    private Double leaseAmountPercentage;
    private Double tiAppraisalEstimateAmount;
    private Double teAppraisalEstimateAmount;
    private EWkfStatus previousWkfStatus;
    //private EWkfStatus quotationStatus;

    private CoVOpinion coVOpinionApplicant;
    private CoVOpinion coVOpinionGuarantor;

    private List<QuotationContactVerification> quotationContactVerifications;
    private List<QuotationContactEvidence> quotationContactEvidences;

    private boolean decline = false;

    // Registration Status
    private List<QuotationRegistrationStatusHistory> quotationRegistrationStatusHistory;
    private List<QuotationRegistrationLocationHistory> quotationRegistrationLocationHistory;
    private RegistrationStatus registrationStatus;
    private RegistrationStorageLocation registrationStorageLocation;
    private Date registrationDeadline;

    // Insurance Status
    private InsuranceStatus insuranceStatus;
    private List<QuotationInsuranceStatusHistory> quotationInsuranceStatusHistory;


    private boolean underwriterApprove = false;
    private boolean underwriterSupervisorApprove = false;
    private Date underwriterSubmitDate;
    private Date underwriterSupervisorSubmitDate;
    private Date firstApplyDate;


    private GroupCommittee secGroupCommittee;
    private Integer numberApproved;

    public static Quotation createInstance() {
        Quotation quotation = EntityFactory.createInstance(Quotation.class);
        quotation.setVatValue(0);
        return quotation;
    }

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "quo_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * @return the reference
     */
    @Column(name = "quo_va_reference", unique = true, nullable = true, length = 20)
    public String getReference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     * @return the externalReference
     */
    @Column(name = "quo_va_external_reference", unique = true, nullable = true, length = 20)
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * @param externalReference the externalReference to set
     */
    public void setExternalReference(String externalReference) {
        this.externalReference = externalReference;
    }

    /**
     * @return the transferReference
     */
    @Transient
    public String getTransferReference() {
        return transferReference;
    }

    /**
     * @param transferReference the transferReference to set
     */
    public void setTransferReference(String transferReference) {
        this.transferReference = transferReference;
    }

    /**
     * @return the contract
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "con_id")
    public Contract getContract() {
        return contract;
    }

    /**
     * @param contract the contract to set
     */
    public void setContract(Contract contract) {
        this.contract = contract;
    }

    /**
     * @return the applicant
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "app_id", referencedColumnName = "app_id")
    public Applicant getApplicant() {
        return applicant;
    }

    /**
     * @param applicant the applicant to set
     */
    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    /**
     * @return the applicantArc
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "arc_app_id")
    public ApplicantArc getApplicantArc() {
        return applicantArc;
    }

    /**
     * @param applicantArc the applicantArc to set
     */
    public void setApplicantArc(ApplicantArc applicantArc) {
        this.applicantArc = applicantArc;
    }

    /**
     * @return the dealer
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dea_id")
    public Dealer getDealer() {
        return dealer;
    }

    /**
     * @param dealer the dealer to set
     */
    public void setDealer(Dealer dealer) {
        this.dealer = dealer;
    }


    /**
     * @return the creditOfficer
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_usr_id_co")
    public SecUser getCreditOfficer() {
        return creditOfficer;
    }

    /**
     * @param creditOfficer the creditOfficer to set
     */
    public void setCreditOfficer(SecUser creditOfficer) {
        this.creditOfficer = creditOfficer;
    }

    /**
     * @return the productionOfficer
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_usr_id_po")
    public SecUser getProductionOfficer() {
        return productionOfficer;
    }

    /**
     * @param productionOfficer the productionOfficer to set
     */
    public void setProductionOfficer(SecUser productionOfficer) {
        this.productionOfficer = productionOfficer;
    }


    /**
     * @return the underwriter
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_usr_id_uw")
    public SecUser getUnderwriter() {
        return underwriter;
    }

    /**
     * @param underwriter the underwriter to set
     */
    public void setUnderwriter(SecUser underwriter) {
        this.underwriter = underwriter;
    }

    /**
     * @return the underwriterSupervisor
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_usr_id_us")
    public SecUser getUnderwriterSupervisor() {
        return underwriterSupervisor;
    }

    /**
     * @param underwriterSupervisor the underwriterSupervisor to set
     */
    public void setUnderwriterSupervisor(SecUser underwriterSupervisor) {
        this.underwriterSupervisor = underwriterSupervisor;
    }

    /**
     * @return the manager
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_usr_id_ma")
    public SecUser getManager() {
        return manager;
    }

    /**
     * @param manager the manager to set
     */
    public void setManager(SecUser manager) {
        this.manager = manager;
    }

    /**
     * @return the documentController
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_usr_id_dc")
    public SecUser getDocumentController() {
        return documentController;
    }

    /**
     * @param documentController the documentController to set
     */
    public void setDocumentController(SecUser documentController) {
        this.documentController = documentController;
    }

    /**
     * @return the fieldCheck
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_usr_id_fc")
    public SecUser getFieldCheck() {
        return fieldCheck;
    }

    /**
     * @param fieldCheck the fieldCheck to set
     */
    public void setFieldCheck(SecUser fieldCheck) {
        this.fieldCheck = fieldCheck;
    }

    /**
     * @return the masset
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ass_id")
    public Asset getAsset() {
        return asset;
    }

    /**
     * @param asset the masset to set
     */
    public void setAsset(Asset asset) {
        this.asset = asset;
    }

    /**
     * @return the assetArc
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ass_id_arc")
    public AssetArc getAssetArc() {
        return assetArc;
    }

    /**
     * @param assetArc the assetArc to set
     */
    public void setAssetArc(AssetArc assetArc) {
        this.assetArc = assetArc;
    }

    /**
     * @return the financialProduct
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @Cascade(value = {org.hibernate.annotations.CascadeType.ALL})
    @JoinColumn(name = "fpd_id", referencedColumnName = "fpd_id")
    public FinProduct getFinancialProduct() {
        return financialProduct;
    }

    /**
     * @param financialProduct the financialProduct to set
     */
    public void setFinancialProduct(FinProduct financialProduct) {
        this.financialProduct = financialProduct;
    }

    /**
     * @return the campaign
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cam_id")
    public Campaign getCampaign() {
        return campaign;
    }

    /**
     * @param campaign the campaign to set
     */
    public void setCampaign(Campaign campaign) {
        this.campaign = campaign;
    }

    /**
     * @return the previousQuotationStatus
     */
    @Column(name = "pre_wkf_sta_id", nullable = true)
    @Convert(converter = EWkfStatus.class)
    public EWkfStatus getPreviousWkfStatus() {
        return previousWkfStatus;
    }

    /**
     * @param previousQuotationStatus the previousQuotationStatus to set
     */
    public void setPreviousWkfStatus(EWkfStatus previousQuotationStatus) {
        this.previousWkfStatus = previousQuotationStatus;

    }

    /**
     * @return the tiDefaultFinanceAmount
     */
    @Column(name = "quo_am_ti_default_finance_amount", nullable = true)
    public Double getTiDefaultFinanceAmount() {
        return tiDefaultFinanceAmount;
    }

    /**
     * @param tiDefaultFinanceAmount the tiDefaultFinanceAmount to set
     */
    public void setTiDefaultFinanceAmount(Double tiDefaultFinanceAmount) {
        this.tiDefaultFinanceAmount = tiDefaultFinanceAmount;
    }

    /**
     * @return the teDefaultFinanceAmount
     */
    @Column(name = "quo_am_te_default_finance_amount", nullable = true)
    public Double getTeDefaultFinanceAmount() {
        return teDefaultFinanceAmount;
    }

    /**
     * @param teDefaultFinanceAmount the teDefaultFinanceAmount to set
     */
    public void setTeDefaultFinanceAmount(Double teDefaultFinanceAmount) {
        this.teDefaultFinanceAmount = teDefaultFinanceAmount;
    }

    /**
     * @return the vatDefaultFinanceAmount
     */
    @Column(name = "quo_am_vat_default_finance_amount", nullable = true)
    public Double getVatDefaultFinanceAmount() {
        return vatDefaultFinanceAmount;
    }

    /**
     * @param vatDefaultFinanceAmount the vatDefaultFinanceAmount to set
     */
    public void setVatDefaultFinanceAmount(Double vatDefaultFinanceAmount) {
        this.vatDefaultFinanceAmount = vatDefaultFinanceAmount;
    }

    /**
     * @return the tiFinanceAmount
     */
    @Column(name = "quo_am_ti_finance_amount", nullable = true)
    public Double getTiFinanceAmount() {
        return tiFinanceAmount;
    }

    /**
     * @param tiFinanceAmount the tiFinanceAmount to set
     */
    public void setTiFinanceAmount(Double tiFinanceAmount) {
        this.tiFinanceAmount = tiFinanceAmount;
    }

    /**
     * @return the tmFinanceAmount
     */
    @Column(name = "quo_am_te_finance_amount", nullable = true)
    public Double getTmFinanceAmount() {
        return tmFinanceAmount;
    }

    /**
     * @param tmFinanceAmount the tmFinanceAmount to set
     */
    public void setTmFinanceAmount(Double tmFinanceAmount) {
        this.tmFinanceAmount = tmFinanceAmount;
    }


    @Column(name = "quo_am_ti_appraisal_estimate_amount", nullable = true)
    public Double getTiAppraisalEstimateAmount() {
        return tiAppraisalEstimateAmount;
    }

    @Column(name = "quo_am_te_appraisal_estimate_amount", nullable = true)
    public Double getTeAppraisalEstimateAmount() {
        return teAppraisalEstimateAmount;
    }

    public void setTiAppraisalEstimateAmount(Double tiAppraisalEstimateAmount) {
        this.tiAppraisalEstimateAmount = tiAppraisalEstimateAmount;
    }

    public void setTeAppraisalEstimateAmount(Double teAppraisalEstimateAmount) {
        this.teAppraisalEstimateAmount = teAppraisalEstimateAmount;
    }

    /**
     * @return the vatFinanceAmount
     */
    @Column(name = "quo_am_vat_finance_amount", nullable = true)
    public Double getVatFinanceAmount() {
        return vatFinanceAmount;
    }

    /**
     * @param vatFinanceAmount the vatFinanceAmount to set
     */
    public void setVatFinanceAmount(Double vatFinanceAmount) {
        this.vatFinanceAmount = vatFinanceAmount;
    }

    /**
     * @return the tiAdvancePaymentAmount
     */
    @Column(name = "quo_am_ti_advance_payment", nullable = true)
    public Double getTiAdvancePaymentAmount() {
        return tiAdvancePaymentAmount;
    }

    /**
     * @param tiAdvancePaymentAmount the tiAdvancePaymentAmount to set
     */
    public void setTiAdvancePaymentAmount(Double tiAdvancePaymentAmount) {
        this.tiAdvancePaymentAmount = tiAdvancePaymentAmount;
    }

    /**
     * @return the teAdvancePaymentAmount
     */
    @Column(name = "quo_am_te_advance_payment_amount", nullable = true)
    public Double getTeAdvancePaymentAmount() {
        return teAdvancePaymentAmount;
    }

    /**
     * @param teAdvancePaymentAmount the teAdvancePaymentAmount to set
     */
    public void setTeAdvancePaymentAmount(Double teAdvancePaymentAmount) {
        this.teAdvancePaymentAmount = teAdvancePaymentAmount;
    }

    /**
     * @return the vatAdvancePaymentAmount
     */
    @Column(name = "quo_am_vat_advance_payment_amount", nullable = true)
    public Double getVatAdvancePaymentAmount() {
        return vatAdvancePaymentAmount;
    }

    /**
     * @param vatAdvancePaymentAmount the vatAdvancePaymentAmount to set
     */
    public void setVatAdvancePaymentAmount(Double vatAdvancePaymentAmount) {
        this.vatAdvancePaymentAmount = vatAdvancePaymentAmount;
    }

    /**
     * @return the advancePaymentPercentage
     */
    @Column(name = "quo_rt_advance_payment_pc", nullable = true)
    public Double getAdvancePaymentPercentage() {
        return advancePaymentPercentage;
    }

    /**
     * @param advancePaymentPercentage the advancePaymentPercentage to set
     */
    public void setAdvancePaymentPercentage(Double advancePaymentPercentage) {
        this.advancePaymentPercentage = advancePaymentPercentage;
    }

    /**
     * @return the term
     */
    @Column(name = "quo_nu_term", nullable = true)
    public Integer getTerm() {
        return term;
    }

    /**
     * @param term the term to set
     */
    public void setTerm(Integer term) {
        this.term = term;
    }

    /**
     * @return the interestRate
     */
    @Column(name = "quo_rt_interest_rate", nullable = true)
    public Double getInterestRate() {
        return interestRate;
    }

    /**
     * @param interestRate the interestRate to set
     */
    public void setInterestRate(Double interestRate) {
        this.interestRate = interestRate;
    }

    /**
     * @return the irrRate
     */
    @Column(name = "quo_rt_irr_rate", nullable = true)
    public Double getIrrRate() {
        return irrRate;
    }

    /**
     * @param irrRate the irrRate to set
     */
    public void setIrrRate(Double irrRate) {
        this.irrRate = irrRate;
    }

    /**
     * @return the tiInstallmentAmount
     */
    @Column(name = "quo_am_ti_installment_amount", nullable = true)
    public Double getTiInstallmentAmount() {
        return tiInstallmentAmount;
    }

    /**
     * @param tiInstallmentAmount the tiInstallmentAmount to set
     */
    public void setTiInstallmentAmount(Double tiInstallmentAmount) {
        this.tiInstallmentAmount = tiInstallmentAmount;
    }

    /**
     * @return the teInstallmentAmount
     */
    @Column(name = "quo_am_te_installment_amount", nullable = true)
    public Double getTeInstallmentAmount() {
        return teInstallmentAmount;
    }

    /**
     * @param teInstallmentAmount the teInstallmentAmount to set
     */
    public void setTeInstallmentAmount(Double teInstallmentAmount) {
        this.teInstallmentAmount = teInstallmentAmount;
    }

    /**
     * @return the vatInstallmentAmount
     */
    @Column(name = "quo_am_vat_installment_amount", nullable = true)
    public Double getVatInstallmentAmount() {
        return vatInstallmentAmount;
    }


    /**
     * @param vatInstallmentAmount the vatInstallmentAmount to set
     */
    public void setVatInstallmentAmount(Double vatInstallmentAmount) {
        this.vatInstallmentAmount = vatInstallmentAmount;
    }

    /**
     * @return the valid
     */
    @Column(name = "quo_bl_valid", nullable = false)
    public Boolean isValid() {
        return valid;
    }

    /**
     * @param valid the valid to set
     */
    public void setValid(Boolean valid) {
        this.valid = valid;
    }


    /**
     * @return the startCreationDate
     */
    @Column(name = "quo_dt_start_creation", nullable = true)
    public Date getStartCreationDate() {
        return startCreationDate;
    }

    /**
     * @param startCreationDate the startCreationDate to set
     */
    public void setStartCreationDate(Date startCreationDate) {
        this.startCreationDate = startCreationDate;
    }

    /**
     * @return the quotationDate
     */
    @Column(name = "quo_dt_quotation", nullable = true)
    public Date getQuotationDate() {
        return quotationDate;
    }

    /**
     * @param quotationDate the quotationDate to set
     */
    public void setQuotationDate(Date quotationDate) {
        this.quotationDate = quotationDate;
    }

    /**
     * @return the submissionDate
     */
    @Column(name = "quo_dt_submission", nullable = true)
    public Date getSubmissionDate() {
        return submissionDate;
    }

    /**
     * @param submissionDate the submissionDate to set
     */
    public void setSubmissionDate(Date submissionDate) {
        this.submissionDate = submissionDate;
    }

    /**
     * @return the firstSubmissionDate
     */
    @Column(name = "quo_dt_first_submission", nullable = true)
    public Date getFirstSubmissionDate() {
        return firstSubmissionDate;
    }

    /**
     * @param firstSubmissionDate the firstSubmissionDate to set
     */
    public void setFirstSubmissionDate(Date firstSubmissionDate) {
        this.firstSubmissionDate = firstSubmissionDate;
    }

    /**
     * @return the acceptationDate
     */
    @Column(name = "quo_dt_acceptation", nullable = true)
    public Date getAcceptationDate() {
        return acceptationDate;
    }

    /**
     * @param acceptationDate the acceptationDate to set
     */
    public void setAcceptationDate(Date acceptationDate) {
        this.acceptationDate = acceptationDate;
    }


    /**
     * @return the rejectDate
     */
    @Column(name = "quo_dt_reject", nullable = true)
    public Date getRejectDate() {
        return rejectDate;
    }

    /**
     * @param rejectDate the rejectDate to set
     */
    public void setRejectDate(Date rejectDate) {
        this.rejectDate = rejectDate;
    }

    /**
     * @return the declineDate
     */
    @Column(name = "quo_dt_decline", nullable = true)
    public Date getDeclineDate() {
        return declineDate;
    }

    /**
     * @param declineDate the declineDate to set
     */
    public void setDeclineDate(Date declineDate) {
        this.declineDate = declineDate;
    }

    /**
     * @return the activationDate
     */
    @Column(name = "quo_dt_activation", nullable = true)
    public Date getActivationDate() {
        return activationDate;
    }

    /**
     * @param activationDate the activationDate to set
     */
    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }


    /**
     * @return the contractStartDate
     */
    @Column(name = "quo_dt_contract_start", nullable = true)
    public Date getContractStartDate() {
        return contractStartDate;
    }

    /**
     * @param contractStartDate the contractStartDate to set
     */
    public void setContractStartDate(Date contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    /**
     * @return the firstDueDate
     */
    @Column(name = "quo_dt_first_due", nullable = true)
    public Date getFirstDueDate() {
        return firstDueDate;
    }

    /**
     * @param firstDueDate the firstDueDate to set
     */
    public void setFirstDueDate(Date firstDueDate) {
        this.firstDueDate = firstDueDate;
    }

    /**
     * @return the lastDueDate
     */
    @Column(name = "quo_dt_last_due", nullable = true)
    public Date getLastDueDate() {
        return lastDueDate;
    }

    /**
     * @param lastDueDate the lastDueDate to set
     */
    public void setLastDueDate(Date lastDueDate) {
        this.lastDueDate = lastDueDate;
    }

    /**
     * @return the bookingDate
     */
    @Column(name = "quo_dt_booking", nullable = true)
    public Date getBookingDate() {
        return bookingDate;
    }

    /**
     * @param bookingDate the bookingDate to set
     */
    public void setBookingDate(Date bookingDate) {
        this.bookingDate = bookingDate;
    }

    @Column(name = "quo_dt_insurance_start", nullable = true)
    public Date getInsuranceStartDate() {
        return insuranceStartDate;
    }

    public void setInsuranceStartDate(Date insuranceStartDate) {
        this.insuranceStartDate = insuranceStartDate;
    }

    @Column(name = "quota_dt_insurance_end_year", nullable = true)
    public Date getInsuranceEndYear() {
        return insuranceEndYear;
    }

    public void setInsuranceEndYear(Date insuranceEndYear) {
        this.insuranceEndYear = insuranceEndYear;
    }

    @Column(name = "fre_id", nullable = true)
    @Convert(converter = EFrequency.class)
    public EFrequency getFrequency() {
        return frequency;
    }

    public void setFrequency(EFrequency frequency) {
        this.frequency = frequency;
    }

    /**
     * @return the numberOfPrincipalGracePeriods
     */
    @Column(name = "quo_nu_principal_grace_periods", nullable = true)
    public Integer getNumberOfPrincipalGracePeriods() {
        return numberOfPrincipalGracePeriods;
    }

    /**
     * @param numberOfPrincipalGracePeriods the numberOfPrincipalGracePeriods to set
     */
    public void setNumberOfPrincipalGracePeriods(
            Integer numberOfPrincipalGracePeriods) {
        this.numberOfPrincipalGracePeriods = numberOfPrincipalGracePeriods;
    }

    /**
     * @return the placeInstallment
     */
    @Column(name = "pla_ins_id", nullable = true)
    @Convert(converter = EPlaceInstallment.class)
    public EPlaceInstallment getPlaceInstallment() {
        return placeInstallment;
    }

    /**
     * @param placeInstallment the placeInstallment to set
     */
    public void setPlaceInstallment(EPlaceInstallment placeInstallment) {
        this.placeInstallment = placeInstallment;
    }

    /**
     * @return the wayOfKnowing
     */
    @Column(name = "way_knw_id", nullable = true)
    @Convert(converter = EMediaPromoting.class)
    public EMediaPromoting getWayOfKnowing() {
        return wayOfKnowing;
    }

    /**
     * @param wayOfKnowing the wayOfKnowing to set
     */
    public void setWayOfKnowing(EMediaPromoting wayOfKnowing) {
        this.wayOfKnowing = wayOfKnowing;
    }

    /**
     * @return the wayOfKnowing
     */
    @Column(name = "way_knw_id1", nullable = true)
    @Convert(converter = EMediaPromoting.class)
    public EMediaPromoting getWayOfKnowing1() {
        return wayOfKnowing1;
    }

    /**
     * @param wayOfKnowing the wayOfKnowing to set
     */
    public void setWayOfKnowing1(EMediaPromoting wayOfKnowing) {
        this.wayOfKnowing1 = wayOfKnowing;
    }

    /**
     * @return the wayOfKnowing
     */
    @Column(name = "way_knw_id2", nullable = true)
    @Convert(converter = EMediaPromoting.class)
    public EMediaPromoting getWayOfKnowing2() {
        return wayOfKnowing2;
    }

    /**
     * @param wayOfKnowing the wayOfKnowing to set
     */
    public void setWayOfKnowing2(EMediaPromoting wayOfKnowing) {
        this.wayOfKnowing2 = wayOfKnowing;
    }

    /**
     * @return the uwRevenuEstimation
     */
    @Column(name = "quo_am_uw_revenu_estimation", nullable = true)
    public Double getUwRevenuEstimation() {
        return uwRevenuEstimation;
    }

    /**
     * @param uwRevenuEstimation the uwRevenuEstimation to set
     */
    public void setUwRevenuEstimation(Double uwRevenuEstimation) {
        this.uwRevenuEstimation = uwRevenuEstimation;
    }

    /**
     * @return the uwAllowanceEstimation
     */
    @Column(name = "quo_am_uw_allowance_estimation", nullable = true)
    public Double getUwAllowanceEstimation() {
        return uwAllowanceEstimation;
    }

    /**
     * @param uwAllowanceEstimation the uwAllowanceEstimation to set
     */
    public void setUwAllowanceEstimation(Double uwAllowanceEstimation) {
        this.uwAllowanceEstimation = uwAllowanceEstimation;
    }

    /**
     * @return the uwNetIncomeEstimation
     */
    @Column(name = "quo_am_uw_net_income_estimation", nullable = true)
    public Double getUwNetIncomeEstimation() {
        return uwNetIncomeEstimation;
    }

    /**
     * @param uwNetIncomeEstimation the uwNetIncomeEstimation to set
     */
    public void setUwNetIncomeEstimation(Double uwNetIncomeEstimation) {
        this.uwNetIncomeEstimation = uwNetIncomeEstimation;
    }

    /**
     * @return the uwBusinessExpensesEstimation
     */
    @Column(name = "quo_am_uw_business_expenses_estimation", nullable = true)
    public Double getUwBusinessExpensesEstimation() {
        return uwBusinessExpensesEstimation;
    }

    /**
     * @param uwBusinessExpensesEstimation the uwBusinessExpensesEstimation to set
     */
    public void setUwBusinessExpensesEstimation(Double uwBusinessExpensesEstimation) {
        this.uwBusinessExpensesEstimation = uwBusinessExpensesEstimation;
    }

    /**
     * @return the uwPersonalExpensesEstimation
     */
    @Column(name = "quo_am_uw_personal_expenses_estimation", nullable = true)
    public Double getUwPersonalExpensesEstimation() {
        return uwPersonalExpensesEstimation;
    }

    /**
     * @param uwPersonalExpensesEstimation the uwPersonalExpensesEstimation to set
     */
    public void setUwPersonalExpensesEstimation(Double uwPersonalExpensesEstimation) {
        this.uwPersonalExpensesEstimation = uwPersonalExpensesEstimation;
    }

    /**
     * @return the uwFamilyExpensesEstimation
     */
    @Column(name = "quo_am_uw_family_expenses_estimation", nullable = true)
    public Double getUwFamilyExpensesEstimation() {
        return uwFamilyExpensesEstimation;
    }

    /**
     * @param uwFamilyExpensesEstimation the uwFamilyExpensesEstimation to set
     */
    public void setUwFamilyExpensesEstimation(Double uwFamilyExpensesEstimation) {
        this.uwFamilyExpensesEstimation = uwFamilyExpensesEstimation;
    }

    /**
     * @return the uwLiabilityEstimation
     */
    @Column(name = "quo_am_uw_liability_estimation", nullable = true)
    public Double getUwLiabilityEstimation() {
        return uwLiabilityEstimation;
    }

    /**
     * @param uwLiabilityEstimation the uwLiabilityEstimation to set
     */
    public void setUwLiabilityEstimation(Double uwLiabilityEstimation) {
        this.uwLiabilityEstimation = uwLiabilityEstimation;
    }

    /**
     * @return the coRevenuEstimation
     */
    @Column(name = "quo_am_co_revenu_estimation", nullable = true)
    public Double getCoRevenuEstimation() {
        return coRevenuEstimation;
    }

    /**
     * @param coRevenuEstimation the coRevenuEstimation to set
     */
    public void setCoRevenuEstimation(Double coRevenuEstimation) {
        this.coRevenuEstimation = coRevenuEstimation;
    }

    /**
     * @return the coAllowanceEstimation
     */
    @Column(name = "quo_am_co_allowance_estimation", nullable = true)
    public Double getCoAllowanceEstimation() {
        return coAllowanceEstimation;
    }

    /**
     * @param coAllowanceEstimation the coAllowanceEstimation to set
     */
    public void setCoAllowanceEstimation(Double coAllowanceEstimation) {
        this.coAllowanceEstimation = coAllowanceEstimation;
    }

    /**
     * @return the coNetIncomeEstimation
     */
    @Column(name = "quo_am_co_net_income_estimation", nullable = true)
    public Double getCoNetIncomeEstimation() {
        return coNetIncomeEstimation;
    }

    /**
     * @param coNetIncomeEstimation the coNetIncomeEstimation to set
     */
    public void setCoNetIncomeEstimation(Double coNetIncomeEstimation) {
        this.coNetIncomeEstimation = coNetIncomeEstimation;
    }

    /**
     * @return the coBusinessExpensesEstimation
     */
    @Column(name = "quo_am_co_business_expenses_estimation", nullable = true)
    public Double getCoBusinessExpensesEstimation() {
        return coBusinessExpensesEstimation;
    }

    /**
     * @param coBusinessExpensesEstimation the coBusinessExpensesEstimation to set
     */
    public void setCoBusinessExpensesEstimation(Double coBusinessExpensesEstimation) {
        this.coBusinessExpensesEstimation = coBusinessExpensesEstimation;
    }

    /**
     * @return the coPersonalExpensesEstimation
     */
    @Column(name = "quo_am_co_personal_expenses_estimation", nullable = true)
    public Double getCoPersonalExpensesEstimation() {
        return coPersonalExpensesEstimation;
    }

    /**
     * @param coPersonalExpensesEstimation the coPersonalExpensesEstimation to set
     */
    public void setCoPersonalExpensesEstimation(Double coPersonalExpensesEstimation) {
        this.coPersonalExpensesEstimation = coPersonalExpensesEstimation;
    }

    /**
     * @return the coFamilyExpensesEstimation
     */
    @Column(name = "quo_am_co_family_expenses_estimation", nullable = true)
    public Double getCoFamilyExpensesEstimation() {
        return coFamilyExpensesEstimation;
    }

    /**
     * @param coFamilyExpensesEstimation the coFamilyExpensesEstimation to set
     */
    public void setCoFamilyExpensesEstimation(Double coFamilyExpensesEstimation) {
        this.coFamilyExpensesEstimation = coFamilyExpensesEstimation;
    }

    /**
     * @return the coLiabilityEstimation
     */
    @Column(name = "quo_am_co_liability_estimation", nullable = true)
    public Double getCoLiabilityEstimation() {
        return coLiabilityEstimation;
    }

    /**
     * @param coLiabilityEstimation the coLiabilityEstimation to set
     */
    public void setCoLiabilityEstimation(Double coLiabilityEstimation) {
        this.coLiabilityEstimation = coLiabilityEstimation;
    }

    /**
     * @return the coGuarantorRevenuEstimation
     */
    @Column(name = "quo_am_co_gua_revenu_estimation", nullable = true)
    public Double getCoGuarantorRevenuEstimation() {
        return coGuarantorRevenuEstimation;
    }

    /**
     * @param coGuarantorRevenuEstimation the coGuarantorRevenuEstimation to set
     */
    public void setCoGuarantorRevenuEstimation(Double coGuarantorRevenuEstimation) {
        this.coGuarantorRevenuEstimation = coGuarantorRevenuEstimation;
    }

    /**
     * @return the coGuarantorAllowanceEstimation
     */
    @Column(name = "quo_am_co_gua_allowance_estimation", nullable = true)
    public Double getCoGuarantorAllowanceEstimation() {
        return coGuarantorAllowanceEstimation;
    }

    /**
     * @param coGuarantorAllowanceEstimation the coGuarantorAllowanceEstimation to set
     */
    public void setCoGuarantorAllowanceEstimation(
            Double coGuarantorAllowanceEstimation) {
        this.coGuarantorAllowanceEstimation = coGuarantorAllowanceEstimation;
    }

    /**
     * @return the coGuarantorNetIncomeEstimation
     */
    @Column(name = "quo_am_co_gua_net_income_estimation", nullable = true)
    public Double getCoGuarantorNetIncomeEstimation() {
        return coGuarantorNetIncomeEstimation;
    }

    /**
     * @param coGuarantorNetIncomeEstimation the coGuarantorNetIncomeEstimation to set
     */
    public void setCoGuarantorNetIncomeEstimation(
            Double coGuarantorNetIncomeEstimation) {
        this.coGuarantorNetIncomeEstimation = coGuarantorNetIncomeEstimation;
    }

    /**
     * @return the coGuarantorBusinessExpensesEstimation
     */
    @Column(name = "quo_am_co_gua_business_expenses_estimation", nullable = true)
    public Double getCoGuarantorBusinessExpensesEstimation() {
        return coGuarantorBusinessExpensesEstimation;
    }

    /**
     * @param coGuarantorBusinessExpensesEstimation the coGuarantorBusinessExpensesEstimation to set
     */
    public void setCoGuarantorBusinessExpensesEstimation(
            Double coGuarantorBusinessExpensesEstimation) {
        this.coGuarantorBusinessExpensesEstimation = coGuarantorBusinessExpensesEstimation;
    }

    /**
     * @return the coGuarantorPersonalExpensesEstimation
     */
    @Column(name = "quo_am_co_gua_personal_expenses_estimation", nullable = true)
    public Double getCoGuarantorPersonalExpensesEstimation() {
        return coGuarantorPersonalExpensesEstimation;
    }

    /**
     * @param coGuarantorPersonalExpensesEstimation the coGuarantorPersonalExpensesEstimation to set
     */
    public void setCoGuarantorPersonalExpensesEstimation(
            Double coGuarantorPersonalExpensesEstimation) {
        this.coGuarantorPersonalExpensesEstimation = coGuarantorPersonalExpensesEstimation;
    }

    /**
     * @return the coGuarantorFamilyExpensesEstimation
     */
    @Column(name = "quo_am_co_gua_family_expenses_estimation", nullable = true)
    public Double getCoGuarantorFamilyExpensesEstimation() {
        return coGuarantorFamilyExpensesEstimation;
    }

    /**
     * @param coGuarantorFamilyExpensesEstimation the coGuarantorFamilyExpensesEstimation to set
     */
    public void setCoGuarantorFamilyExpensesEstimation(
            Double coGuarantorFamilyExpensesEstimation) {
        this.coGuarantorFamilyExpensesEstimation = coGuarantorFamilyExpensesEstimation;
    }

    /**
     * @return the coGuarantorLiabilityEstimation
     */
    @Column(name = "quo_am_co_gua_liability_estimation", nullable = true)
    public Double getCoGuarantorLiabilityEstimation() {
        return coGuarantorLiabilityEstimation;
    }

    /**
     * @param coGuarantorLiabilityEstimation the coGuarantorLiabilityEstimation to set
     */
    public void setCoGuarantorLiabilityEstimation(
            Double coGuarantorLiabilityEstimation) {
        this.coGuarantorLiabilityEstimation = coGuarantorLiabilityEstimation;
    }

    /**
     * @return the fieldCheckPerformed
     */
    @Column(name = "quo_bl_field_check", nullable = true)
    public Boolean isFieldCheckPerformed() {
        return fieldCheckPerformed;
    }

    /**
     * @param fieldCheckPerformed the fieldCheckPerformed to set
     */
    public void setFieldCheckPerformed(Boolean fieldCheckPerformed) {
        this.fieldCheckPerformed = fieldCheckPerformed;
    }


    /**
     * @return the locked
     */
    @Column(name = "quo_bl_locked", nullable = true)
    public Boolean isLocked() {
        return locked;
    }

    /**
     * @param locked the locked to set
     */
    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    /**
     * @return the issueDownPayment
     */
    @Column(name = "quo_bl_issue_down_payment", nullable = true, columnDefinition = "boolean default false")
    public Boolean isIssueDownPayment() {
        return issueDownPayment;
    }

    /**
     * @param issueDownPayment the issueDownPayment to set
     */
    public void setIssueDownPayment(Boolean issueDownPayment) {
        this.issueDownPayment = issueDownPayment;
    }

    /**
     * @return the checkerID
     */
    @Column(name = "quo_va_checker_id", nullable = true, length = 20)
    public String getCheckerID() {
        return checkerID;
    }

    /**
     * @param checkerID the checkerID to set
     */
    public void setCheckerID(String checkerID) {
        this.checkerID = checkerID;
    }

    /**
     * @return the checkerName
     */
    @Column(name = "quo_va_checker_name", nullable = true, length = 50)
    public String getCheckerName() {
        return checkerName;
    }

    /**
     * @param checkerName the checkerName to set
     */
    public void setCheckerName(String checkerName) {
        this.checkerName = checkerName;
    }

    /**
     * @return the checkerPhoneNumber
     */
    @Column(name = "quo_va_checker_phone_number", nullable = true, length = 50)
    public String getCheckerPhoneNumber() {
        return checkerPhoneNumber;
    }

    /**
     * @param checkerPhoneNumber the checkerPhoneNumber to set
     */
    public void setCheckerPhoneNumber(String checkerPhoneNumber) {
        this.checkerPhoneNumber = checkerPhoneNumber;
    }

    /**
     * @return the userLocked
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_usr_id_locked")
    public SecUser getUserLocked() {
        return userLocked;
    }

    /**
     * @param userLocked the userLocked to set
     */
    public void setUserLocked(SecUser userLocked) {
        this.userLocked = userLocked;
    }

    @Column(name = "quo_dt_first_apply", nullable = true)
    public Date getFirstApplyDate() {
        return firstApplyDate;
    }

    public void setFirstApplyDate(Date firstApplyDate) {
        this.firstApplyDate = firstApplyDate;
    }

    /**
     * @return the quotationApplicants
     */
    @OneToMany(mappedBy = "quotation", fetch = FetchType.LAZY)
    public List<QuotationApplicant> getQuotationApplicants() {
        return quotationApplicants;
    }


    /**
     * @param quotationApplicants the quotationApplicants to set
     */
    public void setQuotationApplicants(List<QuotationApplicant> quotationApplicants) {
        this.quotationApplicants = quotationApplicants;
    }

    /**
     * @return the quotationServices
     */
    @OneToMany(mappedBy = "quotation", fetch = FetchType.LAZY)
    public List<QuotationService> getQuotationServices() {
        if (quotationServices == null) {
            quotationServices = new ArrayList<QuotationService>();
        }
        return quotationServices;
    }

    /**
     * @param quotationServices the quotationServices to set
     */
    public void setQuotationServices(List<QuotationService> quotationServices) {
        this.quotationServices = quotationServices;
    }

    /**
     * @return the quotationDocuments
     */
    @OneToMany(mappedBy = "quotation", fetch = FetchType.LAZY)
    public List<QuotationDocument> getQuotationDocuments() {
        if (quotationDocuments == null) {
            quotationDocuments = new ArrayList<QuotationDocument>();
        }
        return quotationDocuments;
    }

    /**
     * @return the quotationDocuments
     */
    @Transient
    public List<QuotationDocument> getQuotationDocuments(EApplicantType applicationType) {
        List<QuotationDocument> quotationDocumentsByType = new ArrayList<QuotationDocument>();
        if (quotationDocuments != null && applicationType != null) {
            for (QuotationDocument quotationDocument : quotationDocuments) {
                if (quotationDocument.getDocument() != null && quotationDocument.getDocument().getApplicantType().equals(applicationType))
                    quotationDocumentsByType.add(quotationDocument);
            }
        }
        return quotationDocumentsByType;
    }

    /**
     * @param quotationDocuments the quotationDocuments to set
     */
    public void setQuotationDocuments(List<QuotationDocument> quotationDocuments) {
        this.quotationDocuments = quotationDocuments;
    }

    /**
     * @return the quotationExtModules
     */
    @OneToMany(mappedBy = "quotation", fetch = FetchType.LAZY)
    @OrderBy("processDate DESC")
    public List<QuotationExtModule> getQuotationExtModules() {
        return quotationExtModules;
    }

    /**
     * @param quotationExtModules the quotationExtModules to set
     */
    public void setQuotationExtModules(List<QuotationExtModule> quotationExtModules) {
        this.quotationExtModules = quotationExtModules;
    }

    /**
     * @return the quotationSupportDecisions
     */
    @OneToMany(mappedBy = "quotation", fetch = FetchType.LAZY)
    public List<QuotationSupportDecision> getQuotationSupportDecisions() {
        return quotationSupportDecisions;
    }

    /**
     * @param quotationSupportDecisions the quotationSupportDecisions to set
     */
    public void setQuotationSupportDecisions(
            List<QuotationSupportDecision> quotationSupportDecisions) {
        this.quotationSupportDecisions = quotationSupportDecisions;
    }

    /**
     * @return the comments
     */
    @OneToMany(mappedBy = "quotation", fetch = FetchType.LAZY)
    @OrderBy("updateDate DESC")
    public List<Comment> getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    /**
     * @return the timestamp
     */
    @Column(name = "timestamp")
    public Integer getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the uwGuarantorRevenuEstimation
     */
    @Column(name = "quo_am_uw_gua_revenu_estimation", nullable = true)
    public Double getUwGuarantorRevenuEstimation() {
        return uwGuarantorRevenuEstimation;
    }

    /**
     * @param uwGuarantorRevenuEstimation the uwGuarantorRevenuEstimation to set
     */
    public void setUwGuarantorRevenuEstimation(
            Double uwGuarantorRevenuEstimation) {
        this.uwGuarantorRevenuEstimation = uwGuarantorRevenuEstimation;
    }

    /**
     * @return the uwAllowanceEstimationGuarantor
     */
    @Column(name = "quo_am_uw_gua_allowance_estimation", nullable = true)
    public Double getUwGuarantorAllowanceEstimation() {
        return uwGuarantorAllowanceEstimation;
    }

    /**
     * @param uwGuarantorAllowanceEstimation the uwGuarantorAllowanceEstimatio to set
     */
    public void setUwGuarantorAllowanceEstimation(
            Double uwGuarantorAllowanceEstimation) {
        this.uwGuarantorAllowanceEstimation = uwGuarantorAllowanceEstimation;
    }

    /**
     * @return the uwNetIncomeEstimationGuarantor
     */
    @Column(name = "quo_am_uw_gua_netincome_estimation", nullable = true)
    public Double getUwGuarantorNetIncomeEstimation() {
        return uwGuarantorNetIncomeEstimation;
    }

    /**
     * @param uwGuarantorNetIncomeEstimation the uwGuarantorNetIncomeEstimation to set
     */
    public void setUwGuarantorNetIncomeEstimation(
            Double uwGuarantorNetIncomeEstimation) {
        this.uwGuarantorNetIncomeEstimation = uwGuarantorNetIncomeEstimation;
    }

    /**
     * @return the uwGuarantorBusinessExpensesEstimation
     */
    @Column(name = "quo_am_uw_gua_business_expenses_estimation", nullable = true)
    public Double getUwGuarantorBusinessExpensesEstimation() {
        return uwGuarantorBusinessExpensesEstimation;
    }

    /**
     * @param uwGuarantorBusinessExpensesEstimation the uwGuarantorBusinessExpensesEstimation to set
     */
    public void setUwGuarantorBusinessExpensesEstimation(
            Double uwGuarantorBusinessExpensesEstimation) {
        this.uwGuarantorBusinessExpensesEstimation = uwGuarantorBusinessExpensesEstimation;
    }

    /**
     * @return the uwGuarantorPersonalExpensesEstimation
     */
    @Column(name = "quo_am_uw_gua_personal_expenses_estimation", nullable = true)
    public Double getUwGuarantorPersonalExpensesEstimation() {
        return uwGuarantorPersonalExpensesEstimation;
    }

    /**
     * @param uwGuarantorPersonalExpensesEstimation the uwGuarantorPersonalExpensesEstimation to set
     */
    public void setUwGuarantorPersonalExpensesEstimation(
            Double uwGuarantorPersonalExpensesEstimation) {
        this.uwGuarantorPersonalExpensesEstimation = uwGuarantorPersonalExpensesEstimation;
    }

    /**
     * @return the uwGuarantorFamilyExpensesEstimation
     */
    @Column(name = "quo_am_uw_gua_family_expenses_estimation", nullable = true)
    public Double getUwGuarantorFamilyExpensesEstimation() {
        return uwGuarantorFamilyExpensesEstimation;
    }

    /**
     * @param uwGuarantorFamilyExpensesEstimation the uwGuarantorFamilyExpensesEstimation to set
     */
    public void setUwGuarantorFamilyExpensesEstimation(
            Double uwGuarantorFamilyExpensesEstimation) {
        this.uwGuarantorFamilyExpensesEstimation = uwGuarantorFamilyExpensesEstimation;
    }

    /**
     * @return the uwGuarantorLiabilityEstimation
     */
    @Column(name = "quo_am_uw_gua_liability_estimation", nullable = true)
    public Double getUwGuarantorLiabilityEstimation() {
        return uwGuarantorLiabilityEstimation;
    }

    /**
     * @param uwGuarantorLiabilityEstimation the uwGuarantorLiabilityEstimation to set
     */
    public void setUwGuarantorLiabilityEstimation(
            Double uwGuarantorLiabilityEstimation) {
        this.uwGuarantorLiabilityEstimation = uwGuarantorLiabilityEstimation;
    }

    /**
     * @return the totalAR
     */
    @Column(name = "quo_am_total_ar", nullable = true)
    public Double getTotalAR() {
        return totalAR;
    }

    /**
     * @param totalAR the totalAR to set
     */
    public void setTotalAR(Double totalAR) {
        this.totalAR = totalAR;
    }

    /**
     * @return the totalUE
     */
    @Column(name = "quo_am_total_ue", nullable = true)
    public Double getTotalUE() {
        return totalUE;
    }

    /**
     * @param totalUE the totalUE to set
     */
    public void setTotalUE(Double totalUE) {
        this.totalUE = totalUE;
    }

    /**
     * @return the totalVAT
     */
    @Column(name = "quo_am_total_vat", nullable = true)
    public Double getTotalVAT() {
        return totalVAT;
    }

    /**
     * @param totalVAT the totalVAT to set
     */
    public void setTotalVAT(Double totalVAT) {
        this.totalVAT = totalVAT;
    }

    @Column(name = "quo_am_total_installment_amount", nullable = true)
    public Double getTotalInstallmentAmount() {
        return totalInstallmentAmount;
    }

    public void setTotalInstallmentAmount(Double totalInstallmentAmount) {
        this.totalInstallmentAmount = totalInstallmentAmount;
    }

    /**
     * @return the tiPrepaidInstallment
     */
    @Column(name = "quo_am_ti_prepaid_installment", nullable = true)
    public Double getTiPrepaidInstallment() {
        return tiPrepaidInstallment;
    }

    /**
     * @param tiPrepaidInstallment the tiPrepaidInstallment to set
     */
    public void setTiPrepaidInstallment(Double tiPrepaidInstallment) {
        this.tiPrepaidInstallment = tiPrepaidInstallment;
    }

    /**
     * @return the numberPrepaidTerm
     */
    @Column(name = "quo_nu_number_prepaid_term", nullable = true)
    public Integer getNumberPrepaidTerm() {
        return numberPrepaidTerm;
    }

    /**
     * @param numberPrepaidTerm the numberPrepaidTerm to set
     */
    public void setNumberPrepaidTerm(Integer numberPrepaidTerm) {
        this.numberPrepaidTerm = numberPrepaidTerm;
    }

    /**
     * @return the vatValue
     */
    @Column(name = "quo_rt_vat", nullable = true, columnDefinition = "double precision default '0'")
    public double getVatValue() {
        return vatValue;
    }

    /**
     * @param vatValue the vatValue to set
     */
    public void setVatValue(double vatValue) {
        this.vatValue = vatValue;
    }

    /**
     * @return the migrationID
     */
    @Column(name = "quo_va_migration_id", nullable = true, length = 30)
    public String getMigrationID() {
        return migrationID;
    }

    /**
     * @param migrationID the migrationID to set
     */
    public void setMigrationID(String migrationID) {
        this.migrationID = migrationID;
    }

    @Column(name = "quo_dt_migration", nullable = true)
    public Date getMigrationDate() {
        return migrationDate;
    }

    public void setMigrationDate(Date migrationDate) {
        this.migrationDate = migrationDate;
    }

    /**
     * @return the originBranch
     */
    @Transient
    public OrgStructure getOriginBranch() {
        return originBranch;
    }

    /**
     * @param originBranch the originBranch to set
     */
    public void setOriginBranch(OrgStructure originBranch) {
        this.originBranch = originBranch;
    }

    /**
     * @param applicantType
     * @return
     */
    @Transient
    public QuotationApplicant getQuotationApplicant(EApplicantType applicantType) {
        if (quotationApplicants != null && !quotationApplicants.isEmpty()) {
            for (QuotationApplicant quotationApplicant : quotationApplicants) {
                if (applicantType.getId().equals(quotationApplicant.getApplicantType().getId()) && CrudAction.DELETE != quotationApplicant.getCrudAction()) {
                    return quotationApplicant;
                }
            }
        }
        return null;
//		return quotationApplicants.get(0);
    }

    /**
     * @param serviId
     * @return
     */
    @Transient
    public QuotationService getQuotationService(Long serviId) {
        if (quotationServices != null && !quotationServices.isEmpty()) {
            for (QuotationService quotationService : quotationServices) {
                if (quotationService.getService().getId().equals(serviId)) {
                    return quotationService;
                }
            }
        }
        return null;
    }

    @Transient
    public QuotationService getQuotationService(String serviCode) {
        if (quotationServices != null && !quotationServices.isEmpty()) {
            for (QuotationService quotationService : quotationServices) {
                if ((quotationService.getService() != null) && quotationService.getService().getCode().equals(serviCode)) {
                    return quotationService;
                }
            }
        }
        return null;
    }

    @Transient
    public QuotationService getQuotationServiceByServiceType(EServiceType serviceType) {
        if (quotationServices != null && !quotationServices.isEmpty()) {
            for (QuotationService quotationService : quotationServices) {
                if ((quotationService.getService() != null) && serviceType.equals(quotationService.getService().getServiceType())) {
                    return quotationService;
                }
            }
        }
        return null;
    }

    @Transient
    public QuotationDocument getQuotationDocument(Long documId) {
        if (quotationDocuments != null && !quotationDocuments.isEmpty()) {
            for (QuotationDocument quotationDocument : quotationDocuments) {
                if ((quotationDocument.getDocument() != null) && quotationDocument.getDocument().getId().equals(documId)) {
                    return quotationDocument;
                }
            }
        }
        return null;
    }

    /**
     * Set main applicant
     *
     * @param mainApplicant
     */
    public void setMainApplicant(Applicant mainApplicant) {
        if (quotationApplicants == null) {
            quotationApplicants = new ArrayList<QuotationApplicant>();
        }
        QuotationApplicant quotationApplicant = getQuotationApplicant(EApplicantType.C);
        if (quotationApplicant == null) {
            quotationApplicant = new QuotationApplicant();
        }
        quotationApplicant.setApplicant(mainApplicant);
        quotationApplicant.setQuotation(this);
        quotationApplicant.setApplicantType(EApplicantType.C);
        quotationApplicants.add(quotationApplicant);
    }

    /**
     * Get customer
     *
     * @return
     */
    @Transient
    public Applicant getGuarantor() {
        QuotationApplicant quotationApplicant = getQuotationApplicant(EApplicantType.G);
        if (quotationApplicant != null) {
            return quotationApplicant.getApplicant();
        }
        return null;
    }

    /**
     * @param guarantor
     */
    public void setGuarantor(Applicant guarantor) {
        if (quotationApplicants == null) {
            quotationApplicants = new ArrayList<QuotationApplicant>();
        }
        QuotationApplicant quotationApplicant = getQuotationApplicant(EApplicantType.G);
        if (quotationApplicant == null) {
            quotationApplicant = new QuotationApplicant();
            quotationApplicants.add(quotationApplicant);
        }
        quotationApplicant.setApplicant(guarantor);
        quotationApplicant.setQuotation(this);
        quotationApplicant.setApplicantType(EApplicantType.G);
    }


    /**
     * @param quotationStatus
     * @return
     */
    public List<QuotationSupportDecision> getQuotationSupportDecisions(EWkfStatus quotationStatus) {
        List<QuotationSupportDecision> quotationSupportDecisionsbyStatus = new ArrayList<QuotationSupportDecision>();
        if (quotationSupportDecisions != null && !quotationSupportDecisions.isEmpty()) {
            for (QuotationSupportDecision quotationSupportDecision : quotationSupportDecisions) {
                if (quotationSupportDecision.getWkfStatus() == quotationStatus && !quotationSupportDecision.isProcessed()) {
                    quotationSupportDecisionsbyStatus.add(quotationSupportDecision);
                }
            }
        }
        return quotationSupportDecisionsbyStatus;
    }

    // add in Field Check
    @OneToMany(mappedBy = "quotation", fetch = FetchType.LAZY)
    public List<QuotationContactVerification> getQuotationContactVerifications() {
        return quotationContactVerifications;
    }

    /**
     * @param quotationContactVerifications the quotationContactVerifications to set
     */
    public void setQuotationContactVerifications(List<QuotationContactVerification> quotationContactVerifications) {
        this.quotationContactVerifications = quotationContactVerifications;
    }

    @Transient
    public QuotationContactVerification getQuotationContactVerification(ContactVerification contactVerification, EApplicantType applicantType) {
        if (quotationContactVerifications != null && !quotationContactVerifications.isEmpty()) {
            for (QuotationContactVerification quotationContactVerification : quotationContactVerifications) {
                if (quotationContactVerification.getApplicantType().equals(applicantType)
                        && quotationContactVerification.getContactVerification().getId().equals(contactVerification.getId())) {
                    return quotationContactVerification;
                }
            }
        }
        return null;
    }

    /**
     * @return the secUser
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "secus_id")
    public SecUser getSecUser() {
        return secUser;
    }

    /**
     * @param secUser the secUser to set
     */
    public void setSecUser(SecUser secUser) {
        this.secUser = secUser;
    }


    /**
     * @return
     */
    @Transient
    @Override
    public List<QuotationWkfHistoryItem> getHistories() {
        return (List<QuotationWkfHistoryItem>) super.getHistories();
    }


    @Transient
    public Applicant getMainApplicant() {
        QuotationApplicant quotationApplicant = this.getQuotationApplicant(EApplicantType.C);
        return quotationApplicant != null ? quotationApplicant.getApplicant() : null;
    }

    @Column(name = "fpd_lease_amount_per", nullable = true)
    public Double getLeaseAmountPercentage() {
        return leaseAmountPercentage;
    }

    public void setLeaseAmountPercentage(Double leaseAmountPercentage) {
        this.leaseAmountPercentage = leaseAmountPercentage;
    }

    @Column(name = "quo_am_uw_business_incomes_estimation", nullable = true)
    public Double getUwBusinessIncomesEstimation() {
        return uwBusinessIncomesEstimation;
    }

    public void setUwBusinessIncomesEstimation(Double uwBusinessIncomesEstimation) {
        this.uwBusinessIncomesEstimation = uwBusinessIncomesEstimation;
    }

    @OneToMany(mappedBy = "quotation", fetch = FetchType.LAZY)
    public List<QuotationContactEvidence> getQuotationContactEvidences() {
        return quotationContactEvidences;
    }

    /**
     * @param quotationContactEvidences the quotationContactEvidences to set
     */
    public void setQuotationContactEvidences(
            List<QuotationContactEvidence> quotationContactEvidences) {
        this.quotationContactEvidences = quotationContactEvidences;
    }

    /**
     * @return the coVOpinionApplicant
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "covop_id_applicant")
    public CoVOpinion getCoVOpinionApplicant() {
        return coVOpinionApplicant;
    }

    /**
     * @param coVOpinionApplicant the coVOpinionApplicant to set
     */
    public void setCoVOpinionApplicant(CoVOpinion coVOpinionApplicant) {
        this.coVOpinionApplicant = coVOpinionApplicant;
    }

    /**
     * @return the coVOpinionGuarantor
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "covop_id_guarantor")
    public CoVOpinion getCoVOpinionGuarantor() {
        return coVOpinionGuarantor;
    }


    @Column(name = "quo_decline", nullable = true)
    public boolean isDecline() {
        return decline;
    }

    public void setDecline(boolean decline) {
        this.decline = decline;
    }

    /**
     * @param coVOpinionGuarantor the coVOpinionGuarantor to set
     */
    public void setCoVOpinionGuarantor(CoVOpinion coVOpinionGuarantor) {
        this.coVOpinionGuarantor = coVOpinionGuarantor;
    }

    @OneToMany(mappedBy = "quotation", fetch = FetchType.LAZY)
    public List<QuotationRegistrationStatusHistory> getQuotationRegistrationStatusHistory() {
        return quotationRegistrationStatusHistory;
    }

    public void setQuotationRegistrationStatusHistory(List<QuotationRegistrationStatusHistory> quotationRegistrationStatusHistory) {
        this.quotationRegistrationStatusHistory = quotationRegistrationStatusHistory;
    }

    @OneToMany(mappedBy = "quotation", fetch = FetchType.LAZY)
    public List<QuotationRegistrationLocationHistory> getQuotationRegistrationLocationHistory() {
        return quotationRegistrationLocationHistory;
    }

    public void setQuotationRegistrationLocationHistory(List<QuotationRegistrationLocationHistory> quotationRegistrationLocationHistory) {
        this.quotationRegistrationLocationHistory = quotationRegistrationLocationHistory;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reg_sta_id", nullable = true)
    public RegistrationStatus getRegistrationStatus() {
        return registrationStatus;
    }

    public void setRegistrationStatus(RegistrationStatus registrationStatus) {
        this.registrationStatus = registrationStatus;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reg_loc_id", nullable = true)
    public RegistrationStorageLocation getRegistrationStorageLocation() {
        return registrationStorageLocation;
    }

    public void setRegistrationStorageLocation(RegistrationStorageLocation registrationStorageLocation) {
        this.registrationStorageLocation = registrationStorageLocation;
    }

    @Column(name = "quo_dt_reg_deadline", nullable = true)
    public Date getRegistrationDeadline() {
        return registrationDeadline;
    }

    public void setRegistrationDeadline(Date registrationDeadline) {
        this.registrationDeadline = registrationDeadline;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ins_sta_id", nullable = true)
    public InsuranceStatus getInsuranceStatus() {
        return insuranceStatus;
    }

    public void setInsuranceStatus(InsuranceStatus insuranceStatus) {
        this.insuranceStatus = insuranceStatus;
    }

    @OneToMany(mappedBy = "quotation", fetch = FetchType.LAZY)
    public List<QuotationInsuranceStatusHistory> getQuotationInsuranceStatusHistory() {
        return quotationInsuranceStatusHistory;
    }

    public void setQuotationInsuranceStatusHistory(List<QuotationInsuranceStatusHistory> quotationInsuranceStatusHistory) {
        this.quotationInsuranceStatusHistory = quotationInsuranceStatusHistory;
    }

    public List<QuotationContactEvidence> getQuotationContactEvidences(DocumentUwGroup documentUwGroup) {
        List<QuotationContactEvidence> quotationContactEvidencesByType = new ArrayList<>();
        if (quotationContactEvidences != null && !quotationContactEvidences.isEmpty()) {
            for (QuotationContactEvidence quotationContactEvidence : quotationContactEvidences) {
                if (quotationContactEvidence.getDocumentUwGroup().getId().equals(documentUwGroup.getId())) {
                    quotationContactEvidencesByType.add(quotationContactEvidence);
                }
            }
        }
        return quotationContactEvidencesByType;
    }

   @Column(name = "quo_bl_uw_approve", nullable = true)
    public boolean isUnderwriterApprove() {
        return underwriterApprove;
    }

    public void setUnderwriterApprove(boolean underwriterApprove) {
        this.underwriterApprove = underwriterApprove;
    }
    @Column(name = "quo_bl_us_approve", nullable = true)
    public boolean isUnderwriterSupervisorApprove() {
        return underwriterSupervisorApprove;
    }

    public void setUnderwriterSupervisorApprove(boolean underwriterSupervisorApprove) {
        this.underwriterSupervisorApprove = underwriterSupervisorApprove;
    }

    @Column(name = "quo_dt_uw_submit", nullable = true)
    public Date getUnderwriterSubmitDate() {
        return underwriterSubmitDate;
    }

    public void setUnderwriterSubmitDate(Date underwriterSubmitDate) {
        this.underwriterSubmitDate = underwriterSubmitDate;
    }

    @Column(name = "quo_dt_us_submit", nullable = true)
    public Date getUnderwriterSupervisorSubmitDate() {
        return underwriterSupervisorSubmitDate;
    }

    public void setUnderwriterSupervisorSubmitDate(Date underwriterSupervisorSubmitDate) {
        this.underwriterSupervisorSubmitDate = underwriterSupervisorSubmitDate;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cre_com_id", nullable = true)
    public GroupCommittee getSecGroupCommittee() {
        return secGroupCommittee;
    }

    public void setSecGroupCommittee(GroupCommittee secGroupCommittee) {
        this.secGroupCommittee = secGroupCommittee;
    }

    @Column(name = "num_click_approve")
    public Integer getNumberApproved() {
        return numberApproved;
    }

    public void setNumberApproved(Integer numberApproved) {
        this.numberApproved = numberApproved;
    }

    @Transient
    public List<QuotationContactEvidence> getQuotationContactEvidence(DocumentUwGroup documentUwGroup, EConfirmEvidence confirmEvidence) {
        List<QuotationContactEvidence> mQuotationContactEvidences = new ArrayList<>();
        if (quotationContactEvidences != null && !quotationContactEvidences.isEmpty()) {
            for (QuotationContactEvidence quotationContactEvidence : quotationContactEvidences) {
                if (quotationContactEvidence.getDocumentUwGroup().getId().equals(documentUwGroup.getId())
                        && quotationContactEvidence.getDocumentConfirmEvidence().getConfirmEvidence().getId().equals(confirmEvidence.getId())) {
                    mQuotationContactEvidences.add(quotationContactEvidence);
                }
            }
        }
        return mQuotationContactEvidences;
    }

    @Transient
    public QuotationContactEvidence getQuotationContactEvidences(DocumentUwGroup documentUwGroup, EConfirmEvidence confirmEvidence) {
        if (quotationContactEvidences != null && !quotationContactEvidences.isEmpty()) {
            for (QuotationContactEvidence quotationContactEvidence : quotationContactEvidences) {
                if (quotationContactEvidence.getDocumentUwGroup().getId().equals(documentUwGroup.getId())
                        && quotationContactEvidence.getDocumentConfirmEvidence().getConfirmEvidence().getId().equals(confirmEvidence.getId())) {
                    return quotationContactEvidence;
                }
            }
        }
        return null;
    }

    @Transient
    public List<EWkfStatus> getNextWkfStatuses() {
        List<EWkfStatus> lstStatus = null;
        try {
            EProductLineCode productLineCode = (this.getFinancialProduct() != null && this.getFinancialProduct().getProductLine() != null) ?
                    this.getFinancialProduct().getProductLine().getProductLineCode() : null;
            //lstStatus = super.getNextWkfStatuses(productLineCode);
        } catch (Exception e) {
            throw new WorkflowException("getNextWkfStatuses", e);
        }
        return lstStatus;
    }

    @OneToMany(mappedBy="quotation", fetch = FetchType.LAZY)
    public List<AssetInformationDocument> getAssetInformationDocument() {
        return assetInformationDocument;
    }

    public void setAssetInformationDocument(List<AssetInformationDocument> assetInformationDocument) {
        this.assetInformationDocument = assetInformationDocument;
    }

    @Override
    public String toString() {
        return "Quotation{" +
                "contract=" + contract +
                ", applicant=" + applicant +
                ", applicantArc=" + applicantArc +
                ", reference='" + reference + '\'' +
                ", externalReference='" + externalReference + '\'' +
                ", transferReference='" + transferReference + '\'' +
                ", dealer=" + dealer +
                ", secUser=" + secUser +
                ", creditOfficer=" + creditOfficer +
                ", productionOfficer=" + productionOfficer +
                ", underwriter=" + underwriter +
                ", underwriterSupervisor=" + underwriterSupervisor +
                ", manager=" + manager +
                ", documentController=" + documentController +
                ", fieldCheck=" + fieldCheck +
                ", asset=" + asset +
                ", assetArc=" + assetArc +
                ", financialProduct=" + financialProduct +
                ", campaign=" + campaign +
                ", tiDefaultFinanceAmount=" + tiDefaultFinanceAmount +
                ", teDefaultFinanceAmount=" + teDefaultFinanceAmount +
                ", vatDefaultFinanceAmount=" + vatDefaultFinanceAmount +
                ", tiFinanceAmount=" + tiFinanceAmount +
                ", tmFinanceAmount=" + tmFinanceAmount +
                ", vatFinanceAmount=" + vatFinanceAmount +
                ", tiAdvancePaymentAmount=" + tiAdvancePaymentAmount +
                ", teAdvancePaymentAmount=" + teAdvancePaymentAmount +
                ", vatAdvancePaymentAmount=" + vatAdvancePaymentAmount +
                ", advancePaymentPercentage=" + advancePaymentPercentage +
                ", tiInstallmentAmount=" + tiInstallmentAmount +
                ", teInstallmentAmount=" + teInstallmentAmount +
                ", vatInstallmentAmount=" + vatInstallmentAmount +
                ", totalInstallmentAmount=" + totalInstallmentAmount +
                ", valid=" + valid +
                ", startCreationDate=" + startCreationDate +
                ", quotationDate=" + quotationDate +
                ", submissionDate=" + submissionDate +
                ", firstSubmissionDate=" + firstSubmissionDate +
                ", acceptationDate=" + acceptationDate +
                ", rejectDate=" + rejectDate +
                ", declineDate=" + declineDate +
                ", activationDate=" + activationDate +
                ", contractStartDate=" + contractStartDate +
                ", firstDueDate=" + firstDueDate +
                ", lastDueDate=" + lastDueDate +
                ", bookingDate=" + bookingDate +
                ", insuranceStartDate=" + insuranceStartDate +
                ", insuranceEndYear=" + insuranceEndYear +
                ", term=" + term +
                ", interestRate=" + interestRate +
                ", irrRate=" + irrRate +
                ", frequency=" + frequency +
                ", numberOfPrincipalGracePeriods=" + numberOfPrincipalGracePeriods +
                ", placeInstallment=" + placeInstallment +
                ", wayOfKnowing=" + wayOfKnowing +
                ", wayOfKnowing1=" + wayOfKnowing1 +
                ", wayOfKnowing2=" + wayOfKnowing2 +
                ", uwRevenuEstimation=" + uwRevenuEstimation +
                ", uwAllowanceEstimation=" + uwAllowanceEstimation +
                ", uwNetIncomeEstimation=" + uwNetIncomeEstimation +
                ", uwBusinessExpensesEstimation=" + uwBusinessExpensesEstimation +
                ", uwBusinessIncomesEstimation=" + uwBusinessIncomesEstimation +
                ", uwPersonalExpensesEstimation=" + uwPersonalExpensesEstimation +
                ", uwFamilyExpensesEstimation=" + uwFamilyExpensesEstimation +
                ", uwLiabilityEstimation=" + uwLiabilityEstimation +
                ", coRevenuEstimation=" + coRevenuEstimation +
                ", coAllowanceEstimation=" + coAllowanceEstimation +
                ", coNetIncomeEstimation=" + coNetIncomeEstimation +
                ", coBusinessExpensesEstimation=" + coBusinessExpensesEstimation +
                ", coPersonalExpensesEstimation=" + coPersonalExpensesEstimation +
                ", coFamilyExpensesEstimation=" + coFamilyExpensesEstimation +
                ", coLiabilityEstimation=" + coLiabilityEstimation +
                ", coGuarantorRevenuEstimation=" + coGuarantorRevenuEstimation +
                ", coGuarantorAllowanceEstimation=" + coGuarantorAllowanceEstimation +
                ", coGuarantorNetIncomeEstimation=" + coGuarantorNetIncomeEstimation +
                ", coGuarantorBusinessExpensesEstimation=" + coGuarantorBusinessExpensesEstimation +
                ", coGuarantorPersonalExpensesEstimation=" + coGuarantorPersonalExpensesEstimation +
                ", coGuarantorFamilyExpensesEstimation=" + coGuarantorFamilyExpensesEstimation +
                ", coGuarantorLiabilityEstimation=" + coGuarantorLiabilityEstimation +
                ", uwGuarantorRevenuEstimation=" + uwGuarantorRevenuEstimation +
                ", uwGuarantorAllowanceEstimation=" + uwGuarantorAllowanceEstimation +
                ", uwGuarantorNetIncomeEstimation=" + uwGuarantorNetIncomeEstimation +
                ", uwGuarantorBusinessExpensesEstimation=" + uwGuarantorBusinessExpensesEstimation +
                ", uwGuarantorPersonalExpensesEstimation=" + uwGuarantorPersonalExpensesEstimation +
                ", uwGuarantorFamilyExpensesEstimation=" + uwGuarantorFamilyExpensesEstimation +
                ", uwGuarantorLiabilityEstimation=" + uwGuarantorLiabilityEstimation +
                ", fieldCheckPerformed=" + fieldCheckPerformed +
                ", locked=" + locked +
                ", userLocked=" + userLocked +
                ", issueDownPayment=" + issueDownPayment +
                ", totalAR=" + totalAR +
                ", totalUE=" + totalUE +
                ", totalVAT=" + totalVAT +
                ", tiPrepaidInstallment=" + tiPrepaidInstallment +
                ", numberPrepaidTerm=" + numberPrepaidTerm +
                ", vatValue=" + vatValue +
                ", checkerID='" + checkerID + '\'' +
                ", checkerName='" + checkerName + '\'' +
                ", checkerPhoneNumber='" + checkerPhoneNumber + '\'' +
                ", comments=" + comments +
                ", quotationApplicants=" + quotationApplicants +
                ", quotationServices=" + quotationServices +
                ", quotationDocuments=" + quotationDocuments +
                ", quotationExtModules=" + quotationExtModules +
                ", quotationSupportDecisions=" + quotationSupportDecisions +
                ", timestamp=" + timestamp +
                ", originBranch=" + originBranch +
                ", migrationID='" + migrationID + '\'' +
                ", leaseAmountPercentage=" + leaseAmountPercentage +
                ", tiAppraisalEstimateAmount=" + tiAppraisalEstimateAmount +
                ", teAppraisalEstimateAmount=" + teAppraisalEstimateAmount +
                ", previousWkfStatus=" + previousWkfStatus +
                ", coVOpinionApplicant=" + coVOpinionApplicant +
                ", coVOpinionGuarantor=" + coVOpinionGuarantor +
                ", quotationContactVerifications=" + quotationContactVerifications +
                ", quotationContactEvidences=" + quotationContactEvidences +
                ", decline=" + decline +
                ", quotationRegistrationStatusHistory=" + quotationRegistrationStatusHistory +
                ", quotationRegistrationLocationHistory=" + quotationRegistrationLocationHistory +
                ", registrationStatus=" + registrationStatus +
                ", registrationStorageLocation=" + registrationStorageLocation +
                ", registrationDeadline=" + registrationDeadline +
                ", insuranceStatus=" + insuranceStatus +
                ", quotationInsuranceStatusHistory=" + quotationInsuranceStatusHistory +
                ", underwriterSubmitDate=" + underwriterSubmitDate +
                ", underwriterSupervisorSubmitDate=" + underwriterSupervisorSubmitDate +
                '}';
    }
}
