package com.soma.mfinance.tools.report;

import com.soma.mfinance.core.shared.report.ReportParameter;


/**
 * 
 * @author kimsuor.seang
 *
 */
public interface Report  {

	/**
	 * @param reportParameter
	 * @return
	 * @throws Exception
	 */
	String generate(ReportParameter reportParameter) throws Exception;
}
