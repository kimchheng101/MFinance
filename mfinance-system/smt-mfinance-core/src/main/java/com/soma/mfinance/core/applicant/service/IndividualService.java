package com.soma.mfinance.core.applicant.service;

import com.soma.mfinance.core.applicant.model.*;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.shared.exception.ValidationFieldsException;
import com.soma.mfinance.core.shared.system.DomainType;
import com.soma.ersys.core.hr.model.eref.ECivility;
import com.soma.ersys.core.hr.model.eref.ETypeAddress;
import org.seuksa.frmk.service.BaseEntityService;

import java.util.Date;
import java.util.List;

/**
 * Individual service interface
 * 
 * @author kimsuor.seang
 *
 */
public interface IndividualService extends BaseEntityService {

	/**
	 * Identify customer to known if he/she exists already in customer database
	 * @param lastName
	 * @param firstName
	 * @param dateOfbirth
	 * @return
	 */
	List<Individual> identify(String lastName, String firstName, Date dateOfbirth);
	
	/**
	 * Save or Update a customer
	 * @param customer
	 * @return
	 */
	Individual saveOrUpdateIndividual(Individual individual);
	
	/**
	 * Save or Update a customer reference information
	 * @param individual
	 * @return
	 */
	Individual saveOrUpdateIndividualReference(Individual individual);
	
	/**
	 * @param individual
	 * @return
	 */
	IndividualArc saveOrUpdateIndividualArc(IndividualArc individual);
		
	/**
	 * Delete an individual
	 * @param individual
	 */
	void deleteIndividual(Individual individual);
	
	/**
	 * Delete an employment
	 * @param employment
	 */
	void deleteEmployment(Employment employment);
	
	/**
	 * Delete a reference
	 * @param reference
	 */
	void deleteReference(IndividualReferenceInfo reference);
	
	/**
	 * Delete a contact info by individual id
	 * @param indId
	 * @param conId
	 */
	void deleteContactInfo(Long indId, Long conId);
	
	/**
	 * 
	 * @param indId
	 * @param refId
	 * @param conId
	 */
	void deleteContactInfo(Long indId, Long refId, Long conId);
	
	/**
	 * Delete a contact address by individual id
	 * @param indId
	 * @param adrId
	 */
	void deleteContactAddress(Long indId, Long adrId);

	/**
	 * Check Individual
	 * @param individual
	 * @throws ValidationFieldsException
	 */
	void checkIndividual(Individual individual) throws ValidationFieldsException;
	
	/**
	 * Check Individual
	 * @param individual
	 * @param domainType
	 * @throws ValidationFieldsException
	 */
	void checkIndividual(Individual individual, DomainType domainType) throws ValidationFieldsException;
	
	/**
	 * @param birthDate
	 * @return
	 */
	boolean isIndividualOver18Years(Date birthDate);
	
	/**
	 * saveOrUpdate Employment 
	 * @param employment
	 */
	void saveOrUpdateEmployment(Employment employment);
	
	/**
	 * Save or Update a customer reference information
	 * @param individualReferenceInfo
	 * @return
	 */
	void saveOrUpdateReferenceInfo(IndividualReferenceInfo individualReferenceInfo);
	
	/**
	 * Save or Update a customer reference information
	 * @param individualReferenceContactInfo
	 * @return
	 */
	void saveOrUpdateReferenceContactInfo(IndividualReferenceContactInfo individualReferenceContactInfo);
	
	/**
	 * Save or update individual contact info
	 * @param individualContactInfo
	 */
	void saveOrUpdateIndividualContactInfo(IndividualContactInfo individualContactInfo);
	
	/**
	 * Save or update individual address
	 * @param individualAddress
	 */
	void saveOrUpdateIndividualAddress(IndividualAddress individualAddress);
	
	/**
	 * Get list individual address by individual id
	 * @param indId
	 * @return
	 */
	List<IndividualAddress> getIndividualAddresses(Long indId);
	
	/**
	 * Get list individual reference info by individual id
	 * @param indId
	 * @return
	 */
	List<IndividualReferenceInfo> getIndividualReferenceInfos(Long indId);
	
	/**
	 * Get list individual contact info by individual id
	 * @param indId
	 * @return
	 */
	List<IndividualContactInfo> getIndividualContactInfos(Long indId);
	
	/**
	 * Get list contracts by individual id
	 * @param indId
	 * @return
	 */
	List<Contract> getContractsByIndividual(Long indId);
	
	/**
	 * Get list contracts guarantor by individual
	 * @param indId
	 * @return
	 */
	List<Contract> getContractsGuarantorByIndividual(Long indId);
	
	/**
	 * 
	 * @param indId
	 * @return
	 */
	IndividualContactInfo getIndividualContactInfoByAddressType(Long indId, ETypeAddress typeAddress);

	/**
	 *
	 * @param individual
	 * @return
	 */
	List<IndividualContactInfo> getIndividualContractInfos(Individual individual);

	List<ECivility> listECivility();
	
}
