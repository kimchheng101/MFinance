package com.soma.mfinance.glf.report.xls;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/25/2017
 * Name  : 4:06 PM
 * Email : k.seang@gl-f.com
 */
public interface PaymentScheduleMfpFields extends GLFPaymentScheduleFields {

    String KHMER_OS_BATTAMBANG = "Khmer OS Battambang";

}
