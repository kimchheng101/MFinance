package com.soma.mfinance.core.applicant.panel;

import com.soma.common.app.eref.ECountry;
import com.soma.mfinance.core.address.panel.AddressPanel;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.applicant.model.Employment;
import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.mfinance.core.applicant.panel.address.BusinessScaleIncomePanel;
import com.soma.mfinance.core.applicant.service.ValidateNumbers;
import com.soma.mfinance.core.application.ApplicationFields;
import com.soma.mfinance.core.application.model.EmploymentOccupation;
import com.soma.mfinance.core.application.model.EmploymentPosition;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.applicant.AddressUtils;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.shared.system.AmountBigUtils;
import com.soma.mfinance.core.shared.system.AutoTextField;
import com.soma.ersys.core.hr.model.address.Address;
import com.soma.ersys.core.hr.model.eref.*;
import com.soma.frmk.vaadin.ui.panel.AbstractControlPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.mfinance.core.applicant.panel.address.BusinessScaleIncomePanel;
import com.soma.mfinance.core.applicant.service.ValidateNumbers;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.FieldEvents;
import com.vaadin.ui.*;
import com.vaadin.ui.Notification.Type;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.amount.AmountUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.soma.mfinance.core.applicant.service.ValidateNumbers.isValidOnlyNumber;
import static com.soma.mfinance.core.applicant.service.ValidateNumbers.isValidPhoneNumber;
import static org.seuksa.frmk.tools.DateUtils.getYear;
import static org.seuksa.frmk.tools.DateUtils.todayDate;

/**
 * @author by kimsuor.seang  on 10/14/2017.
 */
public class CurrentEmploymentPanelKFP extends AbstractControlPanel implements ApplicationFields {

    private static final long serialVersionUID = -1284665400383791317L;

    private Employment employment;

    private TextField txtWorkPlaceName;
    private TextField txtWorkPhone;
    private TextField txtTimeWithEmployerInYear;
    private TextField txtTimeWithEmployerInMonth;
    private Label lblRevenue;
    private AutoTextField txtRevenue;
    private AutoTextField txtAllowance;
    private AutoTextField txtBusinessExpense;
    private AutoTextField txtNetIncome;
    private CheckBox cbAllowCallToWorkPlace;
    private CheckBox cbSameApplicantAddress;
    private ERefDataComboBox<EEmploymentStatus> cbxEmploymentStatus;
    private ERefDataComboBox<EEmploymentIndustry> cbxEmploymentIndustry;
    private ERefDataComboBox<ESeniorityLevel> cbxSeniorityLevel;
    private EntityRefComboBox<EmploymentOccupation> cbxEmploymentOccupation;
    private EntityRefComboBox<EmploymentPosition> cbxEmploymentPosition;
    private AddressPanel addressFormPanel;
    private HorizontalLayout contentPanel;
    private Individual individual;
    private CustomLayout customLayout;
    private Address address;
    private List<EmploymentOccupation> emOccupations;
    private List<EmploymentPosition> employmentPositions;

    private AutoDateField dfDateOfBirthGT;
    private AutoDateField dfDateOfBirthApl;
    private ValidateNumbers curentAdd;
    private List txts;
    private List txtnodot;
    private Quotation quotation;

    private BusinessScaleIncomePanel businessScaleIncomePanel;

    public CurrentEmploymentPanelKFP() {
        individual = new Individual();
        address = new Address();
        curentAdd = new ValidateNumbers();
        txts = new ArrayList();
        txtnodot = new ArrayList();
        txtTimeWithEmployerInYear = ComponentFactory.getTextField(false, 20, 50);
        txts.add(txtTimeWithEmployerInYear);
        txtTimeWithEmployerInMonth = ComponentFactory.getTextField(false, 20, 50);
        txts.add(txtTimeWithEmployerInMonth);

        txtRevenue = new AutoTextField();
        txtRevenue.setRequired(false);
        txtRevenue.setMaxLength(50);
        txtRevenue.setWidth("150");
        txtnodot.add(txtRevenue);

        txtAllowance = new AutoTextField();
        txtAllowance.setRequired(false);
        txtAllowance.setMaxLength(50);
        txtAllowance.setWidth("150");
        txtnodot.add(txtAllowance);

        txtBusinessExpense = new AutoTextField();
        txtBusinessExpense.setRequired(false);
        txtBusinessExpense.setMaxLength(50);
        txtBusinessExpense.setWidth("150");
        txts.add(txtBusinessExpense);

        txtWorkPlaceName = ComponentFactory.getTextField(false, 100, 300);
        txtWorkPhone = ComponentFactory.getTextField(false, 30, 150);
        cbxEmploymentStatus = new ERefDataComboBox<>("", EEmploymentStatus.class);
        cbxEmploymentStatus.setImmediate(true);
        cbxEmploymentIndustry = new ERefDataComboBox<>("", EEmploymentIndustry.class);

        cbxEmploymentOccupation = new EntityRefComboBox<>();
        cbxEmploymentOccupation.setRestrictions(new BaseRestrictions<>(EmploymentOccupation.class));
        cbxEmploymentOccupation.renderer();
        cbxEmploymentOccupation.setImmediate(true);
        cbxEmploymentOccupation.setWidth("200px");
        emOccupations = DataReference.getInstance().getEmploymentOccupation();
        if (emOccupations != null && !emOccupations.isEmpty()) {
            cbxEmploymentOccupation.setVisible(true);
        }

        cbxEmploymentPosition = new EntityRefComboBox<>();
        cbxEmploymentPosition.setRestrictions(new BaseRestrictions<>(EmploymentPosition.class));
        cbxEmploymentPosition.renderer();
        cbxEmploymentPosition.setImmediate(true);
        cbxEmploymentOccupation.setWidth("200px");
        employmentPositions = DataReference.getInstance().getEmploymentPossitions();
        if (employmentPositions != null && !employmentPositions.isEmpty()) {
            cbxEmploymentPosition.setVisible(true);
        }


        cbxSeniorityLevel = new ERefDataComboBox<>("", ESeniorityLevel.class);
        cbxSeniorityLevel.setImmediate(true);

        cbAllowCallToWorkPlace = new CheckBox();
        cbAllowCallToWorkPlace.setValue(false);

        cbSameApplicantAddress = new CheckBox(I18N.message("same.applicant.address"));
        cbSameApplicantAddress.setValue(false);
        cbSameApplicantAddress.setImmediate(true);

        lblRevenue = new Label(I18N.message("basic.salary"));

        buildLayoutCurrentEmploymentPanel();
        addValueChanges();

    }

    public void buildLayoutCurrentEmploymentPanel() {
        String template = "currentEmployment";
        InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
        customLayout = null;
        try {
            customLayout = new CustomLayout(layoutFile);
        } catch (IOException e) {
            Notification.show("Could not locate template " + template, e.getMessage(), Type.ERROR_MESSAGE);
        }
        customLayout.addComponent(new Label(I18N.message("per.month")), "lblPerMonth");
        customLayout.addComponent(new Label(I18N.message("work.place.name")), "lblEmployerName");
        customLayout.addComponent(txtWorkPlaceName, "txtWorkPlaceName");
        customLayout.addComponent(new Label(I18N.message("work.phone")), "lblWorkPhone");
        customLayout.addComponent(txtWorkPhone, "txtWorkPhone");
        customLayout.addComponent(new Label(I18N.message("time.with.employer")), "lblTimeWithEmployer");
        customLayout.addComponent(new Label(I18N.message("years")), "lblYears");
        customLayout.addComponent(new Label(I18N.message("months")), "lblMonths");
        customLayout.addComponent(txtTimeWithEmployerInYear, "txtTimeWithEmployerInYear");
        customLayout.addComponent(txtTimeWithEmployerInMonth, "txtTimeWithEmployerInMonth");
        customLayout.addComponent(new Label(I18N.message("position")), "lblPosition");
        curentAdd.validateNumber(txts);
        curentAdd.validateNumberDot(txtnodot);
        customLayout.addComponent(lblRevenue, "lblRevenue");
        customLayout.addComponent(txtRevenue, "txtRevenue");
        customLayout.addComponent(new Label(I18N.message("allowance.etc")), "lblAverageMonthlyAllowance");
        customLayout.addComponent(txtAllowance, "txtAverageMonthlyAllowance");
        customLayout.addComponent(new Label(I18N.message("business.expense")), "lblBusinessExpense");
        customLayout.addComponent(txtBusinessExpense, "txtBusinessExpense");
        customLayout.addComponent(new Label(I18N.message("employment.status")), "lblEmploymentStatus");
        customLayout.addComponent(cbxEmploymentStatus, "cbxEmploymentStatus");
        customLayout.addComponent(new Label(I18N.message("current.address")), "lblCurrentAddress");
        customLayout.addComponent(new Label(I18N.message("employment.industry")), "lblEmploymentIndustry");
        customLayout.addComponent(cbxEmploymentIndustry, "cbxEmploymentIndustry");
        customLayout.addComponent(new Label(I18N.message("occupation")), "lblOccupation");
        customLayout.addComponent(new Label(I18N.message("position")), "lblPosition");
        customLayout.addComponent(cbxEmploymentOccupation, "cbxOccupation");
        customLayout.addComponent(cbxEmploymentPosition, "cbxEmploymentPosition");
        customLayout.addComponent(new Label(I18N.message("seniorities.level")), "lblSeniorityLevel");
        customLayout.addComponent(cbxSeniorityLevel, "cbxSeniorityLevel");
        customLayout.addComponent(new Label(I18N.message("allow.call.to.work.place")), "lblAllowCallToWorkPlace");
        customLayout.addComponent(cbAllowCallToWorkPlace, "cbAllowCallToWorkPlace");

        addressFormPanel = new AddressPanel(true, ETypeAddress.WORK, "companyAddress");

        if (employment != null && businessScaleIncomePanel != null) {
            businessScaleIncomePanel.setEnabled(false);
            businessScaleIncomePanel.assignValues(employment);
            getUpdateCurrentEmploymentPanel(businessScaleIncomePanel, new CurrentEmploymentPanelKFP());
        } else {
            businessScaleIncomePanel = new BusinessScaleIncomePanel(true, "businessScaleIncome");
        }
        txtNetIncome = businessScaleIncomePanel.getTxtNetIncome();
        txtNetIncome.setImmediate(true);
        txtNetIncome.addBlurListener(new FieldEvents.BlurListener() {
            private static final long serialVersionUID = -5023781933968810733L;

            @Override
            public void blur(FieldEvents.BlurEvent event) {
                autoCalculateTextNetIncome();
            }
        });

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addComponent(cbSameApplicantAddress);
        verticalLayout.addComponent(addressFormPanel);
        verticalLayout.addComponent(businessScaleIncomePanel);

        contentPanel = new HorizontalLayout();
        contentPanel.setMargin(true);
        contentPanel.setSpacing(true);
        contentPanel.addComponent(customLayout);
        contentPanel.addComponent(verticalLayout);
        addComponent(contentPanel);
    }

    public void setApplicant(Individual individual, EApplicantType applicantType) {
        this.individual = individual;
        cbSameApplicantAddress.setCaption(I18N.message("same.applicant.address"));
        if (applicantType == EApplicantType.C) {
            dfDateOfBirthApl = new AutoDateField();
            if (individual != null)
                dfDateOfBirthApl.setValue(individual.getBirthDate() != null ? individual.getBirthDate() : todayDate());

        } else if (applicantType == EApplicantType.G) {
            dfDateOfBirthGT = new AutoDateField();
            if (individual != null)
                dfDateOfBirthGT.setValue(individual.getBirthDate() != null ? individual.getBirthDate() : todayDate());
        }
    }

    public void assignValues(Employment employment) {
        this.employment = employment;
        this.removeAllComponents();
        this.buildLayoutCurrentEmploymentPanel();
        if(employment != null){
            txtTimeWithEmployerInYear.setValue(getDefaultString(employment.getTimeWithEmployerInYear()));
            txtTimeWithEmployerInMonth.setValue(getDefaultString(employment.getTimeWithEmployerInMonth()));
            txtRevenue.setValue(AmountUtils.format(employment.getRevenue()));
            txtAllowance.setValue(AmountUtils.format(employment.getAllowance()));
            txtBusinessExpense.setValue(AmountUtils.format(employment.getBusinessExpense()));
            txtWorkPlaceName.setValue(getDefaultString(employment.getWorkPlaceName()));
            txtWorkPhone.setValue(getDefaultString(employment.getWorkPhone()));
            cbxEmploymentStatus.setSelectedEntity(employment.getEmploymentStatus());
            cbxEmploymentIndustry.setSelectedEntity(employment.getEmploymentIndustry());
            cbxEmploymentOccupation.setSelectedEntity(employment.getEmploymentOccupation());
            cbxEmploymentPosition.setSelectedEntity(employment.getEmploymentPosition());
            cbxSeniorityLevel.setSelectedEntity(employment.getSeniorityLevel());
            cbAllowCallToWorkPlace.setValue(employment.isAllowCallToWorkPlace());
            cbSameApplicantAddress.setValue(employment.isSameApplicantAddress());
            Address address = employment.getAddress();
            if (address == null) {
                address = new Address();
                address.setCountry(ECountry.KHM);
            }
            assignAddressValues(address);
        }
    }

    public void assignAddressValues(Address address) {
        addressFormPanel.assignValues(address);
    }

    public Employment getEmployment(Employment employment) {
        employment.setTimeWithEmployerInYear(MyNumberUtils.getInteger(getInteger(txtTimeWithEmployerInYear)));
        employment.setTimeWithEmployerInMonth(MyNumberUtils.getInteger(getInteger(txtTimeWithEmployerInMonth)));
        employment.setRevenue(MyNumberUtils.getDouble(getDouble(txtRevenue)));
        employment.setAllowance(MyNumberUtils.getDouble(getDouble(txtAllowance)));
        employment.setBusinessExpense(getDouble(txtBusinessExpense));
        employment.setWorkPlaceName(txtWorkPlaceName.getValue());
        employment.setWorkPhone(txtWorkPhone.getValue());
        employment.setEmploymentStatus(cbxEmploymentStatus.getSelectedEntity());
        employment.setEmploymentIndustry(cbxEmploymentIndustry.getSelectedEntity());
        employment.setEmploymentOccupation(cbxEmploymentOccupation.getSelectedEntity());
        employment.setEmploymentPosition(cbxEmploymentPosition.getSelectedEntity());
        employment.setSeniorityLevel(cbxSeniorityLevel.getSelectedEntity());
        employment.setEmploymentType(EEmploymentType.CURR);
        employment.setAllowCallToWorkPlace(cbAllowCallToWorkPlace.getValue());
        employment.setSameApplicantAddress(cbSameApplicantAddress.getValue());
        if (employment.getAddress() == null) {
            Address address = new Address();
            address.setCountry(ECountry.KHM);
            employment.setAddress(address);
        }
        employment.setAddress(addressFormPanel.getAddress(employment.getAddress()));
        return employment;
    }

    public boolean isSameApplicantAddress() {
        return cbSameApplicantAddress.getValue();
    }

    public void reset() {
        individual = null;
    }

    public List<String> isValid() {
        super.reset();
        checkMandatorySelectField(cbxEmploymentStatus, "employment.status");
        if (cbxEmploymentStatus.getSelectedEntity() != null && EMP_CODE.equals(cbxEmploymentStatus.getSelectedEntity().getCode())) {
            if (dfDateOfBirthApl != null ? applicantNgurantor(dfDateOfBirthApl.getValue()) : false)
                errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("Status Employ Cannot Allow Applicant Age More Than 61"));
            else if (dfDateOfBirthGT != null ? applicantNgurantor(dfDateOfBirthGT.getValue()) : false)
                errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("Status Employ Cannot Allow Guarantor Age More Than 61"));
        }
        checkMandatorySelectField(cbxEmploymentIndustry, "employment.industry");
        checkMandatorySelectField(cbxEmploymentOccupation, "employment.occupation");
        checkMandatorySelectField(cbxEmploymentPosition, "employment.position");
        checkMandatorySelectField(cbxSeniorityLevel, "seniorities.level");
        checkMandatoryField(txtRevenue, "revenue");
        checkMandatoryField(txtWorkPlaceName, "employer");
        checkMandatoryField(txtTimeWithEmployerInYear, "time.with.employer");
        checkMandatoryField(txtTimeWithEmployerInMonth, "time.with.employer");
        if (!ValidateNumbers.isValidOnlyNumber(txtWorkPhone.getValue()))
            errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("work phone contain character"));

        /* Check Khmer Phone Number */
        if (!ValidateNumbers.isValidPhoneNumber(txtWorkPhone.getValue())) {
            errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("WorkPhone is invalid!"));
        }
        errors.addAll(addressFormPanel.partialValidate());

        return errors;
    }

    // validate age as employ status
    public boolean applicantNgurantor(Date date) {
        return date == null ? false : getYear(new Date()) - getYear(date) > 61;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void addValueChanges() {
        cbxEmploymentIndustry.addValueChangeListener(new ValueChangeListener() {
            private static final long serialVersionUID = 6740681769253190853L;
            @Override
            public void valueChange(ValueChangeEvent event) {
                if (cbxEmploymentIndustry.getSelectedEntity() != null) {
                    BaseRestrictions<EmploymentOccupation> restrictions = cbxEmploymentOccupation.getRestrictions();
                    List<Criterion> criterions = new ArrayList<>();
                    criterions.add(Restrictions.eq("employmentIndustry", cbxEmploymentIndustry.getSelectedEntity()));
                    criterions.add(Restrictions.eq("statusRecord", EStatusRecord.ACTIV));
                    restrictions.setCriterions(criterions);
                    cbxEmploymentOccupation.renderer();
                } else {
                    cbxEmploymentOccupation.clear();
                }
            }
        });
        cbxEmploymentOccupation.addValueChangeListener(new ValueChangeListener() {
            private static final long serialVersionUID = -2391099593835879734L;

            @Override
            public void valueChange(ValueChangeEvent event) {
                if (cbxEmploymentOccupation.getSelectedEntity() != null) {
                    if (cbxEmploymentPosition.getSelectedEntity() == null) {
                        BaseRestrictions<EmploymentPosition> restrictions = cbxEmploymentPosition.getRestrictions();
                        List<Criterion> criterions = new ArrayList<>();
                        criterions.add(Restrictions.eq("employmentOccupation.id", cbxEmploymentOccupation.getSelectedEntity().getId()));
                        criterions.add(Restrictions.eq("statusRecord", EStatusRecord.ACTIV));
                        restrictions.setCriterions(criterions);
                        cbxEmploymentPosition.renderer();
                    } else {
                        cbxEmploymentPosition.clear();
                    }
                } else {
                    BaseRestrictions<EmploymentPosition> restrictions = cbxEmploymentPosition.getRestrictions();
                    List<Criterion> criterions = new ArrayList<>();
                    restrictions.setCriterions(criterions);
                    cbxEmploymentPosition.renderer();
                }
            }
        });

        cbxEmploymentPosition.addValueChangeListener(new ValueChangeListener() {
            private static final long serialVersionUID = -7065465300200812231L;

            @Override
            public void valueChange(ValueChangeEvent event) {
                if (cbxEmploymentPosition.getSelectedEntity() != null) {
                    cbxEmploymentPosition.setSelectedEntity(cbxEmploymentPosition.getSelectedEntity());

                }
            }
        });
        cbxEmploymentStatus.addValueChangeListener(new ValueChangeListener() {
            private static final long serialVersionUID = 3039130459507880370L;
            public void valueChange(ValueChangeEvent event) {
                if (cbxEmploymentStatus.getSelectedEntity() != null
                        && EMP_CODE.equals(cbxEmploymentStatus.getSelectedEntity().getCode())) {
                    cbxSeniorityLevel.setValue(ESeniorityLevel.getByField(cbxEmploymentStatus.getSelectedEntity()));
                    lblRevenue.setValue(I18N.message("basic.salary"));
                    txtBusinessExpense.setValue("");
                    txtBusinessExpense.setEnabled(false);
                    txtAllowance.setEnabled(true);
                } else {
                    lblRevenue.setValue(I18N.message("total.sales"));
                    txtAllowance.setValue("");
                    txtAllowance.setEnabled(false);
                    txtBusinessExpense.setEnabled(true);
                    cbxSeniorityLevel.setSelectedEntity(null);
                }
            }
        });

        cbSameApplicantAddress.addValueChangeListener(new ValueChangeListener() {
            private static final long serialVersionUID = -4186841321484425109L;

            @Override
            public void valueChange(ValueChangeEvent event) {
                Address applicantAddress = individual != null ? individual.getMainAddress() : new Address();
                if (address == null) {
                    address = new Address();
                    address.setCountry(ECountry.KHM);
                }
                if(cbSameApplicantAddress.getValue() != null){
                    if (cbSameApplicantAddress.getValue()) {
                        address = AddressUtils.copy(applicantAddress, address);
                    }
                    addressFormPanel.assignValues(address);
                    addressFormPanel.setAddressEnabled(!cbSameApplicantAddress.getValue());
                }
            }
        });
    }

    public void setCurrentEmploymentQuotation(Quotation quotation) {
        this.quotation = quotation;
    }

    private void getUpdateCurrentEmploymentPanel(final BusinessScaleIncomePanel businessScaleIncomePanel, final CurrentEmploymentPanelKFP currentEmploymentPanelKFP) {
        currentEmploymentPanelKFP.removeAllComponents();
        currentEmploymentPanelKFP.addComponent(businessScaleIncomePanel);
    }

    private void autoCalculateTextNetIncome() {
        double revenue = AmountBigUtils.getValueDouble(txtRevenue);
        double allowance = AmountBigUtils.getValueDouble(txtAllowance);
        double netIncome = AmountBigUtils.getValueDouble(txtNetIncome);
        double total = revenue + allowance - netIncome;
        if(total < 0){
            total =-total;
        }
        txtBusinessExpense.setValue(AmountBigUtils.format(total));
    }
}
