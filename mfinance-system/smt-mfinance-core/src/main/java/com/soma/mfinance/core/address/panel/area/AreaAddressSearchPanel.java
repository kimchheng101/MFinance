package com.soma.mfinance.core.address.panel.area;

import com.soma.mfinance.core.address.model.Area;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;

/**
 * Area code search panel in collection
 * @author kimsuor.seang
 */
public class AreaAddressSearchPanel extends AbstractSearchPanel<Area> implements FMEntityField {

    /** */
	private static final long serialVersionUID = -2663537408529173899L;

	private TextField txtAreaCode;
    
    /**
     * 
     * @param areaCodeTablePanel
     */
	public AreaAddressSearchPanel(AreaAddressTablePanel areaTablePanel) {
		super(I18N.message("search"), areaTablePanel);
	}
	
	/**
	 * @see AbstractSearchPanel#reset()
	 */
	@Override
	protected void reset() {
		txtAreaCode.setValue("");
	}

	/**
	 * @see AbstractSearchPanel#createForm()
	 */
	@Override
	protected Component createForm() {
		txtAreaCode = ComponentFactory.getTextField("area.code", false, 60, 200);
		HorizontalLayout mainLayout = new HorizontalLayout();
        mainLayout.setSpacing(true);
        mainLayout.addComponent(new FormLayout(txtAreaCode));
		return mainLayout;
	}

	/**
	 * @see AbstractSearchPanel#getRestrictions()
	 */
	@Override
	public BaseRestrictions<Area> getRestrictions() {		
		BaseRestrictions<Area> restrictions = new BaseRestrictions<>(Area.class);	
		if (StringUtils.isNotEmpty(txtAreaCode.getValue())) {
			restrictions.addCriterion(Restrictions.ilike(CODE, txtAreaCode.getValue(), MatchMode.ANYWHERE));
		}
		restrictions.addOrder(Order.desc(ID));
		return restrictions;
	}

}
