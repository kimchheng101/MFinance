package com.soma.mfinance.core.shared.dealer;

import com.soma.mfinance.core.shared.FMEntityField;

/**
 * @author kimsuor.seang
 */
public interface DealerEntityField extends FMEntityField {

	String DEALER_TYPE = "dealerType";
	
}
