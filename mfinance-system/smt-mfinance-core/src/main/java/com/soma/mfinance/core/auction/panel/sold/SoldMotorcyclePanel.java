package com.soma.mfinance.core.auction.panel.sold;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * AUTHOR : kimsuor.seang
 * EMAIL  : chhaichivon1995@gmail.com
 * DATE   : 8/29/2017
 * TIME   : 10:29 AM
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(SoldMotorcyclePanel.NAME)
public class SoldMotorcyclePanel extends AbstractTabsheetPanel implements View {
    public static final String NAME = "sold.asset";
    @Autowired
    private SoldMotorcycleTablePanel tablePanel;
    @Autowired
    private SoldMotorcycleFormPanel formPanel;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        tablePanel.setMainPanel(this);
        formPanel.setCaption(I18N.message("sold.motorcycles"));
        getTabSheet().setTablePanel(tablePanel);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }

    @Override
    public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
        if (selectedTab == formPanel) {
            formPanel.assignValues(tablePanel.getItemSelectedId());
            formPanel.setMainPanel(this);
        } else if (selectedTab == tablePanel && getTabSheet().isNeedRefresh()) {
            tablePanel.refresh();
        }
        getTabSheet().setSelectedTab(selectedTab);
    }

    @Override
    public void onAddEventClick() {
        formPanel.reset();
        getTabSheet().addFormPanel(formPanel);
        getTabSheet().setSelectedTab(formPanel);
    }

    @Override
    public void onEditEventClick() {
        getTabSheet().addFormPanel(formPanel);
        initSelectedTab(formPanel);
    }

    public void displayTablePanel() {
        tablePanel.refresh();
        getTabSheet().setSelectedTab(tablePanel);
    }
}
