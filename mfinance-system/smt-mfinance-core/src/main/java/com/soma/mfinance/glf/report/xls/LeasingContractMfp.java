package com.soma.mfinance.glf.report.xls;

import com.soma.mfinance.core.Enum.EHousing;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.applicant.model.Employment;
import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.financial.model.EServiceType;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationDocument;
import com.soma.mfinance.core.quotation.model.QuotationService;
import com.soma.mfinance.core.shared.conf.AppConfig;
import com.soma.mfinance.core.shared.financialproduct.FinancialProductEntityField;
import com.soma.mfinance.core.shared.report.ReportParameter;
import com.soma.mfinance.tools.report.Report;
import com.soma.ersys.core.hr.model.address.Address;
import com.soma.ersys.core.hr.model.eref.EGender;
import com.soma.ersys.core.hr.model.eref.EMaritalStatus;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.springframework.util.Assert;

import java.io.IOException;
import java.util.*;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/28/2017
 * Name  : 9:40 AM
 * Email : k.seang@gl-f.com
 */
public class LeasingContractMfp implements Report, FinServicesHelper, LeasingContractField {

    Map<String, Object> mapLeasingContractValue = null;
    public final static String REGISTRATION_LABEL = "ចំណាយ​ក្នុង​ការ​ចុះ​បញ្ជី​លើក​ដំបូង";


    @Override
    public String generate(ReportParameter reportParameter) throws Exception {
        Long quotaId = (Long) reportParameter.getParameters().get("quotaId");
        Assert.notNull(quotaId, "Quotation Id can not be null. !");
        Quotation quotation = QUO_SRV.getById(Quotation.class, quotaId);
        Assert.notNull(quotation, "Quotation could not be null.");

        Applicant applicant = quotation.getApplicant();
        Individual individual = applicant.getIndividual();
        Employment applicantEmployment = individual.getCurrentEmployment();
        Address applicantAddress = individual.getMainAddress();
        Address employmentAddress = applicantEmployment.getAddress();
        Asset asset = quotation.getAsset();


        // initialize list of departments in some way
        String leaseName = "";
        String leaseGender = "";
        if (individual != null) {
            leaseName = StringUtils.defaultString(individual.getLastName()) + " " + StringUtils.defaultString(individual.getFirstName());
            leaseGender = individual.getGender() != null ? StringUtils.defaultString(individual.getGender().getDesc()) : "";

        }
        Map<String, String> beans = new HashMap<String, String>();

        //Detail 1
        lesseeDetail(quotation, applicant, individual, leaseName, leaseGender, beans);


        //Detail 2
        this.currentLesseeAddress(individual, applicantAddress, beans);

        // detail 3 work place
        this.lesseeJobAndWorkplace(applicantEmployment, employmentAddress, beans);

        //Detail 4
        // need to change change after or when deploy to real, because it should be
        // is current address
        beans.put(IS_CURRENT_ADDRESS, applicant.getIndividual().getECorrespondence().getCode().equals("001") ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(IS_ID_ADDRESS, applicant.getIndividual().getECorrespondence().getCode().equals("002") ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(IS_HOUSE_ADDRESS, applicant.getIndividual().getECorrespondence().getCode().equals("003") ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(IS_WORK_ADDRESS, applicant.getIndividual().getECorrespondence().getCode().equals("004") ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(IS_OTHER_ADDRESS, applicant.getIndividual().getECorrespondence().getCode().equals(null) ? CHECKING_SYMBOL : CHECKING_SPACE);

        // detail 5
        Date firstPaymentDate = this.contractReRent(quotation, beans);

        //Detail 6
        this.reRentProduct(asset, beans);


        //detail 7
        this.payment(quotation, beans, firstPaymentDate);


        //DETAIL 8 Payment
        this.paymentMethod(quotation, beans);


        return this.generatetoExcel(beans);

    }

    private void lesseeDetail(Quotation quotation, Applicant applicant, Individual individual, String leaseName, String leaseGender, Map<String, String> beans) {
        beans.put(LESSEE_NAME, leaseName);
        beans.put(LESSEE_SEX, leaseGender);
        beans.put(LESSEE_BD, DateUtils.getDateLabel(individual.getBirthDate(), DateUtils.FORMAT_DDMMYYYY_SLASH));
        beans.put(LESSEE_AGE, "" + DateUtils.getAge(individual.getBirthDate()));
        List<QuotationDocument> guranterDocumsnets = quotation.getQuotationDocuments(EApplicantType.C);
        this.showLeaseeInfomationCheckbox(beans, guranterDocumsnets);


        double totalNetIncome = 0d;
        double totalRevenus = 0d;
        double totalAllowance = 0d;
        double totalBusinessExpenses = 0d;
        List<Employment> employments = individual.getEmployments();
        for (Employment employment : employments) {
            totalRevenus += MyNumberUtils.getDouble(employment.getRevenue());
            totalAllowance += MyNumberUtils.getDouble(employment.getAllowance());
            totalBusinessExpenses += MyNumberUtils.getDouble(employment.getBusinessExpense());
        }
        totalNetIncome = totalRevenus + totalAllowance - totalBusinessExpenses;

        beans.put(LESSEE_PHONENUM1, applicant.getIndividual().getMobilePerso() == "" ? applicant.getIndividual().getMobilePerso2() : applicant.getIndividual().getMobilePerso());
        beans.put(LESSEE_INCOME, "" + AmountUtils.format(totalNetIncome));
        beans.put(ISLESSEE_SINGLE, individual.getMaritalStatus().equals(EMaritalStatus.SINGLE) ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(ISLESSEE_SPOUSE, individual.getMaritalStatus().equals(EMaritalStatus.MARRIED) ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(ISLESSEE_DIVORCE, (individual.getMaritalStatus().equals(EMaritalStatus.DIVORCED) || individual.getMaritalStatus().equals(EMaritalStatus.SEPARATED) || individual.getMaritalStatus().equals(EMaritalStatus.WIDOW)) ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(LESSEE_NUM_CHILDREN, "" + MyNumberUtils.getInteger(individual.getNumberOfChildren()));
        beans.put(LESSEE_SPOUSE_NAME, applicant.getIndividual().getIndividualSpouse().getLastName() + " " + applicant.getIndividual().getIndividualSpouse().getFirstName());
        beans.put(LESSEE_SPOUSE_PHONE_NUM, applicant.getIndividual().getIndividualSpouse().getMobilePhone());
        beans.put(LESSEE_ADJUSTMENTAMOUNT, quotation.getTiFinanceAmount().toString());
    }

    private void currentLesseeAddress(Individual individual, Address applicantAddress, Map<String, String> beans) {
        beans.put(LESSEE_HOUSE_NUM, applicantAddress.getHouseNo());
        beans.put(LESSEE_STREET_NUM, applicantAddress.getStreet());
        beans.put(LESSEE_VILLAGE, applicantAddress.getVillage().getDesc());
        beans.put(LESSEE_COMMUNE, applicantAddress.getCommune().getDesc());
        beans.put(LESSEE_DISTRICT, applicantAddress.getDistrict().getDesc());
        beans.put(LESSEE_PROVINCE, applicantAddress.getProvince().getDesc());

        beans.put(LESSEE_LENGTH_STAY, "" + (MyNumberUtils.getInteger(individual.getIndividualAddresses().get(0).getTimeAtAddressInYear())));
        beans.put(LESSEE_LENGTH_STAY_MONTH, "" + (MyNumberUtils.getInteger(individual.getIndividualAddresses().get(0).getTimeAtAddressInMonth())));
        beans.put(LESSEE_AVALABLEDATETIME, StringUtils.defaultString(individual.getConvenientVisitTime()));

        //EResidenceStatus lesseeResidenceStatus = applicantAddress.getProperty();
        beans.put(IS_LESSEE_PARENTHOUSE, individual.getIndividualAddresses().get(0).getHousing().equals(EHousing.TEST) ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(IS_LESSEE_OWNHOUSE, individual.getIndividualAddresses().get(0).getHousing().getCode().equals("owner") ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(IS_LESSEE_RELATIVEHOUSE, individual.getIndividualAddresses().get(0).getHousing().getCode().equals("relative") ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(IS_LESSEE_RENTINGHOUSE, individual.getIndividualAddresses().get(0).getHousing().getCode().equals("rental") ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(IS_LESSEE_OTHER, individual.getIndividualAddresses().get(0).getHousing().getCode().equals("other") ? CHECKING_SYMBOL : CHECKING_SPACE);
    }

    // detail 3 job and workplace lessee
    private void lesseeJobAndWorkplace(Employment applicantEmployment, Address employmentAddress, Map<String, String> beans) {
        beans.put(LESSEE_COMPANY_NAME, applicantEmployment.getWorkPlaceName() != null ? applicantEmployment.getWorkPlaceName() : " ");
        beans.put(LESSEE_COMPANY_TYPE_BUSINESS, applicantEmployment.getEmploymentIndustry() != null ? applicantEmployment.getEmploymentIndustry().getDesc() : " ");
        beans.put(LESSEE_COMPANY_HOUSE_NUM, StringUtils.defaultString(applicantEmployment.getAddress().getHouseNo() != null ? applicantEmployment.getAddress().getHouseNo() : " "));
        beans.put(LESSEE_COMPANY_STREET_NUM, StringUtils.defaultString(applicantEmployment.getAddress().getStreet() != null ? applicantEmployment.getAddress().getStreet() : " "));
        beans.put(LESSEE_COMPANY_VILLAGE, StringUtils.defaultString(applicantEmployment.getAddress().getVillage() != null ? applicantEmployment.getAddress().getVillage().getDesc() : " "));
        beans.put(LESSEE_COMPANY_COMMUNE, applicantEmployment.getAddress().getCommune() != null ? applicantEmployment.getAddress().getCommune().getDesc() : " ");
        beans.put(LESSEE_COMPANY_DISTRICT, applicantEmployment.getAddress().getDistrict() != null ? applicantEmployment.getAddress().getDistrict().getDesc() : " ");
        beans.put(LESSEE_COMPANY_PROVINCE, applicantEmployment.getAddress().getProvince().getDesc());
        beans.put(LESSEE_COMPANY_PHONE_NUM, StringUtils.defaultString(applicantEmployment.getWorkPhone() != null ? applicantEmployment.getWorkPhone() : " "));
        beans.put(LESSEE_JOB, String.valueOf(applicantEmployment.getEmploymentPosition().getDesc()));
    }

    private Date contractReRent(Quotation quotation, Map<String, String> beans) {
        Date contractStartDate = quotation.getContractStartDate();
        Date firstPaymentDate = quotation.getFirstDueDate();
        beans.put(AGREEMENT_NUM, quotation.getReference());
        beans.put(AGREEMENT_DATE, (contractStartDate == null ? "" : DateUtils.getDateLabel(contractStartDate, DateUtils.FORMAT_DDMMYYYY_SLASH)));
        if (quotation != null && quotation.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_PUBLIC)) {
            beans.put("productName", "Motor for Cash Public");
        } else {
            beans.put("productName", "Motor for Cash");
        }
        return firstPaymentDate;
    }

    private void reRentProduct(Asset asset, Map<String, String> beans) {
        beans.put(VEHICLE_MODEL, asset.getModel().getDesc());
        beans.put(VEHICLE_COLOR, asset.getColor().getDesc());
        beans.put(LESSEE_VEHICLEGENDER, asset.getAssetGender().getDesc());
        beans.put(VEHICLE_MANUFACTUREDATE, StringUtils.defaultString(String.valueOf(asset.getAssetYear())));
        beans.put(VEHICLE_MARK, getGenderDesc(asset.getAssetGender().getCode()));
        beans.put(VEHICLE_CC, StringUtils.defaultString(asset.getEngine().getDescEn()));
        beans.put(VEHICLE_ENGINENUM, asset.getEngineNumber());
        beans.put(VEHICLESERIENUM, asset.getChassisNumber());
    }

    private void paymentMethod(Quotation quotation, Map<String, String> beans) {
        beans.put(IS_PAYMENT_6TIME, quotation.getTerm() == 12 ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(IS_PAYMENT_9TIME, quotation.getTerm() == 24 ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(IS_PAYMENT_12TIME, quotation.getTerm() == 36 ? CHECKING_SYMBOL : CHECKING_SPACE);

        /*Transfer owner fee*/
        if (quotation.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_EXISTING)) {
            if (quotation.getQuotationServiceByServiceType(EServiceType.TRANSFEE).isTransferFee()) {
                beans.put(IS_FIVE_PAY, quotation.getTerm() == 12 ? CHECKING_SYMBOL : CHECKING_SPACE);
                beans.put(IS_TWO_FIVE_PAY, quotation.getTerm() == 24 ? CHECKING_SYMBOL : CHECKING_SPACE);
                beans.put(IS_ONE_SEVEN_PAY, quotation.getTerm() == 36 ? CHECKING_SYMBOL : CHECKING_SPACE);
            } else
                beans.put(IS_EXT, CHECKING_SYMBOL);
        } else if (quotation.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_PUBLIC)) {
            beans.put(IS_FIVE_PAY, quotation.getTerm() == 12 ? CHECKING_SYMBOL : CHECKING_SPACE);
            beans.put(IS_TWO_FIVE_PAY, quotation.getTerm() == 24 ? CHECKING_SYMBOL : CHECKING_SPACE);
            beans.put(IS_ONE_SEVEN_PAY, quotation.getTerm() == 36 ? CHECKING_SYMBOL : CHECKING_SPACE);
        }
        /*Verify how to pay money */
        beans.put(IS_MONTHLY_PAYMENT, quotation.getFrequency().getCode().equals("M") ? CHECKING_SYMBOL : CHECKING_SPACE);
    }

    private String generatetoExcel(Map<String, String> beans) throws IOException, InvalidFormatException {
        String templatePath = AppConfig.getInstance().getConfiguration().getString("specific.templatedir");
        String templateFileName = templatePath + "/LeasingContractMfp.xlsx";

        String outputPath = AppConfig.getInstance().getConfiguration().getString("specific.tmpdir");

        String prefixOutputName = "LeasingContractMfp";
        String sufixOutputName = "xlsx";
        String uuid = UUID.randomUUID().toString().replace("-", "");
        String xlsFileName = outputPath + "/" + prefixOutputName + uuid + "." + sufixOutputName;

        XLSTransformer transformer = new XLSTransformer();
        transformer.transformXLS(templateFileName, beans, xlsFileName);

        return prefixOutputName + uuid + "." + sufixOutputName;
    }

    private void payment(Quotation quotation, Map<String, String> beans, Date firstPaymentDate) {
        //1
        beans.put(LESSEE_TiINSTALLMENTAMOUNT, "" + AmountUtils.format(quotation.getTiFinanceAmount()));
        beans.put(LESSEE_ADJUSTMENTAMOUNT, "" + AmountUtils.format(quotation.getTiFinanceAmount()));
        //2
        beans.put(MONTHLY_PAYMENT, "" + AmountUtils.format(quotation.getTotalInstallmentAmount()));
        //3
        beans.put(TERM_PAYMENT, "" + quotation.getTerm());
        beans.put(ADVANCE_PAYMENT_PERCENTAGE, quotation.getInterestRate() + "%");  // Note! Don't know from which field yet
        beans.put(EFFECTIVE_PAYMENT_PERCENTAGE, quotation.getIrrRate() != null ? AmountUtils.format(quotation.getIrrRate()) + " %" : " ");  // Note! Don't know from which field yet
        beans.put(MONTHLYPAYMENTDATE, (firstPaymentDate == null ? "" : leftPad("0" + DateUtils.getDay(firstPaymentDate), 2)));
        beans.put(FIRST_PAYMENT_DATE, (firstPaymentDate == null ? "" : DateUtils.getDateLabel(firstPaymentDate, DateUtils.FORMAT_DDMMYYYY_SLASH)));
        //4
        beans.put(FIRST_PAYMENT_DATE, (firstPaymentDate == null ? "" : DateUtils.getDateLabel(firstPaymentDate, DateUtils.FORMAT_DDMMYYYY_SLASH)));
        QuotationService transferfee = quotation.getQuotationServiceByServiceType(EServiceType.TRANSFEE);

        Double leaseAmountValue = quotation.getTiFinanceAmount();
        Double InsuranceFee = FIN_PROD_SRV.getInsuranceFee(leaseAmountValue);
        Double VatInFee = FIN_PROD_SRV.getVatInsurance(leaseAmountValue);
        beans.put(INSURANCE_FEE, "" + AmountUtils.format(InsuranceFee + VatInFee));

        if (quotation.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_PUBLIC))
            beans.put(SERVICE_FEE, "" + AmountUtils.format(quotation.getQuotationServiceByServiceType(EServiceType.SRVFEE).getTiPrice() +
                    quotation.getQuotationServiceByServiceType(EServiceType.SRVFEE).getVatPrice()));
        else
            beans.put(SERVICE_FEE, "" + AmountUtils.format(quotation.getQuotationServiceByServiceType(EServiceType.SRVFEEEXT).getTiPrice() +
                    quotation.getQuotationServiceByServiceType(EServiceType.SRVFEEEXT).getVatPrice()));

        beans.put(TRANSFER_FEE, "" + (transferfee.isTransferFee() == false ? 0.0 : AmountUtils.format(transferfee.getTiPrice())));
    }

    /**
     * @param beans
     * @param guranterDocumsnets
     */
    private void showLeaseeInfomationCheckbox(Map<String, String> beans, List<QuotationDocument> guranterDocumsnets) {
        QuotationDocument idCardDocument = DOC_SRV.getQuotationDocumentByDocCode("N", guranterDocumsnets);
        beans.put(IS_LESSEEID_CARD, idCardDocument != null ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(LESSEE_ID_CARD, idCardDocument != null ? StringUtils.defaultString(idCardDocument.getReference()) : "");

        QuotationDocument reseidentBookDocument = DOC_SRV.getQuotationDocumentByDocCode("R", guranterDocumsnets);
        beans.put(ISLESSEERESIDENT_BOOKNUM, reseidentBookDocument != null ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(LESSEERESIDENT_BOOKNUM, reseidentBookDocument != null ? StringUtils.defaultString(reseidentBookDocument.getReference()) : "");

        QuotationDocument certifiedDocument = DOC_SRV.getQuotationDocumentByDocCode("G", guranterDocumsnets);
        beans.put(ISLESSEE_CERTIFYLETTERNUM, certifiedDocument != null ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(LESSEE_CERTIFYLETTERNUM, certifiedDocument != null ? certifiedDocument.getReference() : "");

        QuotationDocument familyBookDocument = DOC_SRV.getQuotationDocumentByDocCode("F", guranterDocumsnets);
        beans.put(ISLESSEE_FAMILYBOOKNUM, familyBookDocument != null ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(LESSEE_FAMILYBOOKNUM, familyBookDocument != null ? StringUtils.defaultString(familyBookDocument.getReference()) : "");

        QuotationDocument birthCertificateDocument = DOC_SRV.getQuotationDocumentByDocCode("B", guranterDocumsnets);
        beans.put(ISLESSEE_BIRTHCERTIFICATENUM, birthCertificateDocument != null ? CHECKING_SYMBOL : CHECKING_SPACE);
        beans.put(LESSEE_BIRTHCERTIFICATENUM, birthCertificateDocument != null ? StringUtils.defaultString(birthCertificateDocument.getReference()) : "");
    }

    /**
     * @param value
     * @param size
     * @return
     */
    private String leftPad(String value, int size) {
        return value == null ? null : value.substring(value.length() - size);
    }

    /*retrurn code*/
    private String getGenderDesc(String code) {
        if (EGender.M.getCode().equals(code)) {
            return "ប្រុស";
        } else if (EGender.F.getCode().equals(code)) {
            return "ស្រី";
        } else if (EGender.U.getCode().equals(code)) {
            return "មិនដឹង";
        }
        return "";
    }

}
