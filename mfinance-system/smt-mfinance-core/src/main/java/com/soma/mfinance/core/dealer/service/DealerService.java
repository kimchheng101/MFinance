package com.soma.mfinance.core.dealer.service;

import java.util.List;

import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.DealerAddress;
import com.soma.mfinance.core.dealer.model.DealerBankAccount;
import com.soma.mfinance.core.dealer.model.DealerEmployee;
import com.soma.mfinance.core.dealer.model.DealerGroup;
import com.soma.mfinance.core.dealer.model.DealerPaymentMethod;
import com.soma.mfinance.core.dealer.model.EDealerType;

/**
 * Dealer service interface
 * @author kimsuor.seang
 *
 */
public interface DealerService extends BaseEntityService {

	Dealer createDealer(Dealer dealer);

	DealerBankAccount saveOrUpdateBankAccount(DealerBankAccount bankAccount);

	List<Dealer> getBranches(Dealer parent);

	List<Dealer> getGroupType(DealerGroup group, EDealerType type);

	void saveOrUpdateDealerAddress(DealerAddress dealerAddress);

	void saveOrUpdateDealerPaymentMethod(DealerPaymentMethod dealerPaymentMethod);

	void saveOrUpdateDealerEmployeeAddress(DealerEmployee dealerEmployee);

	BaseRestrictions<Dealer> getRestrictionsDealerByDealerType(EDealerType eDealerType);

	BaseRestrictions<Dealer> getBaseRestrictionsDealer();
}
