package com.soma.mfinance.core.auction.panel.auction;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.auction.model.ContractAuctionData;
import com.soma.mfinance.core.auction.service.AuctionService;

import com.soma.mfinance.core.collection.model.EStockStatus;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.workflow.AuctionWkfStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.NumberField;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.soma.mfinance.core.asset.model.*;
import com.soma.mfinance.core.auction.model.ContractAuctionData;
import com.soma.mfinance.core.auction.service.AuctionService;
import com.soma.mfinance.core.collection.model.EStockStatus;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import org.apache.commons.lang.StringUtils;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.vaadin.dialogs.ConfirmDialog;

import javax.annotation.PostConstruct;

/**
 * Auction form panel
 * @author kimsuor.seang
 * @modify kimsuor.seang (22/09/2017)
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AuctionFormPanel extends AbstractFormPanel implements Button.ClickListener, ConfirmDialog.Listener {

	/** */
	private static final long serialVersionUID = -5488773543019942475L;
	
	@Autowired
	private AuctionService auctionService;
	
	private TextField txtRepossessionFee;
	private TextField txtCollectionFee;
	private TextField txtAssetEstimationPrice1;
	private TextField txtAssetEstimationPrice2;
	private TextField txtAssetEstimationPrice3;
	private NumberField txtMileage;
	
	private EntityRefComboBox<AssetExteriorStatus> cbxExteriorStatus;
	private EntityRefComboBox<EEngineStatus> cbxEngineStatus;
	private EntityRefComboBox<AssetPartsStatus> cbxPartsStatus;
	private EntityRefComboBox<AssetRegistrationStatus> cbxRegistrationStatus;
	private EntityRefComboBox<AssetPlateNumber> cbxPlateNumber;
	private ERefDataComboBox<EStockStatus> cbxStockStatus;
	
	private NavigationPanel navigationPanel;
	private Button btnConfirm;
	private Button btnBackToCSAndAC;	// Back to Collection supervisor and auction controller
	
	private Contract contract;
	private AuctionsPanel panel;
	private EWkfStatus auctionStatus;
	

	@PostConstruct
	public void PostConstruct() {
        super.init();
	}
 
	/**
	 * @see AbstractFormPanel#createForm()
	 */
	@Override
	protected com.vaadin.ui.Component createForm() {
		btnConfirm = new NativeButton(I18N.message("confirm"));
		btnConfirm.setIcon(new ThemeResource("../smt-default/icons/16/tick.png"));
		btnConfirm.addClickListener(this);
		
		navigationPanel = addNavigationPanel();
		navigationPanel.addSaveClickListener(this);
		
		// For Manager Button Back to Collection Supervisor and Auction Controller
		if (ProfileUtil.isManager()) {
			btnBackToCSAndAC = new NativeButton(I18N.message("back.to.collection.supervisor.and.auction.controller"));
			btnBackToCSAndAC.setIcon(new ThemeResource("../smt-default/icons/16/previous.png"));
			btnBackToCSAndAC.addClickListener(this);
			navigationPanel.addButton(btnBackToCSAndAC);
		}
		navigationPanel.addButton(btnConfirm);
		
		txtRepossessionFee = new TextField(I18N.message("repossession.fee"));
		txtRepossessionFee.setWidth(200, Unit.PIXELS);
		txtCollectionFee = new TextField(I18N.message("collection.fee"));
		txtCollectionFee.setWidth(200, Unit.PIXELS);
		txtAssetEstimationPrice1 = new TextField(I18N.message("motorcycle.estimation.price.1"));
		txtAssetEstimationPrice1.setWidth(200, Unit.PIXELS);
		txtAssetEstimationPrice2 = new TextField(I18N.message("motorcycle.estimation.price.2"));
		txtAssetEstimationPrice2.setWidth(200, Unit.PIXELS);
		txtAssetEstimationPrice3 = new TextField(I18N.message("motorcycle.estimation.price.3"));
		txtAssetEstimationPrice3.setWidth(200, Unit.PIXELS);
		txtMileage = new NumberField(I18N.message("mileage"));
		txtMileage.setWidth(200, Unit.PIXELS);
		
		cbxExteriorStatus = new EntityRefComboBox<AssetExteriorStatus>(I18N.message("exterior.status"));
		cbxExteriorStatus.setRestrictions(new BaseRestrictions<AssetExteriorStatus>(AssetExteriorStatus.class));
		cbxExteriorStatus.setWidth(200, Unit.PIXELS);
		cbxExteriorStatus.renderer();
		
		cbxEngineStatus = new EntityRefComboBox<EEngineStatus>(I18N.message("engine.status"), EEngineStatus.values());
		cbxEngineStatus.setWidth(200, Unit.PIXELS);

		cbxPartsStatus = new EntityRefComboBox<AssetPartsStatus>(I18N.message("parts.status"));
		cbxPartsStatus.setRestrictions(new BaseRestrictions<AssetPartsStatus>(AssetPartsStatus.class));
		cbxPartsStatus.setWidth(200, Unit.PIXELS);
		cbxPartsStatus.renderer();
		
		cbxRegistrationStatus = new EntityRefComboBox<AssetRegistrationStatus>(I18N.message("registration.status"));
		cbxRegistrationStatus.setRestrictions(new BaseRestrictions<AssetRegistrationStatus>(AssetRegistrationStatus.class));
		cbxRegistrationStatus.setWidth(200, Unit.PIXELS);
		cbxRegistrationStatus.renderer();
		
		cbxPlateNumber = new EntityRefComboBox<AssetPlateNumber>(I18N.message("plate.number"));
		cbxPlateNumber.setRestrictions(new BaseRestrictions<AssetPlateNumber>(AssetPlateNumber.class));
		cbxPlateNumber.setWidth(200, Unit.PIXELS);
		cbxPlateNumber.renderer();
		cbxStockStatus = new ERefDataComboBox<EStockStatus>(I18N.message("collection.stock.status"), EStockStatus.class);
		
		FormLayout formLayout = new FormLayout();
		formLayout.addComponent(txtRepossessionFee);
		formLayout.addComponent(txtCollectionFee);
		formLayout.addComponent(txtAssetEstimationPrice1);
		formLayout.addComponent(txtAssetEstimationPrice2);
		formLayout.addComponent(txtAssetEstimationPrice3);
		formLayout.addComponent(txtMileage);
		
		formLayout.addComponent(cbxExteriorStatus);
		formLayout.addComponent(cbxEngineStatus);
		formLayout.addComponent(cbxPartsStatus);
		formLayout.addComponent(cbxRegistrationStatus);
		formLayout.addComponent(cbxPlateNumber);
		formLayout.addComponent(cbxStockStatus);
		
		return formLayout;
	}
	
	/**
	 * 
	 * @param id
	 */
	public void assignValues (Long id) {
		reset();
		this.contract = ENTITY_SRV.getById(Contract.class, id);
		if (contract.getContractAuctionData() != null) {
			ContractAuctionData contractAuctionData = contract.getContractAuctionData();
			
			txtRepossessionFee.setValue(AmountUtils.format(contractAuctionData.getRepossessionFeeUsd()));
			txtCollectionFee.setValue(AmountUtils.format(contractAuctionData.getCollectionFeeUsd()));
			txtAssetEstimationPrice1.setValue(AmountUtils.format(contractAuctionData.getTiAssetEstimationPrice1()));
			txtAssetEstimationPrice2.setValue(AmountUtils.format(contractAuctionData.getTiAssetEstimationPrice2()));
			txtAssetEstimationPrice3.setValue(AmountUtils.format(contractAuctionData.getTiAssetEstimationPrice3()));
			txtMileage.setValue(contractAuctionData.getMilleage() != null ? contractAuctionData.getMilleage().toString() : "");;
			
			cbxExteriorStatus.setSelectedEntity(contractAuctionData.getAssetExteriorStatus());
			cbxEngineStatus.setSelectedEntity(contractAuctionData.getAssetEnginStatus());
			cbxPartsStatus.setSelectedEntity(contractAuctionData.getAssetPartsStatus());
			cbxRegistrationStatus.setSelectedEntity(contractAuctionData.getAssetRegistrationStatus());
			cbxPlateNumber.setSelectedEntity(contractAuctionData.getAssetPlateNumber());
			cbxStockStatus.setSelectedEntity(contractAuctionData.getAssetStockStatus());
		}
	}

	/**
	 * @see AbstractFormPanel#getEntity()
	 */
	@Override
	protected ContractAuctionData getEntity() {
		ContractAuctionData contractAuctionData = contract.getContractAuctionData();
		if (contractAuctionData == null) {
			contractAuctionData = new ContractAuctionData();
			contractAuctionData.setContract(contract);
		}
		
		contractAuctionData.setRepossessionFeeUsd(getDataFromTextField(txtRepossessionFee.getValue()));
		contractAuctionData.setCollectionFeeUsd(getDataFromTextField(txtCollectionFee.getValue()));
		contractAuctionData.setTiAssetEstimationPrice1(getDataFromTextField(txtAssetEstimationPrice1.getValue()));
		contractAuctionData.setTiAssetEstimationPrice2(getDataFromTextField(txtAssetEstimationPrice2.getValue()));
		contractAuctionData.setTiAssetEstimationPrice3(getDataFromTextField(txtAssetEstimationPrice3.getValue()));
		
		contractAuctionData.setTeAssetEstimationPrice1(getDataFromTextField(txtAssetEstimationPrice1.getValue()));
		contractAuctionData.setTeAssetEstimationPrice2(getDataFromTextField(txtAssetEstimationPrice2.getValue()));
		contractAuctionData.setTeAssetEstimationPrice3(getDataFromTextField(txtAssetEstimationPrice3.getValue()));
		contractAuctionData.setMilleage(getInteger(txtMileage));
		
		contractAuctionData.setAssetExteriorStatus(cbxExteriorStatus.getSelectedEntity());
		contractAuctionData.setAssetEnginStatus(cbxEngineStatus.getSelectedEntity());
		contractAuctionData.setAssetPartsStatus(cbxPartsStatus.getSelectedEntity());
		contractAuctionData.setAssetRegistrationStatus(cbxRegistrationStatus.getSelectedEntity());
		contractAuctionData.setAssetPlateNumber(cbxPlateNumber.getSelectedEntity());
		contractAuctionData.setAssetStockStatus(cbxStockStatus.getSelectedEntity());
	
		return contractAuctionData;
	}
	
	/**
	 * @see AbstractFormPanel#saveEntity()
	 */
	@Override
	public void saveEntity() {
		
		if(contract != null){
			contract.setStockStatus(getEntity().getAssetStockStatus());
			ENTITY_SRV.update(contract);
		}

		ENTITY_SRV.saveOrUpdate(getEntity());
		contract = ENTITY_SRV.getById(Contract.class, contract.getId());
	}
	
	/**
	 * @see AbstractFormPanel#reset()
	 */
	@Override
	public void reset() {
		super.reset();
		//txtRepossessionFee.setValue("");
		//txtCollectionFee.setValue("");
		txtAssetEstimationPrice1.setValue("");
		txtAssetEstimationPrice2.setValue("");
		txtAssetEstimationPrice3.setValue("");
		txtMileage.setValue("");
		
		cbxExteriorStatus.setSelectedEntity(null);
		cbxEngineStatus.setSelectedEntity(null);
		cbxPartsStatus.setSelectedEntity(null);
		cbxRegistrationStatus.setSelectedEntity(null);
		cbxPlateNumber.setSelectedEntity(null);
		cbxStockStatus.setSelectedEntity(null);
		
		this.contract = null;
	}
	
	/**
	 * Validate all ContractAuctionData
	 * @return
	 */
	private boolean auctionValidate () {
		if (contract != null) {
			 ContractAuctionData auctionData = contract.getContractAuctionData();
			 if (auctionData == null) {
				 this.errors.add(I18N.message("field.required.1", new String[] { I18N.message("all.field") }));
			 } else {
				 checkDouble(auctionData.getRepossessionFeeUsd(), "repossession.fee");
				 checkDouble(auctionData.getCollectionFeeUsd(), "collection.fee");
				 checkDouble(auctionData.getTiAssetEstimationPrice1(), "motorcycle.estimation.price.1");
				 checkDouble(auctionData.getTiAssetEstimationPrice2(), "motorcycle.estimation.price.2");
				 checkDouble(auctionData.getTiAssetEstimationPrice3(), "motorcycle.estimation.price.3");
				 //checkInteger(auctionData.getMilleage(), "mileage");
				 
				 checkObject(auctionData.getAssetExteriorStatus(), "exterior.status");
				 //checkObject(auctionData.getAssetEnginStatus(), "engine.status");
				 checkObject(auctionData.getAssetPartsStatus(), "parts.status");
				 //checkObject(auctionData.getAssetRegistrationStatus(), "registration.status");
				 //checkObject(auctionData.getAssetPlateNumber(), "plate.number");
				 //checkObject(auctionData.getAssetStockStatus(), "collection.stock.status");
			 }
		 }
		
		return errors.isEmpty();
	}
	
	/**
	 * @see AbstractFormPanel#validate()
	 */
	@Override
	protected boolean validate() {
		super.reset();
		
		validateDouble(txtRepossessionFee.getValue().replaceAll(",", ""), "repossession.fee");
		validateDouble(txtCollectionFee.getValue().replaceAll(",", ""), "collection.fee");
		validateDouble(txtAssetEstimationPrice1.getValue().replaceAll(",", ""), "motorcycle.estimation.price.1");
		validateDouble(txtAssetEstimationPrice2.getValue().replaceAll(",", ""), "motorcycle.estimation.price.2");
		validateDouble(txtAssetEstimationPrice3.getValue().replaceAll(",", ""), "motorcycle.estimation.price.3");
		
		return errors.isEmpty();
	}
	
	/**
	 * Check whether the Double value is null or less than zero
	 * @param value
	 * @param messageKey
	 */
	private void checkDouble (Double value, String messageKey) {
		if (value == null || value < 0) {
			this.errors.add(I18N.message("field.required.1", new String[] { I18N.message(messageKey) }));
		}
	}
	
	/**
	 * Check whether the string is able to convert to Double value
	 * @param value
	 * @param messageKey
	 */
	private void validateDouble (String value, String messageKey) {
		if (value != null && StringUtils.isNotEmpty(value)) {
			try {
				Double.parseDouble(value);
			} catch (NumberFormatException e) {
				this.errors.add(I18N.message("field.invalid.format", new String[] { I18N.message(messageKey) }));
			}
		}
	}
	
	/**
	 * Check whether the Integer value is null or less than zero
	 * @param value
	 * @param messageKey
	 */
	private void checkInteger (Integer value, String messageKey) {
		if (value == null || value < 0) {
			this.errors.add(I18N.message("field.required.1", new String[] { I18N.message(messageKey) }));
		}
	}
	
	/**
	 * Check whether the Object value is null
	 * @param value
	 * @param messageKey
	 */
	private void checkObject (Object value, String messageKey) {
		if (value == null) {
			this.errors.add(I18N.message("field.required.1", new String[] { I18N.message(messageKey) }));
		}
	}
	
	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractControlPanel#getDouble(AbstractTextField)
	 */
	@Override
	public Double getDouble (AbstractTextField textField) {
		if (textField.getValue() == null) {
			return 0d;
		}
		return super.getDouble(textField);
	}

	/**
	 * @see Button.ClickListener#buttonClick(ClickEvent)
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		super.reset();
		Boolean validate = true;
		String statusName = "";
		
		if (event.getButton() == btnConfirm) {
			if (ProfileUtil.isCollectionSupervisor() || ProfileUtil.isAuctionController()) {
				statusName = I18N.message("manager.validation");
				auctionStatus = AuctionWkfStatus.VAL;
			}
			if (ProfileUtil.isManager()) {
				statusName = I18N.message("waiting.for.result");
				auctionStatus = AuctionWkfStatus.WRE;
			}
			validate = auctionValidate();
		}else if (event.getButton() == btnBackToCSAndAC) {
			statusName = I18N.message("evaluation");
			auctionStatus = AuctionWkfStatus.EVA;
		}
			
		if (validate == true) {
			ConfirmDialog confirmDialog = ConfirmDialog.show(
					UI.getCurrent(), 
					I18N.message("confirm"),
					I18N.message("confirm.auction", statusName),
					I18N.message("ok"),
					I18N.message("cancel"),
					this);
			confirmDialog.setWidth("400px");
			confirmDialog.setHeight("150px");
			confirmDialog.setImmediate(true);
		} else {
			displayErrors();
		}
	}

	/**
	 * @see ConfirmDialog.Listener#onClose(ConfirmDialog)
	 */
	@Override
	public void onClose(ConfirmDialog result) {
		if (result.isConfirmed()) {
			auctionService.changeAuctionStatus(contract, auctionStatus);
			panel.displayTablePanel();
		}
	}
	
	/**
	 * @param panel
	 */
	public void setMainPanel (AuctionsPanel panel) {
		this.panel = panel;
	}
	/**
	 * 
	 * @param data
	 * @return
	 */
	public Double getDataFromTextField(String data){
		if(data == null || StringUtils.isEmpty(data)){
			return 0d;
		}
		return Double.valueOf(data.replaceAll(",", "").toString());
	}

}
