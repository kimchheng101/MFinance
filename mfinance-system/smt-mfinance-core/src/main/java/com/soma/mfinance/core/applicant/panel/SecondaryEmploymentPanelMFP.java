package com.soma.mfinance.core.applicant.panel;

import com.soma.common.app.eref.EProductLineCode;
import com.soma.mfinance.core.applicant.model.Employment;
import com.soma.mfinance.core.applicant.panel.address.BusinessScaleIncomePanel;
import com.soma.mfinance.core.applicant.service.ValidateNumbers;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.system.AmountBigUtils;
import com.soma.mfinance.core.shared.system.AutoTextField;
import com.soma.mfinance.core.shared.system.WhoseJob;
import com.soma.ersys.core.hr.model.eref.EEmploymentIndustry;
import com.soma.ersys.core.hr.model.eref.EEmploymentStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractControlPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.mfinance.core.applicant.panel.address.BusinessScaleIncomePanel;
import com.soma.mfinance.core.applicant.service.ValidateNumbers;
import com.vaadin.event.FieldEvents;
import com.vaadin.ui.*;
import com.vaadin.ui.Notification.Type;
import org.apache.commons.lang.StringUtils;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Secondary employment
 *
 * @author kimsuor.seang
 */
public class SecondaryEmploymentPanelMFP extends AbstractControlPanel {

    private static final long serialVersionUID = 2542460101873663919L;

    private TextField txtPosition;
    private TextField txtNoMonthInYear;
    private TextField txtRevenue;
    private TextField txtTimeWithEmployerInYear;
    private TextField txtTimeWithEmployerInMonth;
    private TextField txtBusinessExpense;
    private ERefDataComboBox<EEmploymentStatus> cbxEmploymentStatus;
    private ERefDataComboBox<EEmploymentIndustry> cbxEmploymentIndustry;
//    private DataRefComboBox<BusinessTypes> cbxBusinessTypes;
    private EntityRefComboBox<WhoseJob> cbxWhoseJob;
    private BusinessScaleIncomePanel businessScaleIncomePanel;
    private AutoTextField txtNetIncome;
    private CustomLayout customLayout = null;
    private Quotation quotation;
    private ArrayList validateSecond;
    private ArrayList validateNoDot;
    private ValidateNumbers secondEmploy;

    public SecondaryEmploymentPanelMFP() {
        this("secondaryEmployment");
    }

    public SecondaryEmploymentPanelMFP(String template) {
        secondEmploy = new ValidateNumbers();
        validateSecond = new ArrayList();
        validateNoDot = new ArrayList();

//        cbxBusinessTypes = new DataRefComboBox<BusinessTypes>(entityService.list(BusinessTypes.class));
        cbxWhoseJob = new EntityRefComboBox<WhoseJob>("");
        cbxWhoseJob.setRestrictions(new BaseRestrictions<>(WhoseJob.class));
        cbxWhoseJob.renderer();

        txtPosition = ComponentFactory.getTextField(false, 150, 250);
        txtTimeWithEmployerInYear = ComponentFactory.getTextField(false, 20, 50);
        txtTimeWithEmployerInMonth = ComponentFactory.getTextField(false, 20, 50);
        txtRevenue = new AutoTextField();
        txtRevenue.setWidth("150");
        txtRevenue.setImmediate(true);
        txtRevenue.addBlurListener(new FieldEvents.BlurListener() {
            private static final long serialVersionUID = 8401761385543992446L;

            @Override
            public void blur(FieldEvents.BlurEvent event) {
                //autoCalculateNetIncome();

            }
        });
        txtNoMonthInYear = ComponentFactory.getTextField(false, 50, 150);
        txtBusinessExpense = new AutoTextField();
        txtBusinessExpense.setWidth("150");
        txtBusinessExpense.setEnabled(true);
        txtBusinessExpense.setValue(AmountBigUtils.format(0d));
        txtBusinessExpense.setImmediate(true);
        txtBusinessExpense.addBlurListener(new FieldEvents.BlurListener() {
            private static final long serialVersionUID = 8763877374593000842L;

            @Override
            public void blur(FieldEvents.BlurEvent event) {
                //autoCalculateNetIncome();

            }
        });

        txtPosition = ComponentFactory.getTextField(false, 150, 250);
        txtRevenue = ComponentFactory.getTextField(false, 50, 150);
        validateSecond.add(txtRevenue);
        txtNoMonthInYear = ComponentFactory.getTextField(false, 50, 150);
        validateNoDot.add(txtNoMonthInYear);
        txtBusinessExpense = ComponentFactory.getTextField(false, 50, 150);
        validateSecond.add(txtBusinessExpense);

        cbxEmploymentStatus = new ERefDataComboBox<EEmploymentStatus>(EEmploymentStatus.class);
        cbxEmploymentIndustry = new ERefDataComboBox<EEmploymentIndustry>(EEmploymentIndustry.class);
        InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");

        try {
            customLayout = new CustomLayout(layoutFile);
        } catch (IOException e) {
            Notification.show("Could not locate template " + template, e.getMessage(), Type.ERROR_MESSAGE);
        }
        init();

    }
    public void init(){
        customLayout.addComponent(new Label(I18N.message("per.month")), "lblPerMonth");
        customLayout.addComponent(new Label(I18N.message("per.12")), "lblPer12");
        customLayout.addComponent(new Label(I18N.message("employment.industry")), "lblEmploymentIndustry");
        customLayout.addComponent(cbxEmploymentIndustry, "cbxEmploymentIndustry");
        customLayout.addComponent(new Label(I18N.message("position")), "lblPosition");
        customLayout.addComponent(txtPosition, "txtPosition");
        customLayout.addComponent(new Label(I18N.message("no.month.in.year")), "lblNoMonthInYear");
        customLayout.addComponent(txtNoMonthInYear, "txtNoMonthInYear");
        customLayout.addComponent(new Label(I18N.message("revenue")), "lblRevenue");
        customLayout.addComponent(txtRevenue, "txtRevenue");
        customLayout.addComponent(new Label(I18N.message("business.expense")), "lblBusinessExpense");
        customLayout.addComponent(txtBusinessExpense, "txtBusinessExpense");
        customLayout.addComponent(new Label(I18N.message("employment.status")), "lblEmploymentStatus");
        customLayout.addComponent(cbxEmploymentStatus, "cbxEmploymentStatus");
        customLayout.addComponent(new Label(I18N.message("whose.job")), "lblWhoseJob");
        customLayout.addComponent(cbxWhoseJob, "cbxWhoseJob");

        businessScaleIncomePanel = new BusinessScaleIncomePanel(true, "businessScaleIncomeNoRequriedField");

        txtNetIncome = businessScaleIncomePanel.getTxtNetIncome();
        txtNetIncome.setImmediate(true);
        txtNetIncome.addBlurListener(new FieldEvents.BlurListener() {
            private static final long serialVersionUID = -5023781933968810733L;
            @Override
            public void blur(FieldEvents.BlurEvent event) {
                //autoCalculateTextNetIncome();
            }
        });

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSpacing(false);
        verticalLayout.addComponent(customLayout);
        if (this.quotation  != null) {
            if (this.quotation .getAsset() != null) {
                if (this.quotation .getAsset().getAssetRange() != null) {
                    if (this.quotation .getAsset().getAssetRange().getAssetMake() != null) {
                        if (this.quotation .getAsset().getAssetRange().getAssetMake().getId().equals(EProductLineCode.MFP.getId())) {

                        } else if (this.quotation .getAsset().getAssetRange().getAssetMake().getId().equals(EProductLineCode.KFP.getId())) {
                            verticalLayout.addComponent(businessScaleIncomePanel);
                        }
                    }
                }
            }
        }

        secondEmploy.validateNumberDot(validateSecond);
        secondEmploy.validateNumber(validateNoDot);

        addComponent(verticalLayout);
    }

    private void autoCalculateNetIncome() {
        try {
            double revenue = AmountBigUtils.getValueDouble(txtRevenue);
            double businessExpense = AmountBigUtils.getValueDouble(txtBusinessExpense);
            txtNetIncome.setValue(AmountBigUtils.format(revenue - businessExpense));
            double netIncome = AmountBigUtils.getValueDouble(txtNetIncome);
            double total = revenue - netIncome;
            if(total < 0){
                total =-total;
            }
            txtBusinessExpense.setValue(AmountBigUtils.format(total));

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    private void autoCalculateTextNetIncome() {
        try {
            double revenue = AmountBigUtils.getValueDouble(txtRevenue);
            double netIncome = AmountBigUtils.getValueDouble(txtNetIncome);
            double total = revenue - netIncome;
            if(total < 0){
                total =-total;
            }
            txtBusinessExpense.setValue(AmountBigUtils.format(total));

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public void assignValues(Employment employment) {
        reset();
        this.removeAllComponents();
        init();
        txtPosition.setValue(getDefaultString(employment.getPosition()));
        txtRevenue.setValue(AmountBigUtils.format(employment.getRevenue()));
        txtTimeWithEmployerInYear.setValue(getDefaultString(employment.getTimeWithEmployerInYear()));
        txtTimeWithEmployerInMonth.setValue(getDefaultString(employment.getTimeWithEmployerInMonth()));
        txtBusinessExpense.setValue(AmountBigUtils.format(employment.getBusinessExpense()));
//        if(employment.getRevenue()!=null && employment.getNetIncome() != null){
//            txtBusinessExpense.setValue(AmountBigUtils.format(employment.getRevenue()-employment.getNetIncome()));}
        cbxEmploymentStatus.setSelectedEntity(employment.getEmploymentStatus());
        cbxEmploymentIndustry.setSelectedEntity(employment.getEmploymentIndustry());
//        cbxBusinessTypes.setSelectedEntity(employment.getBusinessTypes());
//        cbxWhoseJob.setSelectedEntity(employment.getWhoseJob());
        txtNoMonthInYear.setValue(getDefaultString(employment.getNoMonthInYear()));
        businessScaleIncomePanel.assignValues(employment);

    }

    public Employment getEmployment(Employment employment) {
        if (StringUtils.isNotEmpty(txtPosition.getValue())
                || StringUtils.isNotEmpty(txtRevenue.getValue())
                || StringUtils.isNotEmpty(txtBusinessExpense.getValue())
                || cbxEmploymentStatus.getSelectedEntity() != null) {
            employment.setPosition(txtPosition.getValue());
            employment.setRevenue(AmountBigUtils.getValueDouble(txtRevenue));
            employment.setTimeWithEmployerInYear(getInteger(txtTimeWithEmployerInYear));
            employment.setTimeWithEmployerInMonth(getInteger(txtTimeWithEmployerInMonth));
            employment.setBusinessExpense(AmountBigUtils.getValueDouble(txtBusinessExpense));
            employment.setEmploymentStatus(cbxEmploymentStatus.getSelectedEntity());
            employment.setEmploymentIndustry(cbxEmploymentIndustry.getSelectedEntity());
//            employment.setBusinessTypes(cbxBusinessTypes.getSelectedEntity());
//            employment.setWhoseJob(cbxWhoseJob.getSelectedEntity());
//            employment.setEmploymentType(isCoApplicant? EmploymentType.COSE : EmploymentType.SECO);
            employment.setNoMonthInYear(getInteger(txtNoMonthInYear));

            businessScaleIncomePanel.getBusinessScaleIncome(employment);
            return employment;
        }
        return null;
    }

    public void reset() {
        txtPosition.setValue("");
        txtRevenue.setValue("");
        txtTimeWithEmployerInYear.setValue("");
        txtTimeWithEmployerInMonth.setValue("");
        txtBusinessExpense.setValue("");
        txtBusinessExpense.setValue(null);
        cbxEmploymentStatus.setSelectedEntity(null);
        cbxEmploymentIndustry.setSelectedEntity(null);
//        cbxBusinessTypes.setSelectedEntity(null);
        cbxWhoseJob.setSelectedEntity(null);
        txtNoMonthInYear.setValue("");
        businessScaleIncomePanel.reset();
    }

    public void setSecondaryEmploymentMFP(Quotation quotation){
        this.quotation = quotation;
    }
}
