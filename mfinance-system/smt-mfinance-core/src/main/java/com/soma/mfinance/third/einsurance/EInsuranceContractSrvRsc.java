package com.soma.mfinance.third.einsurance;

import com.soma.common.messaging.ws.resource.cfg.refdata.AbstractRefDataSrvRsc;
import com.soma.mfinance.core.helper.FinServicesHelper;
import org.seuksa.frmk.tools.DateUtils;

import java.util.Date;


/**
 * Created by Kimsuor SEANG
 * Date  : 10/18/2017
 * Name  : 10:05 AM
 * Email : k.seang@gl-f.com
 */
public class EInsuranceContractSrvRsc  extends AbstractRefDataSrvRsc implements FinServicesHelper{


    private static final String CPMI_START_DATE = "01-07-2015";


    /**
     *
     * @param insuranceStartDate
     * @param contractStartDate
     * @param term
     * @return
     */
    private int getStartTermInYear(Date insuranceStartDate, Date contractStartDate, Integer term) {
        int termInYear = term / 12;
        int usedTermInYear = getUsedTermInYear(insuranceStartDate, contractStartDate, termInYear);
        return usedTermInYear + 1;
    }

    public int getUsedTermInYear(Date calculDate, Date insuranceStartDate, Integer termInYear) {
        int usedTermInYear = 0;
        Date beginingCalculDate = DateUtils.getDateAtBeginningOfDay(calculDate);
        Date beginingInsuranceStartDate = DateUtils.getDateAtBeginningOfDay(insuranceStartDate);
        for (int i = 0; i < termInYear; i++) {
            Date d = DateUtils.addYearsDate(beginingInsuranceStartDate, i);
            if (DateUtils.isBeforeDay(beginingCalculDate, d)
                    || DateUtils.isSameDay(beginingCalculDate, d)) {
                break;
            }
            usedTermInYear++;
        }
        return usedTermInYear;
    }

    /**
     * Get Insurance Start Date
     *
     * @param contractStartDate
     * @param term
     * @return
     */
    private Date getInsuranceStartDate(Date contractStartDate, Integer term) {
        Date insuranceStartDate = null;
        int termInYear = term / 12;
        Date cpmiStartDate = DateUtils.getDate(CPMI_START_DATE, DateUtils.FORMAT_DDMMYYYY_MINUS);
        int usedTermInYear = getUsedTermInYear(cpmiStartDate, contractStartDate, termInYear);
        int remainingTermInYear = termInYear - usedTermInYear;
        if (remainingTermInYear > 0) {
            insuranceStartDate = DateUtils.addYearsDate(contractStartDate, usedTermInYear);
        }
        return insuranceStartDate;
    }

/*

    public QuotationVO toEInsuranceDTO(Contract contract, Quotation quotation) {
        FinProduct  finProduct = quotation.getFinancialProduct();
        Assert.notNull(finProduct,"Financial product could not be null");

        QuotationVO quotationVO = new QuotationVO();


        quotationVO.setReference(String.valueOf(quotation.getId()));
        quotationVO.setExternalLid(contract.getReference());
        quotationVO.setMigrationDate(DateUtils.todayDate());
        if(finProduct != null) {
            quotationVO.setProductCode(finProduct.getCode());
        } else {
            quotationVO.setProductCode("");
        }

        quotationVO.setTerm(contract.getTerm());
        Date insuranceStartDate = getInsuranceStartDate(contract.getStartDate(), contract.getTerm());
        int startTermInYear = getStartTermInYear(insuranceStartDate, contract.getStartDate(), quotation.getTerm());
        quotationVO.setStartTermInYear(startTermInYear);
        quotationVO.setAsset(toEInsuranceAssetDTO(contract));

        //quotationVO.setDealerInternalCode("");
        quotationVO.setDealerInternalCode(contract.getDealer().getInternalCode());
        quotationVO.setQuotationDate(contract.getQuotationDate());

        if (quotation.getPlaceInstallment() != null) {
            // In E-Finance, PlaceInstallment has no code property. Then use id instead of Code
            quotationVO.setPlaceInstallmentCode(quotation.getPlaceInstallment().getId().toString());
        }
        if (quotation.getWayOfKnowing() != null) {
            // In E-Finance, Some WayOfKnowing code is null. Then Then use id instead of Code
            String wayOfKnowing = quotation.getWayOfKnowing().getCode();
            wayOfKnowing = wayOfKnowing != null ? wayOfKnowing : quotation.getWayOfKnowing().getId().toString();
            quotationVO.setWayOfKnowingCode(wayOfKnowing);
        }
        quotationVO.setStamp(true);
        quotationVO.setInsuranceStartDate(quotation.getInsuranceStartDate());
        quotationVO.setValid(true);
        quotationVO.setComments(toEInsuranceCommentVO(quotation.getId()));
        quotationVO.setQuotationApplicants(toEInsuranceQuotationApplicantVO(quotation.getQuotationApplicant(EApplicantType.C)));
        quotationVO.setQuotationDocuments(toEInsuranceQuotationDocumentVO(quotation.getQuotationDocuments()));

        return quotationVO;
    }

    private AssetVO toEInsuranceAssetDTO(Contract contract) {
        Asset asset = contract.getAsset();
        Assert.notNull(asset, "Asset could be not .. !");
        AssetVO assetVO = new AssetVO();

        AssetModel assetModel = asset.getModel();
        if (assetModel != null) {
            assetVO.setAssetModelCode(assetModel.getAssetCode());
        }
        if (asset.getColor() != null) {
            assetVO.setAssetColorCode(asset.getColor().getCode());
        }
        if (asset.getEngine() != null) {
            assetVO.setAssetEngineCode(asset.getEngine().getCode());
        }
        if (asset.getAssetGender() != null) {
            assetVO.setAssetGender(asset.getAssetGender().getCode());
        }

        assetVO.setTiCashPriceUsd(asset.getTiAssetApprPrice());
        assetVO.setTeCashPriceUsd(asset.getTeAssetApprPrice());
        assetVO.setVatCashPriceUsd(asset.getVatValue());
        assetVO.setYear(asset.getModel().getYear());
        assetVO.setRegistrationDate(asset.getRegistrationDate());
        assetVO.setPlateNumber(asset.getPlateNumber());
        assetVO.setChassisNumber(asset.getChassisNumber());
        assetVO.setEngineNumber(asset.getEngineNumber());
        assetVO.setCode(asset.getCode());
        assetVO.setDesc(asset.getModel().getDesc());
        assetVO.setDescEn(asset.getModel().getDescEn());

        return assetVO;
    }

    private List<CommentVO> toEInsuranceCommentVO(long quotationId) {
        BaseRestrictions<Comment> restrictions = new BaseRestrictions<>(Comment.class);
        restrictions.addCriterion(Restrictions.eq("quotation.id", quotationId));
        List<Comment> comments = ENTITY_SRV.list(restrictions);
        List<CommentVO> commentVOS = new ArrayList<>();

        comments.forEach(comment -> {
            CommentVO commentVO = new CommentVO();
            commentVO.setComment(comment.getDesc());
            commentVO.setOnlyForUW(comment.isOnlyForUW());
            commentVO.setForManager(comment.isForManager());
            commentVO.setUserCode(comment.getUser().getUsername());
            commentVO.setDocumentUwGroupCode(comment.getDocumentUwGroup() != null ? comment.getDocumentUwGroup().getCode() : "");
            commentVOS.add(commentVO);
        });

        return commentVOS;
    }

    private List<QuotationApplicantVO> toEInsuranceQuotationApplicantVO(QuotationApplicant quotationApplicant) {
        List<QuotationApplicantVO> quotationApplicantVOS = new ArrayList<>();
        QuotationApplicantVO quotationApplicantVO = new QuotationApplicantVO();
        quotationApplicantVO.setApplicant(toEInsuranceApplicantVo(quotationApplicant.getApplicant()));
        quotationApplicantVO.setApplicantType(quotationApplicant.getApplicantType().getCode());
        quotationApplicantVO.setRelationshipCode(quotationApplicant.getRelationship() != null ? quotationApplicant.getRelationship().getCode() : "CC");
        quotationApplicantVO.setSameApplicantAddress(quotationApplicant.isSameApplicantAddress());
        quotationApplicantVO.setCrudAction(quotationApplicant.getCrudAction() != null ? quotationApplicant.getCrudAction().toString() : "");
        quotationApplicantVOS.add(quotationApplicantVO);
        return quotationApplicantVOS;
    }

    private ApplicantVO toEInsuranceApplicantVo(Applicant applicant) {
        ApplicantVO applicantVO = new ApplicantVO();

        applicantVO.setLastName(applicant.getLastName());
        applicantVO.setLastNameEn(applicant.getLastNameEn());
        applicantVO.setFirstName(applicant.getFirstName());
        applicantVO.setFirstNameEn(applicant.getFirstNameEn());
        applicantVO.setNickName(applicant.getIndividual().getNickName());
        applicantVO.setCivility(applicant.getIndividual().getCivility().getCode());
        applicantVO.setBirthDate(applicant.getIndividual().getBirthDate());
        applicantVO.setGender(applicant.getIndividual().getGender().getCode());
        applicantVO.setMaritalStatus(applicant.getIndividual().getMaritalStatus().getCode());
        applicantVO.setNationalityCode(applicant.getIndividual().getNationality().getCode());
        applicantVO.setHomePhone("");  // b.suor said ""
        applicantVO.setMobilePhone(applicant.getIndividual().getMobilePerso());
        applicantVO.setMobilePhone2(applicant.getIndividual().getMobilePerso2());
        applicantVO.setEmail(applicant.getIndividual().getEmailPerso());
        applicantVO.setPlaceOfBirthCode(applicant.getIndividual().getPlaceOfBirth().getCode());
//        applicantVO.setBoReference(applicant.getContract().getId());
        applicantVO.setFirstNameEnSpouse(applicant.getIndividual().getIndividualSpouse().getFirstName());
        applicantVO.setLastNameEnSpouse(applicant.getIndividual().getIndividualSpouse().getLastName());
        applicantVO.setMobilePhoneSpouse(applicant.getIndividual().getIndividualSpouse().getMobilePhone());
        applicantVO.setNumberOfChildren(applicant.getIndividual().getNumberOfChildren());
        applicantVO.setApplicantAddresses(toEInsuranceApplicantAddressVo(applicant.getIndividual().getIndividualAddresses()));
        applicantVO.setEmployments(toEInsuranceEmployments(applicant.getIndividual().getEmployments()));
        return applicantVO;
    }

    private List<ApplicantAddressVO> toEInsuranceApplicantAddressVo(List<IndividualAddress> addresses) {
        List<ApplicantAddressVO> applicantAddressVOS = new ArrayList<>();
        addresses.forEach(address -> {
            ApplicantAddressVO applicantAddressVO = new ApplicantAddressVO();
            applicantAddressVO.setAddress(toEInsuranceAddressVo(address.getAddress()));
            applicantAddressVO.setAddressType(address.getIndividual().getMainAddress().getType().getCode());
            applicantAddressVOS.add(applicantAddressVO);
        });
        return applicantAddressVOS;
    }

    private AddressVO toEInsuranceAddressVo(Address address) {
        AddressVO addressVO = null;
        if (address != null) {
            addressVO = new AddressVO();
            addressVO.setHouseNo(address.getHouseNo());
            addressVO.setStreet(address.getStreet());
            addressVO.setLine1(address.getLine1());
            addressVO.setLine2(address.getLine2());
            addressVO.setLine3(address.getLine3());
            addressVO.setTimeAtAddressInYear(address.getTimeAtAddressInYear());
            addressVO.setTimeAtAddressInMonth(address.getTimeAtAddressInMonth());
            addressVO.setCountryCode(address.getCountry().getCode());
            addressVO.setProvinceCode(address.getProvince().getCode());
            addressVO.setLatitude(address.getLatitude());
            addressVO.setLongitude(address.getLongitude());
        }

        return addressVO;
    }

    private List<EmploymentVO> toEInsuranceEmployments(List<Employment> employments) {
        Assert.notNull(employments, "Employment could not be null");
        List<EmploymentVO> employmentVOS = new ArrayList<>();
        employments.forEach(employment -> {
            System.out.println("Employement Id : " + employment.getId());
            EmploymentVO employmentVO = new EmploymentVO();
            employmentVO.setPosition(employment.getEmploymentPosition() != null ? employment.getEmploymentPosition().getDescEn() : "");
            employmentVO.setEmployerName(employment.getEmployerName() != null ? employment.getEmployerName() : "");
            employmentVO.setTimeWithEmployerInYear(employment.getTimeWithEmployerInYear());
            employmentVO.setTimeWithEmployerInMonth(employment.getTimeWithEmployerInMonth());
            employmentVO.setRevenue(employment.getRevenue());
            employmentVO.setAllowance(employment.getAllowance());
            employmentVO.setBusinessExpense(employment.getBusinessExpense());
            employmentVO.setNoMonthInYear(employment.getNoMonthInYear());
            employmentVO.setEmploymentStatusCode(employment.getEmploymentStatus().getCode());
            employmentVO.setEmploymentIndustryCode(employment.getEmploymentIndustry().getCode());
            employmentVO.setEmploymentType(employment.getEmploymentType().getCode());
            employmentVO.setAllowCallToWorkPlace(employment.isAllowCallToWorkPlace());
            employmentVO.setSameApplicantAddress(employment.isSameApplicantAddress());
            employmentVO.setWorkPhone(employment.getWorkPhone());
            employmentVO.setAddress(toEInsuranceAddressVo1(employment.getAddress()));
            employmentVO.setSeniorityLevelCode(employment.getSeniorityLevel() != null ? employment.getSeniorityLevel().getCode() : "");
            employmentVOS.add(employmentVO);
        });

        return employmentVOS;
    }

    private AddressVO toEInsuranceAddressVo1(Address address) {
        AddressVO addressVO = null;
        if (address != null) {
            addressVO = new AddressVO();
            addressVO.setHouseNo(address.getHouseNo());
            addressVO.setStreet(address.getStreet());
            addressVO.setLine1(address.getLine1());
            addressVO.setLine2(address.getLine2());
            addressVO.setLine3(address.getLine3());
            addressVO.setTimeAtAddressInYear(address.getTimeAtAddressInYear());
            addressVO.setTimeAtAddressInMonth(address.getTimeAtAddressInMonth());
            addressVO.setCountryCode(address.getCountry().getCode());
            addressVO.setProvinceCode(address.getProvince().getCode());
            addressVO.setDistrictCode(address.getDistrict().getCode());
            addressVO.setCommuneCode(address.getCommune().getCode());
            addressVO.setVillageCode(address.getVillage().getCode());
            addressVO.setLatitude(address.getLatitude());
            addressVO.setLongitude(address.getLongitude());
            addressVO.setPropertyAddressCode(address.getProperty() != null ? address.getProperty().getCode() : "");
        }

        return addressVO;
    }

    private List<QuotationDocumentVO> toEInsuranceQuotationDocumentVO(List<QuotationDocument> quotationDocuments) {
        List<QuotationDocumentVO> quotationDocumentVOS = new ArrayList<>();
        quotationDocuments.forEach(quotationDocument -> {
            QuotationDocumentVO quotationDocumentVO = new QuotationDocumentVO();
            quotationDocumentVO.setDocumentCode(quotationDocument.getDocument().getCode());
            quotationDocumentVO.setPath(quotationDocument.getPath());
            quotationDocumentVO.setReference(quotationDocument.getReference());
            quotationDocumentVO.setIssueDate(quotationDocument.getIssueDate());
            quotationDocumentVO.setExpireDate(quotationDocument.getExpireDate());
            quotationDocumentVO.setOriginal(quotationDocument.isOriginal());
            quotationDocumentVO.setCovConfirmationCo(quotationDocument.isCovConfirmationCo());
            quotationDocumentVO.setCovConfirmationUw(quotationDocument.isCovConfirmationUw());
            quotationDocumentVO.setEmployerConfirmationCo(quotationDocument.isEmployerConfirmationCo());
            quotationDocumentVO.setEmployerConfirmationUw(quotationDocument.isEmployerConfirmationUw());
            quotationDocumentVO.setCrudAction(quotationDocument.getCrudAction() != null ? quotationDocument.getCrudAction().toString() : "");
            quotationDocumentVOS.add(quotationDocumentVO);
        });

        return quotationDocumentVOS;
    }*/

}
