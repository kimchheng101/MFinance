package com.soma.mfinance.core.financial.model;

import org.seuksa.frmk.model.entity.MEntityRefA;

/**
 * Meta data of com.soma.mfinance.core.financial.model.CampaignAssetMake
 * @author kimsuor.seang
 */
public interface MCampaign extends MEntityRefA {

	public final static String STARTDATE = "startDate";
	public final static String ENDDATE = "endDate";
	public final static String TERMS = "terms";
	public final static String DEALERS = "dealers";
	public final static String VALIDFORALLDEALERS = "validForAllDealers";
	
}
