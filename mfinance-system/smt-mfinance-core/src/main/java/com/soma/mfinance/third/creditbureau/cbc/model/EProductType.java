package com.soma.mfinance.third.creditbureau.cbc.model;

import org.seuksa.frmk.model.eref.BaseERefData;

import javax.persistence.AttributeConverter;
import java.util.List;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/11/2017
 * Name  : 4:47 PM
 * Email : k.seang@gl-f.com
 */
public class EProductType extends BaseERefData implements AttributeConverter<EProductType, Long> {



    public final static EProductType MTL = new EProductType("MTL", 1);
    public final static EProductType TRA = new EProductType("TRA", 2);
    public final static EProductType WTR = new EProductType("WTR", 3);
    public final static EProductType HAR = new EProductType("HAR", 4);

    /**
     *
     */
    public EProductType() {
    }

    /**
     * @param code
     * @param id
     */
    public EProductType(String code, long id) {
        super(code, id);
    }

    public EProductType convertToEntityAttribute(Long id) {
        return super.convertToEntityAttribute(id);
    }

    @Override
    public Long convertToDatabaseColumn(EProductType arg0) {
        return super.convertToDatabaseColumn(arg0);
    }

    /**
     *
     * @return
     */
    public static List<EProductType> values() {
        return getValues(EProductType.class);
    }

    /**
     *
     * @param code
     * @return
     */
    public static EProductType getByCode(String code) {
        return getByCode(EProductType.class, code);
    }

    /**
     *
     * @param id
     * @return
     */
    public static EProductType getById(long id) {
        return getById(EProductType.class, id);
    }
}
