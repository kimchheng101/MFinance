/**
 * 
 */
package com.soma.mfinance.third.creditbureau.cbc.model.response;

import java.io.Serializable;

/**
 * @author tha.bunsath
 *
 */
public class Totwoamt implements Serializable {

	private static final long serialVersionUID = -3575582283720846890L;
	private String tot_wo_curr;
	private Double tot_wo;
	public String getTot_wo_curr() {
		return tot_wo_curr;
	}
	public void setTot_wo_curr(String tot_wo_curr) {
		this.tot_wo_curr = tot_wo_curr;
	}
	public Double getTot_wo() {
		return tot_wo;
	}
	public void setTot_wo(Double tot_wo) {
		this.tot_wo = tot_wo;
	}


}
