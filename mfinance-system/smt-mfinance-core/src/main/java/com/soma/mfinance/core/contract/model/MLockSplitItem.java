package com.soma.mfinance.core.contract.model;

import org.seuksa.frmk.model.entity.MEntityA;

/**
 * Meta data of com.soma.mfinance.core.contract_old.model.LockSplitItem
 * @author kimsuor.seang
 */
public interface MLockSplitItem extends MEntityA {

	public final static String LOCKSPLIT = "lockSplit";
	public final static String JOURNALEVENT = "journalEvent";
	public final static String TIAMOUNT = "tiAmount";
	public final static String VATAMOUNT = "vatAmount";
	public final static String PRIORITY = "priority";
	public final static String STATUS = "wkfStatus";

}
