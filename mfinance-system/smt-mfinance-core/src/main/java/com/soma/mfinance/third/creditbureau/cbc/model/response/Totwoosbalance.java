/**
 * 
 */
package com.soma.mfinance.third.creditbureau.cbc.model.response;

import java.io.Serializable;

/**
 * @author tha.bunsath
 *
 */
public class Totwoosbalance implements Serializable {

	private static final long serialVersionUID = -3575582283720846890L;
	private String tot_wo_os_curr;
	private Double tot_wo_os_bal;
	public String getTot_wo_os_curr() {
		return tot_wo_os_curr;
	}
	public void setTot_wo_os_curr(String tot_wo_os_curr) {
		this.tot_wo_os_curr = tot_wo_os_curr;
	}
	public Double getTot_wo_os_bal() {
		return tot_wo_os_bal;
	}
	public void setTot_wo_os_bal(Double tot_wo_os_bal) {
		this.tot_wo_os_bal = tot_wo_os_bal;
	}

}
