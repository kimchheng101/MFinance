package com.soma.mfinance.core.Enum;

import org.seuksa.frmk.model.eref.BaseERefData;

import javax.persistence.AttributeConverter;
import java.util.List;

/**
 * Created by b.chea on 2/25/2017.
 */
public class EEmployeeOccupation extends BaseERefData implements AttributeConverter<EEmployeeOccupation, Long> {

    public EEmployeeOccupation(){}

    public EEmployeeOccupation(String code, long id){ super(code, id);}

    @Override
    public Long convertToDatabaseColumn(EEmployeeOccupation arg0) {
        return super.convertToDatabaseColumn(arg0);
    }

    @Override
    public EEmployeeOccupation convertToEntityAttribute(Long dbData) {
        return (EEmployeeOccupation) super.convertToEntityAttribute(dbData);
    }


    public static List<EEmployeeOccupation> values() {
        return getValues(EEmployeeOccupation.class);
    }

    public static EEmployeeOccupation getByCode(String code) {
        return (EEmployeeOccupation) getByCode(EEmployeeOccupation.class, code);
    }

    public static EEmployeeOccupation getById(long id) {
        return (EEmployeeOccupation) getById(EEmployeeOccupation.class, Long.valueOf(id));
    }
}
