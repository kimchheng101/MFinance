package com.soma.mfinance.glf.report.xls;

/**
 * Created by v.sai on 5/5/2017.
 */
public interface LeasingContractField extends BaseContractFields {

    //    detail 1 lessee info
    String SHOP_NAME = "shopName";
    String LESSEE_NAME = "lesseeName";
    String LESSEE_SEX = "lesseeSex";
    String LESSEE_AGE = "lesseeAge";
    String LESSEE_BD = "lesseebd";
    String IS_LESSEEID_CARD = "isLesseeIdCard";
    String LESSEE_ID_CARD = "lesseeIdCard";
    String ISLESSEE_FAMILYBOOKNUM = "isLesseeFamilyBookNum";
    String LESSEE_FAMILYBOOKNUM = "lesseeFamilyBookNum";
    String ISLESSEERESIDENT_BOOKNUM = "isLesseeResidentBookNum";
    String LESSEERESIDENT_BOOKNUM = "lesseeResidentBookNum";
    String ISLESSEE_BIRTHCERTIFICATENUM = "isLesseeBirthCertificateNum";
    String LESSEE_BIRTHCERTIFICATENUM = "lesseeBirthCertificateNum";
    String ISLESSEE_CERTIFYLETTERNUM = "isLesseeCertifyLetterNum";
    String LESSEE_INCOME = "lesseeIncome";
    String ISLESSEE_SINGLE = "isLesseeSingle";
    String ISLESSEE_SPOUSE = "isLesseeSpouse";
    String lesseeFamilyBookNumISLESSEE_SPOUSE = "isLesseeSpouse";
    String ISLESSEE_DIVORCE = "isLesseeDivorce";
    String LESSEE_NUM_CHILDREN = "lesseeNumChildren";
    String LESSEE_SPOUSE_NAME = "lesseeSpouseName";
    String LESSEE_SPOUSE_PHONE_NUM = "lesseeSpousePhoneNum";
    String LESSEE_CERTIFYLETTERNUM = "lesseeCertifyLetterNum";
    String LESSEE_PHONENUM1 = "lesseePhoneNum1";
    String LESSEE_PHONENUM2 = "lesseePhoneNum2";


    //detail 2 lessee current address
    String LESSEE_HOUSE_NUM = "lesseeHouseNum";
    String LESSEE_STREET_NUM = "lesseeStreetNum";
    String LESSEE_VILLAGE = "lesseeVillage";
    String LESSEE_COMMUNE = "lesseeCommune";
    String LESSEE_DISTRICT = "lesseeDistrict";
    String LESSEE_PROVINCE = "lesseeProvince";
    String LESSEE_LENGTH_STAY = "lesseeLengthStay";
    String LESSEE_LENGTH_STAY_MONTH = "lesseeLengthStayMonth";
    String LESSEE_AVALABLEDATETIME = "lesseeAvalableDateTime";

    String LESSEE_TIMEATADDRESSINYEAR = "lesseeTimeAtAddressInYear";
    String LESSEE_TIMEATADDRESSINMONTH = "lesseeTimeAtAddressInMonth";
    String IS_LESSEE_OWNHOUSE = "isLesseeOwnHouse";
    String IS_LESSEE_PARENTHOUSE = "isLesseeParentsHouse";
    String IS_LESSEE_RELATIVEHOUSE = "isLesseeRelativeHouse";
    String IS_LESSEE_RENTINGHOUSE = "isLesseeRentHouse";
    String IS_LESSEE_OTHER = "isLesseeOtherHouse";

    // detail 3 job and workplace lessee
    String LESSEE_COMPANY_NAME = "lesseeCompanyName";
    String LESSEE_COMPANY_TYPE_BUSINESS = "lesseeCompanyTypeBusiness";
    String LESSEE_COMPANY_HOUSE_NUM = "lesseeCompanyHouseNum";
    String LESSEE_COMPANY_STREET_NUM = "lesseeCompanyStreetNum";
    String LESSEE_COMPANY_VILLAGE = "lesseeCompanyVillage";
    String LESSEE_COMPANY_COMMUNE = "lesseeCompanyCommune";
    String LESSEE_COMPANY_DISTRICT = "lesseeCompanyDistrict";
    String LESSEE_COMPANY_PROVINCE = "lesseeCompanyProvince";
    String LESSEE_COMPANY_PHONE_NUM = "lesseeCompanyPhoneNum";
    String LESSEE_JOB = "lesseeJob";
    String LESSEE_POSITION = "lesseePosition";

    //    detail 4
    String IS_CURRENT_ADDRESS = "isCurrentAddress";
    String IS_ID_ADDRESS = "isIdAddress";
    String IS_HOUSE_ADDRESS = "isHouseAddress";
    String IS_WORK_ADDRESS = "isWorkAddress";
    String IS_OTHER_ADDRESS = "isOtherAddress";

    //detail 5 contract rerent
    String AGREEMENT_NUM = "agreementNum";
    String AGREEMENT_DATE = "agreementDate";

    //detail 6 product rerent
    String LESSEE_ASSETPRODUCT = "lesseeAssetProduct";
    String VEHICLE_MODEL = "vehicleModel";
    String VEHICLE_COLOR = "vehicleColor";
    String VEHICLE_MANUFACTUREDATE = "vehicleManufactureDate";
    String VEHICLE_MARK = "vehicleMark";
    String LESSEE_VEHICLEGENDER = "lesseeVehicleGender";
    String VEHICLE_CC = "vehicleCc";
    String VEHICLE_ENGINENUM = "vehicleEngineNum";
    String VEHICLESERIENUM = "vehicleSerieNum";
    String ASSET_PRICE = "assetPrice";
    String PRINCIPLE_AMOUNT = "principalAmount";
    String FIRST_PAYMENT_PRICE = "firstPaymentPrice";
    String LESSEE_ADJUSTMENTAMOUNT = "adjustmentAmount";

    // detail 7 payment
    String LESSEE_TiINSTALLMENTAMOUNT = "tIInstallmentAmount";
    String MONTHLY_PAYMENT = "monthlyPayment";
    String TERM_PAYMENT = "termPayment";
    String ADVANCE_PAYMENT_PERCENTAGE = "advancePaymentPercentage";
    String EFFECTIVE_PAYMENT_PERCENTAGE = "effectivePaymentPercentage";
    String MONTHLYPAYMENTDATE = "monthlyPaymentDate";
    String FIRST_PAYMENT_DATE = "firstPaymentDate";

    String IS_PAYMENT_6TIME = "isPayment6times";
    String IS_PAYMENT_9TIME = "isPayment9times";
    String IS_PAYMENT_12TIME = "isPayment12times";

    String INSURANCE_FEE = "insuranceFee";

    String SERVICE_FEE = "serviceFee";
    String TRANSFER_FEE = "transferFee";
    String IS_FIVE_PAY = "isFivePay";
    String IS_TWO_FIVE_PAY = "isTwoFivePay";
    String IS_ONE_SEVEN_PAY = "isOneSevenPay";
    String IS_ONE_TWENTY_FIVE_PAY = "isOneSevenPay";
    String IS_ONE_PAY = "isOnePay";
    String IS_ZERO_PAY = "isZeroPay";

    String REGISTATIONLABEL = "registrationLabel";
    // detail 8 payment method
    String IS_MONTHLY_PAYMENT = "isMonthlyPayment";
    String IS_EXT = "isEXT";

}
