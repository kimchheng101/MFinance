package com.soma.mfinance.core.financial.dao.impl;

import org.seuksa.frmk.dao.impl.BaseEntityDaoImpl;
import org.springframework.stereotype.Repository;

import com.soma.mfinance.core.financial.dao.FinancialProductDao;

/**
 * Financial product data access implementation
 * @author kimsuor.seang
 */
@Repository
public class FinancialProductDaoImpl extends BaseEntityDaoImpl implements FinancialProductDao {

}
