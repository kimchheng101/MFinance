package com.soma.mfinance.core.contract.model.schedule.service;

import com.soma.mfinance.core.quotation.model.Quotation;
import org.seuksa.frmk.service.BaseEntityService;

import java.util.List;
import java.util.Set;

/**
 * Created by kimsuor.seang
 * USER   c.chhai
 * DATE on 6/8/2017.
 * TIME at 10:09 AM
 * EMIAL   c.chhai@gl-f.com
 */
public interface ContractScheduleService extends BaseEntityService {

	String CONTRACT_MIGRATION_DIR = "contract_migration";
	public boolean checkQuotationHasInScheduleTable(Quotation quotation);
	public Quotation getQuotationFromTableSchedule(Long id);
	public List<Quotation> quotationListSelected(Set<Long> selectedItemIds);
	void executedContractActivateSchedule(Quotation quotation);
	Boolean isExecutedContractActivateSchedule(Quotation quotation);
}
