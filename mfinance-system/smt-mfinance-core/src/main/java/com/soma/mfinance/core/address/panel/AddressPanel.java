package com.soma.mfinance.core.address.panel;

import com.soma.common.app.eref.ECountry;
import com.soma.mfinance.core.Enum.EHousing;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.mfinance.core.applicant.model.IndividualAddress;
import com.soma.mfinance.core.applicant.service.ValidateNumbers;
import com.soma.mfinance.core.custom.component.CustomEntityRefComboBox;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.ersys.core.hr.model.address.*;
import com.soma.ersys.core.hr.model.eref.EResidenceStatus;
import com.soma.ersys.core.hr.model.eref.ETypeAddress;
import com.soma.frmk.vaadin.ui.panel.AbstractControlPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import org.apache.commons.lang.StringUtils;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Address Panel
 *
 * @author kimsuor.seang
 */
public class AddressPanel extends AbstractControlPanel {

    private static final long serialVersionUID = -765492115214802081L;

    private TextField txtHouseNo;
    private TextField txtStreet;
    private TextField txtZipCode;
    private ERefDataComboBox<ECountry> cbxCountry;

    private TextField txtTimeAtAddressInYear;
    private TextField txtTimeAtAddressInMonth;
    private ERefDataComboBox<EResidenceStatus> cbxpropertyAddress;
    private ERefDataComboBox<ETypeAddress> cbxAddressType;
    private ERefDataComboBox<EHousing> cbxHousing;

    private boolean required;

    private Address address;

    private EntityRefComboBox<Province> cbxProvince;
    private EntityRefComboBox<District> cbxDistrict;
    private EntityRefComboBox<Commune> cbxCommune;
    private EntityRefComboBox<Village> cbxVillage;

    private Quotation quotation = new Quotation();
    private Applicant applicant = new Applicant();
    public AddressPanel(ETypeAddress addressType) {
        this(false, addressType, "address");
    }

    public AddressPanel(boolean required, ETypeAddress addressType) {
        this(required, addressType, "address");
    }

    public AddressPanel(boolean required, ETypeAddress addressType, String template) {
        ValidateNumbers currentAdd = new ValidateNumbers();
        ArrayList currentValid = new ArrayList();

        this.required = required;
        setSizeFull();
        txtHouseNo = ComponentFactory.getTextField(false, 20, 150);
        txtStreet = ComponentFactory.getTextField(false, 100, 172);

        txtZipCode = ComponentFactory.getTextField(60, 360);
        currentValid.add(txtZipCode);
        txtTimeAtAddressInYear = ComponentFactory.getTextField(false, 50, 80);
        currentValid.add(txtTimeAtAddressInYear);
        txtTimeAtAddressInMonth = ComponentFactory.getTextField(false, 50, 80);
        currentValid.add(txtTimeAtAddressInMonth);
        cbxpropertyAddress = new ERefDataComboBox<EResidenceStatus>(EResidenceStatus.values());
        if (addressType == ETypeAddress.WORK) {
            cbxpropertyAddress.setVisible(false);
        }

        cbxHousing = new ERefDataComboBox<EHousing>(EHousing.values());
        cbxHousing.setImmediate(true);
        cbxHousing.setWidth("140px");

        cbxCountry = new ERefDataComboBox<ECountry>(ECountry.values());
        cbxCountry.setImmediate(true);
        cbxCountry.setWidth(140, Unit.PIXELS);

        cbxProvince = new CustomEntityRefComboBox();
        cbxProvince.setRestrictions(new BaseRestrictions<>(Province.class));
        cbxProvince.setImmediate(true);
        cbxProvince.setWidth("140px");
        cbxProvince.renderer();

        cbxDistrict = new CustomEntityRefComboBox();
        cbxDistrict.setImmediate(true);
        cbxDistrict.setWidth("140px");

        cbxCommune = new CustomEntityRefComboBox();
        cbxCommune.setImmediate(true);
        cbxCommune.setWidth("140px");

        cbxVillage = new EntityRefComboBox();
        cbxVillage.setImmediate(true);
        cbxVillage.setWidth("140px");

        ((CustomEntityRefComboBox) cbxProvince).setFilteringComboBox(cbxDistrict, District.class);
        ((CustomEntityRefComboBox) cbxDistrict).setFilteringComboBox(cbxCommune, Commune.class);
        ((CustomEntityRefComboBox) cbxCommune).setFilteringComboBox(cbxVillage, Village.class);

        cbxAddressType = new ERefDataComboBox<>(ETypeAddress.class);

        // cbxVillage = new EntityRefComboBox<Village>();
        // cbxVillage.setRestrictions(new BaseRestrictions<Village>(Village.class));
        // cbxVillage.setImmediate(true);
        // cbxVillage.setWidth(150, Unit.PIXELS);


        InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
        CustomLayout customLayout = null;
        try {
            customLayout = new CustomLayout(layoutFile);
            customLayout.setSizeFull();
        } catch (IOException e) {
            Notification.show("Could not locate template " + template, e.getMessage(), Type.ERROR_MESSAGE);
        }
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("house.no")), "lblHouseNo");
        customLayout.addComponent(txtHouseNo, "txtHouseNo");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("street")), "lblStreet");
        customLayout.addComponent(txtStreet, "txtStreet");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("country")), "lblCountry");
        customLayout.addComponent(cbxCountry, "cbxCountry");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("housing")), "lblHousing");
        customLayout.addComponent(cbxHousing, "cbxHousing");

        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("zip.code")), "lblZipCode");
        customLayout.addComponent(txtZipCode, "txtZipCode");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("province")), "lblProvince");
        customLayout.addComponent(cbxProvince, "cbxProvince");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("district")), "lblDistrict");
        customLayout.addComponent(cbxDistrict, "cbxDistrict");

        customLayout.addComponent(ComponentFactory.getLabel("Time at this address"), "lblTime");
        customLayout.addComponent(txtTimeAtAddressInYear, "txtYear");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("year")), "lblYear");
        customLayout.addComponent(txtTimeAtAddressInMonth, "txtMonth");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("month")), "lblMonth");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("commune.st")), "lblCommune");
        customLayout.addComponent(cbxCommune, "cbxCommune");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("village")), "lblVillage");
        customLayout.addComponent(cbxVillage, "cbxVillage");
        addComponent(customLayout);
        currentAdd.validateNumberDot(currentValid);
    }

    /**
     * @param enabled
     */
    public void setAddressEnabled(boolean enabled) {
        if (enabled) {
            cbxCountry.removeStyleName("v-textfield-blackdisabled");
            cbxProvince.removeStyleName("v-textfield-blackdisabled");
            cbxDistrict.removeStyleName("v-textfield-blackdisabled");
            cbxCommune.removeStyleName("v-textfield-blackdisabled");
            cbxVillage.removeStyleName("v-textfield-blackdisabled");
            cbxAddressType.removeStyleName("v-textfield-blackdisabled");
            txtHouseNo.removeStyleName("blackdisabled");
            txtStreet.removeStyleName("blackdisabled");
            //txtTimeAtAddressInYear.removeStyleName("blackdisabled");
            //txtTimeAtAddressInMonth.removeStyleName("blackdisabled");
        } else {
            cbxCountry.addStyleName("v-textfield-blackdisabled");
            cbxProvince.addStyleName("v-textfield-blackdisabled");
            cbxDistrict.addStyleName("v-textfield-blackdisabled");
            cbxCommune.addStyleName("v-textfield-blackdisabled");
            cbxVillage.addStyleName("v-textfield-blackdisabled");
            cbxAddressType.addStyleName("v-textfield-blackdisabled");
            txtHouseNo.addStyleName("blackdisabled");
            txtStreet.addStyleName("blackdisabled");
            //txtTimeAtAddressInYear.addStyleName("blackdisabled");
            //txtTimeAtAddressInMonth.addStyleName("blackdisabled");
        }

        txtHouseNo.setEnabled(enabled);
        txtStreet.setEnabled(enabled);
        cbxCountry.setEnabled(enabled);
        cbxProvince.setEnabled(enabled);
        cbxDistrict.setEnabled(enabled);
        cbxCommune.setEnabled(enabled);
        cbxAddressType.setEnabled(enabled);
        cbxVillage.setEnabled(enabled);
        //txtTimeAtAddressInYear.setEnabled(enabled);
        //txtTimeAtAddressInMonth.setEnabled(enabled);
        //cbxpropertyAddress.setEnabled(enabled);
    }

    /**
     * @param
     * @return
     */
    public Address getAddress(Address address) {
        address.setHouseNo(txtHouseNo.getValue());
        address.setStreet(txtStreet.getValue());
        address.setTimeAtAddressInYear(getInteger(txtTimeAtAddressInYear));
        address.setTimeAtAddressInMonth(getInteger(txtTimeAtAddressInMonth));
        address.setResidenceStatus(cbxpropertyAddress.getSelectedEntity());
        address.setCountry(cbxCountry.getSelectedEntity());
        address.setVillage(cbxVillage.getSelectedEntity());
        address.setCommune(cbxCommune.getSelectedEntity());
        address.setDistrict(cbxDistrict.getSelectedEntity());
        address.setProvince(cbxProvince.getSelectedEntity());
        address.setType(cbxAddressType.getSelectedEntity());
        return address;
    }

    public void getIndividualAddress(Individual individual) {
        IndividualAddress individualAddress;
        if (individual.getIndividualAddress(ETypeAddress.MAIN) == null) {
            individualAddress = new IndividualAddress();
        } else {
            individualAddress = individual.getIndividualAddress(ETypeAddress.MAIN);
        }
        individualAddress.setHousing(cbxHousing.getSelectedEntity());
        individualAddress.setZipCode(txtZipCode.getValue());
        individualAddress.setTimeAtAddressInYear(getInteger(txtTimeAtAddressInYear, 0));
        individualAddress.setTimeAtAddressInMonth(getInteger(txtTimeAtAddressInMonth, 0));
        individual.getIndividualAddresses().add(individualAddress);

        //set enable field false when quotation approve
        if(individual != null){
            if(individual.getId().equals(applicant.getId())){
                quotation = applicant.getQuotation();
                if(quotation != null){
                    if(quotation.getWkfStatus().equals(QuotationWkfStatus.APV)){
                        setEnableAddress(false);
                    }
                }
            }
        }
    }

    public void assignValues(Address address) {
       if(address != null){
           txtHouseNo.setValue(getDefaultString(address.getHouseNo()));
           txtStreet.setValue(getDefaultString(address.getStreet()));
           txtTimeAtAddressInYear.setValue(getDefaultString(address.getTimeAtAddressInYear()));
           txtTimeAtAddressInMonth.setValue(getDefaultString(address.getTimeAtAddressInMonth()));
           cbxpropertyAddress.setSelectedEntity(address.getResidenceStatus());
           cbxCountry.setSelectedEntity(address.getCountry());
           cbxProvince.setSelectedEntity(address.getProvince());
           cbxDistrict.setSelectedEntity(address.getDistrict());
           cbxCommune.setSelectedEntity(address.getCommune());
           cbxAddressType.setSelectedEntity(address.getType());
           cbxVillage.setSelectedEntity(address.getVillage());
           setAddress(address);
       }
    }

    public void assignIndAdress(IndividualAddress individualAddresses) {
        if (individualAddresses != null) {
            if (individualAddresses.getHousing() != null)
                cbxHousing.setSelectedEntity(individualAddresses.getHousing());
            if (individualAddresses.getZipCode() != null)
                txtZipCode.setValue(individualAddresses.getZipCode());
            if (individualAddresses.getTimeAtAddressInYear() != null)
                txtTimeAtAddressInYear.setValue(individualAddresses.getTimeAtAddressInYear().toString());
            if (individualAddresses.getTimeAtAddressInMonth() != null)
                txtTimeAtAddressInMonth.setValue(individualAddresses.getTimeAtAddressInMonth().toString());
        }
    }

    /**
     * Reset panel
     */
    public void reset() {
        assignValues(new Address());
        cbxCountry.setSelectedEntity(ECountry.THA);
    }

    /**
     * @return
     */
    public List<String> fullValidate() {
        super.reset();
        if (required) {
            checkMandatoryField(txtTimeAtAddressInYear, "year");
            checkMandatoryField(txtTimeAtAddressInMonth, "month");
            checkMandatorySelectField(cbxHousing, "housing");
            checkMandatorySelectField(cbxCountry, "country");
            checkMandatorySelectField(cbxProvince, "province");
            checkMandatorySelectField(cbxDistrict, "district");
            checkMandatorySelectField(cbxCommune, "commune");
            //checkMandatorySelectField(cbxAddressType, "address.type");
            checkMandatorySelectField(cbxVillage, "village");
        }
        return errors;
    }

    /**
     * Validate Dealer Address
     *
     * @return
     */
    public List<String> validateDealerAddress() {
        super.reset();
        if (required) {
            checkMandatorySelectField(cbxCountry, "country");
            checkMandatorySelectField(cbxProvince, "province");
            checkMandatorySelectField(cbxDistrict, "district");
            checkMandatorySelectField(cbxCommune, "commune");
            checkMandatorySelectField(cbxAddressType, "address.type");
            checkMandatorySelectField(cbxVillage, "village");
        }
        return errors;
    }

    /**
     * @return
     */
    public List<String> partialValidate() {
        super.reset();
        if (required) {
            checkMandatorySelectField(cbxCountry, "country");
            checkMandatorySelectField(cbxProvince, "province");
            checkMandatorySelectField(cbxDistrict, "district");
            checkMandatorySelectField(cbxCommune, "commune");
            checkMandatorySelectField(cbxVillage, "village");
        }
        return errors;
    }

    /**
     * Validate address
     *
     * @param
     * @return
     */
    public boolean validateAddress() {
        boolean isValid = false;

        if (StringUtils.isNotEmpty(txtHouseNo.getValue())
                || StringUtils.isNotEmpty(txtStreet.getValue())
                || cbxProvince.getSelectedEntity() != null
                || cbxDistrict.getSelectedEntity() != null
                || cbxCommune.getSelectedEntity() != null) {
            isValid = true;
        }

        return isValid;
    }

    /**
     * @return the address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(Address address) {
        this.address = address;
    }


    public  void setEnableAddress(boolean enable){
       txtHouseNo.setEnabled(enable);
       txtStreet.setEnabled(enable);
       txtZipCode.setEnabled(enable);
       cbxCountry.setEnabled(enable);
       txtTimeAtAddressInYear.setEnabled(enable);
       txtTimeAtAddressInMonth.setEnabled(enable);
       cbxpropertyAddress.setEnabled(enable);
       cbxAddressType.setEnabled(enable);
       cbxHousing.setEnabled(enable);

       cbxProvince.setEnabled(enable);
       cbxDistrict.setEnabled(enable);
       cbxCommune.setEnabled(enable);
       cbxVillage.setEnabled(enable);

    }
}
