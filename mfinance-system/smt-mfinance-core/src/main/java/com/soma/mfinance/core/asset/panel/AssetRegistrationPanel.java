package com.soma.mfinance.core.asset.panel;

import com.soma.ersys.core.hr.model.eref.EColor;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.soma.mfinance.core.asset.model.*;
import com.soma.mfinance.core.custom.component.CustomEntityRefComboBox;
import com.soma.mfinance.core.quotation.model.Comment;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.Runo;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Dang Dim
 * Date     : 26-Aug-17, 1:59 PM
 * Email    : d.dim@gl-f.com
 */
public class AssetRegistrationPanel extends AbstractTabPanel {

    private EntityRefComboBox<AssetRange> cbxAssetRange;
    protected EntityRefComboBox<AssetModel> cbxAssetModel;
    private ERefDataComboBox<EEngine> cbxEngine;
    private ERefDataComboBox<EAssetGender> cbxAssetGender;
    private ERefDataComboBox<EAssetYear> cbxYear;
    private ERefDataComboBox<EColor> cbxColor;
    private TextField txtChassisNumber;
    private TextField txtEngineNumber;
    private TextField txtPlateNumber;
    private DateField dfRegistrationDate;

    private TextField txtChassisNumberDc;
    private TextField txtEngineNumberDc;
    private TextField txtPlateNumberDc;
    private DateField dfRegistrationDateDc;

    protected long assetModelId;

    private HorizontalLayout horizontalLayout;
    private Panel dcFormPanelLayout;
    private Quotation quotation;
    private Asset asset;

    private Button btnAddChasisNumber;
    private Button tblViewChasisNumber;

    private Button btnAddEnginNumber;
    private Button tblViewEnginNumber;

    private Button btnAddPlateNumber;
    private Button tblViewPlateNumber;

    private String strChassis;
    private String strEngine;
    private String strPlate;

    public AssetRegistrationPanel() {
        super();
        this.setSizeFull();
    }

    @Override
    protected Component createForm() {
        cbxAssetRange = new CustomEntityRefComboBox<>();
        cbxAssetRange.setCaption(I18N.message("asset.make2"));
        cbxAssetRange.setRestrictions(new BaseRestrictions<>(AssetRange.class));
        cbxAssetRange.renderer();
        cbxAssetRange.setSelectedEntity(null);
        cbxAssetRange.setImmediate(true);

        cbxAssetModel = new CustomEntityRefComboBox<>();
        cbxAssetModel.setCaption(I18N.message("asset.range2"));
        cbxAssetModel.setRequired(true);
        cbxAssetModel.setSelectedEntity(null);
        cbxAssetModel.setImmediate(true);

        ((CustomEntityRefComboBox) cbxAssetRange).setFilteringComboBox(cbxAssetModel, AssetModel.class);
        cbxColor = new ERefDataComboBox<EColor>(I18N.message("color"), EColor.class);
        cbxColor.setRequired(true);
        cbxColor.setSelectedEntity(null);
        cbxColor.setImmediate(true);

        cbxYear = new ERefDataComboBox<>(I18N.message("asset.year"), EAssetYear.values());
        cbxYear.setImmediate(true);
        cbxYear.setVisible(true);
        cbxYear.setRequired(true);
        cbxYear.setWidth("220px");

        cbxAssetGender = new ERefDataComboBox<>(I18N.message("type"), EAssetGender.values());
        cbxAssetGender.setImmediate(true);
        cbxAssetGender.setRequired(true);
        cbxAssetGender.setWidth("220px");

        cbxEngine = new ERefDataComboBox<>(I18N.message("engine"), EEngine.values());
        cbxEngine.setImmediate(true);
        cbxEngine.setRequired(true);
        cbxEngine.setWidth("220px");

        txtChassisNumber = ComponentFactory.getTextField(I18N.message("chassis.number"), true, 50, 220);
        txtEngineNumber = ComponentFactory.getTextField(I18N.message("engine.number"), true, 50, 220);
        txtPlateNumber = ComponentFactory.getTextField(I18N.message("plate.number"), true, 50, 220);
        dfRegistrationDate = ComponentFactory.getAutoDateField(I18N.message("registration.date"), true);

        txtChassisNumberDc = ComponentFactory.getTextField(I18N.message("chassis.number"), true, 50, 220);
        txtEngineNumberDc = ComponentFactory.getTextField(I18N.message("engine.number"), true, 50, 220);
        txtPlateNumberDc = ComponentFactory.getTextField(I18N.message("plate.number"), true, 50, 220);
        dfRegistrationDateDc = ComponentFactory.getAutoDateField(I18N.message("registration.date"), true);

        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(ComponentFactory.getSpaceLayout(0, Unit.PIXELS));
        horizontalLayout.addComponent(this.buildAssetLayout());

        horizontalLayout.addComponent(ComponentFactory.getSpaceLayout(0, Unit.PIXELS));
        horizontalLayout.addComponent(this.buildAssetMiddleLayout());

        /*Button add and view*/
        horizontalLayout.addComponent(buildGridButtonsLayout());

        dcFormPanelLayout = new Panel(I18N.message("document.controller"));
        dcFormPanelLayout.setContent(this.buildAssetDocControllerLayout());
        horizontalLayout.addComponent(dcFormPanelLayout);

        if (ProfileUtil.isDocumentController()) {
            setEnabledAsset();
        }

        final Panel AssetRegistrationPanel = new Panel(I18N.message("asset"));
        AssetRegistrationPanel.setContent(horizontalLayout);
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        verticalLayout.addComponent(AssetRegistrationPanel);
        return verticalLayout;
    }

    /**
     * This is middle layout of asset details
     */
    private FormLayout buildAssetMiddleLayout() {
        FormLayout assetChassisLayout = new FormLayout();
        assetChassisLayout.setStyleName("asset-dc");
        assetChassisLayout.addComponent(txtChassisNumber);
        assetChassisLayout.addComponent(txtEngineNumber);
        assetChassisLayout.addComponent(txtPlateNumber);
        assetChassisLayout.addComponent(dfRegistrationDate);
        return assetChassisLayout;
    }

    /**
     * This method for asset DC
     */
    private GridLayout buildGridButtonsLayout() {

        GridLayout layout = new GridLayout(2, 3);
        layout.setStyleName("grid-buttons-dc");

        btnAddChasisNumber = new Button();
        btnAddChasisNumber.setVisible(true);
        btnAddChasisNumber.setStyleName(Runo.BUTTON_LINK);
        btnAddChasisNumber.setIcon(new ThemeResource("../smt-default/icons/16/add.png"));
        btnAddChasisNumber.setStyleName("margin-top");

        btnAddChasisNumber.addClickListener((event) -> {
            final Window winAddChassisDc = new Window();
            winAddChassisDc.setModal(true);
            winAddChassisDc.setWidth(350, Unit.PIXELS);
            winAddChassisDc.setHeight(220, Unit.PIXELS);
            strChassis = I18N.message("chassis.number");
            winAddChassisDc.setCaption(strChassis);
            final TextField txtMoreChassisDc;
            txtMoreChassisDc = ComponentFactory.getTextField(strChassis, false, 50, 150);
            txtMoreChassisDc.setRequired(true);
            Button btnCancel = new NativeButton(I18N.message("close"), new Button.ClickListener() {
                private static final long serialVersionUID = 3975121141565713259L;

                public void buttonClick(Button.ClickEvent event) {
                    winAddChassisDc.close();
                }
            });
            btnCancel.setIcon(new ThemeResource("../smt-default/icons/16/delete.png"));
            Button btnSave = new NativeButton(I18N.message("save"), new Button.ClickListener() {
                private static final long serialVersionUID = 8088485001713740490L;

                public void buttonClick(Button.ClickEvent event) {
                    if (txtMoreChassisDc.getValue() != null && !txtMoreChassisDc.isEmpty()) {
                        AddMultiAssetChassisDoc addMultiAssetChassisDoc = new AddMultiAssetChassisDoc();
                        addMultiAssetChassisDoc.setAssetChassisDc(txtMoreChassisDc.getValue());
                        if (quotation != null) {
                            addMultiAssetChassisDoc.setQuotation(quotation);
                        }
                        ENTITY_SRV.saveOrUpdate(addMultiAssetChassisDoc);
                        SecUser secUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                        Comment comment = new Comment();
                        comment.setDesc(strChassis + txtMoreChassisDc + " to this asset");
                        comment.setQuotation(quotation);
                        comment.setUser(secUser);
                        ENTITY_SRV.saveOrUpdate(comment);
                        winAddChassisDc.close();
                    } else {
                        MessageBox mb = new MessageBox(
                                UI.getCurrent(), "320px", "140px", I18N.message("information"),
                                MessageBox.Icon.ERROR,
                                I18N.message("the.field.require.can't.null.or.empty"), Alignment.MIDDLE_RIGHT,
                                new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, I18N.message("ok")));
                        mb.show();
                    }
                }
            });
            btnSave.setIcon(new ThemeResource("../smt-default/icons/16/save.png"));
            NavigationPanel navigationPanel = new NavigationPanel();
            navigationPanel.addButton(btnSave);
            navigationPanel.addButton(btnCancel);
            VerticalLayout contentLayout = new VerticalLayout();
            contentLayout.setMargin(true);
            contentLayout.addComponent(new FormLayout(txtMoreChassisDc));
            winAddChassisDc.setContent(new VerticalLayout(navigationPanel, contentLayout));
            UI.getCurrent().addWindow(winAddChassisDc);

        });

        tblViewChasisNumber = new Button();
        tblViewChasisNumber.setVisible(true);
        tblViewChasisNumber.setStyleName(Runo.BUTTON_LINK);
        tblViewChasisNumber.setIcon(new ThemeResource("../smt-default/icons/16/table.png"));
        tblViewChasisNumber.setStyleName("margin-top");
        tblViewChasisNumber.addClickListener((event) -> {
            final Window winAddChssisDc = new Window();
            winAddChssisDc.setModal(true);
            winAddChssisDc.setWidth(500, Unit.PIXELS);
            winAddChssisDc.setHeight(328, Unit.PIXELS);

            winAddChssisDc.setCaption(strChassis);
            List<ColumnDefinition> columnDefinitions = new ArrayList<ColumnDefinition>();
            columnDefinitions.add(new ColumnDefinition("chassis", strChassis, String.class, Table.Align.LEFT, 100));
            columnDefinitions.add(new ColumnDefinition("create.user", I18N.message("create.user"), String.class, Table.Align.LEFT, 120));
            columnDefinitions.add(new ColumnDefinition("create.date", I18N.message("create.date"), Date.class, Table.Align.LEFT, 120));
            SimplePagedTable<AddMultiAssetChassisDoc> pagedTable = new SimplePagedTable<>(columnDefinitions);
            IndexedContainer indexedContainer = new IndexedContainer();
            for (ColumnDefinition column : columnDefinitions) {
                indexedContainer.addContainerProperty(column.getPropertyId(), column.getPropertyType(), null);
            }
            BaseRestrictions<AddMultiAssetChassisDoc> restrictions = new BaseRestrictions<AddMultiAssetChassisDoc>(AddMultiAssetChassisDoc.class);
            restrictions.addCriterion(Restrictions.eq("quotation", quotation));
            List<AddMultiAssetChassisDoc> addMultiChassisDocs = ENTITY_SRV.list(restrictions);
            if (addMultiChassisDocs != null && !addMultiChassisDocs.isEmpty()) {
                for (AddMultiAssetChassisDoc addMultiChassisDoc : addMultiChassisDocs) {
                    Item item = indexedContainer.addItem(addMultiChassisDoc.getId());
                    item.getItemProperty("chassis").setValue(addMultiChassisDoc.getAssetChassisDc());
                    item.getItemProperty("create.user").setValue(addMultiChassisDoc.getCreateUser());
                    item.getItemProperty("create.date").setValue(addMultiChassisDoc.getCreateDate());
                }
                pagedTable.setContainerDataSource(indexedContainer);
            }
            winAddChssisDc.setContent(new VerticalLayout(pagedTable, pagedTable.createControls()));
            UI.getCurrent().addWindow(winAddChssisDc);
        });

        btnAddEnginNumber = new Button();
        btnAddEnginNumber.setVisible(true);
        btnAddEnginNumber.setStyleName(Runo.BUTTON_LINK);
        btnAddEnginNumber.setIcon(new ThemeResource("../smt-default/icons/16/add.png"));
        btnAddEnginNumber.setStyleName("margin-top");
        btnAddEnginNumber.addClickListener((event) -> {
            final Window winEnginDc = new Window();
            winEnginDc.setModal(true);
            winEnginDc.setWidth(350, Unit.PIXELS);
            winEnginDc.setHeight(220, Unit.PIXELS);
            strEngine = I18N.message("engine.number");
            winEnginDc.setCaption(strEngine);
            final TextField txtMoreEngineDc;
            txtMoreEngineDc = ComponentFactory.getTextField(strEngine,
                    false, 50, 150);
            txtMoreEngineDc.setRequired(true);
            Button btnCancel = new NativeButton(I18N.message("close"), new Button.ClickListener() {
                private static final long serialVersionUID = 3975121141565713259L;

                public void buttonClick(Button.ClickEvent event) {
                    winEnginDc.close();
                }
            });
            btnCancel.setIcon(new ThemeResource("../smt-default/icons/16/delete.png"));
            Button btnSave = new NativeButton(I18N.message("save"), new Button.ClickListener() {
                private static final long serialVersionUID = 8088485001713740490L;

                public void buttonClick(Button.ClickEvent event) {
                    if (txtMoreEngineDc.getValue() != null && !txtMoreEngineDc.isEmpty()) {
                        AddMultiAssetEngineDoc addMultiAssetEngineDoc = new AddMultiAssetEngineDoc();
                        addMultiAssetEngineDoc.setAssetEngineDc(txtMoreEngineDc.getValue());

                        if (quotation != null) {
                            addMultiAssetEngineDoc.setQuotation(quotation);
                        }
                        ENTITY_SRV.saveOrUpdate(addMultiAssetEngineDoc);
                        SecUser secUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                        Comment comment = new Comment();
                        comment.setDesc(strEngine + txtMoreEngineDc + " to this asset");
                        comment.setQuotation(quotation);
                        comment.setUser(secUser);
                        ENTITY_SRV.saveOrUpdate(comment);
                        winEnginDc.close();
                    } else {
                        MessageBox mb = new MessageBox(
                                UI.getCurrent(), "320px", "140px", I18N.message("information"), MessageBox.Icon.ERROR,
                                I18N.message("the.field.require.can't.null.or.empty"), Alignment.MIDDLE_RIGHT,
                                new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, I18N.message("ok")));
                        mb.show();
                    }
                }
            });
            btnSave.setIcon(new ThemeResource("../smt-default/icons/16/save.png"));
            NavigationPanel navigationPanel = new NavigationPanel();
            navigationPanel.addButton(btnSave);
            navigationPanel.addButton(btnCancel);
            VerticalLayout contentLayout = new VerticalLayout();
            contentLayout.setMargin(true);
            contentLayout.addComponent(new FormLayout(txtMoreEngineDc));
            winEnginDc.setContent(new VerticalLayout(navigationPanel, contentLayout));
            UI.getCurrent().addWindow(winEnginDc);
        });

        tblViewEnginNumber = new Button();
        tblViewEnginNumber.setVisible(true);
        tblViewEnginNumber.setStyleName(Runo.BUTTON_LINK);
        tblViewEnginNumber.setIcon(new ThemeResource("../smt-default/icons/16/table.png"));
        tblViewEnginNumber.setStyleName("margin-top");
        tblViewEnginNumber.addClickListener((event) -> {
            final Window winAddEngneDc = new Window();
            winAddEngneDc.setModal(true);
            winAddEngneDc.setWidth(500, Unit.PIXELS);
            winAddEngneDc.setHeight(328, Unit.PIXELS);
            strEngine = I18N.message("engine.number");
            winAddEngneDc.setCaption(strEngine);
            List<ColumnDefinition> columnDefinitions = new ArrayList<ColumnDefinition>();
            columnDefinitions.add(new ColumnDefinition("engine", strEngine, String.class, Table.Align.LEFT, 100));
            columnDefinitions.add(new ColumnDefinition("create.user", I18N.message("create.user"), String.class, Table.Align.LEFT, 120));
            columnDefinitions.add(new ColumnDefinition("create.date", I18N.message("create.date"), Date.class, Table.Align.LEFT, 120));
            SimplePagedTable<AddMultiAssetEngineDoc> pagedTable = new SimplePagedTable<>(columnDefinitions);
            IndexedContainer indexedContainer = new IndexedContainer();
            for (ColumnDefinition column : columnDefinitions) {
                indexedContainer.addContainerProperty(column.getPropertyId(), column.getPropertyType(), null);
            }
            BaseRestrictions<AddMultiAssetEngineDoc> restrictions = new BaseRestrictions<AddMultiAssetEngineDoc>(AddMultiAssetEngineDoc.class);
            restrictions.addCriterion(Restrictions.eq("quotation", quotation));
            List<AddMultiAssetEngineDoc> addMultiAssetEngineDocs = ENTITY_SRV.list(restrictions);
            if (addMultiAssetEngineDocs != null && !addMultiAssetEngineDocs.isEmpty()) {
                for (AddMultiAssetEngineDoc addMultiAssetEngineDoc : addMultiAssetEngineDocs) {
                    Item item = indexedContainer.addItem(addMultiAssetEngineDoc.getId());
                    item.getItemProperty("engine").setValue(addMultiAssetEngineDoc.getAssetEngineDc());
                    item.getItemProperty("create.user").setValue(addMultiAssetEngineDoc.getCreateUser());
                    item.getItemProperty("create.date").setValue(addMultiAssetEngineDoc.getCreateDate());
                }
                pagedTable.setContainerDataSource(indexedContainer);
            }
            winAddEngneDc.setContent(new VerticalLayout(pagedTable, pagedTable.createControls()));
            UI.getCurrent().addWindow(winAddEngneDc);
        });

        btnAddPlateNumber = new Button();
        btnAddPlateNumber.setVisible(true);
        btnAddPlateNumber.setStyleName(Runo.BUTTON_LINK);
        btnAddPlateNumber.setIcon(new ThemeResource("../smt-default/icons/16/add.png"));
        btnAddPlateNumber.setStyleName("margin-top");
        btnAddPlateNumber.addClickListener((event) -> {
            final Window winPlateDc = new Window();
            winPlateDc.setModal(true);
            winPlateDc.setWidth(350, Unit.PIXELS);
            winPlateDc.setHeight(220, Unit.PIXELS);
            strPlate = I18N.message("plate.number");
            winPlateDc.setCaption(strPlate);
            final TextField txtMorePlateDc;
            txtMorePlateDc = ComponentFactory.getTextField(strPlate, false, 50, 150);
            txtMorePlateDc.setRequired(true);
            Button btnCancel = new NativeButton(I18N.message("close"), new Button.ClickListener() {
                private static final long serialVersionUID = 3975121141565713259L;

                public void buttonClick(Button.ClickEvent event) {
                    winPlateDc.close();
                }
            });
            btnCancel.setIcon(new ThemeResource("../smt-default/icons/16/delete.png"));
            Button btnSave = new NativeButton(I18N.message("save"), new Button.ClickListener() {
                private static final long serialVersionUID = 8088485001713740490L;

                public void buttonClick(Button.ClickEvent event) {
                    if (txtMorePlateDc.getValue() != null && !txtMorePlateDc.isEmpty()) {
                        AddMultiAssetPlateDoc addMultiAssetPlateDoc = new AddMultiAssetPlateDoc();
                        addMultiAssetPlateDoc.setAssetPlateDc(txtMorePlateDc.getValue());

                        if (quotation != null) {
                            addMultiAssetPlateDoc.setQuotation(quotation);
                        }
                        ENTITY_SRV.saveOrUpdate(addMultiAssetPlateDoc);
                        SecUser secUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                        Comment comment = new Comment();
                        comment.setDesc(strPlate + txtMorePlateDc + " to this asset");
                        comment.setQuotation(quotation);
                        comment.setUser(secUser);
                        ENTITY_SRV.saveOrUpdate(comment);
                        winPlateDc.close();
                    } else {
                        MessageBox mb = new MessageBox(
                                UI.getCurrent(), "320px", "140px", I18N.message("information"), MessageBox.Icon.ERROR,
                                I18N.message("the.field.require.can't.null.or.empty"), Alignment.MIDDLE_RIGHT,
                                new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, I18N.message("ok")));
                        mb.show();
                    }
                }
            });
            btnSave.setIcon(new ThemeResource("../smt-default/icons/16/save.png"));
            NavigationPanel navigationPanel = new NavigationPanel();
            navigationPanel.addButton(btnSave);
            navigationPanel.addButton(btnCancel);
            VerticalLayout contentLayout = new VerticalLayout();
            contentLayout.setMargin(true);
            contentLayout.addComponent(new FormLayout(txtMorePlateDc));
            winPlateDc.setContent(new VerticalLayout(navigationPanel, contentLayout));
            UI.getCurrent().addWindow(winPlateDc);
        });

        tblViewPlateNumber = new Button();
        tblViewPlateNumber.setVisible(true);
        tblViewPlateNumber.setStyleName(Runo.BUTTON_LINK);
        tblViewPlateNumber.setIcon(new ThemeResource("../smt-default/icons/16/table.png"));
        tblViewPlateNumber.setStyleName("margin-top");

        tblViewPlateNumber.addClickListener((event) -> {
            final Window winAddPlateDc = new Window();
            winAddPlateDc.setModal(true);
            winAddPlateDc.setWidth(500, Unit.PIXELS);
            winAddPlateDc.setHeight(328, Unit.PIXELS);
            strPlate = I18N.message("plate.number");
            winAddPlateDc.setCaption(strPlate);
            List<ColumnDefinition> columnDefinitions = new ArrayList<ColumnDefinition>();
            columnDefinitions.add(new ColumnDefinition("plate", strPlate, String.class, Table.Align.LEFT, 100));
            columnDefinitions.add(new ColumnDefinition("create.user", I18N.message("create.user"), String.class, Table.Align.LEFT, 120));
            columnDefinitions.add(new ColumnDefinition("create.date", I18N.message("create.date"), Date.class, Table.Align.LEFT, 120));
            SimplePagedTable<AddMultiAssetPlateDoc> pagedTable = new SimplePagedTable<>(columnDefinitions);
            IndexedContainer indexedContainer = new IndexedContainer();
            for (ColumnDefinition column : columnDefinitions) {
                indexedContainer.addContainerProperty(column.getPropertyId(), column.getPropertyType(), null);
            }
            BaseRestrictions<AddMultiAssetPlateDoc> restrictions = new BaseRestrictions<AddMultiAssetPlateDoc>(AddMultiAssetPlateDoc.class);
            restrictions.addCriterion(Restrictions.eq("quotation", quotation));
            List<AddMultiAssetPlateDoc> addMultiAssetPlateDocs = ENTITY_SRV.list(restrictions);
            if (addMultiAssetPlateDocs != null && !addMultiAssetPlateDocs.isEmpty()) {
                for (AddMultiAssetPlateDoc addMultiAssetPlateDoc : addMultiAssetPlateDocs) {
                    Item item = indexedContainer.addItem(addMultiAssetPlateDoc.getId());
                    item.getItemProperty("plate").setValue(addMultiAssetPlateDoc.getAssetPlateDc());
                    item.getItemProperty("create.user").setValue(addMultiAssetPlateDoc.getCreateUser());
                    item.getItemProperty("create.date").setValue(addMultiAssetPlateDoc.getCreateDate());
                }
                pagedTable.setContainerDataSource(indexedContainer);
            }
            winAddPlateDc.setContent(new VerticalLayout(pagedTable, pagedTable.createControls()));
            UI.getCurrent().addWindow(winAddPlateDc);
        });

        layout.addComponent(btnAddChasisNumber, 0, 0);
        layout.addComponent(tblViewChasisNumber, 1, 0);

        layout.addComponent(btnAddEnginNumber, 0, 1);
        layout.addComponent(tblViewEnginNumber, 1, 1);

        layout.addComponent(btnAddPlateNumber, 0, 2);
        layout.addComponent(tblViewPlateNumber, 1, 2);

        return layout;

    }

    /**
     * This is middle block of asset details
     *
     * @return
     */
    private FormLayout buildAssetDocControllerLayout() {
        FormLayout formLayoutDc = new FormLayout();
        formLayoutDc.addComponent(txtChassisNumberDc);
        formLayoutDc.addComponent(txtEngineNumberDc);
        formLayoutDc.addComponent(txtPlateNumberDc);
        formLayoutDc.addComponent(dfRegistrationDateDc);
        return formLayoutDc;
    }

    /**
     * This is right-layout for asset details
     */
    private FormLayout buildAssetLayout() {
        FormLayout assetDetailLayout = new FormLayout();
        assetDetailLayout.setMargin(true);
        assetDetailLayout.addComponent(cbxAssetRange);
        assetDetailLayout.addComponent(cbxAssetModel);
        assetDetailLayout.addComponent(cbxColor);
        assetDetailLayout.addComponent(cbxYear);
        assetDetailLayout.addComponent(cbxEngine);
        assetDetailLayout.addComponent(cbxAssetGender);
        return assetDetailLayout;
    }

    /*
    * Reset Controls
    */
    public void reset() {
        super.reset();
        cbxAssetRange.setSelectedEntity(null);
        cbxAssetModel.setSelectedEntity(null);
        cbxColor.setSelectedEntity(null);
        cbxYear.setSelectedEntity(null);
        cbxEngine.setSelectedEntity(null);
        cbxAssetGender.setSelectedEntity(null);
        txtChassisNumber.setValue("");
        txtEngineNumber.setValue("");
        txtPlateNumber.setValue("");
        dfRegistrationDate.setValue(null);

        markAsDirty();
    }

    /* Get Asset */
    public Asset getAsset() {
        asset.setEngine(cbxEngine.getSelectedEntity());
        asset.setAssetGender(cbxAssetGender.getSelectedEntity());
        asset.setChassisNumber(txtChassisNumber.getValue());
        asset.setRegistrationDate(dfRegistrationDate.getValue());
        asset.setPlateNumber(txtPlateNumber.getValue());
        asset.setEngineNumber(txtEngineNumber.getValue());
        asset.setModel(cbxAssetModel.getSelectedEntity());
        asset.setColor(cbxColor.getSelectedEntity());
        asset.setAssetYear(cbxYear.getSelectedEntity());
        return asset;
    }

    public void assignValues(Quotation quotation) {
        AssetModel assetModel = null;
        this.quotation = quotation;
        if (quotation != null && quotation.getAsset() != null) {
            this.asset = quotation.getAsset();
            if (asset.getModel() != null) {
                assetModelId = asset.getModel().getId();
                assetModel = ENTITY_SRV.getById(AssetModel.class, asset.getModel().getId());
                cbxAssetRange.setSelectedEntity(assetModel.getAssetRange());
                cbxAssetModel.setSelectedEntity(assetModel);
                if (assetModel.getStatusRecord() == EStatusRecord.INACT) {
                    try {
                        cbxAssetModel.removeItem(assetModel);
                        cbxAssetModel.addItem(assetModel.getId().toString());
                    } catch (Exception e) {
                        cbxAssetModel.addItem(assetModel.getId().toString());
                    }
                    cbxAssetModel.setItemCaption(assetModel.getId().toString(), assetModel.getDescEn());
                }

            }
            cbxColor.setSelectedEntity(asset.getColor());
            cbxYear.setSelectedEntity(asset.getAssetYear());
            cbxEngine.setSelectedEntity(asset.getEngine());
            cbxAssetGender.setSelectedEntity(asset.getAssetGender());

            txtChassisNumber.setValue(asset.getChassisNumber());
            txtEngineNumber.setValue(asset.getEngineNumber());
            txtPlateNumber.setValue(asset.getPlateNumber());
            dfRegistrationDate.setValue(asset.getRegistrationDate());

            txtChassisNumberDc.setValue(asset.getChassisNumber());
            txtEngineNumberDc.setValue(asset.getEngineNumber());
            txtPlateNumberDc.setValue(asset.getPlateNumber());
            dfRegistrationDateDc.setValue(asset.getRegistrationDate());

        }
    }

    public void setEnabledAsset() {

        cbxAssetRange.setEnabled(false);
        cbxAssetModel.setEnabled(false);
        cbxColor.setEnabled(false);
        cbxYear.setEnabled(false);
        cbxEngine.setEnabled(false);
        cbxAssetGender.setEnabled(false);

        txtChassisNumber.setEnabled(false);
        txtEngineNumber.setEnabled(false);
        txtPlateNumber.setEnabled(false);
        dfRegistrationDate.setEnabled(false);

        txtChassisNumberDc.setEnabled(false);
        txtPlateNumberDc.setEnabled(false);
        txtEngineNumberDc.setEnabled(false);
        dfRegistrationDateDc.setEnabled(false);
    }
}
