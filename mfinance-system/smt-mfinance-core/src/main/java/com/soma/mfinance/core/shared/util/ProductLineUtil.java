package com.soma.mfinance.core.shared.util;

import com.soma.common.app.eref.EProductLineCode;
import com.soma.mfinance.core.financial.model.FinProduct;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/23/2017
 * Name  : 1:14 PM
 * Email : k.seang@gl-f.com
 */
public final class ProductLineUtil {

    public static boolean isMotoForPlus(FinProduct finProduct) {

        if (finProduct != null && finProduct.getProductLine() != null &&
                EProductLineCode.MFP.equals(finProduct.getProductLine().getProductLineCode())) {
            return true;
        }
        return false;
    }

}
