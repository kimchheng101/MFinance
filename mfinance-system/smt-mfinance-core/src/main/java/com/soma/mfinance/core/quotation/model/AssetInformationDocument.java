package com.soma.mfinance.core.quotation.model;

import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;

/**
 * 
 * @author th.seng
 * @since 28 Oct, 2017
 *
 */

@Entity
@Table(name = "td_ass_info_doc")
public class AssetInformationDocument extends EntityA {
	private String assetInfo;
	private String assetSizeUnit;
	private Double assetEstPrice;
	private boolean cvConfirm;
	private boolean evidence;
	private boolean original;
	private String path;
	private Quotation quotation;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ass_info_id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	/**
	 * @return the assetInfo
	 */
	@Column(name = "ass_info_ass_info", nullable = true)
	public String getAssetInfo() {
		return assetInfo;
	}

	/**
	 * @param assetInfo the assetInfo to set
	 */
	public void setAssetInfo(String assetInfo) {
		this.assetInfo = assetInfo;
	}

	/**
	 * @return the assetSizeUnit
	 */
	@Column(name = "ass_info_ass_unt", nullable = true)
	public String getAssetSizeUnit() {
		return assetSizeUnit;
	}

	/**
	 * @param assetSizeUnit the assetSizeUnit to set
	 */
	public void setAssetSizeUnit(String assetSizeUnit) {
		this.assetSizeUnit = assetSizeUnit;
	}

	/**
	 * @return the assetEstPrice
	 */
	@Column(name = "ass_info_ass_est_prc", nullable = true)
	public Double getAssetEstPrice() {
		return assetEstPrice;
	}

	/**
	 * @param assetEstPrice the assetEstPrice to set
	 */
	public void setAssetEstPrice(Double assetEstPrice) {
		this.assetEstPrice = assetEstPrice;
	}

	/**
	 * @return the cvConfirm
	 */
	@Column(name = "ass_info_ass_confrm", nullable = true)
	public boolean isCvConfirm() {
		return cvConfirm;
	}

	/**
	 * @param cvConfirm the cvConfirm to set
	 */
	public void setCvConfirm(boolean cvConfirm) {
		this.cvConfirm = cvConfirm;
	}

	/**
	 * @return the evidence
	 */
	@Column(name = "ass_info_ass_evd", nullable = true)
	public boolean isEvidence() {
		return evidence;
	}

	/**
	 * @param evidence the evidence to set
	 */
	public void setEvidence(boolean evidence) {
		this.evidence = evidence;
	}

	/**
	 * @return the original
	 */
	@Column(name = "ass_info_ass_ori", nullable = true)
	public boolean isOriginal() {
		return original;
	}

	/**
	 * @param original the original to set
	 */
	public void setOriginal(boolean original) {
		this.original = original;
	}

	/**
	 * @return the path
	 */
	@Column(name = "ass_info_ass_pth", nullable = true, length = 150)
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * 
	 * @return individual
	 */
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ass_info_quo_id")
	public Quotation getQuotation() {
		return quotation;
	}

	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}
}
