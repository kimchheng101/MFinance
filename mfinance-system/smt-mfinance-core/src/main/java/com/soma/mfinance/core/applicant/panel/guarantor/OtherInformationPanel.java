package com.soma.mfinance.core.applicant.panel.guarantor;

import com.soma.mfinance.core.Enum.ECertifyCurrentAddress;
import com.soma.mfinance.core.Enum.ECertifyIncome;
import com.soma.mfinance.core.Enum.ECorrespondence;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.mfinance.core.applicant.service.ValidateNumbers;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.frmk.vaadin.ui.panel.AbstractControlPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.mfinance.core.applicant.service.ValidateNumbers;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.*;
import com.vaadin.ui.Notification.Type;
import org.apache.commons.lang.BooleanUtils;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.amount.AmountUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Other information panel
 *
 * @author kimsuor.seang
 */
public class OtherInformationPanel extends AbstractControlPanel {

    private static final long serialVersionUID = 8341162309347917428L;

    private TextField txtMonthlyPersonalExpenses;
    private TextField txtMonthlyFamilyExpenses;
    private TextField txtTotalMonthlyExpenses;

    private OptionGroup optDebtOtherSource;
    private TextField txtTotalDebtMonthlyInstallment;

    private OptionGroup optGuarantorOtherLoan;
    private TextField txtConvenientVisitTime;

    private ERefDataComboBox<ECorrespondence> cbxPlaceSendingCorrespondence;
    private ERefDataComboBox<ECertifyCurrentAddress> cbxCertifyCurrentAddress;
    private ERefDataComboBox<ECertifyIncome> cbxCertifyIncome;


    ValidateNumbers numvalid = new ValidateNumbers();

    public OtherInformationPanel() {
        ArrayList txts = new ArrayList();
        setSizeFull();

        String template = "guarantorOtherInformation";
        InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
        CustomLayout customLayout = null;
        try {
            customLayout = new CustomLayout(layoutFile);
        } catch (IOException e) {
            Notification.show("Could not locate template " + template, e.getMessage(), Type.ERROR_MESSAGE);
        }

        optDebtOtherSource = new OptionGroup();
        optDebtOtherSource.addItem(1);
        optDebtOtherSource.setItemCaption(1, I18N.message("yes"));
        optDebtOtherSource.addItem(0);
        optDebtOtherSource.setItemCaption(0, I18N.message("no"));
        optDebtOtherSource.select(0);
        optDebtOtherSource.addStyleName("horizontal");
        txtTotalDebtMonthlyInstallment = ComponentFactory.getTextField(false, 60, 150);
        txtTotalDebtMonthlyInstallment.setEnabled(false);
        txtTotalDebtMonthlyInstallment.setStyleName("blackdisabled");

        optGuarantorOtherLoan = new OptionGroup();
        optGuarantorOtherLoan.addItem(1);
        optGuarantorOtherLoan.setItemCaption(1, I18N.message("yes"));
        optGuarantorOtherLoan.addItem(0);
        optGuarantorOtherLoan.setItemCaption(0, I18N.message("no"));
        optGuarantorOtherLoan.select(0);
        optGuarantorOtherLoan.addStyleName("horizontal");

        txtConvenientVisitTime = ComponentFactory.getTextField(false, 60, 150);
        txtConvenientVisitTime.setWidth("350px");

        cbxCertifyCurrentAddress = new ERefDataComboBox<ECertifyCurrentAddress>(ECertifyCurrentAddress.values());
        cbxCertifyCurrentAddress.setImmediate(true);
        cbxCertifyCurrentAddress.setWidth("220px");

        cbxPlaceSendingCorrespondence = new ERefDataComboBox<ECorrespondence>(ECorrespondence.values());
        cbxPlaceSendingCorrespondence.setImmediate(true);
        cbxPlaceSendingCorrespondence.setWidth("200px");

        cbxCertifyIncome = new ERefDataComboBox<ECertifyIncome>(ECertifyIncome.values());
        cbxCertifyIncome.setImmediate(true);
        cbxCertifyIncome.setWidth("200px");

        txtMonthlyPersonalExpenses = ComponentFactory.getTextField(false, 60, 150);
        txts.add(txtMonthlyPersonalExpenses);
        txtMonthlyPersonalExpenses.setImmediate(true);
        txtMonthlyFamilyExpenses = ComponentFactory.getTextField(false, 60, 150);
        txts.add(txtMonthlyFamilyExpenses);
        txtMonthlyFamilyExpenses.setImmediate(true);
        txtTotalMonthlyExpenses = ComponentFactory.getTextField(false, 60, 150);
        txtTotalMonthlyExpenses.setEnabled(false);
        txtTotalMonthlyExpenses.addStyleName("blackdisabled");

        txtMonthlyPersonalExpenses.addValueChangeListener(new ValueChangeListener() {
            private static final long serialVersionUID = 1522323948219241644L;

            @Override
            public void valueChange(ValueChangeEvent event) {
                txtTotalMonthlyExpenses.setValue(getDefaultString(getTotalMonthlyExpenses()));
            }
        });
        txtMonthlyFamilyExpenses.addValueChangeListener(new ValueChangeListener() {
            private static final long serialVersionUID = -2996851767138046538L;

            @Override
            public void valueChange(ValueChangeEvent event) {
                txtTotalMonthlyExpenses.setValue(getDefaultString(getTotalMonthlyExpenses()));
            }
        });

        customLayout.addComponent(new Label(I18N.message("monthly.personal.expenses")), "lblMonthlyPersonalExpenses");
        customLayout.addComponent(txtMonthlyPersonalExpenses, "txtMonthlyPersonalExpenses");
        customLayout.addComponent(new Label(I18N.message("monthly.family.expenses")), "lblMonthlyFamilyExpenses");
        customLayout.addComponent(txtMonthlyFamilyExpenses, "txtMonthlyFamilyExpenses");
        customLayout.addComponent(new Label(I18N.message("total.expenses")), "lblTotalMonthlyExpenses");
        customLayout.addComponent(txtTotalMonthlyExpenses, "txtTotalMonthlyExpenses");

        customLayout.addComponent(new Label(I18N.message("debt.from.other.source")), "lblDebtOtherSource");
        customLayout.addComponent(optDebtOtherSource, "optDebtOtherSource");

        customLayout.addComponent(new Label(I18N.message("total.debt.monthly.installment")), "lblTotalDebtMonthlyInstallment");
        customLayout.addComponent(txtTotalDebtMonthlyInstallment, "txtTotalDebtMonthlyInstallment");

        customLayout.addComponent(new Label(I18N.message("guarantor.other.loan")), "lblGuarantorOtherLoan");
        customLayout.addComponent(optGuarantorOtherLoan, "optGuarantorOtherLoan");

        customLayout.addComponent(new Label(I18N.message("convenient.time.for.visit")), "lblConvenientVisitTime");
        customLayout.addComponent(txtConvenientVisitTime, "txtConvenientVisitTime");

        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("place.sending.correspondence")), "lblPlaceSendingCorrespondence");
        customLayout.addComponent(cbxPlaceSendingCorrespondence, "cbxPlaceSendingCorrespondence");

        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("certify.current.address")), "lblCertifyCurrentAddress");
        customLayout.addComponent(cbxCertifyCurrentAddress, "cbxCertifyCurrentAddress");

        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("certify.income")), "lblCertifyIncome");
        customLayout.addComponent(cbxCertifyIncome, "cbxCertifyIncome");

        optDebtOtherSource.addValueChangeListener(event -> {
            if (optDebtOtherSource != null && optDebtOtherSource.getValue().equals(1)) {
                txtTotalDebtMonthlyInstallment.setEnabled(true);
                txts.add(txtTotalDebtMonthlyInstallment);
            } else if (optDebtOtherSource.getValue().equals(0)) {
                txtTotalDebtMonthlyInstallment.setEnabled(false);
                txtTotalDebtMonthlyInstallment.setStyleName("blackdisabled");
            }
        });
        if (ProfileUtil.isAdmin()) {
            txtMonthlyPersonalExpenses.setEnabled(true);
            txtMonthlyFamilyExpenses.setEnabled(true);
        }

        numvalid.validateNumberDot(txts);
        addComponent(customLayout);
    }


    /**
     * @return
     */
    public Applicant getApplicant(Applicant applicant) {
        Individual individual = applicant.getIndividual();
        individual.setMonthlyPersonalExpenses(getDouble(txtMonthlyPersonalExpenses));
        individual.setMonthlyFamilyExpenses(getDouble(txtMonthlyFamilyExpenses));
        if (optDebtOtherSource.getValue() != null) {
            individual.setDebtFromOtherSource(Integer.parseInt(optDebtOtherSource.getValue().toString()) == 1);
            individual.setTotalDebtInstallment(getDouble(txtTotalDebtMonthlyInstallment));
        }
        if (optGuarantorOtherLoan.getValue() != null) {
            individual.setGuarantorOtherLoan(Integer.parseInt(optGuarantorOtherLoan.getValue().toString()) == 1);
        }
        individual.setConvenientVisitTime(txtConvenientVisitTime.getValue());

        if (cbxCertifyCurrentAddress.getSelectedEntity() == null)
            individual.setECertifyCurrentAddress(ECertifyCurrentAddress.TEST);
        else
            individual.setECertifyCurrentAddress(cbxCertifyCurrentAddress.getSelectedEntity());

        if (cbxPlaceSendingCorrespondence.getSelectedEntity() == null)
            individual.setECorrespondence(ECorrespondence.CURRENT_ADDRESS);
        else
            individual.setECorrespondence(cbxPlaceSendingCorrespondence.getSelectedEntity());

        if (cbxCertifyIncome.getSelectedEntity() == null)
            individual.setECertifyIncome(ECertifyIncome.TEST);
        else
            individual.setECertifyIncome(cbxCertifyIncome.getSelectedEntity());

        return applicant;
    }

    /**
     * @param applicant
     */
    public void assignValues(Applicant applicant) {
        Quotation quotation;
        Individual individual = applicant.getIndividual();
        if (individual != null) {
            txtMonthlyPersonalExpenses.setValue(AmountUtils.format(individual.getMonthlyPersonalExpenses()));
            txtMonthlyFamilyExpenses.setValue(AmountUtils.format(individual.getMonthlyFamilyExpenses()));
            txtTotalMonthlyExpenses.setValue(AmountUtils.format(MyNumberUtils.getDouble(individual.getMonthlyPersonalExpenses()) + MyNumberUtils.getDouble(individual.getMonthlyFamilyExpenses())));
            txtConvenientVisitTime.setValue(getDefaultString(individual.getConvenientVisitTime()));

            if (individual.getECertifyIncome() != null) {
                cbxCertifyIncome.setSelectedEntity(individual.getECertifyIncome());
            }

            if (individual.getECertifyCurrentAddress() != null) {
                cbxCertifyCurrentAddress.setSelectedEntity(individual.getECertifyCurrentAddress());
            }

            if (individual.getECorrespondence() != null) {
                cbxPlaceSendingCorrespondence.setSelectedEntity(individual.getECorrespondence());
            }

            optDebtOtherSource.setValue(BooleanUtils.toBoolean(individual.isDebtFromOtherSource()) ? 1 : 0);
            txtTotalDebtMonthlyInstallment.setValue(AmountUtils.format(individual.getTotalDebtInstallment()));
            optGuarantorOtherLoan.setValue(BooleanUtils.toBoolean(individual.isGuarantorOtherLoan()) ? 1 : 0);
        }
    }

    /**
     * Get total monthly expenses
     *
     * @return
     */
    private double getTotalMonthlyExpenses() {
        double monthlyPersonalExpenses = getDouble(txtMonthlyPersonalExpenses, 0.0);
        double monthlyFamilyExpenses = getDouble(txtMonthlyFamilyExpenses, 0.0);
        return monthlyPersonalExpenses + monthlyFamilyExpenses;
    }

    /**
     * Reset panel
     */
    public void reset() {
        assignValues(new Applicant());
        cbxCertifyIncome.setSelectedEntity(null);
        cbxCertifyCurrentAddress.setSelectedEntity(null);
        cbxPlaceSendingCorrespondence.setSelectedEntity(null);
    }

    /**
     * @return
     */
    public List<String> validate() {
        super.reset();
        checkDoubleField(txtTotalDebtMonthlyInstallment, "total.debt.monthly.installment");
        return errors;
    }

    /**
     * @return
     */
    public List<String> fullValidate() {
        super.reset();
        //checkMandatoryField(txtMonthlyPersonalExpenses, "monthly.personal.expenses");
        if (MyNumberUtils.getDouble(txtMonthlyPersonalExpenses.getValue(), 0) == 0) {
            errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("Monthly personal expense must be more than 0"));
        }
        checkMandatoryField(txtMonthlyFamilyExpenses, "monthly.family.expenses");
        /*if (MyNumberUtils.getDouble(txtMonthlyFamilyExpenses.getValue(), 0) == 0){
			errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("Monthly family expense must be more than 0"));
		}*/
        checkMandatorySelectField(optDebtOtherSource, "debt.from.other.source");
        if (optDebtOtherSource.getValue() != null) {
            if (Integer.parseInt(optDebtOtherSource.getValue().toString()) == 1) {
                checkMandatoryField(txtTotalDebtMonthlyInstallment, "total.debt.monthly.installment");
                if (MyNumberUtils.getDouble(txtTotalDebtMonthlyInstallment.getValue(), 0) == 0) {
                    errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("Total debt monthly installment must be more than 0"));
                }
            }
        }
        checkMandatorySelectField(optGuarantorOtherLoan, "guarantor.other.loan");
        checkMandatoryField(txtConvenientVisitTime, "convenient.time.for.visit");
        checkMandatorySelectField(cbxPlaceSendingCorrespondence, "place.sending.correspondence");
        return errors;
    }


    public void setEnableOtherInformation(boolean enable) {
        txtMonthlyPersonalExpenses.setEnabled(enable);
        txtMonthlyFamilyExpenses.setEnabled(enable);
        optDebtOtherSource.setEnabled(enable);

        if (!ProfileUtil.isAdmin() && !ProfileUtil.isCreditOfficer() && !ProfileUtil.isProductionOfficer()) {
            txtTotalMonthlyExpenses.setEnabled(enable);
            txtTotalDebtMonthlyInstallment.setEnabled(enable);
        }

        optGuarantorOtherLoan.setEnabled(enable);
        txtConvenientVisitTime.setEnabled(enable);

        cbxPlaceSendingCorrespondence.setEnabled(enable);
        cbxCertifyCurrentAddress.setEnabled(enable);
        cbxCertifyIncome.setEnabled(enable);

    }
}
