package com.soma.mfinance.core.auction.panel.sold;

import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.financial.model.FinProduct;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.workflow.AuctionWkfStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;

import java.util.List;

/**
 * AUTHOR : kimsuor.seang
 * EMAIL  : chhaichivon1995@gmail.com
 * DATE   : 8/29/2017
 * TIME   : 10:30 AM
 */
public class SoldMotorcycleSearchPanel extends AbstractSearchPanel<Contract> {

    private TextField txtReference;
    private TextField txtEngineNumber;
    private TextField txtChassisNumber;
    private TextField txtPlateNumber;
    private TextField txtFirstNameEn;
    private TextField txtLastNameEn;
    HorizontalLayout horizontalLayout;
    FormLayout formLayout;
    private AutoDateField dfStartDate;
    private AutoDateField dfEndDate;
    private EntityRefComboBox<FinProduct> cbxFinancialProduct;

    public SoldMotorcycleSearchPanel(SoldMotorcycleTablePanel tablePanel) {
        super(I18N.message("search"), tablePanel);
    }

    @Override
    protected void reset() {
        txtReference.setValue("");
        txtEngineNumber.setValue("");
        txtPlateNumber.setValue("");
        txtChassisNumber.setValue("");
        txtFirstNameEn.setValue("");
        txtLastNameEn.setValue("");
        dfStartDate.setValue(DateUtils.addMonthsDate(DateUtils.today(), -1));
        dfEndDate.setValue(DateUtils.today());
        cbxFinancialProduct.setSelectedEntity(null);
    }

    @Override
    protected Component createForm() {
        cbxFinancialProduct = new EntityRefComboBox<>(I18N.message("financial.product"));
        cbxFinancialProduct = new EntityRefComboBox<>();
        cbxFinancialProduct.setRestrictions(new BaseRestrictions<>(FinProduct.class));
        cbxFinancialProduct.setImmediate(true);
        cbxFinancialProduct.renderer();
        cbxFinancialProduct.setSelectedEntity(null);
        txtReference = ComponentFactory.getTextField("reference", false, 50, 200);
        txtEngineNumber = ComponentFactory.getTextField("engine.number", false, 50, 200);
        txtChassisNumber = ComponentFactory.getTextField("chassis.number", false, 50, 200);
        txtPlateNumber = ComponentFactory.getTextField("plate.number", false, 50, 200);
        txtFirstNameEn = ComponentFactory.getTextField(I18N.message("firstname.en"), false, 60, 220);
        txtLastNameEn = ComponentFactory.getTextField(I18N.message("lastname.en"), false, 60, 220);
        dfStartDate = ComponentFactory.getAutoDateField(I18N.message("startdate"), false);
        dfStartDate.setValue(DateUtils.addMonthsDate(DateUtils.today(), -1));
        dfEndDate = ComponentFactory.getAutoDateField(I18N.message("enddate"), false);
        dfEndDate.setValue(DateUtils.today());

        horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);

        formLayout = new FormLayout();//1
        formLayout.addComponent(txtReference);
        formLayout.addComponent(txtEngineNumber);
        if (ProfileUtil.isDocumentController()) {
            formLayout.addComponent(dfStartDate);
        }
        horizontalLayout.addComponent(formLayout);

        formLayout = new FormLayout();//2
        formLayout.addComponent(txtChassisNumber);
        formLayout.addComponent(txtPlateNumber);
        if (ProfileUtil.isDocumentController()) {
            formLayout.addComponent(dfEndDate);
        }
        horizontalLayout.addComponent(formLayout);

        if (ProfileUtil.isCollectionSupervisor() || ProfileUtil.isAuctionController()) {
            formLayout = new FormLayout(); //4
            formLayout.addComponent(txtLastNameEn);
            formLayout.addComponent(txtFirstNameEn);
            horizontalLayout.addComponent(formLayout);
        }
        horizontalLayout.addComponent(new FormLayout(cbxFinancialProduct));

        return horizontalLayout;
    }

    @Override
    public BaseRestrictions<Contract> getRestrictions() {
        BaseRestrictions<Contract> restrictions = new BaseRestrictions<>(Contract.class);
        restrictions.addCriterion(Restrictions.eq("requestRepossess", Boolean.TRUE));
        restrictions.addCriterion(Restrictions.eq("auctionStatus", AuctionWkfStatus.SOL));

        restrictions.addAssociation("quotations", "quoa", JoinType.INNER_JOIN);
        restrictions.addAssociation("quoa.asset", "qouasset", JoinType.INNER_JOIN);
        restrictions.addAssociation("quoa.quotationApplicants","quoapp", JoinType.INNER_JOIN);
        restrictions.addAssociation("quoapp.applicant","app", JoinType.INNER_JOIN);


        if (cbxFinancialProduct.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq("financialProduct", cbxFinancialProduct.getSelectedEntity()));
        }
        /*if(ProfileUtil.isDocumentController()) {
            restrictions.addAssociation("contractAuctionData","contractAuctionData", JoinType.INNER_JOIN);
        }*/
        restrictions.addCriterion("quoapp.applicantType", EApplicantType.C);
        if (StringUtils.isNotEmpty(txtReference.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("reference", txtReference.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtEngineNumber.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("qouasset.engineNumber", txtEngineNumber.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtChassisNumber.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("qouasset.chassisNumber", txtChassisNumber.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtPlateNumber.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("qouasset.plateNumber", txtPlateNumber.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtLastNameEn.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("app.individual.lastNameEn", txtLastNameEn.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("app.individual.firstNameEn", txtFirstNameEn.getValue(), MatchMode.ANYWHERE));
        }

     /*   if(ProfileUtil.isDocumentController()) {
            if (dfStartDate.getValue() != null) {
                restrictions.addCriterion(Restrictions.ge("contractAuctionData.sellingDate", DateUtils.getDateAtBeginningOfDay(dfStartDate.getValue())));
            }
            if (dfEndDate.getValue() != null) {
                restrictions.addCriterion(Restrictions.le("contractAuctionData.sellingDate", DateUtils.getDateAtEndOfDay(dfEndDate.getValue())));
            }
        }
        restrictions.addOrder(Order.desc("contractAuctionData"));*/
        List<Contract> list= ENTITY_SRV.list(restrictions);
        return restrictions;
    }

}