package com.soma.mfinance.core.asset.model;

import org.seuksa.frmk.model.entity.EntityA;
import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;


@Entity
@Table(name = "tu_asset_model_hp")
public class AssetModelHp extends EntityA {
	
	private static final long serialVersionUID = -3437583780703636039L;
	private AssetModel assetModel;
	private EEngine hp;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ass_mod_hp_id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ass_mod_id")
	public AssetModel getAssetModel() {
		return assetModel;
	}

	public void setAssetModel(AssetModel assetModel) {
		this.assetModel = assetModel;
	}

	@Column(name = "eng_id", nullable = true)
	@Convert(converter = EEngine.class)
	public EEngine getHp() {
		return hp;
	}

	public void setHp(EEngine hp) {
		this.hp = hp;
	}
    
}
