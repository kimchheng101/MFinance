package com.soma.mfinance.core.collection.service;

import com.soma.mfinance.core.collection.model.CollectionIncentiveReport;
import com.soma.mfinance.core.payment.model.Payment;
import com.soma.mfinance.core.collection.model.CollectionIncentiveReport;
import org.seuksa.frmk.service.BaseEntityService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author by kimsuor.seang  on 12/4/2017.
 */

@Service
public interface CollectionIncentiveReportService extends BaseEntityService {

    void calculateCollectionIncentiveReport(Date selectDate);

    List<CollectionIncentiveReport> getCollectionIncentiveReports(Date selectedDate, Payment payment);
}