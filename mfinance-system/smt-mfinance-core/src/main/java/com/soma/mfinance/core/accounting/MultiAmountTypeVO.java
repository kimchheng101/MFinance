package com.soma.mfinance.core.accounting;

import com.soma.mfinance.core.shared.contract.PenaltyVO;

/****
 * @author p.leap
 */
public class MultiAmountTypeVO {
    double tiPrincipal = 0d;
    double tiInterestRate = 0d;
    double tiInsurance = 0d;
    double tiServiceFee = 0d;
    double otherAmount = 0d;
    double totalInstallment = 0d;
    double installmentAmount = 0d;
    double remainingBalance = 0d;
    double tiTransFee = 0d;
    double penaltyAmount = 0d;
    PenaltyVO penaltyVO;
    double vatTotalInstallment=0d;
    double vatPenaltyAmount=0;

    public MultiAmountTypeVO(){

    }

    public MultiAmountTypeVO(double tiPrincipal,
                             double tiInterestRate,
                             double tiInsurance,
                             double tiServiceFee,
                             double otherAmount,
                             double totalInstallment,
                             double installmentAmount,
                             double remainingBalance,
                             double tiTransFee,
                             double penaltyAmount,
                             PenaltyVO penaltyVO,
                             double vatTotalInstallment,
                             double vatPenaltyAmount){

        this.tiPrincipal=tiPrincipal;
        this.tiInterestRate=tiInterestRate;
        this.tiInsurance=tiInsurance;
        this.tiServiceFee=tiServiceFee;
        this.otherAmount=otherAmount;
        this.totalInstallment=totalInstallment;
        this.installmentAmount=installmentAmount;
        this.remainingBalance=remainingBalance;
        this.tiTransFee=tiTransFee;
        this.penaltyAmount=penaltyAmount;
        this.penaltyVO=penaltyVO;
        this.vatTotalInstallment=vatTotalInstallment;
        this.vatPenaltyAmount=vatPenaltyAmount;
    }
    /***
     *
     * @return tiPrincipal
     */
    public double getTiPrincipal() {
        return tiPrincipal;
    }

    /***
     *
     * @param tiPrincipal
     */
    public void setTiPrincipal(double tiPrincipal) {
        this.tiPrincipal = tiPrincipal;
    }

    /***
     *
     * @return tiInterestRate
     */
    public double getTiInterestRate() {
        return tiInterestRate;
    }

    /***
     *
     * @param tiInterestRate
     */
    public void setTiInterestRate(double tiInterestRate) {
        this.tiInterestRate = tiInterestRate;
    }

    /***
     *
     * @return tiInsurance
     */
    public double getTiInsurance() {
        return tiInsurance;
    }

    /***
     * @param tiInsurance
     */
    public void setTiInsurance(double tiInsurance) {
        this.tiInsurance = tiInsurance;
    }

    /***
     *
     * @return tiServiceFee
     */
    public double getTiServiceFee() {
        return tiServiceFee;
    }

    /**
     *
     * @param tiServiceFee
     */
    public void setTiServiceFee(double tiServiceFee) {
        this.tiServiceFee = tiServiceFee;
    }

    /***
     *
     * @return otherAmount
     */
    public double getOtherAmount() {
        return otherAmount;
    }

    /**
     *
     * @param otherAmount
     */
    public void setOtherAmount(double otherAmount) {
        this.otherAmount = otherAmount;
    }

    /***
     *
     * @return totalInstallment
     */
    public double getTotalInstallment() {
        return totalInstallment;
    }

    /**
     *
     * @param totalInstallment
     */
    public void setTotalInstallment(double totalInstallment) {
        this.totalInstallment = totalInstallment;
    }

    /***
     *
     * @return installmentAmount
     */
    public double getInstallmentAmount() {
        return installmentAmount;
    }

    /**
     *
     * @param installmentAmount
     */
    public void setInstallmentAmount(double installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    /***
     *
     * @return remainingBalance
     */
    public double getRemainingBalance() {
        return remainingBalance;
    }

    /**
     *
     * @param remainingBalance
     */
    public void setRemainingBalance(double remainingBalance) {
        this.remainingBalance = remainingBalance;
    }

    /****
     * @return tiTransFee
     */
    public double getTiTransFee() {
        return tiTransFee;
    }

    /**
     *
     * @param tiTransFee
     */
    public void setTiTransFee(double tiTransFee) {
        this.tiTransFee = tiTransFee;
    }

    public double getPenaltyAmount() {
        return penaltyAmount;
    }

    /***
     *
     * @param penaltyAmount
     */
    public void setPenaltyAmount(double penaltyAmount) {
        this.penaltyAmount = penaltyAmount;
    }

    /***
     *
     * @return penaltyVO
     */
    public PenaltyVO getPenaltyVO() {
        return penaltyVO;
    }

    /***
     *
     * @param penaltyVO
     */
    public void setPenaltyVO(PenaltyVO penaltyVO) {
        this.penaltyVO = penaltyVO;
    }

    /***
     *
     * @return vatTotalInstallment
     */
    public double getVatTotalInstallment() {
        return vatTotalInstallment;
    }

    /***
     *
     * @param vatTotalInstallment
     */
    public void setVatTotalInstallment(double vatTotalInstallment) {
        this.vatTotalInstallment = vatTotalInstallment;
    }

    /***
     *
     * @return vatPenaltyAmount
     */
    public double getVatPenaltyAmount() {
        return vatPenaltyAmount;
    }

    /***
     *
     * @param vatPenaltyAmount
     */
    public void setVatPenaltyAmount(double vatPenaltyAmount) {
        this.vatPenaltyAmount = vatPenaltyAmount;
    }
}
