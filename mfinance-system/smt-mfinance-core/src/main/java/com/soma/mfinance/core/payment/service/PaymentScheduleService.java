package com.soma.mfinance.core.payment.service;

import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import org.seuksa.frmk.service.BaseEntityService;

import java.util.Map;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/25/2017
 * Name  : 11:13 AM
 * Email : k.seang@gl-f.com
 */
public interface PaymentScheduleService extends BaseEntityService {

    Map<Long,Cashflow> getPaymentSchedule(Long contractId) ;
}
