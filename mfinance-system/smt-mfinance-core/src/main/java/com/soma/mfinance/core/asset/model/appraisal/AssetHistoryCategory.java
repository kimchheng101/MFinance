package com.soma.mfinance.core.asset.model.appraisal;


import com.soma.mfinance.core.asset.model.AssetRange;
import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;

/**
 * @author by kimsuor.seang  on 27/10/2017.
 */

@Entity
@Table(name = "td_asset_history_category")
public class AssetHistoryCategory extends EntityRefA {

	private static final long serialVersionUID = -6306218983952229069L;
	private AssetRange assetAppraisalType;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ass_his_cate_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Override
    @Transient
    public String getCode() {
        return code;
    }

    @Column(name = "ass_his_cate_desc", nullable = false, length = 50)
    @Override
    public String getDesc() {
        return super.getDesc();
    }

    @Override
    @Column(name = "ass_his_cate_desc_en", nullable = false, length=50)
    public String getDescEn() {
        return super.getDescEn();
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ass_app_type_id")
    public AssetRange getAssetAppraisalType() {
        return assetAppraisalType;
    }

    public void setAssetAppraisalType(AssetRange assetAppraisalType) {
        this.assetAppraisalType = assetAppraisalType;
    }

}
