package com.soma.mfinance.core.productlines.service;

import com.soma.common.app.eref.EProductLineCode;
import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.quotation.model.Quotation;

/**
 * @author by kimsuor.seang  on 10/26/2017.
 */
public interface ProductLineService {
    EProductLineCode getEProductLineCode(AssetRange assetRange);

     EProductLineCode getEProductLineCodeByQuotation(Quotation quotation);
}
