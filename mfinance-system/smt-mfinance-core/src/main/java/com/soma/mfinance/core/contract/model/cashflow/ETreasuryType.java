package com.soma.mfinance.core.contract.model.cashflow;

import org.seuksa.frmk.model.eref.BaseERefData;

import javax.persistence.AttributeConverter;
import java.util.List;

/**
 * Treasury type
 * 
 * @author kimsuor.seang
 *
 */
public class ETreasuryType extends BaseERefData implements AttributeConverter<ETreasuryType, Long> {
	/** */
	private static final long serialVersionUID = 8773689763758918561L;

	public final static ETreasuryType FCO = new ETreasuryType("FCO", 1);//Financial company
	public final static ETreasuryType DEA = new ETreasuryType("DEA", 2);//Dealer
	public final static ETreasuryType APP = new ETreasuryType("APP", 3);//Applicant
	public final static ETreasuryType MAN = new ETreasuryType("MAN", 4);//Manufacturer
	public final static ETreasuryType PRO = new ETreasuryType("PRO", 5);//Provider
	
	/**
	 * 
	 */
	public ETreasuryType() {
	}
	
	/**
	 * 
	 * @param code
	 * @param id
	 */
	public ETreasuryType(String code, long id) {
		super(code, id);
	}

	@Override
	public ETreasuryType convertToEntityAttribute(Long id) {
		return super.convertToEntityAttribute(id);
	}
	
	@Override
	public Long convertToDatabaseColumn(ETreasuryType arg0) {
		return super.convertToDatabaseColumn(arg0);
	}

	/**
	 * 
	 * @return
	 */
	public static List<ETreasuryType> values() {
		return getValues(ETreasuryType.class);
	}
	
	/**
	 * 
	 * @param code
	 * @return
	 */
	public static ETreasuryType getByCode(String code) {
		return getByCode(ETreasuryType.class, code);
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public static ETreasuryType getById(long id) {
		return getById(ETreasuryType.class, id);
	}
}
