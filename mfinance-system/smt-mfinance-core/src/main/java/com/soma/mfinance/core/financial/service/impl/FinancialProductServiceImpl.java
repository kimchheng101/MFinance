package com.soma.mfinance.core.financial.service.impl;

import com.soma.mfinance.core.accounting.InstallmentVO;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowType;
import com.soma.mfinance.core.financial.dao.FinancialProductDao;
import com.soma.mfinance.core.financial.model.*;
import com.soma.mfinance.core.financial.service.FinServiceRestriction;
import com.soma.mfinance.core.financial.service.FinancialProductService;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.financialproduct.FinancialProductEntityField;
import com.soma.mfinance.core.shared.mplus.accounting.InstallmentServiceMfp;
import org.apache.commons.collections.map.HashedMap;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

import static com.soma.frmk.helper.SeuksaServicesHelper.ENTITY_SRV;


/**
 * Financial product service
 *
 * @author kimsuor.seang
 */
@Service("finProductService")
@Transactional
public class FinancialProductServiceImpl extends BaseEntityServiceImpl implements FinancialProductService, FinancialProductEntityField {

	private static final long serialVersionUID = -5735549630724098577L;

	@Autowired
	private FinancialProductDao dao;

	@Autowired
	private InstallmentServiceMfp installmentServiceMfp;

	@Override
	public FinancialProductDao getDao() {
		return dao;
	}

	@Override
	public FinProduct createFinProduct(FinProduct finProduct) {
		super.saveOrUpdate(finProduct);

		// Create financial product service
		if (finProduct.getFinancialProductServices() != null) {
			for (FinProductService finProductService : finProduct.getFinancialProductServices()) {
				super.saveOrUpdate(finProductService);
			}
		}
		return finProduct;
	}

	@Override
	public void deleteFinProduct(FinProduct finProduct) {
		// Delete financial product service
		if (finProduct.getFinancialProductServices() != null) {
			for (FinProductService finProductService : finProduct.getFinancialProductServices()) {
				super.delete(finProductService);
			}
		}
		super.delete(finProduct);
	}

	@Override
	public FinService getFinServiceByType(EServiceType serviceType) {
		FinServiceRestriction restrictions = new FinServiceRestriction();
		restrictions.setServiceType(serviceType);
		return getFirst(restrictions);
	}

	@Override
	public Map<String, Double> calculationInstallmentAmount(Quotation quotation) {
		Map<String, Double> result = new HashedMap();
		Map<Integer, List<InstallmentVO>> mapInstallmentVO = installmentServiceMfp.getInstallmentVOs(quotation, quotation.getFirstDueDate());

		Double totalInstallmentAmount = 0d;
		Double installment = 0d;
		Double vatPrincipal = 0d;
		for (InstallmentVO installmentVO : mapInstallmentVO.get(1)) {
			totalInstallmentAmount += installmentVO.getTiamount() + installmentVO.getVatAmount();
			if (installmentVO.getCashflowType() == ECashflowType.CAP || installmentVO.getCashflowType() == ECashflowType.IAP) {
				installment += installmentVO.getTiamount();
				vatPrincipal = installmentVO.getVatAmount();
			}
		}

		result.put(FinancialProductEntityField.TOTAL_INT_AMOUNT, totalInstallmentAmount);
		result.put(FinancialProductEntityField.PRINCILPLE_AMOUNT, installment);
		result.put(FinancialProductEntityField.VAT_PRINCILPLE_AMOUNT, vatPrincipal);
		return result;
	}

	@Override
	public Term getNumberOfTermByNbTerm(Integer nbTerm) {
		BaseRestrictions<Term> restrictionTerm = new BaseRestrictions<>(Term.class);
		restrictionTerm.addCriterion(Restrictions.eq(FinancialProductEntityField.VALUE, nbTerm));
		List<Term> listTerm = dao.list(restrictionTerm);
		return listTerm != null && !listTerm.isEmpty() ? listTerm.get(0) : null;
	}

	@Override
	public Double getInsuranceFee(Double leaseAmount) {
		Double insFee = 0.0d;
		BaseRestrictions<FinService> restrictions = new BaseRestrictions<>(FinService.class);
		restrictions.addCriterion(Restrictions.eq("serviceType", EServiceType.INSFEE));
		List<FinService> finServices = ENTITY_SRV.list(restrictions);
		Double insFeePer = finServices.get(0).getTePrice();

		insFee = ((leaseAmount * insFeePer) / 100);
		return insFee;
	}

	@Override
	public Double getVatInsurance(Double leaseAmount) {
		Double vatInsurance = 0.0d;
		BaseRestrictions<FinService> restrictions = new BaseRestrictions<>(FinService.class);
		restrictions.addCriterion(Restrictions.eq("descEn", VAT_INSUR));
		List<FinService> finServices = ENTITY_SRV.list(restrictions);

		Double insFeePer = finServices.get(0).getTePrice();
		Double tax = finServices.get(0).getVat().getValue();

		vatInsurance = ((leaseAmount * insFeePer) / 100) * tax / 100;

		return vatInsurance;
	}

	@Override
	public Double getVatService(String serviceType) {
		BaseRestrictions<FinService> restrictions = new BaseRestrictions<>(FinService.class);

		restrictions.addCriterion(Restrictions.eq("descEn", serviceType));
		List<FinService> finServices = ENTITY_SRV.list(restrictions);

		Double serviceFee = finServices.get(0).getTePrice();
		Double tax = finServices.get(0).getVat().getValue();
		Double vatInsurance = serviceFee * tax / 100;

		return vatInsurance;
	}

	@Override
	public BaseRestrictions<FinProduct> getFinProductRestrictions() {
		BaseRestrictions<FinProduct> restrictionsFinancial = new BaseRestrictions<>(FinProduct.class);
		restrictionsFinancial.getStatusRecordList().add(EStatusRecord.ACTIV);
		return restrictionsFinancial;
	}
}
