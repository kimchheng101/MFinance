package com.soma.mfinance.core.referential.committee;

import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;

/**
 * Created by Dang Dim
 * Date     : 19-Oct-17, 11:16 AM
 * Email    : d.dim@gl-f.com
 */

@Entity
@Table(name = "tu_committee")
public class Committee extends EntityRefA{

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "com_mit_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Override
    @Column(name = "com_code", nullable = true, length=10, unique = true)
    public String getCode() {
        return super.getCode();
    }

    @Column(name = "comm_mit_name", nullable = true, length=255)
    public String getDesc() {
        return super.getDesc();
    }

    @Override
    @Column(name = "comm_mit_des_eng", nullable = true, length=255)
    public String getDescEn() {
        return super.getDescEn();
    }

}
