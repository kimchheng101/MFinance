package com.soma.mfinance.core.applicant.dao;

import org.seuksa.frmk.dao.BaseEntityDao;

/**
 * Applicant data model access
 * @author kimsuor.seang
 *
 */
public interface ApplicantDao extends BaseEntityDao {

}
