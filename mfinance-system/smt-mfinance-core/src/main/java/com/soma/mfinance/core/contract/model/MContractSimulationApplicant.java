package com.soma.mfinance.core.contract.model;

import org.seuksa.frmk.model.entity.MEntityA;


/**
 * Meta data of com.soma.mfinance.core.contract_old.model.ContractSimulationApplicant
 * @author kimsuor.seang
 */
public interface MContractSimulationApplicant extends MEntityA {
	
	public final static String CONTRACTSIMULATION = "contractSimulation";
	public final static String APPLICANTTYPE = "applicantType";
	public final static String APPLICANT = "applicant";

}
