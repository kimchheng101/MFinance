package com.soma.mfinance.core.address.panel.area;

import com.soma.mfinance.core.address.model.Area;
import com.soma.mfinance.core.custom.component.TwinEntityRefListSelect;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.shared.component.ComponentLayoutFactory;
import com.soma.ersys.core.hr.model.address.Commune;
import com.soma.ersys.core.hr.model.address.District;
import com.soma.ersys.core.hr.model.address.Province;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Area code form panel in collection
 *
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AreaAddressFormPanel extends AbstractFormPanel implements FinServicesHelper {

    /** */
    private static final long serialVersionUID = -1489190810291183140L;

    private Area area;
    private TextField txtDescEn;
    private TextField txtDesc;
    private TextField txtAreaCode;
    private EntityComboBox<Province> cbxProvince;
    private TextArea txtRemark;
    TwinEntityRefListSelect<District> twinDis;
    TwinEntityRefListSelect<Commune> twinCom;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addSaveClickListener(this);
    }

    /**
     * @see AbstractFormPanel#createForm()
     */
    @Override
    protected com.vaadin.ui.Component createForm() {

        cbxProvince = new EntityComboBox<>(Province.class, "descEn");
        cbxProvince.setCaption(I18N.message("province"));
        cbxProvince.setRequired(true);
        cbxProvince.renderer();

        twinCom = new TwinEntityRefListSelect("Commune");
        twinCom.setWidth(200);

        twinDis = new TwinEntityRefListSelect("Districts");
        twinDis.setWidth(200);
        twinDis.addItemChangeListener(new TwinEntityRefListSelect.ItemChangeListener<District>() {
            @Override
            public void onItemAdded(List<District> list) {
                for (District district : list) {
                    BaseRestrictions<Commune> restrictions = new BaseRestrictions(Commune.class);
                    restrictions.addCriterion(Restrictions.eq("district.id", district.getId()));
                    twinCom.getSourceListSelect().addEntities(ENTITY_SRV.list(restrictions));
                }
            }

            @Override
            public void onItemRemoved(List<District> list) {
                for (District district : list) {
                    BaseRestrictions<Commune> restrictions = new BaseRestrictions(Commune.class);
                    restrictions.addCriterion(Restrictions.eq("district.id", district.getId()));
                    List<Commune> communeList = EMPL_SRV.list(restrictions);
                    twinCom.getSourceListSelect().removeEntities(communeList);
                    twinCom.getResultListSelect().removeEntities(communeList);
                }
            }
        });

        txtAreaCode = ComponentFactory.getTextField("area.code", true, 60, 200);
        txtDescEn = ComponentFactory.getTextField("desc.en", true, 60, 200);
        txtDesc = ComponentFactory.getTextField("desc", false, 60, 200);
        txtRemark = ComponentFactory.getTextArea("remark", false, 300, 100);

        /** EVENT SELECT ADDRESS LISTENER */

        cbxProvince.addValueChangeListener(new ValueChangeListener() {
            private static final long serialVersionUID = -572281953646438700L;

            @Override
            public void valueChange(ValueChangeEvent event) {
                if (cbxProvince.getSelectedEntity() != null) {
                    BaseRestrictions restrictions = new BaseRestrictions(District.class);
                    restrictions.addCriterion(Restrictions.eq("province.id", cbxProvince.getSelectedEntity().getId()));
                    twinDis.setRestrictions(restrictions);
                    twinDis.renderer();
                } else {
                    twinDis.getSourceListSelect().clear();
                    twinDis.getResultListSelect().clear();
                    twinCom.getSourceListSelect().clear();
                    twinCom.getResultListSelect().clear();
                }
            }
        });

        FormLayout teamLayout = ComponentLayoutFactory.getFormLayoutCaptionAlignLeft(true);
        teamLayout.addComponent(txtAreaCode);
        teamLayout.addComponent(txtDescEn);
        teamLayout.addComponent(txtDesc);
        teamLayout.addComponent(cbxProvince);
        teamLayout.addComponent(twinDis);
        teamLayout.addComponent(twinCom);
        teamLayout.addComponent(txtRemark);
        Panel mainPanel = ComponentFactory.getPanel();
        mainPanel.setSizeFull();
        mainPanel.setContent(teamLayout);
        return mainPanel;
    }

    /**
     * @see AbstractFormPanel#getEntity()
     */
    @Override
    protected Entity getEntity() {
        area.setCode(txtAreaCode.getValue());
        area.setDescEn(txtDescEn.getValue());
        area.setDesc(txtDesc.getValue());
        area.setProvince(cbxProvince.getSelectedEntity() != null ? cbxProvince.getSelectedEntity() : null);
        area.setDistricts(twinDis.getResultListSelect().getAllEntities());
        area.setCommunes(twinCom.getResultListSelect().getAllEntities());
        area.setRemark(txtRemark.getValue());
        return area;
    }

    /**
     * assign to fields by id
     */
    public void assignValues(Long colAreaCodeID) {
        super.reset();
        if (colAreaCodeID != null) {
            area = ENTITY_SRV.getById(Area.class, colAreaCodeID);
            txtAreaCode.setValue(area.getCode());
            txtDescEn.setValue(area.getDescEn());
            txtDesc.setValue(area.getDesc());
            cbxProvince.setSelectedEntity(area.getProvince() != null ? area.getProvince() : null);
            if (area.getDistricts() != null) {
                twinDis.resultSelectAddEntities(area.getDistricts());
                twinDis.getSourceListSelect().removeEntities(area.getDistricts());
            }
            if (area.getCommunes() != null) {
                for (District district : area.getDistricts()) {
                    BaseRestrictions<Commune> restrictions = new BaseRestrictions<>(Commune.class);
                    restrictions.addCriterion(Restrictions.eq("district.id", district.getId()));
                    twinCom.getSourceListSelect().addEntities(ENTITY_SRV.list(restrictions));
                }
                twinCom.getSourceListSelect().removeEntities(area.getCommunes());
                twinCom.resultSelectAddEntities(area.getCommunes());
            }
            txtRemark.setValue(area.getRemark() != null ? area.getRemark() : "");
        }
    }

    /**
     * @see AbstractFormPanel#reset()
     */
    @Override
    public void reset() {
        super.reset();
        area = new Area();
        txtAreaCode.setValue("");
        txtDescEn.setValue("");
        txtDesc.setValue("");
        txtRemark.setValue("");
        cbxProvince.setSelectedEntity(null);

        markAsDirty();
    }

    /**
     * @see AbstractFormPanel#validate()
     */
    @Override
    protected boolean validate() {
        checkMandatoryField(txtAreaCode, "area.code");
        checkMandatoryField(txtDescEn, "desc.en");
        checkMandatorySelectField(cbxProvince, "province");
        return errors.isEmpty();
    }

}
