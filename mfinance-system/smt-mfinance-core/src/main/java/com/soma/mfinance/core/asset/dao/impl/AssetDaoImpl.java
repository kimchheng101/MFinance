package com.soma.mfinance.core.asset.dao.impl;

import com.soma.mfinance.core.asset.dao.AssetDao;
import org.seuksa.frmk.dao.impl.BaseEntityDaoImpl;
import org.springframework.stereotype.Repository;

/**
 * Asset data access implementation
 *
 * @author kimsuor.seang
 */
@Repository("assetDaoImpl")
public class AssetDaoImpl extends BaseEntityDaoImpl implements AssetDao {

}
