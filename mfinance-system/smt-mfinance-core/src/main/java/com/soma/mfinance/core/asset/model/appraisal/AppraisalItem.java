package com.soma.mfinance.core.asset.model.appraisal;

import javax.persistence.*;

import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.common.reference.model.EComponetType;
import org.apache.commons.lang.StringUtils;
import org.seuksa.frmk.model.EntityFactory;
import org.seuksa.frmk.model.entity.EntityRefA;

/**
 * 
 * @author vi.sok
 */
@Entity
@Table(name = "tu_appraisal_item")
public class AppraisalItem extends EntityRefA {

    private static final long serialVersionUID = -8954187823136477015L;
    
    private AppraisalCategory appraisalCategory;
    private Double value;
    private Boolean isMandetory ;
    private EComponetType componetType;
	private Boolean isVisible;
	private Double coefficient;
    private AppraisalGroup appraisalGroup;
    private Double weightCoefficient;
    private AssetRange  assetRange;
    private EAppraisalRange eAppraisalRange;
    private Double fixPrice; //will remove
    private Double changePrice; //will remove

    public static AppraisalItem createInstance() {
    	AppraisalItem instance = EntityFactory.createInstance(AppraisalItem.class);
        return instance;
    }

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "app_item_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Override
    @Column(name = "app_item_code", nullable = true, length=10)
    public String getCode() {
        return super.getCode();
    }

    @Override
    @Column(name = "app_item_desc", nullable = true, length=255)
    public String getDesc() {
        return super.getDesc();
    }

    @Override
    @Column(name = "app_item_desc_en", nullable = false, length=255)
    public String getDescEn() {
        return super.getDescEn();
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "app_cate_id")
	public AppraisalCategory getAppraisalCategory() {
		return appraisalCategory;
	}
	public void setAppraisalCategory(AppraisalCategory appraisalCategory) {
		this.appraisalCategory = appraisalCategory;
	}

	@Column(name = "app_item_val")
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}

    @Column(name = "app_item_mandetory", nullable = true)
    public Boolean getMandetory() {
        return isMandetory;
    }
    public void setMandetory(Boolean mandetory) {
        isMandetory = mandetory;
    }

    @Column(name = "app_com_type_id", nullable = true)
    @Convert(converter = EComponetType.class)
    public EComponetType getComponetType() {
        return componetType;
    }
    public void setComponetType(EComponetType componetType) {
        this.componetType = componetType;
    }

	@Column(name = "app_item_visible")
	public Boolean getVisible() {
		return isVisible;
	}
	public void setVisible(Boolean visible) {
		isVisible = visible;
	}

	@Column(name = "app_item_coefficient")
	public Double getCoefficient() {
		return coefficient;
	}
	public void setCoefficient(Double coefficient) {
		this.coefficient = coefficient;
	}

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "app_group_id")
    public AppraisalGroup getAppraisalGroup() {
        return appraisalGroup;
    }
    public void setAppraisalGroup(AppraisalGroup appraisalGroup) {
        this.appraisalGroup = appraisalGroup;
    }

    @Column(name = "app_item_weight_coeff")
    public Double getWeightCoefficient() {
        return weightCoefficient;
    }
    public void setWeightCoefficient(Double weightCoefficient) {
        this.weightCoefficient = weightCoefficient;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ass_ran_id", nullable = true)
    public AssetRange getAssetRange() {return assetRange;}
    public void setAssetRange(AssetRange assetRange) {this.assetRange = assetRange;}

    @Column(name = "app_appraisal_range", nullable = true)
    @Convert(converter = EAppraisalRange.class)
    public EAppraisalRange geteAppraisalRange() {return eAppraisalRange;}
    public void seteAppraisalRange(EAppraisalRange eAppraisalRange) {this.eAppraisalRange = eAppraisalRange;}

    @Column(name = "ass_app_item_fix_price", nullable = true)
    public Double getFixPrice() {return fixPrice;}
    public void setFixPrice(Double fixPrice) {this.fixPrice = fixPrice;}

    @Column(name = "ass_app_item_chgage_price", nullable = true)
    public Double getChangePrice() {return changePrice;}
    public void setChangePrice(Double changePrice) {this.changePrice = changePrice;}

    @Transient
    public Double getItemWieght(String inputValue,boolean isDream) {
        Double wieght = 0D;
        Double value = 1D;
        if(!StringUtils.isEmpty(inputValue)) {
            value = Double.valueOf(Double.parseDouble(inputValue));
        }
        wieght = value  * weightCoefficient;
        if(!isDream && appraisalGroup != null) {
            wieght = (value/10)  * weightCoefficient;
        }
        return  wieght;
    }
}
