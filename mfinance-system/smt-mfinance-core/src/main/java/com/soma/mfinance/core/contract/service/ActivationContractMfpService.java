package com.soma.mfinance.core.contract.service;

import com.soma.mfinance.core.asset.model.AssetModel;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.quotation.model.Quotation;
import org.seuksa.frmk.service.BaseEntityService;
import org.seuksa.frmk.tools.exception.EntityCreationException;

import java.util.Date;
import java.util.List;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/21/2017
 * Name  : 2:46 PM
 * Email : k.seang@gl-f.com
 */
public interface ActivationContractMfpService extends BaseEntityService {

    /**
     * @param quotation
     * @return
     */
    Contract createContract(Quotation quotation) throws EntityCreationException;

    /**
     * @param quotation
     * @return
     */
    Contract activateContract(Quotation quotation) throws EntityCreationException;

    /**
     * Complete contract_old
     * @param contract
     * @param isForced
     * @return
     */
    Contract complete(Contract contract, boolean isForced, boolean disburse) throws DealerPaymentException, JournalEntryException ;




   /**
     *
     * @param errors
     * @param chassisNo
     * @param engineNo
     * @param contract
     * @return
     */
    List<String> validation(Contract contract, Date firstDueDate, String chassisNo, String engineNo, String taxInvoiceNumber);

}
