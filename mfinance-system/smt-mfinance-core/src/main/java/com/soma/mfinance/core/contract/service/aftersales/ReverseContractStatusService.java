package com.soma.mfinance.core.contract.service.aftersales;

import org.seuksa.frmk.service.BaseEntityService;

/**
 * Reverse the contract_old status to activated
 * 
 * @author meng.kim
 *
 */
public interface ReverseContractStatusService extends BaseEntityService {

	/**
	 * @param request
	 * @return
	 */
	ReverseContractStatusValidateResponse validate(ReverseContractStatusValidateRequest request);
	
}
