package com.soma.mfinance.core.document.service;

import java.util.List;

import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.contract.model.ContractDocument;
import com.soma.mfinance.core.document.model.Document;
import com.soma.mfinance.core.document.model.DocumentGroup;
import com.soma.mfinance.core.quotation.model.QuotationDocument;
import com.soma.mfinance.core.shared.exception.ValidationFieldsException;

/**
 * Customer service interface
 * @author kimsuor.seang
 *
 */
public interface DocumentService extends BaseEntityService {

	/**
	 * Check documents
	 * @param guarantorRequired
	 * @param quotationDocuments
	 * @throws ValidationFieldsException
	 */
	void checkDocuments(boolean guarantorRequired, List<QuotationDocument> quotationDocuments) throws ValidationFieldsException;

	/**
	 * Get document by code
	 * @param appType
	 * @param docCode
	 * @return
	 */
	Document getDocumentByCode(EApplicantType appType, String docCode);
	
	/**
	 * Get list documents
	 * @return
	 */
	List<Document> getDocuments();
	
	/**
	 * Get list documents by group
	 * @param code
	 * @return
	 */
	List<Document> getDocumentByGroups(String code);
	
	/**
	 * Get list document groups
	 * @return
	 */
	List<DocumentGroup> getDocumentGroups();
	
	/**
	 * Get list documents by contract_old
	 * @param contraId
	 * @return
	 */
	List<ContractDocument> getDocumentsByContract(Long contraId);

	/**
	 *
	 * @param quoId
	 * @param docCode
	 * @param applicantType
	 * @return
	 */
	QuotationDocument getQuotationDocumentByDocCode(String docCode,List<QuotationDocument> quotationDocuments);
}
