package com.soma.mfinance.core.accounting.dto;

import org.seuksa.frmk.model.entity.Entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author by kimsuor.seang  on 12/2/2017.
 */
public class RemainingBalanceDTO implements Entity , Serializable {

    private Long id;
    private String reference;
    private Date contractStartDate;
    private Date firstInstallmentDate;

    private String lastNameEn;
    private String firstNameEn;
    private Double interestRate;
    private Double irrRate;

    private Double interestRevenue;
    private Double interestIncome;
    private Double principalRepayment;

    private Double principalBalance;
    private Double interestReceivable;
    private Double unearnedInterestBalance;
    private Double interestInSuspend;
    private Double penalty;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Date getContractStartDate() {
        return contractStartDate;
    }

    public void setContractStartDate(Date contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    public Date getFirstInstallmentDate() {
        return firstInstallmentDate;
    }

    public void setFirstInstallmentDate(Date firstInstallmentDate) {
        this.firstInstallmentDate = firstInstallmentDate;
    }

    public String getLastNameEn() {
        return lastNameEn;
    }

    public void setLastNameEn(String lastNameEn) {
        this.lastNameEn = lastNameEn;
    }

    public String getFirstNameEn() {
        return firstNameEn;
    }

    public void setFirstNameEn(String firstNameEn) {
        this.firstNameEn = firstNameEn;
    }

    public Double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(Double interestRate) {
        this.interestRate = interestRate;
    }

    public Double getIrrRate() {
        return irrRate;
    }

    public void setIrrRate(Double irrRate) {
        this.irrRate = irrRate;
    }

    public Double getInterestRevenue() {
        return interestRevenue;
    }

    public void setInterestRevenue(Double interestRevenue) {
        this.interestRevenue = interestRevenue;
    }

    public Double getInterestIncome() {
        return interestIncome;
    }

    public void setInterestIncome(Double interestIncome) {
        this.interestIncome = interestIncome;
    }

    public Double getPrincipalRepayment() {
        return principalRepayment;
    }

    public void setPrincipalRepayment(Double principalRepayment) {
        this.principalRepayment = principalRepayment;
    }

    public Double getPrincipalBalance() {
        return principalBalance;
    }

    public void setPrincipalBalance(Double principalBalance) {
        this.principalBalance = principalBalance;
    }

    public Double getInterestReceivable() {
        return interestReceivable;
    }

    public void setInterestReceivable(Double interestReceivable) {
        this.interestReceivable = interestReceivable;
    }

    public Double getUnearnedInterestBalance() {
        return unearnedInterestBalance;
    }

    public void setUnearnedInterestBalance(Double unearnedInterestBalance) {
        this.unearnedInterestBalance = unearnedInterestBalance;
    }

    public Double getInterestInSuspend() {
        return interestInSuspend;
    }

    public void setInterestInSuspend(Double interestInSuspend) {
        this.interestInSuspend = interestInSuspend;
    }

    public Double getPenalty() {
        return penalty;
    }

    public void setPenalty(Double penalty) {
        this.penalty = penalty;
    }
}
