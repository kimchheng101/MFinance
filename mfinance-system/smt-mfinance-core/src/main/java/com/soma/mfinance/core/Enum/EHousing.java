package com.soma.mfinance.core.Enum;

import org.seuksa.frmk.model.eref.BaseERefData;

import javax.persistence.AttributeConverter;
import java.util.List;

/**
 * Created by b.chea on 2/25/2017.
 */
public class EHousing extends BaseERefData implements AttributeConverter<EHousing, Long> {
    public static final EHousing TEST = new EHousing("CURRENT_ADDRESS", 1L);

    public EHousing(){

    }

    public  EHousing(String code, long id){
        super(code , id);
    }

    @Override
    public Long convertToDatabaseColumn(EHousing arg0) {
        return super.convertToDatabaseColumn(arg0);
    }

    @Override
    public EHousing convertToEntityAttribute(Long dbData) {
        return (EHousing) super.convertToEntityAttribute(dbData);
    }

    public static List<EHousing> values() {
        return getValues(EHousing.class);
    }

    public static EHousing getByCode(String code) {
        return (EHousing) getByCode(EHousing.class, code);
    }

    public static EHousing getById(long id) {
        return (EHousing) getById(EHousing.class, Long.valueOf(id));
    }
}
