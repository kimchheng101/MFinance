package com.soma.mfinance.core.asset.model.appraisal;

import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * @author by kimsuor.seang  on 11/6/2017.
 */
@MappedSuperclass
public abstract class AbstractAppraisal extends EntityA {

    //ONLY KUBUTA
    private Double fixPrice;
    private Double changePrice;
    private Double adjustValue;


    @Column(name = "apr_fix_price", nullable = true)
    public Double getFixPrice() {
        return fixPrice;
    }
    public void setFixPrice(Double fixPrice) {
        this.fixPrice = fixPrice;
    }

    @Column(name = "apr_chgage_price", nullable = true)
    public Double getChangePrice() {
        return changePrice;
    }
    public void setChangePrice(Double changePrice) {
        this.changePrice = changePrice;
    }

    @Column(name = "apr_adj_value", nullable = true)
    public Double getAdjustValue() {return adjustValue;}
    public void setAdjustValue(Double adjustValue) {
        this.adjustValue = adjustValue;
    }
}
