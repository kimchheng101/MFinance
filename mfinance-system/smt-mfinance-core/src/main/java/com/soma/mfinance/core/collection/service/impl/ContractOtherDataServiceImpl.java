package com.soma.mfinance.core.collection.service.impl;

import com.soma.mfinance.core.accounting.InstallmentVO;
import com.soma.mfinance.core.accounting.MultiAmountTypeVO;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.collection.model.EColResult;
import com.soma.mfinance.core.collection.service.ContractOtherDataService;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.ContractApplicant;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.contract.service.ContractService;
import com.soma.mfinance.core.payment.model.Payment;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.contract.ContractEntityField;
import com.soma.mfinance.core.shared.mplus.accounting.InstallmentServiceMfp;
import com.soma.mfinance.core.workflow.CollectionWkfStatus;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.applicant.model.Individual;
import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.model.meta.NativeColumn;
import org.seuksa.frmk.model.meta.NativeRow;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.amount.Amount;
import org.seuksa.frmk.tools.exception.NativeQueryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.soma.mfinance.core.helper.FinServicesHelper.INSTALLMENT_SERVICE_MFP;


/**
 * Calculate Batch for Overdue contract
 * @author kimsuor.seang
 */
@Service("contractOtherDataService")
@Transactional
public class ContractOtherDataServiceImpl extends BaseEntityServiceImpl implements ContractOtherDataService, ContractEntityField {

    /** */
    private static final long serialVersionUID = 8637497135643835740L;

    private Logger LOG = LoggerFactory.getLogger(ContractOtherDataServiceImpl.class);

    @Autowired
    private EntityDao dao;


    @Autowired
    private ContractService contractService;

    @Autowired
    private InstallmentServiceMfp installmentServiceMfp;

    @Override
    public EntityDao getDao() {
        return dao;
    }

    @Override
    public void calculateOtherDataContracts() {

        ConcurrentLinkedDeque<Long> contractIds = new ConcurrentLinkedDeque<>(getContracts());

        System.out.println("Start");
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 5; i++) {
            executorService.execute(new CollectionDataProcessor(i + 1, contractIds));
        }
        executorService.shutdown();
        try {
            System.out.println("Waiting for Thread completion");
            executorService.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Calculate the data of contract
     * @param contract
     */
    public Contract calculateOtherDataContract(Contract contract) {
        int nbInstallment = contract.getLastPaidNumInstallment();
        List<InstallmentVO> nextInstallmentVOList = null;
        Date installmentDate = null;
        Date calculationDate = DateUtils.todayH00M00S00();
        EColResult defaultColResult =  getByCode(EColResult.class,"001");
        if (nbInstallment < contract.getTerm()) {
            nextInstallmentVOList = installmentServiceMfp.getInstallmentVOs(contract, nbInstallment + 1);
            installmentDate = contractService.getInstallmentDateByInstallmentVOs(nextInstallmentVOList);
        } else {
            nextInstallmentVOList = installmentServiceMfp.getInstallmentVOs(contract, nbInstallment);
            installmentDate = contract.getLastPaidDateInstallment();
        }

        Map<Integer, List<InstallmentVO>> mapInstallmentVO = installmentServiceMfp.getInstallmentVOs(contract);

        if (installmentDate != null && calculationDate.after(installmentDate)) {
            List<Cashflow> cashflowsNotConcel = contractService.getCashflowsNoCancel(contract.getId());
            int nbOverdueInDays = contractService.getNbOverdueInDays(calculationDate, nextInstallmentVOList, 0);
            InstallmentVO nextInstallmentVo = null;
            if (nextInstallmentVOList != null && nextInstallmentVOList.size() > 0) {
                nextInstallmentVo = nextInstallmentVOList.get(0);
            }
            Collection collection = contract.getCollection();
            if (collection == null) {
                collection = Collection.createInstance();
                collection.setColResult(defaultColResult);
                collection.setWkfStatus(CollectionWkfStatus.NEW);
                collection.setContract(contract);
            }
            if(collection.getColResult() == null) {
                collection.setColResult(defaultColResult);
            }
            if (nbOverdueInDays > 0) {
                contract.setOverdue(true);
            } else {
                contract.setOverdue(false);
            }
            if (nbOverdueInDays > 0 && nbOverdueInDays <= 30) {
                collection.setNbInstallmentsInOverdue0030(1);
            } else if (nbOverdueInDays > 30 && nbOverdueInDays <= 60) {
                collection.setNbInstallmentsInOverdue3160(1);
            } else if (nbOverdueInDays > 60 && nbOverdueInDays <= 90) {
                collection.setNbInstallmentsInOverdue6190(1);
            } else if (nbOverdueInDays > 90) {
                collection.setNbInstallmentsInOverdue91XX(1);
            }
            collection.setNbOverdueInDays(nbOverdueInDays);

            // collection.setDebtLevel(contractService.getDebtLevel(DateUtils.todayH00M00S00(), cashflowsNotConcel));   it's not work coz cashflowsNotConcel == null
            collection.setNbInstallmentsInOverdue(contract.getLastPaidNumInstallment() + 1);

            if (contract.getWkfSubStatus() != null
                    && (contract.getWkfSubStatus().equals(ContractWkfStatus.NORMAL)
                    || contract.getWkfSubStatus().equals(ContractWkfStatus.PAST_DUE_OVER_90_DAYS))) {
                if (nbOverdueInDays < 90) {
                    contract.setWkfSubStatus(ContractWkfStatus.NORMAL);
                } else {
                    contract.setWkfSubStatus(ContractWkfStatus.PAST_DUE_OVER_90_DAYS);
                }
                // collection.setWkfStatus(contract.getWkfSubStatus());   It doesn't work now coz contract.getWkfSubStatus() == null
            }

            Integer dueDay = null;
            if (contract.getFirstDueDate() != null) {
                dueDay = DateUtils.getDay(contract.getFirstDueDate());
            }
            collection.setDueDay(dueDay);
            collection.setLastNumInstallmentPaid(nbInstallment);
            collection.setNbInstallmentsPaid(nbInstallment);
            collection.setNextDueDate(installmentDate);
            int contractTerm = MyNumberUtils.getInteger(contract.getTerm());
            int lastNumInstallmentPaid = MyNumberUtils.getInteger(collection.getLastNumInstallmentPaid());
            if (contractTerm == lastNumInstallmentPaid) {
                collection.setNextDueDate(null);
            } else {
                if (nextInstallmentVo != null) {
                    collection.setNextDueDate(nextInstallmentVo.getInstallmentDate());
                }
            }

            // Do next time should be ask for calculation
            // double[] typeNbInstallmentPaid = getPartialORFullInstallmentsPaid(cashflowsNotConcel);
            // collection.setPartialPaidInstallment(typeNbInstallmentPaid[0]);
            // collection.setNbInstallmentsPaid((int) typeNbInstallmentPaid[1]);
            collection.setCurrentTerm(contractTerm);
            Payment lastPayment = contractService.getLastPayment(contract.getId());
            if (lastPayment != null) {
                collection.setLastPaidPaymentMethod(lastPayment.getPaymentMethod());
                collection.setLastPaymentDate(lastPayment.getPaymentDate());
                collection.setTiLastPaidAmount(lastPayment.getTiPaidAmount());
                collection.setTeLastPaidAmount(lastPayment.getTePaidAmount());
                collection.setVatLastPaidAmount(lastPayment.getVatPaidAmount());
            }

            Amount totalAmountUnPaid=new Amount();
            Amount totalPenaltyAmountInOverdue = new Amount();//OLD code: getTotalPenaltyAmountInOverdueUsd(contract, calculationDate, installmentDate, 0);
            //TODO: getTotalAmountAndPenaltyNotPaid(contract) is return Map<Amount,Amount>, First Amount (Key):totalInstallmentNotPaid and Second Amount (Value): totalPenaltyNotPaid
            Map<Amount,Amount> totalAmountAndPenaltyNotPaids = getTotalAmountAndPenaltyNotPaid(contract);
            for(Map.Entry m:totalAmountAndPenaltyNotPaids.entrySet()){
                totalAmountUnPaid= (Amount) m.getKey();
                totalPenaltyAmountInOverdue=(Amount) m.getValue();
            }
            collection.setTeTotalAmountNotPaid(totalAmountUnPaid.getTeAmount());
            collection.setVatTotalAmountNotPaid(totalAmountUnPaid.getVatAmount());
            collection.setTiTotalAmountNotPaid(totalAmountUnPaid.getTiAmount());

            collection.setDealer(contract.getDealer().getNameEn());
            /*Amount totalAmountInOverdue = getTotalAmountInOverdueUsd(contract, calculationDate, nextInstallmentVOList, installmentDate, 0);
            collection.setTeTotalAmountInOverdue(totalAmountInOverdue.getTeAmount());
            collection.setVatTotalAmountInOverdue(totalAmountInOverdue.getVatAmount());
            collection.setTiTotalAmountInOverdue(totalAmountInOverdue.getTiAmount());*/

            Amount unearnInterest=INSTALLMENT_SERVICE_MFP.getUnearnInterestOfReprocess(contract,calculationDate);
            collection.setTiUnEarnInterest(unearnInterest.getTiAmount());
            collection.setTeUnEarnInterest(unearnInterest.getTeAmount());
            collection.setVatUnEarnInterest(unearnInterest.getVatAmount());

            collection.setTePenaltyAmount(totalPenaltyAmountInOverdue.getTeAmount());
            collection.setVatPenaltyAmount(totalPenaltyAmountInOverdue.getVatAmount());
            collection.setTiPenaltyAmount(totalPenaltyAmountInOverdue.getTiAmount());

            Amount balanceCapital = contractService.getRealOutstandingByInstallmentVOs(contract, calculationDate, mapInstallmentVO);

            collection.setTiBalanceCapital(balanceCapital.getTiAmount());
            collection.setTeBalanceCapital(balanceCapital.getTeAmount());
            collection.setVatBalanceCapital(balanceCapital.getVatAmount());

            ContractApplicant contractApplicant = contract.getContractApplicant(EApplicantType.C);
            if(contractApplicant != null ){
                Applicant custApp = contractApplicant.getApplicant();


                Individual individual = custApp.getIndividual();
                if(custApp != null && individual != null) {
                    if(individual.getLastNameEn() != null && individual.getFirstNameEn() != null) {
                        collection.setCustomer(individual.getLastNameEn() +" "+individual.getFirstNameEn());
                    }
                    collection.setLeaseMobilePhone1(individual.getMobilePerso() != null ? individual.getMobilePerso() : "");
                    collection.setLeaseMobilePhone2(individual.getMobilePerso2() != null ? individual.getMobilePerso2() : "");
                    collection.setPlaceofbirth(individual.getPlaceOfBirth() != null  ? individual.getPlaceOfBirth().getDescEn() : "");
                    com.soma.ersys.core.hr.model.address.Address address = custApp.getIndividual().getMainAddress();
                    if(address != null) {
                        collection.setProvince(address.getProvince());
                        collection.setDistrict(address.getDistrict());
                        collection.setCommune(address.getCommune());
                        collection.setVillage(address.getVillage());
                    }
                }
            }
            if (contract.getGuarantor() != null && contract.getGuarantor().getIndividual() != null ) {
                collection.setGuarantorMobilePhone(contract.getGuarantor().getIndividual().getMobilePerso() != null ? contract.getGuarantor().getIndividual().getMobilePerso() : "");
            }
            Quotation quotation = contract.getQuotation();
            collection.setCreditOfficer(quotation.getCreditOfficer());
            saveOrUpdate(contract);
            saveOrUpdate(collection);
            return contract;
        } else {
            return contract;
        }
    }

    /**
     * Get activated contract
     * @return
     */
    private List<Long> getContracts() {
        List<Long> contracts = new ArrayList<>();
        String query =
                "SELECT "
                        + " c.con_id "
                        + " FROM td_contract c"
                        + " WHERE c.wkf_sta_id in (" + ContractWkfStatus.FIN.getId() + ")"
                        + " ORDER BY c.con_dt_start asc";
        try {
            List<NativeRow> contractRows = executeSQLNativeQuery(query);
            for (NativeRow row : contractRows) {
                List<NativeColumn> columns = row.getColumns();
                int i = 0;
                Long cotraId = (Long) columns.get(i++).getValue();
                contracts.add(cotraId);
            }
        } catch (NativeQueryException e) {
            LOG.error(e.getMessage(), e);
        }
        return contracts;
    }


    @Override
    @SuppressWarnings("unchecked")
    public Map<Amount,Amount>getTotalAmountAndPenaltyNotPaid(Contract contract) {
        Map totalAmountAndPenaltyNotPaid=new HashMap();
        Amount totalAmountUnPaidUsd=new Amount();
        Amount totalPenaltyAmountInOverdueUsd=new Amount();
        if (contract != null) {
            int term = contract.getTerm();
            int firstInstallmentNotPaid = contract.getLastPaidNumInstallment() + 1;
            for (int i = firstInstallmentNotPaid; i <= term; i++) {
                List<InstallmentVO> installmentByNumInstallments = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(contract, i);
                MultiAmountTypeVO multiAmountTypeVO = INSTALLMENT_SERVICE_MFP.getEachAmountTypes(contract, installmentByNumInstallments);
                if(multiAmountTypeVO==null){
                    continue;
                }
                totalAmountUnPaidUsd.plusTiAmount(multiAmountTypeVO.getTotalInstallment());
                totalAmountUnPaidUsd.plusTeAmount(multiAmountTypeVO.getTotalInstallment());
                totalAmountUnPaidUsd.plusVatAmount(multiAmountTypeVO.getVatTotalInstallment());

                totalPenaltyAmountInOverdueUsd.plusTiAmount(multiAmountTypeVO.getPenaltyAmount());
                totalPenaltyAmountInOverdueUsd.plusTeAmount(multiAmountTypeVO.getPenaltyAmount());
                totalPenaltyAmountInOverdueUsd.plusVatAmount(multiAmountTypeVO.getVatPenaltyAmount());

            }
        }
        totalAmountAndPenaltyNotPaid.put(totalAmountUnPaidUsd,totalPenaltyAmountInOverdueUsd);
        return totalAmountAndPenaltyNotPaid;

    }
}
