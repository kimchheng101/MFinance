package com.soma.mfinance.core.quotation.impl;

import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.financial.model.FinProduct;
import com.soma.mfinance.core.quotation.SequenceGenerator;

/**
 * Quotation reference number generator
 * @author kimsuor.seang
 */
public class QuotationReferenceGeneratorImpl implements SequenceGenerator {

	private String year;
	private Long sequence;
	private FinProduct finProduct;
	private Dealer dealer;
	public static final String DEFAULT_PREFIX = "GLF";
	
	/**
	 * 
	 * @param dealer
	 * @param type
	 * @param sequence
	 */
	public QuotationReferenceGeneratorImpl(FinProduct dealer, String type, Long sequence) {
		this.finProduct = dealer;
		this.year = type;
		this.sequence = sequence;
	}

	/**
	 *
	 * @param dealer
	 * @param type
	 * @param sequence
	 */
	public QuotationReferenceGeneratorImpl(Dealer dealer,FinProduct finProduct, String type, Long sequence) {
		this.dealer = dealer;
		this.year = type;
		this.sequence = sequence;
		this.finProduct = finProduct;
	}
	/**
	 * @see com.soma.mfinance.core.quotation.SequenceGenerator#generate()
	 */
	@Override
	public String generate() {
		String frefixDigit = "7";
		String sequenceNumber = "0000000" + sequence;
		sequenceNumber = sequenceNumber.substring(sequenceNumber.length() - 7);
		/*if (FIN_CODE_PUBLIC.equals(finProduct.getCode())) {
			frefixDigit = "77";
			sequenceNumber = "000000" + sequence;
			sequenceNumber = sequenceNumber.substring(sequenceNumber.length() - 6);
		}*/
		return DEFAULT_PREFIX + "-" + dealer.getInternalCode() + "-" + frefixDigit + sequenceNumber;
	}
}
