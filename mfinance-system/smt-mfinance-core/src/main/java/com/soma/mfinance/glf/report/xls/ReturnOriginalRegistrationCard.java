package com.soma.mfinance.glf.report.xls;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.QuotationService;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationDocument;
import com.soma.mfinance.core.shared.conf.AppConfig;
import com.soma.mfinance.core.shared.report.ReportParameter;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.mfinance.glf.report.model.CustomerState;
import com.soma.mfinance.glf.report.model.DealerState;
import com.soma.mfinance.glf.report.model.EReportTamplate;
import com.soma.mfinance.glf.report.model.ReportNumberUtil;
import com.soma.mfinance.tools.report.Report;
import com.soma.ersys.core.hr.model.address.Address;
import com.soma.ersys.core.hr.model.eref.EGender;
import com.soma.frmk.security.context.SecApplicationContextHolder;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.joda.time.Years;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.model.entity.EntityA;
import org.seuksa.frmk.tools.DateUtils;

import java.util.*;

/**
 * Created by Dang Dim
 * Date     : 21-Jul-17, 9:21 AM
 * Email    : d.dim@gl-f.com
 */
public class ReturnOriginalRegistrationCard implements Report, FinServicesHelper {

    protected QuotationService quotationService = (QuotationService) SecApplicationContextHolder.getContext().getBean("quotationService");
    public static String NA = "N/A";

    @Override
    public String generate(ReportParameter reportParameter) throws Exception {
        Long quotaId = (Long) reportParameter.getParameters().get("quotaId");
        Quotation quotation = quotationService.getById(Quotation.class, quotaId);
        Applicant applicant = quotation.getApplicant();
        Asset asset = quotation.getAsset();

        /* start report id */
        BaseRestrictions<EntityA> restrictions = null;
        List<EntityA> list = new ArrayList<>();
        DealerState dealerState = new DealerState();
        CustomerState customerState = new CustomerState();
        int number = 1;

        restrictions = new BaseRestrictions(CustomerState.class);
        restrictions.addCriterion(Restrictions.eq("quotation.id", quotaId));
        restrictions.addCriterion(Restrictions.eq("wkfStatus", QuotationWkfStatus.ACT));
        list = ENTITY_SRV.list(restrictions);

        if (!list.isEmpty()) {
            customerState = (CustomerState) list.get(0);
            number = Integer.valueOf(customerState.getState().toString());
        } else {
            restrictions = new BaseRestrictions(DealerState.class);
            restrictions.addCriterion(Restrictions.eq("dealer.id", quotation.getDealer().getId()));
            restrictions.addCriterion(Restrictions.eq("reportTamplate", EReportTamplate.ROR));
            list = ENTITY_SRV.list(restrictions);
            if (!list.isEmpty()) {
                dealerState = (DealerState) list.get(0);
                number = Integer.valueOf(dealerState.getStateNumber().toString());
            }
        }

        String reportNumber = ReportNumberUtil.generateNumber(quotation.getDealer(), EReportTamplate.ROR, number);

        Map<String, String> rorCard = new HashMap<String, String>();

        Date contractStartDate = quotation.getContractStartDate();
        rorCard.put("numReport", reportNumber);
        rorCard.put("contractNumber", quotation.getReference());
        rorCard.put("dateofContract", (DateUtils.getDateLabel(contractStartDate, DateUtils.FORMAT_DDMMYYYY_SLASH)));

        rorCard.put("lesseeName", applicant.getIndividual().getLastName() + " " + applicant.getIndividual().getFirstName());
        rorCard.put("sex", getGenderDesc(applicant.getIndividual().getGender().getCode()));
        rorCard.put("dateOfBirth", DateUtils.getDateLabel(applicant.getIndividual().getBirthDate(), DateUtils.FORMAT_DDMMYYYY_SLASH));
        rorCard.put("age", getAgeByBirthDate(applicant.getIndividual().getBirthDate()));

        QuotationDocument idCardDocument = getDocument("N", quotation.getQuotationDocuments());
        rorCard.put("idNumber", idCardDocument != null ? getDefaultString(idCardDocument.getReference()) : NA);
        rorCard.put("dateOfIssue", idCardDocument != null ? DateUtils.getDateLabel(idCardDocument.getIssueDate(), DateUtils.FORMAT_DDMMYYYY_SLASH) : NA);
        rorCard.put("phonenumber", getDefaultString(applicant.getIndividual().getMobilePerso()));

        Address address = applicant.getIndividual().getMainAddress();
        if (address != null) {
            rorCard.put("houseNo", StringUtils.isNotEmpty(address.getHouseNo()) ? address.getHouseNo() : NA);
            rorCard.put("streetNo", StringUtils.isNotEmpty(address.getStreet()) ? address.getStreet() : NA);
            rorCard.put("village", address.getVillage() != null ? address.getVillage().getDesc() : NA);
            rorCard.put("commune", address.getCommune() != null ? address.getCommune().getDesc() : NA);
            rorCard.put("district", address.getDistrict() != null ? address.getDistrict().getDesc() : NA);
            rorCard.put("province", address.getProvince() != null ? address.getProvince().getDesc() : NA);
        } else {
            rorCard.put("houseNo", NA);
            rorCard.put("streetNo", NA);
            rorCard.put("village", NA);
            rorCard.put("commune", NA);
            rorCard.put("district", NA);
            rorCard.put("province", NA);
        }

        rorCard.put("model", asset.getModel().getDescEn());
        rorCard.put("color", asset.getColor().getDesc());
        rorCard.put("type", getGenderDesc(asset.getAssetGender().getCode()));
        rorCard.put("registrationDate", DateUtils.getDateLabel(asset.getRegistrationDate(), DateUtils.FORMAT_DDMMYYYY_SLASH));

        rorCard.put("numberOfCC", getDefaultString(asset.getEngine().getDescEn()));
        rorCard.put("chassisNumber", asset.getChassisNumber());
        rorCard.put("engineNumber", asset.getEngineNumber());
        rorCard.put("plateNumber", asset.getPlateNumber());
        Date printDate = new Date();
        rorCard.put("printDate", DateUtils.getDateLabel(printDate, DateUtils.FORMAT_DDMMYYYY_SLASH));

        String templatePath = AppConfig.getInstance().getConfiguration().getString("specific.templatedir");
        String templateFileName = templatePath + "/ReturnOriginalRegistrationCard.xlsx";
        String outputPath = AppConfig.getInstance().getConfiguration().getString("specific.tmpdir");

        String prefixOutputName = "ReturnOriginalRegistrationCard";
        String sufixOutputName = "xlsx";
        String uuid = UUID.randomUUID().toString().replace("-", "");
        String xlsFileName = outputPath + "/" + prefixOutputName + uuid + "." + sufixOutputName;
        XLSTransformer transformer = new XLSTransformer();
        transformer.transformXLS(templateFileName, rorCard, xlsFileName);

        if (dealerState.getId() != null) {
            dealerState.setStateNumber(dealerState.getStateNumber() + 1);
            ENTITY_SRV.saveOrUpdate(dealerState);
        } else if (customerState.getId() == null) {
            dealerState.setStateNumber(new Long(number + 1));
            dealerState.setReportTamplate(EReportTamplate.ROR);
            dealerState.setDealer(quotation.getDealer());
            ENTITY_SRV.saveOrUpdate(dealerState);
        }

        if (customerState.getId() == null) {
            customerState.setQuotation(quotation);
            customerState.setState(dealerState.getStateNumber() - 1);
            customerState.setWkfStatus(quotation.getWkfStatus());
            ENTITY_SRV.saveOrUpdate(customerState);
        }

        return prefixOutputName + uuid + "." + sufixOutputName;
    }

    private QuotationDocument getDocument(String documCode, List<QuotationDocument> documents) {
        for (QuotationDocument document : documents) {
            if (document.getDocument().getApplicantType().equals(EApplicantType.C)
                    && documCode.equals(document.getDocument().getCode())) {
                return document;
            }
        }
        return null;
    }

    private String getAgeByBirthDate(Date birthDate) {
        long timestamp = birthDate.getTime();
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);
        Integer y = cal.get(Calendar.YEAR);
        Integer moy = cal.get(Calendar.MONTH) + 1;
        Integer dom = cal.get(Calendar.DAY_OF_MONTH);
        org.joda.time.LocalDate birthdate = new org.joda.time.LocalDate(y, moy, dom);
        Years age = Years.yearsBetween(birthdate, org.joda.time.LocalDate.now());
        return String.format("%s", age.getYears());
    }

    private String getDefaultString(String value) {
        return StringUtils.defaultString(value);
    }

    /*retrurn code*/
    private String getGenderDesc(String code) {
        if (EGender.M.getCode().equals(code)) {
            return "ប្រុស";
        } else if (EGender.F.getCode().equals(code)) {
            return "ស្រី";
        } else if (EGender.U.getCode().equals(code)) {
            return "មិនដឹង";
        }
        return "";
    }
}
