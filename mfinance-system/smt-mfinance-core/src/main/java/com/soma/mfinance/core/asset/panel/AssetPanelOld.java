package com.soma.mfinance.core.asset.panel;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.RefDataId;

import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.asset.model.AssetMake;
import com.soma.mfinance.core.asset.model.AssetModel;
import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.asset.model.MAsset;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.MContract;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.ersys.core.hr.model.eref.EColor;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/**
 * Asset detail panel
 * @author kimsuor.seang
 */
public class AssetPanelOld extends AbstractTabPanel implements MAsset, MContract, FinServicesHelper {

	/** */
	private static final long serialVersionUID = -6228658016714960425L;

	private TextField txtBrand;
	private TextField txtSerie;
	private TextField txtModel;
	private TextField txtAssetPrice;
	private TextField txtChassisNo;
	private TextField txtEngineNo;
	private ERefDataComboBox<EColor> cbxColor;
	private TextField txtDealerName;
	private TextField txtTaxInvoiceNumber;
	private AssetDetailLayout assetDetailLayout;
	private VerticalLayout mainVerLayout;
	private VerticalLayout mainLayout;
	private Contract contract;


	/**
	 * @param values
	 * @return
	 */
	private <T extends RefDataId> ERefDataComboBox<T> getERefDataComboBox(List<T> values) {
		ERefDataComboBox<T> comboBox = new ERefDataComboBox<>(values);
		comboBox.setWidth(200, Unit.PIXELS);
		return comboBox;
	}

	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractTabPanel#createForm()
	 */
	@Override
	protected Component createForm() {
		assetDetailLayout = new AssetDetailLayout();

		txtBrand = ComponentFactory.getTextField(60, 200);
		txtSerie = ComponentFactory.getTextField(60, 200);
		txtModel = ComponentFactory.getTextField(60, 200);

		txtAssetPrice = ComponentFactory.getTextField(60, 200);
		txtChassisNo = ComponentFactory.getTextField(60, 200);
		txtEngineNo = ComponentFactory.getTextField(60, 200);
		cbxColor = getERefDataComboBox(EColor.values());
		txtDealerName = ComponentFactory.getTextField(60, 200);
		txtTaxInvoiceNumber = ComponentFactory.getTextField(50, 200);

		setEnabledAssetControls(false);

		String template = "assetTabLayout";
		InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
		CustomLayout customLayout = null;
		try {
			customLayout = new CustomLayout(layoutFile);
		} catch (IOException e) {
			Notification.show("Could not locate template " + template, e.getMessage(), Type.ERROR_MESSAGE);
		}
		customLayout.addComponent(new Label(I18N.message("asset.make")), "lblBrand");
		customLayout.addComponent(txtBrand, "cbxBrand");
		customLayout.addComponent(new Label(I18N.message("series")), "lblSeries");
		customLayout.addComponent(txtSerie, "cbxSerie");
		customLayout.addComponent(new Label(I18N.message("marketing.name")), "lblMarketingName");
		customLayout.addComponent(txtModel, "cbxModel");
		customLayout.addComponent(new Label(I18N.message("asset.price")), "lblAssetPrice");
		customLayout.addComponent(txtAssetPrice, "txtAssetPrice");
		customLayout.addComponent(new Label(I18N.message("marketing.color")), "lblMarketingColor");
		customLayout.addComponent(cbxColor, "cbxMarketingColor");
		customLayout.addComponent(new Label(I18N.message("chassis.no")), "lblChassisNo");
		customLayout.addComponent(txtChassisNo, "txtChassisNo");
		customLayout.addComponent(new Label(I18N.message("engine.no")), "lblEngineNo");
		customLayout.addComponent(txtEngineNo, "txtEngineNo");
		customLayout.addComponent(new Label(I18N.message("name.en")), "lblName");
		customLayout.addComponent(txtDealerName, "txtName");
		customLayout.addComponent(new Label(I18N.message("tax.invoice.no")), "lblTaxInvoiceNumber");
		customLayout.addComponent(txtTaxInvoiceNumber, "txtTaxInvoiceNumber");

		Panel generalPanel = new Panel();
		generalPanel.setContent(customLayout);
		mainVerLayout = new VerticalLayout();
		mainVerLayout.setSpacing(true);
		mainVerLayout.addComponent(generalPanel);

		mainLayout = new VerticalLayout();
		mainLayout.addComponent(mainVerLayout);

		return mainLayout;
	}

	/**
	 * @param contract
	 */
	public void assignValues(Contract contract) {
		if (contract != null) {
			this.contract = contract;
			assetDetailLayout.assignValues(contract);
			Asset asset = contract.getAsset();
			if (asset != null) {
				AssetModel assetModel = asset.getModel();
				AssetRange assetRange = null;
				AssetMake assetMake = null;
				if (assetModel != null) {
					assetRange = assetModel.getAssetRange();
					if (assetRange != null) {
						assetMake = assetRange.getAssetMake();
					}
				}

				txtBrand.setValue(assetMake.getDescEn());
				txtSerie.setValue(assetRange.getDescEn());
				txtModel.setValue(assetModel.getDescEn());
				txtAssetPrice.setValue(getDefaultString(asset.getTiAssetPrice()));
				cbxColor.setSelectedEntity(asset.getColor());
				txtChassisNo.setValue(getDefaultString(asset.getChassisNumber()));
				txtEngineNo.setValue(getDefaultString(asset.getEngineNumber()));
				txtTaxInvoiceNumber.setValue(getDefaultString(asset.getTaxInvoiceNumber()));
			}
			Dealer dealer = contract.getDealer();
			if (dealer != null) {
				txtDealerName.setValue(getDefaultString(dealer.getNameEn()));
			}
		}
	}

	/**
	 * @param enabled
	 */
	public void setEnabledAssetControls(boolean enabled) {
		txtBrand.setEnabled(enabled);
		txtSerie.setEnabled(enabled);
		txtModel.setEnabled(enabled);
		txtDealerName.setEnabled(enabled);
		txtAssetPrice.setEnabled(enabled);
		txtTaxInvoiceNumber.setEnabled(enabled);
		txtEngineNo.setEnabled(enabled);
		txtChassisNo.setEnabled(enabled);
		cbxColor.setEnabled(enabled);
	}
}
