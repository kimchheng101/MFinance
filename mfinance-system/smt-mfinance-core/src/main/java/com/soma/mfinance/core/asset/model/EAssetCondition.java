package com.soma.mfinance.core.asset.model;

import org.seuksa.frmk.model.eref.BaseERefData;

import javax.persistence.AttributeConverter;
import java.util.List;

/**
 * Created by cheasocheat on 2/21/17.
 */
public class EAssetCondition extends BaseERefData implements AttributeConverter<EAssetCondition, Long> {

    public final static EAssetCondition PERFECT = new EAssetCondition("Perfect(90%)", 1);
    public final static EAssetCondition GOOD = new EAssetCondition("Good(80%)", 2);
    public final static EAssetCondition FAIR = new EAssetCondition("Fair(70%)", 3);
    public final static EAssetCondition SCRATCH = new EAssetCondition("Scratch/Rusty/Low(50%)", 4);
    public final static EAssetCondition BROKEN = new EAssetCondition("Broken/No/Fake", 5);

    public EAssetCondition(){}

    /*
     * @param code
     * @param id
     */
    public EAssetCondition(String code, long id){
        super(code,id);
    }

    @Override
    public Long convertToDatabaseColumn(EAssetCondition attribute) {
        return super.convertToDatabaseColumn(attribute);
    }

    @Override
    public EAssetCondition convertToEntityAttribute(Long dbData) {
        return super.convertToEntityAttribute(dbData);
    }

    /**
     *
     * @return
     */
    public static List<EAssetCondition> values() {
        return getValues(EAssetCondition.class);
    }

    /**
     *
     * @param code
     * @return
     */
    public static EAssetCondition getByCode(String code) {
        return getByCode(EAssetCondition.class, code);
    }

    /**
     *
     * @param id
     * @return
     */
    public static EAssetCondition getById(long id) {
        return getById(EAssetCondition.class, id);
    }

}
