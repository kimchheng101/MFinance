package com.soma.mfinance.core.shared.payment;

import com.soma.mfinance.core.shared.FMEntityField;

/**
 * @author kimsuor.seang
 */
public interface PaymentEntityField extends FMEntityField {

	String INTERNAL_CODE = "Code";
	String EXTERNAL_CODE = "externalCode";
	
	String INSTALLMENT_DATE = "installmentDate";
}
