package com.soma.mfinance.core.registrations.model;

import com.soma.mfinance.core.quotation.model.Quotation;
import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "td_quotation_registration_status_history")
public class QuotationRegistrationStatusHistory extends EntityA {

	private static final long serialVersionUID = -3562428684414352440L;
	
	private RegistrationStatus registrationStatus;
	private Date registrationHistoryDate;
	private Quotation quotation;

	/**
	 * @return id
	 * @see org.seuksa.frmk.model.entity.EntityA#getId()
	 */
    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "quo_reg_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }
    
    /**
	 * @return the registrationStatus
	 */
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reg_sta_id", nullable = true)
	public RegistrationStatus getRegistrationStatus() {
		return registrationStatus;
	}

	/**
	 * @param registrationStatus the registrationStatus to set
	 */
	public void setRegistrationStatus(RegistrationStatus registrationStatus) {
		this.registrationStatus = registrationStatus;
	}
	
	/**
	 * @return the quotation
	 */
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "quo_id")
	public Quotation getquotation() {
		return quotation;
	}

	/**
	 * @param quotation the quotation to set
	 */
	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}

	/**
	 * @return the registrationHistoryDate
	 */
	public Date getRegistrationHistoryDate() {
		return registrationHistoryDate;
	}

	/**
	 * @param registrationHistoryDate the registrationHistoryDate to set
	 */
	public void setRegistrationHistoryDate(Date registrationHistoryDate) {
		this.registrationHistoryDate = registrationHistoryDate;
	}

}

