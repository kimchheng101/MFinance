package com.soma.mfinance.core.quotation.impl;

import com.soma.mfinance.core.quotation.SequenceGenerator;

/**
 * Default Sequence generator 
 * @author kimsuor.seang
 *
 */
public class DefaultSequenceGeneratorImpl implements SequenceGenerator {

	@Override
	public String generate() {
		return "SEQ";
	}

}
