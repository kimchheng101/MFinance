package com.soma.mfinance.core.address.model;

import com.soma.common.app.eref.ECountry;
import com.soma.mfinance.core.collection.model.EColType;
import com.soma.ersys.core.hr.model.address.Commune;
import com.soma.ersys.core.hr.model.address.District;
import com.soma.ersys.core.hr.model.address.Province;
import com.soma.ersys.core.hr.model.address.Village;
import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * An area can covers one or several districts
 *
 * @author kimsuor.seang
 */
@Entity
@Table(name = "tu_area")
public class  Area extends EntityRefA {
    /** */
    private static final long serialVersionUID = -1689786438480248606L;

    private String shordCode;
    private String street;
    private String line1;
    private String line2;
    private String postalCode;
    private String remark;

    private Province province;
    private List<District> districts;
    private List<Commune> communes;
    private ECountry country;


    private EColType colType;

    /**
     * @see org.seuksa.frmk.model.entity.EntityA#getId()
     */
    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "are_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * @see org.seuksa.frmk.model.entity.AuditEntityRef#getCode()
     */
    @Column(name = "are_code", nullable = false, length = 10)
    @Override
    public String getCode() {
        return code;
    }

    /**
     * @return the shordCode
     */
    @Column(name = "are_short_code", nullable = true, length = 10)
    public String getShordCode() {
        return shordCode;
    }

    /**
     * @param shordCode the shordCode to set
     */
    public void setShordCode(String shordCode) {
        this.shordCode = shordCode;
    }

    /**
     * @see org.seuksa.frmk.model.entity.AuditEntityRef#getDesc()
     */
    @Column(name = "are_desc", nullable = true, length = 255)
    @Override
    public String getDesc() {
        return desc;
    }

    /**
     * @see org.seuksa.frmk.model.entity.EntityRefA#getDescEn()
     */
    @Column(name = "are_desc_en", nullable = false, length = 255)
    @Override
    public String getDescEn() {
        return descEn;
    }

    /**
     * @return the street
     */
    @Column(name = "are_street", nullable = true, length = 100)
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the line1
     */
    @Column(name = "are_line1", nullable = true, length = 100)
    public String getLine1() {
        return line1;
    }

    /**
     * @param line1 the line1 to set
     */
    public void setLine1(String line1) {
        this.line1 = line1;
    }

    /**
     * @return the line2
     */
    @Column(name = "are_line2", nullable = true, length = 100)
    public String getLine2() {
        return line2;
    }

    /**
     * @param line2 the line2 to set
     */
    public void setLine2(String line2) {
        this.line2 = line2;
    }

    /**
     * @return the postalCode
     */
    @Column(name = "are_postal_code", nullable = true, length = 10)
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * @param postalCode the postalCode to set
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * @return the province
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pro_id", nullable = true)
    public Province getProvince() {
        return province;
    }

    /**
     * @param province the province to set
     */
    public void setProvince(Province province) {
        this.province = province;
    }

    /**
     * @return the country
     */
    @Column(name = "cou_id", nullable = true)
    @Convert(converter = ECountry.class)
    public ECountry getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(ECountry country) {
        this.country = country;
    }

    /**
     * @return the remark
     */
    @Column(name = "are_remark", nullable = true, length = 255)
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark the remark to set
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "tu_area_district",
            joinColumns = {@JoinColumn(name = "are_id")},
            inverseJoinColumns = {@JoinColumn(name = "dis_id")})
    public List<District> getDistricts() {
        if (districts == null) {
            districts = new ArrayList<>();
        }
        return districts;
    }

    public void setDistricts(List<District> districts) {
        this.districts = districts;
    }

    @Transient
    public void addDistrict(District district) {
        if (districts == null)
            districts = new ArrayList<District>();
        districts.add(district);
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "tu_area_commune",
            joinColumns = {@JoinColumn(name = "are_id")},
            inverseJoinColumns = {@JoinColumn(name = "com_id")})
    public List<Commune> getCommunes() {
        if (communes == null) {
            communes = new ArrayList<>();
        }
        return communes;
    }

    public void setCommunes(List<Commune> communes) {
        this.communes = communes;
    }

    @Transient
    public void addCommune(Commune commune) {
        if (communes == null)
            communes = new ArrayList<Commune>();
        communes.add(commune);
    }

    /**
     * @return the colType
     */
    //@Column(name = "col_typ_id", nullable = true)
    @Convert(converter = EColType.class)
    public EColType getColType() {
        return colType;
    }

    /**
     * @param colType the colType to set
     */
    public void setColType(EColType colType) {
        this.colType = colType;
    }
}
