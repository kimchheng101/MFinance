package com.soma.mfinance.core.marketing.service;

import org.seuksa.frmk.model.entity.MEntityA;

/**
 * Meta data of com.soma.mfinance.core.marketing.model.TeamEmployee
 * @author kimsuor.seang
 */
public interface MTeamEmployee extends MEntityA {

	public final static String TEAM = "team";	
	public final static String EMPLOYEE = "employee";

}
