package com.soma.mfinance.core.asset.panel.appraisal.category;

import com.soma.mfinance.core.asset.model.appraisal.AppraisalCategory;
import com.soma.mfinance.core.asset.panel.appraisal.category.AppraisalCategorySearchPanel;
import com.soma.mfinance.core.shared.asset.AssetEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 
 * @author vi.sok
 *
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AppraisalCategoryTablePanel extends AbstractTablePanel<AppraisalCategory> implements AssetEntityField {
	
	/** */
	private static final long serialVersionUID = -1435121854065342012L;

	@PostConstruct
	public void PostConstruct() {
		setCaption(I18N.message("appraisal.categories"));
		setSizeFull();
		setMargin(true);
		setSpacing(true);
				
		super.init(I18N.message("appraisal.categories"));
		
		addDefaultNavigation();
	}	
	
	/**
	 * Get item selected id
	 * @return
	 */
	public Long getItemSelectedId() {
		if (selectedItem != null) {
			return (Long) selectedItem.getItemProperty(ID).getValue();
		}
		return null;
	}	
	
	/**
	 * Get Paged definition
	 * @return
	 */
	@Override
	protected PagedDataProvider<AppraisalCategory> createPagedDataProvider() {
		PagedDefinition<AppraisalCategory> pagedDefinition = new PagedDefinition<>(searchPanel.getRestrictions());
		pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 50);
		pagedDefinition.addColumnDefinition(DESC_EN, I18N.message("name"), String.class, Align.LEFT, 170);	
		pagedDefinition.addColumnDefinition("eAppraisalType."+ DESC_EN, I18N.message("appraisal.type"), String.class, Align.LEFT, 170);	
		pagedDefinition.addColumnDefinition(SORT_INDEX, I18N.message("sortIndex"), Integer.class, Align.LEFT, 170);
		pagedDefinition.addColumnDefinition("appraisalRange." + DESC_EN, I18N.message("groupdetail.range"), String.class, Align.LEFT, 170);
		EntityPagedDataProvider<AppraisalCategory> pagedDataProvider = new EntityPagedDataProvider<>();
		pagedDataProvider.setPagedDefinition(pagedDefinition);
		return pagedDataProvider;
	}

	/**
	 * @see AbstractTablePanel#getEntity()
	 */
	@Override
	protected AppraisalCategory getEntity() {
		final Long id = getItemSelectedId();
		if (id != null) {
		    return ENTITY_SRV.getById(AppraisalCategory.class, id);
		}
		return null;
	}

	/**
	 * @see AbstractTablePanel#createSearchPanel()
	 */
	@Override
	protected AppraisalCategorySearchPanel createSearchPanel() {
		return new AppraisalCategorySearchPanel(this);		
	}
}
