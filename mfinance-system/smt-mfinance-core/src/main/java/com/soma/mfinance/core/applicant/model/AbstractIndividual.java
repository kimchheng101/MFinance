package com.soma.mfinance.core.applicant.model;

import com.soma.common.app.eref.ELanguage;
import com.soma.mfinance.core.Enum.ECertifyCurrentAddress;
import com.soma.mfinance.core.Enum.ECertifyIncome;
import com.soma.mfinance.core.Enum.ECorrespondence;
import com.soma.mfinance.core.quotation.model.QuotationApplicant;
import com.soma.ersys.core.hr.model.eref.EEducation;
import com.soma.ersys.core.hr.model.eref.ERelationship;
import com.soma.ersys.core.hr.model.eref.EReligion;
import com.soma.ersys.core.hr.model.eref.ETitle;
import com.soma.ersys.core.hr.model.organization.BasePerson;

import javax.persistence.*;
import java.util.List;

/**
 * 
 * @author kimsuor.seang
 *
 */
@MappedSuperclass
public abstract class AbstractIndividual extends BasePerson implements MAbstractIndividual {

	private static final long serialVersionUID = 3834251079538576695L;

	private String reference;
	private String otherNationality;
	
	private Integer numberOfChildren;
	private Integer numberOfHousehold;
	
	private EReligion religion;
	private EEducation education;
	private ELanguage preferredLanguage;
	private ELanguage secondLanguage;
	private ETitle title;

	private Double monthlyGrossIncome;
	private Double monthlyExpenses;
	private Double monthlyPersonalExpenses;
	private Double monthlyFamilyExpenses;
	private Boolean debtFromOtherSource;
	private Double totalDebtInstallment;
	private Boolean guarantorOtherLoan;
	private String convenientVisitTime;
	private String grade;
	
	private Double householdExpenses;
	private Double householdIncome;
	
	private Integer totalFamilyMember;
	
	private Double fixedIncome;
	private Double otherIncomes;
	private Double debtOtherLoans;
	private String debtSource;
	private Boolean staffFinancialCompany;
		
	private List<QuotationApplicant> quotationApplicants;
	

	private Integer timestamp;
	private String middleNameEn;

	private Double monthlyIncomeMember1;
	private Double monthlyIncomeMember2;
	private Double monthlyIncomeMember3;
	private Double monthlyIncomeMember4;
	private Double monthlyIncomeMember5;
	private Double monthlyIncomeMember6;

	private String contactMember1;
	private String contactMember2;
	private String contactMember3;
	private String contactMember4;
	private String contactMember5;
	private String contactMember6;

	private String chiefVillageName;
	private String chiefVillagePhoneNumber;
	private String deputyChiefVillageName;
	private String deputyChiefVillagePhoneNumber;

	private ERelationship relationship1;
	private ERelationship relationship2;
	private ERelationship relationship3;
	private ERelationship relationship4;
	private ERelationship relationship5;
	private ERelationship relationship6;

	private ECertifyIncome eCertifyIncome;
	private ECertifyCurrentAddress eCertifyCurrentAddress;
	private ECorrespondence eCorrespondence;
	private Double totalAllFamilyMemberIncome;
	
	/**
	 * @return the reference
	 */
	@Column(name = "ind_va_reference", nullable = true, length = 25)
	public String getReference() {
		return reference;
	}

	/**
	 * @param reference the reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}
	
	/**
	 * @return the numberOfChildren
	 */
	@Column(name = "ind_nu_number_children", nullable = true)
	public Integer getNumberOfChildren() {
		return numberOfChildren;
	}

	/**
	 * @param numberOfChildren the numberOfChildren to set
	 */
	public void setNumberOfChildren(Integer numberOfChildren) {
		this.numberOfChildren = numberOfChildren;
	}	
	
	/**
	 * @return the numberOfHousehold
	 */
	@Column(name = "ind_nu_number_house_hold", nullable = true)
	public Integer getNumberOfHousehold() {
		return numberOfHousehold;
	}

	/**
	 * @param numberOfHousehold the numberOfHousehold to set
	 */
	public void setNumberOfHousehold(Integer numberOfHousehold) {
		this.numberOfHousehold = numberOfHousehold;
	}

	/**
	 * @return the religion
	 */
	@Column(name = "reg_id", nullable = true)
    @Convert(converter = EReligion.class)
	public EReligion getReligion() {
		return religion;
	}

	/**
	 * @param religion the religion to set
	 */
	public void setReligion(EReligion religion) {
		this.religion = religion;
	}

	/**
	 * @return the education
	 */
	@Column(name = "edu_id", nullable = true)
    @Convert(converter = EEducation.class)
	public EEducation getEducation() {
		return education;
	}

	/**
	 * @param education the education to set
	 */
	public void setEducation(EEducation education) {
		this.education = education;
	}
	
	/**
	 * @return the preferredLanguage
	 */
	@Column(name = "lan_id_preferred", nullable = true)
    @Convert(converter = ELanguage.class)
	public ELanguage getPreferredLanguage() {
		return preferredLanguage;
	}

	/**
	 * @param preferredLanguage the preferredLanguage to set
	 */
	public void setPreferredLanguage(ELanguage preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}

	/**
	 * @return the secondLanguage
	 */
	@Column(name = "lan_id_second", nullable = true)
    @Convert(converter = ELanguage.class)
	public ELanguage getSecondLanguage() {
		return secondLanguage;
	}

	/**
	 * @param secondLanguage the secondLanguage to set
	 */
	public void setSecondLanguage(ELanguage secondLanguage) {
		this.secondLanguage = secondLanguage;
	}
	
	/**
	 * @return the title
	 */
	@Column(name = "tit_id", nullable = true)
    @Convert(converter = ETitle.class)
	public ETitle getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(ETitle title) {
		this.title = title;
	}

	/**
	 * @return the monthlyPersonalExpenses
	 */
	@Column(name = "ind_am_monthly_personal_expenses", nullable = true)
	public Double getMonthlyPersonalExpenses() {
		return monthlyPersonalExpenses;
	}

	/**
	 * @param monthlyPersonalExpenses the monthlyPersonalExpenses to set
	 */
	public void setMonthlyPersonalExpenses(Double monthlyPersonalExpenses) {
		this.monthlyPersonalExpenses = monthlyPersonalExpenses;
	}

	/**
	 * @return the monthlyFamilyExpenses
	 */
	@Column(name = "ind_am_monthly_family_expenses", nullable = true)
	public Double getMonthlyFamilyExpenses() {
		return monthlyFamilyExpenses;
	}

	/**
	 * @param monthlyFamilyExpenses the monthlyFamilyExpenses to set
	 */
	public void setMonthlyFamilyExpenses(Double monthlyFamilyExpenses) {
		this.monthlyFamilyExpenses = monthlyFamilyExpenses;
	}

	/**
	 * @return the debtFromOtherSource
	 */
	@Column(name = "ind_bl_debt_from_other_source", nullable = true)
	public Boolean isDebtFromOtherSource() {
		return debtFromOtherSource;
	}

	/**
	 * @param debtFromOtherSource the debtFromOtherSource to set
	 */
	public void setDebtFromOtherSource(Boolean debtFromOtherSource) {
		this.debtFromOtherSource = debtFromOtherSource;
	}

	/**
	 * @return the totalDebtInstallment
	 */
	@Column(name = "ind_am_total_debt_installment", nullable = true)
	public Double getTotalDebtInstallment() {
		return totalDebtInstallment;
	}

	/**
	 * @param totalDebtInstallment the totalDebtInstallment to set
	 */
	public void setTotalDebtInstallment(Double totalDebtInstallment) {
		this.totalDebtInstallment = totalDebtInstallment;
	}
	
	
	/**
	 * @return the guarantorOtherLoan
	 */
	@Column(name = "ind_bl_guarantor_other_loan", nullable = true)
	public Boolean isGuarantorOtherLoan() {
		return guarantorOtherLoan;
	}

	/**
	 * @param guarantorOtherLoan the guarantorOtherLoan to set
	 */
	public void setGuarantorOtherLoan(Boolean guarantorOtherLoan) {
		this.guarantorOtherLoan = guarantorOtherLoan;
	}

	/**
	 * @return the convenientVisitTime
	 */
	@Column(name = "ind_va_convenient_visit_time", nullable = true, length = 50)
	public String getConvenientVisitTime() {
		return convenientVisitTime;
	}

	/**
	 * @param convenientVisitTime the convenientVisitTime to set
	 */
	public void setConvenientVisitTime(String convenientVisitTime) {
		this.convenientVisitTime = convenientVisitTime;
	}	
	
	/**
	 * @return the grade
	 */
	@Column(name = "ind_va_grade", nullable = true, length = 2)
	public String getGrade() {
		return grade;
	}

	/**
	 * @param grade the grade to set
	 */
	public void setGrade(String grade) {
		this.grade = grade;
	}

	/**
	 * @return the householdExpenses
	 */
	@Column(name = "ind_am_household_expenses", nullable = true)
	public Double getHouseholdExpenses() {
		return householdExpenses;
	}

	/**
	 * @param householdExpenses the householdExpenses to set
	 */
	public void setHouseholdExpenses(Double householdExpenses) {
		this.householdExpenses = householdExpenses;
	}

	/**
	 * @return the householdIncome
	 */
	@Column(name = "ind_am_household_income", nullable = true)
	public Double getHouseholdIncome() {
		return householdIncome;
	}

	/**
	 * @param householdIncome the householdIncome to set
	 */
	public void setHouseholdIncome(Double householdIncome) {
		this.householdIncome = householdIncome;
	}

	/**
	 * @return the quotationApplicants
	 */
	@OneToMany(mappedBy="applicant", fetch = FetchType.LAZY)
	public List<QuotationApplicant> getQuotationApplicants() {
		return quotationApplicants;
	}

	/**
	 * @param quotationApplicants the quotationApplicants to set
	 */
	public void setQuotationApplicants(List<QuotationApplicant> quotationApplicants) {
		this.quotationApplicants = quotationApplicants;
	}
	
	/**
	 * @return the timestamp
	 */
	@Column(name = "timestamp")
	public Integer getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Integer timestamp) {
		this.timestamp = timestamp;
	}
	
	/**
	 * @return totalFamilyMember
	 */
	@Column(name = "ind_nu_number_family_member", nullable = true)
	public Integer getTotalFamilyMember() {
		return totalFamilyMember;
	}
	
	/**
	 * @param totalFamilyMember
	 */
	public void setTotalFamilyMember(Integer totalFamilyMember) {
		this.totalFamilyMember = totalFamilyMember;
	}

	/**
	 * @return the otherNationality
	 */
	@Column(name = "ind_other_nationality", nullable = true, length = 30)
	public String getOtherNationality() {
		return otherNationality;
	}

	/**
	 * @param otherNationality the otherNationality to set
	 */
	public void setOtherNationality(String otherNationality) {
		this.otherNationality = otherNationality;
	}		
	
	/**
	 * @return the fixedIncome
	 */
	@Column(name = "ind_am_fixed_income", nullable = true)
	public Double getFixedIncome() {
		return fixedIncome;
	}

	/**
	 * @param fixedIncome the fixedIncome to set
	 */
	public void setFixedIncome(Double fixedIncome) {
		this.fixedIncome = fixedIncome;
	}

	/**
	 * @return the otherIncomes
	 */
	@Column(name = "ind_am_other_incomes", nullable = true)
	public Double getOtherIncomes() {
		return otherIncomes;
	}

	/**
	 * @param otherIncomes the otherIncomes to set
	 */
	public void setOtherIncomes(Double otherIncomes) {
		this.otherIncomes = otherIncomes;
	}

	/**
	 * @return the debtOtherLoans
	 */
	@Column(name = "ind_am_debt_other_loans", nullable = true)
	public Double getDebtOtherLoans() {
		return debtOtherLoans;
	}

	/**
	 * @param debtOtherLoans the debtOtherLoans to set
	 */
	public void setDebtOtherLoans(Double debtOtherLoans) {
		this.debtOtherLoans = debtOtherLoans;
	}

	/**
	 * @return the debtSource
	 */
	@Column(name = "ind_va_debt_sources", nullable = true, length = 255)
	public String getDebtSource() {
		return debtSource;
	}

	/**
	 * @param debtSource the debtSource to set
	 */
	public void setDebtSource(String debtSource) {
		this.debtSource = debtSource;
	}	

	/**
	 * @return the staffFinancialCompany
	 */
	@Column(name = "ind_bl_staff_fin_company", nullable = true, columnDefinition = "boolean default false")
	public Boolean isStaffFinancialCompany() {
		return staffFinancialCompany;
	}

	/**
	 * @param staffFinancialCompany the staffFinancialCompany to set
	 */
	public void setStaffFinancialCompany(Boolean staffFinancialCompany) {
		this.staffFinancialCompany = staffFinancialCompany;
	}
		

	/**
	 * @return the monthlyGrossIncome
	 */
	@Column(name = "ind_am_monthly_gross_income", nullable = true)
	public Double getMonthlyGrossIncome() {
		return monthlyGrossIncome;
	}

	/**
	 * @param monthlyGrossIncome the monthlyGrossIncome to set
	 */
	public void setMonthlyGrossIncome(Double monthlyGrossIncome) {
		this.monthlyGrossIncome = monthlyGrossIncome;
	}

	/**
	 * @return the monthlyExpenses
	 */
	@Column(name = "ind_am_monthly_expenses", nullable = true)
	public Double getMonthlyExpenses() {
		return monthlyExpenses;
	}

	/**
	 * @param monthlyExpenses the monthlyExpenses to set
	 */
	public void setMonthlyExpenses(Double monthlyExpenses) {
		this.monthlyExpenses = monthlyExpenses;
	}

	/**
	 * Disable the Workflow
	 * @return
	 */
	@Override
	@Transient
	public boolean isWkfEnabled() {
		return false;
	}

	/**
	 * @return the middleNameEn
	 */
	@Column(name = "ind_middlename_en", nullable = true, length = 100)
	public String getMiddleNameEn() {
		return middleNameEn;
	}


	public void setMiddleNameEn(String middleNameEn) {
		this.middleNameEn = middleNameEn;
	}

	@Column(name = "con_member1", nullable = true, length = 100)
	public String getContactMember1() {
		return contactMember1;
	}

	public void setContactMember1(String contactMember1) {
		this.contactMember1 = contactMember1;
	}

	@Column(name = "con_member2", nullable = true, length = 100)
	public String getContactMember2() {
		return contactMember2;
	}

	public void setContactMember2(String contactMember2) {
		this.contactMember2 = contactMember2;
	}

	@Column(name = "con_member3", nullable = true, length = 100)
	public String getContactMember3() {
		return contactMember3;
	}

	public void setContactMember3(String contactMember3) {
		this.contactMember3 = contactMember3;
	}

	@Column(name = "con_member4", nullable = true, length = 100)
	public String getContactMember4() {
		return contactMember4;
	}

	public void setContactMember4(String contactMember4) {
		this.contactMember4 = contactMember4;
	}

	@Column(name = "con_member5", nullable = true, length = 100)
	public String getContactMember5() {
		return contactMember5;
	}

	public void setContactMember5(String contactMember5) {
		this.contactMember5 = contactMember5;
	}

	@Column(name = "con_member6", nullable = true, length = 100)
	public String getContactMember6() {
		return contactMember6;
	}

	public void setContactMember6(String contactMember6) {
		this.contactMember6 = contactMember6;
	}

	@Column(name = "mon_income_member1", nullable = true, length = 100)
	public Double getMonthlyIncomeMember1() {
		return monthlyIncomeMember1;
	}

	public void setMonthlyIncomeMember1(Double monthlyIncomeMember1) {
		this.monthlyIncomeMember1 = monthlyIncomeMember1;
	}

	@Column(name = "mon_income_member2", nullable = true, length = 100)
	public Double getMonthlyIncomeMember2() {
		return monthlyIncomeMember2;
	}

	public void setMonthlyIncomeMember2(Double monthlyIncomeMember2) {
		this.monthlyIncomeMember2 = monthlyIncomeMember2;
	}

	@Column(name = "mon_income_member3", nullable = true, length = 100)
	public Double getMonthlyIncomeMember3() {
		return monthlyIncomeMember3;
	}

	public void setMonthlyIncomeMember3(Double monthlyIncomeMember3) {
		this.monthlyIncomeMember3 = monthlyIncomeMember3;
	}

	@Column(name = "mon_income_member4", nullable = true, length = 100)
	public Double getMonthlyIncomeMember4() {
		return monthlyIncomeMember4;
	}

	public void setMonthlyIncomeMember4(Double monthlyIncomeMember4) {
		this.monthlyIncomeMember4 = monthlyIncomeMember4;
	}

	@Column(name = "mon_income_member5", nullable = true, length = 100)
	public Double getMonthlyIncomeMember5() {
		return monthlyIncomeMember5;
	}

	public void setMonthlyIncomeMember5(Double monthlyIncomeMember5) {
		this.monthlyIncomeMember5 = monthlyIncomeMember5;
	}

	@Column(name = "mon_income_member6", nullable = true, length = 100)
	public Double getMonthlyIncomeMember6() {
		return monthlyIncomeMember6;
	}

	public void setMonthlyIncomeMember6(Double monthlyIncomeMember6) {
		this.monthlyIncomeMember6 = monthlyIncomeMember6;
	}

	@Column(name = "chf_village_name", nullable = true, length = 100)
	public String getChiefVillageName() {
		return chiefVillageName;
	}

	public void setChiefVillageName(String chiefVillageName) {
		this.chiefVillageName= chiefVillageName;
	}

	@Column(name = "dep_chief_village_name", nullable = true, length = 100)
	public String getDeputyChiefVillageName() {
		return deputyChiefVillageName;
	}

	public void setDeputyChiefVillageName(String deputyChiefVillageName) {
		this.deputyChiefVillageName= deputyChiefVillageName;
	}

	@Column(name = "chf_village_phone_number", nullable = true, length = 100)
	public String getChiefVillagePhoneNumber() {
		return chiefVillagePhoneNumber;
	}

	public void setChiefVillagePhoneNumber(String chiefVillagePhoneNumber) {
		this.chiefVillagePhoneNumber= chiefVillagePhoneNumber;
	}

	@Column(name = "dep_chief_village_phone_number", nullable = true, length = 100)
	public String getDeputyChiefVillagePhoneNumber() {
		return deputyChiefVillagePhoneNumber;
	}

	public void setDeputyChiefVillagePhoneNumber(String deputyChiefVillagePhoneNumber) {
		this.deputyChiefVillagePhoneNumber= deputyChiefVillagePhoneNumber;
	}

	@Column(name = "relationship1_id", nullable = true)
	@Convert(converter = ERelationship.class)
	public ERelationship getRelationship1() {
		return relationship1;
	}

	public void setRelationship1(ERelationship relationship1) {
		this.relationship1 = relationship1;
	}

	@Column(name = "relationship2_id", nullable = true)
	@Convert(converter = ERelationship.class)
	public ERelationship getRelationship2() {
		return relationship1;
	}

	public void setRelationship2(ERelationship relationship2) {
		this.relationship2 = relationship2;
	}

	@Column(name = "relationship3_id", nullable = true)
	@Convert(converter = ERelationship.class)
	public ERelationship getRelationship3() {
		return relationship3;
	}

	public void setRelationship3(ERelationship relationship3) {
		this.relationship3 = relationship3;
	}

	@Column(name = "relationship4_id", nullable = true)
	@Convert(converter = ERelationship.class)
	public ERelationship getRelationship4() {
		return relationship4;
	}

	public void setRelationship4(ERelationship relationship4) {
		this.relationship4 = relationship4;
	}

	@Column(name = "relationship5_id", nullable = true)
	@Convert(converter = ERelationship.class)
	public ERelationship getRelationship5() {
		return relationship5;
	}

	public void setRelationship5(ERelationship relationship5) {
		this.relationship5 = relationship5;
	}

	@Column(name = "relationship6_id", nullable = true)
	@Convert(converter = ERelationship.class)
	public ERelationship getRelationship6() {
		return relationship6;
	}

	public void setRelationship6(ERelationship relationship6) {
		this.relationship6 = relationship6;
	}

	@Column(name = "cer_income", nullable = true)
	@Convert(converter = ECertifyIncome.class)
	public ECertifyIncome getECertifyIncome() {
		return eCertifyIncome;
	}

	public void setECertifyIncome(ECertifyIncome eCertifyIncome) {
		this.eCertifyIncome = eCertifyIncome;
	}

	@Column(name = "cer_current_address", nullable = true)
	@Convert(converter = ECertifyCurrentAddress.class)
	public ECertifyCurrentAddress getECertifyCurrentAddress() {
		return eCertifyCurrentAddress;
	}

	public void setECertifyCurrentAddress(ECertifyCurrentAddress eCertifyCurrentAddress) {
		this.eCertifyCurrentAddress = eCertifyCurrentAddress;
	}

	@Column(name = "correspondence", nullable = true)
	@Convert(converter = ECorrespondence.class)
	public ECorrespondence getECorrespondence() {
		return eCorrespondence;
	}

	public void setECorrespondence(ECorrespondence eCorrespondence) {
		this.eCorrespondence = eCorrespondence;
	}

	@Column(name = "total_all_family_member_income", nullable = true, length = 100)
	public Double getTotalAllFamilyMemberIncome() {
		return totalAllFamilyMemberIncome;
	}

	public void setTotalAllFamilyMemberIncome(Double totalAllFamilyMemberIncome) {
		this.totalAllFamilyMemberIncome= totalAllFamilyMemberIncome;
	}


}
