package com.soma.mfinance.core.contract.dao;

import org.seuksa.frmk.dao.BaseEntityDao;

/**
 * Contract data model access
 * @author kimsuor.seang
 *
 */
public interface ContractDao extends BaseEntityDao {

}
