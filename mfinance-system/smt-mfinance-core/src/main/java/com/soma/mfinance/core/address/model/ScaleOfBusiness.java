package com.soma.mfinance.core.address.model;

import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;

/**
 * @author by kimsuor.seang  on 10/13/2017.
 */
@Entity
@Table(name = "tu_scale_of_business")
public class ScaleOfBusiness extends EntityRefA {

    private static final long serialVersionUID = -1762740736615702442L;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sclbus_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Override
    @Transient
    public String getCode() {
        return desc;
    }

    @Column(name = "sclbus_desc", nullable = false, length=50)
    @Override
    public String getDesc() {
        return super.getDesc();
    }

    @Override
    @Column(name = "sclbus_desc_en", nullable = false, length=50)
    public String getDescEn() {
        return super.getDescEn();
    }
}
