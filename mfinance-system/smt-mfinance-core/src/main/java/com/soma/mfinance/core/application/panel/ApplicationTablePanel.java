package com.soma.mfinance.core.application.panel;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.custom.component.ApplicationEntityPagedTable;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.mfinance.core.shared.quotation.QuotationProfileUtils;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.ersys.core.hr.model.address.*;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.DateColumnRenderer;
import com.soma.frmk.vaadin.ui.widget.table.EntityColumnRenderer;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedTable;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ApplicationTablePanel extends AbstractTablePanel<Quotation> implements QuotationEntityField {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@PostConstruct
	public void PostConstruct() {
		super.init(I18N.message("applications"));
		setCaption(I18N.message("applications"));
		setSizeFull();
		setMargin(true);
		setSpacing(true);
		addDefaultNavigation();
	}

	protected NavigationPanel addDefaultNavigation() {
		NavigationPanel navigationPanel = this.addNavigationPanel();
		if(QuotationProfileUtils.isNavigationType2Available()){
			navigationPanel.addEditClickListener(this);
			navigationPanel.addRefreshClickListener(this);
		}else if(QuotationProfileUtils.isNavigationType1Available()){
			navigationPanel.addAddClickListener(this);
			navigationPanel.addEditClickListener(this);
			//navigationPanel.addDeleteClickListener(this);
			navigationPanel.addRefreshClickListener(this);
		}
		return navigationPanel;
	}

	@Override
	protected EntityPagedTable<Quotation> createPagedTable(String caption) {
		EntityPagedTable<Quotation> pagedTable = new ApplicationEntityPagedTable<>(caption, this.createPagedDataProvider());
		pagedTable.buildGrid();
		return pagedTable;
	}

	@Override
	protected PagedDataProvider<Quotation> createPagedDataProvider() {
		PagedDefinition<Quotation> pagedDefinition = new PagedDefinition<>(searchPanel.getRestrictions());
		pagedDefinition.addColumnDefinition(ID, I18N.message("id").toUpperCase(), Long.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition(REFERENCE, I18N.message("reference").toUpperCase(), String.class, Align.LEFT, 130);
		pagedDefinition.addColumnDefinition("financialProduct.descEn", I18N.message("financial.product").toUpperCase(), String.class, Align.LEFT, 150);
		pagedDefinition.addColumnDefinition(CUSTOMER, I18N.message("customer").toUpperCase(), String.class, Align.LEFT, 120, new CustomerFullNameColumnRenderer());
		pagedDefinition.addColumnDefinition("asset.model.descEn", I18N.message("asset").toUpperCase(), String.class, Align.LEFT, 100);
		//pagedDefinition.addColumnDefinition(DEALER_TYPE + ".descEn", I18N.message("dealer.type").toUpperCase(), String.class, Align.LEFT, 100);
		if(ProfileUtil.isAdmin()){
			pagedDefinition.addColumnDefinition(DEALER + ".nameEn", I18N.message("dealer").toUpperCase(), String.class, Align.LEFT, 150);
		}
		pagedDefinition.addColumnDefinition(STATUS, I18N.message("status").toUpperCase(), String.class, Align.LEFT, 130, new StatusColumnRenderer());
		pagedDefinition.addColumnDefinition(QUOTATION_DATE, I18N.message("quotation.date").toUpperCase(), String.class, Align.LEFT, 150, new DateColumnRenderer(DateUtils.FORMAT_YYYYMMDD_HHMMSS_SLASH));
		pagedDefinition.addColumnDefinition(FIRST_SUBMISSION_DATE, I18N.message("first.submission.date").toUpperCase(), String.class, Align.LEFT, 150, new DateColumnRenderer(DateUtils.FORMAT_YYYYMMDD_HHMMSS_SLASH));
		pagedDefinition.addColumnDefinition(UNDERWRITER + "." + DESC, I18N.message("underwriter").toUpperCase(), String.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition(UNDERWRITER_SUPERVISOR + "." + DESC, I18N.message("underwriter.supervisor").toUpperCase(), String.class, Align.LEFT, 200);
		pagedDefinition.addColumnDefinition(ACCEPTATION_DATE, I18N.message("acceptation.date").toUpperCase(), Date.class, Align.LEFT, 150);
		pagedDefinition.addColumnDefinition(CONTRACT_START_DATE, I18N.message("contract.date").toUpperCase(), Date.class, Align.LEFT, 150);
		pagedDefinition.addColumnDefinition(PROVINCE, I18N.message("province").toUpperCase(), String.class, Align.LEFT, 150,new ProviceColumnRenderer());
		pagedDefinition.addColumnDefinition(DISTRICT, I18N.message("district").toUpperCase(), String.class, Align.LEFT, 150, new DistrictColumnRenderer());
		pagedDefinition.addColumnDefinition(COMMUNE, I18N.message("quo.commune").toUpperCase(), String.class, Align.LEFT, 150, new CommuneColumnRenderer());
		pagedDefinition.addColumnDefinition(VILLAGE, I18N.message("village").toUpperCase(), String.class, Align.LEFT, 150 , new VillageColumnRenderer());

		EntityPagedDataProvider<Quotation> pagedDataProvider = new EntityPagedDataProvider<>();
		pagedDataProvider.setPagedDefinition(pagedDefinition);
		return pagedDataProvider;
	}

	@Override
	protected Quotation getEntity() {
		final Long id = getItemSelectedId();
		if (id != null) {
			return ENTITY_SRV.getById(Quotation.class, id);
		}
		return null;
	}

	@Override
	protected ApplicationSearchPanel createSearchPanel() {
		return new ApplicationSearchPanel(this);
	}

	private class CustomerFullNameColumnRenderer extends EntityColumnRenderer {
		@Override
		public Object getValue() {
			Applicant customer = ((Quotation) getEntity()).getMainApplicant();
			return customer != null ? getDefaultString(customer.getIndividual().getLastNameEn()) + " " + getDefaultString(customer.getIndividual().getFirstNameEn()) : "";
		}
	}

	private class StatusColumnRenderer extends EntityColumnRenderer {

		@Override
		public Object getValue() {
			EWkfStatus wkfStatus = ((Quotation) getEntity()).getWkfStatus();
			return wkfStatus.getDesc();
		}
	}

	private class ProviceColumnRenderer extends EntityColumnRenderer {
		@Override
		public Object getValue() {
			Province province = getApplicantAddress((Quotation) getEntity()) == null ? new Province() : getApplicantAddress((Quotation) getEntity()).getProvince();
			return province != null ? getDefaultString(province.getDescEn()) : "";
		}
	}

	private class DistrictColumnRenderer extends EntityColumnRenderer{
		@Override
		public Object getValue() {
			District district = getApplicantAddress((Quotation) getEntity()) == null ? new District() : getApplicantAddress((Quotation) getEntity()).getDistrict();
			return district == null ? "" : getDefaultString(district.getDescEn());
		}
	}

	private class CommuneColumnRenderer extends EntityColumnRenderer{
		@Override
		public Object getValue() {
			Commune commune = getApplicantAddress((Quotation) getEntity()) == null ? new Commune() : getApplicantAddress((Quotation) getEntity()).getCommune();
			return commune !=null ? getDefaultString(commune.getDescEn()) : "";
		}
	}

	private class VillageColumnRenderer extends EntityColumnRenderer{
		@Override
		public Object getValue() {
			Village village = getApplicantAddress((Quotation) getEntity()) == null ? new Village() : getApplicantAddress((Quotation) getEntity()).getVillage();
			return village != null ? getDefaultString(village.getDescEn()) : "";
		}
	}

	private Address getApplicantAddress(Quotation quotation){
		Applicant applicant = quotation.getMainApplicant();
		Address applicantAddress = null;
		if(applicant != null) {
			applicantAddress = applicant.getIndividual().getMainAddress();
		}
		return  applicantAddress != null ? applicantAddress : new Address();

	}

	private String getDefaultString(String value) {
		return value == null?"":value;
	}
}
