package com.soma.mfinance.core.document.model;

import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.financial.model.ProductLine;
import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;
import java.util.List;

/**
 * Document
 *
 * @author kimsuor.seang
 * @modify kimsuor.seang
 */
@Entity
@Table(name = "tu_document")
public class Document extends EntityRefA {

    private static final long serialVersionUID = 2057497814590826522L;

    private boolean mandatory;
    private Integer numGroup;
    private boolean referenceRequired;
    private boolean issueDateRequired;
    private boolean expireDateRequired;
    private boolean submitCreditBureau;
    private boolean fieldCheck;
    private boolean allowUpdateChangeAsset;
    private Integer cbcUploadPriority;
    private EDocumentState documentState;

    private EApplicantType applicantType;
    private DocumentGroup documentGroup;

    private List<DocumentScoring> documentsScoring;

    private ProductLine productLine;
    private AssetRange assetRange;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "doc_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name = "doc_code", nullable = false, length = 10, unique = true)
    @Override
    public String getCode() {
        return super.getCode();
    }

    @Column(name = "doc_desc", nullable = true, length = 50)
    @Override
    public String getDesc() {
        return super.getDesc();
    }

    @Override
    @Column(name = "doc_desc_en", nullable = false, length = 50)
    public String getDescEn() {
        return super.getDescEn();
    }

    @Column(name = "doc_bl_mandatory", nullable = true, columnDefinition = "boolean default true")
    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    @Column(name = "doc_nu_num_group", nullable = true)
    public Integer getNumGroup() {
        return numGroup;
    }

    public void setNumGroup(Integer numGroup) {
        this.numGroup = numGroup;
    }

    @Column(name = "doc_bl_reference_required", nullable = true, columnDefinition = "boolean default true")
    public boolean isReferenceRequired() {
        return referenceRequired;
    }

    public void setReferenceRequired(boolean referenceRequired) {
        this.referenceRequired = referenceRequired;
    }

    @Column(name = "doc_bl_issue_date_required", nullable = true, columnDefinition = "boolean default true")
    public boolean isIssueDateRequired() {
        return issueDateRequired;
    }

    public void setIssueDateRequired(boolean issueDateRequired) {
        this.issueDateRequired = issueDateRequired;
    }

    @Column(name = "doc_bl_expire_date_required", nullable = true, columnDefinition = "boolean default true")
    public boolean isExpireDateRequired() {
        return expireDateRequired;
    }

    public void setExpireDateRequired(boolean expireDateRequired) {
        this.expireDateRequired = expireDateRequired;
    }

    @Column(name = "doc_bl_submit_credit_bureau", nullable = true, columnDefinition = "boolean default true")
    public boolean isSubmitCreditBureau() {
        return submitCreditBureau;
    }

    public void setSubmitCreditBureau(boolean submitCreditBureau) {
        this.submitCreditBureau = submitCreditBureau;
    }

    @Column(name = "doc_bl_field_check", nullable = true, columnDefinition = "boolean default true")
    public boolean isFieldCheck() {
        return fieldCheck;
    }

    public void setFieldCheck(boolean fieldCheck) {
        this.fieldCheck = fieldCheck;
    }

    @Column(name = "doc_bl_allow_change_update_asset", nullable = true, columnDefinition = "boolean default true")
    public boolean isAllowUpdateChangeAsset() {
        return allowUpdateChangeAsset;
    }

    public void setAllowUpdateChangeAsset(boolean allowUpdateChangeAsset) {
        this.allowUpdateChangeAsset = allowUpdateChangeAsset;
    }

    @Column(name = "doc_sta_id", nullable = true)
    @Convert(converter = EDocumentState.class)
    public EDocumentState getDocumentState() {
        return documentState;
    }

    public void setDocumentState(EDocumentState documentState) {
        this.documentState = documentState;
    }

    @Column(name = "app_typ_id", nullable = true)
    @Convert(converter = EApplicantType.class)
    public EApplicantType getApplicantType() {
        return applicantType;
    }

    public void setApplicantType(EApplicantType applicantType) {
        this.applicantType = applicantType;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "dogrp_id")
    public DocumentGroup getDocumentGroup() {
        return documentGroup;
    }

    public void setDocumentGroup(DocumentGroup documentGroup) {
        this.documentGroup = documentGroup;
    }

    @OneToMany(mappedBy = "document", fetch = FetchType.LAZY)
    public List<DocumentScoring> getDocumentsScoring() {
        return documentsScoring;
    }

    public void setDocumentsScoring(List<DocumentScoring> documentsScoring) {
        this.documentsScoring = documentsScoring;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ass_ran_id")
    public AssetRange getAssetRange() {
        return assetRange;
    }

    public void setAssetRange(AssetRange assetRange) {
        this.assetRange = assetRange;
    }

    @Column(name = "doc_cbc_prio", nullable = true, unique = true)
    public Integer getCbcUploadPriority() {
        return cbcUploadPriority;
    }

    public void setCbcUploadPriority(Integer cbcUploadPriority) {
        this.cbcUploadPriority = cbcUploadPriority;
    }
}
