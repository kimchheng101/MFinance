package com.soma.mfinance.core.applicant.panel;

import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.ersys.core.hr.model.eref.EJobPosition;
import com.soma.frmk.vaadin.ui.panel.AbstractControlPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.*;
import org.seuksa.frmk.i18n.I18N;

import java.util.List;

/**
 * @author by kimsuor.seang  on 10/13/2017.
 */
public class EquipmentRentalPanel extends AbstractControlPanel {
	
	private static final long serialVersionUID = 8341162309347917428L;

	private TextArea txtWhoWhat;
	private TextArea txtReason;
	private TextField txtYear;
	private TextField txtMonth;
	private ERefDataComboBox<EJobPosition> cbxapplicantEquipmentPosition;
	
	public EquipmentRentalPanel() {
		setMargin(true);
		setSpacing(true);
		setSizeFull();

		txtYear = ComponentFactory.getTextField(false,20,60);

		txtMonth = ComponentFactory.getTextField(false,20,60);

		cbxapplicantEquipmentPosition = new ERefDataComboBox<EJobPosition>(EJobPosition.values());
		cbxapplicantEquipmentPosition.setWidth(170, Unit.PIXELS);

		txtWhoWhat = new TextArea();
		txtWhoWhat.setSizeFull();
		txtWhoWhat.setWidth("350");
		txtWhoWhat.setRows(6);
		txtWhoWhat.setMaxLength(1000);
		txtWhoWhat.setHeight("100");

		txtReason = new TextArea();
		txtReason.setSizeFull();
		txtReason.setWidth("350");
		txtReason.setRows(6);
		txtReason.setMaxLength(1000);
		txtReason.setHeight("100");

		Label lblWhoWhat = new Label(I18N.message("who.what"));
		lblWhoWhat.setStyleName("marging-top45");
		Label lblReason = new Label(I18N.message("reason"));
		lblReason.setStyleName("marging-top45");

		final GridLayout gridLayout = new GridLayout(5, 4);
		gridLayout.setSpacing(true);

		Label symbol1 = getSymbolRequired();
		symbol1.setStyleName("marging-top45 symbol-require-field");
		Label symbol2 = getSymbolRequired();
		symbol2.setStyleName("marging-top45 symbol-require-field");

		int iCol = 0;
		gridLayout.addComponent(lblWhoWhat, iCol++, 0);
		gridLayout.addComponent(symbol1, iCol++, 0);
		gridLayout.addComponent(txtWhoWhat, iCol++, 0);

		iCol = 0;
		gridLayout.addComponent(lblReason, iCol++, 1);
		gridLayout.addComponent(symbol2, iCol++, 1);
		gridLayout.addComponent(txtReason, iCol++, 1);

		VerticalLayout verticalLayoutLeft = new VerticalLayout();
		verticalLayoutLeft.addComponent(gridLayout);

		HorizontalLayout horizontalLayout = new HorizontalLayout();

		HorizontalLayout horizontalLayoutRight1 = new HorizontalLayout();
		Label lblPosition = new Label();
		lblPosition = new Label(I18N.message("position1"));
		lblPosition.setWidth("110");
		horizontalLayoutRight1.addComponent(lblPosition);
		horizontalLayoutRight1.addComponent(getSymbolRequired());
		horizontalLayoutRight1.addComponent(cbxapplicantEquipmentPosition);

		HorizontalLayout horizontalLayoutRight2 = new HorizontalLayout();
		Label lblLenghExperience = new Label(I18N.message("length.experience"));
		lblLenghExperience.setWidth("110");
		horizontalLayoutRight2.addComponent(lblLenghExperience);
		horizontalLayoutRight2.addComponent(getSymbolRequired());
		horizontalLayoutRight2.addComponent(txtYear);
		horizontalLayoutRight2.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS));
		horizontalLayoutRight2.addComponent(new Label(I18N.message("years")));
		horizontalLayoutRight2.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS));
		horizontalLayoutRight2.addComponent(txtMonth);
		horizontalLayoutRight2.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS));
		horizontalLayoutRight2.addComponent(new Label(I18N.message("months")));

		VerticalLayout verticalLayoutRight = new VerticalLayout();
		verticalLayoutRight.addComponent(horizontalLayoutRight1);
		VerticalLayout space = new VerticalLayout();
		space.setHeight("15");
		verticalLayoutRight.addComponent(space);
		verticalLayoutRight.addComponent(horizontalLayoutRight2);

		horizontalLayout.addComponent(verticalLayoutLeft);
		horizontalLayout.addComponent(verticalLayoutRight);


		addComponent(horizontalLayout);
	}

	public void getEquipmentRentalPanel(Individual individual) {
		individual.setEquipmentRentalName(txtWhoWhat.getValue());
		individual.setEquipmentRentalReason(txtReason.getValue());
		individual.setEquipmentRentalInYear(getInteger(txtYear));
		individual.setEquipmentRentalInMonth(getInteger(txtMonth));
		individual.setApplicantEquipmentPosition(cbxapplicantEquipmentPosition.getSelectedEntity());
	}

	public void assignValues(Individual individual) {
		if (individual != null) {
			if (individual.getEquipmentRentalName() != null) {
				txtWhoWhat.setValue(individual.getEquipmentRentalName());
			}
			if (individual.getEquipmentRentalReason() != null) {
				txtReason.setValue(individual.getEquipmentRentalReason());
			}
			if (individual.getEquipmentRentalInYear() != null) {
				txtYear.setValue(individual.getEquipmentRentalInYear().toString());
			}
			if (individual.getEquipmentRentalInMonth() != null) {
				txtMonth.setValue(individual.getEquipmentRentalInMonth().toString());
			}
			if (individual.getApplicantEquipmentPosition() != null) {
				cbxapplicantEquipmentPosition.setSelectedEntity(individual.getApplicantEquipmentPosition());
			}
		}
	}

	private Label getSymbolRequired() {
		//Label lblSymbolRequired = new Label("*");
		//lblSymbolRequired.setStyleName("symbol-require-field");
		Label lblSymbolRequired = new Label("");
		return lblSymbolRequired;
	}

	public void reset() {
		assignValues(null);
		txtWhoWhat.setValue("");
		txtReason.setValue("");
		txtYear.setValue("");
		txtMonth.setValue("");
		cbxapplicantEquipmentPosition.setSelectedEntity(null);
	}

	public List<String> validate() {
		super.reset();
		/*checkMandatoryField(txtWhoWhat, "who.what");
		checkMandatoryField(txtReason, "reason");
		checkMandatorySelectField(cbxapplicantEquipmentPosition, "position");
		checkMandatoryField(txtYear, "year");
		checkMandatoryField(txtMonth, "month");*/
		return errors;
	}

	public List<String> fullValidate() {
		super.reset();
		checkMandatoryField(txtWhoWhat, "who.what");
		checkMandatoryField(txtReason, "reason");
		checkMandatorySelectField(cbxapplicantEquipmentPosition, "length.experience");
		checkMandatoryField(txtYear, "year");
		checkMandatoryField(txtMonth, "month");
		return errors;
	}
}
