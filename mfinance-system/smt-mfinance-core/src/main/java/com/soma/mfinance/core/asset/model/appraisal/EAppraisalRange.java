package com.soma.mfinance.core.asset.model.appraisal;

import org.seuksa.frmk.model.eref.BaseERefData;
import sun.security.krb5.internal.EncAPRepPart;

import javax.persistence.AttributeConverter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kimsuor SEANG
 * Date  : 7/17/2017
 * Name  : 2:20 PM
 * Email : k.seang@gl-f.com
 */
public class EAppraisalRange extends BaseERefData implements AttributeConverter<EAppraisalRange, Long> {

    public static EAppraisalRange DREAM = new EAppraisalRange("DREAM",1l);
    public static EAppraisalRange NONE_DREAM = new EAppraisalRange("NON-DREAM",2l);
    public static EAppraisalRange TRACTOR = new EAppraisalRange("TRACTOR",6L);
    public static EAppraisalRange COMBINE_HARVESTER = new EAppraisalRange("COMBINE-HARVESTER",7L);
    public static EAppraisalRange WALKING_TRACTOR = new EAppraisalRange("WALKING-TRACTOR",8L);
    /**
     */
    public EAppraisalRange() {
    }

    /**
     * @param code
     * @param id
     */
    public EAppraisalRange(String code, long id) {
        super(code, id);
    }

    @Override
    public Long convertToDatabaseColumn(EAppraisalRange attribute) {
        return super.convertToDatabaseColumn(attribute);
    }

    @Override
    public EAppraisalRange convertToEntityAttribute(Long id) {
        return super.convertToEntityAttribute(id);
    }

    /**
     * @return
     */
    public static List<EAppraisalRange> values() {
        return getValues(EAppraisalRange.class);
    }

    /**
     * @param code
     * @return
     */
    public static EAppraisalRange getByCode(String code) {
        return getByCode(EAppraisalRange.class, code);
    }

    /**
     * @param id
     * @return
     */
    public static EAppraisalRange getById(long id) {
        return getById(EAppraisalRange.class, id);
    }

    public static List<EAppraisalRange> getListAppraisalMotor(){
        List<EAppraisalRange> appraisalRanges = new ArrayList<>();
        appraisalRanges.add(EAppraisalRange.DREAM);
        appraisalRanges.add(EAppraisalRange.NONE_DREAM);
        return appraisalRanges;
    }
    public static List<EAppraisalRange> getListAppraisalKubuta(){
        List<EAppraisalRange> appraisalRanges = new ArrayList<>();
        appraisalRanges.add(EAppraisalRange.TRACTOR);
        appraisalRanges.add(EAppraisalRange.COMBINE_HARVESTER);
        appraisalRanges.add(EAppraisalRange.WALKING_TRACTOR);
        return appraisalRanges;
    }
}
