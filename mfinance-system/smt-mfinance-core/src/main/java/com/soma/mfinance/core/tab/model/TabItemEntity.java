package com.soma.mfinance.core.tab.model;

import com.soma.frmk.security.model.SecProfile;
import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by k.seang on 3/3/2017.
 */
@Entity
@Table(name = "tu_tab_item",
        indexes = {
        @Index(name = "idx_tab_ite_code", columnList = "tab_ite_code")})
public class TabItemEntity extends EntityRefA {

    private TabItemEntity parent;
    private List<TabItemEntity> children;
    private List<SecProfile> tabProfiles;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tab_ite_id",unique = true, nullable = false)
    public Long getId() {
        return this.id;
    }

    @Column(name = "tab_ite_code", nullable = false, unique = true, length = 50)
    public String getCode() {
        return this.code;
    }

    @Column(name = "tab_ite_desc",nullable = false,length = 200)
    public String getDesc() {
        return this.desc;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_tab_ite_id", nullable = true
    )
    public TabItemEntity getParent() {
        return parent;
    }

    public void setParent(TabItemEntity parent) {
        this.parent = parent;
    }

    @OneToMany( mappedBy = "parent",fetch = FetchType.LAZY)
    @OrderBy("sort_index, id")
    public List<TabItemEntity> getChildren() {
        if(this.children == null) {
            this.children = new ArrayList();
        }
        return children;
    }

    public void setChildren(List<TabItemEntity> children) {
        this.children = children;
    }

    @ManyToMany(fetch=FetchType.LAZY)
    @JoinTable(name="tu_tab_profile",
            joinColumns = { @JoinColumn(name = "tab_ite_id") },
            inverseJoinColumns = { @JoinColumn(name = "sec_pro_id") })
    public List<SecProfile> getTabProfiles() {
        if(tabProfiles == null) {
            tabProfiles = new ArrayList<SecProfile>();
        }
        return tabProfiles;
    }

    public void setTabProfiles(List<SecProfile> tabProfiles) {
        this.tabProfiles = tabProfiles;
    }
}
