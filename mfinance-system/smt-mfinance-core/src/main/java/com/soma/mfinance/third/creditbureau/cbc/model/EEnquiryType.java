package com.soma.mfinance.third.creditbureau.cbc.model;

import org.seuksa.frmk.model.eref.BaseERefData;

import javax.persistence.AttributeConverter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/11/2017
 * Name  : 3:02 PM
 * Email : k.seang@gl-f.com
 */
public class EEnquiryType extends BaseERefData implements AttributeConverter<EEnquiryType, Long> {

    public final static EEnquiryType NA = new EEnquiryType("NA", 1);
    public final static EEnquiryType RV = new EEnquiryType("RV", 2);

    /**
     *
     */
    public EEnquiryType() {
    }

    /**
     * @param code
     * @param id
     */
    public EEnquiryType(String code, long id) {
        super(code, id);
    }

    @SuppressWarnings("unchecked")
    @Override
    public EEnquiryType convertToEntityAttribute(Long id) {
        return super.convertToEntityAttribute(id);
    }

    @Override
    public Long convertToDatabaseColumn(EEnquiryType arg0) {
        return super.convertToDatabaseColumn(arg0);
    }



    /**
     *
     * @return
     */
    public static List<EEnquiryType> values() {
        return getValues(EEnquiryType.class);
    }

    /**
     *
     * @param code
     * @return
     */
    public static EEnquiryType getByCode(String code) {
        return getByCode(EEnquiryType.class, code);
    }

    /**
     *
     * @param id
     * @return
     */
    public static EEnquiryType getById(long id) {
        return getById(EEnquiryType.class, id);
    }

   public static List<EEnquiryType> listEEnquiryType () {
        List<EEnquiryType> enquiryTypes = new ArrayList<EEnquiryType>();
        enquiryTypes.add(NA);
        enquiryTypes.add(RV);
        return enquiryTypes;
    }
}
