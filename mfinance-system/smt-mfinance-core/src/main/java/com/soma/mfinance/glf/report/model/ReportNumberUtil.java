package com.soma.mfinance.glf.report.model;

import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.helper.FinServicesHelper;

/**
 * Created by Dang Dim
 * Date     : 28-Jul-17, 9:51 AM
 * Email    : d.dim@gl-f.com
 */
public class ReportNumberUtil implements FinServicesHelper {


    public static String generateNumber(Dealer dealer, EReportTamplate reportTamplate, int number) {
        String numDigit = "";
        numDigit = String.format("%06d", number);
        StringBuilder sb = new StringBuilder();
        sb.append("GLF-").append(reportTamplate.getCode() + "-").append(dealer.getInternalCode() + "-");
        sb.append(numDigit);
        return sb.toString();
    }


}
