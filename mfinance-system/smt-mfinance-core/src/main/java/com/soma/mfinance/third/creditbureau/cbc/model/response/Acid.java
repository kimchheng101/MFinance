/**
 * 
 */
package com.soma.mfinance.third.creditbureau.cbc.model.response;

import java.io.Serializable;

/**
 * @author tha.bunsath
 *
 */
public class Acid implements Serializable {

	private static final long serialVersionUID = 7397021507187663696L;
	
	private String acid1;
	private String acid2;
	private String acid3;
	private String lang;
	public String getAcid1() {
		return acid1;
	}
	public void setAcid1(String acid1) {
		this.acid1 = acid1;
	}
	public String getAcid2() {
		return acid2;
	}
	public void setAcid2(String acid2) {
		this.acid2 = acid2;
	}
	public String getAcid3() {
		return acid3;
	}
	public void setAcid3(String acid3) {
		this.acid3 = acid3;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	
	

}
