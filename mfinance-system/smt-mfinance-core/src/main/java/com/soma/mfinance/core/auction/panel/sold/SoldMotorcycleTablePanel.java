package com.soma.mfinance.core.auction.panel.sold;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.auction.model.ContractAuctionData;
import com.soma.mfinance.core.auction.service.AuctionService;
import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.service.ContractService;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.RowRenderer;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.data.Item;
import com.vaadin.ui.Table;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;

/**
 * AUTHOR : kimsuor.seang
 * EMAIL  : chhaichivon1995@gmail.com
 * DATE   : 8/29/2017
 * TIME   : 10:31 AM
 * MODIFIED BY : THEARA SENG (11/24/2017)
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SoldMotorcycleTablePanel extends AbstractTablePanel<Contract> implements FMEntityField {

    @Autowired
    private ContractService contractService;
    @Autowired
    private AuctionService auctionService;

    private double first_year_rate = 0.90;
    private double second_year_rate = 0.72;
    private double third_year_rate = 0.50;
    private double forth_year_rate = 0.50;
    private double fifth_year_rate = 0.50;
    private double refundPremiumRate = 0.90;

    @PostConstruct
    public void PostConstruct() {
        setCaption(I18N.message("sold.motorcycles"));
        setSizeFull();
        setMargin(true);
        setSpacing(true);
        super.init(I18N.message("sold.motorcycles"));
        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addEditClickListener(this);
        navigationPanel.addRefreshClickListener(this);

    }

    @Override
    protected PagedDataProvider<Contract> createPagedDataProvider() {
        PagedDefinition<Contract> pagedDefinition = new PagedDefinition<>(searchPanel.getRestrictions());
        pagedDefinition.setRowRenderer(new SoldMotorcycleRowRenderer());
        pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Table.Align.LEFT, 70);
        pagedDefinition.addColumnDefinition("contract.reference", I18N.message("contract.reference").toUpperCase(), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("auction.status", I18N.message("auction.status").toUpperCase(), String.class, Table.Align.LEFT, 130);
        pagedDefinition.addColumnDefinition("applicant.individual.lastNameEn", I18N.message("lastname.en").toUpperCase(), String.class, Table.Align.LEFT, 130);
        pagedDefinition.addColumnDefinition("applicant.individual.firstNameEn", I18N.message("firstname.en").toUpperCase(), String.class, Table.Align.LEFT, 130);
        pagedDefinition.addColumnDefinition("asset.model.descEn", I18N.message("model").toUpperCase(), String.class, Table.Align.LEFT, 130);
        pagedDefinition.addColumnDefinition("asset.model.year", I18N.message("year").toUpperCase(), Integer.class, Table.Align.LEFT, 120);
        pagedDefinition.addColumnDefinition("asset.color.descEn", I18N.message("color").toUpperCase(), String.class, Table.Align.LEFT, 120);
        pagedDefinition.addColumnDefinition("asset.engineNumber", I18N.message("engine.number").toUpperCase(), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("asset.chassisNumber", I18N.message("chassis.number").toUpperCase(), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("asset.plateNumber", I18N.message("plate.number").toUpperCase(), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("startDate", I18N.message("contract.date").toUpperCase(), Date.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("insuranceStartDate" , I18N.message("insurance.start.date").toUpperCase(), Date.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("insuranceEndYear" , I18N.message("insurance.end.date").toUpperCase(), Date.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("date.selling", I18N.message("date.of.sells").toUpperCase(), Date.class, Table.Align.LEFT, 100 );
        pagedDefinition.addColumnDefinition("term", I18N.message("term").toUpperCase(), Integer.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("year.no", I18N.message("year.no").toUpperCase(), Integer.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("asset.tiAssetApprPrice", I18N.message("asset.price").toUpperCase(), Double.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("date.of.repossession", I18N.message("date.of.repossession").toUpperCase(), Date.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("principle.balance", I18N.message("principle.balance").toUpperCase(), Double.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("air", I18N.message("air").toUpperCase(), Double.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("num.of.auction", I18N.message("num.of.auction").toUpperCase(), Long.class, Table.Align.LEFT, 150);

        EntityPagedDataProvider<Contract> pagedDataProvider = new EntityPagedDataProvider<>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    @Override
    public Item getSelectedItem() {
        return super.getSelectedItem();
    }

    @Override
    protected AbstractSearchPanel<Contract> createSearchPanel() {
        return new SoldMotorcycleSearchPanel(this);
    }

    @Override
    protected Contract getEntity() {
        final Long id = getItemSelectedId();
        if (id != null) {
            return ENTITY_SRV.getById(Contract.class, id);
        }
        return null;
    }

    private class SoldMotorcycleRowRenderer implements RowRenderer {
        @SuppressWarnings("unchecked")
        @Override
        public void renderer(Item item, org.seuksa.frmk.model.entity.Entity entity) {
            Contract contract = (Contract) entity;
            Asset asset = contract.getAsset();
            ContractAuctionData contractAuctionData = contract.getContractAuctionData();
            Quotation quotation = contract.getQuotation();
            Collection collection = contract.getCollection();

            item.getItemProperty(ID).setValue(contract.getId());
            item.getItemProperty("contract.reference").setValue(contract.getReference());
            item.getItemProperty("auction.status").setValue(contract.getAuctionStatus() != null ? contract.getAuctionStatus().getDesc() : "");

            Applicant applicant = contract.getQuotation().getMainApplicant();
            item.getItemProperty("applicant.individual.lastNameEn").setValue(applicant.getLastNameEn());
            item.getItemProperty("applicant.individual.firstNameEn").setValue(applicant.getFirstNameEn());

            if (asset != null) {
                item.getItemProperty("asset.model.descEn").setValue(asset.getModel().getDescEn());
                item.getItemProperty("asset.model.year").setValue(asset.getModel().getYear());
                item.getItemProperty("asset.color.descEn").setValue(asset.getColor().getDescEn());
                item.getItemProperty("asset.engineNumber").setValue(asset.getEngineNumber());
                item.getItemProperty("asset.chassisNumber").setValue(asset.getChassisNumber());
                item.getItemProperty("asset.plateNumber").setValue(asset.getPlateNumber());
                item.getItemProperty("asset.tiAssetApprPrice").setValue(asset.getTiAssetApprPrice());
            }

            item.getItemProperty("startDate").setValue(contract.getStartDate());
            if(quotation != null){
                item.getItemProperty("insuranceStartDate").setValue(quotation.getInsuranceStartDate());
                item.getItemProperty("insuranceEndYear").setValue(DateUtils.addMonthsDate(quotation.getInsuranceStartDate(), contract.getTerm()));
            }

            item.getItemProperty("date.selling").setValue(contractAuctionData.getSellingDate());
            item.getItemProperty("term").setValue(contract.getTerm());
            Date repossessDate = contract.getContractAuctionData().getRequestRepossessedDate();
            int yearNo = 0;
            if(quotation.getInsuranceStartDate() != null){
                yearNo = DateUtils.getNumberYearOfTwoDates(repossessDate, quotation.getInsuranceStartDate());
            }
            item.getItemProperty("year.no").setValue(yearNo);
            item.getItemProperty("date.of.repossession").setValue(repossessDate);
            item.getItemProperty("principle.balance").setValue(auctionService.getRemainingPrincipalBalance(collection));

            item.getItemProperty("air").setValue(contractService.getUnpaidAIR(contract));
            item.getItemProperty("num.of.auction").setValue(auctionService.getNumberOfAuction(contract));

        }

    }

}

