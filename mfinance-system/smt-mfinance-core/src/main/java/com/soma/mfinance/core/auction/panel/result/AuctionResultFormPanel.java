package com.soma.mfinance.core.auction.panel.result;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.auction.model.Buyer;
import com.soma.mfinance.core.auction.model.ContractAuctionData;
import com.soma.mfinance.core.auction.service.AuctionService;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.workflow.AuctionWkfStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Order;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.Entity;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.vaadin.dialogs.ConfirmDialog;

import javax.annotation.PostConstruct;
import static com.soma.mfinance.core.applicant.service.ValidateNumbers.validateNumberDot;

/**
 * Auction Result Form panel
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AuctionResultFormPanel extends AbstractFormPanel implements Button.ClickListener, ConfirmDialog.Listener {

	private static final long serialVersionUID = -1233781682052124351L;
	
	@Autowired
	private AuctionService auctionService;
	
	private EntityRefComboBox<Buyer> cbxBuyer;
	private TextField txtSellingPrice;
	private AutoDateField dfSellingDate;
	private Button btnSold;
	private Button btnCannotSell;
	
	private Contract contract;
	private EWkfStatus auctionStatus;
	private AuctionResultPanel panel;

	@PostConstruct
	public void PostConstruct() {
        super.init();
	}

	@Override
	protected com.vaadin.ui.Component createForm() {
		btnSold = new NativeButton(I18N.message("sold"));
		btnSold.addClickListener(this);
		btnCannotSell = new NativeButton(I18N.message("cannot.sell"));
		btnCannotSell.addClickListener(this);

		NavigationPanel navigationPanel = addNavigationPanel();
		navigationPanel.addSaveClickListener(this);
		navigationPanel.addButton(btnCannotSell);
		navigationPanel.addButton(btnSold);

		cbxBuyer = new EntityRefComboBox<Buyer>(I18N.message("buyer"));
		BaseRestrictions<Buyer> restrictions = new BaseRestrictions<Buyer>(Buyer.class);
		restrictions.addOrder(Order.asc("firstNameEn"));
		cbxBuyer.setRestrictions(restrictions);
		cbxBuyer.setWidth(200, Unit.PIXELS);
		cbxBuyer.renderer();

		txtSellingPrice = new TextField(I18N.message("selling.price"));
		txtSellingPrice.setWidth(200, Unit.PIXELS);
		validateNumberDot(txtSellingPrice);

		dfSellingDate = ComponentFactory.getAutoDateField(I18N.message("selling.date"), false);
		dfSellingDate.setDateFormat("dd/MM/yyyy");
		dfSellingDate.setValue(DateUtils.today());
		FormLayout formLayout = new FormLayout();
		formLayout.addComponent(cbxBuyer);
		formLayout.addComponent(txtSellingPrice);
		formLayout.addComponent(dfSellingDate);

		return formLayout;
	}

	/**
	 * @see AbstractFormPanel#getEntity()
	 */
	@Override
	protected Entity getEntity() {
		ContractAuctionData contractAuctionData = contract.getContractAuctionData();
		if (contractAuctionData == null) {
			contractAuctionData = new ContractAuctionData();
			contractAuctionData.setContract(contract);
		}
		contractAuctionData.setBuyer(cbxBuyer.getSelectedEntity());
		contractAuctionData.setTiAssetSellingPrice(getDataFormTextField(txtSellingPrice.getValue()));
		contractAuctionData.setSellingDate(dfSellingDate.getValue());

		return contractAuctionData;
	}
	/**
	 * @see AbstractFormPanel#saveEntity()
	 */
	@Override
	public void saveEntity() {
		ENTITY_SRV.saveOrUpdate(getEntity());
		contract = ENTITY_SRV.getById(Contract.class, contract.getId());
	}

	/**
	 * @param id
	 */
	public void assignValues (Long id) {
		reset();
		this.contract = ENTITY_SRV.getById(Contract.class, id);
		if (contract.getContractAuctionData() != null) {
			ContractAuctionData auctionData = contract.getContractAuctionData();
			cbxBuyer.setSelectedEntity(auctionData.getBuyer());
			txtSellingPrice.setValue(auctionData.getTiAssetSellingPrice() == null ? "" : AmountUtils.format(auctionData.getTiAssetSellingPrice()) );
			dfSellingDate.setValue(auctionData.getSellingDate());
		}
	}

	@Override
	public void reset() {
		super.reset();
		this.contract = null;
		cbxBuyer.setSelectedEntity(null);
		txtSellingPrice.setValue(null);
		dfSellingDate.setValue(null);
	}

	@Override
	public Double getDouble (AbstractTextField textField) {
		if (textField.getValue() == null) {
			return 0d;
		}
		return super.getDouble(textField);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		super.reset();
		boolean validate = true;
		String statusName = "";

		if (event.getButton() == btnSold) {
			statusName = I18N.message("sold");
			auctionStatus = AuctionWkfStatus.SOL;
			validate = auctionValidate();
		} else if (event.getButton() == btnCannotSell) {
			statusName = I18N.message("cannot.sell");
			auctionStatus = AuctionWkfStatus.CNS;
		}

		if (validate) {
			ConfirmDialog confirmDialog = ConfirmDialog.show(
					UI.getCurrent(),
					I18N.message("confirm"),
					I18N.message("confirm.auction", statusName),
					I18N.message("ok"),
					I18N.message("cancel"),
					this);
			confirmDialog.setWidth("400px");
			confirmDialog.setHeight("150px");
			confirmDialog.setImmediate(true);
		} else {
			displayErrors();
		}
	}

	private boolean auctionValidate () {
		if (contract != null) {
			 ContractAuctionData auctionData = contract.getContractAuctionData();

			 if (auctionData == null) {
				 this.errors.add(I18N.message("field.required.1", new String[] { I18N.message("all.field") }));
			 } else {
				 checkDouble(auctionData.getRepossessionFeeUsd(), "repossession.fee");
				 checkDouble(auctionData.getCollectionFeeUsd(), "collection.fee");
				 checkDouble(auctionData.getTiAssetSellingPrice(), "selling.price");
				 checkMandatorySelectField(cbxBuyer, "buyer");
				 checkMandatoryField(txtSellingPrice, "selling.price");
				 checkMandatoryDateField(dfSellingDate, "selling.date");
			 }
		}

		return errors.isEmpty();
	}

	/**
	 * @see AbstractFormPanel#validate()
	 */
	@Override
	protected boolean validate() {
		super.reset();
		validateDouble(txtSellingPrice.getValue(), "selling.price");
		return errors.isEmpty();
	}

	private void checkDouble (Double value, String messageKey) {
		if (value == null || value < 0) {
			this.errors.add(I18N.message("field.required.1", new String[] { I18N.message(messageKey) }));
		}
	}

	private void validateDouble (String value, String messageKey) {
		if (value != null && StringUtils.isNotEmpty(value)) {
			try {
				Double.parseDouble(value);
			} catch (NumberFormatException e) {
				this.errors.add(I18N.message("field.invalid.format", new String[] { I18N.message(messageKey) }));
			}
		}
	}

	@Override
	public void onClose(ConfirmDialog dialog) {
		if (dialog.isConfirmed()) {
			saveEntity();
			auctionService.changeAuctionStatus(contract, auctionStatus);
			panel.displayTablePanel();
		}
	}
	
	/**
	 * Set Table panel
	 * @param panel
	 */
	public void setMainPanel (AuctionResultPanel panel) {
		this.panel = panel;
	}
	
	public Double getDataFormTextField(String data){
		if(data == null || StringUtils.isEmpty(data)){
			return 0d;
		}
		return Double.valueOf(data.replaceAll(",", "").toString());
	}

}
