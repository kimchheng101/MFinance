package com.soma.mfinance.core.asset.model;

import com.soma.mfinance.core.quotation.model.Quotation;
import org.seuksa.frmk.model.entity.EntityA;
import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;

/**
 * @author tha.bunsath
 * 
 */
@Entity
@Table(name = "td_multi_asset_chassis_doc")
public class AddMultiAssetChassisDoc extends EntityA {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4211680681646727130L;
	/** */
	private Quotation quotation;
	private String assetChassisDc;
	/**
	 * @return the
	 */
	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "amti_chas_id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}
	/* 
	 * @return
	 */
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "quota_id")
	public Quotation getQuotation() {
		return quotation;
	}
	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}
	/**
	 * 
	 * @return
	 */
	@Column(name = "amti_chassis_dc", nullable = true, length = 100)
	public String getAssetChassisDc() {
		return assetChassisDc;
	}
	public void setAssetChassisDc(String assetChassisDc) {
		this.assetChassisDc = assetChassisDc;
	}
}
