package com.soma.mfinance.third.creditbureau.cbc.service.impl;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.applicant.model.Employment;
import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.mfinance.core.quotation.SequenceGenerator;
import com.soma.mfinance.core.quotation.impl.SequenceGeneratorCBCForMfpImpl;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationDocument;
import com.soma.mfinance.core.shared.quotation.SequenceManager;
import com.soma.mfinance.third.conf.ModuleConfig;
import com.soma.mfinance.third.creditbureau.cbc.XmlBinder;
import com.soma.mfinance.third.creditbureau.cbc.model.*;
import com.soma.mfinance.third.creditbureau.cbc.model.request.*;
import com.soma.mfinance.third.creditbureau.cbc.model.response.Error;
import com.soma.mfinance.third.creditbureau.cbc.model.response.Item;
import com.soma.mfinance.third.creditbureau.cbc.model.response.Response;
import com.soma.mfinance.third.creditbureau.cbc.service.CBCService;
import com.soma.mfinance.third.creditbureau.exception.ErrorCreditBureauException;
import com.soma.mfinance.third.creditbureau.exception.InvokedCreditBureauException;
import com.soma.mfinance.third.creditbureau.exception.ParserCreditBureauException;
import com.soma.ersys.core.hr.model.address.Address;
import com.soma.frmk.config.model.SettingConfig;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.lang.StringUtils;
import org.jibx.runtime.JiBXException;
import org.seuksa.frmk.service.EntityService;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * CBC Service
 *
 * @author kimsuor.seang
 */
@Service("cbcService")
public class CBCServiceImpl implements CBCService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private EntityService entityService;

    public static String REFERENCE_CODE  = "MFC";
    public static String LANG_KH = "kh";
    public static String CADT_RESID= "RESID";
    public static String ZIP_CAM = "855";
    public static String NATION_CODE = "N";

    /**
     * Inquiry data
     *
     * @param quotation
     * @param parameters
     * @return
     */
    public String enquiry(Quotation quotation, Map<String, Object> parameters) throws InvokedCreditBureauException {

        EEnquiryType enquiryType = (EEnquiryType) parameters.get(EEnquiryType.class.toString());
        EProductType productType = (EProductType) parameters.get(EProductType.class.toString());
        EAccountType accountType = (EAccountType) parameters.get(EAccountType.class.toString());

        Configuration config = ModuleConfig.getInstance().getConfiguration();
        String nquiryReference = StringUtils.EMPTY;
        String  runNo = StringUtils.EMPTY;
        SettingConfig cbcRunSetting = entityService.getByCode(SettingConfig.class, "cbc_run_no_mfp");
        if (cbcRunSetting == null) {
            String message = "Could not process CBC interface, please contact administrator !";
            logger.error(message + " [CBC_RUN_NO is not well configured in table TU_PARAMETER]");
            throw new InvokedCreditBureauException(message);
        } else {
            logger.debug("Current CBC Sequence Number = " + cbcRunSetting.getValue());
            long cbcReferenceNumber = SequenceManager.getInstance().getCbcReferenceNumber();
            if (cbcReferenceNumber <= 0) {
                logger.debug("Sequence Number Not initiliazed, get one from database");
                cbcReferenceNumber = Long.parseLong(cbcRunSetting.getValue());
                logger.debug("Current Sequence Number from database = " + cbcReferenceNumber);
            }
            SequenceManager.getInstance().setCbcReferenceNumber(cbcReferenceNumber + 1);
            SequenceGenerator sequenceGenerator = new SequenceGeneratorCBCForMfpImpl(REFERENCE_CODE, cbcReferenceNumber);
            nquiryReference = sequenceGenerator.generate();
            //String [] cbcRef = nquiryReference.split("-");
            //runNo = cbcRef [0];
    }

       // String runNo = leftPad("0000000" + cbcRunSetting.getValue(), 7);

        logger.debug("[CBC - enquiryType] : " + enquiryType);
        logger.debug("[CBC - productType] : " + productType);
        logger.debug("[CBC - accountType] : " + accountType);
        logger.debug("[CBC - runNo] : " + runNo);

        EApplicantType applicantType = parameters.containsKey(EApplicantType.class.toString()) ? (EApplicantType) parameters.get(EApplicantType.class.toString()) : EApplicantType.C;

        boolean isGuarantor = applicantType == EApplicantType.G;

        Request request = new Request();
        request.setService(Request.SERVICE);
        request.setAction(Request.ACTION);

        // Generate header
        Header header = new Header();
        request.setHeader(header);
        header.setMemberId(config.getString("creditbureau.cbc.member_id"));
        header.setUserId(config.getString("creditbureau.cbc.user_id"));
        header.setRunNo(runNo);
        header.setTotItems(1);

        // Generate message
        Message message = new Message();
        request.setMessage(message);
        Enquiry enquiry = new Enquiry();
        message.setEnquiry(enquiry);
        enquiry.setEnquiryType(enquiryType.getCode());
        enquiry.setProductType(productType.getCode());
        enquiry.setNoOfApplicant(1);
        enquiry.setAccountType(accountType.getCode());
        enquiry.setEnquiryReference(nquiryReference);
        enquiry.setLanguage("");
        enquiry.setAmount(MyNumberUtils.getDouble(quotation.getTiFinanceAmount()));
        enquiry.setCurrency(config.getString("creditbureau.cbc.currency"));
        Consumer consumer = new Consumer();
        enquiry.setConsumer(consumer);

        Applicant applicant = isGuarantor ? quotation.getGuarantor() : quotation.getApplicant();
        Individual customer = applicant.getIndividual();

        List<QuotationDocument> documents = quotation.getQuotationDocuments(applicantType);
        Collections.sort(documents, new DocumentComparatorBySortIndex());

        List<Cid> cids = new ArrayList<Cid>();
        //int maxDoc = documents.size() <= 3 ? documents.size() : 3;
        for (QuotationDocument document : documents) {
            if(document.getDocument().isSubmitCreditBureau()) {
                Cid cid = new Cid();
                cid.setCid1(document.getDocument().getCode());
                cid.setCid2(document.getReference());
                if (document.getExpireDate() != null) {
                    Cid3 cid3 = new Cid3();
                    cid.setCid3(cid3);
                    cid3.setCid3d(leftPad("0" + DateUtils.getDay(document.getExpireDate()), 2));
                    cid3.setCid3m(leftPad("0" + DateUtils.getMonth(document.getExpireDate()), 2));
                    cid3.setCid3y(leftPad("0" + DateUtils.getYear(document.getExpireDate()), 4));
                }
                cids.add(cid);
            }
        }

        Cdob cdob = new Cdob();
        Cplb cplb = new Cplb();
        if (customer != null) {
            cdob.setCdbd(leftPad("0" + DateUtils.getDay(customer.getBirthDate()), 2));
            cdob.setCdbm(leftPad("0" + DateUtils.getMonth(customer.getBirthDate()), 2));
            cdob.setCdby(leftPad("0" + DateUtils.getYear(customer.getBirthDate()), 4));
            if (customer.getPlaceOfBirth() != null) {
                cplb.setCplbc(customer.getPlaceOfBirth().getCountry().getCode());
                cplb.setCplbp(customer.getPlaceOfBirth().getCode());
            }
        }


        Cnam cnam = new Cnam();
        cnam.setCnmfa(customer.getLastName());
        cnam.setCnm1a(customer.getFirstName());
        cnam.setLang(LANG_KH);

        cnam.setCnmfe(customer.getLastNameEn());
        cnam.setCnm1e(customer.getFirstNameEn());

        Address customerAddress = customer.getMainAddress();

        Cadr cadr = new Cadr();
        cadr.setCadt(CADT_RESID);
        //cadr.setCadt("POST"); /*--- Update for new version 1.7*/
        String cusProvinceCode = "";
        String cusDistrictCode = "";
        String cusCommuneCode = "";
        String cusVillageCode = "";
        if (customerAddress != null) {
            CBCAddressCode cbcAddressCode = CBCWrongAddressCode.invalidCBCAddress(customerAddress);
            cusProvinceCode = customerAddress.getProvince().getCode();
            cusDistrictCode = customerAddress.getDistrict().getCode();
            cusCommuneCode = customerAddress.getCommune().getCode();
            cusVillageCode = customerAddress.getVillage().getCode();
            if (cbcAddressCode != null) {
                cusProvinceCode = cbcAddressCode.getProvinceCode();
                cusDistrictCode = cbcAddressCode.getDistrictCode();
                cusCommuneCode = cbcAddressCode.getCommuneCode();
            }
        }


        cadr.setCadpr(cusProvinceCode);
        cadr.setCadds(cusDistrictCode);
        cadr.setCadcm(cusCommuneCode);
        cadr.setCadvl(cusVillageCode);

        if (customerAddress != null && customerAddress.getCountry() != null && customerAddress.getCountry().getCode() != null) {
            cadr.setCad9(customerAddress.getCountry().getCode());
        }

        cadr.setLang(LANG_KH);
        /*cadr.setCad7("");*/
        Ccnt ccnt = new Ccnt();
        ccnt.setCcn1("M");
        ccnt.setCcn2(ZIP_CAM);
        ccnt.setCcn4(customer.getMobilePerso());

        if (customer.getCurrentEmployment() != null) {
            Employment employment = customer.getCurrentEmployment();
            Address employmentAddress = employment.getAddress();
            String empProvinceCode = "";
            String empDistrictCode = "";
            String empCommuneCode = "";
            String empVillageCode = "";
            if (employmentAddress != null) {
                empProvinceCode = employmentAddress.getProvince() != null ? employmentAddress.getProvince().getCode() : "";
                empDistrictCode = employmentAddress.getDistrict() != null ? employmentAddress.getDistrict().getCode() : "";
                empCommuneCode = employmentAddress.getCommune() != null ? employmentAddress.getCommune().getCode() : "";
                empVillageCode = employmentAddress.getVillage() != null ? employmentAddress.getVillage().getCode() : "";

                CBCAddressCode cbcAddressCode = CBCWrongAddressCode.invalidCBCAddress(employment.getAddress());
                if (cbcAddressCode != null) {
                    empProvinceCode = cbcAddressCode.getProvinceCode();
                    empDistrictCode = cbcAddressCode.getDistrictCode();
                    empCommuneCode = cbcAddressCode.getCommuneCode();
                }
            }
			/*Address employmentAddress = employment.getAddress();*/
            Cemp cemp = new Cemp();
            cemp.setEtyp("C");
            if (employment.getEmploymentStatus() != null)
                cemp.setEslf(employment.getEmploymentStatus().getCode());

            cemp.setEoce(employment.getEmploymentOccupation().getDescEn());
            int nbTimeWithEmpInYear = employment.getTimeWithEmployerInYear() != null ? employment.getTimeWithEmployerInYear() : 0;
            int nbTimeWithEmpInMonth = employment.getTimeWithEmployerInMonth() != null ? employment.getTimeWithEmployerInMonth() : 0;

            int lengthOfServiceInMonths = nbTimeWithEmpInYear * 12 + nbTimeWithEmpInMonth;
            cemp.setElen(lengthOfServiceInMonths);
            cemp.setEtms(employment.getRevenue());
            cemp.setEcurr(config.getString("creditbureau.cbc.currency"));
            cemp.setEnme(employment.getWorkPlaceName());


            Eadr eadr = new Eadr();
            eadr.setEadt("WORK");
            eadr.setEadp(empProvinceCode);
            eadr.setEadd(empDistrictCode);
            eadr.setEadc(empCommuneCode);
            eadr.setEadv(empVillageCode);
            if (employmentAddress.getCountry() != null && employmentAddress.getCountry().getCode() != null) {
                eadr.setEad9(employmentAddress.getCountry().getCode());
            }
            eadr.setLang(LANG_KH);
            cemp.setEadr(eadr);
            cemp.setLang(LANG_KH);

            consumer.setCapl("P");
            consumer.setCids(cids);
            consumer.setCdob(cdob);
            consumer.setCplb(cplb);
            consumer.setCgnd(customer.getGender().getCode());
            consumer.setCmar(customer.getMaritalStatus().getCode());
            consumer.setCnat(customer.getNationality().getCode());
            consumer.setCnam(cnam);
            consumer.setCadr(cadr);
            consumer.setCcnt(ccnt);
            consumer.setCemp(cemp);
        }

        String xmlResponse = null;
        try {
            xmlResponse = enquiry(request);
            validateResponse(xmlResponse);
            // Increase Run No and update into database
            cbcRunSetting.setValue(String.valueOf(SequenceManager.getInstance().getCbcReferenceNumber()));
            entityService.saveOrUpdate(cbcRunSetting);
        } catch (ErrorCreditBureauException e) {
            logger.error("ErrorCreditBureauException", e);
            throw new InvokedCreditBureauException(e.getMessage(), e);
        } catch (ParserCreditBureauException e) {
            logger.error("ParserCreditBureauException", e);
            throw new InvokedCreditBureauException(e.getMessage(), e);
        } catch (IOException e) {
            logger.error("IOException", e);
            throw new InvokedCreditBureauException(e.getMessage(), e);
        }
        return xmlResponse;
    }

    /**
     * @param request
     * @return
     */
    private String enquiry(Request request) throws IOException {
        try {
            String xmlRequest = XmlBinder.marshal(request);
            logger.debug("[CBC Request] : " + xmlRequest);
            String xmlResponse = doHttpPost(xmlRequest);
            logger.debug("[CBC Response] : " + xmlResponse);
            return xmlResponse;
        } catch (JiBXException e) {
            logger.error("[CBC Error] : ", e);
            throw new IllegalArgumentException("[CBC ERROR] Could not process to CBC interface, please contact administrator !");
        }
    }

    /**
     * Do http post
     *
     * @param xmlRequest
     * @return
     * @throws IOException
     */
    private String doHttpPost(String xmlRequest) throws IOException {
        Configuration config = ModuleConfig.getInstance().getConfiguration();
        String url = config.getString("creditbureau.cbc.url");
        if (StringUtils.isEmpty(url)) {
            String message = "URL of CBC is NULL, please contact administrator !";
            logger.error(message + " [mfinance-module-config.xml]");
            throw new IllegalArgumentException(message);
        }

        PostMethod post = new PostMethod(url);
        post.setDoAuthentication(true);
        // Set Request content
        RequestEntity entity = new StringRequestEntity(xmlRequest, null, null);
        post.setRequestEntity(entity);

        int port = config.getInt("creditbureau.cbc.security.port");
        String username = config.getString("creditbureau.cbc.security.username");
        String password = config.getString("creditbureau.cbc.security.password");

        logger.debug(" [CBC Url] = " + url);
        logger.debug(" [CBC Port] = " + port);
        logger.debug(" [CBC Username] = " + username);
        logger.debug(" [CBC password] = ******");

        // Get HTTP client
        HttpClient httpclient = new HttpClient();
        httpclient.getState().setCredentials(
                new AuthScope(null, port, null),
                new UsernamePasswordCredentials(username, password));
        // Execute request
        httpclient.executeMethod(post);
        // Throw an HttpException for status other than success
        if (post.getStatusCode() / 100 != 2) {
            throw new HttpException(post.getStatusText());
        }
        String response = getStringFromInputStream(post.getResponseBodyAsStream());
        // Release current connection to the connection pool once you are done
        post.releaseConnection();
        return response;
    }

    /**
     * Get string from input stream
     *
     * @param is
     * @return
     */
    private String getStringFromInputStream(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            logger.error("IOException", e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    logger.error("IOException", e);
                }
            }
        }
        return sb.toString();

    }

    /**
     * @param value
     * @param size
     * @return
     */
    private String leftPad(String value, int size) {
        return value == null ? null : value.substring(value.length() - size);
    }


    /**
     * Validate the result of credit bureau
     *
     * @param response
     * @throws InvokedCreditBureauException
     * @throws ErrorCreditBureauException
     * @throws ParserCreditBureauException
     */
    public boolean validateResponse(String response) throws InvokedCreditBureauException, ErrorCreditBureauException, ParserCreditBureauException {
        try {
            Response cbResponse = XmlBinder.unmarshal(response);

            com.soma.mfinance.third.creditbureau.cbc.model.response.Message message = cbResponse.getMessage();

            if (message.getParser() != null) {
                String error = "[RSP_MSG]" + message.getParser().getRspmsg() + "[/RSP_MSG]";
                error += "[REQ_DATA]" + message.getParser().getReqdata() + "[/REQ_DATA]";
                throw new ParserCreditBureauException(error);
            } else {
                Item item = message.getItems().get(0);
                if (item.getParser() != null) {
                    String error = "[RSP_MSG]" + item.getParser().getRspmsg() + "[/RSP_MSG]";
                    error += "[REQ_DATA]" + item.getParser().getReqdata() + "[/REQ_DATA]";
                    throw new ParserCreditBureauException(error);
                } else if (item.getErrors() != null) {
                    List<Error> errors = item.getErrors();
                    String errorDesc = "";
                    for (Error error : errors) {
                        errorDesc += "[ERROR]";
                        errorDesc += "[FIELD]" + error.getField() + "[/FIELD]";
                        errorDesc += "<RSP_MSG>" + error.getRspmsg() + "[/RSP_MSG]";
                        errorDesc += "[DATA]" + error.getData() + "[/DATA]";
                        errorDesc += "[/ERROR]";
                    }
                    throw new ErrorCreditBureauException(errorDesc);
                }
            }
        } catch (JiBXException e) {
            throw new InvokedCreditBureauException(e.getMessage(), e);
        }
        return true;
    }

    /**
     * @param response
     * @return
     */
    public String getReference(String response) throws InvokedCreditBureauException {
        String reference = "";
        try {
            Response cbResponse = XmlBinder.unmarshal(response);
            reference = cbResponse.getMessage().getItems().get(0).getEnquiryReference();
        } catch (JiBXException e) {
            throw new InvokedCreditBureauException(e.getMessage(), e);
        }
        return reference;
    }

    /**
     * @author kimsuor.seang
     */
    private class DocumentComparatorBySortIndex implements Comparator<Object> {
        public int compare(Object o1, Object o2) {
            QuotationDocument c1 = (QuotationDocument) o1;
            QuotationDocument c2 = (QuotationDocument) o2;
            if (c1 == null || c1.getDocument().getSortIndex() == null) {
                if (c2 == null || c2.getDocument().getSortIndex() == null) {
                    return 0;
                }
                return 1;
            }
            if (c2 == null || c2.getDocument().getSortIndex() == null) {
                return -1;
            }
            return c1.getDocument().getSortIndex().compareTo(c2.getDocument().getSortIndex());
        }
    }
}
