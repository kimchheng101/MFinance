package com.soma.mfinance.core.callcenter;

import com.soma.frmk.security.model.SecUser;
import org.seuksa.frmk.service.BaseEntityService;

import java.util.Date;
import java.util.List;

/**
 * Call Center service interface
 * @author kimsuor.seang
 */
public interface CallCenterService extends BaseEntityService {

	/**
	 * @param processDate
	 */
	void assignContracts(Date processDate);
	
	/**
	 * 
	 */
	void reassignContracts();
	
	/**
	 * Get staffs by call center profile
	 * @return
	 */
	List<SecUser> getStaffsByCallCenterProfile();
	

}
