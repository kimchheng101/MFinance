package com.soma.mfinance.core.quotation.dao;

import java.util.List;

import org.seuksa.frmk.dao.BaseEntityDao;

import com.soma.mfinance.core.quotation.model.Quotation;

/**
 * Quotation data model access
 * @author kimsuor.seang
 *
 */
public interface QuotationDao extends BaseEntityDao {
	
	List<Quotation> getQuotationByContractStatus();

}
