package com.soma.mfinance.core.common.reference.model;

import com.soma.ersys.core.hr.model.organization.MBasePerson;

/**
 * Meta data of com.soma.mfinance.core.contract.model.BlackList
 * @author kimsuor.seang
 */
public interface MBlackListItem extends MBasePerson {

	public final static String ID = "id";
	public final static String APPLICANTCATEGORY = "applicantCategory";
	public final static String SOURCE = "source";
	public final static String REASON = "reason";
	
	public final static String DETAILS = "details";
	public final static String REMARKS = "remarks";

}
