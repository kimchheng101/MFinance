/**
 * 
 */
package com.soma.mfinance.third.creditbureau.cbc.model.response;

import java.io.Serializable;

/**
 * @author tha.bunsath
 *
 */
public class Totgwoamt implements Serializable {

	private static final long serialVersionUID = -3575582283720846890L;
	private String tot_gwo_curr;
	private Double tot_gwo;
	public String getTot_gwo_curr() {
		return tot_gwo_curr;
	}
	public void setTot_gwo_curr(String tot_gwo_curr) {
		this.tot_gwo_curr = tot_gwo_curr;
	}
	public Double getTot_gwo() {
		return tot_gwo;
	}
	public void setTot_gwo(Double tot_gwo) {
		this.tot_gwo = tot_gwo;
	}

}
