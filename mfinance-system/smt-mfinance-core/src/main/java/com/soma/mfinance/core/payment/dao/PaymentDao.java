package com.soma.mfinance.core.payment.dao;

import org.seuksa.frmk.dao.BaseEntityDao;

/**
 * Payment data model access
 * @author kimsuor.seang
 *
 */
public interface PaymentDao extends BaseEntityDao{

}
