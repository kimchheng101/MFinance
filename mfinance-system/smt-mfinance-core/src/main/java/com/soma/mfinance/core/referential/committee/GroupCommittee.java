package com.soma.mfinance.core.referential.committee;

import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.financial.model.ProductLine;
import com.soma.frmk.security.model.SecUser;
import org.seuksa.frmk.model.entity.EntityA;
import org.seuksa.frmk.model.entity.EntityRefA;
import org.seuksa.frmk.model.entity.RefDataId;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author by kimsuor.seang  on 9/27/2017.
 */

@Entity
@Table(name = "tu_group_committee")
public class GroupCommittee extends EntityA {

    private SecUser secUser;
    private ProductLine productLine;
    private List<GroupCommitteeDetail>groupCommitteeDetails;
    private String desc;


    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "gro_com_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_uer_id")
    public SecUser getSecUser() {
        return secUser;
    }

    public void setSecUser(SecUser secUser) {
        this.secUser = secUser;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pro_lin_id")
    public ProductLine getProductLine() {
        return productLine;
    }

    public void setProductLine(ProductLine productLine) {
        this.productLine = productLine;
    }

    @OneToMany(mappedBy = "groupCommittee", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public List<GroupCommitteeDetail> getGroupCommitteeDetails() {
        if (groupCommitteeDetails == null){
            groupCommitteeDetails = new ArrayList<>();
        }
        return groupCommitteeDetails;
    }

    public void setGroupCommitteeDetails(List<GroupCommitteeDetail> groupCommitteeDetails) {
        this.groupCommitteeDetails = groupCommitteeDetails;
    }
    public void addGroupCommitteeDetail(GroupCommitteeDetail groupCommitteeDetail){
        if (groupCommitteeDetail !=null ){
            getGroupCommitteeDetails().add(groupCommitteeDetail);
        }
    }

    @Transient
    public String getDesc() {
        if(secUser != null){
            desc = secUser.getDesc();
        }
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public boolean isAppraisalHasAssetRange(AssetRange assetRange){
        for (GroupCommitteeDetail groupCommitteeDetail : groupCommitteeDetails){
            if (assetRange.getId().equals(groupCommitteeDetail.getAssetRange().getId())){
                return true;
            }
        }
        return false;
    }
}
