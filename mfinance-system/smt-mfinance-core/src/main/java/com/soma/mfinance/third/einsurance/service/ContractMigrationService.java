package com.soma.mfinance.third.einsurance.service;

import org.seuksa.frmk.service.BaseEntityService;

import java.util.Set;

/**
 * Created by Kimsuor SEANG
 * Date  : 10/19/2017
 * Name  : 9:04 AM
 * Email : k.seang@gl-f.com
 */
public interface ContractMigrationService extends BaseEntityService {

    String migrationToEinsurance(Long rquotationId);

    Set<String> migrationToEinsurance(Set<Long> listRefNumber);
}
