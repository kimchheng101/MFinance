package com.soma.mfinance.core.collection.model;

import org.seuksa.frmk.model.entity.MEntityA;


/**
 * Meta data of com.soma.mfinance.core.collection.model.ContractFlag
 * @author kimsuor.seang
 */
public interface MContractFlag extends MEntityA {
	
	public final static String CONTRACT = "contract";
	public final static String FLAG = "flag";
	public final static String LOCATION = "location";
	public final static String OTHERLOCATIONVALUE = "otherLocationValue";
	public final static String COMMENT = "comment";
	public final static String DATE = "date";
	public final static String ACTIONDATE = "actionDate";
	public final static String COMMPLETED = "completed";

}
