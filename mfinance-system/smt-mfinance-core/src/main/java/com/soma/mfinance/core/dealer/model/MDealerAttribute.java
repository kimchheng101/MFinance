package com.soma.mfinance.core.dealer.model;


/**
 * Meta data of DealerAttribute
 * @author kimsuor.seang
 */
public interface MDealerAttribute {
	
	public final static String DEALER = "dealer";
	public final static String ASSETMAKE = "assetMake";
	public final static String ASSETCATEGORY = "assetCategory";
	public final static String INSURANCECOVERAGEDURATION = "insuranceCoverageDuration";
	public final static String CONTRACTFEE = "contractFee";
	public final static String CONTRACTFEECHARGEPOINT = "contractFeeChargePoint";
	public final static String TICOMMISSION1AMOUNT = "tiCommission1Amount";
	public final static String COMMISSION2ENABLED = "commission2Enabled";
	public final static String PAYMENTMETHOD = "paymentMethod";
	
}
