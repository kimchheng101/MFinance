package com.soma.mfinance.tools.report.service;

import com.soma.mfinance.tools.report.Report;
import com.soma.mfinance.core.shared.report.ReportParameter;


/**
 * @author kimsuor.seang
 */
public interface ReportService {
	
	/**
	 * @param report
	 * @param reportParameter
	 */
	String extract(Class<? extends Report> report, ReportParameter reportParameter) throws Exception;
}
