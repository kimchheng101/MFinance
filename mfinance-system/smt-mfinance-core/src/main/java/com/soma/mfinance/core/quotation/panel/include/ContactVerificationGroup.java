package com.soma.mfinance.core.quotation.panel.include;

import com.soma.mfinance.core.model.system.ContactVerification;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.TextArea;

import java.io.Serializable;

/**
 * @author kimsuor.seang
 */
public class ContactVerificationGroup implements Serializable {
	private static final long serialVersionUID = -8557089106457426889L;

	private CheckBox cbContactVerification;
	private TextArea txtValue;
	
	public ContactVerificationGroup(ContactVerification contactVerification) {
		cbContactVerification = new CheckBox(contactVerification.getDescEn());
		cbContactVerification.setImmediate(true);
		cbContactVerification.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 8355973254515740463L;
			@Override
			public void valueChange(ValueChangeEvent event) {
			}
		});
		cbContactVerification.setData(contactVerification);			
		txtValue = new TextArea();
		txtValue.setWidth(300, Unit.PIXELS);
		txtValue.setHeight(100, Unit.PIXELS);
	}

	
	public void setEnable(boolean enabled){
		cbContactVerification.setEnabled(enabled);
		txtValue.setEnabled(enabled);
	}
	
	/**
	 * @return the cbContactVerification
	 */
	public CheckBox getCbContactVerification() {
		return cbContactVerification;
	}

	/**
	 * @return the txtValue
	 */
	public TextArea getTxtValue() {
		return txtValue;
	}
	

}
