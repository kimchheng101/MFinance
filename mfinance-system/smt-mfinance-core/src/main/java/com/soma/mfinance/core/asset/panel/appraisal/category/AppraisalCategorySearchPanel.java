package com.soma.mfinance.core.asset.panel.appraisal.category;

import com.soma.mfinance.core.asset.model.appraisal.AppraisalCategory;
import com.soma.mfinance.core.asset.model.appraisal.EAppraisalType;
import com.soma.mfinance.core.shared.asset.AssetEntityField;
import com.soma.mfinance.core.shared.component.ComponentLayoutFactory;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.statusrecord.StatusRecordField;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;

/**
 * 
 * @author vi.sok
 *
 */
public class AppraisalCategorySearchPanel extends AbstractSearchPanel<AppraisalCategory> implements AssetEntityField {

	/** */
	private static final long serialVersionUID = -4160384210454030290L;

	private TextField txtDescEn;
	private TextField txtCode;
	private ERefDataComboBox<EAppraisalType> cbxEAppraisalType;
	private StatusRecordField statusRecordField;
	
	/**
	 * 
	 * @param appraisalCategoryTablePanel
	 */
	public AppraisalCategorySearchPanel(AppraisalCategoryTablePanel appraisalCategoryTablePanel) {
		super(I18N.message("search"), appraisalCategoryTablePanel);
	}
	
	/**
	 * @see AbstractSearchPanel#reset()
	 */
	@Override
	protected void reset() {
		txtDescEn.setValue("");
		statusRecordField.clearValues();
	}

	/**
	 * @see AbstractSearchPanel#createForm()
	 */
	@Override
	protected Component createForm() {
		GridLayout gridLayout = new GridLayout(12, 1);
		gridLayout.setSpacing(true);


		txtDescEn = ComponentFactory.getTextField(null, false, 60, 180);
		cbxEAppraisalType = new ERefDataComboBox<>(EAppraisalType.class);
		txtCode = ComponentFactory.getTextField(null, false, 60, 180);
		statusRecordField = new StatusRecordField();


        int iCol = 0;
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(ComponentLayoutFactory.getLabelCaption("code"), iCol++, 0);
        gridLayout.addComponent(txtCode, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(ComponentLayoutFactory.getLabelCaption("name"), iCol++, 0);
        gridLayout.addComponent(txtDescEn, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(ComponentLayoutFactory.getLabelCaption("appraisal.type"), iCol++, 0);
        gridLayout.addComponent(cbxEAppraisalType, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(statusRecordField, iCol++, 0);
		return gridLayout;
	}

	/**
	 * @see AbstractSearchPanel#getRestrictions()
	 */
	@Override
	public BaseRestrictions<AppraisalCategory> getRestrictions() {
		BaseRestrictions<AppraisalCategory> restrictions = new BaseRestrictions<>(AppraisalCategory.class);
		if (cbxEAppraisalType.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("eAppraisalType", cbxEAppraisalType.getSelectedEntity()));
		}
		if (StringUtils.isNotEmpty(txtCode.getValue())) { 
			restrictions.addCriterion(Restrictions.like(CODE, txtCode.getValue(), MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotEmpty(txtDescEn.getValue())) { 
			restrictions.addCriterion(Restrictions.like(DESC_EN, txtDescEn.getValue(), MatchMode.ANYWHERE));
		}
		
		if (statusRecordField.isInactiveAllValues()) {
			restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
			restrictions.getStatusRecordList().add(EStatusRecord.INACT);
		} 
		if (statusRecordField.getActiveValue()) {
			restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
		}
		if (statusRecordField.getInactiveValue()) {
			restrictions.getStatusRecordList().add(EStatusRecord.INACT);
		}
		
		return restrictions;
	}
}
