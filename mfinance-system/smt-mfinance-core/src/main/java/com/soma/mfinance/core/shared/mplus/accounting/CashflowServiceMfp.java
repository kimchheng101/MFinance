package com.soma.mfinance.core.shared.mplus.accounting;

import java.util.Date;
import java.util.List;

import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.finance.services.shared.Schedule;

/**
 * 
 * @author vi.sok
 * @since 05/05/2017
 */
public interface CashflowServiceMfp extends BaseEntityService {
	/**
	 * 
	 * @param contract
	 * @return
	 */
	List<Schedule> getCashflows(Contract contract);
	/**
	 * 
	 * @param quotation
	 * @param firstPaymentDate
	 * @return
	 */
	List<Schedule> getCashflows(Quotation quotation, Date firstPaymentDate);
}
