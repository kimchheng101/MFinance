package com.soma.mfinance.glf.report.xls;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/27/2017
 * Name  : 11:29 AM
 * Email : k.seang@gl-f.com
 */
public interface GuaranteeContractFields extends  BaseContractFields{

//    public static final String CHECKING_SYMBOL = "V";
//    public static final String CHECKING_SPACE  = "  ";
    public static final String DATE = "date";

    public static final String IS_LESSEEI_DCARD = "isLesseeIdCard";
    public static final String IS_LESSEEID_CARD = "isLesseeIdCard";
    public static final String ISLESSEE_RESIDENTBOOKNUM = "isLesseeResidentBookNum";
    public static final String ISLESSEERESIDENT_BOOKNUM = "isLesseeResidentBookNum";
    public static final String ISLESSEE_CERTIFYLETTERNUM = "isLesseeCertifyLetterNum";
    public static final String ISLESSEE_FAMILYBOOKNUM = "isLesseeFamilyBookNum";
    public static final String ISLESSEE_BIRTHCERTIFICATENUM = "isLesseeBirthCertificateNum";

    public static final String GUARANTEE_RESIDENTBOOKNUM = "guaranteeResidentBookNum";
    public static final String GUARANTEE_NAME = "guaranteeName";
    public static final String GUARANTEE_GENDER = "guaranteeSex";
    public static final String GUARANTEE_DATE_BIRTHDAY = "guaranteeDateBirthday";
    public static final String GUARANTEE_CARD = "guaranteeCard";
    public static final String GUARANTEE_AGREENUM = "guaranteeAgreeNum";
    //String GUARANTEE_AGREEDATE = "guaranteeAgreeDate";
    public static final String GUARANTEE_RENTING = "guaranteeRenting";
    public static final String GUARANTEE_VEHICLEMODEL = "guaranteeVehicleModel";
    public static final String GUARANTEE_CERTIFYLETTERNUM = "guaranteeCertifyLetterNum";
    public static final String GUARANTEE_FAMILYBOOKNUM = "guaranteeFamilyBookNum";
    public static final String GUARANTEE_BIRTHCERTIFICATENUM = "guaranteeBirthCertificateNum";
    public static final String GUARANTEE_PHONENUM1 = "guaranteePhoneNum1";
    public static final String GUARANTEE_PHONENUM2 = "guaranteePhoneNum2";
    public static final String GUARANTEE_INCOME = "guaranteeIncome";
    public static final String GUARANTEE_CONTRACTPAYMENT = "guaranteeContractPayment";
    public static final String GUARANTEE_VEHICLECOLOR = "guaranteeVehicleColor";
    public static final String GUARANTEE_VEHICLEMANUFACTUREDATE = "guaranteeVehicleManufactureDate";
    public static final String GUARANTEE_MARK = "guaranteeMark";
    public static final String GUARANTEE_VEHICLECC = "guaranteeVehicleCc";
    public static final String GUARANTEE_VEHICLEENGINENUM = "guaranteeVehicleEngineNum";
    public static final String GUARANTEE_VEHICLESERIENUM = "guaranteeVehicleSerieNum";
    public static final String GUARANTEE_VEHICLEGENDER = "guaranteeVehicleGender";
    public static final String GUARANTEE_VEHICLEBRAND = "guaranteeVehicleBrand";
    public static final String GUARANTEE_ASSETPRODUCT = "guaranteeAssetProduct";
    public static final String GUARANTEE_RELATIONSHIP_CONTACT = "guaranteeRelationshipContact";

    public static final String GUARANTEE_APPLICANTNAME = "applicantName";
    public static final String GUARANTEE_APPLICANTBIRTHDATE = "applicantBirthDate";
    public static final String GUARANTEE_APPLICANTGENDER = "applicantGender";
    public static final String GUARANTEE_APPLICANTAGE = "applicantAge";

    public static final String GUARANTEE_APPLICANT_NATIONAL_ID = "applicantNatID";
    public static final String GUARANTEE_APPLICANT_RESIDENTBOOK_ID = "applicantResidentBookID";
    public static final String GUARANTEE_APPLICANT_CERTIFICATE = "applicantCertificate";
    public static final String GUARANTEE_APPLICANT_FAMILYBOOK = "applicantFamilyBook";
    public static final String GUARANTEE_APPLICANT_BIRTHCERTIFICATE= "applicantBirthCertificate";

    public static final String IS_APPLICANT_NATIONAL_ID = "IsApplicantNatID";
    public static final String IS_APPLICANT_RESIDENTBOOK_ID = "IsApplicantResidentBookID";
    public static final String IS_APPLICANT_CERTIFICATE = "IsApplicantCertificate";
    public static final String IS_APPLICANT_FAMILYBOOK = "IsApplicantFamilyBook";
    public static final String IS_APPLICANT_BIRTHCERTIFICATE = "IsApplicantBirthCertificate";

    public static final String GUARANTEE_HOUSENUM = "guaranteeHouseNumber";
    public static final String GUARANTEE_STREETNUM = "guaranteeStreetNumber";
    public static final String GUARANTEE_VILLAGE = "guaranteeVillage";
    public static final String GUARANTEE_COMMUNE = "guaranteeCommune";
    public static final String GUARANTEE_DISTRICT = "guaranteeDistrict";
    public static final String GUARANTEE_PROVINCE = "guaranteeProvince";
    public static final String GUARANTEE_HOUSEPHONE = "guaranteeHousePhone";
    public static final String GUARANTEE_AVALABLEDATETIME = "guaranteeAvalableDateTime";

    public static final String GUARANTEE_IS_OWNHOUSE = "isOwnHouse";
    public static final String GUARANTEE_IS_PARENTHOUSE = "isParentHouse";
    public static final String GUARANTEE_IS_RELATIVEHOUSE = "isRelativeHouse";
    public static final String GUARANTEE_IS_RENTINGHOUSE = "isRentingHouse";
    public static final String GUARANTEE_IS_OTHER = "isOther";

    public static final String GUARANTEE_PROFESSIONAL = "guaranteeProfessional";
    public static final String GUARANTEE_COMPANYNAME = "guaranteeCompanyName";
    public static final String GUARANTEE_COMPANYTYPEBUSINESS = "guaranteeCompanyTypeBusiness";
    public static final String GUARANTEE_COMPANYHOUSENUM = "guaranteeCompanyHouseNum";
    public static final String GUARANTEE_COMPANYSTREETNUM = "guaranteeCompanyStreetNum";
    public static final String GUARANTEE_COMPANYVILLAGE = "guaranteeCompanyVillage";
    public static final String GUARANTEE_COMPANYCOMMUNE = "guaranteeCompanyCommune";
    public static final String GUARANTEE_COMPANYDISTRICT = "guaranteeCompanyDistrict";
    public static final String GUARANTEE_COMPANYPROVINCE = "guaranteeCompanyProvince";
    public static final String GUARANTEE_COMPANYPHONENUM = "guaranteeCompanyPhoneNum";
    public static final String GUARANTEE_POSITION = "guaranteePosition";

    public static final String GUARANTEE_TIMEATADDRESSINYEAR = "guaranteeTimeAtAddressInYear";
    public static final String GUARANTEE_TIMEATADDRESSINMONTH = "guaranteeTimeAtAddressInMonth";
    public static final String GUARANTEE_AGE = "guaranteeAge";

    public static final String NATION_ID_DOC_CODE = "N";
    public final static String RES_IND_BOOK_DOC_CODE = "R";
    public final static String VILLAGE_CERTI_DOC_CODE = "G";
    public final static String FAMILY_BOOK_DOC_CODE = "F";
    public final static String BIRTH_CERTIFICATE_DOC_CODE = "B";

}
