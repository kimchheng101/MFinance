package com.soma.mfinance.core.contract.service.cashflow;

import org.seuksa.frmk.service.BaseEntityService;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/24/2017
 * Name  : 1:36 PM
 * Email : k.seang@gl-f.com
 */
public interface CashflowMfpService extends BaseEntityService {
}
