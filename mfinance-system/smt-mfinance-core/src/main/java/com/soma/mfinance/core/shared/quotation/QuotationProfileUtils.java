package com.soma.mfinance.core.shared.quotation;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.financialproduct.FinancialProductEntityField;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;

/**
 * @author kimsuor.seang
 */
public final class QuotationProfileUtils {

    /**
     * @param quotation
     * @return
     */
    public static boolean isSaveUpdateAvailable(Quotation quotation) {
        boolean isAvailable = true;
        Contract contract = quotation.getContract();
        if (!ProfileUtil.isAdmin()) {
            if ((quotation != null && contract != null)
                    && contract.getWkfStatus().equals(QuotationWkfStatus.ACT)
                    || quotation.getWkfStatus().equals(QuotationWkfStatus.ACT)) {
                isAvailable = false;
            }
        }


        return isAvailable;
    }

    /**
     * @param quotation
     * @return
     */
    public static boolean isDeclineAvailable(Quotation quotation) {
        if (quotation.getWkfStatus().equals(QuotationWkfStatus.WCA) && ProfileUtil.isPOS() || ProfileUtil.isAdmin()) {
            return false;
        }
        return quotation.getId() != null && quotation.getFirstSubmissionDate() != null
                && ProfileUtil.isPOS()
                && !quotation.getWkfStatus().equals(QuotationWkfStatus.ACT)
                && !quotation.getWkfStatus().equals(QuotationWkfStatus.RVG)
                && !quotation.getWkfStatus().equals(QuotationWkfStatus.REJ)
                && !quotation.getWkfStatus().equals(QuotationWkfStatus.REU)
                && !quotation.getWkfStatus().equals(QuotationWkfStatus.DEC)
                && !quotation.getWkfStatus().equals(QuotationWkfStatus.LCG)
                && !quotation.getWkfStatus().equals(QuotationWkfStatus.ACG)
                && !quotation.getWkfStatus().equals(QuotationWkfStatus.APS)
                && !quotation.getWkfStatus().equals(QuotationWkfStatus.AUP)
                && !quotation.getWkfStatus().equals(QuotationWkfStatus.APP)
                && quotation.isDecline()
                || quotation.getWkfStatus().equals(QuotationWkfStatus.APV)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.AWT);
    }

    /**
     * @param quotation
     * @return
     */
    public static boolean isCancelAvailable(Quotation quotation) {
        return quotation.getId() != null &&
                (quotation.getWkfStatus().equals(QuotationWkfStatus.QUO) ||
                        quotation.getWkfStatus().equals(QuotationWkfStatus.AUP)) &&
                quotation.getFirstSubmissionDate() == null &&
                ProfileUtil.isPOS();
    }

    /**
     * @param quotation
     * @return
     */
    public static boolean isRequestFieldCheckAvailable(Quotation quotation) {
        return quotation.getId() != null
                && quotation.getWkfStatus().equals(QuotationWkfStatus.PRA)
                && !ProfileUtil.isAdmin();
    }

    /**
     * @param quotation
     * @return
     */
    public static boolean isChangeAssetAvailable(Quotation quotation) {
        return quotation.getId() != null && ProfileUtil.isPOS() && !quotation.isIssueDownPayment()
                && (quotation.getWkfStatus().equals(QuotationWkfStatus.APV))
                && quotation.getDocumentController() == null;
    }

    /**
     * @param quotation
     * @return
     */
    public static boolean isRequestChangeGuarantorAvailable(Quotation quotation) {
        return quotation.getId() != null
                && ProfileUtil.isPOS()
                && (quotation.getWkfStatus().equals(QuotationWkfStatus.ACT))
                && (quotation.getQuotationApplicant(EApplicantType.G) != null);
    }

    /**
     * @param quotation
     * @return
     */
    public static boolean isChangeGuarantorAvailable(Quotation quotation) {
        return quotation.getId() != null
                && ProfileUtil.isPOS()
                && (quotation.getWkfStatus().equals(QuotationWkfStatus.LCG))
                && (quotation.getQuotationApplicant(EApplicantType.G) != null);
    }

    /**
     * @param quotation
     * @return
     */
    public static boolean isRejectAvailable(Quotation quotation) {
        return quotation.getId() != null
                && !ProfileUtil.isAdmin()
                && (quotation.getWkfStatus().equals(QuotationWkfStatus.PRA)
                || (quotation.getWkfStatus().equals(QuotationWkfStatus.REU) && ProfileUtil.isUnderwriterSupervisor())
                || (quotation.getWkfStatus().equals(QuotationWkfStatus.APV) && ProfileUtil.isUW()));
    }


    /**
     * @param quotation
     * @return
     */
    public static boolean isApproveAvailable(Quotation quotation) {
        return quotation.getId() != null
                && (quotation.getWkfStatus().equals(QuotationWkfStatus.PRA) || quotation.getWkfStatus().equals(QuotationWkfStatus.RVG))
                && !ProfileUtil.isAdmin();
    }

    /**
     * @return
     */
    public static boolean isAddQuotationAvailable() {
        return ProfileUtil.isPOS();
    }


    /**
     * @param quotation
     * @return
     */
    public static boolean isSubmitAvailable(Quotation quotation) {
        return quotation.getId() != null
                && (!quotation.getWkfStatus().equals(QuotationWkfStatus.WCA)) //Not Available when status is
                && (!quotation.getWkfStatus().equals(QuotationWkfStatus.ACT)) //Approve also show submit for POS to DC
                && (!quotation.getWkfStatus().equals(QuotationWkfStatus.REJ))
                && (!quotation.getWkfStatus().equals(QuotationWkfStatus.REU))
                && (!quotation.getWkfStatus().equals(QuotationWkfStatus.DEC))
                && (!quotation.getWkfStatus().equals(QuotationWkfStatus.AUP))
        ;
    }

    /**
     * @param quotation
     * @return
     */
    public static boolean isSaveDocumentAvailable(Quotation quotation) {
        try {
            return quotation.getId() != null
                    && (((ProfileUtil.isUnderwriter() || ProfileUtil.isUnderwriterSupervisor())
                    && (quotation.getWkfStatus().equals(QuotationWkfStatus.PRO)
                    || quotation.getWkfStatus().equals(QuotationWkfStatus.PRA)))
                    || (ProfileUtil.isPOS() && quotation.getWkfStatus().equals(QuotationWkfStatus.AWT))
                    || (ProfileUtil.isDocumentController() && (quotation.getWkfStatus().equals(QuotationWkfStatus.APV)
                    || quotation.getWkfStatus().equals(QuotationWkfStatus.ACT))));
        } catch (NullPointerException e) {
            return false;
        }
    }

    /**
     * @param quotation
     * @return
     */
    public static boolean isSaveAssetAvailable(Quotation quotation) {
        return quotation.getId() != null
                && (
                (ProfileUtil.isPOS() && (quotation.getWkfStatus().equals(QuotationWkfStatus.APV) || quotation.getWkfStatus().equals(QuotationWkfStatus.PPO))) ||
                        (ProfileUtil.isDocumentController() &&
                                (quotation.getWkfStatus().equals(QuotationWkfStatus.APV)
                                        || quotation.getWkfStatus().equals(QuotationWkfStatus.PPO)
                                        || quotation.getWkfStatus().equals(QuotationWkfStatus.ACT))
                        )
        );
    }

    /**
     * @param quotation
     * @return
     */
    public static boolean isActivateAvailable(Quotation quotation) {
        return quotation.getId() != null
                && (ProfileUtil.isPOS()
                && QuotationWkfStatus.WCA.equals(quotation.getWkfStatus()));
    }

    /**
     * @param quotation
     * @return
     */
    public static boolean isReportsAvailable(Quotation quotation) {
        if (ProfileUtil.isUnderwriterSupervisor2()) {
            return quotation.getId() != null
                    && (QuotationWkfStatus.PRO.equals(quotation.getWkfStatus())
                    || QuotationWkfStatus.DEC.equals(quotation.getWkfStatus())
                    || QuotationWkfStatus.APU.equals(quotation.getWkfStatus())
                    || QuotationWkfStatus.APS.equals(quotation.getWkfStatus())
                    || QuotationWkfStatus.PRA.equals(quotation.getWkfStatus())
                    || QuotationWkfStatus.AWU.equals(quotation.getWkfStatus())
                    || QuotationWkfStatus.AWS.equals(quotation.getWkfStatus())
                    || QuotationWkfStatus.AWT.equals(quotation.getWkfStatus())
                    || QuotationWkfStatus.APV.equals(quotation.getWkfStatus()))
                    || QuotationWkfStatus.ACT.equals(quotation.getWkfStatus())
                    || QuotationWkfStatus.REU.equals(quotation.getWkfStatus())
                    || QuotationWkfStatus.REJ.equals(quotation.getWkfStatus())
                    ;
        } else {
            return quotation.getId() != null
                    && (QuotationWkfStatus.ACT.equals(quotation.getWkfStatus())
                    || QuotationWkfStatus.PRO.equals(quotation.getWkfStatus())
                    || QuotationWkfStatus.APU.equals(quotation.getWkfStatus())
                    || QuotationWkfStatus.AWU.equals(quotation.getWkfStatus())
                    || QuotationWkfStatus.REU.equals(quotation.getWkfStatus())
                    || QuotationWkfStatus.APV.equals(quotation.getWkfStatus()))
                    ;
        }
    }

    /**
     * @param quotation
     * @return
     */
    public static boolean isFieldCheckTabAvailable(Quotation quotation) {
        EWkfStatus quotationStatus = quotation.getWkfStatus();
        return (ProfileUtil.isPOS() || ProfileUtil.isAdmin())
                && (quotationStatus.equals(QuotationWkfStatus.RFC) || quotation.isFieldCheckPerformed());
    }

    /**
     * @param quotation
     * @return
     */
    public static boolean isUnderwriterTabAvailable(Quotation quotation) {
        return (ProfileUtil.isUW() || ProfileUtil.isManager());
    }

    /**
     * @param quotation
     * @return
     */
    public static boolean isManagementTabAvailable(Quotation quotation) {
        EWkfStatus quotationStatus = quotation.getWkfStatus();
        return ProfileUtil.isManager() && (quotationStatus.equals(QuotationWkfStatus.APS)
                || quotationStatus.equals(QuotationWkfStatus.AWS)
                || quotationStatus.equals(QuotationWkfStatus.AWT)
                || quotationStatus.equals(QuotationWkfStatus.APV)
                || quotationStatus.equals(QuotationWkfStatus.ACT)
                || quotationStatus.equals(QuotationWkfStatus.AWT)
                || quotationStatus.equals(QuotationWkfStatus.RCG));
    }

    /**
     * @param quotation
     * @return
     */
    public static boolean isHistoryStatusTabAvailable(Quotation quotation) {
        return quotation.getId() != null && (ProfileUtil.isPOS() || ProfileUtil.isUW() || ProfileUtil.isManager());
    }

    /**
     * @param quotation
     * @return
     */
    public static boolean isNavigationUnderwriterAvailable(Quotation quotation) {
        EWkfStatus quotationStatus = quotation.getWkfStatus();
        return ProfileUtil.isUW()
                && (
                quotationStatus.equals(QuotationWkfStatus.PRO) || quotationStatus.equals(QuotationWkfStatus.APU)
                        || quotationStatus.equals(QuotationWkfStatus.AWU) || quotationStatus.equals(QuotationWkfStatus.APS)
                        || quotationStatus.equals(QuotationWkfStatus.AWS)
        );
    }

    /**
     * @param quotation
     * @return
     */
    public static boolean isNavigationManagerAvailable(Quotation quotation) {
        EWkfStatus quotationStatus = quotation.getWkfStatus();
        return ProfileUtil.isManager() || ProfileUtil.isUnderwriterSupervisor2()
                && (
                quotationStatus.equals(QuotationWkfStatus.APS)
                        || quotationStatus.equals(QuotationWkfStatus.AWS)
                        || quotationStatus.equals(QuotationWkfStatus.RCG)
        );
    }

    /**
     *
     * */
    public static boolean isDocumentControllerAvailable(Quotation quotation) {
        if (quotation == null) {
            quotation = new Quotation();
        }
        EWkfStatus quotationStatus = quotation.getWkfStatus();
        return ProfileUtil.isDocumentController() && (quotationStatus.equals(QuotationWkfStatus.WIV));
    }


    /**
     * Edit And Refresh
     */
    public static boolean isNavigationType1Available() {
        return ProfileUtil.isPOS();
    }

    /**
     * Edit And Refresh
     */
    public static boolean isNavigationType2Available() {
        return ProfileUtil.isAuctionController()
                || ProfileUtil.isManager()
                || ProfileUtil.isUnderwriter()
                || ProfileUtil.isUnderwriterSupervisor()
                || ProfileUtil.isDocumentController();
    }


    /**
     * Show Mobile Payment Account Tab
     */
    public static boolean isLoanTabAvailable(Quotation quotation) {
        EWkfStatus quotationStatus = quotation.getWkfStatus();
        return (ProfileUtil.isPOS()
                || ProfileUtil.isUnderwriter()
                || ProfileUtil.isUnderwriterSupervisor()
                || ProfileUtil.isAccounting()
                || ProfileUtil.isAccountingSupervisor()
                || ProfileUtil.isDocumentController()
                || ProfileUtil.isManager()
                || ProfileUtil.isAdmin()
                || ProfileUtil.isAccounting())
                && (quotationStatus.equals(QuotationWkfStatus.APV)
                || quotationStatus.equals(QuotationWkfStatus.AWT)
                || quotationStatus.equals(QuotationWkfStatus.WIV)
                || quotationStatus.equals(QuotationWkfStatus.WCA)
                || quotationStatus.equals(QuotationWkfStatus.ACT)); //Add More
    }


    /**
     * All Status Before Approval
     */
    public static boolean isQuotationStatusBeforeApproval(Quotation quotation) {
        return quotation.getWkfStatus().equals(QuotationWkfStatus.QUO)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.PRO)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.DEC)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.PRA)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.SUB)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.RFC)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.REJ)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.REU)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.APU)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.APS)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.AWS)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.AWT)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.AWU);
    }

    /**
     * All Status After Auction Approve
     */
    public static boolean isQuotationStatusAfterAuctionApproval(Quotation quotation) {
        return quotation.getWkfStatus().equals(QuotationWkfStatus.PRO)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.AUP)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.APU)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.APS)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.APV)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.WIV)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.WCA);
    }

    /**
     * All Status After Auction Approve
     */
    public static boolean isBackPOSAvailable(Quotation quotation) {

        if (quotation.getWkfStatus().equals(QuotationWkfStatus.WCA) && ProfileUtil.isPOS()) {
            return false;
        }
        return (!quotation.getWkfStatus().equals(QuotationWkfStatus.QUO)
                && !quotation.getWkfStatus().equals(QuotationWkfStatus.ACT))
                && !ProfileUtil.isAdmin()
                && !QuotationWkfStatus.AUP.equals(quotation.getWkfStatus())
                && !QuotationWkfStatus.WIV.equals(quotation.getWkfStatus())
                ;
    }

    /**
     * All Status After Auction Approve
     */
    public static boolean isTransactionSettlementAvailable(Quotation quotation) {
        Contract contract = quotation.getContract();
        if (contract != null) {
            return (quotation.getWkfStatus().equals(QuotationWkfStatus.ACT)
                    && contract.getWkfStatus().equals(ContractWkfStatus.WTD)
                    && ProfileUtil.isAccounting())
                    && !ProfileUtil.isAdmin()
                    ;
        } else {
            return false;
        }
    }

    /**
     * @param quotation
     * @return
     */
    public static boolean isFinancialPrdTabAvailable(Quotation quotation) {
        return (quotation.getId() != null &&
                (!QuotationWkfStatus.QUO.equals(quotation.getWkfStatus()) &&
                        !QuotationWkfStatus.PPO.equals(quotation.getWkfStatus()) &&
                        !QuotationWkfStatus.AUP.equals(quotation.getWkfStatus()) &&
                        !QuotationWkfStatus.APP.equals(quotation.getWkfStatus())));
    }

    public static boolean isEnabledAppraisalTab(Quotation quotation) {
        return (ProfileUtil.isAuctionController()
                || ProfileUtil.isCreditOfficer()
                || ProfileUtil.isProductionOfficer())
                && (QuotationWkfStatus.QUO.equals(quotation.getWkfStatus())
                || QuotationWkfStatus.APP.equals(quotation.getWkfStatus()));
    }

	public static boolean isNavigationUnderWriterSupervisorAvailable(Quotation quotation) {
		EWkfStatus quotationStatus = quotation.getWkfStatus();
		return ProfileUtil.isUnderwriterSupervisor(ProfileUtil.getCurrentUser()) || ProfileUtil.isUnderwriterSupervisor2()
				&& (
						quotationStatus.equals(QuotationWkfStatus.PRO)
						|| quotationStatus.equals(QuotationWkfStatus.APU)
						|| quotationStatus.equals(QuotationWkfStatus.AWU)
						|| quotationStatus.equals(QuotationWkfStatus.AWS)
						|| quotationStatus.equals(QuotationWkfStatus.REU)
		);
	}

    /**
     * Apply condition on Dealer, Applicant and Gurantor Tab
     * @param quotation
     * @return
     */
	public static boolean isEnabled(Quotation quotation) {
        return  quotation.getWkfStatus().equals(QuotationWkfStatus.QUO)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.ACS)
                || ProfileUtil.isAdmin()
                || ProfileUtil.isUnderwriter()
                || ProfileUtil.isUnderwriterSupervisor()
                || ProfileUtil.isManager();
    }

    public static boolean isChangeOfficeAvailable(Quotation quotation) {
	    return ProfileUtil.isUnderwriterSupervisor2(ProfileUtil.getCurrentUser())
                && (quotation.getWkfStatus().equals(QuotationWkfStatus.PRO)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.QUO)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.APU)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.PRA)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.REJ)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.APV)
        );
    }
    /**
     * @param quotation
     * @return underwriter officer changeable
     */
    public static boolean isComboboxUnderwriterAvaialable(Quotation quotation) {
        return (quotation.getWkfStatus().equals(QuotationWkfStatus.QUO)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.PRO)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.PRA)
        );
    }
    /**
     * @param quotation
     * @return underwriter Supervisor officer changeable
     */
    public static boolean isComboboxUnderwriterSuperVisorAvaialable(Quotation quotation) {
        return (quotation.getWkfStatus().equals(QuotationWkfStatus.QUO)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.PRO)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.PRA)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.REU)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.APU)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.AWU)
        );
    }

    public static boolean isKubuta4Plus(Quotation quotation) {
        return(quotation != null && quotation.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_KUBUTA));
    }


}
