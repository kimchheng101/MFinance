package com.soma.mfinance.core.applicant.service;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Create by Rith RON.
 * User r.ron.
 * Date on 2017-07-07.
 * Time at 10:57 AM.
 * Email r.ron@gl-f.com.
 */
public class ValidateLetter {

    public static Boolean isKhmerLetter(String khWord) {
        if(!StringUtils.isEmpty(khWord)) {
            khWord = khWord.replaceAll("\\s+|\\u200B","");
            Pattern pattern = Pattern.compile("[\\p{InKhmer}]+[\\p{InKhmer_Symbols}]*",Pattern.UNICODE_CHARACTER_CLASS);
            Matcher matcher = pattern.matcher(khWord);
            return matcher.matches();
        }
        return true;
    }

    public static Boolean isEnglishLetter(String enWord) {
        if(!StringUtils.isEmpty(enWord)) {
            Pattern pattern = Pattern.compile("([A-Z][a-z]+)|^([A-Z]+\\s*[A-Z]+)$|^([a-z]+\\s*[a-z]+)$");
            Matcher matcher = pattern.matcher(enWord);
            if (matcher.matches())
                return true;
        }else if(StringUtils.isEmpty(enWord)){
            return true;
        }
        return false;
    }
}
