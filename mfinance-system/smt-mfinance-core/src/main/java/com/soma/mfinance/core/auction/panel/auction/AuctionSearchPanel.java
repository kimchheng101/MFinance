package com.soma.mfinance.core.auction.panel.auction;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.auction.model.EStorageLocation;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.financial.model.FinProduct;
import com.soma.mfinance.core.shared.contract.ContractEntityField;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.workflow.AuctionWkfStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.auction.model.EStorageLocation;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;

import java.util.List;

/**
 * Search panel for auction panel
 * @author kimsuor.seang
 * @modify kimsuor.seang (22/09/2017)
 */
public class AuctionSearchPanel extends AbstractSearchPanel<Contract> implements ContractEntityField {

	private static final long serialVersionUID = 8765174204972444569L;
	private TextField txtReference;
	private ERefDataComboBox<EWkfStatus> cbxAuctionStatus;
	private ERefDataComboBox<EStorageLocation> cbxEStorageLocation;
	private List<EWkfStatus> auctionStatus;
	private TextField txtEngineNumber;
	private TextField txtChassisNumber;
	private TextField txtPlateNumber;
	private TextField txtLastNameEn;
	private TextField txtFirstNameEn;
	private EntityRefComboBox<FinProduct> cbxFinancialProduct;
	private HorizontalLayout horizontalLayout;
	private FormLayout formLayout;
	private FormLayout formLayout1;
	/**
	 * @param tablePanel
	 */
	public AuctionSearchPanel(AbstractTablePanel<Contract> tablePanel) {
		super(I18N.message("search"), tablePanel);
	}

	/**
	 * @see AbstractSearchPanel#createForm()
	 */
	@Override
	protected Component createForm() {
		cbxFinancialProduct = new EntityRefComboBox<>(I18N.message("financial.product"));
		BaseRestrictions<FinProduct> restrictionsFinancial = new BaseRestrictions<>(FinProduct.class);
		restrictionsFinancial.getStatusRecordList().add(EStatusRecord.ACTIV);
		cbxFinancialProduct.setRestrictions(restrictionsFinancial);		
		cbxFinancialProduct.setImmediate(true);
		cbxFinancialProduct.renderer();
		cbxFinancialProduct.setSelectedEntity(null);
		cbxFinancialProduct.setWidth("200");

		txtReference = ComponentFactory.getTextField("reference", false, 50, 200);

		cbxEStorageLocation = new ERefDataComboBox<>(I18N.message("storage.location"), EStorageLocation.class);
		cbxEStorageLocation.setWidth("200");

		/*cbxAuctionStatus = new ERefDataComboBox<>(AuctionWkfStatus.values());
		cbxAuctionStatus.setCaption(I18N.message("auction.status"));
		cbxAuctionStatus.setWidth("200");*/

		txtEngineNumber = ComponentFactory.getTextField("engine.number", false, 50, 200);		
		txtChassisNumber = ComponentFactory.getTextField("chassis.number", false, 50, 200);
		txtPlateNumber = ComponentFactory.getTextField("plate.number", false, 50, 200);
		txtLastNameEn = ComponentFactory.getTextField("lastname.en", false, 50,200);
		txtFirstNameEn =  ComponentFactory.getTextField("firstname.en", false, 50, 200);


		horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSpacing(true);
		
		formLayout = new FormLayout();//1
			formLayout.addComponent(txtReference);
			formLayout.addComponent(txtEngineNumber);
			horizontalLayout.addComponent(formLayout);
		
		formLayout = new FormLayout();//2
			formLayout.addComponent(cbxEStorageLocation);
			formLayout.addComponent(txtChassisNumber);
			horizontalLayout.addComponent(formLayout);
		
		formLayout1 = new FormLayout(); //3
			formLayout1.addComponent(txtPlateNumber);
			formLayout1.addComponent(cbxFinancialProduct);
			horizontalLayout.addComponent(formLayout1);
		if(ProfileUtil.isAuctionController() || ProfileUtil.isCollectionSupervisor()){
		formLayout = new FormLayout(); //4
			formLayout.addComponent(txtLastNameEn);
	        formLayout.addComponent(txtFirstNameEn);
	        horizontalLayout.addComponent(formLayout);
		}
		return horizontalLayout;

	}
  	
/**
	 * @see AbstractSearchPanel#getRestrictions()
	 */
	@Override
	public BaseRestrictions<Contract> getRestrictions() {
		BaseRestrictions<Contract> restrictions = new BaseRestrictions<>(Contract.class);
		if(ProfileUtil.isManager()){
			restrictions.addCriterion(Restrictions.eq("auctionStatus", AuctionWkfStatus.VAL));
		}else if(ProfileUtil.isAuctionController()){
			restrictions.addCriterion(Restrictions.in("auctionStatus", AuctionWkfStatus.auctionStatus()));
		}
		restrictions.addAssociation("quotations", "quoa", JoinType.INNER_JOIN);
		restrictions.addAssociation("quoa.asset", "qouasset", JoinType.INNER_JOIN);
		
		restrictions.addAssociation("quoa.quotationApplicants","quoapp", JoinType.INNER_JOIN);
		restrictions.addAssociation("quoapp.applicant","app", JoinType.INNER_JOIN);
		restrictions.addAssociation("app." + INDIVIDUAL, "ind", JoinType.INNER_JOIN);
		restrictions.addCriterion("quoapp.applicantType", EApplicantType.C);

		if (cbxFinancialProduct.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("financialProduct", cbxFinancialProduct.getSelectedEntity()));
		}
		if (cbxAuctionStatus.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("auctionStatus", cbxAuctionStatus.getSelectedEntity()));
		}
		if (cbxEStorageLocation.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("eStorageLocation.id", cbxEStorageLocation.getSelectedEntity().getId()));
		}
		if (StringUtils.isNotEmpty(txtReference.getValue())) {
			restrictions.addCriterion(Restrictions.ilike(REFERENCE, txtReference.getValue(), MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotEmpty(txtEngineNumber.getValue())) {
			restrictions.addCriterion(Restrictions.ilike("qouasset.engineNumber", txtEngineNumber.getValue(), MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotEmpty(txtChassisNumber.getValue())) {
			restrictions.addCriterion(Restrictions.ilike("qouasset.chassisNumber", txtChassisNumber.getValue(), MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotEmpty(txtPlateNumber.getValue())) {
			restrictions.addCriterion(Restrictions.ilike("qouasset.plateNumber", txtPlateNumber.getValue(), MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotEmpty(txtLastNameEn.getValue())) {
			restrictions.addCriterion(Restrictions.ilike("ind." + LAST_NAME_EN, txtLastNameEn.getValue(), MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
			restrictions.addCriterion(Restrictions.ilike("ind." + FIRST_NAME_EN, txtFirstNameEn.getValue(), MatchMode.ANYWHERE));
		}
		return restrictions;
	}

	/**
	 * @see AbstractSearchPanel#reset()
	 */
	@Override
	protected void reset() {
		txtReference.setValue("");
		cbxAuctionStatus.setSelectedEntity(null);
		cbxEStorageLocation.setSelectedEntity(null);
		txtEngineNumber.setValue("");
		txtChassisNumber.setValue("");
		txtPlateNumber.setValue("");
		txtFirstNameEn.setValue("");
		txtLastNameEn.setValue("");
		cbxFinancialProduct.setSelectedEntity(null);
	}
	
	/**
	 * @param auctionStatus
	 */
	public void assignValues (List<EWkfStatus> auctionStatus) {
		//this.auctionStatus = AuctionWkfStatus.convertAuctionStatus(auctionStatus);
		if (auctionStatus.size() > 1) {
			cbxAuctionStatus = new  ERefDataComboBox<>(AuctionWkfStatus.convertAuctionStatus(auctionStatus));
			cbxAuctionStatus.setCaption(I18N.message("auction.status"));
			cbxAuctionStatus.setWidth("200");
			formLayout1.addComponent(cbxAuctionStatus);
		
		}
	}

}
