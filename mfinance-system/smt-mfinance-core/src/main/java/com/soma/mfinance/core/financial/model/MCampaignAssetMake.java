package com.soma.mfinance.core.financial.model;

import org.seuksa.frmk.model.entity.MEntityRefA;

/**
 * Meta data of com.soma.mfinance.core.financial.model.CampaignAssetMake
 * @author kimsuor.seang
 */
public interface MCampaignAssetMake extends MEntityRefA {

	public final static String ASSETMAKE = "assetMake";
	public final static String CAMPAIGN = "campaign";
	
}
