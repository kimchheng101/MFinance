package com.soma.mfinance.third.einsurance.service.impl;

import com.google.gson.Gson;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.mfinance.third.einsurance.EInsuranceContractSrvRsc;
import com.soma.mfinance.third.einsurance.service.ContractMigrationService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Kimsuor SEANG
 * Date  : 10/19/2017
 * Name  : 9:04 AM
 * Email : k.seang@gl-f.com
 */
@Service("contractMigrationServiceImpl")
public class ContractMigrationServiceImpl extends BaseEntityServiceImpl implements ContractMigrationService, FMEntityField {

    @Value("${ws.einsurance.url}")
    private String wsEinsuranceUrl;

    @Value("${ws.einsurance.migration.contract.endpoint}")
    private String wsMigrationContractEndPoint;

    @Autowired
    private EntityDao dao;

    private JavaMailSender javaMailSender;

    @Override
    public String migrationToEinsurance(Long rquotationId) {
        Quotation quotation = getById(Quotation.class, rquotationId);
        Contract contract = getByField(Contract.class, "reference", quotation.getReference());
        EInsuranceContractSrvRsc eInsuranceContractSrvRsc = new EInsuranceContractSrvRsc();
        /*QuotationVO quotationVO = eInsuranceContractSrvRsc.toEInsuranceDTO(contract, quotation);
        Response result = submitToEInsurance(quotationVO);*/

        //String message = resultMigrate(quotation, result);
        /*if(message.contains("Fail")){
            sendMail(message);
        }*/
        return "";
    }

    @Override
    public Set<String> migrationToEinsurance(Set<Long> selectId) {
        Set<String> messages = new HashSet<>();
        selectId.forEach(id -> {
            String message;
            Quotation quotation = getById(Quotation.class, id);
            Contract contract = getById(Contract.class, quotation.getContract().getId());
            EInsuranceContractSrvRsc eInsuranceContractSrvRsc = new EInsuranceContractSrvRsc();
            /*QuotationVO quotationVO = eInsuranceContractSrvRsc.toEInsuranceDTO(contract, quotation);
            Response result = submitToEInsurance(quotationVO);
            message = resultMigrate(quotation, result);
            messages.add(message);*/

        });
        return messages;
    }

    @Override
    public BaseEntityDao getDao() {
        return dao;
    }

    private String resultMigrate(Quotation quotation, Response response) {
        String message = "";
        if (Response.Status.OK.getStatusCode() == response.getStatus()) {
            HashMap<String, String> output = response.readEntity(HashMap.class);
            Quotation quotationToUpdate = getByField(Quotation.class, "reference", output.get("externalLid"));
            quotationToUpdate.setMigrationDate(DateUtils.todayDate());
            quotationToUpdate.setMigrationID(output.get("externalReference"));
            quotationToUpdate.setCheckerID("1");
            saveOrUpdate(quotationToUpdate);
            logger.debug("Contract - Reference. [" + quotation.getReference() + "] Start Migrate successfully" + output);
            message += "Successfully Migrate | Quotation ID : " + quotation.getId().toString();
        } else {
            Object obj = response.readEntity(Object.class);
            Gson gson = new Gson();
            String jsonInString = gson.toJson(obj);
            JSONObject jsonObject = new JSONObject(jsonInString);
            JSONArray jsonArray = jsonObject.getJSONArray("errors");
            message += "Fail to Migrate | Quotation ID : " + quotation.getId().toString();
            message += " | " + jsonArray.getJSONObject(0).getString("field") + " : " + jsonArray.getJSONObject(0).getString("message");
        }
        return message;
    }

    private void sendMail(String text){
        javaMailSender = SpringUtils.getBean("mailSender");
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo("ph.men@gl-f.com");
        simpleMailMessage.setFrom("v.sai@gl-f.com");
        simpleMailMessage.setSubject("Migrate Data");
        simpleMailMessage.setText(text);

        try {
            javaMailSender.send(simpleMailMessage);
        }catch (MailException m){
            System.out.println("Mail Error" + m.getMessage());
        }
    }

    /*private Response submitToEInsurance(QuotationVO quotationVO){
        Entity<QuotationVO> entity = Entity.json(quotationVO);

        ClientConfig config = new ClientConfig();
        final Client client = ClientBuilder.newClient(config).register(JacksonFeature.class);
        String migrationToEinsuranceUrl = wsEinsuranceUrl + wsMigrationContractEndPoint;
        Response result = client.target(migrationToEinsuranceUrl)
                .request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .post(entity, Response.class);

        return result;
    }*/
}
