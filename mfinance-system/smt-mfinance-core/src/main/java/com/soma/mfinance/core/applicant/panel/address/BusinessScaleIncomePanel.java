package com.soma.mfinance.core.applicant.panel.address;

import com.soma.mfinance.core.address.model.ScaleOfBusiness;
import com.soma.mfinance.core.applicant.model.Employment;
import com.soma.mfinance.core.shared.system.AmountBigUtils;
import com.soma.mfinance.core.shared.system.AutoTextField;
import com.soma.frmk.vaadin.ui.panel.AbstractControlPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author by kimsuor.seang  on 10/13/2017.
 */
public class BusinessScaleIncomePanel extends AbstractControlPanel {

    private static final long serialVersionUID = -776274015214802081L;

    private EntityRefComboBox<ScaleOfBusiness> cbxScaleOfBusiness;
    private AutoTextField txtNetIncome;
    private Label lblTimingOfIncome;

    private Label lblJan;
    private Label lblFeb;
    private Label lblMar;
    private Label lblApr;
    private Label lblMay;
    private Label lblJun;
    private Label lblJul;
    private Label lblAug;
    private Label lblSep;
    private Label lblOct;
    private Label lblNov;
    private Label lblDec;

    private CheckBox cbJan;
    private CheckBox cbFeb;
    private CheckBox cbMar;
    private CheckBox cbApr;
    private CheckBox cbMay;
    private CheckBox cbJun;
    private CheckBox cbJul;
    private CheckBox cbAug;
    private CheckBox cbSep;
    private CheckBox cbOct;
    private CheckBox cbNov;
    private CheckBox cbDec;
    private boolean required;

    public BusinessScaleIncomePanel(boolean required, String template) {
        this.required = required;
        setSizeFull();

        cbxScaleOfBusiness = new EntityRefComboBox<>("");
        cbxScaleOfBusiness.setRestrictions(new BaseRestrictions<>(ScaleOfBusiness.class));
        cbxScaleOfBusiness.renderer();

        txtNetIncome = new AutoTextField();
        txtNetIncome.setWidth("202");

        lblTimingOfIncome = new Label(I18N.message("timing.income"));
        lblJan = new Label(I18N.message("jan"));
        lblFeb = new Label(I18N.message("feb"));
        lblMar = new Label(I18N.message("mar"));
        lblApr = new Label(I18N.message("apr"));
        lblMay = new Label(I18N.message("may"));
        lblJun = new Label(I18N.message("jun"));
        lblJul = new Label(I18N.message("jul"));
        lblAug = new Label(I18N.message("aug"));
        lblSep = new Label(I18N.message("sep"));
        lblOct = new Label(I18N.message("oct"));
        lblNov = new Label(I18N.message("nov"));
        lblDec = new Label(I18N.message("dec"));

        cbJan = new CheckBox();
        cbJan.setValue(false);

        cbFeb = new CheckBox();
        cbFeb.setValue(false);

        cbMar = new CheckBox();
        cbMar.setValue(false);

        cbApr = new CheckBox();
        cbApr.setValue(false);

        cbMay = new CheckBox();
        cbMay.setValue(false);

        cbJun = new CheckBox();
        cbJun.setValue(false);

        cbJul = new CheckBox();
        cbJul.setValue(false);

        cbAug = new CheckBox();
        cbAug.setValue(false);

        cbSep = new CheckBox();
        cbSep.setValue(false);

        cbOct = new CheckBox();
        cbOct.setValue(false);

        cbNov = new CheckBox();
        cbNov.setValue(false);

        cbDec = new CheckBox();
        cbNov.setValue(false);

        InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
        CustomLayout customLayout = null;
        try {
            customLayout = new CustomLayout(layoutFile);
            customLayout.setSizeFull();
        } catch (IOException e) {
            Notification.show("Could not locate template " + template, e.getMessage(), Type.ERROR_MESSAGE);
        }
        customLayout.addComponent(new Label(I18N.message("scale.business")), "lblScaleOfBusiness");
        customLayout.addComponent(cbxScaleOfBusiness, "cbxScaleOfBusiness");
        customLayout.addComponent(new Label(I18N.message("net.income")), "lblNetIncome");
        customLayout.addComponent(new Label(I18N.message("per.year")), "lblPerYear");
        customLayout.addComponent(txtNetIncome, "txtNetIncome");
        customLayout.addComponent(lblTimingOfIncome, "lblTimingOfIncome");
        customLayout.addComponent(lblJan, "lblJan");
        customLayout.addComponent(lblFeb, "lblFeb");
        customLayout.addComponent(lblMar, "lblMar");
        customLayout.addComponent(lblApr, "lblApr");
        customLayout.addComponent(lblMay, "lblMay");
        customLayout.addComponent(lblJun, "lblJun");
        customLayout.addComponent(lblJul, "lblJul");
        customLayout.addComponent(lblAug, "lblAug");
        customLayout.addComponent(lblSep, "lblSep");
        customLayout.addComponent(lblOct, "lblOct");
        customLayout.addComponent(lblNov, "lblNov");
        customLayout.addComponent(lblDec, "lblDec");

        customLayout.addComponent(cbJan, "cbJan");
        customLayout.addComponent(cbFeb, "cbFeb");
        customLayout.addComponent(cbMar, "cbMar");
        customLayout.addComponent(cbApr, "cbApr");
        customLayout.addComponent(cbMay, "cbMay");
        customLayout.addComponent(cbJun, "cbJun");
        customLayout.addComponent(cbJul, "cbJul");
        customLayout.addComponent(cbAug, "cbAug");
        customLayout.addComponent(cbSep, "cbSep");
        customLayout.addComponent(cbOct, "cbOct");
        customLayout.addComponent(cbNov, "cbNov");
        customLayout.addComponent(cbDec, "cbDec");

        addComponent(customLayout);
    }

    public void setAddressEnabled(boolean enabled) {
        cbxScaleOfBusiness.setEnabled(enabled);
        txtNetIncome.setEnabled(enabled);
        cbJan.setEnabled(enabled);
        cbFeb.setEnabled(enabled);
        cbMar.setEnabled(enabled);
        cbApr.setEnabled(enabled);
        cbMay.setEnabled(enabled);
        cbJun.setEnabled(enabled);
        cbJul.setEnabled(enabled);
        cbAug.setEnabled(enabled);
        cbSep.setEnabled(enabled);
        cbOct.setEnabled(enabled);
        cbNov.setEnabled(enabled);
        cbDec.setEnabled(enabled);
    }

    public Employment getBusinessScaleIncome(Employment employment) {

        employment.setScaleOfBusiness(cbxScaleOfBusiness.getSelectedEntity());
        employment.setNetIncome(AmountBigUtils.getValueDouble(txtNetIncome));
        employment.setJan(cbJan.getValue());
        employment.setFeb(cbFeb.getValue());
        employment.setMar(cbMar.getValue());
        employment.setApr(cbApr.getValue());
        employment.setMay(cbMay.getValue());
        employment.setJun(cbJun.getValue());
        employment.setJul(cbJul.getValue());
        employment.setAug(cbAug.getValue());
        employment.setSep(cbSep.getValue());
        employment.setOct(cbOct.getValue());
        employment.setNov(cbNov.getValue());
        employment.setDec(cbDec.getValue());

        return employment;
    }

    public void assignValues(Employment employment) {
        reset();
        cbxScaleOfBusiness.setSelectedEntity(employment.getScaleOfBusiness());
        txtNetIncome.setValue(AmountBigUtils.format(employment.getNetIncome()));
        cbJan.setValue(getIsCheck(employment.isJan()));
        cbFeb.setValue(getIsCheck(employment.isFeb()));
        cbMar.setValue(getIsCheck(employment.isMar()));
        cbApr.setValue(getIsCheck(employment.isApr()));
        cbMay.setValue(getIsCheck(employment.isMay()));
        cbJun.setValue(getIsCheck(employment.isJun()));
        cbJul.setValue(getIsCheck(employment.isJul()));
        cbAug.setValue(getIsCheck(employment.isAug()));
        cbSep.setValue(getIsCheck(employment.isSep()));
        cbOct.setValue(getIsCheck(employment.isOct()));
        cbNov.setValue(getIsCheck(employment.isNov()));
        cbDec.setValue(getIsCheck(employment.isDec()));
    }

    private Boolean getIsCheck(Boolean isTrue) {
        Boolean value = isTrue;
        if (isTrue == null) {
            value = false;
        }
        return value;
    }

    public void reset() {
        cbxScaleOfBusiness.setSelectedEntity(null);
        txtNetIncome.setValue("");
        cbJan.setValue(false);
        cbFeb.setValue(false);
        cbMar.setValue(false);
        cbApr.setValue(false);
        cbMay.setValue(false);
        cbJun.setValue(false);
        cbJul.setValue(false);
        cbAug.setValue(false);
        cbSep.setValue(false);
        cbOct.setValue(false);
        cbNov.setValue(false);
        cbDec.setValue(false);
    }

    public List<String> fullValidate() {
        super.reset();
        if (required) {
            checkMandatorySelectField(cbxScaleOfBusiness, "scale.business");
            checkMandatoryField(txtNetIncome, "net.income");
            if (!isValidateCheckBoxMonth()) {
                errors.add(I18N.message("timing.income"));
            }
        }
        return errors;
    }

    public List<String> partialValidate() {
        super.reset();
        if (required) {
            checkMandatorySelectField(cbxScaleOfBusiness, "scale.business");
            checkMandatoryField(txtNetIncome, "net.income");
        }
        return errors;
    }

    private Boolean isValidateCheckBoxMonth() {
        return (cbJan.getValue() || cbFeb.getValue() || cbMar.getValue()
                || cbApr.getValue() || cbMay.getValue() || cbJun.getValue()
                || cbJul.getValue() || cbAug.getValue() || cbSep.getValue()
                || cbOct.getValue() || cbNov.getValue() || cbDec.getValue());
    }

    public AutoTextField getTxtNetIncome() {
        return txtNetIncome;
    }

}
