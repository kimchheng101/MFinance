package com.soma.mfinance.core.collection.service.impl;

import com.soma.common.app.tools.UserSessionManager;
import com.soma.mfinance.core.address.model.Area;
import com.soma.mfinance.core.collection.model.*;
import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.collection.service.ColContractDetail;
import com.soma.mfinance.core.collection.service.CollectionActionRestriction;
import com.soma.mfinance.core.collection.service.CollectionService;
import com.soma.mfinance.core.collection.service.DebtLevelUtils;
import com.soma.mfinance.core.common.IProfileCode;
import com.soma.mfinance.core.common.security.model.SecUserDeptLevel;
import com.soma.mfinance.core.common.security.model.SecUserDetail;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.ContractUserInbox;
import com.soma.mfinance.core.contract.model.ContractUserSimulInbox;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.contract.service.ContractRestriction;
import com.soma.mfinance.core.contract.service.ContractUserSimulInboxRestriction;
import com.soma.mfinance.core.contract.service.UserInboxService;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.history.FinHistoryType;
import com.soma.mfinance.core.history.service.FinHistoryService;
import com.soma.mfinance.core.shared.collection.CollectionEntityField;
import com.soma.mfinance.core.shared.contract.ContractEntityField;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.frmk.config.model.SettingConfig;
import com.soma.frmk.security.model.SecUser;
import com.soma.mfinance.core.common.IProfileCode;
import com.soma.mfinance.core.common.security.model.SecUserDeptLevel;
import com.soma.mfinance.core.common.security.model.SecUserDetail;
import org.hibernate.Criteria;
import org.hibernate.criterion.*;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.model.meta.NativeColumn;
import org.seuksa.frmk.model.meta.NativeRow;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.exception.NativeQueryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;

import static org.hibernate.criterion.Restrictions.eq;
import static org.hibernate.criterion.Restrictions.in;


/**
 * Collection Service
 *
 * @author kimsuor.seang
 */
@Service("collectionService")
public class CollectionServiceImpl extends BaseEntityServiceImpl implements CollectionService, FinServicesHelper, ContractEntityField, CollectionEntityField {
    /** */
    private static final long serialVersionUID = -1261233583650839609L;

    private Logger LOG = LoggerFactory.getLogger(ContractOtherDataServiceImpl.class);

    private static final String DAY_END_PROCESS_PARAM = "day.end.process.param";


    @Autowired
    private EntityDao dao;

    @Autowired
    private UserInboxService userInboxService;

    @Autowired
    private FinHistoryService finHistoryService;

    /**
     * @see
     */
    @Override
    public EntityDao getDao() {
        return dao;
    }


    /**
     * @return
     */
    @Override
    public List<SecUser> getCollectionUsers(String[] proCode) {
        BaseRestrictions<SecUser> restrictions = new BaseRestrictions<>(SecUser.class);
        restrictions.addAssociation("defaultProfile", "pro", JoinType.INNER_JOIN);
        restrictions.addCriterion(Restrictions.in("pro.code", proCode));
        return list(restrictions);
    }

    /**
     * @return
     */
    private List<SecUser> getAssigneeCollectionUsers(String[] proCode, Integer[] debLevels) {
        BaseRestrictions<SecUser> restrictions = new BaseRestrictions<>(SecUser.class);
        restrictions.addAssociation("defaultProfile", "pro", JoinType.INNER_JOIN);
        restrictions.addCriterion(Restrictions.in("pro.code", proCode));

        DetachedCriteria userDetailCriteria = DetachedCriteria.forClass(SecUserDetail.class, "usrdet");
        userDetailCriteria.add(Restrictions.eq("usrdet.enableAssignContracts", true));
        userDetailCriteria.setProjection(Projections.projectionList().add(Projections.property("usrdet.secUser.id")));
        restrictions.addCriterion(Property.forName("id").in(userDetailCriteria));

        DetachedCriteria userDebtLevelCriteria = DetachedCriteria.forClass(SecUserDeptLevel.class, "usrdlvl");
        userDebtLevelCriteria.add(Restrictions.in("usrdlvl.debtLevel", debLevels));
        userDebtLevelCriteria.setProjection(Projections.projectionList().add(Projections.property("usrdlvl.secUser.id")));
        restrictions.addCriterion(Property.forName("id").in(userDebtLevelCriteria));

        return list(restrictions);
    }

    /**
     * Get Due Days setting
     * @return
     */
    private List<Integer> getDueDaysDate() {
        List<Integer> dueDayDates = new ArrayList<>();
        BaseRestrictions<SettingConfig> restrictions = new BaseRestrictions<>(SettingConfig.class);
        restrictions.addCriterion(Restrictions.like("code", DAY_END_PROCESS_PARAM, MatchMode.START));
        List<SettingConfig> settings = list(restrictions);
        if (settings != null) {
            for (SettingConfig setting : settings) {
                dueDayDates.add(Integer.parseInt(setting.getValue()));
            }
        }
        return dueDayDates;

    }

    /**
     * Assign Day end process
     *
     * @param processDate
     */
    public void assignDayEndContracts(Date processDate) {
        userInboxService.deleteContractSimulByDebtLevels(DebtLevelUtils.DAY_END);

        List<Integer> dueDayDates = getDueDaysDate();
        if (dueDayDates == null || !dueDayDates.contains(DateUtils.getDay(processDate))) {
            return;
        }

        List<SecUser> colStaffs = getAssigneeCollectionUsers(new String[]{IProfileCode.COL_PHO_STAFF}, new Integer[]{0});
        if (colStaffs == null || colStaffs.isEmpty()) {
            return;
        }

        List<ColContractDetail> contracts = getContractsByDebtLevels(DebtLevelUtils.DAY_END, new String[]{IProfileCode.COL_PHO_STAFF});

        Map<Long, UserContract> userContracts = new HashMap<>();

        int indexStaff = 0;
        for (ColContractDetail contract : contracts) {

            SecUser selectStaff = colStaffs.get(indexStaff);
            if (indexStaff == colStaffs.size() - 1) {
                indexStaff = 0;
            } else {
                indexStaff++;
            }
            if (!userContracts.containsKey(selectStaff.getId())) {
                UserContract userContract = new UserContract(selectStaff);
                userContract.addContract(contract);
                userContracts.put(selectStaff.getId(), userContract);
            } else {
                UserContract userContract = userContracts.get(selectStaff.getId());
                userContract.addContract(contract);
            }
        }

        for (Iterator<Long> iter = userContracts.keySet().iterator(); iter.hasNext(); ) {
            UserContract userContract = userContracts.get(iter.next());
            List<ContractUserSimulInbox> contractsUserSimulInbox = new ArrayList<>();
            for (ColContractDetail colContractDetail : userContract.getAssignContracts()) {
                ContractUserSimulInbox contractUserSimulInbox = ContractUserSimulInbox.createInstance();
                contractUserSimulInbox.setContract(Contract.createInstance(colContractDetail.getId()));
                contractUserSimulInbox.setSecUser(userContract.getUser());
                contractUserSimulInbox.setColType(EColType.PHONE);
                contractUserSimulInbox.setDebtLevel(colContractDetail.getDebtLevel());
                contractUserSimulInbox.setProfile(userContract.getUser().getDefaultProfile());
                contractsUserSimulInbox.add(contractUserSimulInbox);
            }
            if (!contractsUserSimulInbox.isEmpty()) {
                saveOrUpdateBulk(contractsUserSimulInbox);
            }
        }
    }

    /**
     * @param processDate
     */
    public void assignPhoneContracts(Date processDate) {
        userInboxService.deleteContractSimulByDebtLevels(DebtLevelUtils.PHONE);
        assignPhoneContracts(processDate, 1);
        assignPhoneContracts(processDate, 2);
        assignPhoneContracts(processDate, 3);
    }

    /**
     * @param processDate
     * @param debtLevel
     */
    private void assignPhoneContracts(Date processDate, int debtLevel) {
        List<SecUser> colStaffs = getAssigneeCollectionUsers(new String[]{IProfileCode.COL_PHO_STAFF}, new Integer[]{debtLevel});
        if (colStaffs == null || colStaffs.isEmpty()) {
            return;
        }

        List<ColContractDetail> contracts = getContractsByDebtLevels(new Integer[]{debtLevel}, new String[]{IProfileCode.COL_PHO_STAFF});

        Map<Long, UserContract> userContracts = new HashMap<>();

        int indexStaff = 0;
        for (ColContractDetail contract : contracts) {

            SecUser selectStaff = colStaffs.get(indexStaff); // getStaffToAssign(colStaffs, userContracts);
            if (indexStaff == colStaffs.size() - 1) {
                indexStaff = 0;
            } else {
                indexStaff++;
            }
            if (!userContracts.containsKey(selectStaff.getId())) {
                UserContract userContract = new UserContract(selectStaff);
                userContract.addContract(contract);
                userContracts.put(selectStaff.getId(), userContract);
            } else {
                UserContract userContract = userContracts.get(selectStaff.getId());
                userContract.addContract(contract);
            }
        }

        for (Iterator<Long> iter = userContracts.keySet().iterator(); iter.hasNext(); ) {
            UserContract userContract = userContracts.get(iter.next());
            List<ContractUserSimulInbox> contractsUserSimulInbox = new ArrayList<>();
            for (ColContractDetail colContractDetail : userContract.getAssignContracts()) {
                ContractUserSimulInbox contractUserSimulInbox = ContractUserSimulInbox.createInstance();
                contractUserSimulInbox.setContract(Contract.createInstance(colContractDetail.getId()));
                contractUserSimulInbox.setSecUser(userContract.getUser());
                contractUserSimulInbox.setColType(EColType.PHONE);
                contractUserSimulInbox.setDebtLevel(colContractDetail.getDebtLevel());
                contractUserSimulInbox.setProfile(userContract.getUser().getDefaultProfile());
                contractsUserSimulInbox.add(contractUserSimulInbox);
            }
            if (!contractsUserSimulInbox.isEmpty()) {
                saveOrUpdateBulk(contractsUserSimulInbox);
            }
            flush();
            clear();
        }
    }

    /**
     *
     */
    public void assignFieldContracts() {
        userInboxService.deleteContractSimulByType(EColType.FIELD);
        assignAreaContracts(DebtLevelUtils.DEBT_LEVEL_4, EColType.FIELD, new String[]{IProfileCode.COL_FIE_STAFF});
        assignAreaContracts(DebtLevelUtils.DEBT_LEVEL_5, EColType.FIELD, new String[]{IProfileCode.COL_FIE_STAFF});
    }

    /**
     * @see com.soma.mfinance.core.collection.service.CollectionService#validateAssistFlagContract(Contract)
     */
    @Override
    public void validateAssistFlagContract(Contract contra) {
        if (contra != null) {
            Collection collection = contra.getCollection();
            Integer debtLvl = null;

            if (collection == null) {
                return;
            }
            debtLvl = collection.getDebtLevel();

            List<SecUser> colStaffs = getAssigneeCollectionUsers(new String[]{IProfileCode.COL_FIE_STAFF}, new Integer[]{debtLvl});
            if (colStaffs == null || colStaffs.isEmpty()) {
                return;
            }
            List<ColContractDetail> contracts = getContractsByDebtLevels(new Integer[]{debtLvl}, new String[]{IProfileCode.COL_FIE_STAFF});

            Map<String, SecUser> staffAreas = getStaffAreas();

            Map<Long, UserContract> userContracts = new HashMap<>();

            int indexStaff = 0;
            for (ColContractDetail contract : contracts) {
                if (contra.getId().equals(contract.getId())) {
                    Area contractArea = getContractArea(contract, EColType.FIELD);
                    boolean assigned = false;
                    if (contractArea != null) {
                        SecUser selectStaff = colStaffs.get(indexStaff);
                        if (staffAreas.containsKey("A" + contractArea.getId() + "S" + selectStaff.getId())) {
                            if (indexStaff == colStaffs.size() - 1) {
                                indexStaff = 0;
                            } else {
                                indexStaff++;
                            }
                            contract.setArea(contractArea);
                            if (!userContracts.containsKey(selectStaff.getId())) {
                                UserContract userContract = new UserContract(selectStaff);
                                userContract.addContract(contract);
                                userContracts.put(selectStaff.getId(), userContract);
                            } else {
                                UserContract userContract = userContracts.get(selectStaff.getId());
                                userContract.addContract(contract);
                            }
                            assigned = true;
                        }
                    }

                    if (!assigned) {
                        ContractUserSimulInboxRestriction restrictions = new ContractUserSimulInboxRestriction();
                        restrictions.setConId(contract.getId());
                        List<ContractUserSimulInbox> inboxs = list(restrictions);
                        if (inboxs != null && !inboxs.isEmpty()) {
                            for (ContractUserSimulInbox inbox : inboxs) {
                                delete(inbox);
                            }
                        }
                        ContractUserSimulInbox contractUserSimulInbox = ContractUserSimulInbox.createInstance();
                        contractUserSimulInbox.setContract(Contract.createInstance(contract.getId()));
                        contractUserSimulInbox.setColType(EColType.FIELD);
                        contractUserSimulInbox.setDebtLevel(debtLvl);
                        contractUserSimulInbox.setArea(contractArea);
                        create(contractUserSimulInbox);
                    }
                    break;
                }
            }

            for (Iterator<Long> iter = userContracts.keySet().iterator(); iter.hasNext(); ) {
                UserContract userContract = userContracts.get(iter.next());
                List<ContractUserSimulInbox> contractsUserSimulInbox = new ArrayList<>();
                for (ColContractDetail colContractDetail : userContract.getAssignContracts()) {
                    ContractUserSimulInbox contractUserSimulInbox = ContractUserSimulInbox.createInstance();
                    contractUserSimulInbox.setContract(Contract.createInstance(colContractDetail.getId()));
                    contractUserSimulInbox.setSecUser(userContract.getUser());
                    contractUserSimulInbox.setColType(EColType.FIELD);
                    contractUserSimulInbox.setDebtLevel(colContractDetail.getDebtLevel());
                    contractUserSimulInbox.setProfile(userContract.getUser().getDefaultProfile());
                    contractUserSimulInbox.setArea(colContractDetail.getArea());
                    contractsUserSimulInbox.add(contractUserSimulInbox);
                }
                if (!contractsUserSimulInbox.isEmpty()) {
                    saveOrUpdateBulk(contractsUserSimulInbox);
                }
            }
        }
    }



    /**
     * Assign Inside repo contracts
     */
    @Override
    public void assignInsideRepoContracts() {
        userInboxService.deleteContractSimulByType(EColType.INSIDE_REPO);
        assignAreaContracts(DebtLevelUtils.DEBT_LEVEL_6, EColType.INSIDE_REPO, new String[]{IProfileCode.COL_INS_STAFF});
    }

    /**
     * Assign OA contracts
     */
    @Override
    public void assignOAContracts() {
        userInboxService.deleteContractSimulByType(EColType.OA);
        assignAreaContracts(DebtLevelUtils.DEBT_LEVEL_7, EColType.OA, new String[]{IProfileCode.COL_OA_STAFF});
    }

    /**
     * Assign field contracts
     */
    private void assignAreaContracts(int debtLevel, EColType colType, String[] profiles) {
        List<SecUser> colStaffs = getAssigneeCollectionUsers(profiles, new Integer[]{debtLevel});
        if (colStaffs == null || colStaffs.isEmpty()) {
            return;
        }

        List<ColContractDetail> contracts = getContractsByDebtLevels(new Integer[]{debtLevel}, profiles);

        Map<String, SecUser> staffAreas = getStaffAreas();

        Map<Long, UserContract> userContracts = new HashMap<>();

        int indexStaff = 0;
        List<ContractUserSimulInbox> contractsUnassignUserSimulInbox = new ArrayList<>();
        for (ColContractDetail contract : contracts) {
            Area contractArea = getContractArea(contract, colType);
            boolean assigned = false;
            if (contractArea != null) {
                SecUser selectStaff = colStaffs.get(indexStaff);
                if (staffAreas.containsKey("A" + contractArea.getId() + "S" + selectStaff.getId())) {
                    if (indexStaff == colStaffs.size() - 1) {
                        indexStaff = 0;
                    } else {
                        indexStaff++;
                    }
                    contract.setArea(contractArea);
                    if (!userContracts.containsKey(selectStaff.getId())) {
                        UserContract userContract = new UserContract(selectStaff);
                        userContract.addContract(contract);
                        userContracts.put(selectStaff.getId(), userContract);
                    } else {
                        UserContract userContract = userContracts.get(selectStaff.getId());
                        userContract.addContract(contract);
                    }
                    assigned = true;
                }
            }

            if (!assigned) {
                ContractUserSimulInbox contractUserSimulInbox = ContractUserSimulInbox.createInstance();
                contractUserSimulInbox.setContract(Contract.createInstance(contract.getId()));
                contractUserSimulInbox.setColType(colType);
                contractUserSimulInbox.setDebtLevel(debtLevel);
                contractUserSimulInbox.setArea(contractArea);
                contractsUnassignUserSimulInbox.add(contractUserSimulInbox);
                if (contractsUnassignUserSimulInbox.size() % 200 == 0) {
                    saveOrUpdateBulk(contractsUnassignUserSimulInbox);
                    flush();
                    clear();
                    contractsUnassignUserSimulInbox.clear();
                }
            }
        }

        flush();
        clear();

        for (Iterator<Long> iter = userContracts.keySet().iterator(); iter.hasNext(); ) {
            UserContract userContract = userContracts.get(iter.next());
            List<ContractUserSimulInbox> contractsUserSimulInbox = new ArrayList<>();
            for (ColContractDetail colContractDetail : userContract.getAssignContracts()) {
                ContractUserSimulInbox contractUserSimulInbox = ContractUserSimulInbox.createInstance();
                contractUserSimulInbox.setContract(Contract.createInstance(colContractDetail.getId()));
                contractUserSimulInbox.setSecUser(userContract.getUser());
                contractUserSimulInbox.setColType(colType);
                contractUserSimulInbox.setDebtLevel(colContractDetail.getDebtLevel());
                contractUserSimulInbox.setProfile(userContract.getUser().getDefaultProfile());
                contractUserSimulInbox.setArea(colContractDetail.getArea());
                contractsUserSimulInbox.add(contractUserSimulInbox);
            }
            if (!contractsUserSimulInbox.isEmpty()) {
                saveOrUpdateBulk(contractsUserSimulInbox);
                flush();
                clear();
            }
        }
    }

    /**
     * @param debtLevels
     * @return
     */
    private List<ColContractDetail> getContractsByDebtLevels(Integer[] debtLevels, String[] profiles) {
        Criteria cri = CONT_SRV.getSessionFactory().getCurrentSession().createCriteria(Contract.class, "con");
        cri.setProjection(Projections.projectionList()
                .add(Projections.property("con.id"), "id")
                .add(Projections.property("con.numberGuarantors"), "numberGuarantors")
                .add(Projections.property("col.debtLevel"), "debtLevel")
                .add(Projections.property("col.nbOverdueInDays"), "nbOverdueInDays")
                .add(Projections.property("col.dueDay"), "dueDay")
                .add(Projections.property("app.id"), "appId")
                .add(Projections.property("app.individual.id"), "indId")
                .add(Projections.property("app.company.id"), "comId")
        );

        cri.createCriteria("con.applicant", "app", JoinType.INNER_JOIN);
        cri.createCriteria("con.collections", "col", JoinType.LEFT_OUTER_JOIN);

        cri.add(Restrictions.eq("con.overdue", true));
        cri.add(Restrictions.in("col.debtLevel", debtLevels));

        DetachedCriteria userInboxSubCriteria = DetachedCriteria.forClass(ContractUserInbox.class, "usrinb");
        userInboxSubCriteria.createAlias("usrinb.secUser", "usr", JoinType.INNER_JOIN);
        userInboxSubCriteria.createAlias("usr.defaultProfile", "pro", JoinType.INNER_JOIN);
        userInboxSubCriteria.add(Restrictions.in("pro.code", profiles));

        userInboxSubCriteria.setProjection(Projections.projectionList().add(Projections.property("usrinb.contract_old.id")));
        cri.add(Property.forName("id").notIn(userInboxSubCriteria));

        cri.addOrder(Order.desc("col.debtLevel"));
        cri.addOrder(Order.desc("col.dueDay"));
        cri.addOrder(Order.desc("con.numberGuarantors"));

        cri.setResultTransformer(Transformers.aliasToBean(ColContractDetail.class));
        List<ColContractDetail> contracts = cri.list();
        return contracts;
    }



    /**
     * get contract_old area
     *
     * @param contract
     * @return
     */
    private Area getContractArea(ColContractDetail contract, EColType colType) {
        Area area = null;
        if (contract.getIndId() != null) {
            String query =
                    " select area.are_id from tu_area area," +
                            " (select distinct adr.pro_id, adr.dis_id, adr.com_id" +
                            " from td_address adr" +
                            " inner join td_individual_address inda on inda.add_id = adr.add_id" +
                            " inner join td_individual ind on ind.ind_id = inda.ind_id" +
                            " where ind.ind_id = " + contract.getIndId() +
                            " ) as areainfo" +
                            " where area.col_typ_id = " + colType.getId() +
                            " and area.pro_id = areainfo.pro_id" +
                            " and area.dis_id = areainfo.dis_id" +
                            " and area.com_id = areainfo.com_id";
            try {
                List<NativeRow> cashflowRows = executeSQLNativeQuery(query);
                if (cashflowRows != null && cashflowRows.size() == 1) {
                    for (NativeRow row : cashflowRows) {
                        List<NativeColumn> columns = row.getColumns();
                        int i = 0;
                        area = new Area();
                        area.setId((Long) columns.get(i++).getValue());
                    }
                }
            } catch (NativeQueryException e) {
                LOG.error(e.getMessage(), e);
            }
        }
        return area;
    }


    /**
     * get staff that contain contract_old area
     * @return
     */
    private Map<String, SecUser> getStaffAreas() {
        BaseRestrictions<EColStaffArea> restrictions = new BaseRestrictions<>(EColStaffArea.class);
        List<EColStaffArea> colStaffAreas = list(restrictions);
        Map<String, SecUser> result = new HashMap<>();
        if (!colStaffAreas.isEmpty()) {
            for (EColStaffArea colStaffArea : colStaffAreas) {
                SecUser staff = colStaffArea.getUser();
                result.put("A" + colStaffArea.getArea().getId() + "S" + staff.getId(), staff);
            }
        }
        return result;
    }

    /**
     * @see com.soma.mfinance.core.collection.service.CollectionService#getCollectionContractsByNextActionDate(java.util.Date, java.util.Date)
     */
    @Override
    public List<Contract> getCollectionContractsByNextActionDate(Date startDate, Date endDate) {
        BaseRestrictions<Contract> restrictions = getCollectionContractBaseRestrictions();
        restrictions.addAssociation("col.lastAction", "lasAct", JoinType.INNER_JOIN);

        if (startDate != null) {
            restrictions.addCriterion(Restrictions.ge("lasAct." + CollectionAction.NEXTACTIONDATE, startDate));
        }
        if (endDate != null) {
            restrictions.addCriterion(Restrictions.le("lasAct." + CollectionAction.NEXTACTIONDATE, endDate));
        }
        return list(restrictions);
    }

    /**
     * @see com.soma.mfinance.core.collection.service.CollectionService#getCollectionContractsUnProcessed()
     */
    @Override
    public List<Contract> getCollectionContractsUnProcessed() {
        BaseRestrictions<Contract> restrictions = getCollectionContractBaseRestrictions();
        restrictions.addCriterion(Restrictions.isNull("col.lastAction"));
        return list(restrictions);
    }

    /**
     * @return
     */
    private ContractRestriction getCollectionContractBaseRestrictions() {
        SecUser secUser = UserSessionManager.getCurrentUser();
        ContractRestriction restrictions = new ContractRestriction();
        restrictions.addAssociation("collections", "col", JoinType.INNER_JOIN);

        DetachedCriteria userContractSubCriteria = DetachedCriteria.forClass(ContractUserInbox.class, "cousr");
        userContractSubCriteria.createAlias("cousr.secUser", "usr", JoinType.INNER_JOIN);
        userContractSubCriteria.add(Restrictions.eq("usr.id", secUser.getId()));
        userContractSubCriteria.setProjection(Projections.projectionList().add(Projections.property("cousr.contract_old.id")));

        restrictions.addCriterion(Property.forName(ContractUserInbox.ID).in(userContractSubCriteria));
        return restrictions;
    }

    /**
     * @author kimsuor.seang
     */
    private class UserContract implements Serializable {

        /** */
        private static final long serialVersionUID = -7793823340519897993L;

        private SecUser user;
        private List<ColContractDetail> assignContracts = new ArrayList<>();

        /**
         * @param user
         */
        public UserContract(SecUser user) {
            this.user = user;
        }

        /**
         * @return the user
         */
        public SecUser getUser() {
            return user;
        }

        /**
         * @return the assignContracts
         */
        public List<ColContractDetail> getAssignContracts() {
            return assignContracts;
        }

        /**
         * @param contract
         */
        public void addContract(ColContractDetail contract) {
            assignContracts.add(contract);
        }


    }



    /**
     * @param contraId
     * @return
     */
    private List<CollectionAction> getLatestColActions(Long contraId) {
        CollectionActionRestriction restrictions = new CollectionActionRestriction();
        restrictions.setContractId(contraId);
        return list(restrictions);
    }

    /**
     * @see com.soma.mfinance.core.collection.service.CollectionService#saveOrUpdateLatestColAction(CollectionAction)
     */
    @Override
    public void saveOrUpdateLatestColAction(CollectionAction colAction) {
        // saveOrUpdate Collection Action
        saveOrUpdate(colAction);
        // saveOrUpdate Collection Latest Action
        Contract contract = colAction.getContract();
        Collection collection = contract.getCollection();
        if (collection != null) {
            List<CollectionAction> actions = getLatestColActions(contract.getId());
            if (actions != null && actions.size() > 1) {
                collection.setLastAction(actions.get(0));
            } else {
                collection.setLastAction(colAction);
            }
            saveOrUpdate(collection);
        }
        finHistoryService.addFinHistory(contract, FinHistoryType.FIN_HIS_CNT, colAction.getComment());
    }

    /**
     * @see com.soma.mfinance.core.collection.service.CollectionService#deleteLatestColAction(CollectionAction)
     */
    @Override
    public void deleteLatestColAction(CollectionAction action) {
        Contract contra = action.getContract();
        Collection collection = null;
        if (contra != null) {
            collection = contra.getCollection();
        }
        if (collection != null && collection.getLastAction() != null) {
            if (action.getId() == collection.getLastAction().getId()) {
                List<CollectionAction> actions = getLatestColActions(contra.getId());
                if (actions.size() >= 2) {
                    collection.setLastAction(actions.get(1));
                } else {
                    collection.setLastAction(null);
                }
                update(collection);
            }
        }
        delete(action);
    }

    public SecUser getCollectionUser(Long conId) {
        BaseRestrictions<ContractUserInbox> restrictions = new BaseRestrictions<>(ContractUserInbox.class);
        restrictions.addAssociation("secUser", "usr", JoinType.INNER_JOIN);
        restrictions.addAssociation("usr.defaultProfile", "pro", JoinType.INNER_JOIN);
        restrictions.addCriterion(in("pro.code", new String[]{IProfileCode.COL_PHO_STAFF, IProfileCode.COL_FIE_STAFF, IProfileCode.COL_OA_STAFF}));
        restrictions.addCriterion(eq("contract.id", conId));
        List<ContractUserInbox> results = list(restrictions);
        if (results != null && !results.isEmpty()) {
            return results.get(0).getSecUser();
        }
        return null;
    }



    @Override
    public Collection getCollection(Contract contract) {
        BaseRestrictions<Collection> restrictions = new BaseRestrictions<>(Collection.class);
        restrictions.addCriterion(eq(Collection.CONTRACTID, contract.getId()));
        restrictions.addOrder(Order.desc(Collection.ID));
        if (ENTITY_SRV.list(restrictions) != null && !ENTITY_SRV.list(restrictions).isEmpty()) {
            return ENTITY_SRV.list(restrictions).get(0);
        }
        return null;
    }

// ===== assign overdue contract =====


    @Override
    public List<Collection> assignOverdueContracts() {

        List<Collection> collections = null;
        List<ColAssignment> colAssignments = null;

        try {
            colAssignments = getActivatedColAssignment();
            collections = getActivatedCollection();
            for (Collection collection : collections) {
                assingeContractCollection(collection,colAssignments);
                // Start update collection by officer
                saveOrUpdate(collection);
            }
        } catch (Exception e) {
            logger.error(String.valueOf(e));
        }
        return collections;
    }

    @Override
    public List<Collection> getActivatedCollection() {
        BaseRestrictions<Collection> collectionBaseRestrictions = new BaseRestrictions<>(Collection.class);
        collectionBaseRestrictions.addAssociation("contract", "con", JoinType.INNER_JOIN);
        collectionBaseRestrictions.addCriterion(Restrictions.eq("con." + CONTRACT_STATUS, ContractWkfStatus.FIN));
        collectionBaseRestrictions.addCriterion(Restrictions.eq(STATUS_RECORD,EStatusRecord.ACTIV));
        return list(collectionBaseRestrictions);
    }

    /**
     * 1. area
     2. collection status
     3. and collection officerand overdue day
     * @param collection
     * @return
     */
    @Override
    public Collection assingeContractCollection(Collection collection,List<ColAssignment> colAssignments) {

        for (ColAssignment colAssingment: colAssignments) {

            boolean foundArea = checkCollectionByArea(collection,colAssingment.getAreas());
            boolean foundStatus = false;
            boolean foundOverdueDay = false;
            // check collection status
            if(colAssingment.getStatuses() != null && collection.getColResult() != null) {
                foundStatus = colAssingment.getStatuses().stream().anyMatch(
                        status -> status.getId().equals(collection.getColResult().getId()));
            }
            // collection officerand overdue day
            if(colAssingment.getOverduePeriod() != null && collection.getNbOverdueInDays() != null) {
                int overdueFrom = Integer.parseInt(colAssingment.getOverduePeriod().getOverdueFrom());
                int overdueTo = Integer.parseInt(colAssingment.getOverduePeriod().getOverdueTo());
                Integer nbOverdueInDays = collection.getNbOverdueInDays();
                if(nbOverdueInDays >= overdueFrom && nbOverdueInDays <= overdueTo) {
                    foundOverdueDay = true;
                }
            }

            if(foundArea && foundOverdueDay && foundStatus) {
               collection.setCollectionOfficer(colAssingment.getAssignee());

            } else {
                // Collection not assinge to other collection officer
            }
        }

        return collection;

    }

    private boolean checkCollectionByArea(Collection collection,List<Area> areas) {
        for (Area area: areas) {
            /*if(collection.getArea() != null && collection.getArea().getId().equals(area.getId())) {
                return true;
            }*/
            if(area.getCommunes() != null && collection.getCommune() != null) {
                return area.getCommunes().stream().anyMatch(
                        commune -> commune.getId().equals(collection.getCommune().getId()));
            }
            if(area.getDistricts() != null && collection.getDistrict() != null) {
                return area.getDistricts().stream().anyMatch(
                        commune -> commune.getId().equals(collection.getDistrict().getId()));
            }
        }

        return false;
    }


    @Override
    public List<ColAssignment> getActivatedColAssignment() {
        BaseRestrictions<ColAssignment> colAssRestrictions = new BaseRestrictions<>(ColAssignment.class);
        colAssRestrictions.addCriterion(Restrictions.eq(STATUS_RECORD,EStatusRecord.ACTIV));
        return list(colAssRestrictions);
    }

    @Override
    public List<Collection> getCollectionIncentive(Date date) {
        Boolean isTrue = false;
        List<Collection> collections  = new ArrayList<>();
        BaseRestrictions<Collection> restrictions  = new BaseRestrictions<>(Collection.class);
        List<Collection> collectionList  = ENTITY_SRV.list(restrictions);
        if(collectionList != null &&  collectionList.size() > 0){
            for (Collection collection : collectionList){
                if(collection.getContract() != null){
                    List<Cashflow> cashflows = CASHFLOW_SRV.getCashflowsByNumberInstallment(collection.getContract().getReference(),collection.getLastNumInstallmentPaid());
                    if(cashflows != null && cashflows.size() > 0){
                        for (Cashflow cashflow : cashflows){
                            if(cashflow != null){
                                if(cashflow.getPayment() != null && cashflow.getPayment().getPaymentDate() != null){
                                    if(cashflow.isPaid() == true &&  cashflow.isCancel() ==  false &&  (cashflow.getPayment().getPaymentDate().after(date) ||  cashflow.getPayment().getPaymentDate().before(date))){
                                       isTrue = true;
                                    }
                                }
                            }
                        }
                        if(isTrue == true){
                            collections.add(collection);
                        }
                    }
                }
            }
            if(collections != null && collections.size() > 0){
                return collections;
            }
        }
        return null;
    }

}
