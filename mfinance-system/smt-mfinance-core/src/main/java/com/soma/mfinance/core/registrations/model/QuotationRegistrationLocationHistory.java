package com.soma.mfinance.core.registrations.model;

import com.soma.mfinance.core.quotation.model.Quotation;
import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;

@Entity
@Table(name = "td_quotation_registration_location_history")
public class QuotationRegistrationLocationHistory extends EntityA{

	
	private static final long serialVersionUID = 5969750825405509123L;
	
	private RegistrationStorageLocation registrationStorageLocation;
	
	private Quotation quotation;
	
	/**
	 * @return id
	 * @see org.seuksa.frmk.model.entity.EntityA#getId()
	 */
    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "quo_loc_id", unique = true, nullable = false)
	public Long getId() {
    	 return id;
	}
    
    /**
	 * @return the registrationStorageLocation
	 */
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reg_loc_id", nullable = true)
	public RegistrationStorageLocation getRegistrationStorageLocation() {
		return registrationStorageLocation;
	}

	/**
	 * @param registrationStorageLocation the registrationStorageLocation to set
	 */
	public void setRegistrationStorageLocation(RegistrationStorageLocation registrationStorageLocation) {
		this.registrationStorageLocation = registrationStorageLocation;
	}
    
    /**
	 * @return the quotation
	 */
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "quo_id")
	public Quotation getquotation() {
		return quotation;
	}

	/**
	 * @param quotation the quotation to set
	 */
	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}

}
