package com.soma.mfinance.core.collection.service;

import com.soma.mfinance.core.collection.model.ContractFlag;
import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.collection.model.ContractFlag;

/**
 * Asset model service interface
 * @author kimsuor.seang
 */
public interface ContractFlagService extends BaseEntityService {

	/**
	 * saveOrUpdate legal case info
	 * @param contractFlag
	 */
	void saveOrUpdateLegalCase(ContractFlag contractFlag);
	
	/**
	 * Withdraw / delete legal case info
	 * @param contractFlag
	 */
	void withdrawLegalCase(ContractFlag contractFlag);
	
}
