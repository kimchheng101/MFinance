package com.soma.mfinance.core.workflow;

import com.soma.common.app.workflow.model.EWkfStatus;

/**
 * 
 * @author kimsuor.seang
 *
 */
public class ISRWkfStatus {
	
	public final static EWkfStatus ISRPEN = EWkfStatus.getById(1200); // Pending
	public final static EWkfStatus ISRCONF = EWkfStatus.getById(1201); // Confirm
}
