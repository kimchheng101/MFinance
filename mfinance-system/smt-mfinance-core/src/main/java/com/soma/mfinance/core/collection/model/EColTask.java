package com.soma.mfinance.core.collection.model;

import java.util.List;

import javax.persistence.AttributeConverter;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.soma.mfinance.third.creditbureau.cbc.model.EAccountType;
import org.seuksa.frmk.model.eref.BaseERefEntity;

/**
 * 
 * @author kimsuor.seang
 *
 */
public class EColTask extends BaseERefEntity implements AttributeConverter<EColTask, Long> {
	/** */
	private static final long serialVersionUID = 1926023334544670059L;

	public final static EColTask PHO = new EColTask("PHO", 1);
	public final static EColTask FAR = new EColTask("FAR", 2);

	/**
	 * 
	 */
	public EColTask() {
	}

	/**
	 *
	 * @param code
	 * @param id
	 */
	public EColTask(String code, long id) {
		super(code, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public EColTask convertToEntityAttribute(Long id) {
		return super.convertToEntityAttribute(id);
	}

	@Override
	public Long convertToDatabaseColumn(EColTask arg0) {
		return super.convertToDatabaseColumn(arg0);
	}

	/**
	 *
	 * @return
	 */
	public static List<EColTask> values() {
		return getValues(EColTask.class);
	}

	/**
	 *
	 * @param code
	 * @return
	 */
	public static EColTask getByCode(String code) {
		return getByCode(EColTask.class, code);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public static EColTask getById(long id) {
		return getById(EColTask.class, id);
	}

}