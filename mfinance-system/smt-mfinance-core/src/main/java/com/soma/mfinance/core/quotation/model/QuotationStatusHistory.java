package com.soma.mfinance.core.quotation.model;

import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.frmk.security.model.SecUser;
import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;

/**
 * AUTHOR : kimsuor.seang
 * EMAIL  : chhaichivon1995@gmail.com
 * DATE   : 8/12/2017
 * TIME   : 3:10 PM
 */
@Entity
@Table(name = "td_quotation_status_history")
public class QuotationStatusHistory extends EntityA {
    private Quotation quotation;
    private QuotationWkfStatus quotationStatus;
    private QuotationWkfStatus previousQuotationStatus;
    private SecUser user;
    private long processTime;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "qushi_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * @return the quotation
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "quota_id")
    public Quotation getQuotation() {
        return quotation;
    }

    /**
     * @param quotation the quotation to set
     */
    public void setQuotation(Quotation quotation) {
        this.quotation = quotation;
    }

    /**
     * @return the status
     */
    @Column(name = "qusta_code", nullable = false, length=3)
    @Enumerated(EnumType.STRING)
    public QuotationWkfStatus getQuotationStatus() {
        return quotationStatus;
    }

    /**
     * @param status the status to set
     */
    public void setQuotationStatus(QuotationWkfStatus quotationStatus) {
        this.quotationStatus = quotationStatus;
    }


    /**
     * @return the previousQuotationStatus
     */
    @Column(name = "qusta_code_prev", nullable = true, length = 3)
    @Enumerated(EnumType.STRING)
    public QuotationWkfStatus getPreviousQuotationStatus() {
        return previousQuotationStatus;
    }

    /**
     * @param previousQuotationStatus the previousQuotationStatus to set
     */
    public void setPreviousQuotationStatus(QuotationWkfStatus previousQuotationStatus) {
        this.previousQuotationStatus = previousQuotationStatus;
    }

    /**
     * @return the user
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_usr_id")
    public SecUser getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(SecUser user) {
        this.user = user;
    }

    /**
     * @return the processTime
     */
    @Transient
    public long getProcessTime() {
        return processTime;
    }

    /**
     * @param processTime the processTime to set
     */
    public void setProcessTime(long processTime) {
        this.processTime = processTime;
    }

}
