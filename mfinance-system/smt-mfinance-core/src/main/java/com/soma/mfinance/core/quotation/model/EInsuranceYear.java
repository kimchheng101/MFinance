package com.soma.mfinance.core.quotation.model;

import org.seuksa.frmk.model.eref.BaseERefData;

import javax.persistence.AttributeConverter;
import java.util.List;

/**
 * Created by s.torn on 8/15/2017.
 */
public class EInsuranceYear extends BaseERefData implements AttributeConverter<EInsuranceYear, Long> {

    public final static EInsuranceYear YEAR1 = new EInsuranceYear("YEAR1", 1);
    public final static EInsuranceYear YEAR2 = new EInsuranceYear("YEAR2", 2);
    public final static EInsuranceYear YEAR3 = new EInsuranceYear("YEAR3", 3);
    public final static EInsuranceYear YEAR4 = new EInsuranceYear("YEAR4", 4);

    /**
     */
    public EInsuranceYear() {
    }

    /**
     * @param code
     * @param id
     */
    public EInsuranceYear(String code, long id) {
        super(code, id);
    }

    @SuppressWarnings("unchecked")
    @Override
    public EInsuranceYear convertToEntityAttribute(Long id) {
        return super.convertToEntityAttribute(id);
    }

    @Override
    public Long convertToDatabaseColumn(EInsuranceYear arg0) {
        return super.convertToDatabaseColumn(arg0);
    }

    /**
     * @return
     */
    public static List<EInsuranceYear> values() {
        return getValues(EInsuranceYear.class);
    }

    /**
     * @param code
     * @return
     */
    public static EInsuranceYear getByCode(String code) {
        return getByCode(EInsuranceYear.class, code);
    }

    /**
     * @param id
     * @return
     */
    public static EInsuranceYear getById(long id) {
        return getById(EInsuranceYear.class, id);
    }
}
