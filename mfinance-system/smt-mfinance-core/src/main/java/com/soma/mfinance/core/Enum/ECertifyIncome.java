package com.soma.mfinance.core.Enum;

import org.seuksa.frmk.model.eref.BaseERefData;

import javax.persistence.AttributeConverter;
import java.util.List;

/**
 * Created by b.chea on 2/25/2017.
 */
public class ECertifyIncome extends BaseERefData implements AttributeConverter<ECertifyIncome, Long> {

    public static final ECertifyIncome TEST = new ECertifyIncome("CURRENT_ADDRESS", 1L);

    public ECertifyIncome(){}

    public ECertifyIncome(String code, long id){ super(code, id);}

    @Override
    public Long convertToDatabaseColumn(ECertifyIncome attribute) {
        return super.convertToDatabaseColumn(attribute);
    }

    @Override
    public ECertifyIncome convertToEntityAttribute(Long id) {
        return getById(ECertifyIncome.class, id);
    }

    public static List<ECertifyIncome> values() {
        return getValues(ECertifyIncome.class);
    }

    public static ECertifyIncome getByCode(String code) {
        return  getByCode(ECertifyIncome.class, code);
    }

    public static ECertifyIncome getById(long id) {
        return  getById(ECertifyIncome.class, id);
    }
}
