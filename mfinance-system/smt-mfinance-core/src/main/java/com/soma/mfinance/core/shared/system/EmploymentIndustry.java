package com.soma.mfinance.core.shared.system;

import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;

/**
 * @author by kimsuor.seang  on 10/18/2017.
 */
@Entity
@Table(name = "tu_employment_industry")
public class EmploymentIndustry extends EntityRefA {

    private static final long serialVersionUID = -6762740091201743621L;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "emind_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }


    @Column(name = "emind_code", nullable = false, length = 5, unique = true)
    @Override
    public String getCode() {
        return super.getCode();
    }

    @Column(name = "emind_desc", nullable = false, length=50)
    @Override
    public String getDesc() {
        return super.getDesc();
    }

    @Override
    @Column(name = "emind_desc_en", nullable = false, length=50)
    public String getDescEn() {
        return super.getDescEn();
    }
}
