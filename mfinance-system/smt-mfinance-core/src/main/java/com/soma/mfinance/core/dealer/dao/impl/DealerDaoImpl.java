package com.soma.mfinance.core.dealer.dao.impl;

import org.seuksa.frmk.dao.impl.BaseEntityDaoImpl;
import org.springframework.stereotype.Repository;

import com.soma.mfinance.core.dealer.dao.DealerDao;

/**
 * Dealer data access implementation
 * @author kimsuor.seang
 *
 */
@Repository
public class DealerDaoImpl extends BaseEntityDaoImpl implements DealerDao {

}
