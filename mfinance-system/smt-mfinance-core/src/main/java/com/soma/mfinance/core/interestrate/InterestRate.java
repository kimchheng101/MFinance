package com.soma.mfinance.core.interestrate;

import com.soma.mfinance.core.financial.model.MInterestRate;
import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;

/**
 * Created by b.chea on 2/22/2017.
 */
@Entity
@Table(name = "tu_interest_rate")
public class InterestRate extends EntityRefA implements MInterestRate {

    private Double value;

    /**
     * @return id
     */
    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "int_rat_sta_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * @see org.seuksa.frmk.model.entity.EntityRefA#getCode()
     */
    @Override
    @Transient
    public String getCode() {
        return "TMP";
    }

    @Override
    @Column(name = "int_rat_desc", nullable = true, length=255)
    public String getDesc() {
        return super.getDesc();
    }

    @Override
    @Column(name = "int_rat_desc_en", nullable = true, length=255)
    public String getDescEn() {
        return super.getDescEn();
    }

    /**
     * @return the value
     */
    @Column(name = "int_rat_value", nullable = true)
    public Double getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Double value) {
        this.value = value;
    }
}
