package com.soma.mfinance.glf.mplus.accounting.service.impl;

import com.soma.mfinance.core.accounting.InstallmentVO;
import com.soma.mfinance.core.accounting.MultiAmountTypeVO;
import com.soma.mfinance.core.accounting.PayOffVO;
import com.soma.mfinance.core.accounting.ServiceFeeVO;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowType;
import com.soma.mfinance.core.contract.model.cashflow.ETreasuryType;
import com.soma.mfinance.core.contract.service.cashflow.impl.CashflowUtils;
import com.soma.mfinance.core.financial.model.EServiceType;
import com.soma.mfinance.core.financial.model.FinService;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.payment.model.EPaymentMethod;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationService;
import com.soma.mfinance.core.shared.contract.PenaltyVO;
import com.soma.mfinance.core.shared.mplus.accounting.CashflowServiceMfp;
import com.soma.mfinance.core.shared.mplus.accounting.InstallmentServiceMfp;
import com.soma.finance.services.shared.Schedule;
import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyMathUtils;
import org.seuksa.frmk.tools.amount.Amount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.IntStream;

/**
 * 
 * @author vi.sok
 * @since 08/05/2017
 */
@Service("installmentServiceMfp")
@Transactional
public class InstallmentServiceImplMfp extends BaseEntityServiceImpl implements InstallmentServiceMfp, FinServicesHelper {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2395938011631250310L;
	protected Logger LOG = LoggerFactory.getLogger(getClass());
	
	@Autowired
    private EntityDao dao;
	
	@Autowired
	private CashflowServiceMfp cashflowServiceMfp;
	@Override
	public BaseEntityDao getDao() {
		// TODO Auto-generated method stub
		return dao;
	}
//==========================================Service Fee installment================================================================
	@Override
	public HashMap<String, ServiceFeeVO> getServiceFeeInstallments(Quotation quotation) {
		List<QuotationService> quotationServices = quotation.getQuotationServices();
		HashMap<String, ServiceFeeVO> serviceVOs = new HashMap<String, ServiceFeeVO>();
		if (quotationServices != null && !quotationServices.isEmpty()) {
			for (QuotationService quotationService : quotationServices) {
				FinService finService = quotationService.getService();
				int eFrequency = quotation.getFrequency().getNbMonths();
				double serviceFee = quotationService.getTiPrice()/(quotation.getTerm()/eFrequency);
				double vat = quotationService.getVatPrice()/(quotation.getTerm()/eFrequency);
				serviceVOs.put(quotationService.getService().getCode(), new ServiceFeeVO(serviceFee,vat,finService));
			}
		}
		return serviceVOs;
	}

	@Override
	public ServiceFeeVO getServiceInstallmentByCode(Quotation quotation, EServiceType serviceType) {
		HashMap<String, ServiceFeeVO> services = getServiceFeeInstallments(quotation);
		if(services != null && !services.isEmpty() && serviceType != null){
			return services.get(serviceType.getCode());
		}
		return null;
	}

	@Override
	public ServiceFeeVO getServiceInstallmentByCode(
			HashMap<String, ServiceFeeVO> services, EServiceType serviceType) {
		if(services != null && !services.isEmpty()){
			return services.get(serviceType.getCode());
		}
		return null;
	}

	
	
	//============================================ installment payment service============================================
	@Override
	public HashMap<Integer, List<InstallmentVO>> getInstallmentVOs(Contract contract) {
		HashMap<Integer, List<InstallmentVO>> installments = new HashMap<Integer, List<InstallmentVO>>();
		List<Schedule> shedules = cashflowServiceMfp.getCashflows(contract);
		int term = contract.getTerm();
		List<QuotationService> quotationServices = contract.getQuotation().getQuotationServices();
		for (int i = 0; i < term; i++) {
			Schedule shedule = shedules.get(i);
			List<InstallmentVO> installmentVOs = new ArrayList<InstallmentVO>();
			double vatPrincipal = 0d;
			InstallmentVO installmentCap = new InstallmentVO();
			installmentCap.setCashflowType(ECashflowType.CAP);
			installmentCap.setBalance(shedule.getBalanceAmount());
			installmentCap.setInstallmentDate(shedule.getInstallmentDate());
			installmentCap.setNumInstallment(shedule.getN());
			installmentCap.setTiamount(shedule.getPrincipalAmount());
			installmentCap.setPeriodStartDate(shedule.getPeriodStartDate());
			installmentCap.setPeriodEndDate(shedule.getPeriodEndDate());
			if (contract.getVatValue() > 0) {
				vatPrincipal = shedule.getPrincipalAmount()*(contract.getVatValue()/100);
			}
			installmentCap.setVatAmount(vatPrincipal);
			installmentCap.setContract(contract);
			installmentVOs.add(installmentCap);
			InstallmentVO installmentIAP = new InstallmentVO();
			installmentIAP.setCashflowType(ECashflowType.IAP);
			installmentIAP.setInstallmentDate(shedule.getInstallmentDate());
			installmentIAP.setNumInstallment(shedule.getN());
			installmentIAP.setTiamount(shedule.getInterestAmount());
			installmentIAP.setVatAmount(0d);
			installmentIAP.setPeriodStartDate(shedule.getPeriodStartDate());
			installmentIAP.setPeriodEndDate(shedule.getPeriodEndDate());
			installmentIAP.setContract(contract);
			installmentVOs.add(installmentIAP);
			HashMap<String, ServiceFeeVO> services = getServiceFeeInstallments(contract.getQuotation());
			if (quotationServices != null && !quotationServices.isEmpty()
					&& services != null && !services.isEmpty()) {
				for (QuotationService quotationService : quotationServices) {
					if(quotationService.getService() != null && quotationService.getService().getServiceType() != null) {
						ServiceFeeVO ServiceFeeVO = getServiceInstallmentByCode(quotationService.getQuotation(), quotationService.getService().getServiceType());
						installmentVOs.add(getInstallmentVOServiceFee(ServiceFeeVO,shedule, contract));
					}
				}
			}
			installments.put(shedule.getN(), installmentVOs);
		}
		return installments;
	}
	@Override
	public HashMap<Integer, List<InstallmentVO>> getInstallmentVOs(
			Quotation quotation, Date firstPaymentDate) {
		HashMap<Integer, List<InstallmentVO>> installments = new HashMap<Integer, List<InstallmentVO>>();
		List<Schedule> shedules = cashflowServiceMfp.getCashflows(quotation, firstPaymentDate);
		int term = quotation.getTerm();
		List<QuotationService> quotationServices = quotation.getQuotationServices();
		for (int i = 0; i < term; i++) {
			Schedule shedule = shedules.get(i);
			List<InstallmentVO> installmentVOs = new ArrayList<InstallmentVO>();
			double vatPrincipal = 0d;
			InstallmentVO installmentCap = new InstallmentVO();
			installmentCap.setCashflowType(ECashflowType.CAP);
			installmentCap.setInstallmentDate(shedule.getInstallmentDate());
			installmentCap.setNumInstallment(shedule.getN());
			installmentCap.setTiamount(shedule.getPrincipalAmount());
			installmentCap.setPeriodStartDate(shedule.getPeriodStartDate());
			installmentCap.setPeriodEndDate(shedule.getPeriodEndDate());
			installmentCap.setBalance(shedule.getBalanceAmount());
			if (quotation.getVatValue() > 0) {
				vatPrincipal = shedule.getPrincipalAmount()*(quotation.getVatValue()/100);
			}
			installmentCap.setVatAmount(vatPrincipal);
			installmentCap.setContract(quotation.getContract());
			installmentVOs.add(installmentCap);
			InstallmentVO installmentIAP = new InstallmentVO();
			installmentIAP.setCashflowType(ECashflowType.IAP);
			installmentIAP.setInstallmentDate(shedule.getInstallmentDate());
			installmentIAP.setNumInstallment(shedule.getN());
			installmentIAP.setTiamount(shedule.getInterestAmount());
			installmentIAP.setVatAmount(0d);
			installmentIAP.setPeriodStartDate(shedule.getPeriodStartDate());
			installmentIAP.setPeriodEndDate(shedule.getPeriodEndDate());
			installmentIAP.setContract(quotation.getContract());
			installmentVOs.add(installmentIAP);
			HashMap<String, ServiceFeeVO> services = getServiceFeeInstallments(quotation);
			if (quotationServices != null && !quotationServices.isEmpty()
					&& services != null && !services.isEmpty()) {
				for (QuotationService quotationService : quotationServices) {
					if(quotationService.getService() != null && quotationService.getService().getServiceType() != null) {
						ServiceFeeVO ServiceFeeVO = getServiceInstallmentByCode(quotationService.getQuotation(), quotationService.getService().getServiceType());
						installmentVOs.add(getInstallmentVOServiceFee(ServiceFeeVO,shedule, quotation));
					}

				}
			}
			installments.put(shedule.getN(), installmentVOs);
		}
		return installments;
	}
	@Override
	public List<InstallmentVO> getInstallmentVOs(Contract contract,
			int numInstallment) {
		HashMap<Integer, List<InstallmentVO>> installmetns = getInstallmentVOs(contract);
		return installmetns.get(numInstallment);
	}

	@Override
	public List<InstallmentVO> getInstallmentVOs(Contract contract, Date dueDate) {
		HashMap<Integer, List<InstallmentVO>> installmetns = getInstallmentVOs(contract);
		if (installmetns != null && !installmetns.isEmpty()) {
			for (int i = 0; i < contract.getTerm(); i++) {
				List<InstallmentVO> installmentVOs = installmetns.get(i);
				if(installmentVOs != null && !installmentVOs.isEmpty()){
					Date installment = installmentVOs.get(0).getInstallmentDate();
					if(DateUtils.getDateAtBeginningOfDay(installment).equals(DateUtils.getDateAtBeginningOfDay(dueDate))){
						return installmentVOs;
					}
				}
			}
		}
		return null;
	}
	@Override
	public List<InstallmentVO> getInstallmentVOs(List<Cashflow> cashflows) {
		List<InstallmentVO> installmentVOs = new ArrayList<InstallmentVO>();
		if (cashflows != null && !cashflows.isEmpty()) {
			for (Cashflow cashflow : cashflows) {
				installmentVOs.add(getInstallmentVO(cashflow));
			}
		}
		return installmentVOs;
	}
	@Override
	public PayOffVO getInstallmentPayOffVos(Contract contract, Date paymentDate,EPaymentMethod ePaymentMethod) {
		int currentNumInstallment = 1;
		if (contract.getLastPaidNumInstallment() != null) {
			currentNumInstallment = contract.getLastPaidNumInstallment() + 1;//currentNumInstallment is current installment number
		}
		Amount tiPrincipal = new Amount(0d, 0d, 0d);
		Amount tiInterestRate = new Amount(0d, 0d, 0d);
		Amount tiInsurance = new Amount(0d, 0d, 0d);
		Amount tiServiceFee = new Amount(0d, 0d, 0d);
		Amount tiTransferOwerShip = new Amount(0d, 0d, 0d);
		Amount tiOtherAmount = new Amount(0d, 0d, 0d);
		Amount tiPenaltyAmount = new Amount(0d, 0d, 0d);
		Amount tiCommissionAmount= new Amount(0d, 0d, 0d);
		Date installmentDate = null;
		int termsOfcontract = contract.getTerm();
		List<PenaltyVO>penaltyVOs=new ArrayList<>();
		int installmentNumInsuranceLastest = ((currentNumInstallment-1)/12 + 1) * 12;

		for (int i = currentNumInstallment; i <= termsOfcontract; i++){
			List<InstallmentVO> installmentByNumInstallments = getInstallmentVOs(contract, i);
			InstallmentVO installmentVOFirstIndex = installmentByNumInstallments.get(0);
			for (InstallmentVO installmentVO : installmentByNumInstallments) {
				if (installmentVO.getCashflowType().equals(ECashflowType.CAP)) {
					tiPrincipal.plusTiAmount(installmentVO.getTiamount());
					tiPrincipal.plusTeAmount(installmentVO.getTiamount());
					tiPrincipal.plusVatAmount(installmentVO.getVatAmount());
				} else if(installmentVO.getCashflowType().equals(ECashflowType.IAP)) {
					/*if(contract.getPenaltyTermPayoff() >= i){
						tiInterestRate.plusTiAmount(installmentVO.getTiamount());
                        tiInterestRate.plusTeAmount(installmentVO.getTiamount());
					} else {
						long nbDiff = DateUtils.getDiffInDays(installmentVO.getInstallmentDate(),DateUtils.getDateAtEndOfDay(paymentDate));
						long nbDaysOfMonth = DateUtils.getNbDaysInMonth(DateUtils.today());

						if(paymentDate.after(installmentVO.getInstallmentDate())){
							tiInterestRate.plusTiAmount(installmentVO.getTiamount());
							tiInterestRate.plusTeAmount(installmentVO.getTiamount());
						} else if(nbDiff <= 30){
							tiInterestRate.plusTiAmount(MyMathUtils.roundAmountTo((nbDaysOfMonth-nbDiff) * installmentVO.getTiamount() / nbDaysOfMonth));
						}
					}*/
					tiInterestRate=calulateInterestPaidOff(contract,tiInterestRate,installmentVO.getTiamount(),paymentDate,i,installmentVO.getInstallmentDate());

				} else if(installmentVO.getService() != null && EServiceType.INSFEE.getCode().equals(installmentVO.getService().getCode())){
					if(installmentNumInsuranceLastest!=0 && installmentVO.getNumInstallment()<=installmentNumInsuranceLastest){
						tiInsurance.plusTiAmount(installmentVO.getTiamount());
						tiInsurance.plusTeAmount(installmentVO.getTiamount());
						tiInsurance.plusVatAmount(installmentVO.getVatAmount());
						/*
						tiInsuranceAll = tiInsurance.getTiAmount() + tiInsurance.getVatAmount();
					Double tiInsOneMonth = installmentVO.getTiamount() + installmentVO.getVatAmount();

					if(lastNumInstallment == 12
							|| (lastNumInstallment == 24 && firstInstallmentNotPaid > 12)
							|| (lastNumInstallment == 36 && firstInstallmentNotPaid > 24) )
						insFeePaidOff = tiInsuranceAll;
					else if (lastNumInstallment == 24 && firstInstallmentNotPaid <= 12)
						insFeePaidOff = tiInsuranceAll - (tiInsOneMonth * 12);
					else if (lastNumInstallment == 36) {
						if (firstInstallmentNotPaid <= 12)
							insFeePaidOff = tiInsuranceAll - (tiInsOneMonth * 24);
						else if (firstInstallmentNotPaid > 12 && firstInstallmentNotPaid <= 24)
							insFeePaidOff = tiInsuranceAll - (tiInsOneMonth * 12);
					}
						 */
					}
				} else if(installmentVO.getService() != null
						&& (EServiceType.SRVFEE.getCode().equals(installmentVO.getService().getCode())
							||EServiceType.SRVFEEEXT.getCode().equals(installmentVO.getService().getCode()))) {
					tiServiceFee.plusTiAmount(installmentVO.getTiamount());
					tiServiceFee.plusTeAmount(installmentVO.getTiamount());
					tiServiceFee.plusVatAmount(installmentVO.getVatAmount());

				}else if(installmentVO.getService() != null
							&& (EServiceType.TRANSFEE.getCode().equals(installmentVO.getService().getCode()))){
					tiTransferOwerShip.plusTiAmount(installmentVO.getTiamount());
					tiTransferOwerShip.plusTeAmount(installmentVO.getTiamount());
					tiTransferOwerShip.plusVatAmount(installmentVO.getVatAmount());

				} else {
					tiOtherAmount.plusTiAmount(installmentVO.getTiamount());
					tiOtherAmount.plusTeAmount(installmentVO.getTiamount());
					tiOtherAmount.plusVatAmount(installmentVO.getVatAmount());
				}
				if(i == termsOfcontract){
					installmentDate = installmentVO.getInstallmentDate();
				}
			}
			PenaltyVO penaltyVO = CONT_SRV.calculatePenalty(contract, installmentVOFirstIndex.getInstallmentDate(), paymentDate, contract.getTiInstallmentAmount());
			if (penaltyVO!= null && penaltyVO.getPenaltyAmount() != null) {
				tiPenaltyAmount.plusTiAmount(penaltyVO.getPenaltyAmount().getTiAmount());
				tiPenaltyAmount.plusTeAmount(penaltyVO.getPenaltyAmount().getTiAmount());
				tiPenaltyAmount.plusVatAmount(penaltyVO.getPenaltyAmount().getVatAmount());
				penaltyVOs.add(penaltyVO);
			}

		}
		PayOffVO payOffVO = new PayOffVO();
		payOffVO.setInstallmentDate(installmentDate);
		payOffVO.setNumInstallment(termsOfcontract);
		payOffVO.setPricipal(tiPrincipal);
		payOffVO.setInterest(tiInterestRate);
		/*
		return insurance
		 */
		int numInstOfEchTerms=currentNumInstallment%12!=0 ? currentNumInstallment%12 : 12;
		int []oneTosixNumInstallment= {1, 2, 3, 4, 5, 6};
		int []nineToTwelve= {9, 10, 11, 12};
		double amountInsurAfterReturn=0d;
		double returnPercent=0d;
		double amountInsurReturn=0d;
		if(IntStream.of(oneTosixNumInstallment).anyMatch(x -> x == numInstOfEchTerms)){
			returnPercent=(100-((numInstOfEchTerms+1)*10))/100d;
			amountInsurReturn=returnPercent*tiInsurance.getTiAmount();
			amountInsurAfterReturn=tiInsurance.getTiAmount()-amountInsurReturn;
		}else if(numInstOfEchTerms==7){
			returnPercent=0.25;
			amountInsurReturn=returnPercent*tiInsurance.getTiAmount();
			amountInsurAfterReturn=tiInsurance.getTiAmount()-amountInsurReturn;//return to customer 25% of total insurance
		}else if(numInstOfEchTerms==8){
			returnPercent=0.2;
			amountInsurReturn=returnPercent*tiInsurance.getTiAmount();
			amountInsurAfterReturn=tiInsurance.getTiAmount()-amountInsurReturn;//return to customer 20% of total insurance
		}else if(IntStream.of(nineToTwelve).anyMatch(x -> x == numInstOfEchTerms)){
			amountInsurAfterReturn=tiInsurance.getTiAmount();//return to customer 0% of total insurance
		}
		tiInsurance.setTiAmount(amountInsurAfterReturn);
		tiInsurance.setTeAmount(amountInsurAfterReturn);
		payOffVO.setInsuranceFee(tiInsurance);
		payOffVO.setInsuranceFeeReturn(amountInsurReturn);
		/* **/
		payOffVO.setServiceFee(tiServiceFee);
		payOffVO.setTransferOwnerShip(tiTransferOwerShip);
		payOffVO.setOtherAmount(tiOtherAmount);
		payOffVO.setPenaltyAmount(tiPenaltyAmount);
		payOffVO.setPenaltyVOs(penaltyVOs);
		List<Cashflow> cashflows = getCashflows(payOffVO,contract,ePaymentMethod);
		for(Cashflow cashflow:cashflows){
			if(cashflow.getService()!=null && cashflow.getService().getServiceType().equals(EServiceType.COMM)){
				tiCommissionAmount=new Amount(cashflow.getTiInstallmentAmount(),cashflow.getVatInstallmentAmount(),cashflow.getTiInstallmentAmount());
				payOffVO.setCommissionFee(tiCommissionAmount);
			}
		}
		payOffVO.setCashflows(cashflows);
		return payOffVO;
	}
	
	
	//==================================Private Method=============================================================
	/**
	 * 
	 * @param payOff
	 * @param contract
	 * @return
	 */
	private List<Cashflow> getCashflows(PayOffVO payOff, Contract contract,EPaymentMethod ePaymentMethod){
		List<InstallmentVO> installmentVOs = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(contract, payOff.getNumInstallment());
		List<Cashflow> cashflows = new ArrayList<>();
		if(ePaymentMethod!=null && ePaymentMethod.getService()!=null){
			double commision;
			double vatOfCommission=0d;
			FinService finService=ePaymentMethod.getService();
			commision =finService.getTiPrice();
			if (finService.getVat() != null) {
				vatOfCommission = finService.getVat().getValue() * commision / 100;
			}

			Cashflow cashflowCommission = CashflowUtils.createCashflow(contract.getProductLine(),
					null, contract, 0d,
					ECashflowType.FEE, ETreasuryType.APP, null,ePaymentMethod,
					commision, vatOfCommission, commision,
					installmentVOs.get(0).getInstallmentDate(), installmentVOs.get(0).getPeriodStartDate(), installmentVOs.get(0).getPeriodEndDate(), installmentVOs.get(0).getNumInstallment());
			cashflowCommission.setService(finService);
			cashflows.add(cashflowCommission);
		}

		if(payOff.getPenaltyVOs()!=null) {
			double penaltyAmount = 0d;
			double vatPenalty = 0d;
			for (PenaltyVO penaltyVO : payOff.getPenaltyVOs()) {
				if (penaltyVO != null && penaltyVO.getNumPenaltyDays() != null && penaltyVO.getNumPenaltyDays() > 0) {
					penaltyAmount += penaltyVO.getPenaltyAmount().getTiAmount();
					vatPenalty += penaltyVO.getPenaltyAmount().getVatAmount();
				}
			}
			Cashflow cashflowPenalty;
			InstallmentVO installmentVO = installmentVOs.get(0);
			if (penaltyAmount!=0d) {
				if (ePaymentMethod != null) {
					cashflowPenalty = CashflowUtils.createCashflow(contract.getProductLine(),
							null, contract, 0d,
							ECashflowType.PEN, ETreasuryType.APP, null, ePaymentMethod,
							penaltyAmount, vatPenalty, penaltyAmount,
							installmentVO.getInstallmentDate(), installmentVO.getPeriodStartDate(), installmentVO.getPeriodEndDate(), installmentVO.getNumInstallment());
				} else {
					cashflowPenalty = CashflowUtils.createCashflow(contract.getProductLine(),
							null, contract, 0d,
							ECashflowType.PEN, ETreasuryType.APP, null, contract.getProductLine().getPaymentConditionCap(),
							penaltyAmount, vatPenalty, penaltyAmount,
							installmentVO.getInstallmentDate(), installmentVO.getPeriodStartDate(), installmentVO.getPeriodEndDate(), installmentVO.getNumInstallment());
				}

				cashflows.add(cashflowPenalty);
			}
		}

		for (InstallmentVO installmentVO : installmentVOs) {
			double amount;
			double vatAmount;
			if (installmentVO.getCashflowType() == ECashflowType.CAP) {
				amount = payOff.getPricipal().getTiAmount();
				vatAmount = payOff.getPricipal().getVatAmount();
			} else if(installmentVO.getCashflowType() == ECashflowType.IAP) {
				amount = payOff.getInterest().getTiAmount();
				vatAmount = payOff.getInterest().getVatAmount();
			}else if(installmentVO.getService() != null &&(EServiceType.INSFEE.getCode().equals(installmentVO.getService().getCode()))){
				amount = payOff.getInsuranceFee().getTiAmount();
				vatAmount = payOff.getInsuranceFee().getVatAmount();
			}else if(installmentVO.getService() != null &&(EServiceType.TRANSFEE.getCode().equals(installmentVO.getService().getCode()))){
				amount = payOff.getTransferOwnerShip().getTiAmount();
				vatAmount = payOff.getTransferOwnerShip().getVatAmount();
			}else if(installmentVO.getService() != null &&
					((EServiceType.SRVFEE.getCode().equals(installmentVO.getService().getCode()))
					|| (EServiceType.SRVFEEEXT.getCode().equals(installmentVO.getService().getCode())))){
				amount = payOff.getServiceFee().getTiAmount();
				vatAmount = payOff.getServiceFee().getVatAmount();
			}else {
				amount = payOff.getOtherAmount().getTiAmount();
				vatAmount = payOff.getOtherAmount().getVatAmount();
			}

			Cashflow cashflow;
			if(amount==0d){
				continue;
			}
			if(ePaymentMethod!=null){
				cashflow = CashflowUtils.createCashflow(contract.getProductLine(),
						null, contract, 0d,
						installmentVO.getCashflowType(), ETreasuryType.APP, null, ePaymentMethod,
						amount, vatAmount, amount,
						installmentVO.getInstallmentDate(), installmentVO.getPeriodStartDate(), installmentVO.getPeriodEndDate(), installmentVO.getNumInstallment());
			}else {
				cashflow = CashflowUtils.createCashflow(contract.getProductLine(),
						null, contract, 0d,
						installmentVO.getCashflowType(), ETreasuryType.APP, null, contract.getProductLine().getPaymentConditionCap(),
						amount, vatAmount, amount,
						installmentVO.getInstallmentDate(), installmentVO.getPeriodStartDate(), installmentVO.getPeriodEndDate(), installmentVO.getNumInstallment());
			}
			cashflow.setService(installmentVO.getService());
			cashflows.add(cashflow);
		}
		return cashflows;
	}
	/**
	 * 
	 * @param serviceFeeVO
	 * @param shedule
	 * @param contract
	 * @return
	 */
	private InstallmentVO getInstallmentVOServiceFee(ServiceFeeVO serviceFeeVO, Schedule shedule, Contract contract){
		InstallmentVO installmentSerivceFee = new InstallmentVO();
		installmentSerivceFee.setCashflowType(ECashflowType.FEE);
		installmentSerivceFee.setInstallmentDate(shedule.getInstallmentDate());
		installmentSerivceFee.setNumInstallment(shedule.getN());
		if (serviceFeeVO != null) {
			installmentSerivceFee.setTiamount(serviceFeeVO.getTiAmount());
			installmentSerivceFee.setVatAmount(serviceFeeVO.getVat());
			installmentSerivceFee.setService(serviceFeeVO.getService());
		}
		installmentSerivceFee.setContract(contract);
		installmentSerivceFee.setPeriodStartDate(shedule.getPeriodStartDate());
		installmentSerivceFee.setPeriodEndDate(shedule.getPeriodEndDate());
		return installmentSerivceFee;
	}
	/**
	 * 
	 * @param serviceFeeVO
	 * @param shedule
	 * @param quotation
	 * @return
	 */
	private InstallmentVO getInstallmentVOServiceFee(ServiceFeeVO serviceFeeVO, Schedule shedule, Quotation quotation){
		InstallmentVO installmentSerivceFee = new InstallmentVO();
		installmentSerivceFee.setCashflowType(ECashflowType.FEE);
		installmentSerivceFee.setInstallmentDate(shedule.getInstallmentDate());
		installmentSerivceFee.setNumInstallment(shedule.getN());

		if (serviceFeeVO != null) {
			installmentSerivceFee.setTiamount(serviceFeeVO.getTiAmount());
			installmentSerivceFee.setVatAmount(serviceFeeVO.getVat());
			installmentSerivceFee.setService(serviceFeeVO.getService());
			installmentSerivceFee.setContract(quotation.getContract());
			installmentSerivceFee.setPeriodStartDate(shedule.getPeriodStartDate());
			installmentSerivceFee.setPeriodEndDate(shedule.getPeriodEndDate());
		}
		return installmentSerivceFee;
	}
	/**
	 * 
	 * @param cashflow
	 * @return
	 */
	private InstallmentVO getInstallmentVO(Cashflow cashflow){
		InstallmentVO installmentVO = new InstallmentVO();
		installmentVO.setCashflowType(cashflow.getCashflowType());
		installmentVO.setInstallmentDate(cashflow.getInstallmentDate());
		installmentVO.setNumInstallment(cashflow.getNumInstallment());
		installmentVO.setTiamount(cashflow.getTiInstallmentAmount());
		installmentVO.setVatAmount(cashflow.getVatInstallmentAmount());
		installmentVO.setService(cashflow.getService());
		installmentVO.setContract(cashflow.getContract());
		installmentVO.setPeriodStartDate(cashflow.getPeriodStartDate());
		installmentVO.setPeriodEndDate(cashflow.getPeriodEndDate());
		return installmentVO;
	}

	@Override
	public Double getOutStanding(Quotation quotation) {
		if (quotation.getContract() != null) {
			Contract contract = CONT_SRV.getContractById(quotation.getContract().getId());
			if (contract.getLastPaidNumInstallment() > 0) {
				List<InstallmentVO> nextInstallmentVOList = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(contract, contract.getLastPaidNumInstallment());
				return nextInstallmentVOList.get(0).getBalance();
			}
		}
		return quotation.getTiFinanceAmount();
	}

	@Override
	public MultiAmountTypeVO getEachAmountTypes(Contract contract,List<InstallmentVO> installmentByNumInstallments){
		double tiPrincipal = 0d;
		double tiInterestRate = 0d;
		double tiInsurance = 0d;
		double tiServiceFee = 0d;
		double otherAmount = 0d;
		double totalInstallment;
		double installmentAmount;
		double remainingBalance;
		double tiTransFee = 0d;
		double penaltyAmount = 0d;
		double vatPenalty=0d;
		double vatTotalInstallment=0d;
		InstallmentVO installmentVOFirstIndex = installmentByNumInstallments.get(0);
		PenaltyVO penaltyVO = CONT_SRV.calculatePenalty(contract, installmentVOFirstIndex.getInstallmentDate(), DateUtils.todayH00M00S00(), contract.getTiInstallmentAmount());
		if(penaltyVO!=null && (penaltyVO.getNumOverdueDays() == null || penaltyVO.getNumOverdueDays() <= 0)){
			//TODO: if contract no Overdue day, so no need to calulate other amount.
			return null;
		}
		if (penaltyVO.getPenaltyAmount() != null) {
			penaltyAmount += penaltyVO.getPenaltyAmount().getTiAmount() ;
			vatPenalty+=penaltyVO.getPenaltyAmount().getVatAmount();
		}

		for (InstallmentVO installmentVO : installmentByNumInstallments) {
			if (installmentVO.getCashflowType() == ECashflowType.CAP) {
				tiPrincipal = installmentVO.getTiamount() + installmentVO.getVatAmount();
				vatTotalInstallment+=installmentVO.getVatAmount();
			} else if (installmentVO.getCashflowType() == ECashflowType.IAP) {
				tiInterestRate = installmentVO.getTiamount() + installmentVO.getVatAmount();
				vatTotalInstallment+=installmentVO.getVatAmount();
			} else if (installmentVO.getService() != null && EServiceType.INSFEE.getCode().equals(installmentVO.getService().getCode())) {
				tiInsurance = installmentVO.getTiamount() + installmentVO.getVatAmount();
				vatTotalInstallment+=installmentVO.getVatAmount();
			} else if (installmentVO.getService() != null && (EServiceType.SRVFEE.getCode().equals(installmentVO.getService().getCode())
					|| EServiceType.SRVFEEEXT.getCode().equals(installmentVO.getService().getCode()))) {
				tiServiceFee = installmentVO.getTiamount() + installmentVO.getVatAmount();
				vatTotalInstallment+=installmentVO.getVatAmount();
			} else if(installmentVO.getService() != null && EServiceType.TRANSFEE.getCode().equals(installmentVO.getService().getCode())) {
				tiTransFee = installmentVO.getTiamount() + installmentVO.getVatAmount();
				vatTotalInstallment+=installmentVO.getVatAmount();
			} else {
				otherAmount += installmentVO.getTiamount() + installmentVO.getVatAmount();
				vatTotalInstallment+=installmentVO.getVatAmount();
			}
		}
		totalInstallment = tiPrincipal + tiInterestRate + tiInsurance + tiServiceFee + otherAmount+penaltyAmount+vatPenalty-vatTotalInstallment;
		installmentAmount = tiPrincipal + tiInterestRate;
		remainingBalance = installmentVOFirstIndex.getBalance();
		return new MultiAmountTypeVO(tiPrincipal,
									tiInterestRate,
									tiInsurance,
									tiServiceFee,
									otherAmount,
									totalInstallment,
									installmentAmount,
									remainingBalance,
									tiTransFee,
								    penaltyAmount,
									penaltyVO,
				                    vatTotalInstallment,
									vatPenalty);
	}

	/***
	 * This method is the same to calculate interest in PayOff, PayOffVO getInstallmentPayOffVos.
	 * @param contract
	 * @param interestTiAmount
	 * @param paymentDate
	 * @param installmentDate
	 * @param i
	 * @return tiInterestRate
	 */
	private Amount calulateInterestPaidOff(Contract contract,Amount tiInterestRate, Double interestTiAmount,Date paymentDate, int i,Date installmentDate){
		if(contract.getPenaltyTermPayoff() >= i){
			tiInterestRate.plusTiAmount(interestTiAmount);
			tiInterestRate.plusTeAmount(interestTiAmount);
		} else {
			long nbDiff = DateUtils.getDiffInDays(installmentDate,DateUtils.getDateAtEndOfDay(paymentDate));
			long nbDaysOfMonth = DateUtils.getNbDaysInMonth(DateUtils.today());

			if(paymentDate.after(installmentDate)){
				tiInterestRate.plusTiAmount(interestTiAmount);
				tiInterestRate.plusTeAmount(interestTiAmount);
			} else if(nbDiff <= 30){
				tiInterestRate.plusTiAmount(MyMathUtils.roundAmountTo((nbDaysOfMonth-nbDiff) * interestTiAmount / nbDaysOfMonth));
				tiInterestRate.plusTeAmount(MyMathUtils.roundAmountTo((nbDaysOfMonth-nbDiff) * interestTiAmount / nbDaysOfMonth));
			}
		}
		return tiInterestRate;
	}

	@Override
	public Amount getUnearnInterestOfReprocess(Contract contract, Date paymentDate) {
		int currentNumInstallment = 1;
		if (contract.getLastPaidNumInstallment() != null) {
			currentNumInstallment = contract.getLastPaidNumInstallment() + 1;
		}
		Amount tiInterestRate = new Amount(0d, 0d, 0d);
		int termsOfcontract = contract.getTerm();

		for (int i = currentNumInstallment; i <= termsOfcontract; i++){
			List<InstallmentVO> installmentByNumInstallments = getInstallmentVOs(contract, i);
			for (InstallmentVO installmentVO : installmentByNumInstallments) {
				if(installmentVO.getCashflowType().equals(ECashflowType.IAP)) {
					tiInterestRate=calulateInterestPaidOff(contract,tiInterestRate,installmentVO.getTiamount(),paymentDate,i,installmentVO.getInstallmentDate());

				}
			}
		}

		return tiInterestRate;
	}
}
