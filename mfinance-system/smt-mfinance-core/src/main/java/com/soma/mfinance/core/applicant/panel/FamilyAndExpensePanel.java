package com.soma.mfinance.core.applicant.panel;


import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.mfinance.core.panel.applicant.FamilyAndExpense;
import com.soma.mfinance.core.shared.system.AmountBigUtils;
import com.soma.mfinance.core.shared.system.AutoTextField;
import com.soma.ersys.core.hr.model.eref.ERelationship;
import com.soma.frmk.security.context.SecApplicationContextHolder;
import com.soma.frmk.vaadin.ui.panel.AbstractControlPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.service.EntityService;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author by th.seng  on 10/20/2017.
 */
public class FamilyAndExpensePanel extends AbstractControlPanel {

	private static final long serialVersionUID = 8341162309347917428L;

	private EntityService entityService = (EntityService) SecApplicationContextHolder
			.getContext().getBean("entityService");
	private FamilyAndExpense familyAndExpense;
	private ERefDataComboBox<ERelationship> cbxRelationship1;
	private ERefDataComboBox<ERelationship> cbxRelationship2;
	private ERefDataComboBox<ERelationship> cbxRelationship3;
	private ERefDataComboBox<ERelationship> cbxRelationship4;
	private ERefDataComboBox<ERelationship> cbxRelationship5;
	private ERefDataComboBox<ERelationship> cbxRelationship6;

	private TextField txtRelationshipName1;
	private TextField txtRelationshipName2;
	private TextField txtRelationshipName3;
	private TextField txtRelationshipName4;
	private TextField txtRelationshipName5;
	private TextField txtRelationshipName6;

	private TextField txtPosition1;
	private TextField txtPosition2;
	private TextField txtPosition3;
	private TextField txtPosition4;
	private TextField txtPosition5;
	private TextField txtPosition6;

	private AutoTextField txtValueTotalDisposalIncome;
	private TextField txtNumberOfadultIncludingApplicant;
	private Label lblNumberOfadultIncludingApplicant;

	private TextField txtLengthOfWorkingYear1;
	private TextField txtLengthOfWorkingMonth1;
	private AutoTextField txtAnnualInCome1;
	private TextField txtContactNumber1;
	private TextField txtLengthOfWorkingYear2;
	private TextField txtLengthOfWorkingMonth2;
	private AutoTextField txtAnnualInCome2;
	private TextField txtContactNumber2;
	private TextField txtLengthOfWorkingYear3;
	private TextField txtLengthOfWorkingMonth3;
	private AutoTextField txtAnnualInCome3;
	private TextField txtContactNumber3;
	private TextField txtLengthOfWorkingYear4;
	private TextField txtLengthOfWorkingMonth4;
	private AutoTextField txtAnnualInCome4;
	private TextField txtContactNumber4;
	private TextField txtLengthOfWorkingYear5;
	private TextField txtLengthOfWorkingMonth5;
	private AutoTextField txtAnnualInCome5;
	private TextField txtContactNumber5;
	private TextField txtLengthOfWorkingYear6;
	private TextField txtLengthOfWorkingMonth6;
	private AutoTextField txtAnnualInCome6;
	private TextField txtContactNumber6;

	private TextField txtNumberOfChild;
	private AutoTextField txtAnnualPersonalExpense;
	private AutoTextField txtAnnualFamilyExpense;

	private CustomLayout customLayout;

	private ERefDataComboBox<ERelationship> generateRelationshipCombobox(){
		ERefDataComboBox<ERelationship> cbxRelationship = new ERefDataComboBox<ERelationship>(ERelationship.values());
		cbxRelationship.setImmediate(true);
		cbxRelationship.setWidth("100px");
		return cbxRelationship;
	}

	public FamilyAndExpensePanel() {
		setMargin(true);
		setSpacing(true);
		setSizeFull();

		String template = "familyAndExpense";
		InputStream layoutFile = getClass().getResourceAsStream(
				"/VAADIN/themes/mfinance/layouts/" + template + ".html");
		customLayout = null;
		try {
			customLayout = new CustomLayout(layoutFile);
		} catch (IOException e) {
			Notification.show("Could not locate template " + template,
					e.getMessage(), Type.ERROR_MESSAGE);
		}

		cbxRelationship1 = generateRelationshipCombobox();

		cbxRelationship2 = generateRelationshipCombobox();

		cbxRelationship3 = generateRelationshipCombobox();

		cbxRelationship4 = generateRelationshipCombobox();

		cbxRelationship5 = generateRelationshipCombobox();

		cbxRelationship6 = generateRelationshipCombobox();

		txtRelationshipName1 = ComponentFactory.getTextField(false, 60, 130);
		txtRelationshipName1.setImmediate(true);
		txtRelationshipName1.setVisible(true);

		txtRelationshipName2 = ComponentFactory.getTextField(false, 60, 130);
		txtRelationshipName2.setImmediate(true);
		txtRelationshipName2.setVisible(true);

		txtRelationshipName3 = ComponentFactory.getTextField(false, 60, 130);
		txtRelationshipName3.setImmediate(true);

		txtRelationshipName4 = ComponentFactory.getTextField(false, 60, 130);
		txtRelationshipName4.setImmediate(true);

		txtRelationshipName5 = ComponentFactory.getTextField(false, 60, 130);
		txtRelationshipName5.setImmediate(true);

		txtRelationshipName6 = ComponentFactory.getTextField(false, 60, 130);
		txtRelationshipName6.setImmediate(true);

		txtPosition1 = ComponentFactory.getTextField(false, 60, 130);
		txtPosition1.setImmediate(true);
		txtPosition2 = ComponentFactory.getTextField(false, 60, 130);
		txtPosition2.setImmediate(true);
		txtPosition3 = ComponentFactory.getTextField(false, 60, 130);
		txtPosition3.setImmediate(true);
		txtPosition4 = ComponentFactory.getTextField(false, 60, 130);
		txtPosition4.setImmediate(true);
		txtPosition5 = ComponentFactory.getTextField(false, 60, 130);
		txtPosition5.setImmediate(true);
		txtPosition6 = ComponentFactory.getTextField(false, 60, 130);
		txtPosition6.setImmediate(true);
		txtValueTotalDisposalIncome = new AutoTextField();
		txtValueTotalDisposalIncome.setWidth("150");
		txtValueTotalDisposalIncome.setEnabled(true);
		txtValueTotalDisposalIncome.addStyleName("blackdisabled");

		txtLengthOfWorkingYear1 = ComponentFactory.getTextField(false, 60, 60);
		txtLengthOfWorkingMonth1 = ComponentFactory.getTextField(false, 60, 60);
		txtAnnualInCome1 = new AutoTextField();
		txtAnnualInCome1.setWidth("60");
		txtContactNumber1 = ComponentFactory.getTextField(false, 60, 130);

		txtLengthOfWorkingYear2 = ComponentFactory.getTextField(false, 60, 60);
		txtLengthOfWorkingMonth2 = ComponentFactory.getTextField(false, 60, 60);
		txtAnnualInCome2 = new AutoTextField();
		txtAnnualInCome2.setWidth("60");
		txtContactNumber2 = ComponentFactory.getTextField(false, 60, 130);

		txtLengthOfWorkingYear3 = ComponentFactory.getTextField(false, 60, 60);
		txtLengthOfWorkingMonth3 = ComponentFactory.getTextField(false, 60, 60);
		txtAnnualInCome3 = new AutoTextField();
		txtAnnualInCome3.setWidth("60");
		txtContactNumber3 = ComponentFactory.getTextField(false, 60, 130);

		txtLengthOfWorkingYear4 = ComponentFactory.getTextField(false, 60, 60);
		txtLengthOfWorkingMonth4 = ComponentFactory.getTextField(false, 60, 60);
		txtAnnualInCome4 = new AutoTextField();
		txtAnnualInCome4.setWidth("60");
		txtContactNumber4 = ComponentFactory.getTextField(false, 60, 130);

		txtLengthOfWorkingYear5 = ComponentFactory.getTextField(false, 60, 60);
		txtLengthOfWorkingMonth5 = ComponentFactory.getTextField(false, 60, 60);
		txtAnnualInCome5 =new AutoTextField();
		txtAnnualInCome5.setWidth("60");
		txtContactNumber5 = ComponentFactory.getTextField(false, 60, 130);

		txtLengthOfWorkingYear6 = ComponentFactory.getTextField(false, 60, 60);
		txtLengthOfWorkingMonth6 = ComponentFactory.getTextField(false, 60, 60);
		txtAnnualInCome6 = new AutoTextField();
		txtAnnualInCome6.setWidth("60");
		txtContactNumber6 = ComponentFactory.getTextField(false, 60, 130);

		txtNumberOfChild = ComponentFactory.getTextField(false, 60, 130);
		txtAnnualPersonalExpense = new AutoTextField();
		txtAnnualPersonalExpense.setWidth("130");
		txtAnnualFamilyExpense = new AutoTextField();
		txtAnnualFamilyExpense.setWidth("130");
		cbxRelationship1.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 7512967103053215319L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cbxRelationship1.getSelectedEntity() != null) {
					resetRelationship1TextField(false,true);
				} else {
					resetRelationship1TextField(true,false);

				}
			}
		});
		cbxRelationship2.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = -8619691223601430987L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cbxRelationship2.getSelectedEntity() != null) {
					resetRelationship2TextField(false,true);
				} else {
					resetRelationship2TextField(true,false);
				}
			}
		});
		cbxRelationship3.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = -835979938959804918L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cbxRelationship3.getSelectedEntity() != null) {
					resetRelationship3TextField(false,true);
				} else {
					resetRelationship3TextField(true,false);
				}
			}
		});
		cbxRelationship4.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = -3032265432401927343L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cbxRelationship4.getSelectedEntity() != null) {
					resetRelationship4TextField(false,true);
				} else {
					resetRelationship4TextField(true,false);
				}
			}
		});
		cbxRelationship5.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 1965937041300117450L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cbxRelationship5.getSelectedEntity() != null) {
					resetRelationship5TextField(false,true);
				} else {
					resetRelationship5TextField(true,false);
				}
			}
		});
		cbxRelationship6.addValueChangeListener(new ValueChangeListener() {

			private static final long serialVersionUID = -4498231792552510737L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cbxRelationship6.getSelectedEntity() != null) {
					resetRelationship6TextField(false,true);
				} else {
					resetRelationship6TextField(true,false);
				}
			}
		});
		txtPosition1.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 8661159614441874825L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				txtValueTotalDisposalIncome
						.setValue(getDefaultString(getTotalHouseholdIncomes()));
			}
		});
		txtPosition2.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 428615636610389575L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				txtValueTotalDisposalIncome
						.setValue(getDefaultString(getTotalHouseholdIncomes()));
			}
		});
		txtPosition3.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = -907888604517533909L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				txtValueTotalDisposalIncome
						.setValue(getDefaultString(getTotalHouseholdIncomes()));
			}
		});
		txtPosition4.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = -2254785913189287191L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				txtValueTotalDisposalIncome
						.setValue(getDefaultString(getTotalHouseholdIncomes()));
			}
		});
		txtPosition5.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = -2550148392205462294L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				txtValueTotalDisposalIncome
						.setValue(getDefaultString(getTotalHouseholdIncomes()));
			}
		});
		txtPosition6.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = -1833319634063384540L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				txtValueTotalDisposalIncome
						.setValue(getDefaultString(getTotalHouseholdIncomes()));
			}
		});

		txtAnnualInCome1.addValueChangeListener(new ValueChangeListener() {

			private static final long serialVersionUID = 4935236815188568444L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				Double totalIncomeOfFamilyMember = getTotalIncomeOfMemberFamily();
				txtValueTotalDisposalIncome.setValue(AmountBigUtils.format(totalIncomeOfFamilyMember));
			}
		});
		txtAnnualInCome2.addValueChangeListener(new ValueChangeListener() {

			private static final long serialVersionUID = 4935236815188568444L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				Double totalIncomeOfFamilyMember = getTotalIncomeOfMemberFamily();
				txtValueTotalDisposalIncome.setValue(AmountBigUtils.format(totalIncomeOfFamilyMember));
			}
		});
		txtAnnualInCome3.addValueChangeListener(new ValueChangeListener() {

			private static final long serialVersionUID = 4935236815188568444L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				Double totalIncomeOfFamilyMember = getTotalIncomeOfMemberFamily();
				txtValueTotalDisposalIncome.setValue(AmountBigUtils.format(totalIncomeOfFamilyMember));
			}
		});
		txtAnnualInCome4.addValueChangeListener(new ValueChangeListener() {

			private static final long serialVersionUID = 4935236815188568444L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				Double totalIncomeOfFamilyMember = getTotalIncomeOfMemberFamily();
				txtValueTotalDisposalIncome.setValue(AmountBigUtils.format(totalIncomeOfFamilyMember));
			}
		});
		txtAnnualInCome5.addValueChangeListener(new ValueChangeListener() {

			private static final long serialVersionUID = 4935236815188568444L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				Double totalIncomeOfFamilyMember = getTotalIncomeOfMemberFamily();
				txtValueTotalDisposalIncome.setValue(AmountBigUtils.format(totalIncomeOfFamilyMember));
			}
		});
		txtAnnualInCome6.addValueChangeListener(new ValueChangeListener() {

			private static final long serialVersionUID = 4935236815188568444L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				Double totalIncomeOfFamilyMember = getTotalIncomeOfMemberFamily();
				txtValueTotalDisposalIncome.setValue(AmountBigUtils.format(totalIncomeOfFamilyMember));
			}
		});

		txtNumberOfadultIncludingApplicant = ComponentFactory.getTextField(false, 50,100);
		lblNumberOfadultIncludingApplicant = new Label(I18N.message("total.family.member"));
		Label lblFamilyInfo = new Label(
				I18N.message("family.member.information"));
		lblFamilyInfo.setStyleName("text-bold");
		customLayout.addComponent(lblFamilyInfo, "lblFamilyMemberInformation");
		customLayout.addComponent(
				new Label(I18N.message("disposal.income.member")),
				"lblDisposalIncome");
		customLayout.addComponent(new Label(I18N.message("relationship")),
				"lblRelationshipTitle");
		customLayout.addComponent(new Label(I18N.message("name.en")), "lblName");
		customLayout.addComponent(
				new Label(I18N.message("position.occupation")), "lblPosition");


		customLayout.addComponent(new Label(I18N.message("length.of.work")),
				"lblLengOfWork");
		customLayout.addComponent(new Label(I18N.message("annual.income")),
				"lblAnnualInCome");
		customLayout.addComponent(new Label(I18N.message("contract.number")),
				"lblContractNumber");
		getCustomerLayoutForm(customLayout, cbxRelationship1, txtRelationshipName1, txtPosition1,
				txtLengthOfWorkingYear1, txtLengthOfWorkingMonth1,
				txtAnnualInCome1, txtContactNumber1, 1);
		getCustomerLayoutForm(customLayout, cbxRelationship2, txtRelationshipName2, txtPosition2,
				txtLengthOfWorkingYear2, txtLengthOfWorkingMonth2,
				txtAnnualInCome2, txtContactNumber2, 2);
		getCustomerLayoutForm(customLayout, cbxRelationship3,txtRelationshipName3, txtPosition3,
				txtLengthOfWorkingYear3, txtLengthOfWorkingMonth3,
				txtAnnualInCome3, txtContactNumber3, 3);
		getCustomerLayoutForm(customLayout, cbxRelationship4,txtRelationshipName4, txtPosition4,
				txtLengthOfWorkingYear4, txtLengthOfWorkingMonth4,
				txtAnnualInCome4, txtContactNumber4, 4);
		getCustomerLayoutForm(customLayout, cbxRelationship5,txtRelationshipName5, txtPosition5,
				txtLengthOfWorkingYear5, txtLengthOfWorkingMonth5,
				txtAnnualInCome5, txtContactNumber5, 5);
		getCustomerLayoutForm(customLayout, cbxRelationship6,txtRelationshipName6, txtPosition6,
				txtLengthOfWorkingYear6, txtLengthOfWorkingMonth6,
				txtAnnualInCome6, txtContactNumber6, 6);
		customLayout.addComponent(
				new Label(I18N.message("total.accumulate.Household.income")),
				"lblTotalDisposalIncome");
		customLayout.addComponent(txtValueTotalDisposalIncome,
				"txtValueTotalDisposalIncome");
		customLayout.addComponent(
				lblNumberOfadultIncludingApplicant,
				"lblTotalFamilyMember");
		customLayout.addComponent(txtNumberOfadultIncludingApplicant,
				"txtTotalNumberFamilyMember");
		customLayout.addComponent(new Label(I18N.message("per.year")),
				"lblYear");
		customLayout.addComponent(
				new Label(I18N.message("number.of.children")),
				"lblNumberOfChild");
		customLayout.addComponent(txtNumberOfChild, "txtNumberOfChild");
		Label lblExpense = new Label(I18N.message("expense"));
		lblExpense.setStyleName("text-bold");
		customLayout.addComponent(lblExpense, "lblExpense");

		customLayout.addComponent(
				new Label(I18N.message("annual.perosnal.expense")),
				"lblAnnualPersonalExpense");
		customLayout.addComponent(txtAnnualPersonalExpense,
				"txtAnnualPersonalExpense");
		customLayout.addComponent(
				new Label(I18N.message("annual.family.expense")),
				"lblAnnualFamilyExpense");
		customLayout.addComponent(txtAnnualFamilyExpense,
				"txtAnnualFamilyExpense");

		addComponent(customLayout);
	}

	private void getCustomerLayoutForm(CustomLayout customLayout,
									   ERefDataComboBox<ERelationship> cbxRelationship, TextField txtRelationshipName,
									   TextField txtPosition, TextField txtLengthOfWorkingYear,
									   TextField txtLengthOfWorkingMonth, TextField txtAnnualInCome,
									   TextField txtContactNumber, int rowNumber) {
		customLayout.addComponent(cbxRelationship, "cbxRelationship"
				+ rowNumber + "");
		customLayout.addComponent(txtRelationshipName, "txtRelationshipName"
				+ rowNumber + "");
		customLayout.addComponent(txtPosition, "txtPosition" + rowNumber + "");
		customLayout.addComponent(txtLengthOfWorkingYear, "txtLenghOfWorkYear"
				+ rowNumber + "");
		customLayout.addComponent(new Label(I18N.message("years")),
				"lblLenghOfWorkYear" + rowNumber + "");
		customLayout.addComponent(txtLengthOfWorkingMonth,
				"txtLenghOfWorkMonth" + rowNumber + "");
		customLayout.addComponent(new Label(I18N.message("months")),
				"lblLenghOfWorkMonth" + rowNumber + "");
		customLayout.addComponent(txtAnnualInCome, "txtAnnualInCome"
				+ rowNumber + "");
		customLayout.addComponent(new Label(I18N.message("per.year")),
				"lblAnnualInCome" + rowNumber + "");
		customLayout.addComponent(txtContactNumber, "txtContactNumber"
				+ rowNumber + "");
	}

	private Double getTotalIncomeOfMemberFamily() {
		Double totalIncomeOfFamilyMember = getDoubleValue(AmountBigUtils.getValueDouble(txtAnnualInCome1))
				+ getDoubleValue(AmountBigUtils.getValueDouble(txtAnnualInCome2))
				+ getDoubleValue(AmountBigUtils.getValueDouble(txtAnnualInCome3))
				+ getDoubleValue(AmountBigUtils.getValueDouble(txtAnnualInCome4))
				+ getDoubleValue(AmountBigUtils.getValueDouble(txtAnnualInCome5))
				+ getDoubleValue(AmountBigUtils.getValueDouble(txtAnnualInCome6));
		return totalIncomeOfFamilyMember;
	}

	/**
	 * Get total accumulate for household income
	 *
	 * @return
	 */
	private double getTotalHouseholdIncomes() {
		double monthlyIncomeMember1 = getDouble(txtPosition1, 0.0);
		double monthlyIncomeMember2 = getDouble(txtPosition2, 0.0);
		double monthlyIncomeMember3 = getDouble(txtPosition3, 0.0);
		double monthlyIncomeMember4 = getDouble(txtPosition4, 0.0);
		double monthlyIncomeMember5 = getDouble(txtPosition5, 0.0);
		double monthlyIncomeMember6 = getDouble(txtPosition6, 0.0);
		return monthlyIncomeMember1 + monthlyIncomeMember2
				+ monthlyIncomeMember3 + monthlyIncomeMember4
				+ monthlyIncomeMember5 + monthlyIncomeMember6;
	}

	/**
	 * @return
	 */
	public Individual getFamilyAndExpensePanel(Individual individual) {
		this.familyAndExpense = individual.getFamilyAndExpense();
		FamilyAndExpense familyAndExpense = getFamilyAndExpense();
		familyAndExpense.setIndividual(individual);
		entityService.saveOrUpdate(familyAndExpense);
		individual.setFamilyAndExpense(familyAndExpense);
		return individual;
	}

	/**
	 *
	 * @return familyAndExpense
	 */
	private FamilyAndExpense getFamilyAndExpense() {
		if(familyAndExpense == null){
			familyAndExpense = new FamilyAndExpense();
		}
		familyAndExpense.setRelationship1(cbxRelationship1.getSelectedEntity());
		familyAndExpense.setRelationship2(cbxRelationship2.getSelectedEntity());
		familyAndExpense.setRelationship3(cbxRelationship3.getSelectedEntity());
		familyAndExpense.setRelationship4(cbxRelationship4.getSelectedEntity());
		familyAndExpense.setRelationship5(cbxRelationship5.getSelectedEntity());
		familyAndExpense.setRelationship6(cbxRelationship6.getSelectedEntity());

		familyAndExpense.setRelationshipName1(txtRelationshipName1.getValue());
		familyAndExpense.setRelationshipName2(txtRelationshipName2.getValue());
		familyAndExpense.setRelationshipName3(txtRelationshipName3.getValue());
		familyAndExpense.setRelationshipName4(txtRelationshipName4.getValue());
		familyAndExpense.setRelationshipName5(txtRelationshipName5.getValue());
		familyAndExpense.setRelationshipName6(txtRelationshipName6.getValue());

		familyAndExpense.setPosition1(getStringValue(txtPosition1.getValue()));
		familyAndExpense.setPosition2(getStringValue(txtPosition2.getValue()));
		familyAndExpense.setPosition3(getStringValue(txtPosition3.getValue()));
		familyAndExpense.setPosition4(getStringValue(txtPosition4.getValue()));
		familyAndExpense.setPosition5(getStringValue(txtPosition5.getValue()));
		familyAndExpense.setPosition6(getStringValue(txtPosition6.getValue()));
		familyAndExpense
				.setLengthWorkInYear1(getIntValue(txtLengthOfWorkingYear1
						.getValue()));
		familyAndExpense
				.setLengthWorkInYear2(getIntValue(txtLengthOfWorkingYear2
						.getValue()));
		familyAndExpense
				.setLengthWorkInYear3(getIntValue(txtLengthOfWorkingYear3
						.getValue()));
		familyAndExpense
				.setLengthWorkInYear4(getIntValue(txtLengthOfWorkingYear4
						.getValue()));
		familyAndExpense
				.setLengthWorkInYear5(getIntValue(txtLengthOfWorkingYear5
						.getValue()));
		familyAndExpense
				.setLengthWorkInYear6(getIntValue(txtLengthOfWorkingYear6
						.getValue()));
		familyAndExpense
				.setLengthWorkInMonth1(getIntValue(txtLengthOfWorkingMonth1
						.getValue()));
		familyAndExpense
				.setLengthWorkInMonth2(getIntValue(txtLengthOfWorkingMonth2
						.getValue()));
		familyAndExpense
				.setLengthWorkInMonth3(getIntValue(txtLengthOfWorkingMonth3
						.getValue()));
		familyAndExpense
				.setLengthWorkInMonth4(getIntValue(txtLengthOfWorkingMonth4
						.getValue()));
		familyAndExpense
				.setLengthWorkInMonth5(getIntValue(txtLengthOfWorkingMonth5
						.getValue()));
		familyAndExpense
				.setLengthWorkInMonth6(getIntValue(txtLengthOfWorkingMonth6
						.getValue()));
		familyAndExpense
				.setAnnualIncome1(getDoubleValue(AmountBigUtils.getValueDouble(txtAnnualInCome1)));
		familyAndExpense
				.setAnnualIncome2(getDoubleValue(AmountBigUtils.getValueDouble(txtAnnualInCome2)));
		familyAndExpense
				.setAnnualIncome3(getDoubleValue(AmountBigUtils.getValueDouble(txtAnnualInCome3)));
		familyAndExpense
				.setAnnualIncome4(getDoubleValue(AmountBigUtils.getValueDouble(txtAnnualInCome4)));
		familyAndExpense
				.setAnnualIncome5(getDoubleValue(AmountBigUtils.getValueDouble(txtAnnualInCome5)));
		familyAndExpense
				.setAnnualIncome6(getDoubleValue(AmountBigUtils.getValueDouble(txtAnnualInCome6)));
		familyAndExpense.setContactNumber1(getStringValue(txtContactNumber1
				.getValue()));
		familyAndExpense.setContactNumber2(getStringValue(txtContactNumber2
				.getValue()));
		familyAndExpense.setContactNumber3(getStringValue(txtContactNumber3
				.getValue()));
		familyAndExpense.setContactNumber4(getStringValue(txtContactNumber4
				.getValue()));
		familyAndExpense.setContactNumber5(getStringValue(txtContactNumber5
				.getValue()));
		familyAndExpense.setContactNumber6(getStringValue(txtContactNumber6
				.getValue()));

		familyAndExpense
				.setNumberOfadultIncludingApplicant(getIntValue(txtNumberOfadultIncludingApplicant
						.getValue()));
		familyAndExpense.setNumberOfChild(getIntValue(txtNumberOfChild
				.getValue()));
		if(AmountBigUtils.getValueDouble(txtAnnualFamilyExpense) != null){
			familyAndExpense
					.setAnnualFamilyExpense(AmountBigUtils.getValueDouble(txtAnnualFamilyExpense));
		}
		if (AmountBigUtils.getValueDouble(txtAnnualPersonalExpense) != null) {
			familyAndExpense
					.setAnnualPersonalExpense(AmountBigUtils.getValueDouble(txtAnnualPersonalExpense));
		}

		entityService.saveOrUpdate(familyAndExpense);

		return familyAndExpense;
	}

	/**
	 * @param individual
	 */
	public void assignValues(Individual individual) {
		reset();
		if (individual.getFamilyAndExpense() != null) {
			this.familyAndExpense = individual.getFamilyAndExpense();
			setValueToFamilyAndExpenseForm(familyAndExpense);
		} else {
			familyAndExpense = new FamilyAndExpense();
		}
	}

	/**
	 * Reset panel
	 */
	public void reset() {
		resetRelationship1TextField(true,true);
		resetRelationship2TextField(true,true);
		resetRelationship3TextField(true,true);
		resetRelationship4TextField(true,true);
		resetRelationship5TextField(true,true);
		resetRelationship6TextField(true,true);
		txtAnnualFamilyExpense.setValue("");
		txtAnnualPersonalExpense.setValue("");
		txtNumberOfadultIncludingApplicant.setValue("");
		txtNumberOfChild.setValue("");
		txtValueTotalDisposalIncome.setValue("");
		cbxRelationship1.setSelectedEntity(null);
		cbxRelationship2.setSelectedEntity(null);
		cbxRelationship3.setSelectedEntity(null);
		cbxRelationship4.setSelectedEntity(null);
		cbxRelationship5.setSelectedEntity(null);
		cbxRelationship6.setSelectedEntity(null);
		txtRelationshipName1.setValue("");
		txtRelationshipName2.setValue("");
		txtRelationshipName3.setValue("");
		txtRelationshipName4.setValue("");
		txtRelationshipName5.setValue("");
		txtRelationshipName6.setValue("");
	}

	/**
	 * @return
	 */
	public List<String> validate() {
		super.reset();
		checkMandatoryField(txtAnnualFamilyExpense, "annual.family.expense");
		checkMandatoryField(txtAnnualPersonalExpense, "annual.perosnal.expense");
		checkMandatoryField(txtNumberOfadultIncludingApplicant, "total.family.member");
		return errors;
	}

	/**
	 * @return
	 */
	public List<String> fullValidate() {
		super.reset();
		checkMandatoryField(txtNumberOfadultIncludingApplicant, "total.family.member");
		checkMandatoryField(txtAnnualPersonalExpense, "annual.perosnal.expense");
		checkMandatoryField(txtAnnualFamilyExpense, "annual.family.expense");
		return errors;
	}

	private Integer getIntValue(String string) {
		if (string == null || "".equals(string)) {
			return 0;
		} else {
			return Integer.parseInt(string);
		}
	}

	private Double getDoubleValue(Double value) {
		if (value == null) {
			return 0d;
		} else {
			return value;
		}
	}

	private void resetRelationship1TextField(Boolean isReset, Boolean isEnabled) {
		if (isReset) {
			txtPosition1.setValue("");
			txtLengthOfWorkingYear1.setValue("");
			txtLengthOfWorkingMonth1.setValue("");
			txtAnnualInCome1.setValue("");
			txtContactNumber1.setValue("");
		}

		txtPosition1.setEnabled(isEnabled);
		txtLengthOfWorkingYear1.setEnabled(isEnabled);
		txtLengthOfWorkingMonth1.setEnabled(isEnabled);
		txtAnnualInCome1.setEnabled(isEnabled);
		txtContactNumber1.setEnabled(isEnabled);
	}

	private void resetRelationship2TextField(Boolean isReset, Boolean isEnabled) {
		if (isReset) {
			txtPosition2.setValue("");
			txtLengthOfWorkingYear2.setValue("");
			txtLengthOfWorkingMonth2.setValue("");
			txtAnnualInCome2.setValue("");
			txtContactNumber2.setValue("");
		}

		txtPosition2.setEnabled(isEnabled);
		txtLengthOfWorkingYear2.setEnabled(isEnabled);
		txtLengthOfWorkingMonth2.setEnabled(isEnabled);
		txtAnnualInCome2.setEnabled(isEnabled);
		txtContactNumber2.setEnabled(isEnabled);
	}

	private void resetRelationship3TextField(Boolean isReset, Boolean isEnabled) {
		if (isReset) {
			txtPosition3.setValue("");
			txtLengthOfWorkingYear3.setValue("");
			txtLengthOfWorkingMonth3.setValue("");
			txtAnnualInCome3.setValue("");
			txtContactNumber3.setValue("");
		}

		txtPosition3.setEnabled(isEnabled);
		txtLengthOfWorkingYear3.setEnabled(isEnabled);
		txtLengthOfWorkingMonth3.setEnabled(isEnabled);
		txtAnnualInCome3.setEnabled(isEnabled);
		txtContactNumber3.setEnabled(isEnabled);
	}

	private void resetRelationship4TextField(Boolean isReset, Boolean isEnabled) {
		if (isReset) {
			txtPosition4.setValue("");
			txtLengthOfWorkingYear4.setValue("");
			txtLengthOfWorkingMonth4.setValue("");
			txtAnnualInCome4.setValue("");
			txtContactNumber4.setValue("");
		}
		txtPosition4.setEnabled(isEnabled);
		txtLengthOfWorkingYear4.setEnabled(isEnabled);
		txtLengthOfWorkingMonth4.setEnabled(isEnabled);
		txtAnnualInCome4.setEnabled(isEnabled);
		txtContactNumber4.setEnabled(isEnabled);
	}

	private void resetRelationship5TextField(Boolean isReset, Boolean isEnabled) {
		if (isReset) {
			txtPosition5.setValue("");
			txtLengthOfWorkingYear5.setValue("");
			txtLengthOfWorkingMonth5.setValue("");
			txtAnnualInCome5.setValue("");
			txtContactNumber5.setValue("");
		}

		txtPosition5.setEnabled(isEnabled);
		txtLengthOfWorkingYear5.setEnabled(isEnabled);
		txtLengthOfWorkingMonth5.setEnabled(isEnabled);
		txtAnnualInCome5.setEnabled(isEnabled);
		txtContactNumber5.setEnabled(isEnabled);
	}

	private void resetRelationship6TextField(Boolean isReset, Boolean isEnabled) {
		if (isReset) {
			txtPosition6.setValue("");
			txtLengthOfWorkingYear6.setValue("");
			txtLengthOfWorkingMonth6.setValue("");
			txtAnnualInCome6.setValue("");
			txtContactNumber6.setValue("");
		}
		txtPosition6.setEnabled(isEnabled);
		txtLengthOfWorkingYear6.setEnabled(isEnabled);
		txtLengthOfWorkingMonth6.setEnabled(isEnabled);
		txtAnnualInCome6.setEnabled(isEnabled);
		txtContactNumber6.setEnabled(isEnabled);
	}

	private void setValueToFamilyAndExpenseForm(FamilyAndExpense familyAndExpense) {
		cbxRelationship1.setSelectedEntity(familyAndExpense.getRelationship1());
		cbxRelationship2.setSelectedEntity(familyAndExpense.getRelationship2());
		cbxRelationship3.setSelectedEntity(familyAndExpense.getRelationship3());
		cbxRelationship4.setSelectedEntity(familyAndExpense.getRelationship4());
		cbxRelationship5.setSelectedEntity(familyAndExpense.getRelationship5());
		cbxRelationship6.setSelectedEntity(familyAndExpense.getRelationship6());

		txtRelationshipName1.setValue(familyAndExpense.getRelationshipName1() != null ? familyAndExpense.getRelationshipName1() : "");
		txtRelationshipName2.setValue(familyAndExpense.getRelationshipName2() != null ? familyAndExpense.getRelationshipName2() : "");
		txtRelationshipName3.setValue(familyAndExpense.getRelationshipName3() != null ? familyAndExpense.getRelationshipName3() : "");
		txtRelationshipName4.setValue(familyAndExpense.getRelationshipName4() != null ? familyAndExpense.getRelationshipName4() : "");
		txtRelationshipName5.setValue(familyAndExpense.getRelationshipName5() != null ? familyAndExpense.getRelationshipName5() : "");
		txtRelationshipName6.setValue(familyAndExpense.getRelationshipName6() != null ? familyAndExpense.getRelationshipName6() : "");

		txtPosition1.setValue(getStringValue(familyAndExpense.getPosition1()));
		txtLengthOfWorkingYear1.setValue(getDefaultString(familyAndExpense.getLengthWorkInYear1()));
		txtLengthOfWorkingMonth1.setValue(getDefaultString(familyAndExpense.getLengthWorkInMonth1()));
		txtAnnualInCome1.setValue(getDefaultString(AmountBigUtils.format(familyAndExpense.getAnnualIncome1())));
		txtContactNumber1.setValue(familyAndExpense.getContactNumber1());

		txtPosition2.setValue(getStringValue(familyAndExpense.getPosition2()));
		txtLengthOfWorkingYear2.setValue(getDefaultString(familyAndExpense.getLengthWorkInYear2()));
		txtLengthOfWorkingMonth2.setValue(getDefaultString(familyAndExpense.getLengthWorkInMonth2()));
		txtAnnualInCome2.setValue(getDefaultString(AmountBigUtils.format(familyAndExpense.getAnnualIncome2())));
		txtContactNumber2.setValue(familyAndExpense.getContactNumber2());

		txtPosition3.setValue(getStringValue(familyAndExpense.getPosition3()));
		txtLengthOfWorkingYear3.setValue(getDefaultString(familyAndExpense.getLengthWorkInYear3()));
		txtLengthOfWorkingMonth3.setValue(getDefaultString(familyAndExpense.getLengthWorkInMonth3()));
		txtAnnualInCome3.setValue(getDefaultString(AmountBigUtils.format(familyAndExpense.getAnnualIncome3())));
		txtContactNumber3.setValue(familyAndExpense.getContactNumber3());

		txtPosition4.setValue(getStringValue(familyAndExpense.getPosition4()));
		txtLengthOfWorkingYear4.setValue(getDefaultString(familyAndExpense.getLengthWorkInYear4()));
		txtLengthOfWorkingMonth4.setValue(getDefaultString(familyAndExpense.getLengthWorkInMonth4()));
		txtAnnualInCome4.setValue(getDefaultString(AmountBigUtils.format(familyAndExpense.getAnnualIncome4())));
		txtContactNumber4.setValue(familyAndExpense.getContactNumber4());

		txtPosition5.setValue(getStringValue(familyAndExpense.getPosition5()));
		txtLengthOfWorkingYear5.setValue(getDefaultString(familyAndExpense.getLengthWorkInYear5()));
		txtLengthOfWorkingMonth5.setValue(getDefaultString(familyAndExpense.getLengthWorkInMonth5()));
		txtAnnualInCome5.setValue(getDefaultString(AmountBigUtils.format(familyAndExpense.getAnnualIncome5())));
		txtContactNumber5.setValue(familyAndExpense.getContactNumber5());

		txtPosition6.setValue(getStringValue(familyAndExpense.getPosition6()));
		txtLengthOfWorkingYear6.setValue(getDefaultString(familyAndExpense.getLengthWorkInYear6()));
		txtLengthOfWorkingMonth6.setValue(getDefaultString(familyAndExpense.getLengthWorkInMonth6()));
		txtAnnualInCome6.setValue(getDefaultString(AmountBigUtils.format(familyAndExpense.getAnnualIncome6())));
		txtContactNumber6.setValue(familyAndExpense.getContactNumber6());
		Double totalIncome = familyAndExpense.getAnnualIncome1()
				+ familyAndExpense.getAnnualIncome2()
				+ familyAndExpense.getAnnualIncome3()
				+ familyAndExpense.getAnnualIncome4()
				+ familyAndExpense.getAnnualIncome5()
				+ familyAndExpense.getAnnualIncome6();
		txtValueTotalDisposalIncome.setValue(getDefaultString(AmountBigUtils.format(totalIncome)));
		txtNumberOfChild.setValue(getDefaultString(familyAndExpense.getNumberOfChild()));
		txtNumberOfadultIncludingApplicant.setValue(getDefaultString(familyAndExpense.getNumberOfadultIncludingApplicant()));
		txtAnnualFamilyExpense.setValue(getDefaultString(AmountBigUtils.format(familyAndExpense.getAnnualFamilyExpense())));
		txtAnnualPersonalExpense.setValue(getDefaultString(AmountBigUtils.format(familyAndExpense.getAnnualPersonalExpense())));

	}

	/**
	 *
	 * @param value
	 * @return
	 */
	private String getStringValue(String value) {
		String strValue = "";
		if (value != null) {
			strValue = value;
		}
		return strValue;
	}
	public Label getLblNumberOfadultIncludingApplicant(){
		return this.lblNumberOfadultIncludingApplicant;
	}
	public void setLblNumberOfadultIncludingApplicant(Label lblNumberOfadultIncludingApplicant){
		this.lblNumberOfadultIncludingApplicant = lblNumberOfadultIncludingApplicant;
	}

}
