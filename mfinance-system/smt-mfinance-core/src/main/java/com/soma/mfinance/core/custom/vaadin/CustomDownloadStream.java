package com.soma.mfinance.core.custom.vaadin;

import com.vaadin.server.Constants;
import com.vaadin.server.DownloadStream;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinResponse;
import com.vaadin.util.FileTypeResolver;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;

/**
 * Created by kaokimchheng on 5/9/17.
 */
public class CustomDownloadStream extends DownloadStream {
    /**
     * Creates a new instance of DownloadStream.
     *
     * @param stream
     * @param contentType
     * @param fileName
     */
    private int length;

    public CustomDownloadStream(InputStream stream, String contentType, String fileName, int length) {
        super(stream, contentType, fileName);
        this.length = length;
    }

    @Override
    public void writeResponse(VaadinRequest request, VaadinResponse response) throws IOException {
        if (getParameter("Location") != null) {
            response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", getParameter("Location"));
            return;
        }

        // Download from given stream
        final InputStream data = getStream();
        if (data == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        if (data != null) {

            OutputStream out = null;

            try {
                response.setHeader("Content-Disposition", "inline; filename=\"" + getFileName() + "\"");
                response.setHeader("Content-Type", FileTypeResolver.getMIMEType(getFileName()));
                response.setHeader("Content-Length", String.valueOf(length));
                response.setHeader("X-Frame-Options", "SAMEORIGIN");
                response.setHeader("Cache-Control",String.valueOf(getCacheTime())+",no-store,max-age=21600");

                int bufferSize = getBufferSize();
                if (bufferSize <= 0 || bufferSize > Constants.MAX_BUFFER_SIZE) {
                    bufferSize = Constants.DEFAULT_BUFFER_SIZE;
                }
                final byte[] buffer = new byte[bufferSize];
                int bytesRead = 0;

                out = response.getOutputStream();

                long totalWritten = 0;
                while ((bytesRead = data.read(buffer)) > 0) {
                    out.write(buffer, 0, bytesRead);

                    totalWritten += bytesRead;
                    if (totalWritten >= buffer.length) {
                        // Avoid chunked encoding for small resources
                        out.flush();
                    }
                }
            } finally {
                if (out != null)
                    out.close();
                if (data != null)
                    data.close();
            }
        }
    }
}
