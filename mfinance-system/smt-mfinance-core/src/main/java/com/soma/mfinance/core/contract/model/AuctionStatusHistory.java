package com.soma.mfinance.core.contract.model;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.frmk.security.model.SecUser;
import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;

/**
 * 
 * @author kimsuor.seang
 *
 */
@Entity
@Table(name = "td_auction_history")
public class AuctionStatusHistory extends EntityA {

    private static final long serialVersionUID = -4053267507390718585L;
    
    private Contract contract;
    private EWkfStatus auctionStatus;
	private EWkfStatus previousAuctionStatus;
	private SecUser user;
    
    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "histo_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "con_id")
	public Contract getContract() {
		return contract;
	}
	public void setContract(Contract contract) {
		this.contract = contract;
	}

	@Column(name = "auct_hist_auct_status", nullable = true)
	@Convert(converter = EWkfStatus.class)
	public EWkfStatus getAuctionStatus() {
		return auctionStatus;
	}
	public void setAuctionStatus(EWkfStatus auctionStatus) {
		this.auctionStatus = auctionStatus;
	}

	@Column(name = "auct_hist_auct_status_pre", nullable = true)
	@Convert(converter = EWkfStatus.class)
	public EWkfStatus getPreviousAuctionStatus() {
		return previousAuctionStatus;
	}
	public void setPreviousAuctionStatus(EWkfStatus previousAuctionStatus) {
		this.previousAuctionStatus = previousAuctionStatus;
	}

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_usr_id")
	public SecUser getUser() {
		return user;
	}
	public void setUser(SecUser user) {
		this.user = user;
	}
	
}
