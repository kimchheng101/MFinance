package com.soma.mfinance.core.collection.model;

import org.seuksa.frmk.model.eref.BaseERefData;

import javax.persistence.AttributeConverter;
import java.util.List;

/**
 * 
 * @author th.seng
 *
 */
public class EStockStatus extends BaseERefData implements AttributeConverter<EStockStatus, Long> {

	private static final long serialVersionUID = 8800717903072047607L;

	public EStockStatus() {
	}

	public EStockStatus(String code, long id) {
		super(code, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public EStockStatus convertToEntityAttribute(Long id) {
		return super.convertToEntityAttribute(id);
	}
	
	@Override
	public Long convertToDatabaseColumn(EStockStatus arg0) {
		return super.convertToDatabaseColumn(arg0);
	}

	public static List<EStockStatus> values() {
		return getValues(EStockStatus.class);
	}
	
	public static EStockStatus getByCode(String code) {
		return getByCode(EStockStatus.class, code);
	}
	
	public static EStockStatus getById(long id) {
		return getById(EStockStatus.class, id);
	}
}
