package com.soma.mfinance.core.asset.model;

import com.soma.ersys.core.hr.model.eref.EColor;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * AUTHOR : kimsuor.seang
 * EMAIL  : chhaichivon1995@gmail.com
 * DATE   : 8/29/2017
 * TIME   : 11:22 AM
 */
/*
@Data
@Entity
@Table(name = "td_bo_asset")
public class AssetBO extends AbstractAsset {
    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "asset_bo_id", unique = true, nullable = false)
    public Long getId() {
        return null;
    }
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "color_id")
    private EColor color;
    @Column(name = "asset_nu_year", nullable = true)
    private Integer year;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "engin_id")
    private EEngine engine;
    @Column(name = "asgen_code", nullable = true, length = 5)
    @Enumerated(EnumType.STRING)
    private EAssetGender assetGender;
    @Column(name = "asset_am_ti_cash_price", nullable = true)
    private Double tiCashPrice;
    @Column(name = "asset_am_te_cash_price", nullable = true)
    private Double teCashPrice;
    @Column(name = "asset_am_vat_cash_price", nullable = true)
    private Double vatCashPrice;
    @Column(name = "asmod_id_fk", nullable = true)
    private Long asmodIdFk;
    @Column(name = "asset_dt_registration", nullable = true)
    private Date registrationDate;
    @Column(name = "asset_va_plate_number", nullable = true, length = 30)
    private String plateNumber;
    @Column(name = "asset_va_chassis_number", nullable = true, length = 50)
    private String chassisNumber;
    @Column(name = "asset_va_engine_number", nullable = true, length = 50)
    private String engineNumber;
    @Transient
    private boolean hasChanged = false;
}
*/
