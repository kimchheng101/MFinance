package com.soma.mfinance.core.financial.model;

/**
 * Meta data of com.soma.mfinance.core.financial.model.CampaignDealer
 * @author kimsuor.seang
 */
public interface MCampaignDealer {
	
	public final static String DEALER = "dealer";
	public final static String CAMPAIGN = "campaign";
	
}
