package com.soma.mfinance.core.asset.service.impl;

import com.soma.mfinance.core.asset.model.AssetMake;
import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.asset.service.AssetRangeRestriction;
import com.soma.mfinance.core.asset.service.AssetRangeSequenceImpl;
import com.soma.mfinance.core.asset.service.AssetRangeService;
import com.soma.mfinance.core.asset.service.AssetSequenceManager;
import com.soma.mfinance.core.financial.model.ProductLine;
import com.soma.mfinance.core.quotation.SequenceGenerator;
import com.soma.mfinance.core.asset.model.AssetMake;
import com.soma.mfinance.core.asset.model.AssetRange;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.soma.mfinance.core.shared.FMEntityField.ID;

/**
 * Asset range service
 *
 * @author kimsuor.seang
 */
@Service("assetRangeService")
public class AssetRangeServiceImpl extends BaseEntityServiceImpl implements AssetRangeService {

    /** */
    private static final long serialVersionUID = 3839730827037552185L;

    @Autowired
    private EntityDao dao;

    /**
     * @see org.seuksa.frmk.service.impl.BaseEntityServiceImpl#getDao()
     */
    @Override
    public BaseEntityDao getDao() {
        return dao;
    }

    /**
     * @see com.soma.mfinance.core.asset.service.AssetRangeService#saveOrUpdateAssetRange(AssetRange)
     */
    @Override
    public void saveOrUpdateAssetRange(AssetRange assetRange) {
        if (assetRange.getCode().length() != 6) {
            String assMakeCode = StringUtils.EMPTY;
            if (assetRange.getAssetMake() != null) {
                assMakeCode = assetRange.getAssetMake().getCode();
            }
            Integer sequence = AssetSequenceManager.getInstance().getSequenceAssetRange(assMakeCode);
            SequenceGenerator sequenceGenerator = new AssetRangeSequenceImpl(assMakeCode, sequence);
            assetRange.setCode(sequenceGenerator.generate());
        }
        saveOrUpdate(assetRange);
    }

    /**
     * @see com.soma.mfinance.core.asset.service.AssetRangeService#getAssetRangesByAssetMake(AssetMake)
     */
    @Override
    public List<AssetRange> getAssetRangesByAssetMake(AssetMake assetMake) {
        AssetRangeRestriction restrictions = new AssetRangeRestriction();
        restrictions.setAssetMake(assetMake);
        return list(restrictions);
    }

    @Override
    public BaseRestrictions getBaseRestrictionsAssetRangesByProductLine(ProductLine productLine) {
        BaseRestrictions<AssetRange> restrictions = new BaseRestrictions<>(AssetRange.class);
        restrictions.addCriterion(Restrictions.eq("assetMake" + "." + ID, productLine.getId()));
        return restrictions;
    }

}
