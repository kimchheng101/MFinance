package com.soma.mfinance.core.asset.model.appraisal;

import org.seuksa.frmk.model.EntityFactory;
import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;

/**
 * @author vi.sok
 */
@Entity
@Table(name = "tu_appraisal_category")
public class AppraisalCategory extends EntityRefA {

    private static final long serialVersionUID = -8954187823136477015L;

    private EAppraisalType eAppraisalType;
    private EAppraisalRange appraisalRange;

    /**
     * @return
     */
    public static AppraisalCategory createInstance() {
        AppraisalCategory instance = EntityFactory.createInstance(AppraisalCategory.class);
        return instance;
    }

    /**
     * Get masset type's is.
     *
     * @return The masset type's is.
     */
    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "app_cate_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * @see org.seuksa.frmk.model.entity.EntityRefA#getCode()
     */
    @Override
    @Column(name = "app_cate_code", nullable = true, length = 10, unique = true)
    public String getCode() {
        return super.getCode();
    }

    /**
     * Get the masset make's description in locale language.
     *
     * @return <String>
     */
    @Override
    @Column(name = "app_cate_desc", nullable = true, length = 255)
    public String getDesc() {
        return super.getDesc();
    }

    /**
     * Get the masset make's name in English.
     *
     * @return <String>
     */
    @Override
    @Column(name = "app_cate_desc_en", nullable = false, length = 255)
    public String getDescEn() {
        return super.getDescEn();
    }

    /**
     * @return the eAppraisalType
     */
    @Column(name = "app_cate_typ_id", nullable = true)
    @Convert(converter = EAppraisalType.class)
    public EAppraisalType geteAppraisalType() {
        return eAppraisalType;
    }

    /**
     * @param eAppraisalType the eAppraisalType to set
     */
    public void seteAppraisalType(EAppraisalType eAppraisalType) {
        this.eAppraisalType = eAppraisalType;
    }

    @Column(name = "app_range_id")
    @Convert(converter = EAppraisalRange.class)
    public EAppraisalRange getAppraisalRange() {
        return appraisalRange;
    }

    public void setAppraisalRange(EAppraisalRange appraisalRange) {
        this.appraisalRange = appraisalRange;
    }
}
