package com.soma.mfinance.core.contract.service.impl;

import java.util.List;

import com.soma.mfinance.core.contract.service.ContractNoteRestriction;
import com.soma.mfinance.core.contract.service.ContractRequestRestriction;
import com.soma.mfinance.core.contract.service.ContractSmsRestriction;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soma.mfinance.core.contract.model.Appointment;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.ContractNote;
import com.soma.mfinance.core.contract.model.ContractRequest;
import com.soma.mfinance.core.contract.model.ContractSms;
import com.soma.mfinance.core.contract.service.NoteService;
import com.soma.mfinance.core.history.FinHistoryType;
import com.soma.mfinance.core.history.service.FinHistoryService;
import com.soma.ersys.core.hr.model.organization.Employee;
import com.soma.ersys.core.hr.model.organization.OrgStructure;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.util.i18n.I18N;

/**
 * Note Service Implementation
 * @author kimsuor.seang
 */
@Service("noteService")
public class NoteServiceImpl extends BaseEntityServiceImpl implements NoteService {

	/** */
	private static final long serialVersionUID = -5241077153142416137L;
	
	protected Logger LOG = LoggerFactory.getLogger(NoteServiceImpl.class);
		
	@Autowired
	private EntityDao dao;
	
	@Autowired
	private FinHistoryService finHistoryService;

	/**
	 * @see org.seuksa.frmk.service.impl.BaseEntityServiceImpl#getDao()
	 */
	@Override
	public BaseEntityDao getDao() {
		return dao;
	}

	/**
	 * @see NoteService#getLatestNotes(Contract)
	 */
	@Override
	public List<ContractNote> getLatestNotes(Contract contract) {
		ContractNoteRestriction restrictions = new ContractNoteRestriction();
		restrictions.setConId(contract.getId());
		restrictions.setMaxResults(3);
		restrictions.setOrder(Order.desc(ContractNote.CREATEDATE));
		return list(restrictions);
	}

	/**
	 * @see NoteService#getNotesByContract(Contract)
	 */
	@Override
	public List<ContractNote> getNotesByContract(Contract contract) {
		ContractNoteRestriction restrictions = new ContractNoteRestriction();
		restrictions.setConId(contract.getId());
		restrictions.setOrder(Order.desc(ContractNote.CREATEDATE));
		return list(restrictions);
	}

	/**
	 * @see NoteService#getSmsByContract(Contract)
	 */
	@Override
	public List<ContractSms> getSmsByContract(Contract contract) {
		ContractSmsRestriction restrictions = new ContractSmsRestriction();
		restrictions.setConId(contract.getId());
		restrictions.setOrder(Order.desc(ContractSms.CREATEDATE));
		return list(restrictions);
	}

	/**
	 * @see NoteService#getRequestsByContract(Contract)
	 */
	@Override
	public List<ContractRequest> getRequestsByContract(Contract contract) {
		ContractRequestRestriction restrictions = new ContractRequestRestriction();
		restrictions.setConId(contract.getId());
		restrictions.setOrder(Order.desc(ContractRequest.CREATEDATE));
		return list(restrictions);
	}

	/**
	 * @see NoteService#getAppointmentByContract(Contract)
	 */
	@Override
	public List<Appointment> getAppointmentByContract(Contract contract) {
		BaseRestrictions<Appointment> restrictions = new BaseRestrictions<>(Appointment.class);
		restrictions.addCriterion(Restrictions.eq("contract", contract));
		restrictions.addOrder(Order.asc("startDate"));
		List<Appointment> appointments = list(restrictions);
		return appointments;
	}
	
	/**
	 * @see NoteService#saveOrUpdateContractRequest(ContractRequest)
	 */
	@Override
	public void saveOrUpdateContractRequest(ContractRequest request) {
		saveOrUpdate(request);
		String requestType = request.getRequestType() != null ? request.getRequestType().getDescLocale() : StringUtils.EMPTY;
		String desc = requestType + StringUtils.SPACE + request.getComment();
		finHistoryService.addFinHistory(request.getContract(), FinHistoryType.FIN_HIS_REQ, desc);
	}
	
	/**
	 * @see NoteService#saveOrUpdateSMS(ContractSms)
	 */
	@Override
	public void saveOrUpdateSMS(ContractSms contractSms) {
		saveOrUpdate(contractSms);
		String desc = I18N.message("sms.to") + StringUtils.SPACE + contractSms.getSendTo() + StringUtils.SPACE + contractSms.getPhoneNumber();
		finHistoryService.addFinHistory(contractSms.getContract(), FinHistoryType.FIN_HIS_SMS, desc);
	}

	@Override
	public String getUserDepartment(String login) {
		SecUser secUser = getByField(SecUser.class, "login", login);
		if (secUser != null) {
			Employee employee = getByField(Employee.class, "secUser", secUser);
			if (employee != null) {
				OrgStructure branch = employee.getBranch();
				return branch != null ? branch.getNameEn() : "";
			}
		}
		return "";
	}
	
}
