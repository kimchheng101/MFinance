package com.soma.mfinance.glf.report.model;

import org.seuksa.frmk.model.eref.BaseERefData;

import javax.persistence.AttributeConverter;
import java.util.List;

/**
 * Created by Dang Dim
 * Date     : 26-Jul-17, 3:15 PM
 * Email    : d.dim@gl-f.com
 */
public class EReportTamplate extends BaseERefData implements AttributeConverter<EReportTamplate, Long> {

    public static final EReportTamplate SPA = new EReportTamplate("SPA", 1l);
    public static final EReportTamplate ROR = new EReportTamplate("ROR", 2l);

    public EReportTamplate() {

    }

    public EReportTamplate(String code, long id) {
        super(code, id);
    }

    @Override
    public Long convertToDatabaseColumn(EReportTamplate attribute) {
        return super.convertToDatabaseColumn(attribute);
    }

    @Override
    public EReportTamplate convertToEntityAttribute(Long dbData) {
        return super.convertToEntityAttribute(dbData);
    }

    /**
     * @return
     */
    public static List<EReportTamplate> values() {
        return getValues(EReportTamplate.class);
    }

    /**
     * @param code
     * @return
     */
    public static EReportTamplate getByCode(String code) {
        return getByCode(EReportTamplate.class, code);
    }

    /**
     * @param id
     * @return
     */
    public static EReportTamplate getById(long id) {
        return getById(EReportTamplate.class, id);
    }
}
