package com.soma.mfinance.core.shared.exception;

/**
 * Business exception
 * @author kimsuor.seang
 */
public class BusinessException extends Exception {

	private static final long serialVersionUID = -4766708106666455914L;

	public BusinessException() {
		super();
	}
	
	public BusinessException(String message) {
		super(message);
	}
	
	public BusinessException(String message, Throwable e) {
		super(message, e);
	}
}
