package com.soma.mfinance.core.custom.component;

import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.model.entity.RefDataId;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ki.kao on 3/10/2017.
 */
public class CustomEntityRefComboBox<T extends RefDataId> extends EntityRefComboBox<T> {

    public CustomEntityRefComboBox() {
        super(((String) null));
    }

    public CustomEntityRefComboBox(String caption) {
        super(caption);
    }

    public CustomEntityRefComboBox(List values) {
        super(values);
    }

    public CustomEntityRefComboBox(String caption, List values) {
        super(caption, values);
    }

    public CustomEntityRefComboBox(String caption, boolean onlyActive) {
        super(caption, onlyActive);
    }

    public void setFilteringComboBox(EntityRefComboBox<T> filterComboBox, Class<T> filterClass) {
        if (filterComboBox != null) {
            this.addValueChangeListener(valueChangeEvent -> {
                if (getSelectedEntity() != null) {
                    String filterById = getField(filterClass, this.restrictions.getEntityClass().getSimpleName()) + ".id";
                    BaseRestrictions<T> restrictions = new BaseRestrictions<>(filterClass);
                    restrictions.addCriterion(Restrictions.eq(filterById, getSelectedEntity().getId()));
                    filterComboBox.setRestrictions(restrictions);
                    filterComboBox.renderer();
                } else
                    filterComboBox.clear();
            });
        }
    }

    private String getField(Class<T> filterClass, String classField) {
        List<Field> privateFields = new ArrayList<>();
        Field[] allFields = filterClass.getDeclaredFields();
        for (Field field : allFields) {
            if (field.getName().equalsIgnoreCase(classField))
                return field.getName();
        }
        return null;
    }

}
