package com.soma.mfinance.core.shared.system;

import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;

/**
 * @author by kimsuor.seang  on 10/14/2017.
 */
@Entity
@Table(name = "tu_whose_job")
public class WhoseJob extends EntityRefA {

    private static final long serialVersionUID = -1762740736615702442L;


    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "whojob_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Override
    @Transient
    public String getCode() {
        return desc;
    }

    @Column(name = "whojob_desc", nullable = false, length=50)
    @Override
    public String getDesc() {
        return super.getDesc();
    }

    @Override
    @Column(name = "whojob_desc_en", nullable = false, length=50)
    public String getDescEn() {
        return super.getDescEn();
    }
}
