package com.soma.mfinance.core.shared.system;

/**
 * Created by Kimsuor SEANG
 * Date  : 5/10/2017
 * Name  : 1:55 PM
 * Email : k.seang@gl-f.com
 */
public class WorkFlowUtils {

    public final static String EWKFFLOW_CODE_APPLICATION = "Applicant";
    public final static String EWKFFLOW_CODE_CONCTRACT = "Contract";
    public final static String EWKFFLOW_CODE_QUOTATION = "Quotation";

}
