package com.soma.mfinance.core.contract.service;

import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.contract.model.Letter;

/**
 * 
 * @author kimsuor.seang
 */
public interface LetterService extends BaseEntityService {

	/**
	 * saveOrUpdate letter
	 * @param letter
	 */
	void saveOrUpdateLetter(Letter letter);
	
}
