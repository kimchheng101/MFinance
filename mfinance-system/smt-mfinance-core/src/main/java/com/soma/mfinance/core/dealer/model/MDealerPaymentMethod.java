package com.soma.mfinance.core.dealer.model;

import org.seuksa.frmk.model.entity.MEntityRefA;

/**
 * Meta data of DealerPaymentMethod
 * @author kimsuor.seang
 */
public interface MDealerPaymentMethod extends MEntityRefA {
	
	public final static String DEALER = "dealer";
	public final static String PAYMENTMETHOD = "paymentMethod";
	public final static String TYPE = "type";
	public final static String DEALERACCOUNTHOLDER = "dealerAccountHolder";
	public final static String DEALERBANKACCOUNT = "dealerBankAccount";
	
}
