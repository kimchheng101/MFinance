package com.soma.mfinance.third.creditbureau.cbc.model;
/**
 * 
 * @author tha.bunsath
 *
 */
public class CBCAddressCode {
		private String provinceCode;
		private String districtCode;
		private String communeCode;
		private String villageCode;
		public String getProvinceCode() {
			return provinceCode;
		}
		public void setProvinceCode(String provinceCode) {
			this.provinceCode = provinceCode;
		}
		public String getDistrictCode() {
			return districtCode;
		}
		public void setDistrictCode(String districtCode) {
			this.districtCode = districtCode;
		}
		public String getCommuneCode() {
			return communeCode;
		}
		public void setCommuneCode(String communeCode) {
			this.communeCode = communeCode;
		}
		public String getVillageCode() {
			return villageCode;
		}
		public void setVillageCode(String villageCode) {
			this.villageCode = villageCode;
		}
}
