package com.soma.mfinance.glf.report.model;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.quotation.model.Quotation;
import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;

/**
 * Created by Dang Dim
 * Date     : 28-Jul-17, 8:51 AM
 * Email    : d.dim@gl-f.com
 */
@Entity
@Table(name = "ts_customer_state")
public class CustomerState extends EntityA {

    private Quotation quotation;
    private Long state;
    private EWkfStatus wkfStatus;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "state_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @ManyToOne
    @JoinColumn(name = "quo_id")
    public Quotation getQuotation() {
        return quotation;
    }

    public void setQuotation(Quotation quotation) {
        this.quotation = quotation;
    }

    @Column(name = "cus_state")
    public Long getState() {
        return state;
    }

    public void setState(Long state) {
        this.state = state;
    }

    @Column(name = "wkf_id")
    @Convert(converter = EWkfStatus.class)
    public EWkfStatus getWkfStatus() {
        return wkfStatus;
    }

    public void setWkfStatus(EWkfStatus wkfStatus) {
        this.wkfStatus = wkfStatus;
    }
}
