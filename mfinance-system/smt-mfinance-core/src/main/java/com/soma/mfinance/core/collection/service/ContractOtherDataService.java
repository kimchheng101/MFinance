package com.soma.mfinance.core.collection.service;

import com.soma.mfinance.core.contract.model.Contract;
import org.seuksa.frmk.service.BaseEntityService;
import org.seuksa.frmk.tools.amount.Amount;

import java.util.Map;

/**
 * Contract Other data service interface
 * @author kimsuor.seang
 */
public interface ContractOtherDataService extends BaseEntityService {

	/**
	 * @return
	 */
	void calculateOtherDataContracts();
	
	/**
	 * @param contract
	 */
	Contract calculateOtherDataContract(Contract contract);

	//Amount getTotalAmountInOverdueUsd(Contract contract, Date calDate, List<InstallmentVO> installmentVOs, Date installmentDate, int gracePeriod);

	//Amount getTotalPenaltyAmountInOverdueUsd(Contract contract, Date calDate, Date installmentDate,int gracePeriod);

	/***
	 * Map<totalAmount,penaltyAmount>
	 * @param contract
	 * @return
	 */
	public Map<Amount,Amount> getTotalAmountAndPenaltyNotPaid(Contract contract);

}
