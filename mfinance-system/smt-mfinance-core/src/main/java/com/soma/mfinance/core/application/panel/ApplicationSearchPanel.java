package com.soma.mfinance.core.application.panel;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.common.security.model.SecUserDetail;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.financial.model.FinProduct;
import com.soma.mfinance.core.financial.model.Term;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.shared.system.FMProfile;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.mfinance.core.widget.SecUserComboBox;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.ersys.core.hr.model.address.Province;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.statusrecord.StatusRecordField;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.common.security.model.SecUserDetail;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.service.EntityService;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ApplicationSearchPanel extends AbstractSearchPanel<Quotation> implements FMEntityField {

    /**
     * @Author: sr.soth
     * @Date : 19-05-17
     */
    private static final long serialVersionUID = 1L;
   /* private TextField txtName;
    private TextField txtColor;*/

    private TextField txtReference;
    private TextField txtFirstNameEn;
    private TextField txtLastNameEn;
    private TextField txtChassisNumber;
    private TextField txtEngineNumber;

    private EntityRefComboBox<Province> cbxProvince;
    private EntityRefComboBox<FinProduct> cbxFinancialProduct;
    private EntityRefComboBox<Term> cbxTermInYears;
    private ERefDataComboBox<EDealerType> cbxDealerType;
    private SecUserComboBox cbxUnderWriterOfficer;
    private SecUserComboBox cbxUnderWriterSupervisor;
    private DealerComboBox cbxDealer;
    private StatusRecordField statusRecordField;
    private SecUserDetail usrDetail;

    private List<ERefDataComboBox<EWkfStatus>> cbxQuotationStatus;

    public ApplicationSearchPanel(ApplicationTablePanel applicationTablePanel) {
        super(I18N.message("search"), applicationTablePanel);
    }

    @Override
    protected Component createForm() {
        final GridLayout gridLayout = new GridLayout(10, 1);

        EntityService entityService = SpringUtils.getBean(EntityService.class);
        SecUser secUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        txtReference = ComponentFactory.getTextField(I18N.message("reference"), false, 60, 220);
        txtFirstNameEn = ComponentFactory.getTextField(I18N.message("firstname"), false, 60, 220);
        txtLastNameEn = ComponentFactory.getTextField(I18N.message("lastname"), false, 60, 220);
        txtChassisNumber = ComponentFactory.getTextField(I18N.message("chassis.number"), false, 50, 220);
        txtEngineNumber = ComponentFactory.getTextField(I18N.message("engine.number"), false, 50, 220);

        //ComboBox
        cbxProvince = new EntityRefComboBox<>(I18N.message("province"));
        cbxProvince.setRestrictions(new BaseRestrictions<>(Province.class));
        cbxProvince.renderer();
        cbxProvince.setImmediate(true);
        cbxProvince.setWidth("220px");

        cbxFinancialProduct = new EntityRefComboBox<>(I18N.message("financial.product"));
        BaseRestrictions<FinProduct> restrictionsFinancial = new BaseRestrictions<>(FinProduct.class);
        restrictionsFinancial.getStatusRecordList().add(EStatusRecord.ACTIV);
        cbxFinancialProduct.setWidth("220px");
        cbxFinancialProduct.setRestrictions(restrictionsFinancial);
        cbxFinancialProduct.setImmediate(true);
        cbxFinancialProduct.renderer();
        cbxFinancialProduct.setSelectedEntity(null);

        cbxUnderWriterOfficer = new SecUserComboBox(DataReference.getInstance().getUsers(FMProfile.UW, EStatusRecord.ACTIV));
        cbxUnderWriterOfficer.setCaption(I18N.message("uw.officer"));
        cbxUnderWriterOfficer.setWidth("150px");
        cbxUnderWriterOfficer.setImmediate(true);

        cbxUnderWriterSupervisor = new SecUserComboBox(DataReference.getInstance().getUsers(FMProfile.US, EStatusRecord.ACTIV));
        cbxUnderWriterSupervisor.setCaption(I18N.message("uw.supervisor"));
        cbxUnderWriterSupervisor.setWidth("150px");
        cbxUnderWriterSupervisor.setImmediate(true);


        cbxTermInYears = new EntityRefComboBox<>(I18N.message("term"));
        cbxTermInYears.setWidth("220px");
        cbxTermInYears.setRestrictions(new BaseRestrictions<>(Term.class));
        cbxTermInYears.setImmediate(true);
        cbxTermInYears.renderer();
        cbxTermInYears.setSelectedEntity(null);

        cbxQuotationStatus = new ArrayList<>();
        for(int i = 0; i < 4; i++){
            ERefDataComboBox<EWkfStatus> comboBox = new ERefDataComboBox<>(I18N.message("quotation.status"), QuotationWkfStatus.getComboBoxQuotationStatus());
            comboBox.setImmediate(true);
            comboBox.setWidth("220px");
            cbxQuotationStatus.add(comboBox);
        }

        cbxDealerType = new ERefDataComboBox<>(I18N.message("dealer.type"), EDealerType.values());
        cbxDealerType.setImmediate(true);
        cbxDealerType.setWidth("220px");

        entityService = SpringUtils.getBean(EntityService.class);
        usrDetail = entityService.getByField(SecUserDetail.class, "secUser.id", secUser.getId());
        BaseRestrictions<Dealer> restrictions = new BaseRestrictions<Dealer>(Dealer.class);
        restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
        cbxDealer = new DealerComboBox(I18N.message("dealer"), entityService.list(restrictions));
        cbxDealer.setWidth("220px");
        cbxDealer.setImmediate(true);
        if (ProfileUtil.isPOS()) {
            cbxDealer.setEnabled(false);
            cbxDealer.setStyleName("blackdisabled");
        }
        if (ProfileUtil.isPOS() && usrDetail != null && usrDetail.getDealer() != null) {
            cbxDealer.setSelectedEntity(usrDetail.getDealer());
        }

        statusRecordField = new StatusRecordField();

        //Left
        FormLayout formLayoutLeft = new FormLayout();
        formLayoutLeft.addComponent(txtLastNameEn);
        formLayoutLeft.addComponent(txtChassisNumber);
        formLayoutLeft.addComponent(txtEngineNumber);
        formLayoutLeft.addComponent(cbxProvince);
        formLayoutLeft.addComponent(cbxFinancialProduct);

        //Middle
        FormLayout formLayoutCenter = new FormLayout();
        formLayoutCenter.addComponent(txtFirstNameEn);
        formLayoutCenter.addComponent(cbxDealerType);
        formLayoutCenter.addComponent(cbxTermInYears);

        formLayoutCenter.addComponent(cbxQuotationStatus.get(2));
        formLayoutCenter.addComponent(cbxQuotationStatus.get(3));


        //Right
        FormLayout formLayoutRight = new FormLayout();
        formLayoutRight.addComponent(txtReference);
        formLayoutRight.addComponent(cbxDealer);

        formLayoutRight.addComponent(cbxQuotationStatus.get(0));
        formLayoutRight.addComponent(cbxQuotationStatus.get(1));


        FormLayout formLayoutRightEnd = new FormLayout();
        formLayoutRightEnd.addComponent(cbxUnderWriterOfficer);
        formLayoutRightEnd.addComponent(cbxUnderWriterSupervisor);

        int col = 0;
        gridLayout.addComponent(formLayoutLeft, col++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(20, Unit.PIXELS), col++, 0);
        gridLayout.addComponent(formLayoutCenter, col++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(20, Unit.PIXELS), col++, 0);
        gridLayout.addComponent(formLayoutRight, col++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(20, Unit.PIXELS), col++, 0);
        gridLayout.addComponent(new FormLayout(statusRecordField), col++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(20, Unit.PIXELS), col++, 0);
        if (ProfileUtil.isUnderwriter(ProfileUtil.getCurrentUser()) || ProfileUtil.isUnderwriterSupervisor(ProfileUtil.getCurrentUser())) {
            gridLayout.addComponent(formLayoutRightEnd, col++, 0);
        }
        return gridLayout;
    }

    @Override
    public BaseRestrictions<Quotation> getRestrictions() {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        txtLastNameEn.setValue(txtLastNameEn.getValue().trim());
        txtFirstNameEn.setValue(txtFirstNameEn.getValue().trim());
        restrictions.addOrder(Order.desc(UPDATE_DATE));
        List<Object> list = null;
        if (ProfileUtil.isCreditOfficer() || ProfileUtil.isProductionOfficer()) {
            list = new ArrayList<Object>();
            BaseRestrictions<SecUserDetail> dealerCriteria = new BaseRestrictions<>(SecUserDetail.class);
            dealerCriteria.addCriterion(Restrictions.eq("secUser.id", ProfileUtil.getCurrentUser().getId()));
            List<SecUserDetail> userDetails = ENTITY_SRV.list(dealerCriteria);
            if (userDetails != null && !userDetails.isEmpty()) {
                dealerCriteria = new BaseRestrictions<>(SecUserDetail.class);
                //dealerCriteria.addCriterion(Restrictions.eq("dealer.id", userDetails.get(0).getDealer().getId()));
                dealerCriteria.addCriterion(Restrictions.eq("secUser.id", userDetails.get(0).getSecUser().getId()));
                userDetails = ENTITY_SRV.list(dealerCriteria);
                for (SecUserDetail detail : userDetails)
                    //list.add(detail.getSecUser().getId());
                    list.add(detail.getDealer().getId());
            }
            /*if (list.isEmpty())
                list.add(ProfileUtil.getCurrentUser().getId());
            restrictions.addCriterion(Restrictions.or(Restrictions.in("productionOfficer.id", list), Restrictions.in("creditOfficer.id", list)));*/
            restrictions.addCriterion(Restrictions.in("dealer.id", list));

            list = Arrays.asList(new Object[]{
                    QuotationWkfStatus.QUO,
                    QuotationWkfStatus.ACS,
                    QuotationWkfStatus.ACT,
                    QuotationWkfStatus.APV,
                    QuotationWkfStatus.WCA,
                    QuotationWkfStatus.AWT,
                    QuotationWkfStatus.RFC,
                    QuotationWkfStatus.DEC,
                    QuotationWkfStatus.REJ
            });

            restrictions.addCriterion(Restrictions.in("wkfStatus", list));
        } else if (ProfileUtil.isAuctionController()) {
            list = new ArrayList<>(Arrays.asList(new Object[]{
                    QuotationWkfStatus.APP
            }));
            restrictions.addCriterion(Restrictions.in("wkfStatus", list));
        } else if (ProfileUtil.isCollectionSupervisor()) {
            list = Arrays.asList(new Object[]{
                    QuotationWkfStatus.AUP,
                    QuotationWkfStatus.ACT
            });
            restrictions.addCriterion(Restrictions.in("wkfStatus", list));
        } else if (ProfileUtil.isUnderwriter()) {
            restrictions.addCriterion(Restrictions.eq("wkfStatus", QuotationWkfStatus.PRO));
        } else if (ProfileUtil.isUnderwriterSupervisor()) {
            list = Arrays.asList(new Object[]{
                    QuotationWkfStatus.APU,
                    QuotationWkfStatus.AWU,
                    QuotationWkfStatus.REU,
                    QuotationWkfStatus.PRO
            });

            restrictions.addCriterion(Restrictions.in("wkfStatus", list));
            restrictions.addCriterion(Restrictions.eq("underwriterSupervisor", ProfileUtil.getCurrentUser()));

        } else if (ProfileUtil.isManager()) {
            list = Arrays.asList(new Object[]{
                    QuotationWkfStatus.APS,
                    QuotationWkfStatus.AWS,
                    QuotationWkfStatus.AWU,
                    QuotationWkfStatus.PAC
            });
            restrictions.addCriterion(Restrictions.in("wkfStatus", list));

        } else if (ProfileUtil.isDocumentController()) {
            restrictions.addCriterion(Restrictions.eq("wkfStatus", QuotationWkfStatus.WIV));
        } else if (ProfileUtil.isAccountingController()) {
            restrictions.addCriterion(Restrictions.eq("wkfStatus", QuotationWkfStatus.ACT));
        } else if (ProfileUtil.isCallCenterLeader()) {
            restrictions.addCriterion(Restrictions.eq("wkfStatus", QuotationWkfStatus.CAN));
        } else if (ProfileUtil.isUnderwriterSupervisor2()) {
            restrictions.addCriterion(Restrictions.in("wkfStatus", QuotationWkfStatus.getComboBoxQuotationStatus()));
        }

        List<EWkfStatus> wkfStatusList = new ArrayList<>();

        cbxQuotationStatus.forEach(comboBox -> {
            if(comboBox.getSelectedEntity() != null){
                wkfStatusList.add(comboBox.getSelectedEntity());
            }
        });

        if (!wkfStatusList.isEmpty()) {
            restrictions.addCriterion(Restrictions.in("wkfStatus", wkfStatusList));
        }

        if (ProfileUtil.isUnderwriter(ProfileUtil.getCurrentUser()) || ProfileUtil.isUnderwriterSupervisor(ProfileUtil.getCurrentUser())) {
            if (cbxUnderWriterOfficer.getSelectedEntity() != null) {
                restrictions.addCriterion(Restrictions.eq("underwriter.id", cbxUnderWriterOfficer.getSelectedEntity().getId()));
            }
            if (cbxUnderWriterSupervisor.getSelectedEntity() != null) {
                restrictions.addCriterion(Restrictions.eq("underwriterSupervisor.id", cbxUnderWriterSupervisor.getSelectedEntity().getId()));
            }
        }

        if (cbxProvince.getSelectedEntity() != null || StringUtils.isNotEmpty(txtFirstNameEn.getValue())
                || StringUtils.isNotEmpty(txtLastNameEn.getValue())) {
            restrictions.addAssociation("quotationApplicants", "quoapp", JoinType.INNER_JOIN);
            restrictions.addAssociation("quoapp." + APPLICANT, "app", JoinType.INNER_JOIN);
            restrictions.addAssociation("app." + INDIVIDUAL, "ind", JoinType.INNER_JOIN);
            restrictions.addCriterion("quoapp." + APPLICANT_TYPE, EApplicantType.C);
        }

        if (cbxProvince.getSelectedEntity() != null) {
            restrictions.addAssociation("ind.individualAddresses", "appaddr", JoinType.INNER_JOIN);
            restrictions.addAssociation("appaddr.address", "addr", JoinType.INNER_JOIN);
            restrictions.addCriterion("addr.province.id", cbxProvince.getSelectedEntity().getId());
        }

        if (StringUtils.isNotEmpty(txtLastNameEn.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("ind." + LAST_NAME_EN, txtLastNameEn.getValue(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("ind." + FIRST_NAME_EN, txtFirstNameEn.getValue(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotEmpty(txtReference.getValue())) {
            restrictions.addCriterion(Restrictions.ilike(REFERENCE, txtReference.getValue(), MatchMode.ANYWHERE));
        }

        if (cbxTermInYears.getSelectedEntity() != null) {
            restrictions.addCriterion("term", cbxTermInYears.getSelectedEntity().getValue());
        }

        restrictions.addAssociation(DEALER, "dea", JoinType.INNER_JOIN);
        if (cbxDealer.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, cbxDealer.getSelectedEntity().getId()));
        }
        if (cbxDealerType.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq("dea.dealerType", cbxDealerType.getSelectedEntity()));
        }

        if (cbxFinancialProduct.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq(FINANCIAL_PRODUCT + "." + ID, cbxFinancialProduct.getSelectedEntity().getId()));
        }

        restrictions.addAssociation(ASSET, "asset", JoinType.INNER_JOIN);
        if (StringUtils.isNotEmpty(txtChassisNumber.getValue())) {
            restrictions.addCriterion(Restrictions.ilike(ASSET + ".chassisNumber", txtChassisNumber.getValue(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotEmpty(txtEngineNumber.getValue())) {
            restrictions.addCriterion(Restrictions.ilike(ASSET + ".engineNumber", txtEngineNumber.getValue(), MatchMode.ANYWHERE));
        }
        if (!this.statusRecordField.getActiveValue().booleanValue() && !this.statusRecordField.getInactiveValue().booleanValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.INACT);
            restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
        }
        if (this.statusRecordField.getActiveValue().booleanValue() && this.statusRecordField.getInactiveValue().booleanValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.INACT);
            restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
        }
        if (!this.statusRecordField.getActiveValue().booleanValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.INACT);
        }

        if (!this.statusRecordField.getInactiveValue().booleanValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
        }

        if (ProfileUtil.isUnderwriter()) {
            restrictions.addCriterion(Restrictions.eq("underwriter.id", ((SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId()));
        }
        if (ProfileUtil.isUnderwriterSupervisor()) {
            restrictions.addCriterion(Restrictions.eq("underwriterSupervisor.id", ((SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId()));
        }
        return restrictions;
    }

    @Override
    protected void reset() {
        txtFirstNameEn.setValue("");
        txtLastNameEn.setValue("");
        txtReference.setValue("");
        txtChassisNumber.setValue("");
        txtEngineNumber.setValue("");
        cbxProvince.setSelectedEntity(null);
        cbxFinancialProduct.setSelectedEntity(null);
        cbxDealerType.setSelectedEntity(null);
        cbxTermInYears.setSelectedEntity(null);
        statusRecordField.setActiveValue(true);
        statusRecordField.getInactiveValue(false);
        cbxQuotationStatus.forEach(comboBox -> comboBox.setSelectedEntity(null));
        cbxUnderWriterOfficer.setSelectedEntity(null);
        cbxUnderWriterSupervisor.setSelectedEntity(null);
    }

}
