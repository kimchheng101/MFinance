package com.soma.mfinance.core.financial.model;

/**
 * Meta data of com.soma.mfinance.core.financial.model.MinimumInterest
 * @author kimsuor.seang
 */
public interface MMinimumInterest {
	
	public final static String ASSETCATEGORY = "assetCategory";
	public final static String TERM = "term";
	public final static String MINIMUMINTERESTAMOUNT = "minimumInterestAmount";

}
