package com.soma.mfinance.core.custom.component;

import com.soma.frmk.vaadin.ui.widget.combo.EntityComboBox;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.model.entity.EntityA;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ki.kao on 3/22/2017.
 */
public class CustomEntityComboBox<T extends EntityA> extends EntityComboBox<T> {

    private Class<T> clazz;
    private String representEmpty;
    private String displayField;

    public CustomEntityComboBox(Class<T> entityClass, String displayField) {
        super(entityClass, displayField);
        this.clazz = entityClass;
        this.displayField = displayField;
    }

    public CustomEntityComboBox(Class<T> entityClass, String caption, String displayField, String emptyLabel) {
        super(entityClass, caption, displayField, emptyLabel);
        this.clazz = entityClass;
        this.displayField = displayField;
        this.representEmpty = emptyLabel;
    }


    public void setFilteringComboBox(EntityComboBox<T> filterComboBox, Class<T> filterClass) {
        if (filterComboBox != null) {
            this.addValueChangeListener(valueChangeEvent -> {
                if (getSelectedEntity() != null) {
                    String filterById = getField(filterClass, this.clazz.getSimpleName()) + ".id";
                    BaseRestrictions<T> restrictions = new BaseRestrictions<>(filterClass);
                    restrictions.addCriterion(Restrictions.eq(filterById, getSelectedEntity().getId()));
                    filterComboBox.renderer(ENTITY_SRV.list(restrictions));
                } else
                    filterComboBox.clear();
            });
        }
    }

    private String getField(Class<T> filterClass, String classField) {
        List<Field> privateFields = new ArrayList<>();
        Field[] allFields = filterClass.getDeclaredFields();
        for (Field field : allFields) {
            if (field.getName().equalsIgnoreCase(classField))
                return field.getName();
        }
        return null;
    }

    public void renderer(List<T> entities) {
        this.getValueMap().clear();
        this.removeAllItems();
        if (entities != null && !entities.isEmpty()) {
            if (this.representEmpty != null && !this.representEmpty.isEmpty()) {
                this.addItem("[empty]");
                this.setItemCaption("[empty]", this.representEmpty);
                this.setNullSelectionAllowed(false);
            }

            EntityA entity;
            for (Iterator var2 = entities.iterator(); var2.hasNext(); this.getValueMap().put(entity.getId().toString(), (T) entity)) {
                entity = (EntityA) var2.next();
                this.addItem(entity.getId().toString());

                try {
                    PropertyUtilsBean e = new PropertyUtilsBean();
                    Object value = e.getNestedProperty(entity, this.displayField);
                    this.setItemCaption(entity.getId().toString(), String.valueOf(value));
                } catch (Exception var6) {
                    throw new IllegalStateException("Error on Field [" + this.displayField + "]", var6);
                }
            }
        }
    }


}
