package com.soma.mfinance.core.workflow;

import com.soma.common.app.workflow.model.EWkfStatus;

public class ReturnWkfStatus {
	public final static EWkfStatus REPEN = EWkfStatus.getById(1400); // Pending
	public final static EWkfStatus RECLO = EWkfStatus.getById(1401); // close
}
