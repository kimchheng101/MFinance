package com.soma.mfinance.core.loanaccount;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.loanaccount.model.LoanAccount;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import org.seuksa.frmk.i18n.I18N;

import java.util.List;

/**
 * Created by cheasocheat on 5/4/17.
 */
public class LoanTabPanel extends AbstractTabPanel {
    private TextField txtAccId;
    private TextField txtPhoneNumber;
    private TextField txtAccountName;

    @Override
    protected Component createForm() {
        //ValidateNumbers loanAccount = new ValidateNumbers();
        //ArrayList validLoan = new ArrayList();

        txtAccId = ComponentFactory.getTextField(I18N.message("account.id"), true, 50, 220);
        txtPhoneNumber = ComponentFactory.getTextField(I18N.message("phone.number"), true, 50, 220);
        txtAccountName = ComponentFactory.getTextField(I18N.message("account.name"), true, 50, 220);
        //validLoan.add(txtPhoneNumber);

        FormLayout accountLayout = new FormLayout();
        accountLayout.addComponent(txtAccId);
        accountLayout.addComponent(txtAccountName);
        accountLayout.addComponent(txtPhoneNumber);

        //loanAccount.validateNumber(validLoan);
        return accountLayout;
    }

    /***
     * GetLoanAccount method
     *
     */

    public Applicant getLoanAccount(Applicant applicant) {
        if (applicant != null) {
            LoanAccount loanAccount = applicant.getLoanAccount();
            if(loanAccount == null){
                loanAccount = new LoanAccount();
            }
            loanAccount.setAccountId(this.getDefaultString(txtAccId.getValue()));
            loanAccount.setAccountName(this.getDefaultString(txtAccountName.getValue()));
            loanAccount.setPhoneNumber(this.getDefaultString(txtPhoneNumber.getValue()));
            loanAccount.setLeaseAmount(applicant.getQuotation() != null ? this.getDefaultString(applicant.getQuotation().getTiFinanceAmount()) : "");
            applicant.setLoanAccount(loanAccount);
        }
        return applicant;
    }

    /***
     * AssignValue to LoanAccount
     */

    public void assignLoanAccountValue(Applicant applicant) {
        this.reset();
        if (applicant.getLoanAccount() != null) {
            txtAccId.setValue(applicant.getLoanAccount().getAccountId());
            txtAccountName.setValue(applicant.getLoanAccount().getAccountName());
            txtPhoneNumber.setValue(applicant.getLoanAccount().getPhoneNumber());
        }
        if(txtAccountName.getValue().isEmpty())
            txtAccountName.setValue(applicant.getIndividual().getLastNameEn() + " " + applicant.getIndividual().getFirstNameEn());
        if(txtPhoneNumber.getValue().isEmpty())
            txtPhoneNumber.setValue(applicant.getIndividual().getMobilePerso());
    }

    /**
     * reset
     * */
    public void reset(){
        txtAccId.setValue("");
        txtAccountName.setValue("");
        txtPhoneNumber.setValue("");
    }

    /**
     *  Validate
     * */
    public List<String> isValid() {
        super.reset();
        checkMandatoryField(txtAccId, I18N.message("account.id"));
        checkMandatoryField(txtAccountName, I18N.message("account.name"));
        checkMandatoryField(txtPhoneNumber, I18N.message("phone.number"));
        return errors;
    }

    public String getDefaultString(String value) {
        return value == null ? "" : value.trim();
    }
}
