package com.soma.mfinance.core.collection.service;

import java.util.Date;
import java.util.List;

import com.soma.mfinance.core.collection.model.ColAssignment;
import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.collection.model.CollectionAction;
import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.common.security.model.SecUserDeptLevel;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.frmk.security.model.SecUser;

/**
 * Contract Other data service interface
 * @author kimsuor.seang
 */
public interface CollectionService extends BaseEntityService {

	List<SecUser> getCollectionUsers(String[] proCode);
	

	void assignDayEndContracts(Date processDate);
	
	void assignPhoneContracts(Date processDate);
	
	void assignFieldContracts();
	
	void assignInsideRepoContracts();
	
	void assignOAContracts();

	
	/**
	 * Get collection contracts by next action date
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<Contract> getCollectionContractsByNextActionDate(Date startDate, Date endDate);
	
	/**
	 * 
	 * @return
	 */
	List<Contract> getCollectionContractsUnProcessed();
	

	/**
	 * saveOrUpdate Latest Collection Action
	 * @param colAction
	 */
	void saveOrUpdateLatestColAction(CollectionAction colAction);
	
	/**
	 * Delete latest collection action
	 * @param action
	 */
	void deleteLatestColAction(CollectionAction action);

	/**
	 * 
	 * @param contra
	 */
	void validateAssistFlagContract(Contract contra);

	
	/**
	 * @param conId
	 * @return
	 */
	SecUser getCollectionUser(Long conId);

	/**
	 * 
	 * @param contract
	 * @return
	 */
	Collection getCollection(Contract contract);

	List<Collection> assignOverdueContracts();

	List<Collection> getActivatedCollection();

	Collection assingeContractCollection(Collection collection,List<ColAssignment> colAssignments) ;

	List<ColAssignment> getActivatedColAssignment();

	List<Collection> getCollectionIncentive(Date date);
}
