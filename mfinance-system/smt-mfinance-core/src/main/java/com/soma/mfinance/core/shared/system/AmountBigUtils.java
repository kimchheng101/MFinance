package com.soma.mfinance.core.shared.system;

import java.text.DecimalFormat;

import org.apache.commons.lang3.StringUtils;

import com.vaadin.ui.TextField;
/**
 *
 * @author Sok.vina
 *
 */
public class AmountBigUtils {
    /**
     * @param value
     * @return
     */
    public static String format(Double value) {
        if (value != null && StringUtils.isNotEmpty(value.toString())) {
            DecimalFormat stringValue = new DecimalFormat("###,###,###,###.00");
            if(getDoubleUSD(value) == 0d || getDoubleUSD(value) == 0.00){
                return "0.00";
            } else {
                return stringValue.format(value);
            }
        }
        return "";
    }

    /**
     * @param value
     * @return
     */
    public static Double getDoubleUSD(String value) {
        if (value != null && StringUtils.isNotEmpty(value)) {
            return Double.valueOf(value.replaceAll(",", ""));
        }
        return 0d;
    }
    /**
     * @param value
     * @return
     */
    public static Double getDoubleUSD(Double value) {
        if (value != null && StringUtils.isNotEmpty(value.toString())) {
            return Double.valueOf(value.toString().replaceAll(",", ""));
        }
        return 0d;
    }
    /**
     * @param value
     * @return
     */
    public static String getValue(String value) {
        if (value != null && StringUtils.isNotEmpty(value)) {
            return value.replaceAll(",", "");
        }
        return "";
    }
    /**
     * @param value
     * @return
     */
    public static Double getValueDouble(TextField component) {
        if (component.getValue() != null && StringUtils.isNotEmpty(component.getValue())) {
            return Double.valueOf(component.getValue().replaceAll(",", ""));
        }
        return 0d;
    }
}

