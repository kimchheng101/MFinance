package com.soma.mfinance.core.history.service;

import java.util.List;

import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.history.FinHistory;
import com.soma.mfinance.core.history.FinHistoryType;


/**
 * Fin History service
 * @author kimsuor.seang
 */
public interface FinHistoryService extends BaseEntityService {
	
	/**
	 * Create new fin history 
	 * @param contract
	 * @param historyType
	 * @param comment
	 */
	void addFinHistory(Contract contract, FinHistoryType historyType, String comment);
	
	/**
	 * Get fin histories by contract_old id & history type
	 * @param conId
	 * @param historyTypes
	 * @return
	 */
	List<FinHistory> getFinHistories(Long conId, FinHistoryType[] historyTypes);
}

