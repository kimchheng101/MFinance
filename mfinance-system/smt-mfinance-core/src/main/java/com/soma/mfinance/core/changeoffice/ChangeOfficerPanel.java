package com.soma.mfinance.core.changeoffice;

import com.soma.mfinance.core.quotation.QuotationService;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.quotation.QuotationProfileUtils;
import com.soma.mfinance.core.shared.system.FMProfile;
import com.soma.mfinance.core.widget.SecUserComboBox;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.event.SaveClickListener;
import com.vaadin.ui.*;
import org.seuksa.frmk.tools.spring.SpringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static com.soma.mfinance.core.helper.FinServicesHelper.QUO_SRV;

/**
 * Created by Dang Dim
 * Date     : 05-Sep-17, 10:26 AM
 * Email    : d.dim@gl-f.com
 */
public class ChangeOfficerPanel extends AbstractTabPanel {

    private static final long serialVersionUID = -4551937970638730828L;
    private SecUserComboBox cbxUnderwriter;
    private SecUserComboBox cbxUnderwriterSupervisor;
    private TextArea txtDescription;
    private Quotation quotation;


    public ChangeOfficerPanel() {
        super();
        setSizeFull();
    }

    @Override
    protected Component createForm() {
        VerticalLayout contentLayout = new VerticalLayout();

        List<SecUser> secUsersUS = QUO_SRV.getSecUserByProfile(FMProfile.US);
        List<SecUser> secUsersUW = QUO_SRV.getSecUserByProfile(FMProfile.UW);
        cbxUnderwriter = new SecUserComboBox(secUsersUW);
        cbxUnderwriterSupervisor = new SecUserComboBox(secUsersUS);
        txtDescription = ComponentFactory.getTextArea(false, 300, 80);

        String template = "changeOfficerLayout";
        InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
        CustomLayout customLayout = null;

        try {
            customLayout = new CustomLayout(layoutFile);
        } catch (IOException e) {
            Notification.show("Could not locate template " + template, e.getMessage(), Notification.Type.ERROR_MESSAGE);
        }

        customLayout.addComponent(new Label(org.seuksa.frmk.i18n.I18N.message("underwriter")), "lblUnderwriter");
        customLayout.addComponent(cbxUnderwriter, "cbxUnderwriter");

        customLayout.addComponent(new Label(org.seuksa.frmk.i18n.I18N.message("underwriter.supervisor")), "lblUnderwriterSupervisor");
        customLayout.addComponent(cbxUnderwriterSupervisor, "cbxUnderwriterSupervisor");

        //customLayout.addComponent(new Label(I18N.message("description")), "lblDescription");
        //customLayout.addComponent(txtDescription, "txtDescription");

        contentLayout.addComponent(customLayout);

        return contentLayout;
    }

    private void setVisiableComboBox() {
        boolean isChangeable = true;
        boolean isChangeable2 = true;
        if (!QuotationProfileUtils.isComboboxUnderwriterAvaialable(quotation)) {
            cbxUnderwriter.setEnabled(false);
            isChangeable = false;
        }

        if (!QuotationProfileUtils.isComboboxUnderwriterSuperVisorAvaialable(quotation)) {
            cbxUnderwriterSupervisor.setEnabled(false);
            isChangeable2 = false;
        }

        if (!(isChangeable && isChangeable2)) {
            txtDescription.setEnabled(false);
        }

    }

    public void assignValues(Quotation quotation) {

        resetComponent();
        this.quotation = quotation;
        cbxUnderwriter.setEnabled(true);
        cbxUnderwriterSupervisor.setEnabled(true);
        txtDescription.setEnabled(true);

        if (quotation.getUnderwriter() != null) {
            cbxUnderwriter.setSelectedEntity(quotation.getUnderwriter());
        }

        if (quotation.getUnderwriterSupervisor() != null) {
            cbxUnderwriterSupervisor.setSelectedEntity(quotation.getUnderwriterSupervisor());
        }

        setVisiableComboBox();

    }

    public Quotation getQuotation(Quotation quotation){
        if (cbxUnderwriter.getSelectedEntity() != null )
            quotation.setUnderwriter(cbxUnderwriter.getSelectedEntity());
        if (cbxUnderwriterSupervisor.getSelectedEntity() != null)
            quotation.setUnderwriterSupervisor(cbxUnderwriterSupervisor.getSelectedEntity());
        return quotation;
    }

    private void resetComponent() {
        cbxUnderwriter.setSelectedEntity(null);
        cbxUnderwriterSupervisor.setSelectedEntity(null);
        cbxUnderwriter.setEnabled(true);
        cbxUnderwriterSupervisor.setEnabled(true);
    }

    public void reset() {
        assignValues(new Quotation());
    }

}
