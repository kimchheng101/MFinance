package com.soma.mfinance.core.quotation.panel;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.quotation.QuotationService;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationApplicant;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.mfinance.core.widget.QuotationStatusComboBox;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.ersys.core.hr.model.address.Province;
import com.soma.frmk.security.context.SecApplicationContextHolder;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.EntityColumnRenderer;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.*;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Dang Dim
 * Date     : 06-Sep-17, 8:47 AM
 * Email    : d.dim@gl-f.com
 */
public class CheckPanel extends AbstractTabPanel implements View, QuotationEntityField {

    private static final long serialVersionUID = 7590210068280002924L;

    private static final String DEALER_TYPE = "dealer.type";
    private final QuotationService quotationService = SpringUtils.getBean(QuotationService.class);

    private TabSheet tabSheet;
    private SimplePagedTable<Quotation> pagedTable;
    private List<ColumnDefinition> columnDefinitions;
    private EntityRefComboBox<EDealerType> cbxDealerType;
    private DealerComboBox cbxDealer;
    private TextField txtReference;
    private AutoDateField dfStartDate;
    private AutoDateField dfEndDate;
    private TextField txtLastNameEn;
    private TextField txtFirstNameEn;
    private TextField txtGuaFamilyName;
    private TextField txtGuaFirstName;
    private QuotationStatusComboBox cbxQuotationStatus;
    private QuotationStatusComboBox cbxQuotationStatus2;
    private QuotationStatusComboBox cbxQuotationStatus3;
    private QuotationStatusComboBox cbxQuotationStatus4;
    private EntityRefComboBox<Province> cbxProvince;
    private Property.ValueChangeListener valueChangeListener;

    public CheckPanel() {
        super();
        setSizeFull();
    }

    @Override
    protected Component createForm() {

        tabSheet = new TabSheet();
        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setSpacing(true);
        contentLayout.setMargin(true);

        VerticalLayout gridLayoutPanel = new VerticalLayout();
        VerticalLayout searchLayout = new VerticalLayout();
        HorizontalLayout buttonsLayout = new HorizontalLayout();

        Button btnSearch = new Button(I18N.message("search"));
        btnSearch.setClickShortcut(ShortcutAction.KeyCode.ENTER, null); // null it means we don't modify key of shortcut Enter(default = 13)
        btnSearch.setIcon(new ThemeResource("../smt-default/icons/16/search.png"));
        btnSearch.addClickListener(event -> {
            search();
        });

        Button btnReset = new Button(I18N.message("reset"));
        btnReset.addClickListener(event -> {
            reset();
        });

        buttonsLayout.setSpacing(true);
        buttonsLayout.addComponent(btnSearch);
        buttonsLayout.addComponent(btnReset);
        final GridLayout gridLayout = new GridLayout(12, 3);
        gridLayout.setSpacing(true);
        cbxDealer = new DealerComboBox(null, ENTITY_SRV.list(getDealerRestriction()), I18N.message("all"));
        cbxDealer.setWidth("220px");

        List<EDealerType> dealerTypes = EDealerType.values();
        cbxDealerType = new EntityRefComboBox<>(dealerTypes);
        cbxDealerType.setImmediate(true);
        cbxDealerType.setWidth("220px");

        cbxDealerType.addValueChangeListener(event -> {
            BaseRestrictions<Dealer> restrictions = getDealerRestriction();
            if (cbxDealerType.getSelectedEntity() != null) {
                restrictions.addCriterion(Restrictions.eq("dealerType", cbxDealerType.getSelectedEntity()));
            }
            cbxDealer.setDealers(ENTITY_SRV.list(restrictions));
            cbxDealer.setSelectedEntity(null);
        });

        txtReference = ComponentFactory.getTextField(false, 20, 150);

        dfStartDate = ComponentFactory.getAutoDateField("", false);
        dfStartDate.setValue(DateUtils.addDaysDate(DateUtils.today(), -1));
        dfEndDate = ComponentFactory.getAutoDateField("", false);
        dfEndDate.setValue(DateUtils.today());

        txtLastNameEn = ComponentFactory.getTextField(I18N.message("lastname.en"), false, 60, 150);
        txtFirstNameEn = ComponentFactory.getTextField(I18N.message("firstname.en"), false, 60, 150);
        txtGuaFamilyName = ComponentFactory.getTextField(I18N.message("lastname.en"), false, 60, 150);
        txtGuaFirstName = ComponentFactory.getTextField(I18N.message("firstname.en"), false, 60, 150);

        cbxQuotationStatus = new QuotationStatusComboBox(null, true);
        cbxQuotationStatus.setImmediate(true);
        cbxQuotationStatus.setWidth("220px");

        cbxQuotationStatus2 = new QuotationStatusComboBox(null, true);
        cbxQuotationStatus2.setImmediate(true);
        cbxQuotationStatus2.setWidth("220px");

        cbxQuotationStatus3 = new QuotationStatusComboBox(null, true);
        cbxQuotationStatus3.setImmediate(true);
        cbxQuotationStatus3.setWidth("220px");

        cbxQuotationStatus4 = new QuotationStatusComboBox(null, true);
        cbxQuotationStatus4.setImmediate(true);
        cbxQuotationStatus4.setWidth("220px");

        cbxProvince = new EntityRefComboBox<Province>();
        cbxProvince.setRestrictions(new BaseRestrictions<Province>(Province.class));
        cbxProvince.renderer();
        cbxProvince.setImmediate(true);
        cbxProvince.setWidth("220px");

        int iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("dealer.type")), iCol++, 0);
        gridLayout.addComponent(cbxDealerType, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("dealer")), iCol++, 0);
        gridLayout.addComponent(cbxDealer, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("contract.reference")), iCol++, 0);
        gridLayout.addComponent(txtReference, iCol++, 0);

        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("quotation.status")), iCol++, 1);
        gridLayout.addComponent(cbxQuotationStatus, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("quotation.status")), iCol++, 1);
        gridLayout.addComponent(cbxQuotationStatus2, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("province")), iCol++, 1);
        gridLayout.addComponent(cbxProvince, iCol++, 1);

        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("quotation.status")), iCol++, 2);
        gridLayout.addComponent(cbxQuotationStatus3, iCol++, 2);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 2);
        gridLayout.addComponent(new Label(I18N.message("quotation.status")), iCol++, 2);
        gridLayout.addComponent(cbxQuotationStatus4, iCol++, 2);

        gridLayoutPanel.addComponent(gridLayout);

        HorizontalLayout checkLayout = new HorizontalLayout();
        HorizontalLayout lesseeLayout = new HorizontalLayout();
        lesseeLayout.setSpacing(true);
        lesseeLayout.setMargin(true);

        lesseeLayout.addComponent(new FormLayout(txtLastNameEn));
        lesseeLayout.addComponent(new FormLayout(txtFirstNameEn));

        Panel lesseePanel = new Panel();

        lesseePanel.setCaption(I18N.message("lessee.caption"));
        lesseePanel.setContent(lesseeLayout);

        HorizontalLayout guaranLayout = new HorizontalLayout();
        guaranLayout.setSpacing(true);
        guaranLayout.setMargin(true);

        guaranLayout.addComponent(new FormLayout(txtGuaFamilyName));
        guaranLayout.addComponent(new FormLayout(txtGuaFirstName));

        Panel guarantorPanel = new Panel();
        guarantorPanel.setCaption(I18N.message("guarantor"));
        guarantorPanel.setContent(guaranLayout);

        checkLayout.setMargin(true);
        checkLayout.setSpacing(true);
        checkLayout.addComponent(lesseePanel);
        checkLayout.addComponent(guarantorPanel);

        searchLayout.setMargin(true);
        searchLayout.setSpacing(true);
        searchLayout.addComponent(gridLayoutPanel);
        searchLayout.addComponent(checkLayout);
        searchLayout.addComponent(buttonsLayout);

        Panel searchPanel = new Panel();
        searchPanel.setCaption(I18N.message("search"));
        searchPanel.setContent(searchLayout);

        this.columnDefinitions = createColumnDefinitions();
        pagedTable = new SimplePagedTable<Quotation>(this.columnDefinitions);
        pagedTable.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            private static final long serialVersionUID = -6676228064499031341L;

            @Override
            public void itemClick(ItemClickEvent event) {
                boolean isDoubleClick = event.isDoubleClick() || SecApplicationContextHolder.getContext().clientDeviceIsMobileOrTablet();
                if (isDoubleClick) {
                    Item item = event.getItem();
                    Long quoId = (Long) item.getItemProperty("id").getValue();
                    String basePath = Page.getCurrent().getLocation().getScheme() + ":" + Page.getCurrent().getLocation().getSchemeSpecificPart();
                    VaadinService.getCurrentRequest().getWrappedSession().setAttribute("quoId", quoId);

                    getUI().getPage().open(basePath, "_blank");

                }
            }
        });

        contentLayout.addComponent(searchPanel);
        contentLayout.addComponent(pagedTable);
        contentLayout.addComponent(pagedTable.createControls());

        tabSheet.addTab(contentLayout, I18N.message("application"));

        return tabSheet;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        search();
    }

    private BaseRestrictions<Dealer> getDealerRestriction() {
        BaseRestrictions<Dealer> restrictions = new BaseRestrictions<Dealer>(Dealer.class);
        restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
        return restrictions;
    }

    public void search() {
        if (/*!requireField() && */quotationService != null) {
            List<Quotation> quotations = quotationService.getListQuotation(getRestrictions());
            setIndexedContainer(quotations);
        }
    }

    /**
     * @return Validate Controls
     */
    private boolean requireField() {
        if (StringUtils.isEmpty(txtGuaFamilyName.getValue())
                && StringUtils.isEmpty(txtGuaFirstName.getValue())
                && StringUtils.isEmpty(txtLastNameEn.getValue())
                && StringUtils.isEmpty(txtFirstNameEn.getValue())) {

            MessageBox mb = new MessageBox(
                    UI.getCurrent(),
                    "400px",
                    "160px",
                    I18N.message("information"),
                    MessageBox.Icon.ERROR,
                    I18N.message("field.required.1", I18N.message("check.input.lessee.gaurantor")),
                    Alignment.MIDDLE_RIGHT,
                    new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, I18N.message("ok")));
            mb.show();
            return true;
        }
        return false;
    }

    /**
     * @param qoutations Index Containers
     */
    @SuppressWarnings("unchecked")
    private void setIndexedContainer(List<Quotation> qoutations) {
        Container.Indexed indexedContainer = pagedTable.getContainerDataSource();
        indexedContainer.removeAllItems();
        if (qoutations != null && !qoutations.isEmpty()) {
            for (Quotation quotation : qoutations) {
                Item item = indexedContainer.addItem(quotation.getId());
                if (item != null) {
                    item.getItemProperty(ID).setValue(quotation.getId());
                    item.getItemProperty(REFERENCE).setValue(quotation != null ? quotation.getReference() : "");
                    item.getItemProperty(CUSTOMER).setValue(quotation.getMainApplicant().getLastNameEn() + " " + quotation.getMainApplicant().getFirstNameEn());
                    item.getItemProperty(ASSET + "." + DESC_EN).setValue(quotation.getAsset() != null ? quotation.getAsset().getModel().getDescEn() : "");
                    item.getItemProperty(DEALER + "." + DEALER_TYPE + "." + DESC).setValue(quotation.getDealer() != null && quotation.getDealer().getDealerType() != null ? quotation.getDealer().getDealerType().getDesc() : "");
                    item.getItemProperty(DEALER + "." + NAME_EN).setValue(quotation.getDealer() != null ? quotation.getDealer().getNameEn() : "");
                    item.getItemProperty(STATUS).setValue(quotation.getWkfFlow() != null ? quotation.getWkfStatus().getDesc() : "");
                    item.getItemProperty(QUOTATION_DATE).setValue(quotation.getCreateDate());
                    item.getItemProperty(FIRST_SUBMISSION_DATE).setValue(quotation.getFirstSubmissionDate());
                }
            }
        }
        pagedTable.refreshContainerDataSource();
    }

    protected List<ColumnDefinition> createColumnDefinitions() {
        columnDefinitions = new ArrayList<ColumnDefinition>();
        columnDefinitions.add(new ColumnDefinition(ID, I18N.message("id"), Long.class, Table.Align.LEFT, 60));
        columnDefinitions.add(new ColumnDefinition(REFERENCE, I18N.message("reference"), String.class, Table.Align.LEFT, 125));
        columnDefinitions.add(new ColumnDefinition(CUSTOMER, I18N.message("customer"), String.class, Table.Align.LEFT, 150, new CustomerFullNameColumnRenderer()));
        columnDefinitions.add(new ColumnDefinition(ASSET + "." + DESC_EN, I18N.message("asset"), String.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition(DEALER + "." + DEALER_TYPE + "." + DESC, I18N.message("dealer.type"), String.class, Table.Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition(DEALER + "." + NAME_EN, I18N.message("dealer"), String.class, Table.Align.LEFT, 200));
        columnDefinitions.add(new ColumnDefinition(STATUS, I18N.message("status"), String.class, Table.Align.LEFT, 140, new StatusColumnRenderer()));
        columnDefinitions.add(new ColumnDefinition(QUOTATION_DATE, I18N.message("quotation.date"), Date.class, Table.Align.LEFT, 80));
        columnDefinitions.add(new ColumnDefinition(FIRST_SUBMISSION_DATE, I18N.message("first.submission.date"), Date.class, Table.Align.LEFT, 150));
        return columnDefinitions;
    }

    private class CustomerFullNameColumnRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            Applicant customer = ((Quotation) getEntity()).getMainApplicant();
            return customer.getLastNameEn() + " " + customer.getFirstNameEn();
        }
    }

    private class StatusColumnRenderer extends EntityColumnRenderer {

        @Override
        public Object getValue() {
            EWkfStatus quotationStatus = ((Quotation) getEntity()).getWkfStatus();
            Contract contractStatus = null;
            contractStatus = ((Quotation) getEntity()).getContract();

            if (contractStatus == null) {
                return quotationStatus.getDesc();
            }

            if (ProfileUtil.isDocumentController()
                    || ProfileUtil.isUnderwriterSupervisor()
                    || ProfileUtil.isUnderwriter()
                    || ProfileUtil.isCreditOfficer()
                    || ProfileUtil.isProductionOfficer()
                    || ProfileUtil.isCreditOfficerMovable()
                    || ProfileUtil.isCollectionController()
                    || ProfileUtil.isCollectionSupervisor()
                    || ProfileUtil.isManager()
                    || ProfileUtil.isPOS()
                    || ProfileUtil.isAdmin()) {

            }
            if (contractStatus.getWkfStatus() == ContractWkfStatus.REP
                    || contractStatus.getWkfStatus() == ContractWkfStatus.THE
                    || contractStatus.getWkfStatus() == ContractWkfStatus.ACC
                    || contractStatus.getWkfStatus() == ContractWkfStatus.WRI
                    || contractStatus.getWkfStatus() == ContractWkfStatus.EAR
                    || contractStatus.getWkfStatus() == ContractWkfStatus.CLO
                //|| contractStatus.getContractStatus() == ContractWkfStatus.TRA
                    ) {
                return contractStatus.getWkfStatus().getDesc();
            }

            return quotationStatus.getDesc();
        }
    }

    /**
     * @return List of quotation by criteria
     */
    public BaseRestrictions<Quotation> getRestrictions() {

        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<Quotation>(Quotation.class);
        restrictions.addAssociation("quotationApplicants", "quoapp", JoinType.INNER_JOIN);
        restrictions.addAssociation("quoapp." + APPLICANT, "app", JoinType.INNER_JOIN);
        restrictions.addAssociation("app." + INDIVIDUAL, "ind", JoinType.INNER_JOIN);

        if (StringUtils.isNotEmpty(txtGuaFamilyName.getValue()) || StringUtils.isNotEmpty(txtGuaFirstName.getValue())
                && StringUtils.isNotEmpty(txtLastNameEn.getValue()) || StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {

            restrictions.addCriterion("quoapp." + APPLICANT_TYPE, EApplicantType.C);
            restrictions.addCriterion("quoapp." + APPLICANT_TYPE, EApplicantType.G);
            Disjunction disjunction = Restrictions.disjunction();
            Conjunction conjunction = Restrictions.conjunction();
            Conjunction conjunction1 = Restrictions.conjunction();

            conjunction.add(Restrictions.ilike("ind." + LAST_NAME_EN, txtGuaFamilyName.getValue(), MatchMode.ANYWHERE));
            conjunction.add(Restrictions.ilike("ind." + FIRST_NAME_EN, txtGuaFirstName.getValue(), MatchMode.ANYWHERE));
            conjunction1.add(Restrictions.ilike("ind." + LAST_NAME_EN, txtLastNameEn.getValue(), MatchMode.ANYWHERE));
            conjunction1.add(Restrictions.ilike("ind." + FIRST_NAME_EN, txtFirstNameEn.getValue(), MatchMode.ANYWHERE));

            disjunction.add(conjunction);
            disjunction.add(conjunction1);
            restrictions.addCriterion(disjunction);
        } else {
            if (StringUtils.isNotEmpty(txtReference.getValue())) {
                restrictions.addCriterion(Restrictions.like(REFERENCE, txtReference.getValue(), MatchMode.ANYWHERE));
            }

            if (StringUtils.isNotEmpty(txtGuaFamilyName.getValue()) || StringUtils.isNotEmpty(txtGuaFirstName.getValue())) {
                restrictions.addCriterion("quoapp." + APPLICANT_TYPE, EApplicantType.G);
            }

            if (StringUtils.isNotEmpty(txtLastNameEn.getValue()) || StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
                restrictions.addCriterion("quoapp." + APPLICANT_TYPE, EApplicantType.C);
            }

            if (StringUtils.isNotEmpty(txtGuaFamilyName.getValue())) {
                restrictions.addCriterion(Restrictions.ilike("ind." + LAST_NAME_EN, txtGuaFamilyName.getValue(), MatchMode.ANYWHERE));
            }
            if (StringUtils.isNotEmpty(txtGuaFirstName.getValue())) {
                restrictions.addCriterion(Restrictions.ilike("ind." + FIRST_NAME_EN, txtGuaFirstName.getValue(), MatchMode.ANYWHERE));
            }

            if (StringUtils.isNotEmpty(txtLastNameEn.getValue())) {
                restrictions.addCriterion(Restrictions.ilike("ind." + LAST_NAME_EN, txtLastNameEn.getValue(), MatchMode.ANYWHERE));
            }

            if (StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
                restrictions.addCriterion(Restrictions.ilike("ind." + FIRST_NAME_EN, txtFirstNameEn.getValue(), MatchMode.ANYWHERE));
            }
        }

        restrictions.addAssociation(DEALER, "dea", JoinType.INNER_JOIN);
        if (cbxDealer.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, cbxDealer.getSelectedEntity().getId()));
        }

        if (cbxProvince.getSelectedEntity() != null) {
            restrictions.addAssociation("ind.individualAddresses", "appaddr", JoinType.INNER_JOIN);
            restrictions.addAssociation("appaddr.address", "addr", JoinType.INNER_JOIN);
            restrictions.addCriterion("addr.province.id", cbxProvince.getSelectedEntity().getId());
        }

        if (cbxQuotationStatus.getSelectedEntity() != null
                || cbxQuotationStatus2.getSelectedEntity() != null
                || cbxQuotationStatus3.getSelectedEntity() != null
                || cbxQuotationStatus4.getSelectedEntity() != null) {

            Disjunction orJunction = Restrictions.or();
            Conjunction andJunction = Restrictions.and();

            if (cbxQuotationStatus.getSelectedEntity() == QuotationWkfStatus.ACT
                    || cbxQuotationStatus2.getSelectedEntity() == QuotationWkfStatus.ACT
                    || cbxQuotationStatus3.getSelectedEntity() == QuotationWkfStatus.ACT
                    || cbxQuotationStatus4.getSelectedEntity() == QuotationWkfStatus.ACT) {
                restrictions.addAssociation("contract", "cotra", JoinType.LEFT_OUTER_JOIN);
                andJunction.add(Restrictions.eq("cotra.wkfStatus", ContractWkfStatus.FIN));
                andJunction.add(Restrictions.eq(STATUS, QuotationWkfStatus.ACT));
                orJunction.add(andJunction);
            }

            if (cbxQuotationStatus.getSelectedEntity() == ContractWkfStatus.WRI
                    || cbxQuotationStatus2.getSelectedEntity() == ContractWkfStatus.WRI
                    || cbxQuotationStatus3.getSelectedEntity() == ContractWkfStatus.WRI
                    || cbxQuotationStatus4.getSelectedEntity() == ContractWkfStatus.WRI) {
                restrictions.addAssociation("contract", "cotra", JoinType.LEFT_OUTER_JOIN);
                orJunction.add(Restrictions.eq("cotra.wkfStatus", ContractWkfStatus.WRI));
            }
            if (cbxQuotationStatus.getSelectedEntity() == ContractWkfStatus.REP
                    || cbxQuotationStatus2.getSelectedEntity() == ContractWkfStatus.REP
                    || cbxQuotationStatus3.getSelectedEntity() == ContractWkfStatus.REP
                    || cbxQuotationStatus4.getSelectedEntity() == ContractWkfStatus.REP) {
                restrictions.addAssociation("contract", "cotra", JoinType.LEFT_OUTER_JOIN);
                orJunction.add(Restrictions.eq("cotra.wkfStatus", ContractWkfStatus.REP));
            }
            if (cbxQuotationStatus.getSelectedEntity() == ContractWkfStatus.THE
                    || cbxQuotationStatus2.getSelectedEntity() == ContractWkfStatus.THE
                    || cbxQuotationStatus3.getSelectedEntity() == ContractWkfStatus.THE
                    || cbxQuotationStatus4.getSelectedEntity() == ContractWkfStatus.THE) {
                restrictions.addAssociation("contract", "cotra", JoinType.LEFT_OUTER_JOIN);
                orJunction.add(Restrictions.eq("cotra.wkfStatus", ContractWkfStatus.THE));
            }
            if (cbxQuotationStatus.getSelectedEntity() == ContractWkfStatus.ACC
                    || cbxQuotationStatus2.getSelectedEntity() == ContractWkfStatus.ACC
                    || cbxQuotationStatus3.getSelectedEntity() == ContractWkfStatus.ACC
                    || cbxQuotationStatus4.getSelectedEntity() == ContractWkfStatus.ACC) {
                restrictions.addAssociation("contract", "cotra", JoinType.LEFT_OUTER_JOIN);
                orJunction.add(Restrictions.eq("cotra.wkfStatus", ContractWkfStatus.ACC));
            }
            if (cbxQuotationStatus.getSelectedEntity() == ContractWkfStatus.EAR
                    || cbxQuotationStatus2.getSelectedEntity() == ContractWkfStatus.EAR
                    || cbxQuotationStatus3.getSelectedEntity() == ContractWkfStatus.EAR
                    || cbxQuotationStatus4.getSelectedEntity() == ContractWkfStatus.EAR) {
                restrictions.addAssociation("contract", "cotra", JoinType.LEFT_OUTER_JOIN);
                orJunction.add(Restrictions.eq("cotra.wkfStatus", ContractWkfStatus.EAR));
            }

            if (cbxQuotationStatus.getSelectedEntity() != null && cbxQuotationStatus.getSelectedEntity() != QuotationWkfStatus.ACT) {
                restrictions.addAssociation("contract", "cotra", JoinType.LEFT_OUTER_JOIN);
                orJunction.add(Restrictions.eq("cotra.wkfStatus", cbxQuotationStatus.getSelectedEntity()));
                orJunction.add(Restrictions.eq(STATUS, cbxQuotationStatus.getSelectedEntity()));
            }
            if (cbxQuotationStatus2.getSelectedEntity() != null && cbxQuotationStatus2.getSelectedEntity() != QuotationWkfStatus.ACT) {
                restrictions.addAssociation("contract", "cotra", JoinType.LEFT_OUTER_JOIN);
                orJunction.add(Restrictions.eq("cotra.wkfStatus", cbxQuotationStatus2.getSelectedEntity()));
                orJunction.add(Restrictions.eq(STATUS, cbxQuotationStatus2.getSelectedEntity()));
            }
            if (cbxQuotationStatus3.getSelectedEntity() != null && cbxQuotationStatus3.getSelectedEntity() != QuotationWkfStatus.ACT) {
                restrictions.addAssociation("contract", "cotra", JoinType.LEFT_OUTER_JOIN);
                orJunction.add(Restrictions.eq("cotra.wkfStatus", cbxQuotationStatus3.getSelectedEntity()));
                orJunction.add(Restrictions.eq(STATUS, cbxQuotationStatus3.getSelectedEntity()));
            }
            if (cbxQuotationStatus4.getSelectedEntity() != null && cbxQuotationStatus4.getSelectedEntity() != QuotationWkfStatus.ACT) {
                restrictions.addAssociation("contract", "cotra", JoinType.LEFT_OUTER_JOIN);
                orJunction.add(Restrictions.eq("cotra.wkfStatus", cbxQuotationStatus4.getSelectedEntity()));
                orJunction.add(Restrictions.eq(STATUS, cbxQuotationStatus4.getSelectedEntity()));
            }
            restrictions.addCriterion(orJunction);

        } else if (!cbxQuotationStatus.getValueMap().values().isEmpty() || !cbxQuotationStatus2.getValueMap().values().isEmpty()
                || !cbxQuotationStatus3.getValueMap().values().isEmpty() || !cbxQuotationStatus4.getValueMap().values().isEmpty()) {
            List<EWkfStatus> defaultQuotationStatusList = EWkfStatus.values();

            if (defaultQuotationStatusList != null && !defaultQuotationStatusList.isEmpty()) {
                restrictions.addCriterion(Restrictions.in(STATUS, defaultQuotationStatusList));
            } else {
                LinkedHashMap<String, EWkfStatus> quotationStatusMap = new LinkedHashMap<String, EWkfStatus>();
                quotationStatusMap.putAll(cbxQuotationStatus.getValueMap());
                quotationStatusMap.putAll(cbxQuotationStatus2.getValueMap());
                quotationStatusMap.putAll(cbxQuotationStatus3.getValueMap());
                quotationStatusMap.putAll(cbxQuotationStatus4.getValueMap());
                restrictions.addCriterion(Restrictions.in(STATUS, quotationStatusMap.values()));
            }
        } else {
            restrictions.addCriterion(Restrictions.isNull(STATUS));
        }

        return restrictions;
    }

    /**
     * @param quotation
     */
    public void assignValues(Quotation quotation) {
        if (quotation != null) {
            List<QuotationApplicant> quotationApplicants = quotation.getQuotationApplicants();
            for (QuotationApplicant quotationApplicant : quotationApplicants) {
                if (quotationApplicant.getApplicantType().equals(EApplicantType.C)) {
                    txtLastNameEn.setValue(quotationApplicant.getApplicant().getLastNameEn());
                    txtFirstNameEn.setValue(quotationApplicant.getApplicant().getFirstNameEn());
                }
                if (quotationApplicant.getApplicantType().equals(EApplicantType.G)) {
                    txtGuaFamilyName.setValue(quotationApplicant.getApplicant().getLastNameEn());
                    txtGuaFirstName.setValue(quotationApplicant.getApplicant().getFirstNameEn());
                }
            }
            cbxDealerType.setSelectedEntity(EDealerType.HEAD);
        }
        search();
    }

    /**
     * Reset all controls in Check panel
     */
    public void reset() {
        txtReference.setValue("");
        cbxDealer.setSelectedEntity(null);
        cbxDealerType.setSelectedEntity(EDealerType.HEAD);
        dfStartDate.setValue(DateUtils.addMonthsDate(DateUtils.today(), -2));
        dfEndDate.setValue(DateUtils.today());
        txtGuaFamilyName.setValue("");
        txtGuaFirstName.setValue("");
        txtLastNameEn.setValue("");
        txtFirstNameEn.setValue("");
        pagedTable.removeAllItems();
    }
}
