package com.soma.mfinance.core.application;

import com.soma.mfinance.core.shared.FMEntityField;

/**
 * Created by Kimsuor SEANG
 * Date  : 6/3/2017
 * Name  : 2:31 PM
 * Email : k.seang@gl-f.com
 */
public interface ApplicationFields extends FMEntityField {

    public static String EMP_CODE = "N";
    public static String SELF_EMP_CODE = "Y";
}
