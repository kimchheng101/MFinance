package com.soma.mfinance.core.collection.model;

import com.soma.mfinance.core.address.model.Area;
import com.soma.frmk.security.model.SecUser;
import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author kimsuor.seang
 *
 */
@Entity
@Table(name = "tu_col_assfile")
public class ColAssignment extends EntityA {

	/** */
	private static final long serialVersionUID = 4497571547024622545L;
	
	private SecUser assignee;
	private SecUser assignedBy;
	private boolean defaultAssignee;
	private OverduePeriod overduePeriod;
	private EColTask task;
	private List<EColResult> statuses;
	private List<Area> areas;

	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "col_ass_id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ovd_prd_id", nullable = true)
	public OverduePeriod getOverduePeriod() {
		return overduePeriod;
	}

	public void setOverduePeriod(OverduePeriod overduePeriod) {
		this.overduePeriod = overduePeriod;
	}

	/**
	 * @return the task
	 */
    @Column(name = "col_sta_id", nullable = true)
    @Convert(converter = EColTask.class)
	public EColTask getCollectionTask() {
		return task;
	}

	/**
	 * @param task the task to set
	 */
	public void setCollectionTask(EColTask task) {
		this.task = task;
	}

	/**
	 * @return the statuses
	 */
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="tu_col_assignment_result",
				joinColumns = { @JoinColumn(name = "col_ass_id") }, 
				inverseJoinColumns = { @JoinColumn(name = "col_res_id") })
	public List<EColResult> getStatuses() {
		if (statuses == null) {
			statuses = new ArrayList<>();
		}
		return statuses;
	}

	/**
	 * @param statuses the statuses to set
	 */
	public void setStatuses(List<EColResult> statuses) {
		this.statuses = statuses;
	}

	/**
	 * @return the groups
	 */
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="tu_col_assignment_area",
				joinColumns = { @JoinColumn(name = "col_ass_id") }, 
				inverseJoinColumns = { @JoinColumn(name = "are_id") })
	public List<Area> getAreas() {
		if (areas == null) {
			areas = new ArrayList<>();
		}
		return areas;
	}

	/**
	 * @param areas the groups to set
	 */
	public void setAreas(List<Area> areas) {
		this.areas = areas;
	}


	/**
	 * @return the assignee
	 */
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ass_sec_usr_id", nullable = true)
	public SecUser getAssignee() {
		return assignee;
	}

	/**
	 * @param assignee the assignee to set
	 */
	public void setAssignee(SecUser assignee) {
		this.assignee = assignee;
	}

	/**
	 * @return the assignedBy
	 */
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ass_by_sec_usr_id", nullable = true)
	public SecUser getAssignedBy() {
		return assignedBy;
	}

	/**
	 * @param assignedBy the assignedBy to set
	 */
	public void setAssignedBy(SecUser assignedBy) {
		this.assignedBy = assignedBy;
	}

	/**
	 * @return the task
	 */
    @Column(name = "col_tas_id", nullable = true)
    @Convert(converter = EColTask.class)
	public EColTask getTask() {
		return task;
	}

	/**
	 * @param task the task to set
	 */
	public void setTask(EColTask task) {
		this.task = task;
	}

	/**
	 * @return the defaultAssignee
	 */
	@Column(name = "col_ass_bl_default_assignee", nullable = false, columnDefinition = "boolean default false")
	public boolean isDefaultAssignee() {
		return defaultAssignee;
	}

	/**
	 * @param defaultAssignee the defaultAssignee to set
	 */
	public void setDefaultAssignee(boolean defaultAssignee) {
		this.defaultAssignee = defaultAssignee;
	}

	@Transient
	public void addArea(Area area) {
		if(areas == null)
			areas = new ArrayList<Area>();
		areas.add(area);
	}

	@Transient
	public void addStatus(EColResult colResult) {
		if(statuses == null)
			statuses = new ArrayList<EColResult>();
		statuses.add(colResult);
	}

	@Transient
	public String getAreaDes() {
		StringBuffer areaDesc = new StringBuffer();
		if(areas != null) {
			for (Area area : areas) {
				areaDesc.append(area.getDescEn()).append(" ,");
			}
		}
		return areaDesc.toString();
	}
}
