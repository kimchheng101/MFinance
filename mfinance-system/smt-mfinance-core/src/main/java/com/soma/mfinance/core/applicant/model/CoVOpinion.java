package com.soma.mfinance.core.applicant.model;

import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;

/**
 * Created by ki.kao on 7/13/2017.
 */
@Entity
@Table(name = "tu_cov_opinion")
public class CoVOpinion extends EntityRefA {

    private static final long serialVersionUID = -1204946642516804246L;

    /**
     * @see org.seuksa.frmk.mvc.model.entity.EntityA#getId()
     */
    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "covop_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * @see org.seuksa.frmk.mvc.model.entity.AuditEntityRef#getCode()
     */
    @Override
    @Transient
    public String getCode() {
        return null;
    }

    /**
     * @see org.seuksa.frmk.mvc.model.entity.AuditEntityRef#getDesc()
     */
    @Column(name = "covop_desc", nullable = false, length=50)
    @Override
    public String getDesc() {
        return super.getDesc();
    }

    /**
     * @return <String>
     */
    @Override
    @Column(name = "covop_desc_en", nullable = false, length=50)
    public String getDescEn() {
        return super.getDescEn();
    }
}
