package com.soma.mfinance.core.quotation.model;

import org.seuksa.frmk.model.eref.BaseERefData;

import javax.persistence.AttributeConverter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author by kimsuor.seang  on 11/13/2017.
 */
public class EInstallmentType extends BaseERefData implements AttributeConverter<EInstallmentType, Long> {

    public final static EInstallmentType INSTALLMENT = new EInstallmentType("INSTALLMENT", 1);
    public final static EInstallmentType LAST_INSTALLMENT = new EInstallmentType("LAST_INSTALLMENT", 2);

    /**
     * ts_ref_table
     */
    EInstallmentType() {

    }

    EInstallmentType(String code, long id) {
        super(code, id);
    }

    @Override
    public Long convertToDatabaseColumn(EInstallmentType attribute) {
        return super.convertToDatabaseColumn(attribute);
    }

    @SuppressWarnings("unchecked")
    @Override
    public EInstallmentType convertToEntityAttribute(Long dbData) {
        return super.convertToEntityAttribute(dbData);
    }

    public static List<EInstallmentType> values() {
        return getValues(EInstallmentType.class);
    }

    public static EInstallmentType getByCode(String code) {
        return getByCode(EInstallmentType.class, code);
    }

    public static EInstallmentType getById(long id) {
        return getById(EInstallmentType.class, id);
    }

    public static List<EInstallmentType> getEInstallmentTypeValue(){
        List<EInstallmentType> eInstallmentType = new ArrayList<>();
            eInstallmentType.add(INSTALLMENT);
            eInstallmentType.add(LAST_INSTALLMENT);
       return eInstallmentType;

    }

}
