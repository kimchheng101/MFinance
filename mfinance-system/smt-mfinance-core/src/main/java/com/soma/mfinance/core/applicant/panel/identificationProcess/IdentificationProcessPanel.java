package com.soma.mfinance.core.applicant.panel.identificationProcess;

import com.soma.mfinance.core.application.panel.ApplicationPanel;
import com.soma.mfinance.client.jersey.ClientQuotation;
import com.soma.mfinance.core.application.panel.ApplicationFormPanel;
import com.soma.mfinance.core.custom.loader.PropertyLoader;
import com.soma.mfinance.core.financial.model.FinProduct;
import com.soma.mfinance.core.quotation.QuotationService;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.finance.services.shared.system.EFrequency;
import com.soma.frmk.security.model.SecUser;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.mfinance.core.application.panel.ApplicationPanel;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.Entity;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.seuksa.frmk.tools.DateUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.Response;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by b.chea on 5/25/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class IdentificationProcessPanel extends AbstractFormPanel {


    private PropertyLoader propertyLoader = SpringUtils.getBean(PropertyLoader.class);
    private QuotationService quotationService = SpringUtils.getBean(QuotationService.class);
    public static final String NAME = "Identification Process";

    private ApplicationPanel applicationPanel;
    private TextField txtFamilyName;
    private TextField txtFirstName;
    private EntityRefComboBox<FinProduct> cbxFinancialProduct;
    private AutoDateField dfDateOfBirth;
    private TextField txtContractId;
    private NativeButton btnIdentify;
    private ApplicationFormPanel applicationFormPanel;
    protected final static Logger logger = LoggerFactory.getLogger(IdentificationProcessPanel.class);

    /** */
    @PostConstruct
    public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
        btnIdentify = new NativeButton(I18N.message("identify"));
        btnIdentify.setIcon(new ThemeResource(
                "../smt-default/icons/16/user.png"));
        btnIdentify.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {

                if(cbxFinancialProduct.getSelectedEntity() != null){
                    errors.add(I18N.message("Please Choose Type Of Financial Product"));
                    //applicationPanel.removeIdentificationProcessPanel();
                    applicationPanel.addApplicationFormPanel();
                    if(cbxFinancialProduct.getSelectedEntity().getCode().equals("PUB")){
                        if(getMFinanceApplication() != null){
                            //applicationPanel.getApplicationFormPanel().assignValues(getMFinanceApplication());
                        }else{
                            applicationPanel.getApplicationFormPanel().reset();
                        }
                        Notification.show("", I18N.message("Public"), Notification.Type.HUMANIZED_MESSAGE);
                    } else {
                        Notification.show("", I18N.message("Existing"), Notification.Type.HUMANIZED_MESSAGE);
                        //applicationPanel.getApplicationFormPanel().assignValues(getLesseeInfo(txtContractId.getValue()));
                    }
                }
            }
        });
        navigationPanel.addButton(btnIdentify);
    }


    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractFormPanel#createForm()
     */
    @Override
    protected com.vaadin.ui.Component createForm() {
        txtFamilyName = ComponentFactory.getTextField(I18N.message("familyname"), true, 100, 200);
        txtFirstName = ComponentFactory.getTextField(I18N.message("firstname"), true, 100, 200);
        txtContractId = ComponentFactory.getTextField(I18N.message("contract.id"), true, 100, 200);
        cbxFinancialProduct = new EntityRefComboBox<>();
        cbxFinancialProduct.setRestrictions(new BaseRestrictions<>(FinProduct.class));
        cbxFinancialProduct.setCaption(I18N.message("financial.product"));
        cbxFinancialProduct.setImmediate(true);
        cbxFinancialProduct.renderer();
        dfDateOfBirth = ComponentFactory.getAutoDateField(I18N.message("dateofbirth"), true);
        dfDateOfBirth.setValue(DateUtils.today());

        FormLayout formLayout = new FormLayout();
        formLayout.addComponent(txtFamilyName);
        formLayout.addComponent(txtFirstName);
        formLayout.addComponent(dfDateOfBirth);
        formLayout.addComponent(cbxFinancialProduct);
        formLayout.addComponent(txtContractId);
        return formLayout;
    }

    /**
     * Assign value to form
     *
     * @param
     */
    public void assignValues(Long interestId) {
        reset();
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractFormPanel#reset()
     */
    @Override
    public void reset() {
        super.reset();
        txtFamilyName.setValue("");
        txtFirstName.setValue("");
        dfDateOfBirth.setValue(DateUtils.today());
        cbxFinancialProduct.setValue(null);
        txtContractId.setValue("");
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractFormPanel#validate()
     */
    @Override
    protected boolean validate() {
        checkDoubleField(txtFamilyName, "familyname");
        checkMandatoryField(txtFirstName, "firstname");
        checkMandatoryField(txtContractId, "contract.id");
        checkMandatoryDateField(dfDateOfBirth, "dateofbirth");
        checkMandatorySelectField(cbxFinancialProduct, "financial.product");
        return errors.isEmpty();
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractFormPanel#getEntity()
     */
    @Override
    protected Entity getEntity() {
        return null;
    }

    public void setApplicationPanel(ApplicationPanel applicationPanel) {
        this.applicationPanel = applicationPanel;
    }

    private Long getLesseeInfo(String value) {

        Response response = null;
        if (propertyLoader != null) {
            String ef_ws_url = propertyLoader.getEnvironment().getProperty("ws.motoforplus.webservice");
            SecUser secUser = ProfileUtil.getCurrentUser();
            if (ef_ws_url != null && !ef_ws_url.isEmpty()) {
                response = ClientQuotation.getQuotationByReference(ef_ws_url, value, secUser.getLogin());
            }
        }


        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.eq("reference", value));
        List<Quotation> lstQuotations = ENTITY_SRV.list(restrictions);

        return (lstQuotations.size() > 0) ? lstQuotations.get(0).getId() : null;
    }

    public Long getMFinanceApplication() {

        try{
            if(txtContractId.getValue() == null){
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd 00:00:00.0");
                Date date = sdf.parse(sdf.format(dfDateOfBirth.getValue()));
                java.sql.Timestamp sq = new java.sql.Timestamp(date.getTime());

                BaseRestrictions<Individual> individuals = new BaseRestrictions<>(Individual.class);
                individuals.addCriterion(Restrictions.eq("lastNameEn", txtFamilyName.getValue()));
                individuals.addCriterion(Restrictions.eq("firstNameEn", txtFirstName.getValue()));
                individuals.addCriterion(Restrictions.eq("birthDate" , sq));
                List<Individual> listIndividuals = ENTITY_SRV.list(individuals);

                if(listIndividuals.size() > 0){
                    BaseRestrictions<Applicant> applicants = new BaseRestrictions<>(Applicant.class);
                    applicants.addCriterion(Restrictions.eq("individual.id" , listIndividuals.get(0).getId()));
                    List<Applicant> listApplicants = ENTITY_SRV.list(applicants);
                    if(listApplicants.size() >0){
                        BaseRestrictions<Quotation> quotations = new BaseRestrictions<>(Quotation.class);
                        quotations.addCriterion(Restrictions.eq("applicant.id" , listApplicants.get(0).getId()));
                        List<Quotation> listQuotations = ENTITY_SRV.list(quotations);
                        return listQuotations.get(0).getId();
                    }
                }
            }else{
                BaseRestrictions<Contract> contracts = new BaseRestrictions<>(Contract.class);
                contracts.addCriterion(Restrictions.ilike("externalReference", txtContractId.getValue() , MatchMode.ANYWHERE));
                List<Contract> listContracts = ENTITY_SRV.list(contracts);

                if(listContracts.size()>0){
                    BaseRestrictions<Quotation> quotations = new BaseRestrictions<>(Quotation.class);
                    quotations.addCriterion(Restrictions.eq("contract.id" , listContracts.get(0).getId()));
                    List<Quotation> listQuotations = ENTITY_SRV.list(quotations);
                    listQuotations.get(0).setTiAppraisalEstimateAmount(0.0);
                    listQuotations.get(0).setTeAppraisalEstimateAmount(0.0);
                    listQuotations.get(0).setLeaseAmountPercentage(100.0);
                    listQuotations.get(0).setInterestRate(0.0);
                    listQuotations.get(0).setTerm(24);
                    listQuotations.get(0).setFrequency(EFrequency.M);

                    return listQuotations.get(0).getId();
                }

            }
        }catch (Exception e){}
        return null;
    }

}

