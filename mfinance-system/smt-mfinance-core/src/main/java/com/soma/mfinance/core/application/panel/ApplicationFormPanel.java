package com.soma.mfinance.core.application.panel;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.panel.ApplicantDetailPanel;
import com.soma.mfinance.core.applicant.panel.guarantor.GuarantorPanel;
import com.soma.mfinance.core.application.historyStatus.HistoryStatusPanel;
import com.soma.mfinance.core.asset.panel.AssetPanel;
import com.soma.mfinance.core.asset.panel.AssetTabPanel;
import com.soma.mfinance.core.changeoffice.ChangeOfficerPanel;
import com.soma.mfinance.core.collection.panel.RepossessedPopupPanel;
import com.soma.mfinance.core.comment.CommentTablePanel;
import com.soma.mfinance.core.common.security.model.SecUserDetail;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.custom.component.CustomPopUp;
import com.soma.mfinance.core.dealer.panel.DealerPanel;
import com.soma.mfinance.core.document.panel.DocumentsPanel;
import com.soma.mfinance.core.financial.panel.product.FinProductPanel;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.QuotationService;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationSupportDecision;
import com.soma.mfinance.core.quotation.model.SupportDecision;
import com.soma.mfinance.core.quotation.panel.CheckPanel;
import com.soma.mfinance.core.quotation.panel.FieldCheckPanel;
import com.soma.mfinance.core.quotation.panel.ManagementPanel;
import com.soma.mfinance.core.quotation.panel.UnderwriterPanel;
import com.soma.mfinance.core.quotation.panel.comment.CommentFormPanel;
import com.soma.mfinance.core.referential.committee.GroupCommittee;
import com.soma.mfinance.core.shared.exception.ValidationFields;
import com.soma.mfinance.core.shared.exception.ValidationFieldsException;
import com.soma.mfinance.core.shared.exception.quotation.InvalidQuotationException;
import com.soma.mfinance.core.shared.financialproduct.FinancialProductEntityField;
import com.soma.mfinance.core.shared.quotation.QuotationProfileUtils;
import com.soma.mfinance.core.shared.system.DomainType;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.panel.ApplicantDetailPanel;
import com.soma.mfinance.core.applicant.panel.guarantor.GuarantorPanel;
import com.soma.mfinance.core.application.historyStatus.HistoryStatusPanel;
import com.soma.mfinance.core.asset.panel.AssetPanel;
import com.soma.mfinance.core.asset.panel.AssetTabPanel;
import com.soma.mfinance.core.changeoffice.ChangeOfficerPanel;
import com.soma.mfinance.core.common.security.model.SecUserDetail;
import com.soma.mfinance.third.einsurance.service.ContractMigrationService;
import com.soma.frmk.config.model.SettingConfig;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.beans.IntrospectionException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author kimsuor.seang (modified) Date: 04/09/2018
 */


@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ApplicationFormPanel extends AbstractFormPanel implements TabSheet.SelectedTabChangeListener, FinServicesHelper, Button.ClickListener {

    private static final long serialVersionUID = 1L;

    private Quotation quotation;
    private ApplicationPanel applicationPanel;
    private FinProductPanel finProductPanel;
    private FieldCheckPanel fieldCheckPanel;
    private DealerPanel dealerPanel;
    private HorizontalLayout fieldCheckRequestLayout;
    private Panel fieldCheckRequestPanel;
    private List<CheckBox> cbSupportDecisions;

    private ApplicantDetailPanel applicantDetailPanel;
    private CommentTablePanel commentTablePanel;
    private HistoryStatusPanel historyStatusPanel;

    private AssetTabPanel assetTabPanel;
    private DocumentsPanel documentsPanel;
    private UnderwriterPanel underwriterPanel;

    private ChangeOfficerPanel changeOfficerPanel;
    private CheckPanel checkPanel;

    private NativeButton btnDecline, btnCancel, btnSubmit, btnApprove, btnContractStartDate, btnReqRepossess;
    private NativeButton btnBackPOS, btnBackAUC, btnReject, btnActivate, btnConTranSettlement, btnBackAppraisal;

    private TabSheet applicationTab;
    private NavigationPanel defaultNavigationPanel;
    private VerticalLayout navigationPanel;
    private GuarantorPanel guarantorPanel;
    private ManagementPanel managementPanel;
    private VerticalLayout contentLayout;
    public MenuBar reportMenu;
    private Panel quotationInfoPanel;

    private DateField dfInsuranceStartDate;

    @Autowired
    private QuotationService quotationService;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        this.dfInsuranceStartDate = ComponentFactory.getAutoDateField("Start Insurance Contract", true);
        navigationPanel = new VerticalLayout();
        defaultNavigationPanel = new NavigationPanel();
        quotationInfoPanel = new Panel();

        addComponent(navigationPanel, 0);
        /* Add 3 More Button*/
        btnDecline = new NativeButton(I18N.message("decline"));
        btnDecline.setIcon(FontAwesome.MINUS_CIRCLE);
        btnDecline.addClickListener(this);

        btnCancel = new NativeButton(I18N.message("cancel"));
        btnCancel.setIcon(FontAwesome.BAN);
        btnCancel.addClickListener(this);

        btnSubmit = new NativeButton(I18N.message("submit"));
        btnSubmit.setIcon(FontAwesome.STEP_FORWARD);
        btnSubmit.addClickListener(this);

        btnApprove = new NativeButton(I18N.message("Approve"));
        btnApprove.setIcon(new ThemeResource("../smt-default/icons/16/tick.png"));
        btnApprove.addClickListener(this);

        btnReqRepossess = new NativeButton(I18N.message("Repossess"));
        btnReqRepossess.setIcon(new ThemeResource("../smt-default/icons/16/tick.png"));
        btnReqRepossess.addClickListener(this);
        btnReqRepossess.addClickListener(event -> {
            UI.getCurrent().addWindow(new RepossessedPopupPanel(ContractWkfStatus.REP, quotation.getContract(),applicationPanel,applicationPanel.getApplicationTablePanel()));
        });


        btnBackPOS = new NativeButton(I18N.message("back.pos"));
        btnBackPOS.setIcon(FontAwesome.STEP_BACKWARD);
        btnBackPOS.addClickListener(this);

        btnBackAUC = new NativeButton(I18N.message("back.auc"));
        btnBackAUC.setIcon(FontAwesome.STEP_BACKWARD);
        btnBackAUC.addClickListener(this);

        btnReject = new NativeButton(I18N.message("reject"));
        btnReject.setIcon(FontAwesome.MINUS_CIRCLE);
        btnReject.addClickListener(this);

        btnActivate = new NativeButton(I18N.message("activate"));
        btnActivate.setIcon(new ThemeResource("../smt-default/icons/16/tick.png"));
        btnActivate.addClickListener(this);

        btnContractStartDate = new NativeButton("Insurance Start Date");
        btnContractStartDate.addClickListener(this);

        btnConTranSettlement = new NativeButton(I18N.message("confirm.transaction.settlement"));
        btnConTranSettlement.setIcon(new ThemeResource("../smt-default/icons/16/tick.png"));
        btnConTranSettlement.addClickListener(this);

        btnBackAppraisal = new NativeButton(I18N.message("back.appraisal"));
        btnBackAppraisal.setIcon(FontAwesome.STEP_BACKWARD);
        btnBackAppraisal.addClickListener(this);

        defaultNavigationPanel.addSaveClickListener(this);

        reportMenu = new MenuBar();
        reportMenu.addStyleName("menu-button");

        // If is admin and senior CO, no report menu
        if (!(ProfileUtil.isAdmin() || ProfileUtil.isSeniorCO())) {
            defaultNavigationPanel.addButton(reportMenu);
        }

        defaultNavigationPanel.addButton(btnCancel);
        defaultNavigationPanel.addButton(btnReject);
        defaultNavigationPanel.addButton(btnBackAUC);

        /*Hide Submit button when whe profile is ADMIN*/
        if (!(ProfileUtil.isAdmin() || ProfileUtil.isSeniorCO())) {
            defaultNavigationPanel.addButton(btnSubmit);
        }
        if(ProfileUtil.isSeniorCO()){
            defaultNavigationPanel.getSaveClickButton().setEnabled(false);
        }
        if(!ProfileUtil.isSeniorCO()){
            defaultNavigationPanel.addButton(btnBackPOS);
            defaultNavigationPanel.addButton(btnDecline);
        }

        defaultNavigationPanel.addButton(btnApprove);
        defaultNavigationPanel.addButton(btnReqRepossess);
        defaultNavigationPanel.addButton(btnActivate);
        defaultNavigationPanel.addButton(btnContractStartDate);
        defaultNavigationPanel.addButton(btnBackAppraisal);

        navigationPanel.addComponent(defaultNavigationPanel);
    }

    @Override
    protected com.vaadin.ui.Component createForm() {
        applicationTab = new TabSheet();
        contentLayout = new VerticalLayout();
        // if UW or UWS request field check

        historyStatusPanel = new HistoryStatusPanel();
        assetTabPanel = new AssetTabPanel();
        finProductPanel = new FinProductPanel();
        applicantDetailPanel = new ApplicantDetailPanel();
        guarantorPanel = new GuarantorPanel();
        documentsPanel = new DocumentsPanel();
        dealerPanel = new DealerPanel();
        commentTablePanel = new CommentTablePanel();
        fieldCheckPanel = new FieldCheckPanel();

        applicationTab.addTab(assetTabPanel, I18N.message("asset"));
        applicationTab.addTab(dealerPanel, I18N.message("dealer"));
        applicationTab.addTab(applicantDetailPanel, I18N.message("applicant"));
        applicationTab.addTab(guarantorPanel, I18N.message("guarantor"));
        applicationTab.addTab(documentsPanel, I18N.message("documents"));
        applicationTab.addTab(commentTablePanel, I18N.message("Comment"));
        applicationTab.addTab(fieldCheckPanel, I18N.message("field.check"));

        //displayTabs();
        applicationTab.addSelectedTabChangeListener(this);

        fieldCheckRequestPanel = new Panel();
        contentLayout.addComponent(fieldCheckRequestPanel);

        contentLayout.addComponent(applicationTab);

        return contentLayout;
    }

    @Override
    protected Quotation getEntity() {
        if (ProfileUtil.isDocumentController() && QuotationWkfStatus.WIV.equals(quotation.getWkfStatus())) {
            assetTabPanel.getAssetPanel().saveConfirmAssetInDocumentController();
            this.quotation = assetTabPanel.getAsset();

        }
        updateQuotationTab(applicationTab.getSelectedTab());
        return quotation;
    }

    protected void panelFieldCheck(Quotation quotation) {
        fieldCheckRequestLayout = new HorizontalLayout();
        fieldCheckRequestLayout.setMargin(true);
        cbSupportDecisions = new ArrayList<CheckBox>();

        //List<SupportDecision> supportDecisions = DataReference.getInstance().getSupportDecisions(QuotationWkfStatus.DEC);
        List<SupportDecision> supportDecisions = ENTITY_SRV.list(SupportDecision.class);

        List<QuotationSupportDecision> quotationSupportDecisions = quotation.getQuotationSupportDecisions();
        if (supportDecisions != null && !supportDecisions.isEmpty()) {
            for (SupportDecision supportDecision : supportDecisions) {
                CheckBox cbSupportDecision = new CheckBox(supportDecision.getDescEn());

                for (QuotationSupportDecision quotationSupportDecision : quotationSupportDecisions) {
                    Long supId = quotationSupportDecision.getSupportDecision().getId();
                    if (supportDecision.getId() == supId) {
                        cbSupportDecision.setData(supportDecision);
                        cbSupportDecision.setValue(true);
                        cbSupportDecision.setEnabled(false);
                        cbSupportDecisions.add(cbSupportDecision);
                        fieldCheckRequestLayout.addComponent(cbSupportDecision);
                        //fieldCheckRequestLayout.addComponent(ComponentFactory.getSpaceLayout(20, Unit.PIXELS));
                    }
                }

            }
            fieldCheckRequestPanel.setContent(fieldCheckRequestLayout);
            fieldCheckRequestPanel.setCaption(I18N.message("request.modification"));
            fieldCheckRequestPanel.addStyleName("panel-margin-bottom");
        }
    }

    protected Quotation getQuotationAfterValidation() {

        if (ProfileUtil.isPOS()) {
            // set first submission date when POS submit
            if (QuotationWkfStatus.QUO.equals(quotation.getWkfStatus())) {
                if (quotation.getFirstSubmissionDate() == null){
                    quotation.setFirstSubmissionDate(DateUtils.today());
                }
            }
            if (QuotationWkfStatus.ACS.equals(quotation.getWkfStatus())){
                if (quotation.getFirstApplyDate() == null){
                    quotation.setFirstApplyDate(DateUtils.today());
                }
            }
            SecUserDetail secUserDetail = getSecUserDetail();
            if (secUserDetail != null && secUserDetail.getDealer() != null) {
                quotation.setDealer(secUserDetail.getDealer());
            }
        }
        return quotation;
    }

    /*Update Application Tab*/
    private void updateQuotationTab(com.vaadin.ui.Component selectedTab) {
        if (selectedTab != null) {
            super.reset();
            if (selectedTab == assetTabPanel) {
                this.quotation = assetTabPanel.getAsset();
            }
            if(quotation.getWkfStatus().equals(QuotationWkfStatus.ACS) || ProfileUtil.isAdmin()){
                finProductPanel.getQuotation(quotation);
            }
            if (selectedTab == applicantDetailPanel) {
                Applicant applicant = quotation.getMainApplicant();
                if (applicant == null) {
                    quotation.setApplicant(new Applicant());
                    quotation.setMainApplicant(quotation.getApplicant());
                }
                applicantDetailPanel.getApplicant(quotation.getMainApplicant());
            }
            if (selectedTab == guarantorPanel) {
                guarantorPanel.getQuotationApplicant(quotation);
            }
            if (selectedTab == documentsPanel) {
                documentsPanel.getDocuments(quotation);
            }
            if (selectedTab == fieldCheckPanel) {
                fieldCheckPanel.getQuotation(quotation);
            }

            if (selectedTab == dealerPanel) {
                dealerPanel.getQuotation(quotation);
            }

            if (selectedTab == changeOfficerPanel) {
                changeOfficerPanel.getQuotation(quotation);
            }

            if (selectedTab == checkPanel) {
                checkPanel.assignValues(quotation);
            }

            validate();
        }
    }

    public void assignValues(Quotation quotation, boolean firstTabSelect) {
        this.quotation = quotation;
        if (quotation.getInsuranceStartDate() != null)  {
            dfInsuranceStartDate.setValue(quotation.getInsuranceStartDate());
        }
        assingeValueToTabs();
        displayTabs();
        if (ProfileUtil.isPOS() && (quotation.getWkfStatus().equals(QuotationWkfStatus.RFC)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.DEC)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.REJ))) {
            contentLayout.addComponentAsFirst(fieldCheckRequestPanel);
            this.panelFieldCheck(this.quotation);
        } else
            contentLayout.removeComponent(fieldCheckRequestPanel);

        this.assignMenus(this.quotation);
        if (quotation != null && quotation.getMainApplicant() != null) {
            navigationPanel.addComponent(quotationInfoPanel);
        } else {
            navigationPanel.removeComponent(quotationInfoPanel);
        }

        if (firstTabSelect) {
            applicationTab.setSelectedTab(assetTabPanel);
        }
    }

    /**
     * @param quotationId
     * @param firstTabSelect
     */
    public void assignValues(Long quotationId, boolean firstTabSelect) {
        super.reset();
        if (quotationId != null) {
            Quotation quotation = ENTITY_SRV.getById(Quotation.class, quotationId);
            assignValues(quotation, firstTabSelect);
        }
    }

    private void assingeValueToTabs() {

        if (assetTabPanel != null) {
            assetTabPanel.assignValues(quotation);
            applicationTab.setSelectedTab(assetTabPanel);
        }
        if (applicantDetailPanel != null) {
            applicantDetailPanel.assignValues(quotation);
        }

        if (documentsPanel != null) {
            documentsPanel.ApplicationFormPanel(this);
            documentsPanel.assignValues(quotation);
        }

        if (underwriterPanel != null) {
            underwriterPanel.assignValues(quotation);
            underwriterPanel.setApplicationPanel(applicationPanel);
        }

        if (dealerPanel != null)
            dealerPanel.assignValues(quotation);


        commentTablePanel.assignValues(quotation);

        if (fieldCheckPanel != null) {
            fieldCheckPanel.assignValues(quotation);
        }

        if (managementPanel != null) {
            managementPanel.assignValues(quotation);
        }

        if (guarantorPanel != null) {
            guarantorPanel.assignValues(quotation);
        }
        if (finProductPanel != null) {
            finProductPanel.setApplicationFormPanel(this);
            finProductPanel.assignValues(quotation);
        }

        if (changeOfficerPanel != null) {
            changeOfficerPanel.assignValues(quotation);

        }
        if (checkPanel != null) {
            checkPanel.assignValues(quotation);
        }

        displayContractInfo(quotationInfoPanel, quotation);

    }

    private void displayTabs() {

        if (quotation != null) {
            if (QuotationProfileUtils.isKubuta4Plus(quotation)) {
                removeGuarantorPanel();
            } else {

                //Manage tab financial product
                if (finProductPanel != null) {
                    applicationTab.removeComponent(finProductPanel);
                }

                if (QuotationProfileUtils.isFinancialPrdTabAvailable(quotation)) {
                    if (finProductPanel != null) {
                        applicationTab.addTab(finProductPanel, I18N.message("financial.product"), null, 1);
                    }
                }

                if (ProfileUtil.isUW() || ProfileUtil.isManager()) {
                    if (applicationTab.getTab(underwriterPanel) == null) {
                        underwriterPanel = new UnderwriterPanel(this);
                        applicationTab.addTab(underwriterPanel, I18N.message("underwriter"));
                    }
                } else {
                    if (applicationTab.getTab(underwriterPanel) != null) {
                        applicationTab.removeComponent(underwriterPanel);
                    }
                }

                if (ProfileUtil.isUW() || ProfileUtil.isManager()) {
                    if (applicationTab.getTab(checkPanel) == null) {
                        checkPanel = new CheckPanel();
                        applicationTab.addTab(checkPanel, I18N.message("check"));
                    }
                }

                if (ProfileUtil.isManager() || (ProfileUtil.isUnderwriterSupervisor2() && (quotation.getWkfStatus().equals(QuotationWkfStatus.APV)
                        || (quotation.getWkfStatus().equals(QuotationWkfStatus.AWS)
                        || (quotation.getWkfStatus().equals(QuotationWkfStatus.ACT)
                        || (quotation.getWkfStatus().equals(QuotationWkfStatus.AWT)
                        || (quotation.getWkfStatus().equals(QuotationWkfStatus.APS)))))))) {
                    if (applicationTab.getTab(managementPanel) == null) {
                        managementPanel = new ManagementPanel(this);
                        applicationTab.addTab(managementPanel, I18N.message("management"));
                    }
                } else {
                    if (applicationTab.getTab(managementPanel) != null) {
                        applicationTab.removeComponent(managementPanel);
                    }
                }

                if (applicationTab.getTab(historyStatusPanel) == null) {
                    applicationTab.addTab(historyStatusPanel, I18N.message("history.status"));
                }

                if (quotation != null && quotation.getFinancialProduct() != null) {
                    if (FinancialProductEntityField.FIN_CODE_EXISTING.equals(quotation.getFinancialProduct().getCode())) {
                        removeGuarantorPanel();
                    } else {
                        addGuarantorPanel();
                    }
                }

                if (QuotationProfileUtils.isChangeOfficeAvailable(quotation)) {
                    if (applicationTab.getTab(changeOfficerPanel) == null) {
                        changeOfficerPanel = new ChangeOfficerPanel();
                        applicationTab.addTab(changeOfficerPanel, I18N.message("change.officer"));
                    }
                }

            }
        }

    }

    /*** ASSIGN MENU
     */
    public void assignMenus(Quotation quotation) {

        btnSubmit.setVisible(ProfileUtil.isManager() || ProfileUtil.isUW(ProfileUtil.getCurrentUser()) ? false : QuotationProfileUtils.isSubmitAvailable(quotation));

        if (quotation != null && !QuotationProfileUtils.isKubuta4Plus(quotation)) {
            btnBackAppraisal.setVisible(false);
            btnApprove.setVisible(ProfileUtil.isCollectionSupervisor(ProfileUtil.getCurrentUser()) && quotation.getWkfStatus().equals(QuotationWkfStatus.AUP));
            defaultNavigationPanel.getSaveClickButton().setVisible(quotation.getWkfStatus().equals(QuotationWkfStatus.REJ)
                    || quotation.getWkfStatus().equals(QuotationWkfStatus.REU)
                    || quotation.getWkfStatus().equals(QuotationWkfStatus.DEC) ? false : QuotationProfileUtils.isSaveUpdateAvailable(quotation));
        } else {
            BaseRestrictions<GroupCommittee> restrictions = new BaseRestrictions<>(GroupCommittee.class);
            restrictions.addCriterion(Restrictions.eq("secUser.id", ProfileUtil.getCurrentUser().getId()));
            List<GroupCommittee> list = ENTITY_SRV.list(restrictions);
            if (list != null && !list.isEmpty() && ProfileUtil.isManager()) {
                if (quotation.getSecGroupCommittee() != null) {
                    boolean isAppraisal = false;
                    GroupCommittee groupCommittee = quotation.getSecGroupCommittee();
                    if (groupCommittee.isAppraisalHasAssetRange(quotation.getAsset().getAssetRange())) {
                        isAppraisal = !groupCommittee.getSecUser().getId().equals(ProfileUtil.getCurrentUser().getId());
                    }
                    btnApprove.setVisible(isAppraisal);
                    btnBackAppraisal.setVisible(isAppraisal);
                    defaultNavigationPanel.getSaveClickButton().setVisible(isAppraisal);
                    defaultNavigationPanel.addButton(new NativeButton());
                }else {
                    for (GroupCommittee committee : list){
                        if (!committee.isAppraisalHasAssetRange(quotation.getAsset().getAssetRange())){
                            btnApprove.setVisible(false);
                            btnBackAppraisal.setVisible(false);
                            defaultNavigationPanel.getSaveClickButton().setVisible(false);
                            defaultNavigationPanel.addButton(new NativeButton());
                        }
                    }
                }
            } else {
                btnApprove.setVisible(false);
                btnBackAppraisal.setVisible(false);
                defaultNavigationPanel.getSaveClickButton().setVisible(!ProfileUtil.isManager()? true : false);
            }
        }

        btnReqRepossess.setVisible(ProfileUtil.isCollectionSupervisor(ProfileUtil.getCurrentUser()) && quotation.getWkfStatus().equals(QuotationWkfStatus.ACT));
        btnBackPOS.setVisible(ProfileUtil.isManager() || ProfileUtil.isUW(ProfileUtil.getCurrentUser()) || ProfileUtil.isPOS() ? false : QuotationProfileUtils.isBackPOSAvailable(quotation));
        btnBackAUC.setVisible(ProfileUtil.isCollectionSupervisor(ProfileUtil.getCurrentUser()) && quotation.getWkfStatus().equals(QuotationWkfStatus.AUP));
        btnDecline.setVisible(QuotationProfileUtils.isDeclineAvailable(quotation));
        btnCancel.setVisible(QuotationProfileUtils.isCancelAvailable(quotation));
        btnReject.setVisible(ProfileUtil.isUnderwriterSupervisor(ProfileUtil.getCurrentUser())
                && quotation.getWkfStatus().equals(QuotationWkfStatus.REU) ? false : QuotationProfileUtils.isRejectAvailable(quotation));

        btnActivate.setVisible(QuotationProfileUtils.isActivateAvailable(quotation));
        reportMenu.setVisible(QuotationProfileUtils.isReportsAvailable(quotation));
        btnConTranSettlement.setVisible(QuotationProfileUtils.isTransactionSettlementAvailable(quotation));

        btnContractStartDate.setVisible(ProfileUtil.isDocumentController());
    }

    @Override
    public void saveEntity() {
        try {
            quotation = QUO_SRV.saveOrUpdateQuotation(getEntity());
            assignMenus(quotation);
            navigationPanel.removeAllComponents();
            applicationPanel.setAdd(false);
            displayContractInfo(quotationInfoPanel, quotation);
            navigationPanel.addComponent(defaultNavigationPanel);
            navigationPanel.addComponent(quotationInfoPanel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addGuarantorPanel() {
        if (applicationTab.getTab(guarantorPanel) == null) {
            guarantorPanel = new GuarantorPanel();
        }
        applicationTab.addTab(guarantorPanel, I18N.message("guarantor"), null, 3);
    }

    public void removeGuarantorPanel() {
        applicationTab.removeComponent(guarantorPanel);
    }

    /**
     * Reset
     */
    @Override
    public void reset() {
        super.reset();
        assetTabPanel.reset();
        finProductPanel.reset();
        applicantDetailPanel.reset();
        documentsPanel.reset();
        fieldCheckPanel.reset();
        applicationTab.setSelectedTab(assetTabPanel);
        markAsDirty();
    }

    /**
     * @return
     */
    @Override
    public boolean validate() {
        if (isSelectedTab(assetTabPanel))
            errors.addAll(assetTabPanel.isValid());
        else if (isSelectedTab(finProductPanel))
            errors.addAll(finProductPanel.isValid());
        else if (isSelectedTab(applicantDetailPanel))
            errors.addAll(applicantDetailPanel.isValid());
        else if (isSelectedTab(guarantorPanel))
            errors.addAll(guarantorPanel.fullValidate());
        else if (isSelectedTab(documentsPanel))
            errors.addAll(documentsPanel.fullValidate());
        else if (isSelectedTab(dealerPanel))
            errors.addAll(dealerPanel.fullValidate());
        return errors.isEmpty();
    }

    /*New Changes*/
    @Override
    public void selectedTabChange(TabSheet.SelectedTabChangeEvent event) {
        // persist data when changing selected tab
        AbstractTabPanel selectedTab = (AbstractTabPanel) event.getTabSheet().getSelectedTab();
        //updateQuotationTab(selectedTab);
        applicantDetailPanel.getIdentityPanels().getAddress();
        if (applicantDetailPanel.getIdentityPanels().getAddress() != null) {
            guarantorPanel.setAddress(applicantDetailPanel.getIdentityPanels().getAddress());
            if (quotation != null) {
                if (quotation.getMainApplicant() != null) {
                    guarantorPanel.setMainApplicant(quotation.getMainApplicant());
                }
            }

        }
        if (selectedTab == historyStatusPanel) {
            historyStatusPanel.assignValues(quotation);
        } else if (selectedTab == assetTabPanel) {
            assetTabPanel.assignValues(quotation);
        } else if (selectedTab == applicantDetailPanel) {
            applicantDetailPanel.assignValues(quotation);
        } else if (selectedTab == guarantorPanel) {
            guarantorPanel.assignValues(quotation);
        } else if (selectedTab == documentsPanel) {
            documentsPanel.assignValues(quotation);
        } else if (selectedTab == dealerPanel) {
            dealerPanel.assignValues(quotation);
        } else if (selectedTab == commentTablePanel) {
            commentTablePanel.assignValues(quotation);
        } else if (selectedTab == fieldCheckPanel) {
            fieldCheckPanel.assignValues(quotation);
        } else if (selectedTab == finProductPanel) {
            super.reset();
            finProductPanel.assignValues(quotation, this);
        } else if (selectedTab == underwriterPanel) {
            underwriterPanel.assignValues(quotation);
        } else if (selectedTab == managementPanel) {
            managementPanel.assignValues(quotation);
        } else if (selectedTab == changeOfficerPanel) {
            changeOfficerPanel.assignValues(quotation);
        } else if (selectedTab == checkPanel) {
            checkPanel.assignValues(quotation);
        }
        selectedTab.updateNavigationPanel(navigationPanel, defaultNavigationPanel);
        displayContractInfo(quotationInfoPanel, quotation);
        navigationPanel.addComponent(quotationInfoPanel);
    }

    private boolean isSelectedTab(com.vaadin.ui.Component selectedTab) {
        return (applicationTab.getSelectedTab() == selectedTab);
    }

    @Override
    public void buttonClick(Button.ClickEvent event) {
        try {
            NativeButton btn = (NativeButton) event.getSource();
            EWkfStatus newQuotationStatus = null;
            boolean forManager = false;
            if (btn == btnBackPOS) {
                if (QuotationProfileUtils.isKubuta4Plus(quotation)) {
                    newQuotationStatus = QuotationWkfStatus.ACS;
                } else {
                    newQuotationStatus = QuotationWkfStatus.QUO;
                }
                if (ProfileUtil.isDocumentController()){
                    newQuotationStatus = QuotationWkfStatus.APV;
                }else {
                    newQuotationStatus = QuotationWkfStatus.QUO;
                }
                showCommentFormPanel(newQuotationStatus, event.getButton().getCaption(), forManager);

            } else if (btn == btnBackAUC) {
                newQuotationStatus = QuotationWkfStatus.APP;
                showCommentFormPanel(newQuotationStatus, event.getButton().getCaption(), forManager);
            } else if (btn == btnApprove) {
                if (QuotationProfileUtils.isKubuta4Plus(quotation)) {

                    BaseRestrictions<GroupCommittee> secUserGroupCommittee = new BaseRestrictions<>(GroupCommittee.class);
                    secUserGroupCommittee.addCriterion(Restrictions.eq("secUser.id", ProfileUtil.getCurrentUser().getId()));
                    GroupCommittee committee = ENTITY_SRV.list(secUserGroupCommittee).get(0);

                    if (quotation.getSecGroupCommittee() == null) {
                        quotation.setSecGroupCommittee(committee);
                        quotation.setNumberApproved(1);
                        newQuotationStatus = QuotationWkfStatus.PAC;
                        ENTITY_SRV.saveOrUpdate(quotation);
                    } else if (quotation.getNumberApproved() > 0) {
                        newQuotationStatus = QuotationWkfStatus.AUP;
                    }

                    showCommentFormPanel(newQuotationStatus, event.getButton().getCaption(), forManager);

                } else {
                    newQuotationStatus = QuotationWkfStatus.ACS;
                    showCommentFormPanel(newQuotationStatus, event.getButton().getCaption(), forManager);
                }
            } else if (btn == btnReqRepossess) {

            } else if (btn == btnCancel) {
                newQuotationStatus = QuotationWkfStatus.CAN;
                showCommentFormPanel(newQuotationStatus, event.getButton().getCaption(), forManager);
            } else if (btn == btnDecline) {
                newQuotationStatus = QuotationWkfStatus.DEC;
                showCommentFormPanel(newQuotationStatus, event.getButton().getCaption(), forManager);
            } else if (btn == btnSubmit) {
                //saveButtonClick(null);
                quotation = getEntity();
                AssetPanel assetPanel = assetTabPanel.getAssetPanel();
                validationBeforeSubmmit(quotation);
                if (ProfileUtil.isDocumentController()) {
                    if (assetPanel != null && assetPanel.isMatchDC()) {
                        quotation = getQuotationAfterValidation();
                        QUO_SRV.submitQuotation(quotation);
                        applicationPanel.displayApplicationTablePanel();
                    }
                } else if (ProfileUtil.isAuctionController()) {
                    newQuotationStatus = QuotationWkfStatus.AUP;
                    quotation = getQuotationAfterValidation();
                    showCommentFormPanel(newQuotationStatus, event.getButton().getCaption(), forManager);
                } else {
                    quotation = getQuotationAfterValidation();
                    QUO_SRV.submitQuotation(quotation);
                    applicationPanel.displayApplicationTablePanel();
                }
            } else if (btn == btnActivate) {
                performActivateContract();
            } else if (btn == btnConTranSettlement) {
                processTransferSetlement();
                Notification.show("", "Application has been transfer settlement to customer... !", Notification.Type.HUMANIZED_MESSAGE);
                applicationPanel.displayApplicationTablePanel();
            } else if (btn == btnContractStartDate) {
                insuranceStartDate();
            } else if (btn == btnBackAppraisal) {
                newQuotationStatus = QuotationWkfStatus.QUO;
                showCommentFormPanel(newQuotationStatus, event.getButton().getCaption(), forManager);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void insuranceStartDate() {
        final Window window = new Window("Insurance");
        window.setModal(true);
        window.setResizable(false);
        window.setWidth(320, Unit.PIXELS);
        window.setHeight(140, Unit.PIXELS);
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        verticalLayout.setSpacing(true);
        final Button btnSaveInsuranceDate = new NativeButton("Validate");
        btnSaveInsuranceDate.setIcon(new ThemeResource("../smt-default/icons/16/tick.png"));
        this.dfInsuranceStartDate.setWidth(95, Unit.PIXELS);
        this.dfInsuranceStartDate.setValue(DateUtils.today());
        FormLayout formDetailPanel = new FormLayout();
        formDetailPanel.setWidth("100%");
        formDetailPanel.addComponent(this.dfInsuranceStartDate);
        verticalLayout.addComponents(formDetailPanel, btnSaveInsuranceDate);
        window.setContent(verticalLayout);
        UI.getCurrent().addWindow(window);

        btnSaveInsuranceDate.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                quotation.setInsuranceStartDate(dfInsuranceStartDate.getValue());
                QUO_SRV.saveOrUpdateQuotation(quotation);
                Notification.show("Insurance Start Date Saved");
                window.close();
            }
        });
    }


    public void setApplicationPanel(ApplicationPanel applicationPanel) {
        this.applicationPanel = applicationPanel;
    }

    public void validationBeforeSubmmit(Quotation quotation) throws ValidationFieldsException, InvalidQuotationException, IntrospectionException, InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        ValidationFields validationFields = new ValidationFields();
        validationFields.add(!assetTabPanel.getAssetPanel().isValid().isEmpty(), DomainType.ASS, null, "asset", assetTabPanel.getAssetPanel().isValid());
        validationFields.add(!assetTabPanel.getAppraisalPanel().isValid().isEmpty(), DomainType.ASS_APP, null, "asset.appraisal", assetTabPanel.getAppraisalPanel().isValid());
       /* Check status for CMO*/
        if (quotation.getWkfStatus().equals(QuotationWkfStatus.ACS)) {

            if (quotation != null && quotation.getFinancialProduct() != null) {
                if (FinancialProductEntityField.FIN_CODE_EXISTING.equals(quotation.getFinancialProduct().getCode())) {
                    /*Exists Leasing*/
                    validationFields.add(!applicantDetailPanel.getEmployeePanels().isValid().isEmpty(), DomainType.EMP, null, "employment", applicantDetailPanel.getEmployeePanels().isValid());
                    validationFields.add(!applicantDetailPanel.getIdentityPanelsMFP().isValid().isEmpty(), DomainType.IDE, null, "identity", applicantDetailPanel.getIdentityPanelsMFP().isValid());
                    validationFields.add(!applicantDetailPanel.getOtherInfoPanel().isValid().isEmpty(), DomainType.OTH_INF, null, "other.information", applicantDetailPanel.getOtherInfoPanel().isValid());
                } else {
                    /*Public Leasing*/
                    validationFields.add(!applicantDetailPanel.getIdentityPanelsMFP().isValid().isEmpty(), DomainType.IDE, null, "identity", applicantDetailPanel.getIdentityPanelsMFP().isValid());
                    validationFields.add(!applicantDetailPanel.getEmployeePanels().isValid().isEmpty(), DomainType.EMP, null, "employment", applicantDetailPanel.getEmployeePanels().isValid());
                    validationFields.add(!applicantDetailPanel.getOtherInfoPanel().isValid().isEmpty(), DomainType.OTH_INF, null, "other.information", applicantDetailPanel.getOtherInfoPanel().isValid());
                    validationFields.add(!guarantorPanel.getIdentityPanel().fullValidate().isEmpty(), DomainType.IDE, null, "Gidentit", guarantorPanel.getIdentityPanel().fullValidate());
                    validationFields.add(!guarantorPanel.getCurrentEmploymentPanel().isValid().isEmpty(), DomainType.EMP, null, "Gemployment", guarantorPanel.getCurrentEmploymentPanel().isValid());
                    validationFields.add(!guarantorPanel.getOtherInformationPanel().fullValidate().isEmpty(), DomainType.OTH_INF, null, "Gother.information", guarantorPanel.getOtherInformationPanel().fullValidate());
                }
            }

            validationFields.add(!dealerPanel.fullValidate().isEmpty(), DomainType.DEA, null, "dealer", dealerPanel.fullValidate());
            validationFields.add(!documentsPanel.fullValidate().isEmpty(), DomainType.DOC, null, "document", documentsPanel.fullValidate());
        }

        if (ProfileUtil.isPOS() && quotation.getWkfStatus().equals(QuotationWkfStatus.APV)) {
            validationFields.add(!applicantDetailPanel.getLoanTabPanel().isValid().isEmpty(), DomainType.IDE, null, "loan.information", applicantDetailPanel.getLoanTabPanel().isValid());
        }
        if (QuotationProfileUtils.isFinancialPrdTabAvailable(quotation) && quotation.getWkfStatus().equals(QuotationWkfStatus.ACS)) {
            validationFields.add(!finProductPanel.isValid().isEmpty(), DomainType.FIN, null, "financial.product", finProductPanel.isValid());
        }
        if (ProfileUtil.isDocumentController() && quotation.getWkfStatus().equals(QuotationWkfStatus.WIV)) {
            validationFields.add(!this.isValidInsurance().isEmpty(), DomainType.INS, null, "Insurance Start", this.isValidInsurance());
        }

        if (!validationFields.getErrorMessages().isEmpty()) {
            new CustomPopUp(validationFields.getMessage());
            throw new ValidationFieldsException(validationFields.getErrorMessages());
            //new Notification("<h2 style='display:inline'>Mandatory fields are required</h2>", validationFields.getMessage(), Notification.Type.ERROR_MESSAGE, true).show(Page.getCurrent());
        }
        if (!quotation.isValid()) throw new InvalidQuotationException();

    }

    /**
     * performActivateContract
     */
    private void performActivateContract()
    {     final Window window = new Window(I18N.message("activation.process"));
            window.setModal(true);

            VerticalLayout contentLayout = new VerticalLayout();
            contentLayout.setSpacing(true);

            FormLayout formLayout = new FormLayout();
            formLayout.setMargin(true);
            formLayout.setSpacing(true);
            final DateField dfContractStartDate = ComponentFactory.getAutoDateField(I18N.message("contract.start.date"), true);
            final DateField dfFirstPaymentDate = ComponentFactory.getAutoDateField(I18N.message("first.payment.date"), true);

            int hour = DateUtils.getHour(DateUtils.today());
            java.util.Calendar cal = java.util.Calendar.getInstance();

            if (cal.get(java.util.Calendar.AM_PM) == java.util.Calendar.PM) {
                if (hour >= 1) {
                    Date tomorrow = org.apache.commons.lang.time.DateUtils.addDays(DateUtils.today(), 1);

                    dfContractStartDate.setValue(tomorrow);
                    dfContractStartDate.setEnabled(false);
                    dfFirstPaymentDate.setValue(DateUtils.addMonthsDate(tomorrow, 1));

                }
            } else {
                dfContractStartDate.setValue(DateUtils.today());
                dfContractStartDate.setEnabled(false);
                dfFirstPaymentDate.setValue(DateUtils.addMonthsDate(DateUtils.today(), 1));
            }
            dfContractStartDate.setWidth(95, Unit.PIXELS);
            dfFirstPaymentDate.setWidth(95, Unit.PIXELS);

            formLayout.addComponent(dfContractStartDate);
            formLayout.addComponent(dfFirstPaymentDate);

            final Button btnSave = new NativeButton(I18N.message("activate"));
            btnSave.setIcon(new ThemeResource("../smt-default/icons/16/tick.png"));
            btnSave.addClickListener(new Button.ClickListener() {
                private static final long serialVersionUID = 785084307126131618L;

                @Override
                public void buttonClick(Button.ClickEvent event) {
                    SettingConfig settingConfig = ENTITY_SRV.getByCode(SettingConfig.class, "max.first.payment.date.mfp");
                    Integer maxFirstPaymentDay = 45;
                    if (settingConfig != null && org.apache.commons.lang3.StringUtils.isNotEmpty(settingConfig.getValue())) {
                        maxFirstPaymentDay = Integer.parseInt(settingConfig.getValue().toString());
                    }

                    Date firstPaymentDate = DateUtils.getDateAtBeginningOfDay(dfFirstPaymentDate.getValue());
                    Date maxFirstPaymentDate = DateUtils.addDaysDate(DateUtils.getDateAtBeginningOfDay(dfContractStartDate.getValue()), maxFirstPaymentDay);
                    checkDateToActivate(firstPaymentDate, maxFirstPaymentDate, dfContractStartDate, dfFirstPaymentDate, window);
                    displayContractInfo(quotationInfoPanel, quotation);
                }
            });

            Button btnCancel = new NativeButton(I18N.message("close"), new Button.ClickListener() {
                private static final long serialVersionUID = -2627907504174196770L;

                public void buttonClick(Button.ClickEvent event) {
                    window.close();
                }
            });
            btnCancel.setIcon(new ThemeResource("../smt-default/icons/16/delete.png"));

            NavigationPanel navigationPanel = new NavigationPanel();
            navigationPanel.addButton(btnSave);
            navigationPanel.addButton(btnCancel);

            contentLayout.addComponent(navigationPanel);
            contentLayout.addComponent(formLayout);
            window.setContent(contentLayout);
            window.setWidth("350px");
            window.setHeight("200px");
            UI.getCurrent().addWindow(window);
        }

    private void checkDateToActivate(Date firstPaymentDate, Date maxFirstPaymentDate, DateField dfContractStartDate, DateField dfFirstPaymentDate, Window window) {
        if (DateUtils.getDateWithoutTime(firstPaymentDate).compareTo(DateUtils.getDateWithoutTime(maxFirstPaymentDate)) <= 0) {
            if (DateUtils.getDay(firstPaymentDate) >= 29) {
                MessageBox mb = new MessageBox(UI.getCurrent(), "400px", "160px", I18N.message("information"),
                        MessageBox.Icon.ERROR, I18N.message("first.payment.date.should.be.less.than.28"),
                        Alignment.MIDDLE_RIGHT, new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, I18N.message("ok")));
                mb.show();
            } else if (dfContractStartDate.getValue().compareTo(dfFirstPaymentDate.getValue()) > 0) {
                MessageBox mb = new MessageBox(UI.getCurrent(), "400px", "160px", I18N.message("information"),
                        MessageBox.Icon.ERROR, I18N.message("first.payment.date.should.be.greater.than.or.equals.contract.start.date"),
                        Alignment.MIDDLE_RIGHT, new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, I18N.message("ok")));
                mb.show();
            } else {

                QUO_SRV.activateQuotation(quotation.getId(), dfContractStartDate.getValue(), dfFirstPaymentDate.getValue());

                String messageDisplay = I18N.message("success.activate.message", new String[]{quotation.getExternalReference()});
                if(quotation.getInsuranceStartDate() != null){
                    ContractMigrationService contractMigrationService = SpringUtils.getBean(ContractMigrationService.class);
                    messageDisplay += "\n" + contractMigrationService.migrationToEinsurance(quotation.getId());
                }

                Notification notification = new Notification("", Notification.Type.HUMANIZED_MESSAGE);

                window.close();
                notification.setDescription(messageDisplay);
                notification.setDelayMsec(5000);
                notification.show(Page.getCurrent());
                applicationPanel.displayApplicationTablePanel();
            }

        } else {
            MessageBox mb = new MessageBox(UI.getCurrent(), "400px", "160px", I18N.message("information"),
                    MessageBox.Icon.ERROR, I18N.message("first.payment.date.should.be.less.than.max.first.data.payment", " " + DateUtils.getDateLabel(maxFirstPaymentDate, DateUtils.DEFAULT_DATE_FORMAT)),
                    Alignment.MIDDLE_RIGHT, new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, I18N.message("ok")));
            mb.show();
        }

    }

    private void displayContractInfo(Panel contractPanel, final Quotation quotation) {
        String template = "contractInfo";
        Applicant mainApplicant = quotation.getMainApplicant();
        Contract contract = quotation.getContract();
        InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
        CustomLayout inputFieldLayout = null;
        VerticalLayout verticalLayout = new VerticalLayout();
        try {
            inputFieldLayout = new CustomLayout(layoutFile);
        } catch (IOException e) {
            Notification.show("Could not locate template " + template, e.getMessage(), Notification.Type.ERROR_MESSAGE);
        }
        inputFieldLayout.setSizeFull();
        inputFieldLayout.addComponent(new Label(I18N.message("applicant")), "lblApplicant");
        inputFieldLayout.addComponent(new Label(mainApplicant != null ? mainApplicant.getNameEn() : ""), "lnkApplicant");
        inputFieldLayout.addComponent(new Label(I18N.message("reference")), "lblReference");
        if (quotation.getReference() != null) {
            inputFieldLayout.addComponent(new Label("<b>" + quotation.getReference() + "</b>", ContentMode.HTML), "lblReferenceValue");
        } else {
            inputFieldLayout.addComponent(new Label("<b>" + "N/A" + "</b>", ContentMode.HTML), "lblReferenceValue");
        }
        if (quotation.getContract() != null) {
            inputFieldLayout.addComponent(new Label(I18N.message("startdate")), "lblStartDate");
            inputFieldLayout.addComponent(new Label("<b>" + DateUtils.date2StringDDMMYYYY_SLASH(contract.getStartDate()) + "</b>", ContentMode.HTML), "lblStartDateValue");
            inputFieldLayout.addComponent(new Label(I18N.message("enddate")), "lblEndDate");
            inputFieldLayout.addComponent(new Label("<b>" + DateUtils.date2StringDDMMYYYY_SLASH(contract.getEndDate()) + "</b>", ContentMode.HTML), "lblEndDateValue");
            inputFieldLayout.addComponent(new Label(I18N.message("penalty.rule")), "lblPenaltyRule");
            inputFieldLayout.addComponent(new Label(contract.getPenaltyRule() != null ? contract.getPenaltyRule().getDescEn() : " "), "txtPenaltyRule");
        } else {
            inputFieldLayout.addComponent(new Label(I18N.message("startdate")), "lblStartDate");
            inputFieldLayout.addComponent(new Label("<b>" + DateUtils.date2StringDDMMYYYY_SLASH(quotation.getCreateDate()) + "</b>", ContentMode.HTML), "lblStartDateValue");
            inputFieldLayout.addComponent(new Label(I18N.message("enddate")), "lblEndDate");
            inputFieldLayout.addComponent(new Label("<b>" + "N/A" + "</b>", ContentMode.HTML), "lblEndDateValue");
            inputFieldLayout.addComponent(new Label(I18N.message("penalty.rule")), "lblPenaltyRule");
            inputFieldLayout.addComponent(new Label("<b>" + "N/A" + "</b>", ContentMode.HTML), "txtPenaltyRule");
        }

        inputFieldLayout.addComponent(new Label(I18N.message("guarantor")), "lblGuarantor");
        if (quotation.getGuarantor() != null) {
            inputFieldLayout.addComponent(new Label(quotation.getGuarantor() != null ? quotation.getGuarantor().getNameEn() : ""), "lnkGuarantor");
        } else {
            inputFieldLayout.addComponent(new Label("N/A"), "lnkGuarantor");
        }
        inputFieldLayout.addComponent(new Label(I18N.message("outstanding")), "lblOutstanding");
        inputFieldLayout.addComponent(new Label(I18N.message("status")), "lblStatus");
        inputFieldLayout.addComponent(new Label(quotation.getWkfStatus().getDesc()), "txtStatus");
        inputFieldLayout.addComponent(new Label(I18N.message("quotation.ratio")), "lblRatio");
        double applicantRatio = quotationService.calculateApplicantRatio(quotation);
        inputFieldLayout.addComponent(new Label("<b> <font color=\"" + quotationService.getRatioColor(applicantRatio) + "\">" + AmountUtils.format(applicantRatio) + "</font></b>", ContentMode.HTML), "lblRatioValue");

            /* Amount outstanding = contractService.getRealOutstanding(DateUtils.todayH00M00S00(), quotation.getContract().getId());*/

        inputFieldLayout.addComponent(new Label(AmountUtils.format(INSTALLMENT_SERVICE_MFP.getOutStanding(quotation))), "txtOutstanding");
        if (!applicationPanel.getAdd()) {
            verticalLayout.addComponent(inputFieldLayout);
        }
        contractPanel.setContent(verticalLayout);

    }

    private SecUserDetail getSecUserDetail() {
        SecUser secUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return ENTITY_SRV.getByField(SecUserDetail.class, "secUser.id", secUser.getId());
    }

    private void processTransferSetlement() {
        Contract contract = quotation.getContract();
        if (contract != null && contract.getNextWkfStatuses() != null && !contract.getNextWkfStatuses().isEmpty()) {
            logger.debug("> process perform transfer setlement ....");
            this.saveEntity();
            CONT_SRV.changeContractStatus(contract, contract.getNextWkfStatuses().get(0));
        } else {
            logger.debug("> there are no contract_old for process perform transfer setlement ....");
        }
    }

    public Quotation getQuotation() {
        return this.quotation;
    }

    public ApplicationPanel getApplicationPanel() {
        return applicationPanel;
    }

    public void clearErrorMessage() {
        if (this.errors != null && this.errors.size() > 1) {
            this.errors.clear();
        }
    }

    public void setError(List<String> errors) {
        this.errors = errors;
    }

    public List<String> isValidInsurance() {
        super.reset();
        if (quotation.getInsuranceStartDate() == null) {
            dfInsuranceStartDate.setValue(null);
            checkMandatoryDateField(dfInsuranceStartDate, "Insurance Date Required");
        }
        return errors;
    }

    public void showCommentFormPanel(EWkfStatus newStatus, String caption, final boolean forManager) {
        CommentFormPanel commentFormPanel = new CommentFormPanel(quotation, newStatus, forManager, new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (caption.equals(I18N.message("back.auc")))
                    QUO_SRV.changeQuotationStatus(quotation, QuotationWkfStatus.APP);
                else if (caption.equals(I18N.message("decline")))
                    QUO_SRV.decline(quotation);
                else if (caption.equals(I18N.message("approve"))) {
                    QUO_SRV.changeQuotationStatus(quotation, newStatus);
                    quotation.setDecline(true);
                } else if (caption.equals(I18N.message("back.pos"))) {
                    QUO_SRV.changeQuotationStatus(quotation, newStatus);
                    quotation.setDecline(true);
                    Notification.show("", "Application was backed to POS.", Notification.Type.HUMANIZED_MESSAGE);
                } else if (caption.equals(I18N.message("cancel"))) {
                    QUO_SRV.changeQuotationStatus(quotation, newStatus);
                    Notification.show("", "Cancel", Notification.Type.HUMANIZED_MESSAGE);
                } else if (caption.equals("Submit")) {
                    QUO_SRV.submitQuotation(quotation);
                }else if (caption.equals(I18N.message("back.appraisal"))){
                    quotation.setSecGroupCommittee(null);
                    quotation.setNumberApproved(0);
                    QUO_SRV.changeQuotationStatus(quotation, QuotationWkfStatus.QUO);
                }

                applicationPanel.displayApplicationTablePanel();
            }
        });
        commentFormPanel.setWidth(700, Unit.PIXELS);
        commentFormPanel.setHeight(350, Unit.PIXELS);
        commentFormPanel.setCaption(caption);
        UI.getCurrent().addWindow(commentFormPanel);
    }

    public AssetPanel getAssetPanel(){
        return assetTabPanel.getAssetPanel();
    }
}
