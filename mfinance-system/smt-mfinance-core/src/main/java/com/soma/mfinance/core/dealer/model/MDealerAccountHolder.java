package com.soma.mfinance.core.dealer.model;

import org.seuksa.frmk.model.entity.MEntityRefA;

/**
 * Meta data of DealerAccountHolder
 * @author kimsuor.seang
 */
public interface MDealerAccountHolder extends MEntityRefA {

	public final static String DEALER = "dealer";
	public final static String ACCOUNTHOLDERID = "accountHolder";
	
}
