package com.soma.mfinance.core.application.historyStatus;

import com.soma.common.app.workflow.service.WkfHistoryItemRestriction;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationWkfHistoryItem;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.EntityColumnRenderer;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Component;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.exception.DaoException;

import java.util.*;

/**
 * Created by sr.soth on 5/11/2017.
 */

public class HistoryStatusPanel extends AbstractTabPanel implements FinServicesHelper {

    private List<ColumnDefinition> columnDefinitions;
    private SimplePagedTable<QuotationWkfHistoryItem> pagedTable;
    private Item selectedItem;
    private Quotation quotation;

    @Override
    protected Component createForm() {
        this.columnDefinitions = createColumnDefinitions();
        pagedTable = new SimplePagedTable<>(this.columnDefinitions);
        // pagedTable.addItemClickListener(this);

        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setSpacing(true);
        // contentLayout.addComponent(navigationPanel);
        contentLayout.addComponent(pagedTable);
        contentLayout.addComponent(pagedTable.createControls());
        return contentLayout;
    }

    protected List<ColumnDefinition> createColumnDefinitions() {
        List<ColumnDefinition> columnDefinitions = new ArrayList<>();
        columnDefinitions.add(new ColumnDefinition("id", "ID", Long.class, Table.Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("oldValue", "FROM", String.class, Table.Align.LEFT, 250));
        columnDefinitions.add(new ColumnDefinition("newValue", "TO", String.class, Table.Align.LEFT, 250));
        columnDefinitions.add(new ColumnDefinition("changeDate", "DATE", String.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition("createUser", "USER CREATE", String.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition("profile", "PROFILE", String.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition("processTimeCustom", "PROCESSING TIME", String.class, Table.Align.LEFT, 150));
        return columnDefinitions;
    }

    private IndexedContainer getIndexedContainer(Long quo_id) {

        IndexedContainer indexedContainer = new IndexedContainer();
        try {

            for (ColumnDefinition column : this.columnDefinitions) {
                indexedContainer.addContainerProperty(column.getPropertyId(), column.getPropertyType(), null);
            }

            WkfHistoryItemRestriction restrictions = new WkfHistoryItemRestriction(QuotationWkfHistoryItem.class);
            restrictions.setEntityId(quo_id);
            restrictions.setPropertyName("wkfStatus");
            List<QuotationWkfHistoryItem> wkfBaseHistoryItems = WKF_SRV.getHistories(restrictions);

            Map<Integer, QuotationWkfHistoryItem> maps = new HashMap<>();
            Integer i = 0;
            Date prevDate = null;
            for (QuotationWkfHistoryItem historyItem : wkfBaseHistoryItems) {
                maps.put(i++, historyItem);

                QuotationWkfHistoryItem prevItem = maps.get(i - 2);
                if (prevItem != null) {
                    prevDate = prevItem.getChangeDate();
                }

                Item item = indexedContainer.addItem(historyItem.getId());
                item.getItemProperty("id").setValue(historyItem.getId());
                item.getItemProperty("oldValue").setValue(historyItem.getOldValue());
                item.getItemProperty("newValue").setValue(historyItem.getNewValue());
                item.getItemProperty("changeDate").setValue(DateUtils.formatDate(historyItem.getChangeDate(), DateUtils.FORMAT_YYYYMMDD_HHMMSS_SLASH));
                item.getItemProperty("createUser").setValue(historyItem.getCreateUser());
                //item.getItemProperty("profile").setValue(historyItem.getSecUserProfile());
                item.getItemProperty("processTimeCustom").setValue(new ProcessTimeColumnRender(historyItem.getChangeDate(), prevDate).getValue());
            }
        } catch (DaoException e) {
            logger.error("DaoException", e);
        }
        return indexedContainer;
    }

    public void assignValues(Quotation quotation) {
        this.selectedItem = null;
        this.quotation = quotation;
        Long quota_id = quotation.getId();
        if (quota_id != null) {
            pagedTable.setContainerDataSource(getIndexedContainer(quota_id));
        } else {
            pagedTable.removeAllItems();
        }
    }

    public class ProcessTimeColumnRender extends EntityColumnRenderer {
        private Date changeDate, prevDate;


        public ProcessTimeColumnRender(Date changeDate, Date prevDate) {
            this.changeDate = changeDate;
            this.prevDate = prevDate;
        }

        @Override
        public Object getValue() {
            long changeTime = changeDate.getTime();
            long prevTime = 0;
            if (prevDate != null) {
                prevTime = prevDate.getTime();

                long processTime = changeTime - prevTime;

                String format = String.format("%%0%dd", 2);
                processTime = processTime / 1000;
                String seconds = String.format(format, processTime % 60);
                String minutes = String.format(format, (processTime % 3600) / 60);
                String hours = String.format(format, processTime / 3600);
                String days = String.format(format, processTime / (3600 * 24));
                String time = days + "d " + hours + "h:" + minutes + "m:" + seconds + "s";
                return time;
            } else {
                return "0";
            }
        }

    }
}
