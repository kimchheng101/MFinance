package com.soma.mfinance.core.shared.service;

import com.soma.mfinance.core.shared.FMEntityField;

/**
 * @author kimsuor.seang
 */
public interface ServiceEntityField extends FMEntityField {
	
	String INSFEE = "INSFEE";
	String REGFEE = "REGFEE";
	String SERFEE = "SERFEE";
}
