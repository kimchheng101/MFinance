package com.soma.mfinance.core.contract.model;

import org.seuksa.frmk.model.entity.MEntityA;


/**
 * Meta data of com.soma.mfinance.core.contract_old.model.ContractSimulation
 * @author kimsuor.seang
 */
public interface MContractSimulation extends MEntityA {
	
	public final static String CONTRACT = "contract";
	public final static String AFTERSALEEVENTTYPE = "afterSaleEventType";
	public final static String EVENTDATE = "eventDate";
	public final static String EXTERNALREFERENCE = "externalReference";
	public final static String APPLICANT = "applicant";
	public final static String CONTRACTSIMULATIONAPP = "contractSimulationApplicants";

}
