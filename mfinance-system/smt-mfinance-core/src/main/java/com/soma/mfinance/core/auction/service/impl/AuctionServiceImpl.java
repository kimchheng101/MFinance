package com.soma.mfinance.core.auction.service.impl;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.common.app.workflow.service.WkfHistoryItemRestriction;
import com.soma.mfinance.core.auction.model.AuctionWkfHistoryItem;
import com.soma.mfinance.core.auction.model.ContractAuctionData;
import com.soma.mfinance.core.auction.service.AuctionService;
import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.contract.model.AuctionStatusHistory;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.ContractAdjustment;
import com.soma.mfinance.core.shared.cashflow.CashflowEntityField;
import com.soma.mfinance.core.shared.contract.ContractEntityField;
import com.soma.mfinance.core.workflow.AuctionWkfStatus;
import com.soma.mfinance.core.workflow.ContractHistoReason;
import com.soma.frmk.security.model.SecUser;
import com.soma.mfinance.core.auction.model.AuctionWkfHistoryItem;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.seuksa.frmk.tools.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import static com.soma.frmk.helper.SeuksaServicesHelper.ENTITY_SRV;

/**
 * Auction status implementation
 *
 * @author kimsuor.seang
 * @modify th.seng (22/11/2017)
 */
@Service("auctionService")
public class AuctionServiceImpl extends BaseEntityServiceImpl implements AuctionService, ContractEntityField, CashflowEntityField {
    /** */
    private static final long serialVersionUID = 5766925421286475829L;

    @Autowired
    private EntityDao dao;

    /**
     * @see org.seuksa.frmk.service.impl.BaseEntityServiceImpl#getDao()
     */
    @Override
    public BaseEntityDao getDao() {
        return dao;
    }

    /**
     * @see com.soma.mfinance.core.auction.service.AuctionService#( Contract , com.soma.mfinance)
     */
    @Override
    public void changeAuctionStatus(Contract contract, EWkfStatus auctionStatus) {
        AuctionStatusHistory history = new AuctionStatusHistory();
        history.setContract(contract);
        history.setAuctionStatus(auctionStatus);
        history.setPreviousAuctionStatus(contract.getAuctionStatus());
        history.setUser((SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        ENTITY_SRV.saveOrUpdate(history);

        contract.setAuctionStatus(auctionStatus);
        contract.setRequestRepossess(true);
        ENTITY_SRV.saveOrUpdate(contract);
    }

   /* *//**
     * @see com.soma.mfinance.core.auction.service.AuctionService#getRealInsuranceBalance(java.util.Date, java.util.List)
     *//*
    @Override
    public Amount getRealInsuranceBalance(Date calculDate, List<Cashflow> cashflows) {
        Amount insuranceBalance = new Amount(0d, 0d, 0d);
        for (Cashflow cashflow : cashflows) {
            if (cashflow.getCashflowType().equals(ECashflowType.FEE)
                    && !cashflow.isCancel()
                    && (!cashflow.isPaid() || (cashflow.getPayment() != null && cashflow.getPayment().getPaymentDate().compareTo(calculDate) > 0))
                    && !cashflow.isUnpaid()
                    && "INSFEE".equals(cashflow.getService().getCode())) {
                insuranceBalance.plusTiAmount(cashflow.getTiInstallmentAmount());
                insuranceBalance.plusTeAmount(cashflow.getTeInstallmentAmount());
                insuranceBalance.plusVatAmount(cashflow.getVatInstallmentAmount());
            }
        }

        return insuranceBalance;
    }*/

  /*  *//**
     * @see com.soma.mfinance.core.auction.service.AuctionService#(java.util.Date, java.lang.Long)
     *//*
    @Override
    public Amount getRealInsuranceBalance(Date calculDate, Long cotraId) {
        BaseRestrictions<Cashflow> restrictions = new BaseRestrictions<>(Cashflow.class);
        restrictions.addCriterion(Restrictions.eq(CANCEL, Boolean.FALSE));
        restrictions.addCriterion(Restrictions.eq(UNPAID, Boolean.FALSE));
        restrictions.addCriterion(Restrictions.eq(CASHFLOW_TYPE, ECashflowType.FEE));
        restrictions.addCriterion(Restrictions.eq(CONTRACT + "." + ID, cotraId));

        return getRealInsuranceBalance(calculDate, list(restrictions));
    }*/

  /*  *//**
     * @see com.soma.mfinance.core.auction.service.AuctionService#getRealServiceIncomeBalance(java.util.Date, java.util.List)
     *//*
    @Override
    public Amount getRealServiceIncomeBalance(Date calculDate, List<Cashflow> cashflows) {
        Amount serviceIncomeBalance = new Amount(0d, 0d, 0d);
        for (Cashflow cashflow : cashflows) {
            if (cashflow.getCashflowType().equals(ECashflowType.FEE)
                    && !cashflow.isCancel()
                    && (!cashflow.isPaid() || (cashflow.getPayment() != null && cashflow.getPayment().getPaymentDate().compareTo(calculDate) > 0))
                    && !cashflow.isUnpaid()
                    && "SERFEE".equals(cashflow.getService().getCode())) {
                serviceIncomeBalance.plusTiAmount(cashflow.getTiInstallmentAmount());
                serviceIncomeBalance.plusTeAmount(cashflow.getTeInstallmentAmount());
                serviceIncomeBalance.plusVatAmount(cashflow.getVatInstallmentAmount());
            }
        }

        return serviceIncomeBalance;
    }*/

    /**
     * @see com.soma.mfinance.core.auction.service.AuctionService# getRealServiceIncomeBalance(java.util.Date, java.lang.Long)
     */
   /* @Override
    public Amount getRealServiceIncomeBalance(Date calculDate, Long cotraId) {
        BaseRestrictions<Cashflow> restrictions = new BaseRestrictions<>(Cashflow.class);
        restrictions.addCriterion(Restrictions.eq(CANCEL, Boolean.FALSE));
        restrictions.addCriterion(Restrictions.eq(UNPAID, Boolean.FALSE));
        restrictions.addCriterion(Restrictions.eq(CASHFLOW_TYPE, ECashflowType.FEE));
        restrictions.addCriterion(Restrictions.eq(CONTRACT + "." + ID, cotraId));

        return getRealServiceIncomeBalance(calculDate, list(restrictions));
    }*/

    @Override
    public Long getDayFromRepossess(Contract contract) {
        if (contract != null) {
            Date repossessDate = contract.getContractAuctionData().getRequestRepossessedDate();
            if (repossessDate != null) {
                return DateUtils.getDiffInDays(DateUtils.today(), repossessDate);
            }
        }
        return 0l;
    }

    /**
     * @see com.soma.mfinance.core.auction.service.AuctionService#getDateOfRepossess(Contract)
     */
    @Override
    public Date getDateOfRepossess(Contract contract) {
        WkfHistoryItemRestriction<AuctionWkfHistoryItem> restrictions = new WkfHistoryItemRestriction<AuctionWkfHistoryItem>(contract.getWkfHistoryItemClass());
        if (contract != null) {
            restrictions.setEntity(contract.getWkfFlow().getEntity());
            restrictions.setEntityId(contract.getId());
        }
        restrictions.setReason(ContractHistoReason.CONTRACT_REP);

        List<AuctionWkfHistoryItem> histories = dao.list(restrictions);
        if (histories == null || histories.isEmpty()) {
            return null;
        }
        return histories.get(0).getChangeDate();
    }

    @Override
    public Long getNumberOfAuction(Contract contract) {
        BaseRestrictions<AuctionStatusHistory> auctionNumber = new BaseRestrictions<>(AuctionStatusHistory.class);
        auctionNumber.addCriterion(Restrictions.eq("auctionStatus", AuctionWkfStatus.CNS));
        auctionNumber.addCriterion(Restrictions.eq("contract.id", contract.getId()));
        return count(auctionNumber);
    }

    @Override
    public Double getRemainingPrincipalBalance(Collection collection) {
        double remainingBalance = 0;
        if (collection != null) {
            remainingBalance = collection.getTiBalanceCapital();
        }
        return remainingBalance;
    }

    @Override
    public Double getPenaltyAmount(Collection collection) {
        double penalty = 0;
        if (collection != null) {
            penalty = collection.getTiPenaltyAmount();
        }
        return penalty;
    }

    @Override
    public Double[] getRemainingServiceIncomeBalance(Contract contract) {
        double unEarnServiceIncome = 0d;
        double insuranceBalance = 0d;
        double remainUnEarnIncome = 0d;
        if (contract.getId() != null) {
            BaseRestrictions<ContractAdjustment> restrictions = new BaseRestrictions(ContractAdjustment.class);
            restrictions.addCriterion(Restrictions.eq("contract.id", contract.getId()));
            List<ContractAdjustment> adjustmentList = ENTITY_SRV.list(restrictions);
            if (adjustmentList != null && !adjustmentList.isEmpty()) {
                if (adjustmentList.get(0).getTiUnpaidUnearnedServicingIncomeUsd() != null) {
                    unEarnServiceIncome = adjustmentList.get(0).getTiUnpaidUnearnedServicingIncomeUsd();
                }
                if (adjustmentList.get(0).getTiUnpaidUnearnedInsuranceIncomeUsd() != null) {
                    insuranceBalance = adjustmentList.get(0).getTiUnpaidUnearnedInsuranceIncomeUsd();
                }
                if (adjustmentList.get(0).getTiUnpaidAccruedInterestReceivableUsd() != null) {
                    remainUnEarnIncome = adjustmentList.get(0).getTiUnpaidAccruedInterestReceivableUsd();
                }
            }
        }
        return new Double[]{unEarnServiceIncome, insuranceBalance, remainUnEarnIncome};
    }
}
