package com.soma.mfinance.core.quotation;

/**
 * Sequence generator
 * @author kimsuor.seang
 *
 */
public interface SequenceGenerator {
	
	/**
	 * @return
	 */
	String generate();
}
