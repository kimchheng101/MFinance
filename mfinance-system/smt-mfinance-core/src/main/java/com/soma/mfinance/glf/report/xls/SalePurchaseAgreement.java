package com.soma.mfinance.glf.report.xls;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.QuotationService;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationDocument;
import com.soma.mfinance.core.shared.conf.AppConfig;
import com.soma.mfinance.core.shared.report.ReportParameter;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.mfinance.glf.report.model.CustomerState;
import com.soma.mfinance.glf.report.model.DealerState;
import com.soma.mfinance.glf.report.model.EReportTamplate;
import com.soma.mfinance.glf.report.model.ReportNumberUtil;
import com.soma.mfinance.tools.report.Report;
import com.soma.ersys.core.hr.model.address.Address;
import com.soma.ersys.core.hr.model.eref.EGender;
import com.soma.ersys.core.hr.model.eref.ENationality;
import com.soma.frmk.security.context.SecApplicationContextHolder;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.joda.time.Years;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.model.entity.EntityA;
import org.seuksa.frmk.tools.DateUtils;

import java.util.*;

/**
 * Created by Dang Dim
 * Date     : 21-Jul-17, 9:57 AM
 * Email    : d.dim@gl-f.com
 */
public class SalePurchaseAgreement implements Report, FinServicesHelper {

    protected QuotationService quotationService = (QuotationService) SecApplicationContextHolder.getContext().getBean("quotationService");
    public static String NA = "N/A";

    @Override
    public String generate(ReportParameter reportParameter) throws Exception {
        Long quotaId = (Long) reportParameter.getParameters().get("quotaId");
        Quotation quotation = quotationService.getById(Quotation.class, quotaId);

        BaseRestrictions<EntityA> restrictions = null;
        List<EntityA> list = new ArrayList<>();
        DealerState dealerState = new DealerState();
        CustomerState customerState = new CustomerState();
        int number = 1;

        restrictions = new BaseRestrictions(CustomerState.class);
        restrictions.addCriterion(Restrictions.eq("quotation.id", quotaId));
        restrictions.addCriterion(Restrictions.eq("wkfStatus", QuotationWkfStatus.APV));
        list = ENTITY_SRV.list(restrictions);

        if (!list.isEmpty()) {
            customerState = (CustomerState) list.get(0);
            number = Integer.valueOf(customerState.getState().toString());
        } else {
            restrictions = new BaseRestrictions(DealerState.class);
            restrictions.addCriterion(Restrictions.eq("dealer.id", quotation.getDealer().getId()));
            restrictions.addCriterion(Restrictions.eq("reportTamplate", EReportTamplate.SPA));
            list = ENTITY_SRV.list(restrictions);
            if (!list.isEmpty()) {
                dealerState = (DealerState) list.get(0);
                number = Integer.valueOf(dealerState.getStateNumber().toString());
            }
        }

        String reportNumber = ReportNumberUtil.generateNumber(quotation.getDealer(), EReportTamplate.SPA, number);

        Applicant applicant = quotation.getApplicant();
        Asset asset = quotation.getAsset();

        Map<String, String> apaBean = new HashMap<String, String>();

        Date printDate = new Date();
        apaBean.put("numReport", reportNumber);
        apaBean.put("day", String.valueOf(DateUtils.getDay(printDate)));
        apaBean.put("month", String.valueOf(DateUtils.getMonth(printDate)));
        apaBean.put("year", String.valueOf(DateUtils.getYear(printDate)));

        apaBean.put("lesseeName", applicant.getIndividual().getLastName() + " " + applicant.getIndividual().getFirstName());
        apaBean.put("sex", getGenderDesc(applicant.getIndividual().getGender().getCode()));
        apaBean.put("dateOfBirth", DateUtils.getDateLabel(applicant.getIndividual().getBirthDate(), DateUtils.FORMAT_DDMMYYYY_SLASH));
        apaBean.put("age", getAgeByBirthDate(applicant.getIndividual().getBirthDate()));
        apaBean.put("nation", getNation(applicant.getIndividual().getNationality().getCode()));

        QuotationDocument idCardDocument = getDocument("N", quotation.getQuotationDocuments());
        if (idCardDocument != null && !idCardDocument.getReference().isEmpty()){
            apaBean.put("idNumber", getDefaultString(idCardDocument.getReference()));
        }else
            apaBean.put("idNumber", NA);
        if (idCardDocument != null && idCardDocument.getIssueDate() != null){
            apaBean.put("idDay", String.valueOf(DateUtils.getDay(idCardDocument.getIssueDate())));
            apaBean.put("idMonth", String.valueOf(DateUtils.getMonth(idCardDocument.getIssueDate())));
            apaBean.put("idYear", String.valueOf(DateUtils.getYear(idCardDocument.getIssueDate())));
        }else {
            apaBean.put("idDay", NA);
            apaBean.put("idMonth",NA);
            apaBean.put("idYear",NA);
        }


        Address address = applicant.getIndividual().getMainAddress();
        if (address != null) {
            apaBean.put("houseNo", StringUtils.isNotEmpty(address.getHouseNo()) ? address.getHouseNo() : NA);
            apaBean.put("streetNo", StringUtils.isNotEmpty(address.getStreet()) ? address.getStreet() : NA);
            apaBean.put("village", address.getVillage() != null ? address.getVillage().getDesc() : NA);
            apaBean.put("commune", address.getCommune() != null ? address.getCommune().getDesc() : NA);
            apaBean.put("district", address.getDistrict() != null ? address.getDistrict().getDesc() : NA);
            apaBean.put("province", address.getProvince() != null ? address.getProvince().getDesc() : NA);
        } else {
            apaBean.put("houseNo", NA);
            apaBean.put("streetNo", NA);
            apaBean.put("village", NA);
            apaBean.put("commune", NA);
            apaBean.put("district", NA);
            apaBean.put("province", NA);
        }

        apaBean.put("model", asset.getModel().getDescEn());
        apaBean.put("color", asset.getColor().getDesc());
        apaBean.put("type", getGenderDesc(asset.getAssetGender().getCode()));
        apaBean.put("registrationDate", DateUtils.getDateLabel(asset.getRegistrationDate(), DateUtils.FORMAT_DDMMYYYY_SLASH));

        apaBean.put("numberOfCC", getDefaultString(asset.getEngine().getDescEn()));
        apaBean.put("chassisNumber", asset.getChassisNumber());
        apaBean.put("engineNumber", asset.getEngineNumber());
        apaBean.put("plateNumber", asset.getPlateNumber());

        String templatePath = AppConfig.getInstance().getConfiguration().getString("specific.templatedir");
        String templateFileName = templatePath + "/SalePurchaseAgreement.xlsx";
        String outputPath = AppConfig.getInstance().getConfiguration().getString("specific.tmpdir");

        String prefixOutputName = "SalePurchaseAgreement";
        String sufixOutputName = "xlsx";
        String uuid = UUID.randomUUID().toString().replace("-", "");
        String xlsFileName = outputPath + "/" + prefixOutputName + uuid + "." + sufixOutputName;
        XLSTransformer transformer = new XLSTransformer();
        transformer.transformXLS(templateFileName, apaBean, xlsFileName);

        if (dealerState.getId() != null) {
            dealerState.setStateNumber(dealerState.getStateNumber() + 1);
            ENTITY_SRV.saveOrUpdate(dealerState);
        } else if (customerState.getId() == null) {
            dealerState.setStateNumber(new Long(number + 1));
            dealerState.setReportTamplate(EReportTamplate.SPA);
            dealerState.setDealer(quotation.getDealer());
            ENTITY_SRV.saveOrUpdate(dealerState);
        }

        if (customerState.getId() == null) {
            customerState.setQuotation(quotation);
            customerState.setState(dealerState.getStateNumber() - 1);
            customerState.setWkfStatus(quotation.getWkfStatus());
            ENTITY_SRV.saveOrUpdate(customerState);
        }
        return prefixOutputName + uuid + "." + sufixOutputName;
    }

    /* Get document for Applicant*/
    private QuotationDocument getDocument(String documCode, List<QuotationDocument> documents) {
        for (QuotationDocument document : documents) {
            if (document.getDocument().getApplicantType().equals(EApplicantType.C)
                    && documCode.equals(document.getDocument().getCode())) {
                return document;
            }
        }
        return null;
    }

    /* Calculate Age from your date of birth*/
    private String getAgeByBirthDate(Date birthDate) {
        long timestamp = birthDate.getTime();
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);
        Integer y = cal.get(Calendar.YEAR);
        Integer moy = cal.get(Calendar.MONTH) + 1;
        Integer dom = cal.get(Calendar.DAY_OF_MONTH);
        org.joda.time.LocalDate birthdate = new org.joda.time.LocalDate(y, moy, dom);
        Years age = Years.yearsBetween(birthdate, org.joda.time.LocalDate.now());
        return String.format("%s", age.getYears());
    }

    private String getDefaultString(String value) {
        return StringUtils.defaultString(value);
    }

    /*retrurn code*/
    private String getGenderDesc(String code) {
        if (EGender.M.getCode().equals(code)) {
            return "ប្រុស";
        } else if (EGender.F.getCode().equals(code)) {
            return "ស្រី";
        } else if (EGender.U.getCode().equals(code)) {
            return "មិនដឹង";
        }
        return "";
    }

    private String getNation(String code) {
        if (ENationality.KHMER.getCode().equals(code)) {
            return "ខ្មែរ";
        }
        return "";
    }
}
