package com.soma.mfinance.core.accounting.services.impl;

import com.soma.mfinance.core.accounting.dto.RemainingBalanceDTO;
import com.soma.mfinance.core.accounting.services.AccountingService;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowType;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.ersys.finance.accounting.tools.helper.ErsysAccountingAppServicesHelper;
import com.soma.finance.services.shared.CalculationParameter;
import com.soma.finance.services.tools.LoanUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.soma.mfinance.core.contract.model.MContract.*;

/**
 * @author by kimsuor.seang  on 11/24/2017.
 */
@Service
public class AccountingServiceImpl extends BaseEntityServiceImpl implements AccountingService, FinServicesHelper, ErsysAccountingAppServicesHelper {

    @Autowired
    private EntityDao entityDao;

    @Override
    public BaseEntityDao getDao() {
        return entityDao;
    }

    @Override
    public List<Contract> getContractsRemainingBalance(EDealerType dealerType, Dealer dealer, String reference, Date endDate) {
        BaseRestrictions<Contract> restrictions = new BaseRestrictions<>(Contract.class);
        restrictions.addAssociation(DEALER, "dea", JoinType.INNER_JOIN);
        if (dealer != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        }
        if (dealerType != null) {
            restrictions.addCriterion(Restrictions.eq("dea.dealerType", dealerType));
        }
        if (StringUtils.isNotEmpty(reference)) {
            restrictions.addCriterion(Restrictions.like(REFERENCE, reference, MatchMode.ANYWHERE));
        }
        if (endDate != null) {
            restrictions.addCriterion(Restrictions.le(STARTDATE, DateUtils.getDateAtEndOfDay(endDate)));
            restrictions.addOrder(Order.asc(STARTDATE));
        }
        if (ENTITY_SRV.list(restrictions) != null && ENTITY_SRV.list(restrictions).size() > 0) {
            return ENTITY_SRV.list(restrictions);
        }
        return null;
    }

    @Override
    public List<RemainingBalanceDTO> getLeaseTransactionsRemainingBalance(EDealerType dealerType, Dealer dealer, String reference, Date endDate) {
        List<RemainingBalanceDTO> remainingBalanceDTOS = new ArrayList<>();
        List<Contract> contracts = getContractsRemainingBalance(dealerType, dealer, reference, endDate);
        Double amountDefault = 0d;

        if (contracts != null && contracts.size() > 0) {
            for (Contract contract : contracts) {
                List<Cashflow> cashflowsPaid = getCashflowsPaid(contract);

                RemainingBalanceDTO remainingBalanceDTO = new RemainingBalanceDTO();
                remainingBalanceDTO.setId(contract.getId());
                remainingBalanceDTO.setReference(contract.getReference());
                remainingBalanceDTO.setContractStartDate(contract.getStartDate());
                remainingBalanceDTO.setFirstInstallmentDate(contract.getFirstDueDate());
                if (contract.getApplicant() != null && contract.getApplicant().getIndividual() != null) {
                    remainingBalanceDTO.setFirstNameEn(contract.getApplicant().getIndividual().getFirstNameEn());
                    remainingBalanceDTO.setLastNameEn(contract.getApplicant().getIndividual().getLastNameEn());
                }
                remainingBalanceDTO.setInterestRate(contract.getInterestRate());
                remainingBalanceDTO.setIrrRate(MyNumberUtils.getDouble(contract.getIrrRate()));

                CalculationParameter calculationParameter = new CalculationParameter();
                calculationParameter.setInitialPrincipal(contract.getTiFinancedAmount());
                if (contract.getTerm() != null && contract.getFrequency() != null) {
                    calculationParameter.setNumberOfPeriods(LoanUtils.getNumberOfPeriods(contract.getTerm(), contract.getFrequency()));
                }
                calculationParameter.setPeriodicInterestRate(contract.getInterestRate() / 100d);
                calculationParameter.setFrequency(contract.getFrequency());

                if (contract.getWkfStatus().equals(ContractWkfStatus.THE) || contract.getWkfStatus().equals(ContractWkfStatus.ACC) ||
                        contract.getWkfStatus().equals(ContractWkfStatus.EAR) || contract.getWkfStatus().equals(ContractWkfStatus.CLO) ||
                        contract.getWkfStatus().equals(ContractWkfStatus.REP)) {
                    remainingBalanceDTO.setPrincipalBalance(amountDefault);
                    remainingBalanceDTO.setUnearnedInterestBalance(amountDefault);
                } else {
                    Double totalPrincipalBalance = getPrincipalBalance(contract.getQuotation(), endDate, cashflowsPaid);
                    remainingBalanceDTO.setPrincipalBalance(totalPrincipalBalance);

                    Double totalInterestRate = getTotalInterestRate(contract.getQuotation());
                    Double totalEarningInterest = getTotalEarningInterest(cashflowsPaid, endDate);
                    Double totalUnearnedInterest = getTotalUnearnedInterest(totalInterestRate, totalEarningInterest);
                    remainingBalanceDTO.setUnearnedInterestBalance(totalUnearnedInterest);
                }
                remainingBalanceDTOS.add(remainingBalanceDTO);
            }
        }
        return remainingBalanceDTOS;
    }

    @Override
    public List<Cashflow> getCashflowsPaid(Contract contract) {
        if (contract != null && contract.getId() != null) {
            BaseRestrictions<Cashflow> restrictions = new BaseRestrictions<>(Cashflow.class);
            restrictions.addCriterion("contract.id", contract.getId());
            restrictions.addCriterion(Restrictions.in("cashflowType", new ECashflowType[]{ECashflowType.CAP, ECashflowType.IAP}));
            restrictions.addCriterion(Restrictions.eq("paid", true));
            restrictions.addCriterion(Restrictions.eq("unpaid", false));
            restrictions.addCriterion(Restrictions.eq("cancel", false));
            if (!ENTITY_SRV.list(restrictions).isEmpty() && ENTITY_SRV.list(restrictions).size() > 0) {
                return ENTITY_SRV.list(restrictions);
            }
        }
        return null;
    }

    @Override
    public Double getPrincipalBalance(Quotation quotation, Date calculDate, List<Cashflow> cashflows) {
        Double result = 0.0d;
        Double totalTiFinanceAmount = 0d;
        Double totalTiInstallmentAmount = 0d;
        if (cashflows != null && cashflows.size() > 0) {
            for (Cashflow cashflow : cashflows) {
                if (cashflow.getCashflowType().equals(ECashflowType.CAP) && cashflow.getInstallmentDate().compareTo(calculDate) < 0 && !cashflow.isUnpaid()) {
                    totalTiInstallmentAmount += cashflow.getTiInstallmentAmount();
                    cashflow.getContract().getQuotation().getTiFinanceAmount();
                }
                if (cashflow.getContract() != null) {
                    if (cashflow.getContract().getQuotation() != null) {
                        quotation = cashflow.getContract().getQuotation();
                    }
                }
            }
            if (quotation != null) {
                totalTiFinanceAmount = quotation.getTmFinanceAmount();
            }
            result = totalTiFinanceAmount - totalTiInstallmentAmount;
        }
        return result;
    }

    @Override
    public Double getTotalInterestRate(Quotation quotation) {
        /**
         * total interest = (interest rate /100) * Lease Amount * Term
         */
        Double interestRate = 0d;
        Double totalLeaveAmount = 0d;
        Integer term = 0;
        if (quotation != null) {
            interestRate = quotation.getInterestRate();
            totalLeaveAmount = quotation.getTmFinanceAmount();
            term = quotation.getTerm();
            return (interestRate / 100) * totalLeaveAmount * term;
        }
        return 0d;
    }

    @Override
    public Double getTotalEarningInterest(List<Cashflow> cashflows, Date calculDate) {
        Double result = 0d;
        if (cashflows != null && cashflows.size() > 0) {
            for (Cashflow cashflow : cashflows) {
                if (cashflow.getCashflowType().equals(ECashflowType.IAP) && cashflow.getInstallmentDate().compareTo(calculDate) < 0 && !cashflow.isUnpaid()) {
                    result += cashflow.getTiInstallmentAmount();
                }
            }
            return result;
        }
        return 0d;
    }

    @Override
    public Double getTotalUnearnedInterest(Double totalInterest, Double totalEarningInterest) {
        if (totalInterest != null && totalEarningInterest != null) {
            return totalInterest - totalEarningInterest;
        }
        return 0d;
    }
}
