package com.soma.mfinance.core.applicant.model;

import org.seuksa.frmk.model.entity.MEntityA;

/**
 * Meta data of com.soma.mfinance.core.applicant.model.IndividualReferenceInfo
 * @author kimsuor.seang
 */
public interface MIndividualReferenceInfo extends MEntityA {

	public final static String REFERENCETYPE = "referenceType";
	public final static String RELATIONSHIP = "relationship";
	public final static String LASTNAMEEN = "lastNameEn";
	public final static String FIRSTNAMEEN = "firstNameEn";
	public final static String PHONE = "phone";
	
}
