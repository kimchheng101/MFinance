package com.soma.mfinance.core.shared.contract;

import com.soma.mfinance.core.shared.FMEntityField;

/**
 * @author kimsuor.seang
 */
public interface ContractEntityField extends FMEntityField {
	
	String AUCTION_STATUS = "auctions";
	String OVERDUE = "overdue";
	String CONTRACT_OTHER_DATA = "contractOtherData";
	String QUOTATION_DATE = "quotationDate";
	String APPROVAL_DATE = "approvalDate";	
}
