package com.soma.mfinance.core.quotation.model;

import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.model.system.ContactVerification;

import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;

/**
 * @author kimsuor.seang
 */
@Entity
@Table(name = "td_quotation_contact_verification")
public class QuotationContactVerification extends EntityA {

	private static final long serialVersionUID = -2436094822502946599L;
	private Quotation quotation;
	private ContactVerification contactVerification;
	private String value;
	private EApplicantType applicantType;
	
	/**
     * Get quotation applicant's is.
     * @return The quotation applicant's is.
     */
    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "qou_cev_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }
	
	/**
	 * @return the quotation
	 */
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "quo_id")
	public Quotation getQuotation() {
		return quotation;
	}

	/**
	 * @param quotation the quotation to set
	 */
	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}

	/**
	 * @return the contactVerification
	 */
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ctevi_id")
	public ContactVerification getContactVerification() {
		return contactVerification;
	}

	/**
	 * @param contactVerification the contactVerification to set
	 */
	public void setContactVerification(
			ContactVerification contactVerification) {
		this.contactVerification = contactVerification;
	}

	/**
	 * @return the value
	 */
	@Column(name = "qou_cev_va_value", nullable = true, length = 1000)
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	@Column(name = "app_typ_id", nullable = false)
	@Convert(converter = EApplicantType.class)
	public EApplicantType getApplicantType() {
		return applicantType;
	}

	/**
	 * @param applicantType the applicantType to set
	 */
	public void setApplicantType(EApplicantType applicantType) {
		this.applicantType = applicantType;
	}
}
