package com.soma.mfinance.core.contract.dao.impl;

import com.soma.mfinance.core.contract.dao.ContractDao;
import org.seuksa.frmk.dao.impl.BaseEntityDaoImpl;
import org.springframework.stereotype.Repository;

/**
 * Contract data access implementation
 *
 * @author kimsuor.seang
 */
@Repository
public class ContractDaoImpl extends BaseEntityDaoImpl implements ContractDao {

}
