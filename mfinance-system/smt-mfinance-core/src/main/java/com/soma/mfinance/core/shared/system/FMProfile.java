package com.soma.mfinance.core.shared.system;

/**
 * @author kimsuor.seang
 */
public interface FMProfile {
	Long AD =new Long(1);
	Long PO =new Long(2);
	Long UW=new Long(3);
	Long MA=new Long(4);
	Long CO=new Long(5);
	Long DM=new Long(6);
	Long DM2=new Long(31);
	Long US=new Long(7);
	Long AC=new Long(8);
	Long CC=new Long(9);
	Long CS=new Long(10);
	Long MK=new Long(11);
	Long SC=new Long(12);
	Long CM=new Long(13);
	Long UC=new Long(14);
	Long ARC=new Long(16);
	Long ARM=new Long(17);
	Long AGT=new Long(18);
	Long US2=new Long(19);
	Long CT=new Long(20);
	Long AO=new Long(21);
	Long PRL=new Long(22);
	Long SFCO=new Long(23);
	Long DCS=new Long(25);
	Long CO2=new Long(26);
	Long TRU=new Long(27);
	Long OP_COR=new Long(28);
	Long ADU=new Long(29);
	Long ADM = new Long(32);
	/*Long CO = new Long(1);
	Long PO = new Long(2);
	Long UW = new Long(3);
	Long MA = new Long(4);
	Long AD = new Long(5);
	Long DC = new Long(6);
	Long US = new Long(7);
	Long AC = new Long(8);
	Long CC = new Long(9);
	Long CS = new Long(10);
	Long MK = new Long(11);
	Long SC = new Long(12);
	Long CM = new Long(13);
	Long UC = new Long(14);*/
}
