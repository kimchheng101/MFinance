package com.soma.mfinance.core.asset.panel.appraisal.group;

import com.soma.mfinance.core.asset.model.appraisal.AppraisalGroup;
import com.soma.mfinance.core.shared.asset.AssetEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.ui.Table;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by Kimsuor SEANG
 * Date  : 7/19/2017
 * Name  : 1:48 PM
 * Email : k.seang@gl-f.com
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AppraisalGroupTablePanel extends AbstractTablePanel<AppraisalGroup> implements AssetEntityField {

    @PostConstruct
    public void PostConstruct() {
        setCaption(I18N.message("groupdetail.groups"));
        setSizeFull();
        setMargin(true);
        setSpacing(true);
        super.init(I18N.message("groupdetail.groups"));

        addDefaultNavigation();
    }

    @Override
    protected AppraisalGroup getEntity() {
        final Long id = getItemSelectedId();
        if (id != null) {
            return ENTITY_SRV.getById(AppraisalGroup.class, id);
        }
        return null;
    }

    @Override
    protected PagedDataProvider<AppraisalGroup> createPagedDataProvider() {
        PagedDefinition<AppraisalGroup> pagedDefinition = new PagedDefinition<>(searchPanel.getRestrictions());
        pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Table.Align.LEFT, 50);
        pagedDefinition.addColumnDefinition(CODE, I18N.message("code"), String.class, Table.Align.LEFT, 170);
        pagedDefinition.addColumnDefinition(DESC_EN, I18N.message("name"), String.class, Table.Align.LEFT, 170);
        pagedDefinition.addColumnDefinition(coefficient, I18N.message("coefficient"), Double.class, Table.Align.LEFT, 170);
        pagedDefinition.addColumnDefinition(SORT_INDEX, I18N.message("sortIndex"), Integer.class, Table.Align.LEFT, 170);
        EntityPagedDataProvider<AppraisalGroup> pagedDataProvider = new EntityPagedDataProvider<>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    @Override
    protected AbstractSearchPanel<AppraisalGroup> createSearchPanel() {
        return new AppraisalGroupSearchPanel(this);
    }
}
