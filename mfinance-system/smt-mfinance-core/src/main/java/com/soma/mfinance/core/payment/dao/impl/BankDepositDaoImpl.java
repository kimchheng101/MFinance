package com.soma.mfinance.core.payment.dao.impl;

import org.seuksa.frmk.dao.impl.BaseEntityDaoImpl;
import org.springframework.stereotype.Repository;

import com.soma.mfinance.core.payment.dao.BankDepositDao;

/**
 * 
 * @author meng.kim
 *
 */
@Repository
public class BankDepositDaoImpl extends BaseEntityDaoImpl implements BankDepositDao {

}
