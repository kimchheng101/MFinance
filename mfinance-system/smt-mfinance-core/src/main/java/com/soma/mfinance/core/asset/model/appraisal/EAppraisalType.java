package com.soma.mfinance.core.asset.model.appraisal;

import java.util.List;

import javax.persistence.AttributeConverter;

import org.seuksa.frmk.model.eref.BaseERefData;

/**
 * 
 * @author vi.sok
 *
 */
public class EAppraisalType extends BaseERefData implements AttributeConverter<EAppraisalType, Long> {
	/** */
	private static final long serialVersionUID = 8800717908072047609L;

	/**
	 */
	public EAppraisalType() {
	}
	
	/**
	 * @param code
	 * @param id
	 */
	public EAppraisalType(String code, long id) {
		super(code, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public EAppraisalType convertToEntityAttribute(Long id) {
		return super.convertToEntityAttribute(id);
	}
	
	@Override
	public Long convertToDatabaseColumn(EAppraisalType arg0) {
		return super.convertToDatabaseColumn(arg0);
	}

	/** 
	 * @return
	 */
	public static List<EAppraisalType> values() {
		return getValues(EAppraisalType.class);
	}
	
	/**
	 * @param code
	 * @return
	 */
	public static EAppraisalType getByCode(String code) {
		return getByCode(EAppraisalType.class, code);
	}
	
	/**
	 * @param id
	 * @return
	 */
	public static EAppraisalType getById(long id) {
		return getById(EAppraisalType.class, id);
	}
}
