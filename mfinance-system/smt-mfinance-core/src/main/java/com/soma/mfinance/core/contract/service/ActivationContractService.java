package com.soma.mfinance.core.contract.service;

import com.soma.mfinance.core.asset.model.AssetModel;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.quotation.model.Quotation;
import org.seuksa.frmk.service.BaseEntityService;
import org.seuksa.frmk.tools.exception.EntityCreationException;

import java.util.Date;
import java.util.List;

/**
 * Activation Contract Service
 * @author kimsuor.seang
 */
public interface ActivationContractService extends BaseEntityService {

	/**
	 * @param quotation
	 * @return
	 */
	Contract createContract(Quotation quotation) throws EntityCreationException;
			
	/**
	 * Complete contract_old
	 * @param contract
	 * @param isForced
	 * @return
	 */
	Contract complete(Contract contract, boolean isForced, boolean disburse) throws DealerPaymentException, JournalEntryException ;
	
	/**
	 * @param contract
	 */
	void unholdDealerPayment(Contract contract) throws DealerPaymentException, JournalEntryException ;
	
	/**
	 * 
	 * @param paymentIds
	 */
	void sendPaymentToAP(List<Long> paymentIds);
	
	/**
	 * 
	 * @param
	 * @param chassisNo
	 * @param engineNo
	 * @param contract
	 * @return
	 */
	List<String> validation(Contract contract, Date firstDueDate, String chassisNo, String engineNo, String taxInvoiceNumber);
	
	/**
	 * @param dealer
	 * @param assetModel
	 * @return
	 */
	double getInsurancePremium(Dealer dealer, AssetModel assetModel);
	
	/**
	 * 
	 */
	void updateContractData(Contract contract);
}
