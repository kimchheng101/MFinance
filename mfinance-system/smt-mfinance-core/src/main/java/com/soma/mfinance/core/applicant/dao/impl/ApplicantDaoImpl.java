package com.soma.mfinance.core.applicant.dao.impl;

import com.soma.mfinance.core.applicant.dao.ApplicantDao;
import org.seuksa.frmk.dao.impl.BaseEntityDaoImpl;
import org.springframework.stereotype.Repository;

/**
 * Applicant data access implementation
 *
 * @author kimsuor.seang
 */
@Repository
public class ApplicantDaoImpl extends BaseEntityDaoImpl implements ApplicantDao {

}
