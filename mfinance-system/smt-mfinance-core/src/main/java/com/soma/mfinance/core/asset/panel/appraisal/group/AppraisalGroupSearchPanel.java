package com.soma.mfinance.core.asset.panel.appraisal.group;

import com.soma.mfinance.core.asset.model.appraisal.AppraisalGroup;
import com.soma.mfinance.core.shared.asset.AssetEntityField;
import com.soma.mfinance.core.shared.component.ComponentLayoutFactory;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.statusrecord.StatusRecordField;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;

/**
 * Created by Kimsuor SEANG
 * Date  : 7/19/2017
 * Name  : 1:48 PM
 * Email : k.seang@gl-f.com
 */
public class AppraisalGroupSearchPanel extends AbstractSearchPanel<AppraisalGroup> implements AssetEntityField {

    private TextField txtDescEn;
    private TextField txtCode;
    private StatusRecordField statusRecordField;
    /**
     *
     * @param appraisalGroupTablePanel
     */
    public AppraisalGroupSearchPanel(AppraisalGroupTablePanel appraisalGroupTablePanel) {
        super(I18N.message("search"), appraisalGroupTablePanel);
    }
    @Override
    protected void reset() {

    }

    @Override
    protected Component createForm() {
        GridLayout gridLayout = new GridLayout(20, 3);
        gridLayout.setSpacing(true);
        txtDescEn = ComponentFactory.getTextField(null, false, 60, 180);

        txtCode = ComponentFactory.getTextField(null, false, 60, 180);
        statusRecordField = new StatusRecordField();


        int iCol = 0;
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(ComponentLayoutFactory.getLabelCaption("code"), iCol++, 0);
        gridLayout.addComponent(txtCode, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(ComponentLayoutFactory.getLabelCaption("name"), iCol++, 0);
        gridLayout.addComponent(txtDescEn, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(statusRecordField, iCol++, 0);
        return gridLayout;
    }

    @Override
    public BaseRestrictions<AppraisalGroup> getRestrictions() {
        BaseRestrictions<AppraisalGroup> restrictions = new BaseRestrictions<>(AppraisalGroup.class);

        if (StringUtils.isNotEmpty(txtCode.getValue())) {
            restrictions.addCriterion(Restrictions.like(CODE, txtCode.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtDescEn.getValue())) {
            restrictions.addCriterion(Restrictions.like(DESC_EN, txtDescEn.getValue(), MatchMode.ANYWHERE));
        }

        if (statusRecordField.isInactiveAllValues()) {
            restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
            restrictions.getStatusRecordList().add(EStatusRecord.INACT);
        }
        if (statusRecordField.getActiveValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
        }
        if (statusRecordField.getInactiveValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.INACT);
        }

        return restrictions;
    }
}
