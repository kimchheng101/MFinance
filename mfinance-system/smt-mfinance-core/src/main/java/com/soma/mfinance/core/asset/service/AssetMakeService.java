package com.soma.mfinance.core.asset.service;

import com.soma.mfinance.core.asset.model.AssetMake;
import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.asset.model.AssetMake;

/**
 * Asset make service interface
 * @author kimsuor.seang
 */
public interface AssetMakeService extends BaseEntityService {

	/**
	 * saveOrUpdate Asset Make
	 * @param assetMake
	 */
	void saveOrUpdateAssetMake(AssetMake assetMake);
	
}
