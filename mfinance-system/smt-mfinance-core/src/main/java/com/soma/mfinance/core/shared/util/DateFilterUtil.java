package com.soma.mfinance.core.shared.util;

import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.frmk.config.model.SettingConfig;
import org.apache.commons.lang.StringUtils;
import org.seuksa.frmk.tools.DateUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
public class DateFilterUtil {

	/**
     * 
     * @param dt
     * @param dt
     * @return
     */
	public static Date getStartDate(Date dt) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        cal.set(Calendar.HOUR_OF_DAY, 17);
        cal.set(Calendar.MINUTE, 30);
        cal.set(Calendar.SECOND, 01);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
    
    /**
     * 
     * @param dt
     * @param dt
     * @return
     */
    public static Date getEndDate(Date dt) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        cal.set(Calendar.HOUR_OF_DAY, 17);
        cal.set(Calendar.MINUTE, 30);
        cal.set(Calendar.SECOND, 00);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
    
    public static Date getWorkingStartDate(Date dt) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        cal.set(Calendar.HOUR_OF_DAY, 8);
        cal.set(Calendar.MINUTE, 30);
        cal.set(Calendar.SECOND, 00);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
    public static Date getWorkingEndDate(Date dt) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        cal.set(Calendar.HOUR_OF_DAY, 17);
        cal.set(Calendar.MINUTE, 30);
        cal.set(Calendar.SECOND, 00);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }


    /**
     * Validation between time save contract from 2:00 PM to 7:00 AM
     * @param start1Pm
     * @return
     */
    /*public static boolean validateTimeSaveContractToSchedule(int start1Pm){
        Calendar calendar = new GregorianCalendar();
        int hour = DateUtils.getHour(DateUtils.today());
        int minute = DateUtils.getMinute(DateUtils.today());
        int second  = calendar.get(Calendar.SECOND);

        Calendar cal = Calendar.getInstance();
        if (cal.get(Calendar.AM_PM) == Calendar.PM) {
            if (hour >= start1Pm) {
                System.out.print(" H " + hour + " :M " + minute + " :S " + second + " PM");
                return true;
            }
        }
        else{
            if (hour < end7Am) {
                System.out.print(" H " + hour + " :M " + minute + " S " + second + " AM");
                return true;
            }
        }
        return false;
    }*/
    public static Date getWorkingUWEndDate(Date dt) {
        SettingConfig settingConfigEnd = DataReference.getInstance().getSettingConfigByCode("UW-End-Hour");
        SettingConfig settingConfigeEndMinute = DataReference.getInstance().getSettingConfigByCode("UW-End-Minute");
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        if (settingConfigEnd != null && StringUtils.isNotEmpty(settingConfigEnd.getValue())) {
            cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(settingConfigEnd.getValue().toString()));
            cal.set(Calendar.MINUTE, Integer.parseInt(settingConfigeEndMinute.getValue().toString()));
        }
        cal.set(Calendar.SECOND, 00);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getWorkingUWStartDate(Date dt) {
        SettingConfig settingConfigStart = DataReference.getInstance().getSettingConfigByCode("UW-Start-Hour");
        SettingConfig settingConfigStartMinute = DataReference.getInstance().getSettingConfigByCode("UW-Start-Minute");
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        if (settingConfigStart != null && StringUtils.isNotEmpty(settingConfigStart.getValue())) {
            cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(settingConfigStart.getValue().toString()));
            cal.set(Calendar.MINUTE, Integer.parseInt(settingConfigStartMinute.getValue().toString()));
        }
        cal.set(Calendar.SECOND, 00);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
}
