package com.soma.mfinance.core.system;

import com.soma.ersys.vaadin.ui.security.secuser.UserProfileFormPopupPanel;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.VerticalLayout;

/**
 * Created by Dang Dim
 * Date     : 18-Oct-17, 8:46 AM
 * Email    : d.dim@gl-f.com
 */

public class CustomUserProfilePopupPanel extends UserProfileFormPopupPanel {

    private com.vaadin.ui.TextField txtdefaultProfile;
    private SecUser secUser;

    public CustomUserProfilePopupPanel() {
        super();
        this.setHeight("280");
    }

    @Override
    public void setSecUser(SecUser secUser) {
        super.setSecUser(secUser);
        this.secUser = secUser;
        if(secUser != null) {
            this.txtdefaultProfile.setValue(secUser.getDefaultProfile().getDescEn());
        }
    }

    @Override
    public void open() {
        super.open();
    }

    @Override
    public VerticalLayout createSecUserInfo() {
        VerticalLayout mainLayout = super.createSecUserInfo();
        FormLayout formDetailPanel= (FormLayout) mainLayout.getComponent(1);
        txtdefaultProfile = ComponentFactory.getTextField("profile.default", true, 60, 200.0F);
        txtdefaultProfile.setEnabled(false);
        formDetailPanel.addComponent(this.txtdefaultProfile,2);
        return mainLayout;
    }
}
