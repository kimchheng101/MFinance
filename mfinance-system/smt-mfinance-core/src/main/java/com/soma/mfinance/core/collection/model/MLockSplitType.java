package com.soma.mfinance.core.collection.model;

import org.seuksa.frmk.model.entity.MEntityRefA;

/**
 * Meta data of com.soma.mfinance.core.collection.model.ELockSplitType
 * @author kimsuor.seang
 */
public interface MLockSplitType extends MEntityRefA {

	public final static String LOCKSPLITCASHFLOWTYPES = "lockSplitCashflowTypes";
	public final static String LOCKSPLITGROUP = "lockSplitGroup";

}
