package com.soma.mfinance.core.applicant.panel;

import com.google.gson.Gson;
import com.soma.common.app.eref.ECountry;
import com.soma.ersys.core.hr.model.address.*;
import com.soma.ersys.core.hr.model.eref.*;
import com.soma.frmk.security.context.SecApplicationContextHolder;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox.ButtonType;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.soma.mfinance.client.jersey.ClientQuotation;
import com.soma.mfinance.core.Enum.ECertifyCurrentAddress;
import com.soma.mfinance.core.Enum.ECertifyIncome;
import com.soma.mfinance.core.Enum.ECorrespondence;
import com.soma.mfinance.core.Enum.EHousing;
import com.soma.mfinance.core.applicant.model.*;
import com.soma.mfinance.core.applicant.service.ValidateLetter;
import com.soma.mfinance.core.application.panel.ApplicationPanel;
import com.soma.mfinance.core.common.security.model.SecUserDetail;
import com.soma.mfinance.core.custom.erefdata.ERefDataHelper;
import com.soma.mfinance.core.custom.loader.PropertyLoader;
import com.soma.mfinance.core.document.model.Document;
import com.soma.mfinance.core.financial.model.FinProduct;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationApplicant;
import com.soma.mfinance.core.quotation.model.QuotationDocument;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.mfinance.core.shared.financialproduct.FinancialProductEntityField;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.*;
import com.vaadin.ui.Table.Align;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.Entity;
import org.seuksa.frmk.model.entity.RefDataId;
import org.seuksa.frmk.service.EntityService;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Customer identity panel
 *
 * @author kimsuor.seang
 */
@SuppressWarnings("Duplicates")
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class IdentificationPanel extends AbstractFormPanel implements FMEntityField, FinServicesHelper {

    private static final long serialVersionUID = -3020972492621000892L;

    private PropertyLoader propertyLoader = SpringUtils.getBean(PropertyLoader.class);

    private ApplicationPanel applicationPanel;

    private TextField txtFirstNameEn;
    private TextField txtLastNameEn;
    private AutoDateField dfDateOfBirth;
    private Panel contentPanel;
    private NavigationPanel navigationPanel;
    private NativeButton btnNewApplicant;
    private NativeButton btnIdentify;
    private VerticalLayout verticalLayout;
    private FormLayout formLayout;
    private SimplePagedTable<Applicant> pagedTable;
    private List<ColumnDefinition> columnDefinitions;
    private Item selectedItem = null;

    private EntityRefComboBox<FinProduct> cbxFinancialProduct;
    private TextField txtContractId;
    protected Logger logger = LoggerFactory.getLogger(this.getClass());
/*
    public IdentificationPanel() {
		super();
		setMargin(true);
		setSizeFull();
	}*/

    /** */
    @PostConstruct
    public void PostConstruct() {
        super.init();
        //navigationPanel.addButton(btnIdentify);
    }

    @Override
    protected com.vaadin.ui.Component createForm() {
        contentPanel = new Panel(I18N.message("identification.process"));
        contentPanel.setSizeFull();
        navigationPanel = addNavigationPanel();
        btnIdentify = new NativeButton(I18N.message("identify"));
        btnIdentify.setIcon(new ThemeResource("../smt-default/icons/16/user.png"));
        btnIdentify = new NativeButton(I18N.message("identify"));
        btnIdentify.setIcon(new ThemeResource("../smt-default/icons/16/user.png"));
        btnNewApplicant = new NativeButton(I18N.message("new.applicant"));
        btnNewApplicant.setIcon(new ThemeResource("../smt-default/icons/16/user.png"));
        btnNewApplicant.setVisible(false);
        navigationPanel.addButton(btnIdentify);
        navigationPanel.addButton(btnNewApplicant);

        txtFirstNameEn = ComponentFactory.getTextField("firstname.en", true, 50, 200);
        txtLastNameEn = ComponentFactory.getTextField("lastname.en", true, 50, 200);
        dfDateOfBirth = ComponentFactory.getAutoDateField("dateofbirth", true);
        dfDateOfBirth.setWidth(200, Unit.PIXELS);

        txtContractId = ComponentFactory.getTextField(I18N.message("contract.id"), false, 100, 200);
        cbxFinancialProduct = new EntityRefComboBox<>();
        cbxFinancialProduct.setRestrictions(new BaseRestrictions<>(FinProduct.class));
        cbxFinancialProduct.setCaption(I18N.message("financial.product"));
        cbxFinancialProduct.setRequired(true);
        cbxFinancialProduct.setImmediate(true);
        cbxFinancialProduct.renderer();

        formLayout = new FormLayout();
        formLayout.setMargin(true);
        formLayout.addComponent(cbxFinancialProduct);
        formLayout.addComponent(txtContractId);
        formLayout.addComponent(txtLastNameEn);
        formLayout.addComponent(txtFirstNameEn);
        formLayout.addComponent(dfDateOfBirth);
        verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        verticalLayout.addComponent(navigationPanel);
        contentPanel.setContent(verticalLayout);
        addComponent(contentPanel);
        this.columnDefinitions = createColumnDefinitions();
        pagedTable = new SimplePagedTable<Applicant>(this.columnDefinitions);
        btnIdentify.addClickListener(new ClickListener() {
            private static final long serialVersionUID = -9047707680639618553L;

            @Override
            public void buttonClick(ClickEvent event) {
                proccessIdentifyClick();
            }
        });

        btnNewApplicant.addClickListener(new ClickListener() {
            private static final long serialVersionUID = -5334205712092090703L;

            @Override
            public void buttonClick(ClickEvent event) {
                Quotation quotation = createNewQuotation();
                quotation.setMainApplicant(getApplicant());
                applicationPanel.setQuotation(quotation);
                applicationPanel.addApplicationFormPanel();
            }
        });
        cbxFinancialProduct.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                FinProduct selectedProduct = cbxFinancialProduct.getSelectedEntity();
                if (selectedProduct != null && FinancialProductEntityField.FIN_CODE_EXISTING.equals(selectedProduct.getCode())) {
                    txtContractId.setRequired(true);
                    txtContractId.setEnabled(true);
                    txtFirstNameEn.setRequired(false);
                    txtFirstNameEn.setEnabled(false);
                    txtLastNameEn.setRequired(false);
                    txtLastNameEn.setEnabled(false);
                    dfDateOfBirth.setEnabled(false);
                    dfDateOfBirth.setRequired(false);
                } else {
                    txtContractId.setRequired(false);
                    txtContractId.setEnabled(false);
                    txtFirstNameEn.setRequired(true);
                    txtFirstNameEn.setEnabled(true);
                    txtLastNameEn.setRequired(true);
                    txtLastNameEn.setEnabled(true);
                    dfDateOfBirth.setEnabled(true);
                    dfDateOfBirth.setRequired(true);
                }
            }
        });
        pagedTable.addItemClickListener(new ItemClickListener() {
            private static final long serialVersionUID = -4677521976229171029L;

            @Override
            public void itemClick(ItemClickEvent event) {
                selectedItem = event.getItem();
                boolean isDoubleClick = event.isDoubleClick() || SecApplicationContextHolder.getContext().clientDeviceIsMobileOrTablet();
                if (isDoubleClick) {
                    Long appliId = (Long) selectedItem.getItemProperty(ID).getValue();
                    if (!APP_SRV.isMaxQuotationAuthorisedReach(appliId)) {
                        Quotation quotation = createNewQuotation();
                        Applicant applicant = getApplicant(appliId);
                        quotation.setApplicant(applicant);
                        quotation.setMainApplicant(applicant);
                        applicationPanel.setQuotation(quotation);
                        applicationPanel.addApplicationFormPanel();

                    } else {
                        MessageBox mb = new MessageBox(UI.getCurrent(), I18N.message("information"),
                                MessageBox.Icon.INFO, I18N.message("maximum.applicant.reached"),
                                new MessageBox.ButtonConfig(ButtonType.OK, I18N.message("ok")));
                        mb.setWidth("300px");
                        mb.setHeight("150px");
                        mb.show();
                    }
                }
            }
        });
        return contentPanel;
    }

    /**
     * @return
     */
    private Applicant getApplicant() {
        Applicant mainApplicant = Applicant.createInstance(EApplicantCategory.INDIVIDUAL);
        Individual individual = Individual.createInstance();
        individual.setFirstNameEn(txtFirstNameEn.getValue());
        individual.setLastNameEn(txtLastNameEn.getValue());
        individual.setBirthDate(dfDateOfBirth.getValue());
        mainApplicant.setIndividual(individual);
        return mainApplicant;
    }

    /**
     * @return
     */
    private Applicant getApplicant(Long appliId) {
        return APP_SRV.getApplicantCategory(EApplicantCategory.INDIVIDUAL, appliId);
    }

    /**
     * Reset panel
     */
    public void reset() {
        super.reset();
        verticalLayout.removeAllComponents();
        verticalLayout.addComponent(navigationPanel);
        txtFirstNameEn.setValue("");
        txtLastNameEn.setValue("");
        dfDateOfBirth.setValue(null);
        cbxFinancialProduct.setSelectedEntity(null);
        txtContractId.setValue("");
        verticalLayout.addComponent(formLayout);
        btnIdentify.setVisible(true);
        btnNewApplicant.setVisible(false);
    }

    @Override
    protected Entity getEntity() {
        return null;
    }

    @Override
    protected boolean validate() {

        checkMandatorySelectField(cbxFinancialProduct, "financial.product");
        if (cbxFinancialProduct.getSelectedEntity() != null && FinancialProductEntityField.FIN_CODE_EXISTING.equals(cbxFinancialProduct.getSelectedEntity().getCode())) {
            checkMandatoryField(txtContractId, "contract.id");
        } else {
            checkMandatoryField(txtLastNameEn, "familyname");
            checkMandatoryField(txtFirstNameEn, "firstname");
            checkMandatoryDateField(dfDateOfBirth, "dateofbirth");
            if (INDIVI_SRV.isIndividualOver18Years(dfDateOfBirth.getValue())) {
                errors.add(I18N.message("applicant.not.over.18.and.65.years"));
            }
            if (!ValidateLetter.isEnglishLetter(txtLastNameEn.getValue())) {
                errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("last.name.not.allow.to.input.number.or.firstspace.endspace.or.symbols.en"));
            }
            if (!ValidateLetter.isEnglishLetter(txtFirstNameEn.getValue())) {
                errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("first.name.not.allow.to.input.number.or.firstspace.endspace.or.symbols.en"));
            }
        }
        return errors.isEmpty();
    }

    /**
     * Get indexed container
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    private IndexedContainer getIndexedContainer(List<Individual> applicants) {
        IndexedContainer indexedContainer = new IndexedContainer();
        for (ColumnDefinition column : this.columnDefinitions) {
            indexedContainer.addContainerProperty(column.getPropertyId(), column.getPropertyType(), null);
        }

        for (Individual individual : applicants) {
            Item item = indexedContainer.addItem(individual.getId());
            item.getItemProperty(ID).setValue(individual.getId());
            item.getItemProperty(FIRST_NAME).setValue(individual.getFirstNameEn());
            item.getItemProperty(LAST_NAME).setValue(individual.getLastNameEn());
            item.getItemProperty(BIRTH_DATE).setValue(individual.getBirthDate());
        }
        return indexedContainer;
    }


    @SuppressWarnings("Duplicates")
    protected List<ColumnDefinition> createColumnDefinitions() {
        columnDefinitions = new ArrayList<ColumnDefinition>();
        columnDefinitions.add(new ColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition(FIRST_NAME, I18N.message("firstname.en"), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition(LAST_NAME, I18N.message("lastname.en"), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition(BIRTH_DATE, I18N.message("dateofbirth"), Date.class, Align.LEFT, 100));
        return columnDefinitions;
    }

    /**
     * @param quotation
     */
    private void setSecUserDetailDealer(Quotation quotation) {
        SecUserDetail secUserDetail = getSecUserDetail();
        if (ProfileUtil.isPOS() && secUserDetail != null && secUserDetail.getDealer() != null) {
            quotation.setDealer(secUserDetail.getDealer());
        }
    }

    /**
     * @return
     */
    private SecUserDetail getSecUserDetail() {
        SecUser secUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        EntityService entityService = SpringUtils.getBean(EntityService.class);
        return entityService.getByField(SecUserDetail.class, "secUser.id", secUser.getId());
    }

    private Quotation createNewQuotation() {
        Quotation quotation = Quotation.createInstance();
        quotation.setWkfStatus(QuotationWkfStatus.QUO);
        quotation.setStartCreationDate(DateUtils.today());
        quotation.setValid(true);
        if (cbxFinancialProduct.getSelectedEntity() != null) {
            quotation.setFinancialProduct(cbxFinancialProduct.getSelectedEntity());
        }
        if (ProfileUtil.isPOS()) {
            SecUser secUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            quotation.setCreditOfficer(secUser);
        }
        setSecUserDetailDealer(quotation);
        return quotation;
    }

    @SuppressWarnings("Duplicates")
    private void proccessIdentifyClick() {
        this.errors.clear();
        if (validate()) {
            Quotation quotation = createNewQuotation();
            if (FinancialProductEntityField.FIN_CODE_EXISTING.equals(cbxFinancialProduct.getSelectedEntity().getCode())
                    && txtContractId.getValue() != "") {
                Response response = null;
                if (propertyLoader != null) {
                    String ef_ws_url = propertyLoader.getEnvironment().getProperty("ws.mfinance.webservice");
                    if (ef_ws_url != null && !ef_ws_url.isEmpty()) {
                        response = ClientQuotation.getEFContractByReference(ef_ws_url, txtContractId.getValue());
                    }
                }
                getCustomerInfoFromEfinance(response, quotation);
            } else {
                List<Individual> individuals = INDIVI_SRV.identify(txtLastNameEn.getValue(), txtFirstNameEn.getValue(), dfDateOfBirth.getValue());
                // Incase there are no application found
                if (individuals == null || individuals.isEmpty()) {
                    quotation.setMainApplicant(getApplicant());
                    applicationPanel.setQuotation(quotation);
                    applicationPanel.addApplicationFormPanel();
                } else {
                    super.reset();
                    verticalLayout.setMargin(true);
                    verticalLayout.setSpacing(true);
                    verticalLayout.removeAllComponents();
                    btnIdentify.setVisible(false);
                    btnNewApplicant.setVisible(true);
                    verticalLayout.addComponent(navigationPanel);
                    VerticalLayout informationLayout = new VerticalLayout();
                    informationLayout.setMargin(true);
                    informationLayout.addComponent(new Label(I18N.message("identification.match", txtLastNameEn.getValue(), txtFirstNameEn.getValue()), ContentMode.HTML));
                    verticalLayout.addComponent(informationLayout);
                    pagedTable.setContainerDataSource(getIndexedContainer(individuals));
                    verticalLayout.addComponent(pagedTable);
                }
            }
            applicationPanel.getAssetPanel().assignValues(quotation);
            applicationPanel.getAssetPanel().assetRangeKubuta(quotation);
            applicationPanel.getAssetPanel().setEnableDocumentButton(false);
        } else {
            this.displayErrors();
            this.processAfterError();
        }
    }

    private void getCustomerInfoFromEfinance(Response response, Quotation quotation) {
        try {
            if (response != null && response.getStatus() == Response.Status.OK.getStatusCode()) {
                Object obj = response.readEntity(Object.class);
                Gson gson = new Gson();
                String jsonInString = gson.toJson(obj);
                JSONObject jsonObject = new JSONObject(jsonInString);
                if (jsonObject != null) {
                    JSONArray jsonArray = jsonObject.getJSONArray("RSLT_DATA");
                    if (jsonArray != null && jsonArray.length() != 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

                            //Applicant
                            Applicant applicant = setApplicant(quotation, i, object, formatter);

                            setQuotationApplicant(quotation, applicant);

                            setDocument(quotation, object, formatter);

                            applicationPanel.setQuotation(quotation);
                            applicationPanel.addApplicationFormPanel();
                        }
                    } else {
                        errors.add(I18N.message("There is no quotation with this reference!"));
                        this.displayErrors();
                        this.processAfterError();
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setDocument(Quotation quotation, JSONObject object, SimpleDateFormat formatter) {
        JSONArray jsLstDocuments = object.getJSONArray("DOCUMENTS");
        List<QuotationDocument> lstQuotaDocuments = new ArrayList<>();
        QuotationDocument quotationDocument = null;
        if (jsLstDocuments != null && jsLstDocuments.length() > 0) {
            for (int j = 0; j < jsLstDocuments.length(); j++) {
                JSONObject jsDocument = jsLstDocuments.getJSONObject(j);
                if (jsDocument != null) {
                    Document document = this.getDocumentByCode(jsDocument.getLong("ID"), jsDocument.getLong("GROUP_ID"));
                    if (document != null) {
                        quotationDocument = new QuotationDocument();
                        quotationDocument.setPath(jsDocument.getString("DOC_PATH"));
                        quotationDocument.setReference(jsDocument.getString("DOC_REFERENCE"));
                        if (!jsDocument.getString("DOC_ISS_DATE").equals("null")) {
                            try {
                                quotationDocument.setIssueDate(formatter.parse(jsDocument.getString("DOC_ISS_DATE")));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        if (!jsDocument.getString("DOC_EXP_DATE").equals("null")) {
                            try {
                                quotationDocument.setExpireDate(formatter.parse(jsDocument.getString("DOC_EXP_DATE")));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                        quotationDocument.setOriginal(jsDocument.getBoolean("DOC_ORIGIN"));
                        quotationDocument.setCovConfirmationCo(jsDocument.getBoolean("COV_CONF_CO"));
                        quotationDocument.setCovConfirmationUw(jsDocument.getBoolean("COV_CONF_UW"));
                        quotationDocument.setEmployerConfirmationCo(jsDocument.getBoolean("EMP_CONF_CO"));
                        quotationDocument.setEmployerConfirmationUw(jsDocument.getBoolean("EMP_CONF_UW"));
                        quotationDocument.setDocument(document);
                        lstQuotaDocuments.add(quotationDocument);
                    }
                }
            }
            quotation.setQuotationDocuments(lstQuotaDocuments);
        }
    }

    private void setQuotationApplicant(Quotation quotation, Applicant applicant) {
        List<QuotationApplicant> quotationApplicants = new ArrayList<>();

        QuotationApplicant quotationApplicant = new QuotationApplicant();
        quotationApplicant.setApplicant(applicant);
        quotationApplicant.setApplicantType(EApplicantType.C);
        quotationApplicant.setQuotation(quotation);
        quotationApplicants.add(quotationApplicant);

        quotation.setQuotationApplicants(quotationApplicants);
    }

    private Applicant setApplicant(Quotation quotation, int i, JSONObject object, SimpleDateFormat formatter) {
        JSONArray lstApplicants = object.getJSONArray("APPLICANTS");
        Applicant applicant = null;
        for (int j = 0; j < lstApplicants.length(); j++) {
            JSONObject jsonApplicant = lstApplicants.getJSONObject(i);
            if (jsonApplicant != null) {

                //Individual
                Individual individual = new Individual();

                String civility = jsonApplicant.getString("CIVILITY");
                if (civility.equals(ECivility.MR.getCode())) {
                    individual.setCivility(ECivility.MR);
                } else if (civility.equals(ECivility.MRS.getCode())) {
                    individual.setCivility(ECivility.MRS);
                } else if (civility.equals(ECivility.MS.getCode())) {
                    individual.setCivility(ECivility.MS);
                } else if (civility.equals("MISS")) {
                    individual.setCivility(ECivility.getByCode("MISS"));
                }

                individual.setFirstName(jsonApplicant.getString("FIRST_NAME"));
                individual.setFirstNameEn(jsonApplicant.getString("FIRST_NAME_EN"));
                individual.setLastName(jsonApplicant.getString("LAST_NAME"));
                individual.setLastNameEn(jsonApplicant.getString("LAST_NAME_EN"));
                String gender = jsonApplicant.getString("GENDER");
                if (gender.equals(EGender.F.getCode())) {
                    individual.setGender(EGender.F);
                } else if (gender.equals(EGender.M.getCode())) {
                    individual.setGender(EGender.M);
                } else if (gender.equals(EGender.U)) {
                    individual.setGender(EGender.U);
                }

                individual.setChiefVillagePhoneNumber(jsonApplicant.getString("CHIEF_PHONE"));
                individual.setChiefVillageName(jsonApplicant.getString("CHIEF_NAME"));
                individual.setDeputyChiefVillagePhoneNumber(jsonApplicant.getString("DEPUTY_CHIEF_PHONE"));
                individual.setDeputyChiefVillageName(jsonApplicant.getString("DEPUTY_CHIEF_NAME"));
                //City of birth
                String cityOfBirth = jsonApplicant.getString("CITY_OF_BIRTH");
                if (cityOfBirth != null && cityOfBirth != "") {
                    if (("Cambodia".compareToIgnoreCase(cityOfBirth.trim())) == 0) {
                        individual.setBirthCountry(ECountry.KHM);
                    }
                }

                individual.setMobilePerso(jsonApplicant.getString("MOBILE_PHONE"));
                individual.setMobilePerso2(jsonApplicant.getString("MOBILE_PHONE_2"));
                try {
                    individual.setBirthDate(formatter.parse(jsonApplicant.getString("BIRTH_DATE")));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                individual.setTotalFamilyMember(jsonApplicant.getInt("TOTAL_FAMILY_MEMBER"));

                //Individual Spouse
                List<IndividualSpouse> individualSpouses = new ArrayList<>();
                IndividualSpouse individualSpouse = new IndividualSpouse();
                individualSpouse.setFirstName("");
                individualSpouse.setLastName("");
                individualSpouse.setNickName("");
                individualSpouse.setMiddleName("");
                individualSpouse.setMobilePhone(jsonApplicant.getString("SPOUSE_MOBILE"));
                //individualSpouse.setIndividual(individual);

                individualSpouses.add(individualSpouse);
                individual.setIndividualSpouses(individualSpouses);

                //Place of birth
                JSONObject jsonBirthPlace = jsonApplicant.getJSONObject("PLACE_OF_BIRTH");
                Province provOfBirth = new Province();
                provOfBirth.setId(jsonBirthPlace.getLong("ID"));
                provOfBirth.setCode(jsonBirthPlace.getString("CODE"));
                provOfBirth.setDesc(jsonBirthPlace.getString("DESC"));
                provOfBirth.setDescEn(jsonBirthPlace.getString("DESC_EN"));
                try {
                    provOfBirth.setUpdateDate(formatter.parse(jsonBirthPlace.getString("UPDATE_DATE")));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                //province.setUpdateDate();
                individual.setPlaceOfBirth(provOfBirth);

                //Nationality
                Long nationality = jsonApplicant.getLong("NATIONALITY");
                if (nationality == 1l) {
                    individual.setNationality(ENationality.KHMER);
                }

                //Marital Status
                String maritalStatus = jsonApplicant.getString("MARITAL_STATUS");
                if (this.getMaritialStatus(maritalStatus) != null) {
                    individual.setMaritalStatus(this.getMaritialStatus(maritalStatus));
                }

                //Address
                IndividualAddress indAddr = new IndividualAddress();
                JSONObject jsonAddr = jsonApplicant.getJSONObject("ADDRESS");

                if (jsonAddr != null) {
                    indAddr.setTimeAtAddressInYear(jsonAddr.getInt("TIME_YEAR"));
                    indAddr.setTimeAtAddressInMonth(jsonAddr.getInt("TIME_MONTH"));
                    indAddr.setZipCode(jsonAddr.getString("ZIP_CODE"));

                    Address address = new Address();
                    address.setHouseNo(jsonAddr.getString("HOUSE_NO"));
                    address.setStreet(jsonAddr.getString("STREET"));

                    String country = jsonAddr.getString("COUNTRY");
                    if (("Cambodia".compareToIgnoreCase(country.trim())) == 0) {
                        address.setCountry(ECountry.KHM);
                    } else if (("Laos".compareToIgnoreCase(country.trim())) == 0) {
                        address.setCountry(ECountry.LAO);
                    }  //==>> Add more TH + VN
                    else {
                        address.setCountry(ECountry.KHM);
                    }


                    //Housing
                    JSONObject housing = jsonAddr.getJSONObject("HOUSING");
                    if (housing != null) {
                        String eHousingCode = this.getEHousingCode(housing.getString("CODE"));
                        if (eHousingCode != null) {
                            indAddr.setHousing(this.getRefDataObject(EHousing.class, eHousingCode));
                        }
                    }

                    //Province
                    JSONObject jsonProv = jsonAddr.getJSONObject("PROVIN");
                    Province province = new Province();
                    province.setId(jsonProv.getLong("ID"));
                    province.setCode(jsonProv.getString("CODE"));
                    province.setDesc(jsonProv.getString("DESC"));
                    province.setDescEn(jsonProv.getString("DESC_EN"));
                    try {
                        province.setUpdateDate(formatter.parse(jsonProv.getString("UPDATE_DATE")));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    //District
                    JSONObject jsonDistrict = jsonAddr.getJSONObject("DISTRICT");
                    District district = new District();
                    district.setId(jsonDistrict.getLong("ID"));
                    district.setCode(jsonDistrict.getString("CODE"));
                    district.setDesc(jsonDistrict.getString("DESC"));
                    district.setDescEn(jsonDistrict.getString("DESC_EN"));
                    try {
                        district.setUpdateDate(formatter.parse(jsonDistrict.getString("UPDATE_DATE")));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    district.setProvince(province);

                    //Commune
                    JSONObject jsonCommune = jsonAddr.getJSONObject("COMMUNE");
                    Commune commune = new Commune();
                    commune.setId(jsonCommune.getLong("ID"));
                    commune.setCode(jsonCommune.getString("CODE"));
                    commune.setDesc(jsonCommune.getString("DESC"));
                    commune.setDescEn(jsonCommune.getString("DESC_EN"));
                    try {
                        commune.setUpdateDate(formatter.parse(jsonCommune.getString("UPDATE_DATE")));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    commune.setDistrict(district);

                    //Village
                    JSONObject jsonVillage = jsonAddr.getJSONObject("VILLAGE");
                    Village villeage = new Village();
                    villeage.setId(jsonVillage.getLong("ID"));
                    villeage.setCode(jsonVillage.getString("CODE"));
                    villeage.setDesc(jsonVillage.getString("DESC"));
                    villeage.setDescEn(jsonVillage.getString("DESC_EN"));
                    try {
                        villeage.setUpdateDate(formatter.parse(jsonVillage.getString("UPDATE_DATE")));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    villeage.setCommune(commune);

                    //add to address
                    address.setProvince(province);
                    address.setDistrict(district);
                    address.setCommune(commune);
                    address.setVillage(villeage);
                    address.setType(ETypeAddress.MAIN);

                    //Add to individual addr
                    indAddr.setAddress(address);


                    List<IndividualAddress> lstIndividualAddress = new ArrayList<>();
                    lstIndividualAddress.add(indAddr);

                    //set individual address to individual
                    individual.setIndividualAddresses(lstIndividualAddress);

                    //else wont work
                    individual.setECertifyCurrentAddress(ECertifyCurrentAddress.TEST);
                    individual.setECertifyIncome(ECertifyIncome.TEST);
                    individual.setECorrespondence(ECorrespondence.CURRENT_ADDRESS);

                    /*Employment employment = new Employment();
                    employment.setEmploymentType(EEmploymentType.CURR);
                    employment.setWorkPlaceName("ABCD");

                    List<Employment> employments = new ArrayList<Employment>();
                    employments.add(employment);

                    individual.setEmployments(employments);*/

                    applicant = new Applicant();
                    applicant.setIndividual(individual);
                    applicant.setApplicantCategory(EApplicantCategory.INDIVIDUAL);
                }
            }
        }

        quotation.setApplicant(applicant);
        return applicant;
    }


    public void setApplicationPanel(ApplicationPanel applicationPanel) {
        this.applicationPanel = applicationPanel;
    }


    @SuppressWarnings("Duplicates")
    private <T extends RefDataId> T getRefDataObject(Class<T> klass, String code) {
        if (klass != null && code != null && !code.isEmpty()) {
            Map<Long, T> tmp = (Map<Long, T>) ERefDataHelper.refData.get(klass.getName());
            for (Map.Entry<Long, T> entry : tmp.entrySet()) {
                if (entry.getValue().getCode().equals(code))
                    return entry.getValue();
            }
        }
        return null;
    }


    public EMaritalStatus getMaritialStatus(String maritalStatus) {
        switch (maritalStatus.trim().toLowerCase()) {
            case "married":
                return EMaritalStatus.getByCode("M");
            case "single":
                return EMaritalStatus.getByCode("S");
            case "widow.widower":
                return EMaritalStatus.getByCode("W");
            case "divorced":
                return EMaritalStatus.getByCode("D");
            case "separated":
                return EMaritalStatus.getByCode("P");
            case "defacto":
                return EMaritalStatus.getByCode("C");
            default:
                return EMaritalStatus.UNKNOWN;
        }
    }

    public String getEHousingCode(String code) {
        String eHousingCode = null;
        if (code.equals("01")) { //owner //config in ra
            //set owner object to individual
            eHousingCode = "owner";
        } else if (code.equals("02")) { //parent
            eHousingCode = "parent";
        } else if (code.equals("03")) { //rental
            eHousingCode = "rental";
        } else if (code.equals("04")) { //relative
            eHousingCode = "relative";
        } else if (code.equals("05")) { //other
            eHousingCode = "other";
        }
        return eHousingCode;
    }

    private Document getDocumentByCode(Long id, Long gid) {
        if (id == null || gid == null) return null;
        BaseRestrictions<Document> restrictions = new BaseRestrictions<>(Document.class);
        restrictions.addCriterion(Restrictions.eq("id", id));
        restrictions.addCriterion(Restrictions.eq("documentGroup.id", gid));
        List<Document> lstDocuments = ENTITY_SRV.list(restrictions);
        if (lstDocuments != null && !lstDocuments.isEmpty()) {
            return lstDocuments.get(0);
        }
        return null;
    }
}
