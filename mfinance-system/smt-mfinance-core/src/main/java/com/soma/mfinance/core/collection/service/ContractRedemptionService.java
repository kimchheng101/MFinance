package com.soma.mfinance.core.collection.service;

import org.seuksa.frmk.service.BaseEntityService;

/**
 * Contract Other data service interface
 * @author kimsuor.seang
 */
public interface ContractRedemptionService extends BaseEntityService {

	/**
	 * calculateRedemptionPeriod
	 */
	void calculateRedemptionPeriod();

}
