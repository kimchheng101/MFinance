package com.soma.mfinance.core.common.reference.model;

import com.soma.mfinance.core.applicant.model.EApplicantType;
import org.seuksa.frmk.model.eref.BaseERefData;

import javax.persistence.AttributeConverter;
import java.util.List;

/**
 * Created by Kimsuor SEANG
 * Date  : 6/9/2017
 * Name  : 3:35 PM
 * Email : k.seang@gl-f.com
 */
public class EComponetType extends BaseERefData implements AttributeConverter<EComponetType, Long> {
    private static final long serialVersionUID = -818020523286600621L;

    public static final EComponetType COMBOBOX = new EComponetType("cbx",1);
    public static final EComponetType TEXTFILED = new EComponetType("txt",2);

    /**
     */
    public EComponetType() {
    }

    /**
     * @param code
     * @param id
     */
    public EComponetType(String code, long id) {
        super(code, id);
    }

    @Override
    public Long convertToDatabaseColumn(EComponetType attribute) {
        return super.convertToDatabaseColumn(attribute);
    }

    @Override
    public EComponetType convertToEntityAttribute(Long dbData) {
        return super.convertToEntityAttribute(dbData);
    }


    /**
     *
     * @return
     */
    public static List<EComponetType> values() {
        return getValues(EComponetType.class);
    }

    /**
     *
     * @param code
     * @return
     */
    public static EComponetType getByCode(String code) {
        return getByCode(EComponetType.class, code);
    }

    /**
     *
     * @param id
     * @return
     */
    public static EComponetType getById(long id) {
        return getById(EComponetType.class, id);
    }
}
