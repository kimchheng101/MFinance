package com.soma.mfinance.core.asset.panel.appraisal;


import com.soma.mfinance.core.asset.model.appraisal.*;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.mfinance.third.creditbureau.cbc.model.EProductType;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.data.Property;
import com.vaadin.event.FieldEvents;
import com.vaadin.server.Page;
import com.vaadin.ui.*;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.CrudAction;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyNumberUtils;
import sun.rmi.runtime.Log;

import java.text.DecimalFormat;
import java.util.*;

import static com.soma.mfinance.core.helper.FinServicesHelper.ASSET_APPRAISAL_SERVICE;

/**
 * @author by kimsuor.seang  on 10/9/2017.
 */

public class AppraisalKFPPanel extends AbstractTabPanel {

    private static final long serialVersionUID = 1513967976783351757L;
    private Label lblItem, space, width, height, lblChangePrice, lblFixPrice, lblItemName, lblHistory, lblPercent, lblMonthsOfUse,
            lblHoursOfUse, lblBrandNew, lblSecondHand, lblDepreciationMonth, lblDepreciation, lblCostOfRepair, lblCostOfChange,
            lblPriceBeforeAdjustment, lblAdjustment, lblPriceAfterAdjustment, lblLeaseAmount, lblPriceBeforeAdjustmentAppraisal,
            lblAdjustmentAppraisal, lblPriceAfterAdjustmentAppraisal, lblLeaseAmountAppraisal, lblLeaseAmountCommittee, lblAdjustValue;
    private TextField txtBrandNew, txtSecondHand, txtDepreciationMonth, txtDepreciation, txtCostOfRepair, txtCostOfChange,
            txtPriceBeforeAdjustment, txtAdjustment, txtPriceAfterAdjustment, txtLeaseAmount, txtPriceBeforeAdjustmentAppraisal,
            txtAdjustmentAppraisal, txtPriceAfterAdjustmentAppraisal, txtLeaseAmountAppraisal, txtLeaseAmountCommittee,
            txtMonthsOfUse, txtHoursOfUse;
    private Map<Long, List<AssetHistoryItem>> mapAssetHistory;
    private Map<Long, List<AppraisalItem>> mapAppraisalCategory;
    private Map<Long, String> mapFixPrice, mapChangePrice, mapAdjustValue, mapPercent;
    private Map<Long, TextField> mapTxtFixPrice, mapTxtChangePrice, mapTxtAdjustValue;
    private Map<Long, CheckBox> mapCbFixPrice, mapCbChangePrice, mapCbHistory;
    private Map<Long, Label> mapLblHistory;
    private TextField txtFixPrice, txtChangePrice, txtAjustValue;
    private CheckBox cbFixPrice, cbChangePrice, cbHistory;
    private Double fixCost, changeCost, depreciationMonth, beforeAdjustment, assetPrice, ajustment, leaseAmount;
    private List<AppraisalCategory> appraisalCategories = new ArrayList<>();
    private List<AssetHistoryCategory> assetHistoryCategories = new ArrayList<>();
    private List<TextField> lstFixPrice, lstChangePrice = new ArrayList<>();
    private Quotation quotation;
    private HistoryItem historyItem;
    private Boolean condition;
    private Panel panel;
    private Long depreciation = 0l;
    private Integer percent = 0;
    private AssetAppraisal assetAppraisal;
    private List<AppraisalItem> appraisalItems = new ArrayList<>();
    private List<AssetHistoryItem> assetHistoryItems = new ArrayList<>();
    private List<Appraisal> appraisalList = new ArrayList<>();
    private List<HistoryItem> historyItemList = new ArrayList<>();
    private Double leaseAmountPercent = 0d;

    public AppraisalKFPPanel() {
        super();
        setSizeFull();
    }

    @Override
    protected Component createForm() {
        panel = new Panel(I18N.message("kubota.for.plus"));

        leaseAmountPercent = ASSET_APPRAISAL_SERVICE.getLeaseAmountPercent("K4P-LS");

        createAppraisalPanel();
        return panel;
    }

    public void createAppraisalPanel() {
        mapAppraisalCategory = new HashMap<>();
        mapFixPrice = new HashMap<>();
        mapChangePrice = new HashMap<>();
        mapAdjustValue = new HashMap<>();
        mapTxtFixPrice = new HashMap<>();
        mapTxtAdjustValue = new HashMap<>();
        mapTxtChangePrice = new HashMap<>();
        mapCbFixPrice = new HashMap<>();
        mapCbChangePrice = new HashMap<>();
        appraisalItems = new ArrayList<>();
        if (quotation != null && quotation.getAsset() != null) {
            VerticalLayout typePanel = new VerticalLayout();
            HorizontalLayout items;
            HorizontalLayout marginLayout;
            lstFixPrice = new ArrayList<>();
            lstChangePrice = new ArrayList<>();
            Panel categoryPanel;
            VerticalLayout itemLayout;
            if (quotation != null && quotation.getAsset() != null && quotation.getAsset().getAssetRange() != null)
                appraisalCategories = ASSET_APPRAISAL_SERVICE.getAppraisalCategoriesByAssetRang(quotation.getAsset().getAssetRange());
            if (appraisalCategories != null) {
                for (AppraisalCategory assetAppraisalCategory : appraisalCategories) {
                    categoryPanel = new Panel(assetAppraisalCategory.getDescEn());
                    itemLayout = new VerticalLayout();
                    itemLayout.setMargin(true);
                    condition = true;
                    lblItemName = new Label();
                    lblItemName.setWidth("320px");
                    lblChangePrice = new Label("Change Cost");
                    lblChangePrice.setWidth("150px");
                    lblFixPrice = new Label("Repairable Cost");
                    lblFixPrice.setWidth("150px");
                    lblAdjustValue = new Label("Adjusted Value By Appraisal");
                    lblAdjustValue.setWidth("150px");
                    appraisalItems = ASSET_APPRAISAL_SERVICE.getAppraisalItemByCategory(assetAppraisalCategory);
                    for (final AppraisalItem assetAppraisalItem : appraisalItems) {
                        width = new Label();
                        width.setWidth("20px");
                        height = new Label();
                        height.setHeight("5px");
                        space = new Label();
                        space.setWidth("40px");
                        items = new HorizontalLayout();
                        items.setSizeFull();
                        lblItem = new Label(assetAppraisalItem.getDescEn());
                        lblItem.setWidth("300px");
                        txtFixPrice = ComponentFactory.getTextField(false, 50, 150);
                        txtChangePrice = ComponentFactory.getTextField(false, 50, 150);
                        txtAjustValue = ComponentFactory.getTextField(false, 50, 150);
                        txtFixPrice.setEnabled(false);
                        txtChangePrice.setEnabled(false);
                        cbFixPrice = new CheckBox();
                        cbChangePrice = new CheckBox();
                        cbChangePrice.setValue(false);
                        if (condition == true) {
                            items.addComponent(lblItemName);
                            items.addComponent(lblFixPrice);
                            items.addComponent(space);
                            items.addComponent(lblChangePrice);
                            space = new Label();
                            space.setWidth("20px");
                            items.addComponent(space);
                            items.addComponent(lblAdjustValue);
                            itemLayout.addComponent(items);
                            itemLayout.addComponent(height);
                            items = new HorizontalLayout();
                            width = new Label();
                            width.setWidth("20px");
                            height = new Label();
                            height.setHeight("5px");
                        }
                        condition = false;
                        items.addComponent(lblItem);
                        items.addComponent(cbFixPrice);
                        items.addComponent(txtFixPrice);
                        items.addComponent(width);
                        items.addComponent(cbChangePrice);
                        items.addComponent(txtChangePrice);
                        width = new Label();
                        width.setWidth("20px");
                        items.addComponent(width);
                        items.addComponent(txtAjustValue);
                        itemLayout.addComponent(height);
                        itemLayout.addComponent(items);
                        lstFixPrice.add(txtFixPrice);
                        lstChangePrice.add(txtChangePrice);

                        mapFixPrice.put(assetAppraisalItem.getId(), txtFixPrice.getValue());
                        mapTxtFixPrice.put(assetAppraisalItem.getId(), txtFixPrice);

                        mapChangePrice.put(assetAppraisalItem.getId(), txtChangePrice.getValue());
                        mapTxtChangePrice.put(assetAppraisalItem.getId(), txtChangePrice);

                        mapAdjustValue.put(assetAppraisalItem.getId(), txtAjustValue.getValue());
                        mapTxtAdjustValue.put(assetAppraisalItem.getId(), txtAjustValue);

                        cbFixPrice.addValueChangeListener(new Property.ValueChangeListener() {
                            private static final long serialVersionUID = 8163610088054182170L;

                            @Override
                            public void valueChange(Property.ValueChangeEvent event) {
                                if (mapCbFixPrice.get(assetAppraisalItem.getId()).getValue() == true) {
                                    if (assetAppraisalItem.getFixPrice() != null) {
                                        mapTxtFixPrice.get(assetAppraisalItem.getId()).setValue(assetAppraisalItem.getFixPrice().toString());
                                        mapFixPrice.put(assetAppraisalItem.getId(), assetAppraisalItem.getFixPrice().toString());
                                        fixCost = fixCost + assetAppraisalItem.getFixPrice();
                                    }
                                    mapCbChangePrice.get(assetAppraisalItem.getId()).setEnabled(false);
                                    if (assetAppraisalItem.getChangePrice() == 0) {
                                        mapCbFixPrice.get(assetAppraisalItem.getId()).setEnabled(false);
                                    }
                                } else {
                                    mapTxtFixPrice.get(assetAppraisalItem.getId()).setValue("");
                                    if (assetAppraisalItem.getFixPrice() != null)
                                        fixCost = fixCost - assetAppraisalItem.getFixPrice();
                                    mapCbChangePrice.get(assetAppraisalItem.getId()).setEnabled(true);
                                    if (assetAppraisalItem.getChangePrice() == 0) {
                                        mapCbFixPrice.get(assetAppraisalItem.getId()).setEnabled(false);
                                    }
                                    mapFixPrice.put(assetAppraisalItem.getId(), "");
                                }
                                txtCostOfRepair.setValue(fixCost.toString());
                                beforeAdjustment = Double.parseDouble(txtBrandNew.getValue()) - (fixCost + changeCost + depreciation);
                                txtPriceBeforeAdjustment.setValue(new DecimalFormat("#.##").format(beforeAdjustment));
                                ajustment = ((percent * beforeAdjustment) / 100);
                                txtPriceAfterAdjustment.setValue(new DecimalFormat("#.##").format(beforeAdjustment - ajustment));
                                if (leaseAmountPercent != null)
                                    leaseAmount = (beforeAdjustment - ajustment) * leaseAmountPercent / 100;
                                txtLeaseAmount.setValue(new DecimalFormat("#.##").format(leaseAmount));
                            }
                        });
                        cbChangePrice.addValueChangeListener(new Property.ValueChangeListener() {
                            private static final long serialVersionUID = -1548441731346996149L;

                            @Override
                            public void valueChange(Property.ValueChangeEvent event) {
                                if (mapCbChangePrice.get(assetAppraisalItem.getId()).getValue() == true) {
                                    if (assetAppraisalItem.getChangePrice() != null) {
                                        mapTxtChangePrice.get(assetAppraisalItem.getId()).setValue(assetAppraisalItem.getChangePrice().toString());
                                        mapChangePrice.put(assetAppraisalItem.getId(), assetAppraisalItem.getChangePrice().toString());
                                        changeCost = changeCost + assetAppraisalItem.getChangePrice();
                                    }
                                    mapCbFixPrice.get(assetAppraisalItem.getId()).setEnabled(false);
                                    if (assetAppraisalItem.getChangePrice() == 0) {
                                        mapCbFixPrice.get(assetAppraisalItem.getId()).setEnabled(false);
                                    }
                                } else {
                                    mapTxtChangePrice.get(assetAppraisalItem.getId()).setValue("");
                                    if (assetAppraisalItem.getChangePrice() != null)
                                        changeCost = changeCost - assetAppraisalItem.getChangePrice();
                                    mapCbFixPrice.get(assetAppraisalItem.getId()).setEnabled(true);
                                    if (assetAppraisalItem.getFixPrice() == 0) {
                                        mapCbFixPrice.get(assetAppraisalItem.getId()).setEnabled(false);
                                    }
                                    mapChangePrice.put(assetAppraisalItem.getId(), "");
                                }
                                txtCostOfChange.setValue(changeCost.toString());
                                beforeAdjustment = Double.parseDouble(txtBrandNew.getValue()) - (fixCost + changeCost + depreciation);
                                txtPriceBeforeAdjustment.setValue(new DecimalFormat("#.##").format(beforeAdjustment));
                                ajustment = ((percent * beforeAdjustment) / 100);
                                txtPriceAfterAdjustment.setValue(new DecimalFormat("#.##").format(beforeAdjustment - ajustment));
                                if (leaseAmountPercent != null)
                                    leaseAmount = (beforeAdjustment - ajustment) * leaseAmountPercent / 100;
                                txtLeaseAmount.setValue(new DecimalFormat("#.##").format(leaseAmount));

                            }
                        });
                        mapCbFixPrice.put(assetAppraisalItem.getId(), cbFixPrice);
                        mapCbChangePrice.put(assetAppraisalItem.getId(), cbChangePrice);

                        txtAjustValue.addValueChangeListener(new Property.ValueChangeListener() {
                            private static final long serialVersionUID = -1553413446248134881L;

                            @Override
                            public void valueChange(Property.ValueChangeEvent event) {
                                adjustValues();
                            }
                        });

                        txtAjustValue.addBlurListener(new FieldEvents.BlurListener() {
                            private static final long serialVersionUID = 5142613800883521775L;

                            @Override
                            public void blur(FieldEvents.BlurEvent event) {
                                adjustValues();
                            }
                        });
                    }
                    categoryPanel.setContent(itemLayout);
                    marginLayout = new HorizontalLayout();
                    marginLayout.setMargin(true);
                    marginLayout.addComponent(categoryPanel);
                    typePanel.addComponent(marginLayout);

                    //PUT APPRAISAL ITEM BY CATEGORY ID
                    mapAppraisalCategory.put(assetAppraisalCategory.getId(), appraisalItems);
                }
            }
            HorizontalLayout col = new HorizontalLayout();
            VerticalLayout row = new VerticalLayout();
            VerticalLayout verticalLayout = new VerticalLayout();
            row.setMargin(true);
            row.addComponent(createHistoryPanel());
            verticalLayout.addComponent(row);
            row = new VerticalLayout();
            row.setMargin(true);
            row.addComponent(createTotalPanel());
            verticalLayout.addComponent(row);
            col.addComponent(typePanel);
            col.addComponent(verticalLayout);
            panel.setContent(col);
        }
    }

    public void adjustValues() {
        if (!txtPriceAfterAdjustment.getValue().isEmpty()) {
            Double afterAdjustmentAppraisal = Double.parseDouble(txtPriceAfterAdjustment.getValue()) - getAdjustValue();
            txtPriceAfterAdjustmentAppraisal.setValue(afterAdjustmentAppraisal.toString());
            Double LeaseAmountAppraisal = afterAdjustmentAppraisal * leaseAmountPercent / 100;
            txtLeaseAmountAppraisal.setValue(LeaseAmountAppraisal.toString());
        } else {
            txtPriceAfterAdjustmentAppraisal.setValue("0");
            txtLeaseAmountAppraisal.setValue("0");
        }
    }

    public Panel createHistoryPanel() {
        Panel panel = new Panel(I18N.message("hisotry"));
        panel.setWidth("450px");
        Panel categoryPanel;
        VerticalLayout itemsLayout;
        HorizontalLayout colItems;
        VerticalLayout categoryLayout;
        VerticalLayout verticalLayout = new VerticalLayout();
        mapAssetHistory = new HashMap<>();
        mapCbHistory = new HashMap<>();
        mapLblHistory = new HashMap<>();
        mapPercent = new HashMap<>();
        assetHistoryItems = new ArrayList<>();
        Integer i = 0;
        if (quotation != null && quotation.getAsset() != null) {
            if (quotation.getAsset().getAssetRange() != null)
                assetHistoryCategories = ASSET_APPRAISAL_SERVICE.getAssetHistoryCategories(quotation.getAsset().getAssetRange());
            for (AssetHistoryCategory assetHistoryCategory : assetHistoryCategories) {
                categoryPanel = new Panel(assetHistoryCategory.getDescEn());
                itemsLayout = new VerticalLayout();
                itemsLayout.setMargin(true);
                assetHistoryItems = ASSET_APPRAISAL_SERVICE.getAssetHistoryItems(assetHistoryCategory);
                for (final AssetHistoryItem assetHistoryItem : assetHistoryItems) {
                    colItems = new HorizontalLayout();
                    cbHistory = new CheckBox();
                    lblHistory = new Label(assetHistoryItem.getDescEn());
                    lblHistory.setWidth("220px");
                    lblPercent = new Label("0%");
                    lblPercent.setWidth("100px");
                    height = new Label();
                    height.setHeight("5px");
                    width = new Label();
                    width.setWidth("20px");
                    colItems.addComponent(lblHistory);
                    colItems.addComponent(cbHistory);
                    colItems.addComponent(width);
                    colItems.addComponent(lblPercent);
                    itemsLayout.addComponent(height);
                    itemsLayout.addComponent(colItems);
                    mapCbHistory.put(assetHistoryItem.getId(), cbHistory);
                    mapLblHistory.put(assetHistoryItem.getId(), lblPercent);
                    cbHistory.addValueChangeListener(new Property.ValueChangeListener() {

                        private static final long serialVersionUID = -1553413446248134881L;

                        @Override
                        public void valueChange(Property.ValueChangeEvent event) {
                            if (mapCbHistory.get(assetHistoryItem.getId()).getValue() == true) {
                                if (assetHistoryItem.getPercent() != null) {
                                    mapLblHistory.get(assetHistoryItem.getId()).setValue(assetHistoryItem.getPercent() + "%");
                                    mapPercent.put(assetHistoryItem.getId(), assetHistoryItem.getPercent().toString());
                                    percent = percent + assetHistoryItem.getPercent();
                                }
                                /*if (ASSET_APPRAISAL_SERVICE.getGroupAppraisalItem(quotation) != null && ASSET_APPRAISAL_SERVICE.getGroupAppraisalItem(quotation).getPriceOfBrandNew() != null) {
                                    beforeAdjustment = ASSET_APPRAISAL_SERVICE.getGroupAppraisalItem(quotation).getPriceOfBrandNew() - (fixCost + changeCost + depreciation);
                                } else {
                                    beforeAdjustment = Double.parseDouble(txtBrandNew.getValue()) - (fixCost + changeCost + depreciation);
                                    ajustment = ((percent * beforeAdjustment) / 100);
                                    txtAdjustment.setValue(new DecimalFormat("#.##").format(ajustment));
                                    txtPriceAfterAdjustment.setValue(new DecimalFormat("#.##").format(beforeAdjustment - ajustment));
                                    if (leaseAmountPercent != null) {
                                        leaseAmount = (beforeAdjustment - ajustment) * leaseAmountPercent / 100;
                                    }
                                    txtLeaseAmount.setValue(new DecimalFormat("#.##").format(leaseAmount));
                                }*/
                            } else {
                                mapPercent.put(assetHistoryItem.getId(), null);
                                if (assetHistoryItem.getPercent() != null) {
                                    percent = percent - assetHistoryItem.getPercent();
                                }
                                mapLblHistory.get(assetHistoryItem.getId()).setValue("0%");
                                beforeAdjustment = Double.parseDouble(txtBrandNew.getValue()) - (fixCost + changeCost + depreciation);
                                ajustment = ((percent * beforeAdjustment) / 100);
                                txtAdjustment.setValue(new DecimalFormat("#.##").format(ajustment));
                                txtPriceAfterAdjustment.setValue(new DecimalFormat("#.##").format(beforeAdjustment - ajustment));
                                if (leaseAmountPercent != null) {
                                    leaseAmount = (beforeAdjustment - ajustment) * leaseAmountPercent / 100;
                                }
                                txtLeaseAmount.setValue(new DecimalFormat("#.##").format(leaseAmount));
                            }
                        }
                    });
                    mapAssetHistory.put(assetHistoryCategory.getId(), assetHistoryItems);
                }
                categoryPanel.setContent(itemsLayout);
                categoryLayout = new VerticalLayout();
                categoryLayout.setMargin(true);
                categoryLayout.addComponent(categoryPanel);
                verticalLayout.addComponent(categoryLayout);
            }
        }
        panel.setContent(verticalLayout);
        return panel;
    }


    public Panel createTotalPanel() {
        Panel panel = new Panel(I18N.message("Total"));
        panel.setWidth("450px");
        txtMonthsOfUse = ComponentFactory.getTextField(false, 50, 150);
        lblMonthsOfUse = new Label(I18N.message("months.of.use"));
        lblMonthsOfUse.setWidth("200px");
        txtHoursOfUse = ComponentFactory.getTextField(false, 50, 150);
        lblHoursOfUse = new Label(I18N.message("hours.of.use"));
        lblHoursOfUse.setWidth("200px");
        txtBrandNew = ComponentFactory.getTextField(false, 50, 150);
        txtBrandNew.setEnabled(false);
        lblBrandNew = new Label(I18N.message("price.brand.new"));
        lblBrandNew.setWidth("200px");
        txtSecondHand = ComponentFactory.getTextField(false, 50, 150);
        lblSecondHand = new Label(I18N.message("price.second.hand"));
        lblSecondHand.setWidth("200px");
        txtDepreciationMonth = ComponentFactory.getTextField(false, 50, 150);
        txtDepreciationMonth.setEnabled(false);
        lblDepreciationMonth = new Label(I18N.message("depreciate.month"));
        lblDepreciationMonth.setWidth("200px");
        txtDepreciation = ComponentFactory.getTextField(false, 50, 150);
        txtDepreciation.setEnabled(false);
        lblDepreciation = new Label(I18N.message("depreciate"));
        lblDepreciation.setWidth("180px");
        txtCostOfRepair = ComponentFactory.getTextField(false, 50, 150);
        txtCostOfRepair.setEnabled(false);
        lblCostOfRepair = new Label(I18N.message("cost.repair"));
        lblCostOfRepair.setWidth("180px");
        txtCostOfChange = ComponentFactory.getTextField(false, 50, 150);
        txtCostOfChange.setEnabled(false);
        lblCostOfChange = new Label(I18N.message("cost.change"));
        lblCostOfChange.setWidth("180px");
        txtPriceBeforeAdjustment = ComponentFactory.getTextField(false, 50, 150);
        txtPriceBeforeAdjustment.setEnabled(false);
        lblPriceBeforeAdjustment = new Label(I18N.message("price.before.adjustment"));
        lblPriceBeforeAdjustment.setWidth("220px");
        txtAdjustment = ComponentFactory.getTextField(false, 50, 150);
        txtAdjustment.setEnabled(false);
        lblAdjustment = new Label(I18N.message("adjustment"));
        lblAdjustment.setWidth("220px");
        txtPriceAfterAdjustment = ComponentFactory.getTextField(false, 50, 150);
        txtPriceAfterAdjustment.setEnabled(false);
        lblPriceAfterAdjustment = new Label(I18N.message("price.after.adjustment"));
        lblPriceAfterAdjustment.setWidth("220px");
        txtLeaseAmount = ComponentFactory.getTextField(false, 50, 150);
        txtLeaseAmount.setEnabled(false);
        lblLeaseAmount = new Label(I18N.message("leaseamount"));
        lblLeaseAmount.setWidth("220px");

        txtPriceBeforeAdjustmentAppraisal = ComponentFactory.getTextField(false, 50, 150);
        txtPriceBeforeAdjustmentAppraisal.setEnabled(false);
        lblPriceBeforeAdjustmentAppraisal = new Label(I18N.message("price.before.adjustment"));
        lblPriceBeforeAdjustmentAppraisal.setWidth("220px");
        txtAdjustmentAppraisal = ComponentFactory.getTextField(false, 50, 150);
        txtAdjustmentAppraisal.setEnabled(false);
        lblAdjustmentAppraisal = new Label(I18N.message("adjustment"));
        lblAdjustmentAppraisal.setWidth("220px");
        txtPriceAfterAdjustmentAppraisal = ComponentFactory.getTextField(false, 50, 150);
        txtPriceAfterAdjustmentAppraisal.setEnabled(false);
        lblPriceAfterAdjustmentAppraisal = new Label(I18N.message("price.after.adjustment"));
        lblPriceAfterAdjustmentAppraisal.setWidth("220px");
        txtLeaseAmountAppraisal = ComponentFactory.getTextField(false, 50, 150);
        txtLeaseAmountAppraisal.setEnabled(false);
        lblLeaseAmountAppraisal = new Label(I18N.message("leaseamount"));
        lblLeaseAmountAppraisal.setWidth("220px");

        txtLeaseAmountCommittee = ComponentFactory.getTextField(false, 50, 150);
        txtLeaseAmountCommittee.setEnabled(false);
        lblLeaseAmountCommittee = new Label(I18N.message("leaseamount"));
        lblLeaseAmountCommittee.setWidth("220px");

        VerticalLayout verticalLayout = new VerticalLayout();
        VerticalLayout totalLayout = new VerticalLayout();
        totalLayout.setMargin(true);
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(lblMonthsOfUse);
        horizontalLayout.addComponent(txtMonthsOfUse);
        totalLayout.addComponent(horizontalLayout);
        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(lblHoursOfUse);
        horizontalLayout.addComponent(txtHoursOfUse);
        height = new Label();
        height.setHeight("5px");
        totalLayout.addComponent(height);
        totalLayout.addComponent(horizontalLayout);
        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(lblBrandNew);
        horizontalLayout.addComponent(txtBrandNew);
        height = new Label();
        height.setHeight("5px");
        totalLayout.addComponent(height);
        totalLayout.addComponent(horizontalLayout);
        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(lblSecondHand);
        horizontalLayout.addComponent(txtSecondHand);
        height = new Label();
        height.setHeight("5px");
        totalLayout.addComponent(height);
        totalLayout.addComponent(horizontalLayout);
        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(lblDepreciationMonth);
        horizontalLayout.addComponent(txtDepreciationMonth);
        height = new Label();
        height.setHeight("5px");
        totalLayout.addComponent(height);
        totalLayout.addComponent(horizontalLayout);
        verticalLayout.addComponent(totalLayout);


        Panel losses = new Panel((I18N.message("losess")));
        totalLayout = new VerticalLayout();
        totalLayout.setMargin(true);
        VerticalLayout lossesLayout = new VerticalLayout();
        lossesLayout.setMargin(true);
        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(lblDepreciation);
        horizontalLayout.addComponent(txtDepreciation);
        lossesLayout.addComponent(horizontalLayout);
        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(lblCostOfRepair);
        horizontalLayout.addComponent(txtCostOfRepair);
        height = new Label();
        height.setHeight("5px");
        lossesLayout.addComponent(height);
        lossesLayout.addComponent(horizontalLayout);
        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(lblCostOfChange);
        horizontalLayout.addComponent(txtCostOfChange);
        height = new Label();
        height.setHeight("5px");
        lossesLayout.addComponent(height);
        lossesLayout.addComponent(horizontalLayout);
        losses.setContent(lossesLayout);
        totalLayout.addComponent(losses);
        verticalLayout.addComponent(totalLayout);

        Panel estimatedPrice = new Panel((I18N.message("estimated.price.co")));
        VerticalLayout estimatedLayout = new VerticalLayout();
        totalLayout = new VerticalLayout();
        totalLayout.setMargin(true);
        estimatedLayout.setMargin(true);
        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(lblPriceBeforeAdjustment);
        horizontalLayout.addComponent(txtPriceBeforeAdjustment);
        estimatedLayout.addComponent(horizontalLayout);
        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(lblAdjustment);
        horizontalLayout.addComponent(txtAdjustment);
        height = new Label();
        height.setHeight("5px");
        estimatedLayout.addComponent(height);
        estimatedLayout.addComponent(horizontalLayout);
        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(lblPriceAfterAdjustment);
        horizontalLayout.addComponent(txtPriceAfterAdjustment);
        height = new Label();
        height.setHeight("5px");
        estimatedLayout.addComponent(height);
        estimatedLayout.addComponent(horizontalLayout);
        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(lblLeaseAmount);
        horizontalLayout.addComponent(txtLeaseAmount);
        height = new Label();
        height.setHeight("5px");
        estimatedLayout.addComponent(height);
        estimatedLayout.addComponent(horizontalLayout);
        estimatedPrice.setContent(estimatedLayout);
        totalLayout.addComponent(estimatedPrice);
        verticalLayout.addComponent(totalLayout);

        Panel estimatedPriceAppraisal = new Panel((I18N.message("estimated.price.ap")));
        VerticalLayout estimatedLayoutAppraisal = new VerticalLayout();
        totalLayout = new VerticalLayout();
        totalLayout.setMargin(true);
        estimatedLayoutAppraisal.setMargin(true);
        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(lblPriceBeforeAdjustmentAppraisal);
        horizontalLayout.addComponent(txtPriceBeforeAdjustmentAppraisal);
        estimatedLayoutAppraisal.addComponent(horizontalLayout);
        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(lblAdjustmentAppraisal);
        horizontalLayout.addComponent(txtAdjustmentAppraisal);
        height = new Label();
        height.setHeight("5px");
        estimatedLayoutAppraisal.addComponent(height);
        estimatedLayoutAppraisal.addComponent(horizontalLayout);
        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(lblPriceAfterAdjustmentAppraisal);
        horizontalLayout.addComponent(txtPriceAfterAdjustmentAppraisal);
        height = new Label();
        height.setHeight("5px");
        estimatedLayoutAppraisal.addComponent(height);
        estimatedLayoutAppraisal.addComponent(horizontalLayout);
        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(lblLeaseAmountAppraisal);
        horizontalLayout.addComponent(txtLeaseAmountAppraisal);
        height = new Label();
        height.setHeight("5px");
        estimatedLayoutAppraisal.addComponent(height);
        estimatedLayoutAppraisal.addComponent(horizontalLayout);
        estimatedPriceAppraisal.setContent(estimatedLayoutAppraisal);
        totalLayout.addComponent(estimatedPriceAppraisal);
        verticalLayout.addComponent(totalLayout);

        Panel estimatedPriceCommittee = new Panel((I18N.message("estimated.price.cc")));
        VerticalLayout estimatedLayoutCommittee = new VerticalLayout();
        totalLayout = new VerticalLayout();
        totalLayout.setMargin(true);
        estimatedLayoutCommittee.setMargin(true);
        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(lblLeaseAmountCommittee);
        horizontalLayout.addComponent(txtLeaseAmountCommittee);
        estimatedLayoutCommittee.addComponent(horizontalLayout);
        estimatedPriceCommittee.setContent(estimatedLayoutCommittee);
        totalLayout.addComponent(estimatedPriceCommittee);
        verticalLayout.addComponent(totalLayout);

        if (quotation != null) {
            if (quotation.getAsset() != null) {
                txtBrandNew.setValue(quotation.getAsset().getTeAssetApprPrice() != null ? quotation.getAsset().getTeAssetApprPrice().toString() : "0.00");
                if (quotation.getAsset().getTeAssetApprPrice() == null)
                    assetPrice = 0.0;
                else
                    assetPrice = quotation.getAsset().getTeAssetApprPrice();
                /*if (ASSET_APPRAISAL_SERVICE.getGroupAppraisalItem(quotation) != null) {
                    if (ASSET_APPRAISAL_SERVICE.getGroupAppraisalItem(quotation).getPriceOfBrandNew() != null) {
                        if (quotation.getAsset().getAssetRange() != null && quotation.getAsset().getAssetRange().getProductType() != null) {
                            if (quotation.getAsset().getAssetRange().getProductType() == EProductType.TRA) {
                                String st = assetPanel != null && assetPanel.getAssetModel() != null ? assetPanel.getAssetModel().getDesc() : null;
                                if (st != null && st.substring(0, 1).equals("M")) {
                                    //depreciationMonth = (getGroupAppraisalItem(quotation.getId()).getPriceOfBrandNew() - 5000 ) /5000 ;
                                    depreciationMonth = (Double.parseDouble(txtBrandNew.getValue()) - 5000) / 5000;
                                } else {
                                    depreciationMonth = (Double.parseDouble(txtBrandNew.getValue()) - 5000) / 6000;
                                }
                            } else if (quotation.getAsset().getAssetRange().getProductType() == EProductType.HAR) {
                                depreciationMonth = (Double.parseDouble(txtBrandNew.getValue()) - 500) / 3000;
                            } else if (quotation.getAsset().getAssetRange().getProductType() == EProductType.WTR) {
                                depreciationMonth = (Double.parseDouble(txtBrandNew.getValue()) - 300) / 60;
                            }
                        }
                    }

                } else {
                    if (quotation.getAsset().getAssetRange() != null && quotation.getAsset().getAssetRange().getProductType() != null) {
                        if (quotation.getAsset().getAssetRange().getProductType() == EProductType.TRA) {
                            String st = assetPanel != null && assetPanel.getAssetModel() != null ? assetPanel.getAssetModel().getDesc() : null;
                            if (st != null) {
                                if (st.substring(0, 1).equals("M")) {
                                    depreciationMonth = (quotation.getAsset().getTeAssetApprPrice() - 5000) / 5000;
                                } else {
                                    depreciationMonth = (quotation.getAsset().getTeAssetApprPrice() - 5000) / 6000;
                                }
                            }
                        } else if (quotation.getAsset().getAssetRange().getProductType() == EProductType.HAR) {
                            depreciationMonth = (Double.parseDouble(txtBrandNew.getValue()) - 500) / 3000;
                        } else if (quotation.getAsset().getAssetRange().getProductType() == EProductType.WTR) {
                            depreciationMonth = (Double.parseDouble(txtBrandNew.getValue()) - 300) / 60;
                        }
                    }
                }*/

                txtDepreciationMonth.setValue(new DecimalFormat("#.##").format(depreciationMonth));
            }
        }

        txtCostOfChange.setValue("0.0");
        txtCostOfRepair.setValue("0.0");
        txtDepreciation.setValue("0.0");
        txtPriceBeforeAdjustment.setValue(beforeAdjustment.toString());
        txtPriceAfterAdjustment.setValue("0.0");
        txtAdjustment.setValue(ajustment.toString());
        txtLeaseAmount.setValue(leaseAmount.toString());
        //setValueComponent();

        txtMonthsOfUse.addValueChangeListener(new Property.ValueChangeListener() {
            private static final long serialVersionUID = -7139183234106145993L;

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if (txtMonthsOfUse.getValue().isEmpty()) {
                    txtMonthsOfUse.setValue("0");
                }
                //setValueComponent();
            }
        });

        txtHoursOfUse.addValueChangeListener(new Property.ValueChangeListener() {
            private static final long serialVersionUID = 5632844573985738039L;

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if (txtHoursOfUse.getValue().isEmpty()) {
                    txtHoursOfUse.setValue("0");
                }
                if (Long.parseLong(txtHoursOfUse.getValue()) > 6000) {
                    Notification notification = new Notification("", Notification.Type.WARNING_MESSAGE);
                    //notification.setDescription(I18N.message("warning.hours.of.use", quotation.getSequence()));
                    notification.setDelayMsec(5000);
                    notification.show(Page.getCurrent());
                    txtHoursOfUse.setValue("0");
                } else {
                    //setValueComponent();
                }
            }
        });

        txtBrandNew.addValueChangeListener(new Property.ValueChangeListener() {
            private static final long serialVersionUID = -3243750030704890350L;

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                //setValueComponent();
            }
        });

        panel.setContent(verticalLayout);
        return panel;
    }


    public void assignValues(Quotation quotation) {
        this.quotation = quotation;
        this.removeAllComponents();
        super.init();
        AssetAppraisal assetAppraisal;
        List<HistoryItem> historyItems;
        createAppraisalPanel();
        if (quotation != null) {
            fixCost = 0d;
            changeCost = 0d;
            assetAppraisal = ASSET_APPRAISAL_SERVICE.getAssetAppraisalByAsset(quotation.getAsset());
            if (assetAppraisal != null) {
                if (assetAppraisal.getHoursOfUse() != null)
                    txtHoursOfUse.setValue(assetAppraisal.getHoursOfUse().toString());
                if (assetAppraisal.getMonthsOfUse() != null) {
                    txtMonthsOfUse.setValue(assetAppraisal.getMonthsOfUse().toString());
                }
                if (quotation.getAsset() != null && quotation.getAsset().getTeAssetPrice() != null) {
                    txtBrandNew.setValue(quotation.getAsset().getTeAssetPrice().toString());
                }
                if (assetAppraisal.getPriceOfSecondhand() != null) {
                    txtSecondHand.setValue(assetAppraisal.getPriceOfSecondhand().toString());
                }
                if (assetAppraisal.getDepreciatePerMonth() != null) {
                    txtDepreciationMonth.setValue(assetAppraisal.getDepreciatePerMonth().toString());
                }
                if (assetAppraisal.getDepreciate() != null) {
                    txtDepreciation.setValue(assetAppraisal.getDepreciate().toString());
                    if (assetAppraisal.getCostOfRepair() != null) {
                        txtCostOfRepair.setValue(assetAppraisal.getCostOfRepair().toString());
                    }
                    if (assetAppraisal.getCostOfRepair() != null) {
                        fixCost = assetAppraisal.getCostOfRepair();
                    }
                    if (assetAppraisal.getCostOfChange() != null) {
                        txtCostOfChange.setValue(assetAppraisal.getCostOfChange().toString());
                    }
                    if (assetAppraisal.getCostOfChange() != null) {
                        changeCost = assetAppraisal.getCostOfChange();
                    }
                }
                if (assetAppraisal.getPriceBeforeAdjustment() != null) {
                    txtPriceBeforeAdjustment.setValue(assetAppraisal.getPriceBeforeAdjustment().toString());
                }
                if (assetAppraisal.getAdjustment() != null) {
                    txtAdjustment.setValue(assetAppraisal.getAdjustment().toString());
                }
                if (assetAppraisal.getPriceAfterAdjustment() != null) {
                    txtPriceAfterAdjustment.setValue(assetAppraisal.getPriceAfterAdjustment().toString());
                }
                if (assetAppraisal.getLeaseAmount() != null) {
                    txtLeaseAmount.setValue(assetAppraisal.getLeaseAmount().toString());
                }

                percent = 0;
                historyItems = ASSET_APPRAISAL_SERVICE.getHistoryItemsByAssetAppraisal(assetAppraisal);
                getHistoryItems(historyItems);

                if (assetAppraisal.getHoursOfUse() != null && quotation.getAsset().getOdometer() != null) {
                    setValueHoursOfUse(quotation.getAsset().getOdometer());
                }

                setValueMonthsOfUse(assetAppraisal);

                if (assetAppraisal.getAppraisals() != null) {
                    for (Appraisal appraisal : assetAppraisal.getAppraisals()) {
                        for (Map.Entry<Long, List<AppraisalItem>> entry : mapAppraisalCategory.entrySet()) {
                            List<AppraisalItem> appraisalItems = entry.getValue();
                            if (appraisalItems != null) {
                                for (AppraisalItem item : appraisalItems) {
                                    if (item.getId() != null) {
                                        if(item.getId() ==  appraisal.getItemId()){

                                            if (mapTxtFixPrice.get(appraisal) != null) {
                                                if (mapTxtFixPrice.get(appraisal.getId()) != null) {
                                                    mapTxtFixPrice.get(appraisal.getId()).setValue(getDefaultString(appraisal.getFixPrice()));
                                                } else {
                                                    mapTxtFixPrice.get(appraisal.getId()).setValue(getDefaultString(""));
                                                }
                                            }

                                            if (mapTxtChangePrice.get(appraisal) != null) {
                                                if (mapTxtChangePrice.get(appraisal.getId()) != null) {
                                                    mapTxtChangePrice.get(appraisal.getId()).setValue(getDefaultString(appraisal.getChangePrice()));
                                                } else {
                                                    mapTxtChangePrice.get(appraisal.getId()).setValue(getDefaultString(""));
                                                }
                                            }

                                            if (mapTxtAdjustValue.get(appraisal) != null) {
                                                if (mapTxtAdjustValue.get(appraisal.getId()) != null) {
                                                    mapTxtAdjustValue.get(appraisal.getId()).setValue(getDefaultString(appraisal.getAdjustValue()));
                                                } else {
                                                    mapTxtAdjustValue.get(appraisal.getId()).setValue("");
                                                }
                                            }


                                        }
                                    }
                                }
                            }

                        }
                    }
                }


            } else {
                this.assetAppraisal = AssetAppraisal.createInstance();
            }
        }

        if (quotation.getAsset().getAssetRange() != null) {
            appraisalCategories = ASSET_APPRAISAL_SERVICE.getAppraisalCategoriesByAssetRang(quotation.getAsset().getAssetRange());
            if (appraisalCategories != null) {
                getAssetAppraisalItems(appraisalCategories);
            }
        }

        List<AssetHistoryCategory> assetHistoryCategories = ASSET_APPRAISAL_SERVICE.getAssetHistoryCategories(quotation.getAsset().getAssetRange());
        if (assetHistoryCategories != null) {
            getAssetHistoryCategories(assetHistoryCategories);
        }

        if (quotation != null && quotation.getAsset() != null && quotation.getAsset().getAssetRange() != null && quotation.getAsset().getAssetRange().getProductType() != null) {
            if (quotation.getAsset().getAssetRange().getProductType() == EProductType.WTR) {
                txtHoursOfUse.setEnabled(false);
                txtMonthsOfUse.setEnabled(true);
            } else {
                txtHoursOfUse.setEnabled(false);
                txtMonthsOfUse.setEnabled(false);
            }
        }

//        setValueComponent();
        if (quotation.getWkfStatus() == QuotationWkfStatus.PAC) {
            txtLeaseAmountCommittee.setEnabled(true);
            txtLeaseAmountCommittee.setValue(txtLeaseAmountAppraisal.getValue());
        }
        if (quotation.getWkfStatus() == QuotationWkfStatus.QUO) {
            txtAdjustmentAppraisal.setValue(txtAdjustment.getValue());
            txtPriceBeforeAdjustmentAppraisal.setValue(txtPriceBeforeAdjustment.getValue());
            txtLeaseAmountAppraisal.setValue(txtLeaseAmount.getValue());
            txtPriceAfterAdjustmentAppraisal.setValue(txtPriceAfterAdjustment.getValue());
            enabledAdjustValue(true);
            enabledCOValue(false);
        } else {
            enabledAdjustValue(false);
            enabledCOValue(true);
        }
    }


    protected void getHistoryItems(List<HistoryItem> historyItems) {
        if (historyItems != null) {
            for (HistoryItem historyItem : historyItems) {
                if (historyItem.getPercent() != null) {
                    mapLblHistory.get(historyItem.getAssetHistoryItem().getId()).setValue(historyItem.getPercent() + "%");
                    mapCbHistory.get(historyItem.getAssetHistoryItem().getId()).setValue(true);
                    mapPercent.put(historyItem.getAssetHistoryItem().getId(), historyItem.getPercent().toString());
                    percent = percent + historyItem.getPercent();
                } else {
                    mapCbHistory.get(historyItem.getAssetHistoryItem().getId()).setValue(false);
                }
            }
        }
    }


    protected void getAssetAppraisalItems(List<AppraisalCategory> appraisalCategories) {
        for (AppraisalCategory assetAppraisalCategory : appraisalCategories) {
            List<AppraisalItem> appraisalItems = ASSET_APPRAISAL_SERVICE.getAppraisalItemByCategory(assetAppraisalCategory);
            if (appraisalItems != null) {
                for (AppraisalItem assetAppraisalItem : appraisalItems) {
                    if (mapFixPrice.get(assetAppraisalItem.getId()) != null) {
                        mapTxtFixPrice.get(assetAppraisalItem.getId()).setValue(mapFixPrice.get(assetAppraisalItem.getId()));
                        if (assetAppraisalItem.getFixPrice() != null) {
                            if(assetAppraisalItem.getFixPrice() == 0){
                                mapCbFixPrice.get(assetAppraisalItem.getId()).setEnabled(false);
                            }
                        }
                    }
                    if (mapChangePrice.get(assetAppraisalItem.getId()) != null) {
                        mapTxtChangePrice.get(assetAppraisalItem.getId()).setValue(mapChangePrice.get(assetAppraisalItem.getId()));
                        if (assetAppraisalItem.getChangePrice() != null) {
                            if (assetAppraisalItem.getChangePrice() == 0) {
                                mapCbChangePrice.get(assetAppraisalItem.getId()).setEnabled(false);
                            }
                        }

                    }
                    if (mapTxtAdjustValue.get(assetAppraisalItem.getId()) != null) {
                        mapTxtAdjustValue.get(assetAppraisalItem.getId()).setValue(mapAdjustValue.get(assetAppraisalItem.getId()));
                    }
                }
            }
        }
    }

    protected void getAssetHistoryCategories(List<AssetHistoryCategory> assetHistoryCategories) {
        for (AssetHistoryCategory assetHistoryCategory : assetHistoryCategories) {
            List<AssetHistoryItem> getAssetHistoryItems = ASSET_APPRAISAL_SERVICE.getAssetHistoryItems(assetHistoryCategory);
            if (getAssetHistoryItems != null) {
                for (AssetHistoryItem assetHistoryItem : getAssetHistoryItems) {
                    if (assetHistoryItem.getPercent() == 0) {
                        mapCbHistory.get(assetHistoryItem.getId()).setEnabled(false);
                    }
                }
            }
        }
    }

    public Boolean getFindId(List<Long> lsId, AppraisalItem assetAppraisalItem) {
        Boolean findId = false;
        for (int i = 0; i < lsId.size(); i++) {
            if (assetAppraisalItem.getId() == lsId.get(i)) {
                return true;
            } else {
                findId = false;
            }
        }
        return findId;
    }

    public Boolean getFindHId(List<Long> lsId, AssetHistoryItem assetHistoryItem) {
        Boolean findId = false;
        for (int i = 0; i < lsId.size(); i++) {
            if (assetHistoryItem.getId() == lsId.get(i)) {
                return true;
            } else {
                findId = false;
            }
        }
        return findId;
    }

    @Override
    public void reset() {
        mapFixPrice = new HashMap<>();
        mapChangePrice = new HashMap<>();
        mapTxtFixPrice = new HashMap<>();
        mapTxtChangePrice = new HashMap<>();
        lstFixPrice = new ArrayList<>();
        lstChangePrice = new ArrayList<>();
        quotation = new Quotation();
        fixCost = 0.0;
        changeCost = 0.0;
        depreciationMonth = 0.0;
        beforeAdjustment = 0.0;
        assetPrice = 0.0;
        depreciation = 0l;
        percent = 0;
        ajustment = 0.0;
        leaseAmount = 0.0;
    }


   /* public void setValueComponent() {
        if (quotation != null && quotation.getAsset() != null && quotation.getAsset().getAssetRange() != null && quotation.getAsset().getAssetRange().getProductType() == EProductType.WTR) {
            if (txtMonthsOfUse != null && !txtMonthsOfUse.getValue().isEmpty())
                depreciation = Math.round(depreciationMonth * Integer.parseInt(txtMonthsOfUse.getValue()));
        } else {
            if (txtHoursOfUse != null && !txtHoursOfUse.getValue().isEmpty())
                depreciation = Math.round(depreciationMonth * Integer.parseInt(txtHoursOfUse.getValue()));
        }
        txtDepreciation.setValue(depreciation.toString());
        if (ASSET_APPRAISAL_SERVICE.getGroupAppraisalItem(quotation) != null && ASSET_APPRAISAL_SERVICE.getGroupAppraisalItem(quotation).getPriceOfBrandNew() != null) {
            beforeAdjustment = ASSET_APPRAISAL_SERVICE.getGroupAppraisalItem(quotation).getPriceOfBrandNew() - (fixCost + changeCost + depreciation);
        } else {
            beforeAdjustment = Double.parseDouble(txtBrandNew.getValue()) - (fixCost + changeCost + depreciation);
            txtPriceBeforeAdjustment.setValue(new DecimalFormat("#.##").format(beforeAdjustment));
            ajustment = ((percent * beforeAdjustment) / 100);
            txtPriceAfterAdjustment.setValue(new DecimalFormat("#.##").format(beforeAdjustment - ajustment));
            txtAdjustment.setValue(ajustment.toString());
            if (leaseAmountPercent != null) {
                leaseAmount = (beforeAdjustment - ajustment) * leaseAmountPercent / 100;
                txtLeaseAmount.setValue(new DecimalFormat("#.##").format(leaseAmount));
            }
        }
    }*/

    /*public void resetCost() {
        if (txtCostOfRepair != null)
            txtCostOfRepair.setValue("0");
        if (txtCostOfChange != null)
            txtCostOfChange.setValue("0");
        if (txtHoursOfUse != null)
            txtHoursOfUse.setValue("0");
        fixCost = 0.0;
        changeCost = 0.0;
        if (quotation != null && ASSET_APPRAISAL_SERVICE.getGroupAppraisalItem(quotation) != null) {
            BaseRestrictions<AppraisalItem> lstappraisalItem = new BaseRestrictions<>(AppraisalItem.class);
            lstappraisalItem.addCriterion(Restrictions.eq("groupAppraisalItem.id", ASSET_APPRAISAL_SERVICE.getGroupAppraisalItem(quotation).getId()));
            List<AppraisalItem> list = ENTITY_SRV.list(lstappraisalItem);
            if (list.size() > 0) {
                for (AppraisalItem appraisalItem : list) {
                    ENTITY_SRV.delete(appraisalItem);
                }
            }
        }
        percent = 0;
        if (quotation != null && ASSET_APPRAISAL_SERVICE.getGroupAppraisalItem(quotation) != null) {
            BaseRestrictions<HistoryItem> lstappraisalItem = new BaseRestrictions<>(HistoryItem.class);
            lstappraisalItem.addCriterion(Restrictions.eq("groupAppraisalItem.id", ASSET_APPRAISAL_SERVICE.getGroupAppraisalItem(quotation).getId()));
            List<HistoryItem> list = ENTITY_SRV.list(lstappraisalItem);
            if (list.size() > 0) {
                for (HistoryItem historyItem : list) {
                    ENTITY_SRV.delete(historyItem);
                }
            }
        }
    }*/

    private Double getAdjustValue() {
        double adjustValue = 0;
        if (quotation != null && quotation.getAsset() != null && quotation.getAsset().getAssetRange() != null)
            appraisalCategories = ASSET_APPRAISAL_SERVICE.getAppraisalCategoriesByAssetRang(quotation.getAsset().getAssetRange());
        if (appraisalCategories != null) {
            for (AppraisalCategory assetAppraisalCategory : appraisalCategories) {
                for (AppraisalItem assetAppraisalItem : ASSET_APPRAISAL_SERVICE.getAppraisalItemByCategory(assetAppraisalCategory)) {
                    if (mapTxtAdjustValue.get(assetAppraisalItem.getId()) != null && !mapTxtAdjustValue.get(assetAppraisalItem.getId()).getValue().equals("")) {
                        adjustValue = adjustValue + Double.parseDouble(mapTxtAdjustValue.get(assetAppraisalItem.getId()).getValue());
                    }
                }
            }
        }
        return adjustValue;
    }

    private void enabledAdjustValue(Boolean b) {
        if (quotation != null && quotation.getAsset() != null && quotation.getAsset().getAssetRange() != null)
            appraisalCategories = ASSET_APPRAISAL_SERVICE.getAppraisalCategoriesByAssetRang(quotation.getAsset().getAssetRange());
        if (appraisalCategories != null) {
            for (AppraisalCategory assetAppraisalCategory : appraisalCategories) {
                for (AppraisalItem assetAppraisalItem : ASSET_APPRAISAL_SERVICE.getAppraisalItemByCategory(assetAppraisalCategory)) {
                    if (mapTxtAdjustValue.get(assetAppraisalItem.getId()) != null) {
                        mapTxtAdjustValue.get(assetAppraisalItem.getId()).setEnabled(b);
                    }
                }
            }
        }
    }

    private void enabledCOValue(boolean b) {
        if (quotation != null && quotation.getAsset() != null && quotation.getAsset().getAssetRange() != null)
            appraisalCategories = ASSET_APPRAISAL_SERVICE.getAppraisalCategoriesByAssetRang(quotation.getAsset().getAssetRange());
        if (appraisalCategories != null) {
            for (AppraisalCategory assetAppraisalCategory : appraisalCategories) {
                for (AppraisalItem assetAppraisalItem : ASSET_APPRAISAL_SERVICE.getAppraisalItemByCategory(assetAppraisalCategory)) {
                    if(assetAppraisalItem.getId() != null){
                        if(mapTxtChangePrice != null){
                            if (mapTxtChangePrice.get(assetAppraisalItem.getId()) != null && assetAppraisalItem.getChangePrice() != null) {
                                mapTxtChangePrice.get(assetAppraisalItem.getId()).setEnabled(b);
                            }
                        }
                        if(mapTxtFixPrice != null){
                            if (mapTxtFixPrice.get(assetAppraisalItem.getId()) != null && assetAppraisalItem.getFixPrice() != null) {
                                mapTxtFixPrice.get(assetAppraisalItem.getId()).setEnabled(b);
                            }
                        }
                        if(mapCbChangePrice != null){
                            if (mapCbChangePrice.get(assetAppraisalItem.getId()) != null && assetAppraisalItem.getChangePrice() != null) {
                                mapCbChangePrice.get(assetAppraisalItem.getId()).setEnabled(b);
                            }
                        }
                        if(mapCbFixPrice != null){
                            if (mapCbFixPrice.get(assetAppraisalItem.getId()) != null && assetAppraisalItem.getFixPrice() != null) {
                                mapCbFixPrice.get(assetAppraisalItem.getId()).setEnabled(b);
                            }
                        }
                    }
                }
            }
        }
    }

    public Integer getMonth(Date dateOfBirth) {
        long ageInDay = DateUtils.getDiffInDays(DateUtils.today(), dateOfBirth).longValue();
        int month = (int) (ageInDay / 30L);
        return month;
    }

    public Integer getMonthOfUse() {
        return Integer.parseInt(txtMonthsOfUse.getValue());
    }

    public Integer getHoursOfUse() {
        return Integer.parseInt(txtHoursOfUse.getValue());
    }

    public void setValueHoursOfUse(long odometer) {
        if (txtHoursOfUse != null) {
            Long hoursOfUse;
            if (!txtMonthsOfUse.getValue().isEmpty() && Integer.parseInt(txtMonthsOfUse.getValue()) == 0) {
                txtHoursOfUse.setValue("0");
                return;
            }
            hoursOfUse = (odometer / Integer.parseInt(txtMonthsOfUse.getValue())) * 12;
            if (hoursOfUse < 1000) {
                txtHoursOfUse.setValue("1000");
            } else {
                txtHoursOfUse.setValue(hoursOfUse.toString());
            }
        }
    }

    public void setValueMonthsOfUse(AssetAppraisal assetAppraisal) {
        if (assetAppraisal != null) {
            if (assetAppraisal.getMonthsOfUse() != null && assetAppraisal.getMonthsOfUse() == 0) {
                if (txtMonthsOfUse != null) {
                    Integer monthOfUse = 0;
                    if (quotation.getAsset() != null && quotation.getAsset().getPurchageDate() != null) {
                        monthOfUse = getMonth(quotation.getAsset().getPurchageDate());
                    } else if (quotation.getAsset() != null && quotation.getAsset().getImportDate() != null) {
                        monthOfUse = getMonth(quotation.getAsset().getImportDate());
                    }
                    txtMonthsOfUse.setValue(monthOfUse.toString());
                }
            }
        }
    }

    public List<String> isValid() {
        List<String> error = new ArrayList<>();
        super.reset();
        super.removeErrorsPanel();
        /*if (quotation != null && quotation.getAsset() != null && quotation.getAsset().getAssetRange() != null && quotation.getAsset().getAssetRange().getProductType() == EProductType.WTR) {
            if (!txtLeaseAmount.getValue().isEmpty() && Double.parseDouble(txtLeaseAmount.getValue()) <= 300) {
                errors.add(I18N.message("lease.amount.less.than.300"));
            } else {
                checkMandatoryField(txtLeaseAmount, "lease.amount");
            }
        } else {
            if (!txtLeaseAmount.getValue().isEmpty() && Double.parseDouble(txtLeaseAmount.getValue()) <= 3000) {
                errors.add(I18N.message("lease.amount.less.than.3000"));
            } else {
                checkMandatoryField(txtLeaseAmount, "lease.amount");
            }
        }
        if (!errors.isEmpty()) {
            super.displayErrorsPanel();
        }*/
        return error;
    }

    public void getAppraisalCategories(List<AppraisalCategory> appraisalCategories) {
        if (appraisalCategories != null) {
            this.appraisalCategories = appraisalCategories;
        }
    }


    public void setAssetAppraisal(AssetAppraisal assetAppraisal) {
        this.assetAppraisal = assetAppraisal;
    }

    public AssetAppraisal getAssetAppraisal() {
        if (quotation != null && quotation.getAsset() != null) {
            if (ASSET_APPRAISAL_SERVICE.getAssetAppraisalByAsset(quotation.getAsset()) != null) {
                this.assetAppraisal = ASSET_APPRAISAL_SERVICE.getAssetAppraisalByAsset(quotation.getAsset());
            } else {
                this.assetAppraisal = new AssetAppraisal();
            }

            if (mapAppraisalCategory != null && !mapAppraisalCategory.isEmpty()) {
                for (Map.Entry<Long, List<AppraisalItem>> entry : mapAppraisalCategory.entrySet()) {
                    appraisalList = new ArrayList<>();
                    List<AppraisalItem> appraisalItems = entry.getValue();
                    if (appraisalItems != null) {
                        for (AppraisalItem item : appraisalItems) {
                            if (item.getId() != null) {
                                Appraisal appraisal = this.assetAppraisal.getAppraisalById(item.getId());
                                if (appraisal == null) {
                                    appraisal = new Appraisal();
                                    appraisal.setCrudAction(CrudAction.CREATE);
                                } else {
                                    appraisal.setCrudAction(CrudAction.UPDATE);
                                }
                                if (mapTxtFixPrice.get(item.getId()) != null) {
                                    if (mapTxtFixPrice.get(item.getId()).getValue() != null) {
                                        appraisal.setFixPrice(MyNumberUtils.getDouble(mapTxtFixPrice.get(item.getId()).getValue(), 0d));
                                    }
                                }
                                if (mapTxtChangePrice.get(item.getId()) != null) {
                                    if (mapTxtChangePrice.get(item.getId()).getValue() != null) {
                                        appraisal.setChangePrice(MyNumberUtils.getDouble(mapTxtChangePrice.get(item.getId()).getValue().toString(), 0d));
                                    }
                                }

                                if (this.quotation.getWkfStatus() == QuotationWkfStatus.QUO && mapTxtAdjustValue.get(item.getId()) != null && !mapTxtAdjustValue.get(item.getId()).getValue().equals("")) {
                                    appraisal.setAdjustValue(Double.parseDouble(mapTxtAdjustValue.get(item.getId()).getValue()));
                                }
                                appraisal.setItemId(item.getId());
                                appraisal.setAssetAppraisal(this.assetAppraisal);
                                appraisalList.add(appraisal);
                            }
                        }
                    }
                }
            }

            if (mapAssetHistory != null && !mapAssetHistory.isEmpty()) {
                for (Map.Entry<Long, List<AssetHistoryItem>> entry : mapAssetHistory.entrySet()) {
                    historyItemList = new ArrayList<>();
                    List<AssetHistoryItem> assetHistoryItems = entry.getValue();
                    if (assetHistoryItems != null) {
                        for (AssetHistoryItem assetHistoryItem : assetHistoryItems) {
                            if (assetHistoryItem.getId() != null) {
                                HistoryItem historyItem = ENTITY_SRV.getByField(HistoryItem.class, "assetHistoryItem.id", assetHistoryItem.getId());
                                if (historyItem == null) {
                                    historyItem = new HistoryItem();
                                    historyItem.setCrudAction(CrudAction.CREATE);
                                } else {
                                    historyItem.setCrudAction(CrudAction.UPDATE);
                                }
                                if (mapPercent.get(assetHistoryItem.getId()) != null) {
                                    historyItem.setPercent(MyNumberUtils.getInteger(mapPercent.get(historyItem.getId()), 0));
                                }
                                historyItemList.add(historyItem);
                            }
                        }
                    }
                }
            }

            if (quotation.getAsset().getAssetRange() != null && quotation.getAsset().getAssetRange().isWorkingTrackor() != null && !quotation.getAsset().getAssetRange().isWorkingTrackor()) {
                if (txtLeaseAmount.getValue() != "") {
                    if (Double.parseDouble(txtLeaseAmount.getValue()) <= 300) {
                        txtLeaseAmount.setValue("");
                    } else if (assetAppraisal != null) {
                        setEntityAssetAppraisal(assetAppraisal);
                    }
                }
            } else if (assetAppraisal != null) {
                setEntityAssetAppraisal(assetAppraisal);
            }

        }
        return assetAppraisal;
    }

    public AssetAppraisal setEntityAssetAppraisal(AssetAppraisal assetAppraisal) {
        assetAppraisal.setMonthsOfUse(txtMonthsOfUse != null ? getLong(txtMonthsOfUse, 0) : 0);
        assetAppraisal.setHoursOfUse(txtHoursOfUse != null ? getLong(txtHoursOfUse, 0) : 0);
        assetAppraisal.setPriceOfBrandNew(txtBrandNew != null ? getDouble(txtBrandNew, 0.0) : 0d);
        assetAppraisal.setPriceOfSecondhand(txtSecondHand != null ? getDouble(txtSecondHand, 0.0) : 0d);
        assetAppraisal.setDepreciatePerMonth(txtDepreciationMonth != null ? getDouble(txtDepreciationMonth, 0.0) : 0d);
        assetAppraisal.setDepreciate(txtDepreciation != null ? getDouble(txtDepreciation, 0.0) : 0d);
        assetAppraisal.setCostOfRepair(txtCostOfRepair != null ? getDouble(txtCostOfRepair, 0.0) : 0d);
        assetAppraisal.setCostOfChange(txtCostOfChange != null ? getDouble(txtCostOfChange, 0.0) : 0d);
        assetAppraisal.setPriceBeforeAdjustment(txtPriceBeforeAdjustment != null ? getDouble(txtPriceBeforeAdjustment, 0.0) : 0d);
        assetAppraisal.setAdjustment(txtAdjustment != null ? getDouble(txtAdjustment, 0.0) : 0d);
        assetAppraisal.setPriceAfterAdjustment(txtPriceAfterAdjustment != null ? getDouble(txtPriceAfterAdjustment, 0.0) : 0d);
        assetAppraisal.setLeaseAmount(txtLeaseAmount != null ? getDouble(txtLeaseAmount, 0.0) : 0d);
        assetAppraisal.setAppraisals(appraisalList);
        assetAppraisal.setHistoryItems(historyItemList);
        if (quotation.getWkfStatus() == QuotationWkfStatus.QUO && !txtLeaseAmountAppraisal.getValue().equals("")) {
            assetAppraisal.setPriceAfterAdjustmentAppraisal(Double.parseDouble(txtPriceAfterAdjustmentAppraisal.getValue()));
            assetAppraisal.setLeaseAmountAppraisal(Double.parseDouble(txtAdjustmentAppraisal.getValue()));
        }
        if (quotation.getWkfStatus() == QuotationWkfStatus.PAC && !txtLeaseAmountCommittee.getValue().equals("")) {
            assetAppraisal.setLeaseAmountCommittee(Double.parseDouble(txtLeaseAmountCommittee.getValue()));
        }
        return assetAppraisal;
    }

}

