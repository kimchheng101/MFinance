package com.soma.mfinance.core.applicant.service;

import org.seuksa.frmk.model.entity.MEntityA;


/**
 * Meta data of com.soma.mfinance.core.applicant.model.Driver
 * @author kimsuor.seang
 */
public interface MDriver extends MEntityA {
	
	public final static String CONTRACT = "contract";

}
