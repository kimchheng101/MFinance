package com.soma.mfinance.core.workflow;

import com.soma.common.app.workflow.model.EWkfStatus;
import org.seuksa.frmk.tools.reflection.MyClassUtils;

import java.util.List;

/**
 * Collection Status
 * 
 * @author sr.soth / 22-05-17
 *
 */
public class CollectionWkfStatus{

	/*public final static EWkfStatus WAITING_CALLING = EWkfStatus.getById(101);  // Waiting Calling
	public final static EWkfStatus ALREADY_PAID = EWkfStatus.getById(102);  // Already paid
	public final static EWkfStatus WAITING_INS_RESULT = EWkfStatus.getById(103);  // Waiting Insurance Result*/

	public final static EWkfStatus NEW = EWkfStatus.getById(1);
	public final static EWkfStatus ASS_V =  EWkfStatus.getById(2);
	public final static EWkfStatus REOPE =  EWkfStatus.getById(3);

	public static List<EWkfStatus> values() {
		List<EWkfStatus> statuses = (List<EWkfStatus>) MyClassUtils.getStaticValues(CollectionWkfStatus.class, EWkfStatus.class);
		return statuses;
	}
}