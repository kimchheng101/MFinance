package com.soma.mfinance.core.document.panel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Date;

import org.apache.commons.io.output.NullOutputStream;
import org.apache.commons.lang.StringUtils;

import com.soma.mfinance.core.shared.conf.AppConfig;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author kimsuor.seang
 */
public class DocumentUploader implements Receiver, SucceededListener {
	
	private static final long serialVersionUID = -4738310396410864219L;

	private static final Logger LOGGER = LoggerFactory.getLogger(DocumentUploader.class);

	private File file;
	private String tmpFileName;
	private CheckBox cbDocument;
	private Button btnPath;
	private Upload uploadComponent;
	
	public DocumentUploader(CheckBox cbDocument, Button btnPath, Upload uploadComponent) {
		this.cbDocument = cbDocument;
		this.btnPath = btnPath;
		this.uploadComponent = uploadComponent;
	}

	@Override
    public OutputStream receiveUpload(String filename, String mimeType) {
        FileOutputStream fos = null;

		filename = filename.replaceAll("\\s+","_").trim();  // covert file name to underscore(_)

        if (StringUtils.isEmpty(filename)) {
        	uploadComponent.interruptUpload();
        	return new NullOutputStream();
        }
        try {
        	String tmpDir = AppConfig.getInstance().getConfiguration().getString("specific.tmpdir");
        	String tmpPath = "tmp_" + (new Date().getTime());
        	tmpFileName = tmpPath + "/" + filename;
        	File tmpDirPath = new File(tmpDir + "/" + tmpPath);
        	if (!tmpDirPath.exists()) {
				tmpDirPath.mkdirs();
        	}	        	
            file = new File(tmpDir + "/" + tmpFileName);
            fos = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            Notification.show("Could not open file<br/>", e.getMessage(), Type.ERROR_MESSAGE);
			LOGGER.error("------+--++++++++++++=> FileNotFoundException : file ", e);
            return null;
        }
        return fos; // Return the output stream to write to
    }
	@Override
    public void uploadSucceeded(SucceededEvent event) {
    	this.cbDocument.setValue(true);
    	btnPath.setData(tmpFileName);
    	btnPath.setVisible(true);
    }
}
