package com.soma.mfinance.core.contract.model.cashflow;


/**
 * 
 * @author seanglay.chhoeurn
 *
 */
public interface MCashflowLockSplitType {

	public final static String LOCKSPLITTYPE = "lockSplitType";
	public final static String CASHFLOWTYPE = "cashflowType";
}
