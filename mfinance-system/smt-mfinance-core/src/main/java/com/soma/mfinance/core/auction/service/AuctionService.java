package com.soma.mfinance.core.auction.service;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.contract.model.Contract;
import org.seuksa.frmk.service.BaseEntityService;

import java.util.Date;

/**
 * Auction Service Interface
 * @author kimsuor.seang
 * @modify th.seng (22/11/2017)
 */
public interface AuctionService extends BaseEntityService {
	
	/**
	 * Change the Auction status of the contract
	 * @param contract
	 * @param auctionStatus
	 */
	void changeAuctionStatus (Contract contract, EWkfStatus auctionStatus);
	
	/**
	 * Get insurance balance
	 * @param calculDate
	 * @param cashflows
	 * @return
	 */
	//Amount getRealInsuranceBalance (Date calculDate, List<Cashflow> cashflows);
	
	/**
	 * Get insurance balance
	 * @param calculDate
	 * @param cotraId
	 * @return
	 */
	//Amount getRealInsuranceBalance (Date calculDate, Long cotraId);
	
	/**
	 * Get Service income balance
	 * @param calculDate
	 * @param cashflows
	 * @return
	 */
	//Amount getRealServiceIncomeBalance (Date calculDate, List<Cashflow> cashflows);
	
	/**
	 * Get Service income balance
	 * @param calculDate
	 * @param cotraId
	 * @return
	 */
	//Amount getRealServiceIncomeBalance (Date calculDate, Long cotraId);
	
	/**
	 * Get the number of day from repossess
	 * @param contract
	 * @return
	 */
	Long getDayFromRepossess (Contract contract);
	
	/**
	 * Get the date of repossess
	 * @param contract
	 * @return
	 */
	Date getDateOfRepossess (Contract contract);

	/**
	 * @param contract
	 * @return
	 */
	Long getNumberOfAuction(Contract contract);

	/**
	 * @param collection
	 * @return
	 */
	Double getRemainingPrincipalBalance(Collection collection);

	/**
	 * @param collection
	 * @return
	 */
	Double getPenaltyAmount(Collection collection);

	/**
	 * @param contract
	 * @return
	 */
	Double[] getRemainingServiceIncomeBalance(Contract contract);

}
