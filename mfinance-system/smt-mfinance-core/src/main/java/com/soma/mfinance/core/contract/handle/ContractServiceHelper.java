package com.soma.mfinance.core.contract.handle;

import com.soma.ersys.core.hr.model.address.Address;
import com.soma.ersys.core.hr.model.organization.ContactInfo;
import com.soma.mfinance.core.address.model.AddressArc;
import com.soma.mfinance.core.applicant.model.*;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.asset.model.AssetArc;
import com.soma.mfinance.core.asset.model.AssetCategory;
import com.soma.mfinance.core.asset.model.AssetMake;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.DealerAttribute;
import org.seuksa.frmk.model.entity.EStatusRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/24/2017
 * Name  : 1:51 PM
 * Email : k.seang@gl-f.com
 */
public final class ContractServiceHelper {

    /**
     * Copy Asset
     * @param asset
     * @return
     */
    public static AssetArc copyAsset(Asset asset) {
        AssetArc assArc = new AssetArc();
        assArc.setSerie(asset.getSerie());
        assArc.setColor(asset.getColor());
        assArc.setYear(asset.getYear());
        assArc.setEngine(asset.getEngine());
        assArc.setAssetGender(asset.getAssetGender());
        assArc.setTiAssetPrice(asset.getTiAssetPrice());
        assArc.setTeAssetPrice(asset.getTeAssetPrice());
        assArc.setVatAssetPrice(asset.getVatAssetPrice());
        assArc.setVatValue(asset.getVatValue());
        assArc.setGrade(asset.getGrade());
        assArc.setModel(asset.getModel());
        assArc.setRegistrationDate(asset.getRegistrationDate());
        assArc.setRegistrationProvince(asset.getRegistrationProvince());
        assArc.setRegistrationPlateType(asset.getRegistrationPlateType());
        assArc.setRegistrationBookStatus(asset.getRegistrationBookStatus());
        assArc.setPlateNumber(asset.getPlateNumber());
        assArc.setChassisNumber(asset.getChassisNumber());
        assArc.setEngineNumber(asset.getEngineNumber());
        assArc.setModelUsed(asset.isModelUsed());
        assArc.setMileage(asset.getMileage());
        assArc.setRiderName(asset.getRiderName());

        assArc.setCode(asset.getCode());
        assArc.setDesc(asset.getDesc());
        assArc.setDescEn(asset.getDescEn());

        return assArc;
    }

    /**
     * Copy Applicant to Applicant Arc
     * @param applicant
     * @return
     */
    public static ApplicantArc copyApplicant(Applicant applicant) {
        ApplicantArc appArc = new ApplicantArc();
        appArc.setApplicantCategory(applicant.getApplicantCategory());

        Individual individual = applicant.getIndividual();
        IndividualArc individualArc = IndividualArc.createInstance();

        individualArc.setFirstName(individual.getFirstName());
        individualArc.setFirstNameEn(individual.getFirstNameEn());
        individualArc.setLastName(individual.getLastName());
        individualArc.setLastNameEn(individual.getLastNameEn());
        individualArc.setNickName(individual.getNickName());
        individualArc.setTypeIdNumber(individual.getTypeIdNumber());
        individualArc.setIdNumber(individual.getIdNumber());
        individualArc.setIssuingIdNumberDate(individual.getIssuingIdNumberDate());
        individualArc.setExpiringIdNumberDate(individual.getExpiringIdNumberDate());
        individualArc.setTitle(individual.getTitle());
        individualArc.setCivility(individual.getCivility());
        individualArc.setBirthDate(individual.getBirthDate());
        individualArc.setPlaceOfBirth(individual.getPlaceOfBirth());
        individualArc.setGender(individual.getGender());
        individualArc.setMaritalStatus(individual.getMaritalStatus());
        individualArc.setNationality(individual.getNationality());
        individualArc.setMobilePerso(individual.getMobilePerso());
        individualArc.setEmailPerso(individual.getEmailPerso());
        individualArc.setPhoto(individual.getPhoto());

        individualArc.setReference(individual.getReference());
        individualArc.setOtherNationality(individual.getOtherNationality());
        individualArc.setNumberOfChildren(individual.getNumberOfChildren());
        individualArc.setNumberOfHousehold(individual.getNumberOfHousehold());
        individualArc.setReligion(individual.getReligion());
        individualArc.setEducation(individual.getEducation());
        individualArc.setSecondLanguage(individual.getSecondLanguage());
        individualArc.setMonthlyPersonalExpenses(individual.getMonthlyPersonalExpenses());
        individualArc.setMonthlyFamilyExpenses(individual.getMonthlyFamilyExpenses());
        individualArc.setDebtFromOtherSource(individual.isDebtFromOtherSource());
        individualArc.setTotalDebtInstallment(individual.getTotalDebtInstallment());
        individualArc.setGuarantorOtherLoan(individual.isGuarantorOtherLoan());
        individualArc.setConvenientVisitTime(individual.getConvenientVisitTime());
        individualArc.setHouseholdExpenses(individual.getHouseholdExpenses());
        individualArc.setHouseholdIncome(individual.getHouseholdIncome());
        individualArc.setTotalFamilyMember(individual.getTotalFamilyMember());

        List<IndividualAddressArc> individualAddressArcs = copyIndividualAddressArcs(individual.getIndividualAddresses());
        if (individualAddressArcs != null && !individualAddressArcs.isEmpty()) {
            individualArc.setIndividualAddresses(individualAddressArcs);
        }

        List<EmploymentArc> employmentArcs = copyEmploymentArcs(individual.getEmployments());
        if (employmentArcs != null && !employmentArcs.isEmpty()) {
            individualArc.setEmployments(employmentArcs);
        }

        List<IndividualContactInfoArc> individualContactInfoArcs = copyIndividualContactInfoArcs(individual.getIndividualContactInfos());
        if (individualContactInfoArcs != null && !individualContactInfoArcs.isEmpty()) {
            individualArc.setIndividualContactInfos(individualContactInfoArcs);
        }

        List<IndividualReferenceInfoArc> applicantReferenceInfoArcs = copyIndividualReferenceInfoArcs(individual.getIndividualReferenceInfos());
        if (applicantReferenceInfoArcs != null && !applicantReferenceInfoArcs.isEmpty()) {
            individualArc.setIndividualReferenceInfos(applicantReferenceInfoArcs);
        }
        appArc.setIndividual(individualArc);
        return appArc;
    }

    /**
     * Copy Applicant Reference Info Arc
     * @param individualReferenceInfos
     * @return
     */
    public static List<IndividualReferenceInfoArc> copyIndividualReferenceInfoArcs(List<IndividualReferenceInfo> individualReferenceInfos) {
        List<IndividualReferenceInfoArc> individualReferenceInfoArcs = null;
        if (individualReferenceInfos != null && !individualReferenceInfos.isEmpty()) {
            individualReferenceInfoArcs = new ArrayList<>();
            for (IndividualReferenceInfo individualReferenceInfo : individualReferenceInfos) {
                IndividualReferenceInfoArc individualReferenceInfoArc = copyIndividualReferenceInfo(individualReferenceInfo);
                individualReferenceInfoArcs.add(individualReferenceInfoArc);
            }
        }
        return individualReferenceInfoArcs;
    }

    /**
     * Copy Applicant Reference Info
     * @param individualReferenceInfo
     * @return
     */
    public static IndividualReferenceInfoArc copyIndividualReferenceInfo(IndividualReferenceInfo individualReferenceInfo) {
        IndividualReferenceInfoArc individualReferenceInfoArc = new IndividualReferenceInfoArc();
        individualReferenceInfoArc.setReferenceType(individualReferenceInfo.getReferenceType());
        individualReferenceInfoArc.setRelationship(individualReferenceInfo.getRelationship());
        individualReferenceInfoArc.setLastNameEn(individualReferenceInfo.getLastNameEn());
        individualReferenceInfoArc.setFirstNameEn(individualReferenceInfo.getFirstNameEn());
        List<IndividualReferenceContactInfoArc> individualReferenceContactInfoArc = copyIndividualReferenceContactInfoArcs(individualReferenceInfo.getIndividualReferenceContactInfos());
        if (individualReferenceContactInfoArc != null && !individualReferenceContactInfoArc.isEmpty()) {
            individualReferenceInfoArc.setIndividualReferenceContactInfos(individualReferenceContactInfoArc);
        }
        return individualReferenceInfoArc;
    }

    /**
     * Copy Applicant Reference Contact Info Arcs
     * @param individualReferenceContactInfos
     * @return
     */
    public static List<IndividualReferenceContactInfoArc> copyIndividualReferenceContactInfoArcs(List<IndividualReferenceContactInfo> individualReferenceContactInfos) {
        List<IndividualReferenceContactInfoArc> individualReferenceContactInfoArcs = null;
        if (individualReferenceContactInfos != null && !individualReferenceContactInfos.isEmpty()) {
            individualReferenceContactInfoArcs = new ArrayList<>();
            for (IndividualReferenceContactInfo individualReferenceContactInfo : individualReferenceContactInfos) {
                IndividualReferenceContactInfoArc individualReferenceContactInfoArc = new IndividualReferenceContactInfoArc();
                individualReferenceContactInfoArc.setContactInfo(copyContactInfo(individualReferenceContactInfo.getContactInfo()));
                individualReferenceContactInfoArcs.add(individualReferenceContactInfoArc);
            }
        }
        return individualReferenceContactInfoArcs;
    }

    /**
     * Copy Applicant Contact Info Arc
     * @param individualContactInfos
     * @return
     */
    public static List<IndividualContactInfoArc> copyIndividualContactInfoArcs(List<IndividualContactInfo> individualContactInfos) {
        List<IndividualContactInfoArc> individualContactInfoArcs = null;
        if (individualContactInfos != null && !individualContactInfos.isEmpty()) {
            individualContactInfoArcs = new ArrayList<>();
            for (IndividualContactInfo individualContactInfo : individualContactInfos) {
                IndividualContactInfoArc individualContactInfoArc = copyIndividualContactInfo(individualContactInfo);
                individualContactInfoArcs.add(individualContactInfoArc);
            }
        }
        return individualContactInfoArcs;
    }

    /**
     * Copy Applicant Contact Info
     * @param individualContactInfo
     * @return
     */
    public static IndividualContactInfoArc copyIndividualContactInfo(IndividualContactInfo individualContactInfo) {
        IndividualContactInfoArc individualContactInfoArc = new IndividualContactInfoArc();
        individualContactInfoArc.setContactInfo(copyContactInfo(individualContactInfo.getContactInfo()));
        return individualContactInfoArc;
    }

    /**
     * Copy Contact Info
     * @return
     */
    public static ContactInfo copyContactInfo(ContactInfo contactInfo) {
        ContactInfo contactInfoArc = new ContactInfo();
        contactInfoArc.setTypeInfo(contactInfo.getTypeInfo());
        contactInfoArc.setTypeAddress(contactInfo.getTypeAddress());
        contactInfoArc.setValue(contactInfo.getValue());
        return contactInfoArc;
    }

    /**
     * Copy Employment Arcs
     * @param employments
     * @return
     */
    public static List<EmploymentArc> copyEmploymentArcs(List<Employment> employments) {
        List<EmploymentArc> employmentArcs = null;
        if (employments != null && !employments.isEmpty()) {
            employmentArcs = new ArrayList<EmploymentArc>();
            for (Employment employment : employments) {
                EmploymentArc employmentArc = copyEmployment(employment);
                employmentArcs.add(employmentArc);
            }
        }
        return employmentArcs;
    }

    /**
     * Copy Employment
     * @param employment
     * @return
     */
    public static EmploymentArc copyEmployment(Employment employment) {
        EmploymentArc empArc = new EmploymentArc();

        empArc.setPosition(employment.getPosition());
        empArc.setEmployerName(employment.getEmployerName());
        empArc.setTimeWithEmployerInYear(employment.getTimeWithEmployerInYear());
        empArc.setTimeWithEmployerInMonth(employment.getTimeWithEmployerInMonth());
        empArc.setRevenue(employment.getRevenue());
        empArc.setAllowance(employment.getAllowance());
        empArc.setBusinessExpense(employment.getBusinessExpense());
        empArc.setNoMonthInYear(employment.getNoMonthInYear());
        empArc.setEmploymentStatus(employment.getEmploymentStatus());
        empArc.setEmploymentIndustry(employment.getEmploymentIndustry());
        empArc.setEmploymentType(employment.getEmploymentType());
        empArc.setEmploymentCategory(employment.getEmploymentCategory());
        empArc.setLegalForm(employment.getLegalForm());
        empArc.setAllowCallToWorkPlace(employment.isAllowCallToWorkPlace());
        empArc.setSameApplicantAddress(employment.isSameApplicantAddress());
        empArc.setWorkPhone(employment.getWorkPhone());
        empArc.setSeniorityLevel(employment.getSeniorityLevel());

        if (employment.getAddress() != null) {
            empArc.setAddress(copyAddress(employment.getAddress()));
        }
        return empArc;
    }

    /**
     * Copy Applicant Address Arcs from quotation's applicantAddresses
     * @param individualAddresses
     * @return
     */
    public static List<IndividualAddressArc> copyIndividualAddressArcs(List<IndividualAddress> individualAddresses) {
        List<IndividualAddressArc> individualAddressArcs = null;
        if (individualAddresses != null && !individualAddresses.isEmpty()) {
            individualAddressArcs = new ArrayList<>();
            for (IndividualAddress individualAddress : individualAddresses) {
                IndividualAddressArc individualAddressArc = new IndividualAddressArc();
                individualAddressArc.setAddress(copyAddress(individualAddress.getAddress()));
                individualAddressArcs.add(individualAddressArc);
            }
        }
        return individualAddressArcs;
    }

    /**
     *
     * @param address
     * @return
     */
    public static AddressArc copyAddress(Address address) {
        AddressArc addArc = new AddressArc();
        addArc.setHouseNo(address.getHouseNo());
        addArc.setStreet(address.getStreet());
        addArc.setCountry(address.getCountry());
        addArc.setProvince(address.getProvince());
        addArc.setCommune(address.getCommune());
        addArc.setDistrict(address.getDistrict());
        addArc.setVillage(address.getVillage());
        addArc.setProperty(address.getProperty());
        addArc.setTimeAtAddressInMonth(address.getTimeAtAddressInMonth());
        addArc.setTimeAtAddressInYear(address.getTimeAtAddressInYear());
        addArc.setStatusRecord(EStatusRecord.ACTIV);
        addArc.setType(address.getType());
        return addArc;
    }
    /**
     *
     * @param dealer
     * @param assetMake
     * @param assetCategory
     * @return
     */
    public static DealerAttribute getDealerAttribute(Dealer dealer, AssetMake assetMake, AssetCategory assetCategory) {
        List<DealerAttribute> dealerAttributes = null;
        if (dealer != null) {
            dealerAttributes = dealer.getDealerAttributes();
        }
        if (dealerAttributes != null && !dealerAttributes.isEmpty()) {
            for (DealerAttribute dealerAttribute : dealerAttributes) {
                if (dealerAttribute.getAssetMake().getId().equals(assetMake.getId())
                        && dealerAttribute.getAssetCategory().getId().equals(assetCategory.getId())) {
                    return dealerAttribute;
                }
            }
        }
        return null;
    }
}
