package com.soma.mfinance.glf.accounting.service;

import java.util.Date;
import java.util.List;

import com.soma.mfinance.core.quotation.model.EInsuranceYear;
import com.soma.mfinance.core.registrations.model.ProfileContractStatus;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.service.BaseEntityService;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.shared.accounting.InsuranceExpense;
import com.soma.mfinance.core.shared.accounting.InsuranceIncome;
import com.soma.mfinance.core.shared.accounting.InsuranceIncomeAdjustment;
import com.soma.mfinance.core.shared.accounting.LeaseAdjustment;
import com.soma.mfinance.core.shared.accounting.LeaseTransaction;
import com.soma.mfinance.core.shared.accounting.LeasesReport;
import com.soma.mfinance.core.shared.accounting.ReferalFee;
import com.soma.mfinance.core.shared.accounting.ReferalFeeAdjustment;
import com.soma.mfinance.core.shared.accounting.RegistrationExpense;
import com.soma.mfinance.core.shared.accounting.ServiceTransaction;
import com.soma.mfinance.core.shared.accounting.ServicingIncome;
import com.soma.mfinance.core.shared.accounting.ServicingIncomeAdjustment;

/**
 * @author kimsuor.seang
 */
public interface GLFLeasingAccountingService extends BaseEntityService {
	/**
	 *
 	 * @param dealerType
	 * @param dealer
	 * @param reference
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<LeaseTransaction> getLeaseTransactions(EDealerType dealerType, Dealer dealer, String reference, Date startDate, Date endDate);

	/**
	 *
	 * @param dealerType
	 * @param dealer
	 * @param reference
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<LeaseTransaction> getNetLeasings(EDealerType dealerType, Dealer dealer, String reference, Date startDate, Date endDate);

	/**
	 *
	 * @param dealerType
	 * @param dealer
	 * @param reference
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<ServiceTransaction> getServiceTransactions(EDealerType dealerType, Dealer dealer, String reference, Date startDate, Date endDate);

	/**
	 *
	 * @param dealerType
	 * @param dealer
	 * @param reference
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<ReferalFee> getReferalFees(EDealerType dealerType, Dealer dealer, String reference, Date startDate, Date endDate);

	/**
	 *
	 * @param dealerType
	 * @param dealer
	 * @param reference
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<InsuranceIncome> getInsuranceIncomes(EDealerType dealerType, Dealer dealer, String reference, Date startDate, Date endDate);

	/**
	 *
	 * @param dealerType
	 * @param dealer
	 * @param reference
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<InsuranceExpense> getInsuranceExpenses(EDealerType dealerType, Dealer dealer, String reference, Date startDate, Date endDate);

	/**
	 *
	 * @param dealerType
	 * @param dealer
	 * @param reference
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<RegistrationExpense> getRegistrationExpenses(EDealerType dealerType, Dealer dealer, String reference, Date startDate, Date endDate);

	/**
	 *
	 * @param dealerType
	 * @param dealer
	 * @param reference
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<ServicingIncome> getServicingIncomes(EDealerType dealerType, Dealer dealer, String reference, Date startDate, Date endDate);

	/**
	 *
	 * @param dealerType
	 * @param dealer
	 * @param contractStatus
	 * @param reference
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<LeaseAdjustment> getLeaseAdjustments(EDealerType dealerType, Dealer dealer, EWkfStatus contractStatus, String reference, Date startDate, Date endDate);

	/**
	 *
	 * @param dealerType
	 * @param dealer
	 * @param contractStatus
	 * @param reference
	 * @param startDate
	 * @param endDate
	 * @param payStartDate
	 * @param insuranceYear
	 * @return
	 */
	List<LeaseAdjustment> getLeaseAdjustments(EDealerType dealerType, Dealer dealer, ProfileContractStatus contractStatus, String reference, Date startDate, Date endDate, Date payStartDate, EInsuranceYear insuranceYear);

	/**
	 *
	 * @param dealerType
	 * @param dealer
	 * @param reference
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<ReferalFeeAdjustment> getReferalFeeAdjustments(EDealerType dealerType, Dealer dealer, String reference, Date startDate, Date endDate);

	/**
	 *
	 * @param dealerType
	 * @param dealer
	 * @param reference
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<InsuranceIncomeAdjustment> getInsuranceIncomeAdjustments(EDealerType dealerType, Dealer dealer, String reference, Date startDate, Date endDate);

	/**
	 *
	 * @param dealerType
	 * @param dealer
	 * @param reference
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<ServicingIncomeAdjustment> getServicingIncomeAdjustments(EDealerType dealerType, Dealer dealer, String reference, Date startDate, Date endDate);

	/**
	 *
	 * @param dealerType
	 * @param dealer
	 * @param reference
	 * @param endDate
	 * @return
	 */
	List<LeaseTransaction> getRemainingBalance(EDealerType dealerType, Dealer dealer, String reference, Date endDate);

	/**
	 *
	 * @param calculDate
	 * @param restrictions
	 * @return
	 */
	List<LeasesReport> getLeaseReports(Date calculDate, BaseRestrictions<Contract> restrictions);
}
