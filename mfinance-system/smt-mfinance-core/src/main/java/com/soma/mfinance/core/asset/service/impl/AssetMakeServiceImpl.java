package com.soma.mfinance.core.asset.service.impl;

import com.soma.mfinance.core.asset.model.AssetMake;
import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soma.mfinance.core.asset.model.AssetMake;
import com.soma.mfinance.core.asset.service.AssetMakeSequenceImpl;
import com.soma.mfinance.core.asset.service.AssetMakeService;
import com.soma.mfinance.core.asset.service.AssetSequenceManager;
import com.soma.mfinance.core.quotation.SequenceGenerator;

/**
 * Asset make service
 * @author kimsuor.seang
 */       
@Service("assetMakeService")
public class AssetMakeServiceImpl extends BaseEntityServiceImpl implements AssetMakeService {
	
	/** */
	private static final long serialVersionUID = -6293684761226832807L;
	
	@Autowired
    private EntityDao dao;
	
	/**
	 * @see org.seuksa.frmk.service.impl.BaseEntityServiceImpl#getDao()
	 */
	@Override
	public BaseEntityDao getDao() {
		return dao;
	}
	
	/**
	 * @see com.soma.mfinance.core.asset.service.AssetMakeService#saveOrUpdateAssetMake(AssetMake)
	 */
	@Override
	public void saveOrUpdateAssetMake(AssetMake assetMake) {
		if (assetMake.getCode().length() != 3) {
			SequenceGenerator sequenceGenerator = new AssetMakeSequenceImpl(AssetSequenceManager.getInstance().getSequenceAssetMake());
			assetMake.setCode(sequenceGenerator.generate());
		}
		saveOrUpdate(assetMake);
	}
	
}
