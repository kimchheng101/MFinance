package com.soma.mfinance.core.auction.panel.auction;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.auction.model.ContractAuctionData;
import com.soma.mfinance.core.auction.service.AuctionService;
import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.workflow.AuctionWkfStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.RowRenderer;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.auction.model.ContractAuctionData;
import com.soma.mfinance.core.auction.service.AuctionService;
import com.soma.mfinance.core.collection.model.Collection;
import com.vaadin.data.Item;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.Entity;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.amount.Amount;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Table panel for auction panel
 *
 * @author kimsuor.seang
 * @modify kimsuor.seang (22/09/2017)
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AuctionTablePanel extends AbstractTablePanel<Contract> implements FMEntityField {

    private static final long serialVersionUID = -4303398771579970070L;

    @Autowired
    private AuctionService auctionService;

    @PostConstruct
    public void PostConstruct() {
        setCaption(I18N.message("auctions"));
        setSizeFull();
        setMargin(true);
        setSpacing(true);

        super.init(I18N.message("auctions"));
        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addEditClickListener(this);
        navigationPanel.addRefreshClickListener(this);
    }

    /**
     * @see AbstractTablePanel#createPagedDataProvider()
     */
    @Override
    protected PagedDataProvider<Contract> createPagedDataProvider() {
        PagedDefinition<Contract> pagedDefinition = new PagedDefinition<Contract>(searchPanel.getRestrictions());

        pagedDefinition.setRowRenderer(new AuctionRowRenderer());
        pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Align.RIGHT, 70);
        pagedDefinition.addColumnDefinition("auction.status", I18N.message("auction.status").toUpperCase(), String.class, Align.RIGHT, 120);
        pagedDefinition.addColumnDefinition("storage.location", I18N.message("storage.location").toUpperCase(), String.class, Align.RIGHT, 130);
        pagedDefinition.addColumnDefinition("collection.stock.status", I18N.message("collection.stock.status").toUpperCase(), String.class, Align.RIGHT, 130);
        pagedDefinition.addColumnDefinition("lastname.en", I18N.message("lastname.en").toUpperCase(), String.class, Align.LEFT, 130);
        pagedDefinition.addColumnDefinition("firstname.en", I18N.message("firstname.en").toUpperCase(), String.class, Align.LEFT, 130);
        pagedDefinition.addColumnDefinition("num.day.from.repossess", I18N.message("num.day.from.repossess").toUpperCase(), Long.class, Align.RIGHT, 200);
        pagedDefinition.addColumnDefinition("num.of.auction", I18N.message("num.of.auction").toUpperCase(), Long.class, Align.RIGHT, 140);
        pagedDefinition.addColumnDefinition("contract.reference", I18N.message("contract.reference").toUpperCase(), String.class, Align.RIGHT, 150);
        pagedDefinition.addColumnDefinition("financialProduct", I18N.message("financial.product").toUpperCase(), String.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("remaining.principal.balance", I18N.message("remaining.principal.balance").toUpperCase(), Double.class, Align.RIGHT, 180);
        pagedDefinition.addColumnDefinition("remaining.unearned.income.balance", I18N.message("remaining.unearned.income.balance").toUpperCase(), Double.class, Align.RIGHT, 180);
        pagedDefinition.addColumnDefinition("remaining.insurance.balance", I18N.message("remaining.insurance.balance").toUpperCase(), Double.class, Align.RIGHT, 180);
        pagedDefinition.addColumnDefinition("remaining.service.income.balance", I18N.message("remaining.service.income.balance").toUpperCase(), Double.class, Align.RIGHT, 200);
        pagedDefinition.addColumnDefinition("penalty.amount", I18N.message("penalty.amount").toUpperCase(), Double.class, Align.RIGHT, 140);
        pagedDefinition.addColumnDefinition("repossession.fee", I18N.message("repossession.fee").toUpperCase(), Amount.class, Align.RIGHT, 140);
        pagedDefinition.addColumnDefinition("collection.fee", I18N.message("collection.fee").toUpperCase(), Amount.class, Align.RIGHT, 140);
        pagedDefinition.addColumnDefinition("total.amount", I18N.message("total.amount").toUpperCase(), Amount.class, Align.RIGHT, 140);
        pagedDefinition.addColumnDefinition("motorcycle.estimation.price.1", I18N.message("motorcycle.estimation.price.1").toUpperCase(), Amount.class, Align.RIGHT, 200);
        pagedDefinition.addColumnDefinition("motorcycle.estimation.price.2", I18N.message("motorcycle.estimation.price.2").toUpperCase(), Amount.class, Align.RIGHT, 200);
        pagedDefinition.addColumnDefinition("motorcycle.estimation.price.3", I18N.message("motorcycle.estimation.price.3").toUpperCase(), Amount.class, Align.RIGHT, 200);
        pagedDefinition.addColumnDefinition("average.estimated.price.of.motorcycle", I18N.message("average.estimated.price.of.motorcycle").toUpperCase(), Amount.class, Align.RIGHT, 220);
        pagedDefinition.addColumnDefinition("model", I18N.message("model").toUpperCase(), String.class, Align.RIGHT, 150);
        pagedDefinition.addColumnDefinition("year", I18N.message("year").toUpperCase(), String.class, Align.RIGHT, 100);
        pagedDefinition.addColumnDefinition("color", I18N.message("color").toUpperCase(), String.class, Align.RIGHT, 100);
        pagedDefinition.addColumnDefinition("month.of.repossession", I18N.message("month.of.repossession").toUpperCase(), String.class, Align.RIGHT, 150);
        pagedDefinition.addColumnDefinition("engine.number", I18N.message("engine.number").toUpperCase(), String.class, Align.RIGHT, 150);
        pagedDefinition.addColumnDefinition("chassis.number", I18N.message("chassis.number").toUpperCase(), String.class, Align.RIGHT, 150);
        pagedDefinition.addColumnDefinition("asset.plate.number", I18N.message("plate.number").toUpperCase(), String.class, Align.RIGHT, 150);
        pagedDefinition.addColumnDefinition("asset.price", I18N.message("asset.price").toUpperCase(), Double.class, Align.RIGHT, 150);
        pagedDefinition.addColumnDefinition("contract.date", I18N.message("contract.date").toUpperCase(), Date.class, Align.RIGHT, 150);
        pagedDefinition.addColumnDefinition("lease.amount", I18N.message("lease.amount").toUpperCase(), Double.class, Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("mileage", I18N.message("mileage").toUpperCase(), Integer.class, Align.RIGHT, 100);
        pagedDefinition.addColumnDefinition("exterior.status", I18N.message("exterior.status").toUpperCase(), String.class, Align.RIGHT, 150);
        pagedDefinition.addColumnDefinition("engine.status", I18N.message("engine.status").toUpperCase(), String.class, Align.RIGHT, 150);
        pagedDefinition.addColumnDefinition("parts.status", I18N.message("parts.status").toUpperCase(), String.class, Align.RIGHT, 150);
        pagedDefinition.addColumnDefinition("registration.status", I18N.message("registration.status").toUpperCase(), String.class, Align.RIGHT, 150);
        pagedDefinition.addColumnDefinition("plate.number", I18N.message("plate.number").toUpperCase(), String.class, Align.RIGHT, 150);

        EntityPagedDataProvider<Contract> pagedDataProvider = new EntityPagedDataProvider<Contract>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    /**
     * @see AbstractTablePanel#getEntity()
     */
    @Override
    protected Contract getEntity() {
        final Long id = getItemSelectedId();
        if (id != null) {
            return ENTITY_SRV.getById(Contract.class, id);
        }
        return null;
    }

    /**
     * @see AbstractTablePanel#createSearchPanel()
     */
    @Override
    protected AbstractSearchPanel<Contract> createSearchPanel() {
        List<EWkfStatus> auctionWkfStatus = new ArrayList<EWkfStatus>();

        if (ProfileUtil.isManager()) {
            auctionWkfStatus.add(AuctionWkfStatus.VAL);
        } else if (ProfileUtil.isCollectionSupervisor() || ProfileUtil.isAuctionController()) {
            auctionWkfStatus.add(AuctionWkfStatus.EVA);
            auctionWkfStatus.add(AuctionWkfStatus.CNS);
        }

        AuctionSearchPanel searchPanel = new AuctionSearchPanel(this);
        searchPanel.assignValues(auctionWkfStatus);
        return searchPanel;
    }

    /**
     * Custom row renderer
     * @author kimsuor.seang
     */
    private class AuctionRowRenderer implements RowRenderer {

        /**
         * @see RowRenderer#renderer(Item, Entity)
         */
        @SuppressWarnings("unchecked")
        @Override
        public void renderer(Item item, Entity entity) {
            final Contract contract = (Contract) entity;
            Collection collection = contract.getCollection();
            Asset asset = contract.getAsset();
            ContractAuctionData auctionData = contract.getContractAuctionData();
            Applicant applicant = contract.getQuotation().getMainApplicant();

            item.getItemProperty(ID).setValue(contract.getId());
            item.getItemProperty("auction.status").setValue(contract.getAuctionStatus() != null ? contract.getAuctionStatus().getDesc() : "");
            item.getItemProperty("storage.location").setValue(contract.getStorageLocation() != null ? contract.getStorageLocation().getDescEn() : "");
            item.getItemProperty("collection.stock.status").setValue(auctionData.getAssetStockStatus() != null ? auctionData.getAssetStockStatus().getDescEn() : "");
            item.getItemProperty("lastname.en").setValue(applicant.getLastNameEn());
            item.getItemProperty("firstname.en").setValue(applicant.getFirstNameEn());

            Date reprocessDate = auctionData.getRequestRepossessedDate();
            item.getItemProperty("num.day.from.repossess").setValue(DateUtils.getDiffInDays(DateUtils.today(), reprocessDate));
            item.getItemProperty("num.of.auction").setValue(auctionService.getNumberOfAuction(contract));
            item.getItemProperty("contract.reference").setValue(contract.getReference());
            item.getItemProperty("financialProduct").setValue(contract.getFinancialProduct().getDescEn());
            item.getItemProperty("remaining.principal.balance").setValue(auctionService.getRemainingPrincipalBalance(collection));

            Double[] auctionBalance = auctionService.getRemainingServiceIncomeBalance(contract);
            item.getItemProperty("remaining.service.income.balance").setValue(auctionBalance[0]);
            item.getItemProperty("remaining.insurance.balance").setValue(auctionBalance[1]);
            item.getItemProperty("remaining.unearned.income.balance").setValue(auctionBalance[2]);
            item.getItemProperty("lease.amount").setValue(MyNumberUtils.getDouble(contract.getTiFinancedAmount()));

            if (auctionData != null) {
                double reprocessFee = MyNumberUtils.getDouble(auctionData.getRepossessionFeeUsd() != null ? auctionData.getRepossessionFeeUsd() : 0);
                double collectionFee = MyNumberUtils.getDouble(auctionData.getCollectionFeeUsd() != null ? auctionData.getCollectionFeeUsd() : 0);
                item.getItemProperty("penalty.amount").setValue(auctionService.getPenaltyAmount(collection));
                item.getItemProperty("repossession.fee").setValue(AmountUtils.convertToAmount(reprocessFee));
                item.getItemProperty("collection.fee").setValue(AmountUtils.convertToAmount(collectionFee));

                Double assetEstimate1 = MyNumberUtils.getDouble(auctionData.getTiAssetEstimationPrice1());
                Double assetEstimate2 = MyNumberUtils.getDouble(auctionData.getTiAssetEstimationPrice2());
                Double assetEstimate3 = MyNumberUtils.getDouble(auctionData.getTiAssetEstimationPrice3());
                item.getItemProperty("motorcycle.estimation.price.1").setValue(AmountUtils.convertToAmount(assetEstimate1));
                item.getItemProperty("motorcycle.estimation.price.2").setValue(AmountUtils.convertToAmount(assetEstimate2));
                item.getItemProperty("motorcycle.estimation.price.3").setValue(AmountUtils.convertToAmount(assetEstimate3));
                item.getItemProperty("average.estimated.price.of.motorcycle").setValue(AmountUtils.convertToAmount((assetEstimate1 + assetEstimate2 + assetEstimate3) / 3));

                item.getItemProperty("mileage").setValue(auctionData.getMilleage());
                item.getItemProperty("exterior.status").setValue(auctionData.getAssetExteriorStatus() != null ?
                        auctionData.getAssetExteriorStatus().getDescEn() : "");
                item.getItemProperty("engine.status").setValue(auctionData.getAssetEnginStatus() != null ?
                        auctionData.getAssetEnginStatus().getDescEn() : "");
                item.getItemProperty("parts.status").setValue(auctionData.getAssetPartsStatus() != null ?
                        auctionData.getAssetPartsStatus().getDescEn() : "");
                item.getItemProperty("registration.status").setValue(auctionData.getAssetRegistrationStatus() != null ?
                        auctionData.getAssetRegistrationStatus().getDescEn() : "");
                item.getItemProperty("plate.number").setValue(auctionData.getAssetPlateNumber() != null ? auctionData.getAssetPlateNumber().getDescEn() : "");
                item.getItemProperty("total.amount").setValue(AmountUtils.convertToAmount(reprocessFee + collectionFee));
            }
            item.getItemProperty("contract.date").setValue(contract.getStartDate());

            if (asset != null) {
                item.getItemProperty("model").setValue(asset.getModel().getDesc());
                item.getItemProperty("year").setValue(asset.getAssetYear().getDesc());
                item.getItemProperty("color").setValue(asset.getColor().getDescEn());
                item.getItemProperty("engine.number").setValue(asset.getEngineNumber());
                item.getItemProperty("chassis.number").setValue(asset.getChassisNumber());
                item.getItemProperty("asset.plate.number").setValue(asset.getPlateNumber());
                item.getItemProperty("asset.price").setValue(MyNumberUtils.getDouble(asset.getTiAssetApprPrice()));
            }

            item.getItemProperty("month.of.repossession").setValue(DateUtils.getDateLabel(reprocessDate));
        }
    }

}
