package com.soma.mfinance.core.contract.service.aftersales.impl;

import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.ContractAdjustment;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowCode;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowType;
import com.soma.mfinance.core.contract.model.cashflow.ETreasuryType;
import com.soma.mfinance.core.contract.service.aftersales.*;
import com.soma.mfinance.core.contract.service.aftersales.*;
import com.soma.mfinance.core.contract.service.cashflow.impl.CashflowUtils;
import com.soma.mfinance.core.financial.model.EServiceType;
import com.soma.mfinance.core.financial.model.FinService;
import com.soma.mfinance.core.financial.model.ProductLine;
import com.soma.mfinance.core.payment.model.EPaymentMethod;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.financialproduct.FinancialProductEntityField;
import com.soma.mfinance.glf.accounting.service.GLFLeasingAccountingService;
import com.soma.ersys.finance.accounting.model.JournalEvent;
import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author kimsuor.seang
 *
 */
@Service("lossService")
public class LossServiceImpl extends BaseEntityServiceImpl implements LossService {
	/** */
	private static final long serialVersionUID = -7054001806232224584L;

	@Autowired
    private EntityDao dao;
	
	@Autowired
	private EarlySettlementService earlySettlementService;
		
	@Autowired
	private GLFLeasingAccountingService accountingService;
	
	@Override
	public EntityDao getDao() {
		return dao;
	}
	
	/**
	 * @param request
	 * @return
	 */
	public LossSimulateResponse simulate(LossSimulateRequest request) {
		
		EarlySettlementSimulateRequest earlySettlementSimulateRequest = new EarlySettlementSimulateRequest();
		earlySettlementSimulateRequest.setCotraId(request.getCotraId());
		earlySettlementSimulateRequest.setEventDate(request.getEventDate());
		earlySettlementSimulateRequest.setIncludePenalty(false);		
		EarlySettlementSimulateResponse earlySettlementSimulateResponse = earlySettlementService.simulate(earlySettlementSimulateRequest);
		
		LossSimulateResponse response = new LossSimulateResponse();
		
		/*Amount adjustmentInterest = earlySettlementSimulateResponse.getAdjustmentInterest();
		adjustmentInterest.plus(earlySettlementSimulateResponse.getBalanceInterest());*/

		response.setCotraId(request.getCotraId());
		response.setEventDate(request.getEventDate());
		response.setTotalPrincipal(earlySettlementSimulateResponse.getBalanceCapital());
		response.setTotalOther(earlySettlementSimulateResponse.getTotalOther());
		response.setInsuranceFee(earlySettlementSimulateResponse.getInsuranceFee());
		response.setServicingFee(earlySettlementSimulateResponse.getServicingFee());
		response.setTransferFee(earlySettlementSimulateResponse.getBalanceTransferFee());
		response.setTotalInterest(earlySettlementSimulateResponse.getBalanceInterest());
		response.setCashflows(earlySettlementSimulateResponse.getCashflows());
		
		return response;
	}
	
	/**
	 * @param request
	 * @return
	 */
	public LossSaveResponse save(LossSaveRequest request) {
		LossSaveResponse lossSaveResponse = new LossSaveResponse();
		return lossSaveResponse;
	}
	
	/**
	 * @param request
	 */
	public void validate(LossValidateRequest request) {
		Contract contract = getById(Contract.class, request.getCotraId());
		
		ContractAdjustment contractAdjustment = contract.getContractAdjustment();
		if (contractAdjustment == null) {
			contractAdjustment = new ContractAdjustment();
			contractAdjustment.setContract(contract);
		}
		
		ProductLine productLine = getById(ProductLine.class, contract.getProductLine().getId());
		Integer term = contract.getTerm();
		
		Date lossDate = request.getEventDate();
		EPaymentMethod lossPaymentCondition = EPaymentMethod.LOSS;

		if(request.getContractStatus() != null){
			contract.setWkfStatus(request.getContractStatus());
			contract.setOverdue(false);
			contract.setLostDate(request.getEventDate());
			Quotation quotation = contract.getQuotation();
			quotation.setWkfStatus(request.getContractStatus());
			saveOrUpdate(quotation);
		}

		//TODO: LOOP to generate Cashflow
		for(int numCashflow = 1; numCashflow <=6 ; numCashflow++) {
			List<Cashflow> lossCashflows = new ArrayList<>();
			ECashflowType cashflowType = ECashflowType.FEE;
			JournalEvent journalEvent = null;
			FinService serviceFee = null;

			double teAmount = 0d ,vatAmount = 0d , tiAmount = 0d;

			if(numCashflow==1) {
				cashflowType = ECashflowType.CAP;
				journalEvent = getByCode(JournalEvent.class, ECashflowType.CAP_JOURNAL_EVENT);
				teAmount = request.getTotalPrincipal().getTeAmount();
				vatAmount = request.getTotalPrincipal().getVatAmount();
				tiAmount = request.getTotalPrincipal().getTiAmount();
				contractAdjustment.setTiAdjustmentPrincipal(request.getTotalPrincipal().getTiAmount());
				contractAdjustment.setVatAdjustmentPrincipal(request.getTotalPrincipal().getVatAmount());
				contractAdjustment.setTeAdjustmentPrincipal(request.getTotalPrincipal().getTeAmount());
			} else if (numCashflow==2) {
				cashflowType = ECashflowType.IAP;
				journalEvent = getByCode(JournalEvent.class, ECashflowType.IAP_JOURNAL_EVENT);
				teAmount = request.getTotalInterest().getTeAmount();
				vatAmount = request.getTotalInterest().getVatAmount();
				tiAmount = request.getTotalInterest().getTiAmount();
				contractAdjustment.setTiAdjustmentInterest(request.getTotalInterest().getTiAmount());
				contractAdjustment.setTeAdjustmentInterest(request.getTotalInterest().getTeAmount());
				contractAdjustment.setVatAdjustmentInterest(request.getTotalInterest().getVatAmount());
			} else if (numCashflow==3 && MyNumberUtils.getDouble(request.getInsuranceFee().getTeAmount())>0) {
				serviceFee = getByCode(FinService.class, EServiceType.INSFEE.getCode());
				teAmount = request.getInsuranceFee().getTeAmount();
				vatAmount = request.getInsuranceFee().getVatAmount();
				tiAmount = request.getInsuranceFee().getTiAmount();
				contractAdjustment.setTiUnpaidUnearnedInsuranceIncomeUsd(request.getInsuranceFee().getTiAmount());
				contractAdjustment.setVatUnpaidUnearnedInsuranceIncomeUsd(request.getInsuranceFee().getVatAmount());
				contractAdjustment.setTeUnpaidUnearnedInsuranceIncomeUsd(request.getInsuranceFee().getTeAmount());
			} else if (numCashflow==4 && MyNumberUtils.getDouble(request.getServicingFee().getTeAmount()) > 0) {
				if(contract.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_PUBLIC)) {
					serviceFee = getByCode(FinService.class, EServiceType.SRVFEE.getCode());
				} else if(contract.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_EXISTING)) {
					serviceFee = getByCode(FinService.class, EServiceType.SRVFEEEXT.getCode());
				}
				teAmount = request.getServicingFee().getTeAmount();
				vatAmount = request.getServicingFee().getVatAmount();
				tiAmount = request.getServicingFee().getTiAmount();
				contractAdjustment.setTiUnpaidUnearnedServicingIncomeUsd(request.getServicingFee().getTiAmount());
				contractAdjustment.setVatUnpaidUnearnedServicingIncomeUsd(request.getServicingFee().getVatAmount());
				contractAdjustment.setTeUnpaidUnearnedServicingIncomeUsd(request.getServicingFee().getTeAmount());
			} else if(numCashflow == 5 && MyNumberUtils.getDouble(request.getTransferFee() != null ? request.getTransferFee().getTeAmount() : null) > 0) {
				serviceFee = getByCode(FinService.class, EServiceType.TRANSFEE.getCode());
			} else if(numCashflow==6 && MyNumberUtils.getDouble(request.getTotalOther().getTeAmount()) > 0) {
				teAmount = request.getTotalOther().getTeAmount();
				vatAmount = request.getTotalOther().getVatAmount();
				tiAmount = request.getTotalOther().getTiAmount();
			}

			if(teAmount>0) {
				Cashflow cashflow = CashflowUtils.createCashflow(productLine, null,
						contract, contract.getVatValue(),
						cashflowType, ETreasuryType.APP,journalEvent,
						lossPaymentCondition, teAmount, vatAmount, tiAmount, lossDate, lossDate, lossDate, term);
				cashflow.setCashflowCode(ECashflowCode.LOS);
				cashflow.setPaid(true);

				if(serviceFee!=null)
					cashflow.setService(serviceFee);

				saveOrUpdate(cashflow);
				lossCashflows.add(cashflow);
			}
		}

		/*Date calculDate = DateUtils.addDaysDate(request.getEventDate(), -1);
		List<LeaseTransaction> leaseTransactions = accountingService.getLeaseTransactions(null, contract.getDealer(), contract.getReference(), 
				DateUtils.getDateAtBeginningOfMonth(calculDate), calculDate);
		if (leaseTransactions != null && leaseTransactions.size() == 1) {
			LeaseTransaction leaseTransaction = leaseTransactions.get(0);
			contractAdjustment.setTiBalanceInterestInSuspendUsd(leaseTransaction.getInterestInSuspendCumulated().getTiAmount());
			contractAdjustment.setVatBalanceInterestInSuspendUsd(leaseTransaction.getInterestInSuspendCumulated().getVatAmount());
			contractAdjustment.setTeBalanceInterestInSuspendUsd(leaseTransaction.getInterestInSuspendCumulated().getTeAmount());
			contractAdjustment.setTiUnpaidAccruedInterestReceivableUsd(leaseTransaction.getInterestReceivable().getTiAmount());
			contractAdjustment.setVatUnpaidAccruedInterestReceivableUsd(leaseTransaction.getInterestReceivable().getVatAmount());
			contractAdjustment.setTeUnpaidAccruedInterestReceivableUsd(leaseTransaction.getInterestReceivable().getTeAmount());
		}
		
		List<ReferalFee> referalFees = accountingService.getReferalFees(null, contract.getDealer(), contract.getReference(), 
				DateUtils.getDateAtBeginningOfMonth(calculDate), calculDate);
		if (referalFees != null && referalFees.size() == 1) {
			ReferalFee referalFee = referalFees.get(0);
			contractAdjustment.setTiUnpaidDeferredCommissionReferalFeeUsd(referalFee.getDeferredCommissionReferalFee().getTiAmount());
			contractAdjustment.setVatUnpaidDeferredCommissionReferalFeeUsd(referalFee.getDeferredCommissionReferalFee().getVatAmount());
			contractAdjustment.setTeUnpaidDeferredCommissionReferalFeeUsd(referalFee.getDeferredCommissionReferalFee().getTeAmount());
			contractAdjustment.setTiUnpaidAcrrualExpensesReferalFeeUsd(referalFee.getAcrrualExpenses().getTiAmount());
			contractAdjustment.setVatUnpaidAcrrualExpensesReferalFeeUsd(referalFee.getAcrrualExpenses().getVatAmount());
			contractAdjustment.setTeUnpaidAcrrualExpensesReferalFeeUsd(referalFee.getAcrrualExpenses().getTeAmount());
		}
		
		List<InsuranceIncome> insuranceIncomes = accountingService.getInsuranceIncomes(null, contract.getDealer(), contract.getReference(), 
				DateUtils.getDateAtBeginningOfMonth(calculDate), calculDate);
		if (insuranceIncomes != null && insuranceIncomes.size() == 1) {
			InsuranceIncome insuranceIncome = insuranceIncomes.get(0);
			contractAdjustment.setTiBalanceInsuranceIncomeInSuspendUsd(insuranceIncome.getInsuranceIncomeInSuspendCumulated().getTiAmount());
			contractAdjustment.setVatBalanceInsuranceIncomeInSuspendUsd(insuranceIncome.getInsuranceIncomeInSuspendCumulated().getVatAmount());
			contractAdjustment.setTeBalanceInsuranceIncomeInSuspendUsd(insuranceIncome.getInsuranceIncomeInSuspendCumulated().getTeAmount());
			contractAdjustment.setTiUnpaidAccrualReceivableInsuranceIncomeUsd(insuranceIncome.getAccountReceivable().getTiAmount());
			contractAdjustment.setVatUnpaidAccrualReceivableInsuranceIncomeUsd(insuranceIncome.getAccountReceivable().getVatAmount());
			contractAdjustment.setTeUnpaidAccrualReceivableInsuranceIncomeUsd(insuranceIncome.getAccountReceivable().getTeAmount());
		}
		
		if (request.getWkfStatus() != null &&
				(request.getWkfStatus().equals(ContractWkfStatus.WRI) || request.getWkfStatus().equals(ContractWkfStatus.REP))) {
			List<ServicingIncome> servicingIncomes = accountingService.getServicingIncomes(null, contract.getDealer(), contract.getReference(), 
					DateUtils.getDateAtBeginningOfMonth(calculDate), calculDate);
			if (servicingIncomes != null && servicingIncomes.size() == 1) {
				ServicingIncome servicingIncome = servicingIncomes.get(0);
				contractAdjustment.setTiBalanceServicingIncomeInSuspendUsd(servicingIncome.getServicingIncomeInSuspendCumulated().getTiAmount());
				contractAdjustment.setVatBalanceServicingIncomeInSuspendUsd(servicingIncome.getServicingIncomeInSuspendCumulated().getVatAmount());
				contractAdjustment.setTeBalanceServicingIncomeInSuspendUsd(servicingIncome.getServicingIncomeInSuspendCumulated().getTeAmount());
				contractAdjustment.setTiUnpaidAccrualReceivableServicingIncomeUsd(servicingIncome.getAccountReceivable().getTiAmount());
				contractAdjustment.setVatUnpaidAccrualReceivableServicingIncomeUsd(servicingIncome.getAccountReceivable().getVatAmount());
				contractAdjustment.setTeUnpaidAccrualReceivableServicingIncomeUsd(servicingIncome.getAccountReceivable().getTeAmount());
			}
		}
		
		saveOrUpdate(contractAdjustment);*/

		// Update application to in-active
		/*ClientLOSApplication.updateUnActiveApplication(ContractUtils.getApplicationID(contract));
		*/
	}

}
