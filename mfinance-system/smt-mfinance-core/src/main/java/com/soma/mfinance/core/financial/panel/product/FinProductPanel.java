package com.soma.mfinance.core.financial.panel.product;

import com.soma.mfinance.core.application.panel.ApplicationFormPanel;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.asset.model.AssetModel;
import com.soma.mfinance.core.asset.model.appraisal.EAppraisalRange;
import com.soma.mfinance.core.custom.erefdata.ERefDataHelper;
import com.soma.mfinance.core.financial.model.*;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.interestrate.InterestRate;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationService;
import com.soma.mfinance.core.shared.financialproduct.FinancialProductEntityField;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.finance.services.shared.system.EFrequency;
import com.soma.finance.services.tools.LoanUtils;
import com.soma.finance.services.tools.formula.Rate;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.FieldEvents;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.Reindeer;
import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.CrudAction;
import org.seuksa.frmk.model.entity.RefDataId;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyMathUtils;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.amount.AmountUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author kimsuor.seang
 */
public class FinProductPanel extends AbstractTabPanel implements FinancialProductEntityField, ValueChangeListener, FieldEvents.BlurListener, Button.ClickListener, FinServicesHelper {

    private static final long serialVersionUID = -8137888259948555117L;
    private EntityRefComboBox<FinProduct> cbxFinancialProduct;
    private EntityRefComboBox<Term> cbxTermInMonth;
    private EntityRefComboBox<InterestRate> cbxPeriodicInterestRate;
    private EntityRefComboBox<LeaseAmountPercent> cbxLeaseAmountPercentage;
    private TextField txtLeaseAmount;
    private TextField txtAppraisalEstimateAmount;
    private TextField txtTotalInstallAmount;
    private ERefDataComboBox<EFrequency> cbxFrequency;
    private TextField txtInstallmentAmont;
    private HorizontalLayout invalidMessageLayout;
    private VerticalLayout servicesLayout;
    private Boolean invalidQuotation;
    private Map<String, ContentValue> services;
    private ApplicationFormPanel applicationFormPanel;
    private Quotation quotation;
    private Button btnCalcul;

    TextField txtTiServiceAmount;
    CheckBox cbSplitWithInstallment;
    CheckBox cbIsTranFee;
    List<QuotationService> quotationServices;

    public FinProductPanel() {
        super();
        setSizeFull();
    }

    @Override
    protected Component createForm() {
        Map<String, Map<Long, ? extends RefDataId>> refData = ERefDataHelper.refData;
        refData.size();
        servicesLayout = new VerticalLayout();
        Label iconInvalidMessage = new Label();
        iconInvalidMessage.setIcon(new ThemeResource("../smt-default/icons/16/danger.png"));
        Label lblInvalidMessage = new Label(I18N.message("quotation.invalid"));
        lblInvalidMessage.setStyleName("error");
        invalidMessageLayout = new HorizontalLayout(iconInvalidMessage, lblInvalidMessage);
        invalidMessageLayout.setVisible(false);

        cbxFinancialProduct = new EntityRefComboBox<>();
        cbxFinancialProduct.setRestrictions(new BaseRestrictions<>(FinProduct.class));
        cbxFinancialProduct.setImmediate(true);
        cbxFinancialProduct.renderer();
        cbxFinancialProduct.setEnabled(false);
        cbxFinancialProduct.setStyleName("blackdisabled");
        cbxFinancialProduct.addValueChangeListener(this);

        cbxTermInMonth = new EntityRefComboBox<>();
        cbxTermInMonth.setRestrictions(new BaseRestrictions<>(Term.class));
        cbxTermInMonth.renderer();

        cbxPeriodicInterestRate = new EntityRefComboBox<>();
        cbxPeriodicInterestRate.setRestrictions(new BaseRestrictions<>(InterestRate.class));
        cbxPeriodicInterestRate.renderer();

        cbxLeaseAmountPercentage = new EntityRefComboBox<>();
        BaseRestrictions restrictions = new BaseRestrictions<>(LeaseAmountPercent.class);
        cbxLeaseAmountPercentage.setRestrictions(restrictions);
        cbxLeaseAmountPercentage.renderer();
        cbxLeaseAmountPercentage.addValueChangeListener(this);


        txtLeaseAmount = ComponentFactory.getTextField(false, 50, 170);
        txtLeaseAmount.setStyleName("blackdisabled");
        txtLeaseAmount.setEnabled(false);


        txtAppraisalEstimateAmount = ComponentFactory.getTextField(false, 50, 170);
        txtAppraisalEstimateAmount.setStyleName("blackdisabled");
        txtAppraisalEstimateAmount.setEnabled(false);

        cbxFrequency = new ERefDataComboBox<EFrequency>(EFrequency.listInMonth());
        cbxFrequency.setSelectedEntity(EFrequency.M);
        cbxFrequency.setEnabled(false);
        cbxFrequency.setStyleName("blackdisabled");

        txtInstallmentAmont = ComponentFactory.getTextField(false, 50, 150);
        txtInstallmentAmont.setStyleName("blackdisabled");
        txtInstallmentAmont.setEnabled(false);

        txtTotalInstallAmount = ComponentFactory.getTextField(false, 50, 150);
        txtTotalInstallAmount.setStyleName("blackdisabled");
        txtTotalInstallAmount.setEnabled(false);

        btnCalcul = new Button("");
        btnCalcul.setIcon(new ThemeResource("icons/32/calculatrice.png"));
        btnCalcul.setStyleName(Reindeer.BUTTON_LINK);
        btnCalcul.addClickListener(this);

        String template = "quotationFinanceCalculation";
        InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
        CustomLayout inputFieldLayout = null;
        try {
            inputFieldLayout = new CustomLayout(layoutFile);
        } catch (IOException e) {
            Notification.show("Could not locate template " + template, e.getMessage(), Type.ERROR_MESSAGE);
        }
        cbxFinancialProduct.addValueChangeListener(this);
        cbxLeaseAmountPercentage.addValueChangeListener(this);
        cbxPeriodicInterestRate.addValueChangeListener(this);
        cbxTermInMonth.addValueChangeListener(this);

        inputFieldLayout.setSizeFull();
        inputFieldLayout.addComponent(new Label(I18N.message("financial.product")), "lblFinancialProduct");
        inputFieldLayout.addComponent(cbxFinancialProduct, "cbxFinancialProduct");
        // inputFieldLayout.addComponent(new Label(I18N.message("groupdetail.estimate.amount")), "lblAppraisalEstimateAmount");
        inputFieldLayout.addComponent(new Label(I18N.message("appraisal.estimatePrice")), "lblAppraisalEstimateAmount");
        inputFieldLayout.addComponent(txtAppraisalEstimateAmount, "txtAppraisalEstimateAmount");
        inputFieldLayout.addComponent(new Label(I18N.message("lease.amount.percentage")), "lblLeaseAmountPecentage");
        inputFieldLayout.addComponent(cbxLeaseAmountPercentage, "txtLeaseAmountPercentage");
        inputFieldLayout.addComponent(new Label(I18N.message("lease.amount")), "lblLeaseAmount");
        inputFieldLayout.addComponent(txtLeaseAmount, "txtLeaseAmount");
        inputFieldLayout.addComponent(new Label(I18N.message("frequency")), "lblFrequency");
        inputFieldLayout.addComponent(cbxFrequency, "cbxFrequency");
        inputFieldLayout.addComponent(new Label(I18N.message("term.month")), "lblTermInMonth");
        inputFieldLayout.addComponent(cbxTermInMonth, "txtTermInMonth");
        inputFieldLayout.addComponent(new Label(I18N.message("periodic.interest.rate")), "lblPeriodicInterestRate");
        inputFieldLayout.addComponent(cbxPeriodicInterestRate, "txtPeriodicInterestRate");
        inputFieldLayout.addComponent(new Label(I18N.message("installment.amount")), "lblInstallmentAmont");
        inputFieldLayout.addComponent(new HorizontalLayout(txtInstallmentAmont, btnCalcul), "txtInstallmentAmont");
        inputFieldLayout.addComponent(invalidMessageLayout, "lblInvalidMessage");

        final Panel financialProductPanel = new Panel(I18N.message("financial.product"));

        VerticalLayout contentLayout = new VerticalLayout(inputFieldLayout, servicesLayout);
        contentLayout.setMargin(true);
        financialProductPanel.setContent(contentLayout);

        setInvalidQuotationFlag(true);
        return financialProductPanel;
    }

    private void displayServicesPanel() {
        FinProduct financialProduct;
        if ((financialProduct = cbxFinancialProduct.getSelectedEntity()) != null) {
            if (financialProduct.getFinancialProductServices() != null) {
                Panel servicePanel = new Panel(I18N.message("services"));
                final GridLayout gridServiceLayout = new GridLayout(10, financialProduct.getFinancialProductServices().size() + 1);
                gridServiceLayout.setSpacing(true);
                gridServiceLayout.setMargin(true);
                gridServiceLayout.addComponent(ComponentFactory.getLabel("", 150), 0, 0);
                gridServiceLayout.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS), 1, 0);
                gridServiceLayout.addComponent(ComponentFactory.getLabel("amount", 150), 2, 0);
                gridServiceLayout.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS), 3, 0);
                gridServiceLayout.addComponent(ComponentFactory.getLabel("split.with.installment", 150), 4, 0);
                gridServiceLayout.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS), 5, 1);
                gridServiceLayout.addComponent(new Label(I18N.message("total.installment.amount")), 6, 1);
                gridServiceLayout.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS), 7, 1);
                gridServiceLayout.addComponent(txtTotalInstallAmount, 8, 1);
                int row = 1;
                services = new HashMap<>();
                for (FinProductService financialService : financialProduct.getFinancialProductServices()) {
                    txtTiServiceAmount = new TextField();
                    cbSplitWithInstallment = new CheckBox();
                    cbSplitWithInstallment.setEnabled(false);
                    cbSplitWithInstallment.setValue(financialService.getService().isSplitWithInstallment());
                    txtTiServiceAmount.setData(financialService.getService());
                    txtTiServiceAmount.setEnabled(financialService.getService().isAllowChangePrice());
                    txtTiServiceAmount.setValue(AmountUtils.format(financialService.getService().getTePrice()));

                    boolean isTranFee = quotation.getQuotationService(EServiceType.TRANSFEE.getCode()) != null ? quotation.getQuotationService(EServiceType.TRANSFEE.getCode()).isTransferFee() : true;
                    cbIsTranFee = new CheckBox();
                    cbIsTranFee.setValue(isTranFee);
                    cbIsTranFee.addValueChangeListener(this);
                    cbIsTranFee.setImmediate(true);

                    if(quotation.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_PUBLIC)) {
                        cbIsTranFee.setEnabled(false);
                    }

                    if (financialService.getService().getServiceType().equals(EServiceType.TRANSFEE)) {
                        if(!isTranFee)
                            txtTiServiceAmount.setValue("");

                        gridServiceLayout.addComponent(cbIsTranFee, 3, row);
                    }
                    gridServiceLayout.addComponent(new Label(financialService.getService().getDescEn()), 0, row);
                    gridServiceLayout.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS), 1, row);

                    gridServiceLayout.addComponent(txtTiServiceAmount, 2, row);
                    gridServiceLayout.addComponent(cbSplitWithInstallment, 4, row);
                    services.put(financialService.getService().getCode(), new ContentValue(txtTiServiceAmount, cbSplitWithInstallment, cbIsTranFee));
                    row++;
                }
                servicePanel.setContent(gridServiceLayout);
                servicesLayout.removeAllComponents();
                servicesLayout.addComponent(servicePanel);
            }
        }
    }

    public void assignValues(Quotation quotation) {
        this.quotation = quotation;
        if (this.quotation != null && this.quotation.getFinancialProduct() != null) {
            cbxFinancialProduct.setSelectedEntity(quotation.getFinancialProduct());
            displayValues(this.quotation);
        } else {
            reset();
        }

        boolean enable = false;
        if (ProfileUtil.isPOS() && (this.quotation.getWkfStatus().equals(QuotationWkfStatus.QUO)
                || this.quotation.getWkfStatus().equals(QuotationWkfStatus.ACS))
                || ProfileUtil.isAdmin()) {
            enable = true;
        }
        setEnabled(enable);
    }

    public void assignValues(Quotation quotation, ApplicationFormPanel applicationFormPanel) {
        this.applicationFormPanel = applicationFormPanel;
        assignValues(quotation);
    }


    private void displayValues(Quotation quotation) {
        Asset asset = quotation.getAsset();
        if (asset != null) {
            FinProduct financialProduct = quotation.getFinancialProduct();
            cbxFinancialProduct.setSelectedEntity(financialProduct);

            Term term = FIN_PROD_SRV.getNumberOfTermByNbTerm(quotation.getTerm());
            cbxTermInMonth.setSelectedEntity(term);

            BaseRestrictions<InterestRate> restrictionInterest = new BaseRestrictions<>(InterestRate.class);
            restrictionInterest.addCriterion(Restrictions.eq(VALUE, quotation.getInterestRate()));
            List<InterestRate> listInterest = ENTITY_SRV.list(restrictionInterest);
            cbxPeriodicInterestRate.setSelectedEntity((listInterest != null && !listInterest.isEmpty()) ? listInterest.get(0) : null);

            BaseRestrictions<LeaseAmountPercent> restrictionAmount = new BaseRestrictions<>(LeaseAmountPercent.class);
            restrictionAmount.addCriterion(Restrictions.eq(VALUE, quotation.getLeaseAmountPercentage()));
            List<LeaseAmountPercent> listLeaseAmount = ENTITY_SRV.list(restrictionAmount);
            cbxLeaseAmountPercentage.setSelectedEntity((listLeaseAmount != null && !listLeaseAmount.isEmpty()) ? listLeaseAmount.get(0) : null);

            txtAppraisalEstimateAmount.setValue(AmountUtils.format(MyNumberUtils.getDouble(quotation.getTiAppraisalEstimateAmount())));
            txtLeaseAmount.setValue(AmountUtils.format(quotation.getTiFinanceAmount()));
            // Lease amount has value
            if(txtLeaseAmount.getValue() != null && !txtLeaseAmount.getValue().equals("0.00")) {
                valueServiceChange();
            }
            cbxFrequency.setSelectedEntity(EFrequency.M);
            txtInstallmentAmont.setValue(AmountUtils.format(quotation.getTiInstallmentAmount()));
            txtTotalInstallAmount.setValue(AmountUtils.format(quotation.getTotalInstallmentAmount()));
            setInvalidQuotationFlag(!quotation.isValid() || (asset != null && asset.isHasChanged()));

            if (invalidQuotation) {
                if (asset.isHasChanged())
                    cbxFinancialProduct.setSelectedEntity(null);
                asset.setHasChanged(false);
            }
        }

        boolean enable = false;
        if (ProfileUtil.isPOS() && (quotation.getWkfStatus().equals(QuotationWkfStatus.QUO)
                || quotation.getWkfStatus().equals(QuotationWkfStatus.AUP)) || ProfileUtil.isAdmin()) {
            enable = true;
        }
        setEnabled(enable);
    }

    /**
     * Get quotation back to application form panel
     * @param quotation
     */
    public Quotation getQuotation(Quotation quotation) {
        if (cbxFinancialProduct.getSelectedEntity() != null && quotation != null) {
            FinProduct financialProduct = quotation.getFinancialProduct();
            quotation.setFinancialProduct(financialProduct);
            quotation.setVatAdvancePaymentAmount(0d);
            if (cbxLeaseAmountPercentage.getSelectedEntity() != null) {
                quotation.setLeaseAmountPercentage(cbxLeaseAmountPercentage.getSelectedEntity().getValue());
            }
            if (cbxPeriodicInterestRate.getSelectedEntity() != null) {
                quotation.setInterestRate(cbxPeriodicInterestRate.getSelectedEntity().getValue());
            }
            if (cbxFinancialProduct.getSelectedEntity() != null) {
                quotation.setNumberOfPrincipalGracePeriods(cbxFinancialProduct.getSelectedEntity().getNumberOfPrincipalGracePeriods());
            }
            if (cbxTermInMonth.getSelectedEntity() != null) {
                quotation.setTerm(cbxTermInMonth.getSelectedEntity().getValue());
            }
            quotation.setTiFinanceAmount(getDouble(txtLeaseAmount, 0d));
            quotation.setTmFinanceAmount(getDouble(txtLeaseAmount, 0d));
            quotation.setFrequency(EFrequency.M);
            quotation.setTiInstallmentAmount(getDouble(txtInstallmentAmont, 0d));
            quotation.setTeInstallmentAmount(getDouble(txtInstallmentAmont, 0d));
            quotation.setTotalInstallmentAmount(getDouble(txtTotalInstallAmount, 0d));
            quotation.setValid(!invalidQuotation);
            quotation.setVatValue(financialProduct.getVat() != null ? financialProduct.getVat().getValue() : 0.0d);
            quotation.setVatFinanceAmount(quotation.getTiInstallmentAmount() * (quotation.getVatValue() / 100));
            quotationServices = quotation.getQuotationServices();
            quotation.setQuotationServices(quotationServices);
            // Service
           /* for (ContentValue contentValue : services.values()) {
                QuotationService quotationService = quotation.getQuotationService(contentValue.finService.getId());
                if (quotationService == null) {
                    quotationService = new QuotationService();
                    quotationService.setCrudAction(CrudAction.CREATE);
                    quotationService.setService(contentValue.finService);
                    quotationServices.add(quotationService);
                } else
                    quotationService.setCrudAction(CrudAction.UPDATE);
                double serviceAmount = calculateService(contentValue);

                double vatAmount = calculateServiceVat(serviceAmount, contentValue);
                quotationService.setQuotation(quotation);
                quotationService.setService(contentValue.finService);
                quotationService.setTiPrice(serviceAmount);
                quotationService.setTePrice(serviceAmount);
                quotationService.setVatPrice(vatAmount);
                quotationService.setVatValue(contentValue.finService.getVat().getValue());
                quotationService.setSplitWithInstallment(contentValue.checkBox.getValue());
                quotationService.setTransferFee(contentValue.checkBoxTranFee.getValue());

            }*/

        }
        return quotation;
    }

    @Override
    public void valueChange(ValueChangeEvent event) {
        if (event.getProperty().equals(cbxFinancialProduct)) {
            if (cbxFinancialProduct.getSelectedEntity() != null) {
                FinProduct selectedProduct = cbxFinancialProduct.getSelectedEntity();
                displayServicesPanel();
                if (ProfileUtil.isCreditOfficer() || ProfileUtil.isAdmin() || ProfileUtil.isProductionOfficer()) {
                    if (FIN_CODE_PUBLIC.equals(selectedProduct.getCode()))
                        applicationFormPanel.addGuarantorPanel();
                    else if (FIN_CODE_EXISTING.equals(selectedProduct.getCode()))
                        applicationFormPanel.removeGuarantorPanel();
                }
                setInvalidQuotationFlag(false);
                //displayValues(this.quotation);

                // lease amount
                BaseRestrictions restrictions = new BaseRestrictions<>(LeaseAmountPercent.class);
                if (cbxFinancialProduct.getSelectedEntity().getCode().equals(FIN_CODE_EXISTING)) {
                    restrictions.addCriterion(Restrictions.between("value", 70.0, 100.0));
                    restrictions.addOrder(Order.desc("value"));
                    cbxLeaseAmountPercentage.setRestrictions(restrictions);
                    cbxLeaseAmountPercentage.renderer();
                } else if (cbxFinancialProduct.getSelectedEntity().getCode().equals(FIN_CODE_PUBLIC)) {

                   if(quotation.getAsset() != null) {
                        Asset asset = quotation.getAsset();
                        AssetModel assetModel = ENTITY_SRV.getById(AssetModel.class, asset.getModel().getId());

                        if(assetModel.getAssetRange().getDescEn().equals(EAppraisalRange.DREAM.getDescEn())) {
                            restrictions.addCriterion(Restrictions.eq("value", 70.0));
                        } else  {
                            restrictions.addCriterion(Restrictions.eq("value", 60.0));
                        }
                        cbxLeaseAmountPercentage.setRestrictions(restrictions);
                        cbxLeaseAmountPercentage.renderer();
                   }
                }
            } else {
                setInvalidQuotationFlag(true);
            }
        } else if (event.getProperty().equals(cbxLeaseAmountPercentage)) {
            if (cbxLeaseAmountPercentage.getSelectedEntity() != null) {
                txtLeaseAmount.setValue(AmountUtils.format(calculateLeaseAmount()));
                valueServiceChange();
                calculateInsurance();
            } else {
                txtLeaseAmount.setValue("0.00");
            }
        } else if (event.getProperty().equals(cbxPeriodicInterestRate)) {
            if (cbxPeriodicInterestRate.getSelectedEntity() != null) {
                txtInstallmentAmont.setValue("");
            }
        } else if (event.getProperty().equals(cbxTermInMonth)) {
            if (cbxTermInMonth.getSelectedEntity() != null) {
                if(txtLeaseAmount.getValue() != null) {
                    calculateInsurance();
                    txtInstallmentAmont.setValue("");
                    //calculationInstallmentAmount();
                }
            }
        } else if (event.getProperty().equals(cbIsTranFee) ) {
            if (!cbIsTranFee.getValue())
                txtTiServiceAmount.setValue("");
            else
                txtTiServiceAmount.setValue(AmountUtils.format(60.0));
            txtInstallmentAmont.setValue("");
            //calculationInstallmentAmount();
        }
    }
    @Override
    public void blur(FieldEvents.BlurEvent event) {
         if(event.getComponent().equals(txtLeaseAmount)) {
            if(txtLeaseAmount.getValue() != null) {
                txtInstallmentAmont.setValue("");
                calculateInsurance();
                valueServiceChange();
            }
        }
    }

    @Override
    public void buttonClick(Button.ClickEvent clickEvent) {
        Button clickButton = (Button) clickEvent.getSource();
        if (btnCalcul == clickButton) {
            calculationInstallmentAmount();
        }
    }

    public List<String> isValid() {
        super.reset();
        checkMandatorySelectField(cbxFinancialProduct, "financial.product");
        checkMandatorySelectField(cbxLeaseAmountPercentage, "lease.amount.percent");
        checkMandatorySelectField(cbxPeriodicInterestRate, "periodic.interest.rate");
        checkMandatorySelectField(cbxFrequency, "frequency");
        checkMandatorySelectField(cbxTermInMonth, "term.month");
        checkMandatoryField(txtInstallmentAmont, "Calculation Installment Amount");
        //checkDoubleField(txtInstallmentAmont, "Please calculate installment amount");
        if(txtInstallmentAmont.getValue().equals("0.00")) {
            errors.add("Please calculate installment amount");
        }
        if (invalidQuotation) {
            errors.add(I18N.message("quotation.invalid"));
        }
        if (quotation.getAsset() != null && quotation.getAsset().getAssetYear() != null) {
            if (!validateAssetYear(Integer.parseInt(quotation.getAsset().getAssetYear().getCode()))) {
                errors.add(I18N.message("asset.less.than.7"));
            }
        }
        return errors;
    }

    public void setInvalidQuotationFlag(boolean invalid) {
        invalidQuotation = invalid;
        invalidMessageLayout.setVisible(invalid);
    }

    public void reset() {
        super.reset();
        cbxFinancialProduct.setSelectedEntity(null);
        txtAppraisalEstimateAmount.setValue(null);
        cbxLeaseAmountPercentage.setSelectedEntity(null);
        txtLeaseAmount.setValue(null);
        cbxPeriodicInterestRate.setSelectedEntity(null);
        cbxFrequency.setSelectedEntity(null);
        cbxTermInMonth.setSelectedEntity(null);
        txtInstallmentAmont.setValue(null);
        servicesLayout.removeAllComponents();
        txtTotalInstallAmount.setValue(null);
        setInvalidQuotationFlag(true);

    }

    private double calculateLeaseAmount() {
        if (cbxLeaseAmountPercentage.getSelectedEntity() != null && txtAppraisalEstimateAmount.getValue() != null){
            return MyMathUtils.roundUp((cbxLeaseAmountPercentage.getSelectedEntity().getValue() / 100) * getDouble(txtAppraisalEstimateAmount, 0d),10);
        }
        return 0.00d;
    }

    private double calculateService(ContentValue contentValue) {
        return MyMathUtils.roundAmountTo(contentValue.finService.getTePrice());
    }

    private double calculateServiceVat(double serviceAmount, ContentValue contentValue) {
        return MyMathUtils.roundAmountTo(serviceAmount * (contentValue.finService.getVat().getValue() / 100));
    }

    private static class ContentValue {
        FinService finService;
        TextField textField;
        CheckBox checkBox;
        CheckBox checkBoxTranFee;

        private ContentValue(TextField textField, CheckBox checkBox, CheckBox isCbTranFee) {
            this.textField = textField;
            this.checkBox = checkBox;
            this.checkBoxTranFee = isCbTranFee;

            if (textField != null && textField.getData() instanceof FinService) {
                finService = (FinService) textField.getData();
            }
        }
    }

    @Override
    protected void displayErrorsPanel() {
        super.displayErrorsPanel();
    }

    public boolean validateAssetYear(int assetYear) {
        int term = cbxTermInMonth.getSelectedEntity() != null ? cbxTermInMonth.getSelectedEntity().getValue() + 1 : 0;
        if (term > 0) {
            int year = Calendar.getInstance().get(Calendar.YEAR);
            int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
            int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
            int assetMonth = 1;
            int assetDay = 1;
            int newMonth = (month - assetMonth) + term;
            int newYear = year - assetYear;
            int newDay = day - assetDay;
            if (newMonth > 12 || newMonth == 12) {
                while (newMonth > 12 || newMonth == 12) {
                    newMonth = newMonth - 12;
                    newYear = newYear + 1;
                }
            }
            if (newYear < 7) {
                return true;
            }
            if (newYear == 7 && newMonth == 0 && newDay == 0) {
                return true;
            }
        }
        return false;
    }

    public void setApplicationFormPanel(ApplicationFormPanel applicationFormPanel) {
        this.applicationFormPanel = applicationFormPanel;
    }

    private void calculationInstallmentAmount() {
        if(applicationFormPanel != null){
            applicationFormPanel.clearErrorMessage();
        }
        Quotation tmpQuotation = getQuotation(quotation);
        if (calculationValidation().isEmpty()) {
            if (services != null) {
                quotationServices = this.quotation.getQuotationServices();
                // Service
                for (ContentValue contentValue : services.values()) {
                    QuotationService quotationService = quotation.getQuotationService(contentValue.finService.getId());
                    if (quotationService == null) {
                        quotationService = new QuotationService();
                        quotationService.setCrudAction(CrudAction.CREATE);
                        quotationService.setService(contentValue.finService);
                        quotationServices.add(quotationService);
                    } else
                        quotationService.setCrudAction(CrudAction.UPDATE);
                    if (txtTiServiceAmount.getData() != null) {
                        if (contentValue.finService.getServiceType().equals(EServiceType.INSFEE)) {
                            contentValue.finService.setTePrice(calculateInsurance());
                        } else if (contentValue.finService.getServiceType().equals(EServiceType.SRVFEE)) {
                            contentValue.finService.setTePrice(MyNumberUtils.getDouble(contentValue.textField.getValue(), 0d));
                        } else if (contentValue.finService.getServiceType().equals(EServiceType.TRANSFEE)) {
                            contentValue.finService.setTePrice(MyNumberUtils.getDouble(contentValue.textField.getValue(), 0d));
                        } else if (contentValue.finService.getServiceType().equals(EServiceType.SRVFEEEXT)) {
                            contentValue.finService.setTePrice(MyNumberUtils.getDouble(contentValue.textField.getValue(), 0d));
                        } else if (contentValue.finService.getServiceType().equals(EServiceType.INS_SERV_VAT)) {
                            contentValue.finService.setTePrice(0d);
                        }

                        double serviceAmount;
                        double vatAmount;
                        serviceAmount = calculateService(contentValue);
                        vatAmount = calculateServiceVat(serviceAmount, contentValue);
                        quotationService.setQuotation(this.quotation);
                        quotationService.setService(contentValue.finService);
                        quotationService.setTiPrice(serviceAmount);
                        quotationService.setTePrice(serviceAmount);
                        quotationService.setVatPrice(vatAmount);
                        quotationService.setVatValue(contentValue.finService.getVat().getValue());
                        quotationService.setSplitWithInstallment(contentValue.checkBox.getValue());
                        quotationService.setTransferFee(contentValue.checkBoxTranFee.getValue());
                    }
                }
                tmpQuotation.setQuotationServices(quotationServices);
            }

            if(quotation.getFirstDueDate() == null)
                tmpQuotation.setFirstDueDate(DateUtils.todayDate());

            Map<String, Double> mapCal = FIN_PROD_SRV.calculationInstallmentAmount(tmpQuotation);
            txtTotalInstallAmount.setValue(AmountUtils.format(mapCal.get(TOTAL_INT_AMOUNT)));
            txtInstallmentAmont.setValue(AmountUtils.format(mapCal.get(PRINCILPLE_AMOUNT)));

            // Set IRR in Quotation to put on Report
           quotation.setIrrRate(100 * Rate.calculateIRR(LoanUtils.getNumberOfPeriods(quotation.getTerm(),
                    quotation.getFrequency()),
                    NumberUtils.toDouble(txtInstallmentAmont.getValue()),
                    quotation.getTiFinanceAmount()));
            QUO_SRV.saveOrUpdate(quotation);
        } else {
            if(applicationFormPanel != null) {
                applicationFormPanel.displayErrors();
            }
        }
    }


    public List<String> calculationValidation() {
        super.reset();
        checkMandatorySelectField(cbxFinancialProduct, "financial.product");
        checkMandatorySelectField(cbxLeaseAmountPercentage, "lease.amount.percent");
        checkMandatorySelectField(cbxPeriodicInterestRate, "periodic.interest.rate");
        checkMandatorySelectField(cbxFrequency, "frequency");
        checkMandatorySelectField(cbxTermInMonth, "term.month");
        checkDoubleField(txtInstallmentAmont, "Calculation Installment Amount");
        if(applicationFormPanel != null){
            applicationFormPanel.setError(errors);
        }
        return errors;
    }

    private Double calculateInsurance() {
        Integer term = cbxTermInMonth.getSelectedEntity() != null ? cbxTermInMonth.getSelectedEntity().getValue() : 12;
        Double leaseAmountValue = txtLeaseAmount.getValue() != null ? MyNumberUtils.getDouble(txtLeaseAmount.getValue(), 0d) : 0.0d;
        Double insuranceFee = (FIN_PROD_SRV.getInsuranceFee(leaseAmountValue) * term) / 12;
        return insuranceFee;
    }

    /**
     * Value Change
     */
    void valueServiceChange() {
        Double leaseAmountValue;
        if(services.values() != null){
            for (ContentValue contentValue : services.values()){
                if(txtTiServiceAmount.getData() != null){
                    if (contentValue.finService.getServiceType().equals(EServiceType.INSFEE)) {

                        leaseAmountValue = MyNumberUtils.getDouble(txtLeaseAmount.getValue(), 0d);
                        Double totalInsuranceFee = FIN_PROD_SRV.getInsuranceFee(leaseAmountValue);
                        contentValue.textField.setValue(MyNumberUtils.formatDoubleToString(totalInsuranceFee));
                    } else if (contentValue.finService.getServiceType().equals(EServiceType.SRVFEE)) {

                        contentValue.textField.setValue(MyNumberUtils.formatDoubleToString(contentValue.finService.getTePrice()));
                    } else if (contentValue.finService.getServiceType().equals(EServiceType.TRANSFEE)) {
                        if(!contentValue.checkBoxTranFee.getValue())
                            contentValue.textField.setValue("");
                        else
                            contentValue.textField.setValue(MyNumberUtils.formatDoubleToString(contentValue.finService.getTePrice()));
                    } else if (contentValue.finService.getDescEn().equals(VAT_INSUR)) {

                        leaseAmountValue = MyNumberUtils.getDouble(txtLeaseAmount.getValue(), 0d);
                        Double totalVatIns = FIN_PROD_SRV.getVatInsurance(leaseAmountValue);
                        contentValue.textField.setValue(MyNumberUtils.formatDoubleToString(totalVatIns));
                    } else if (contentValue.finService.getDescEn().equals(VAT_SERVI)) {

                        Double totalVatServ = FIN_PROD_SRV.getVatService(VAT_SERVI);
                        contentValue.textField.setValue(MyNumberUtils.formatDoubleToString(totalVatServ));
                    } else if (contentValue.finService.getDescEn().equals(VAT_SERVI_EXT)) {

                        Double totalVatServ = FIN_PROD_SRV.getVatService(VAT_SERVI_EXT);
                        contentValue.textField.setValue(MyNumberUtils.formatDoubleToString(totalVatServ));
                    }
                }
            }
        }
    }
}
