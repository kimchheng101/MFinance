package com.soma.mfinance.core.quotation.panel;

import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;
import org.seuksa.frmk.i18n.I18N;

import java.util.List;

/**
 * Created by Dang Dim
 * Date     : 08-Sep-17, 10:38 AM
 * Email    : d.dim@gl-f.com
 */
public class PolicyRulePanel extends AbstractTabPanel implements FMEntityField{

    /*private AppliedQuotationPolicyVO appliedPolicies = null;
    private SimplePagedTable<QuotationPolicy> table;
    List<ColumnDefinition> columnDefinitions;*/

    @Override
    protected Component createForm() {
        VerticalLayout rootPanel = new VerticalLayout();

        /*columnDefinitions = createColumnDefinition();
        table = new SimplePagedTable<QuotationPolicy>(columnDefinitions);
        table.setCaption(I18N.message("quotation.policy.table"));
        table.setSortEnabled(false);
        table.setSelectable(false);
        table.setColumnCollapsingAllowed(false);
        table.setColumnReorderingAllowed(false);
        rootPanel.addComponent(table);
        rootPanel.setSizeFull();*/

        return rootPanel;
    }

}
