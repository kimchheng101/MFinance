package com.soma.mfinance.core.contract.service;

import com.soma.mfinance.core.quotation.SequenceGenerator;

/**
 * Reference number generator
 * @author kimsuor.seang
 */
public class LockSplitSequenceImpl implements SequenceGenerator {
	
	private Long sequence;
		
	/**
	 * 
	 * @param dealer
	 * @param type
	 * @param sequence
	 */
	public LockSplitSequenceImpl(Long sequence) {
		this.sequence = sequence;
	}
	
	/**
	 * @see com.soma.mfinance.core.quotation.SequenceGenerator#generate()
	 */
	@Override
	public String generate() {
		String sequenceNumber = "0000000000" + sequence;
		return sequenceNumber.substring(sequenceNumber.length() - 10);
	}
}
