package com.soma.mfinance.core.quotation.panel;

import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.application.panel.ApplicationFormPanel;
import com.soma.mfinance.core.application.panel.ApplicationPanel;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.financialproduct.FinancialProductEntityField;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.mfinance.core.shared.quotation.QuotationProfileUtils;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.SelectedTabChangeListener;
import com.vaadin.ui.VerticalLayout;
import org.seuksa.frmk.i18n.I18N;


/**
 * Underwriter panel
 *
 * @author kimsuor.seang
 */
public class UnderwriterPanel extends AbstractTabPanel implements QuotationEntityField {

    private static final long serialVersionUID = -5682940523541469251L;

//	private ContractFormPanel quotationFormPanel;

    private VerticalLayout contentLayout;
    private TabSheet tabSheetUw;
    private ScoringPanel scoringPanel;
    private CreditBureauPanel applicantCreditBureauPanel;
    private CreditBureauPanel guarantorCreditBureauPanel;
    private VerticalLayout navigationPanel;

    private Quotation quotation;
    private ApplicationPanel applicationPanel;
    private ApplicationFormPanel applicationFormPanel;

    public UnderwriterPanel(ApplicationFormPanel applicationFormPanel) {
        this.applicationFormPanel = applicationFormPanel;

    }

    public ApplicationFormPanel getApplicationFormPanel() {
        return applicationFormPanel;
    }

    /**
     * @return
     */

    @Override
    protected com.vaadin.ui.Component createForm() {
        contentLayout = new VerticalLayout();
        tabSheetUw = new TabSheet();
        scoringPanel = new ScoringPanel(this);
        applicantCreditBureauPanel = new CreditBureauPanel(EApplicantType.C);
        guarantorCreditBureauPanel = new CreditBureauPanel(EApplicantType.G);
        if(quotation != null){
               if(quotation.getFinancialProduct() != null){
                   if (this.quotation.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_EXISTING)) {
                       tabSheetUw.addTab(applicantCreditBureauPanel, I18N.message("applicant.credit"));
                   } else {
                       tabSheetUw.addTab(applicantCreditBureauPanel, I18N.message("applicant.credit"));
                       tabSheetUw.addTab(guarantorCreditBureauPanel, I18N.message("guarantor.credit"));
                   }
               }
        }
        tabSheetUw.addTab(scoringPanel, I18N.message("underwriter"));
        contentLayout.addComponent(tabSheetUw);

        tabSheetUw.addSelectedTabChangeListener(new SelectedTabChangeListener() {
            private static final long serialVersionUID = -382492569665690482L;

            @Override
            public void selectedTabChange(SelectedTabChangeEvent event) {
                updateNavigationPanel(navigationPanel, null);
            }
        });

        return contentLayout;
    }

    /**
     * Set quotation
     *
     * @param quotation
     */
    public void assignValues(Quotation quotation) {
        this.quotation = quotation;
        super.removeAllComponents();
        super.init();
        scoringPanel.assignValues(quotation);
        applicantCreditBureauPanel.assignValues(quotation, EApplicantType.C);
        guarantorCreditBureauPanel.assignValues(quotation, EApplicantType.G);
    }

    /**
     * Reset panel
     */
    public void reset() {
    }

    @Override
    public void updateNavigationPanel(VerticalLayout navigationPanel, NavigationPanel defaultNavigationPanel) {
        this.navigationPanel = navigationPanel;
        navigationPanel.removeAllComponents();
        Component selectedTab = tabSheetUw.getSelectedTab();
        if (ProfileUtil.isUnderwriter(ProfileUtil.getCurrentUser()) || ProfileUtil.isUnderwriterSupervisor2()) {
            if (selectedTab == applicantCreditBureauPanel) {
                if (QuotationProfileUtils.isNavigationUnderwriterAvailable(quotation)) {
                    navigationPanel.addComponent(applicantCreditBureauPanel.getNavigationPanel());
                }
                applicantCreditBureauPanel.assignValues(quotation, EApplicantType.C);
            } else if (selectedTab == guarantorCreditBureauPanel) {
                if (QuotationProfileUtils.isNavigationUnderwriterAvailable(quotation)) {
                    navigationPanel.addComponent(guarantorCreditBureauPanel.getNavigationPanel());
                }
                guarantorCreditBureauPanel.assignValues(quotation, EApplicantType.G);
            } else if (selectedTab == scoringPanel) {
                if (QuotationProfileUtils.isNavigationUnderwriterAvailable(quotation)) {
                    navigationPanel.addComponent(scoringPanel.getNavigationPanel());
                }
                scoringPanel.assignValues(quotation);
            }
        }
        if (ProfileUtil.isUnderwriterSupervisor(ProfileUtil.getCurrentUser()) || ProfileUtil.isUnderwriterSupervisor2()) {
            if (selectedTab == applicantCreditBureauPanel) {
                if (QuotationProfileUtils.isNavigationUnderWriterSupervisorAvailable(quotation)) {
                    navigationPanel.addComponent(applicantCreditBureauPanel.getNavigationPanel());
                }
                applicantCreditBureauPanel.assignValues(quotation, EApplicantType.C);
            } else if (selectedTab == guarantorCreditBureauPanel) {
                if (QuotationProfileUtils.isNavigationUnderWriterSupervisorAvailable(quotation)) {
                    navigationPanel.addComponent(guarantorCreditBureauPanel.getNavigationPanel());
                }
                guarantorCreditBureauPanel.assignValues(quotation, EApplicantType.G);
            } else if (selectedTab == scoringPanel) {
                if (QuotationProfileUtils.isNavigationUnderWriterSupervisorAvailable(quotation)) {
                    navigationPanel.addComponent(scoringPanel.getNavigationPanel());
                }
                scoringPanel.assignValues(quotation);
            }
        }

    }

    public void setApplicationPanel(ApplicationPanel applicationPanel) {
        this.applicationPanel = applicationPanel;
    }

}
