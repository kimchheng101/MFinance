/**
 * Auto-generated on Fri Jul 10 16:12:17 ICT 2015
 */
package com.soma.mfinance.core.quotation.model;

import org.seuksa.frmk.model.entity.MEntityA;

/**
 * Meta data of com.soma.mfinance.core.quotation.model.QuotationApplicant
 * @author 
 */
public interface MQuotationApplicant extends MEntityA {

	public final static String APPLICANT = "applicant";
	public final static String QUOTATION = "quotation";
	public final static String APPLICANTTYPE = "applicantType";
	public final static String RELATIONSHIP = "relationship";
	public final static String SAMEAPPLICANTADDRESS = "sameApplicantAddress";
	public final static String STATUSRECORD = "statusRecord";
	public final static String CRUDACTION = "crudAction";

}
