package com.soma.mfinance.core.panel.applicant;

import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.ersys.core.hr.model.eref.ERelationship;
import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;

/**
 * Created by th.seng on 10/20/2017.
 */

@Entity
@Table(name = "td_family_expense")
public class FamilyAndExpense extends EntityA {

    private static final long serialVersionUID = 7265133298692199414L;
    private Individual individual;

    private ERelationship relationship1;
    private ERelationship relationship2;
    private ERelationship relationship3;
    private ERelationship relationship4;
    private ERelationship relationship5;
    private ERelationship relationship6;

    private Double annualIncome1;
    private Double annualIncome2;
    private Double annualIncome3;
    private Double annualIncome4;
    private Double annualIncome5;
    private Double annualIncome6;
    private String position1;
    private String position2;
    private String position3;
    private String position4;
    private String position5;
    private String position6;
    private String contactNumber1;
    private String contactNumber2;
    private String contactNumber3;
    private String contactNumber4;
    private String contactNumber5;
    private String contactNumber6;
    private Integer lengthWorkInYear1;
    private Integer lengthWorkInYear2;
    private Integer lengthWorkInYear3;
    private Integer lengthWorkInYear4;
    private Integer lengthWorkInYear5;
    private Integer lengthWorkInYear6;
    private Integer lengthWorkInMonth1;
    private Integer lengthWorkInMonth2;
    private Integer lengthWorkInMonth3;
    private Integer lengthWorkInMonth4;
    private Integer lengthWorkInMonth5;
    private Integer lengthWorkInMonth6;

    private Integer numberOfadultIncludingApplicant;
    private Integer numberOfChild;
    private Double annualPersonalExpense;
    private Double annualFamilyExpense;

    private String relationshipName1;
    private String relationshipName2;
    private String relationshipName3;
    private String relationshipName4;
    private String relationshipName5;
    private String relationshipName6;


    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "famex_id", unique = true, nullable = false)
    public Long getId() {
        // TODO Auto-generated method stub
        return id;
    }

    @Column(name = "rel_id_1", nullable = true)
    @Convert(converter = ERelationship.class)
    public ERelationship getRelationship1() {
        return relationship1;
    }

    public void setRelationship1(ERelationship relationship1) {
        this.relationship1 = relationship1;
    }

    @Column(name = "rel_id_2", nullable = true)
    @Convert(converter = ERelationship.class)
    public ERelationship getRelationship2() {
        return relationship2;
    }

    public void setRelationship2(ERelationship relationship2) {
        this.relationship2 = relationship2;
    }

    @Column(name = "rel_id_3", nullable = true)
    @Convert(converter = ERelationship.class)
    public ERelationship getRelationship3() {
        return relationship3;
    }

    public void setRelationship3(ERelationship relationship3) {
        this.relationship3 = relationship3;
    }

    @Column(name = "rel_id_4", nullable = true)
    @Convert(converter = ERelationship.class)
    public ERelationship getRelationship4() {
        return relationship4;
    }

    public void setRelationship4(ERelationship relationship4) {
        this.relationship4 = relationship4;
    }

    @Column(name = "rel_id_5", nullable = true)
    @Convert(converter = ERelationship.class)
    public ERelationship getRelationship5() {
        return relationship5;
    }

    public void setRelationship5(ERelationship relationship5) {
        this.relationship5 = relationship5;
    }

    @Column(name = "rel_id_6", nullable = true)
    @Convert(converter = ERelationship.class)
    public ERelationship getRelationship6() {
        return relationship6;
    }

    public void setRelationship6(ERelationship relationship6) {
        this.relationship6 = relationship6;
    }

    @Column(name = "famex_annual_income_member_1", nullable = true)
    public Double getAnnualIncome1() {
        return annualIncome1;
    }

    public void setAnnualIncome1(Double annualIncome1) {
        this.annualIncome1 = annualIncome1;
    }

    @Column(name = "famex_annual_income_member_2", nullable = true)
    public Double getAnnualIncome2() {
        return annualIncome2;
    }

    public void setAnnualIncome2(Double annualIncome2) {
        this.annualIncome2 = annualIncome2;
    }

    @Column(name = "famex_annual_income_member_3", nullable = true)
    public Double getAnnualIncome3() {
        return annualIncome3;
    }

    public void setAnnualIncome3(Double annualIncome3) {
        this.annualIncome3 = annualIncome3;
    }

    @Column(name = "famex_annual_income_member_4", nullable = true)
    public Double getAnnualIncome4() {
        return annualIncome4;
    }

    public void setAnnualIncome4(Double annualIncome4) {
        this.annualIncome4 = annualIncome4;
    }

    @Column(name = "famex_annual_income_member_5", nullable = true)
    public Double getAnnualIncome5() {
        return annualIncome5;
    }

    public void setAnnualIncome5(Double annualIncome5) {
        this.annualIncome5 = annualIncome5;
    }

    @Column(name = "famex_annual_income_member_6", nullable = true)
    public Double getAnnualIncome6() {
        return annualIncome6;
    }

    public void setAnnualIncome6(Double annualIncome6) {
        this.annualIncome6 = annualIncome6;
    }

    @Column(name = "famex_position_1", nullable = true)
    public String getPosition1() {
        return position1;
    }

    public void setPosition1(String position1) {
        this.position1 = position1;
    }

    @Column(name = "famex_position_2", nullable = true)
    public String getPosition2() {
        return position2;
    }

    public void setPosition2(String position2) {
        this.position2 = position2;
    }

    @Column(name = "famex_position_3", nullable = true)
    public String getPosition3() {
        return position3;
    }

    public void setPosition3(String position3) {
        this.position3 = position3;
    }

    @Column(name = "famex_position_4", nullable = true)
    public String getPosition4() {
        return position4;
    }

    public void setPosition4(String position4) {
        this.position4 = position4;
    }

    @Column(name = "famex_position_5", nullable = true)
    public String getPosition5() {
        return position5;
    }

    public void setPosition5(String position5) {
        this.position5 = position5;
    }

    @Column(name = "famex_position_6", nullable = true)
    public String getPosition6() {
        return position6;
    }

    public void setPosition6(String position6) {
        this.position6 = position6;
    }

    @Column(name = "famex_contact_num_1", nullable = true)
    public String getContactNumber1() {
        return contactNumber1;
    }

    public void setContactNumber1(String contactNumber1) {
        this.contactNumber1 = contactNumber1;
    }

    @Column(name = "famex_contact_num_2", nullable = true)
    public String getContactNumber2() {
        return contactNumber2;
    }

    public void setContactNumber2(String contactNumber2) {
        this.contactNumber2 = contactNumber2;
    }

    @Column(name = "famex_contact_num_3", nullable = true)
    public String getContactNumber3() {
        return contactNumber3;
    }

    public void setContactNumber3(String contactNumber3) {
        this.contactNumber3 = contactNumber3;
    }

    @Column(name = "famex_contact_num_4", nullable = true)
    public String getContactNumber4() {
        return contactNumber4;
    }

    public void setContactNumber4(String contactNumber4) {
        this.contactNumber4 = contactNumber4;
    }

    @Column(name = "famex_contact_num_5", nullable = true)
    public String getContactNumber5() {
        return contactNumber5;
    }

    public void setContactNumber5(String contactNumber5) {
        this.contactNumber5 = contactNumber5;
    }

    @Column(name = "famex_contact_num_6", nullable = true)
    public String getContactNumber6() {
        return contactNumber6;
    }

    public void setContactNumber6(String contactNumber6) {
        this.contactNumber6 = contactNumber6;
    }

    @Column(name = "famex_length_working_year_1", nullable = true)
    public Integer getLengthWorkInYear1() {
        return lengthWorkInYear1;
    }

    public void setLengthWorkInYear1(Integer lengthWorkInYear1) {
        this.lengthWorkInYear1 = lengthWorkInYear1;
    }

    @Column(name = "famex_length_working_year_2", nullable = true)
    public Integer getLengthWorkInYear2() {
        return lengthWorkInYear2;
    }

    public void setLengthWorkInYear2(Integer lengthWorkInYear2) {
        this.lengthWorkInYear2 = lengthWorkInYear2;
    }

    @Column(name = "famex_length_working_year_3", nullable = true)
    public Integer getLengthWorkInYear3() {
        return lengthWorkInYear3;
    }

    public void setLengthWorkInYear3(Integer lengthWorkInYear3) {
        this.lengthWorkInYear3 = lengthWorkInYear3;
    }

    @Column(name = "famex_length_working_year_4", nullable = true)
    public Integer getLengthWorkInYear4() {
        return lengthWorkInYear4;
    }

    public void setLengthWorkInYear4(Integer lengthWorkInYear4) {
        this.lengthWorkInYear4 = lengthWorkInYear4;
    }

    @Column(name = "famex_length_working_year_5", nullable = true)
    public Integer getLengthWorkInYear5() {
        return lengthWorkInYear5;
    }

    public void setLengthWorkInYear5(Integer lengthWorkInYear5) {
        this.lengthWorkInYear5 = lengthWorkInYear5;
    }

    @Column(name = "famex_length_working_year_6", nullable = true)
    public Integer getLengthWorkInYear6() {
        return lengthWorkInYear6;
    }

    public void setLengthWorkInYear6(Integer lengthWorkInYear6) {
        this.lengthWorkInYear6 = lengthWorkInYear6;
    }

    @Column(name = "famex_length_working_month_1", nullable = true)
    public Integer getLengthWorkInMonth1() {
        return lengthWorkInMonth1;
    }

    public void setLengthWorkInMonth1(Integer lengthWorkInMonth1) {
        this.lengthWorkInMonth1 = lengthWorkInMonth1;
    }

    @Column(name = "famex_length_working_month_2", nullable = true)
    public Integer getLengthWorkInMonth2() {
        return lengthWorkInMonth2;
    }

    public void setLengthWorkInMonth2(Integer lengthWorkInMonth2) {
        this.lengthWorkInMonth2 = lengthWorkInMonth2;
    }

    @Column(name = "famex_length_working_month_3", nullable = true)
    public Integer getLengthWorkInMonth3() {
        return lengthWorkInMonth3;
    }

    public void setLengthWorkInMonth3(Integer lengthWorkInMonth3) {
        this.lengthWorkInMonth3 = lengthWorkInMonth3;
    }

    @Column(name = "famex_length_working_month_4", nullable = true)
    public Integer getLengthWorkInMonth4() {
        return lengthWorkInMonth4;
    }

    public void setLengthWorkInMonth4(Integer lengthWorkInMonth4) {
        this.lengthWorkInMonth4 = lengthWorkInMonth4;
    }

    @Column(name = "famex_length_working_month_5", nullable = true)
    public Integer getLengthWorkInMonth5() {
        return lengthWorkInMonth5;
    }

    public void setLengthWorkInMonth5(Integer lengthWorkInMonth5) {
        this.lengthWorkInMonth5 = lengthWorkInMonth5;
    }

    @Column(name = "famex_length_working_month_6", nullable = true)
    public Integer getLengthWorkInMonth6() {
        return lengthWorkInMonth6;
    }

    public void setLengthWorkInMonth6(Integer lengthWorkInMonth6) {
        this.lengthWorkInMonth6 = lengthWorkInMonth6;
    }

    @Column(name = "famex_number_adult_including_applicant", nullable = true)
    public Integer getNumberOfadultIncludingApplicant() {
        return numberOfadultIncludingApplicant;
    }

    public void setNumberOfadultIncludingApplicant(
            Integer numberOfadultIncludingApplicant) {
        this.numberOfadultIncludingApplicant = numberOfadultIncludingApplicant;
    }

    @Column(name = "famex_number_child", nullable = true)
    public Integer getNumberOfChild() {
        return numberOfChild;
    }

    public void setNumberOfChild(Integer numberOfChildren) {
        this.numberOfChild = numberOfChildren;
    }

    @Column(name = "famex_annual_personal_expense", nullable = true)
    public Double getAnnualPersonalExpense() {
        return annualPersonalExpense;
    }

    public void setAnnualPersonalExpense(Double annualPersonalExpense) {
        this.annualPersonalExpense = annualPersonalExpense;
    }

    @Column(name = "famex_annual_family_expense", nullable = true)
    public Double getAnnualFamilyExpense() {
        return annualFamilyExpense;
    }

    public void setAnnualFamilyExpense(Double annualFamilyExpense) {
        this.annualFamilyExpense = annualFamilyExpense;
    }


    @Column(name = "relationship_name1", nullable = true)
    public String getRelationshipName1() {
        return relationshipName1;
    }

    public void setRelationshipName1(String relationshipName1) {
        this.relationshipName1 = relationshipName1;
    }

    @Column(name = "relationship_name2", nullable = true)
    public String getRelationshipName2() {
        return relationshipName2;
    }

    public void setRelationshipName2(String relationshipName2) {
        this.relationshipName2 = relationshipName2;
    }

    @Column(name = "relationship_name3", nullable = true)
    public String getRelationshipName3() {
        return relationshipName3;
    }

    public void setRelationshipName3(String relationshipName3) {
        this.relationshipName3 = relationshipName3;
    }

    @Column(name = "relationship_name4", nullable = true)
    public String getRelationshipName4() {
        return relationshipName4;
    }

    public void setRelationshipName4(String relationshipName4) {
        this.relationshipName4 = relationshipName4;
    }

    @Column(name = "relationship_name5", nullable = true)
    public String getRelationshipName5() {
        return relationshipName5;
    }

    public void setRelationshipName5(String relationshipName5) {
        this.relationshipName5 = relationshipName5;
    }

    @Column(name = "relationship_name6", nullable = true)
    public String getRelationshipName6() {
        return relationshipName6;
    }

    public void setRelationshipName6(String relationshipName6) {
        this.relationshipName6 = relationshipName6;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ind_id")
    public Individual getIndividual() {
        return individual;
    }

    public void setIndividual(Individual individual) {
        this.individual = individual;
    }
}
