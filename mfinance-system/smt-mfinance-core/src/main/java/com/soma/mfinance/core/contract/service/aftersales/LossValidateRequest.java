package com.soma.mfinance.core.contract.service.aftersales;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.dealer.model.Dealer;
import org.seuksa.frmk.tools.amount.Amount;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class LossValidateRequest implements Serializable {

	private static final long serialVersionUID = 8136176355032037369L;

	private Long cotraId;
	
	private List<Cashflow> cashflows;
	
	private Amount totalPrincipal;
	private Amount totalInterest;
	private Amount totalOther;
	private Amount totalPenalty;
	private Amount insuranceFee;
	private Amount servicingFee;
	private Amount transferFee;
	private Dealer dealer;
	private Date eventDate;
	
	private EWkfStatus contractStatus;

	public Long getCotraId() {
		return cotraId;
	}
	
	public void setCotraId(Long cotraId) {
		this.cotraId = cotraId;
	}

	public List<Cashflow> getCashflows() {
		return cashflows;
	}

	public void setCashflows(List<Cashflow> cashflows) {
		this.cashflows = cashflows;
	}

	public Amount getTotalPrincipal() {
		return totalPrincipal;
	}

	public void setTotalPrincipal(Amount totalPrincipal) {
		this.totalPrincipal = totalPrincipal;
	}

	public Amount getTotalInterest() {
		return totalInterest;
	}

	public void setTotalInterest(Amount totalInterest) {
		this.totalInterest = totalInterest;
	}

	public Amount getTotalOther() {
		return totalOther;
	}

	public void setTotalOther(Amount totalOther) {
		this.totalOther = totalOther;
	}

	public Amount getTotalPenalty() {
		return totalPenalty;
	}

	public void setTotalPenalty(Amount totalPenalty) {
		this.totalPenalty = totalPenalty;
	}

	public Amount getInsuranceFee() {
		return insuranceFee;
	}

	public void setInsuranceFee(Amount insuranceFee) {
		this.insuranceFee = insuranceFee;
	}

	public Amount getServicingFee() {
		return servicingFee;
	}

	public void setServicingFee(Amount servicingFee) {
		this.servicingFee = servicingFee;
	}

	public Amount getTransferFee() {
		return transferFee;
	}

	public void setTransferFee(Amount transferFee) {
		this.transferFee = transferFee;
	}

	public Dealer getDealer() {
		return dealer;
	}

	public void setDealer(Dealer dealer) {
		this.dealer = dealer;
	}

	public EWkfStatus getWkfStatus() {
		return contractStatus;
	}

	public void setWkfStatus(EWkfStatus contractStatus) {
		this.contractStatus = contractStatus;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public EWkfStatus getContractStatus() {
		return contractStatus;
	}

	public void setContractStatus(EWkfStatus contractStatus) {
		this.contractStatus = contractStatus;
	}
}
