package com.soma.mfinance.core.contract.model.schedule.service;

import com.soma.mfinance.core.contract.model.MContract;
import com.soma.mfinance.core.contract.model.schedule.ContractActivateSchedule;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.contract.ContractEntityField;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.frmk.helper.SeuksaServicesHelper;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by kimsuor.seang
 * USER   c.chhai
 * DATE on 6/8/2017.
 * TIME at 10:09 AM
 * EMIAL   c.chhai@gl-f.com
 */

@Service("ContractScheduleService")
@Transactional
public class ContractScheduleServiceImp extends BaseEntityServiceImpl implements MContract ,ContractScheduleService, ContractEntityField, QuotationEntityField , SeuksaServicesHelper {

	public static ContractScheduleServiceImp createInstance() {
		ContractScheduleServiceImp contractScheduleMigrationServiceImp = new ContractScheduleServiceImp();
		return contractScheduleMigrationServiceImp;
	}

	@Override
	public List<Quotation> quotationListSelected(Set<Long> selectedItemIds) {
		List<Quotation> quotationList = new ArrayList<>();
		for (Long quotationId : selectedItemIds) {
			Quotation quotation = ENTITY_SRV.getById(Quotation.class, quotationId);
			quotationList.add(quotation);
		}
		return quotationList;
	}

	@Override
	public void executedContractActivateSchedule(Quotation quotation) {
		BaseRestrictions<ContractActivateSchedule> contractActivateScheduleBaseRestrictions = new BaseRestrictions<>(ContractActivateSchedule.class);
		contractActivateScheduleBaseRestrictions.addCriterion(Restrictions.eq("quotation.id", quotation.getId()));
		List<ContractActivateSchedule> contractActivateSchedules = ENTITY_SRV.list(contractActivateScheduleBaseRestrictions);
		if(contractActivateScheduleBaseRestrictions != null){
			for (ContractActivateSchedule  contractActivateSchedule : contractActivateSchedules){
				contractActivateSchedule.setExecuted(true);
				ENTITY_SRV.saveOrUpdate(contractActivateSchedule);
			}
		}
	}

	@Override
	public Boolean isExecutedContractActivateSchedule(Quotation quotation) {
		BaseRestrictions<ContractActivateSchedule> contractActivateScheduleBaseRestrictions = new BaseRestrictions<>(ContractActivateSchedule.class);
		contractActivateScheduleBaseRestrictions.addCriterion(Restrictions.eq("quotation.id", quotation.getId()));
		List<ContractActivateSchedule> contractActivateSchedules = ENTITY_SRV.list(contractActivateScheduleBaseRestrictions);
		if(contractActivateScheduleBaseRestrictions != null){
			for (ContractActivateSchedule  contractActivateSchedule : contractActivateSchedules){
				if(contractActivateSchedule.getExecuted()){
					return true;
				}
			}
		}
		return false;
	}


	@Override
	public boolean checkQuotationHasInScheduleTable(Quotation quotation) {
		BaseRestrictions<ContractActivateSchedule> contractActivateScheduleBaseRestrictions = new BaseRestrictions<>(ContractActivateSchedule.class);
		contractActivateScheduleBaseRestrictions.addCriterion(Restrictions.eq("quotation.id", quotation.getId()));
		List<ContractActivateSchedule> contractActivateSchedulesList = ENTITY_SRV.list(contractActivateScheduleBaseRestrictions);
		if(contractActivateSchedulesList.isEmpty()){
			return true;
		}
		return  false;
	}

	@Override
	public Quotation getQuotationFromTableSchedule(Long id) {
		 Quotation quotation = ENTITY_SRV.getById(Quotation.class, id);
		return quotation;
	}

	@Override
	public BaseEntityDao getDao() {
		return null;
	}

}
