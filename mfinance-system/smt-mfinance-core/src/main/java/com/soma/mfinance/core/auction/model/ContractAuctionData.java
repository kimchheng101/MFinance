package com.soma.mfinance.core.auction.model;

import com.soma.mfinance.core.asset.model.*;
import com.soma.mfinance.core.collection.model.EStockStatus;
import com.soma.mfinance.core.contract.model.Contract;
import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "td_contract_auction_data")
public class ContractAuctionData extends EntityA {


	private Double penaltyUsd;
	private Double repossessionFeeUsd;
	private Double collectionFeeUsd;
	private Double teAssetEstimationPrice1;
	private Double vatAssetEstimationPrice1;
	private Double tiAssetEstimationPrice1;
	private Double teAssetEstimationPrice2;
	private Double vatAssetEstimationPrice2;
	private Double tiAssetEstimationPrice2;
	private Double teAssetEstimationPrice3;
	private Double vatAssetEstimationPrice3;
	private Double tiAssetEstimationPrice3;
	private Double teAssetSellingPrice;
	private Double vatAssetSellingPrice;
	private Double tiAssetSellingPrice;
	
	private Integer milleage;
	private AssetExteriorStatus assetExteriorStatus;
	private EEngineStatus assetEnginStatus;
	private AssetPartsStatus assetPartsStatus;
	private AssetRegistrationStatus assetRegistrationStatus;
	private AssetPlateNumber assetPlateNumber;
	private Buyer buyer;
	private String purchaseAgreementFile;
	
	private Contract contract;
	
	private Date sellingDate;
	private Date requestRepossessedDate;
	//private AssetEnginStatus assetEnginStatus;
	private EStockStatus eStockStatus;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "con_auc_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name = "con_auc_am_penalty_usd", nullable = true)
	public Double getPenaltyUsd() {
		return penaltyUsd;
	}

	public void setPenaltyUsd(Double penaltyUsd) {
		this.penaltyUsd = penaltyUsd;
	}

    @Column(name = "con_auc_am_repossession_fee_usd", nullable = true)
	public Double getRepossessionFeeUsd() {
		return repossessionFeeUsd;
	}

	public void setRepossessionFeeUsd(Double repossessionFeeUsd) {
		this.repossessionFeeUsd = repossessionFeeUsd;
	}

	@Column(name = "con_auc_am_collection_fee_usd", nullable = true)
	public Double getCollectionFeeUsd() {
		return collectionFeeUsd;
	}

	public void setCollectionFeeUsd(Double collectionFeeUsd) {
		this.collectionFeeUsd = collectionFeeUsd;
	}

	@Column(name = "con_auc_am_te_asset_estimation_price_1", nullable = true)
	public Double getTeAssetEstimationPrice1() {
		return teAssetEstimationPrice1;
	}

	public void setTeAssetEstimationPrice1(Double teAssetEstimationPrice1) {
		this.teAssetEstimationPrice1 = teAssetEstimationPrice1;
	}

	@Column(name = "con_auc_am_vat_asset_estimation_price_1", nullable = true)
	public Double getVatAssetEstimationPrice1() {
		return vatAssetEstimationPrice1;
	}

	/**
	 * @param vatAssetEstimationPrice1 the vatAssetEstimationPrice1 to set
	 */
	public void setVatAssetEstimationPrice1(Double vatAssetEstimationPrice1) {
		this.vatAssetEstimationPrice1 = vatAssetEstimationPrice1;
	}

	@Column(name = "con_auc_am_ti_asset_estimation_price_1", nullable = true)
	public Double getTiAssetEstimationPrice1() {
		return tiAssetEstimationPrice1;
	}

	/**
	 * @param tiAssetEstimationPrice1 the tiAssetEstimationPrice1 to set
	 */
	public void setTiAssetEstimationPrice1(Double tiAssetEstimationPrice1) {
		this.tiAssetEstimationPrice1 = tiAssetEstimationPrice1;
	}

	@Column(name = "con_auc_am_te_asset_estimation_price_2", nullable = true)
	public Double getTeAssetEstimationPrice2() {
		return teAssetEstimationPrice2;
	}

	public void setTeAssetEstimationPrice2(Double teAssetEstimationPrice2) {
		this.teAssetEstimationPrice2 = teAssetEstimationPrice2;
	}

	@Column(name = "con_auc_am_vat_asset_estimation_price_2", nullable = true)
	public Double getVatAssetEstimationPrice2() {
		return vatAssetEstimationPrice2;
	}

	public void setVatAssetEstimationPrice2(Double vatAssetEstimationPrice2) {
		this.vatAssetEstimationPrice2 = vatAssetEstimationPrice2;
	}

	@Column(name = "con_auc_am_ti_asset_estimation_price_2", nullable = true)
	public Double getTiAssetEstimationPrice2() {
		return tiAssetEstimationPrice2;
	}

	public void setTiAssetEstimationPrice2(Double tiAssetEstimationPrice2) {
		this.tiAssetEstimationPrice2 = tiAssetEstimationPrice2;
	}

	@Column(name = "con_auc_am_te_asset_estimation_price_3", nullable = true)
	public Double getTeAssetEstimationPrice3() {
		return teAssetEstimationPrice3;
	}

	public void setTeAssetEstimationPrice3(Double teAssetEstimationPrice3) {
		this.teAssetEstimationPrice3 = teAssetEstimationPrice3;
	}

	@Column(name = "con_auc_am_vat_asset_estimation_price_3", nullable = true)
	public Double getVatAssetEstimationPrice3() {
		return vatAssetEstimationPrice3;
	}

	public void setVatAssetEstimationPrice3(Double vatAssetEstimationPrice3) {
		this.vatAssetEstimationPrice3 = vatAssetEstimationPrice3;
	}

	@Column(name = "con_auc_am_ti_asset_estimation_price_3", nullable = true)
	public Double getTiAssetEstimationPrice3() {
		return tiAssetEstimationPrice3;
	}

	public void setTiAssetEstimationPrice3(Double tiAssetEstimationPrice3) {
		this.tiAssetEstimationPrice3 = tiAssetEstimationPrice3;
	}	

	@Column(name = "con_auc_am_te_asset_selling_price", nullable = true)
	public Double getTeAssetSellingPrice() {
		return teAssetSellingPrice;
	}

	public void setTeAssetSellingPrice(Double teAssetSellingPrice) {
		this.teAssetSellingPrice = teAssetSellingPrice;
	}

	@Column(name = "con_auc_am_vat_asset_selling_price", nullable = true)
	public Double getVatAssetSellingPrice() {
		return vatAssetSellingPrice;
	}

	public void setVatAssetSellingPrice(Double vatAssetSellingPrice) {
		this.vatAssetSellingPrice = vatAssetSellingPrice;
	}

	@Column(name = "con_auc_am_ti_asset_selling_price", nullable = true)
	public Double getTiAssetSellingPrice() {
		return tiAssetSellingPrice;
	}

	/**
	 * @param tiAssetSellingPrice the tiAssetSellingPrice to set
	 */
	public void setTiAssetSellingPrice(Double tiAssetSellingPrice) {
		this.tiAssetSellingPrice = tiAssetSellingPrice;
	}

	@Column(name = "con_auc_nu_milleage", nullable = true)
	public Integer getMilleage() {
		return milleage;
	}

	public void setMilleage(Integer milleage) {
		this.milleage = milleage;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ass_ext_sta_id")
	public AssetExteriorStatus getAssetExteriorStatus() {
		return assetExteriorStatus;
	}

	public void setAssetExteriorStatus(AssetExteriorStatus assetExteriorStatus) {
		this.assetExteriorStatus = assetExteriorStatus;
	}

    @Column(name = "ass_eng_sta_id", nullable = true)
    @Convert(converter = EEngineStatus.class)
	public EEngineStatus getAssetEnginStatus() {
		return assetEnginStatus;
	}

	public void setAssetEnginStatus(EEngineStatus assetEnginStatus) {
		this.assetEnginStatus = assetEnginStatus;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ass_par_sta_id")
	public AssetPartsStatus getAssetPartsStatus() {
		return assetPartsStatus;
	}

	public void setAssetPartsStatus(AssetPartsStatus assetPartsStatus) {
		this.assetPartsStatus = assetPartsStatus;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ass_reg_sta_id")
	public AssetRegistrationStatus getAssetRegistrationStatus() {
		return assetRegistrationStatus;
	}

	public void setAssetRegistrationStatus(
			AssetRegistrationStatus assetRegistrationStatus) {
		this.assetRegistrationStatus = assetRegistrationStatus;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ass_pla_id")
	public AssetPlateNumber getAssetPlateNumber() {
		return assetPlateNumber;
	}

	public void setAssetPlateNumber(AssetPlateNumber assetPlateNumber) {
		this.assetPlateNumber = assetPlateNumber;
	}

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "buy_id")
	public Buyer getBuyer() {
		return buyer;
	}

	public void setBuyer(Buyer buyer) {
		this.buyer = buyer;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "con_id", unique = true)
	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	@Column(name = "con_auc_am_purchase_agreement_file", nullable = true)
	public String getPurchaseAgreementFile() {
		return purchaseAgreementFile;
	}

	public void setPurchaseAgreementFile(String purchaseAgreementFile) {
		this.purchaseAgreementFile = purchaseAgreementFile;
	}

	@Column(name = "con_auc_dt_selling", nullable = true)
	public Date getSellingDate() {
		return sellingDate;
	}

	public void setSellingDate(Date sellingDate) {
		this.sellingDate = sellingDate;
	}
	@Column(name = "con_auc_dt_req_repossess", nullable = true)
	public Date getRequestRepossessedDate() {
		return requestRepossessedDate;
	}

	public void setRequestRepossessedDate(Date repossessedDate) {
		this.requestRepossessedDate = repossessedDate;
	}

	public void setAssetStockStatus(EStockStatus stockStatus){
		this.eStockStatus = stockStatus;
	}

	@Column(name = "ststa_id", nullable = true)
	@Convert(converter = EStockStatus.class)
	public EStockStatus getAssetStockStatus(){
		return eStockStatus;
	}
}
