package com.soma.mfinance.core.asset.service;

import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.asset.model.appraisal.*;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.asset.model.appraisal.*;
import org.seuksa.frmk.service.BaseEntityService;

import javax.xml.namespace.QName;
import java.util.List;
import java.util.Map;

/**
 * Created by Kimsuor SEANG
 * Date  : 6/10/2017
 * Name  : 10:17 AM
 * Email : k.seang@gl-f.com
 */
public interface AssetAppraisalService extends BaseEntityService {

    List<AppraisalItem> getAppraisalItemByCategory(AppraisalCategory appraisalCategory);

    Map<String, Double> getMapApprasalValue(AssetAppraisal assetAppraisal);

    List<AppraisalCategory> getAppraisalCategoriesByAssetRang(AssetRange assetRange);

    Double getAvgCoefficientByGroup(AppraisalItem appraisalItem);

    List<AssetHistoryCategory> getAssetHistoryCategories(AssetRange  assetRange);

    List<AssetHistoryItem> getAssetHistoryItems(AssetHistoryCategory assetHistoryCategory);

    List<AppraisalItem> getAssetAppraisalItem(Long id);

    List<HistoryItem> getHistoryItemsByAssetHistoryItem(AssetHistoryItem assetHistoryItem);

    List<HistoryItem> getHistoryItemsByAssetAppraisal(AssetAppraisal assetAppraisal);

    AssetAppraisal getAssetAppraisalByAsset(Asset asset);

    Double getLeaseAmountPercent(String code);


}
