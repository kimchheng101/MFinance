package com.soma.mfinance.core.shared.financialproduct;

import com.soma.mfinance.core.shared.FMEntityField;

/**
 * @author kimsuor.seang
 */
public interface FinancialProductEntityField extends FMEntityField {



    String VAT_PEL = "PELNALTY";
    String VAT = "VAT";
    String VAT_SERVI = "VAT-SERVI";
    String VAT_SERVI_EXT = "VAT-SERVI-EXT";
    String VAT_INSUR = "VAT-INSUR";
    final static String FIN_CODE_PUBLIC = "PUB";
    final static String FIN_CODE_EXISTING = "EXT";
    final static String FIN_CODE_KUBUTA = "KUB";
    static final String TOTAL_INT_AMOUNT = "TOL-INSTALL";
    static final String PRINCILPLE_AMOUNT = "PRINCILPLE_AMOUNT";
    static final String VAT_PRINCILPLE_AMOUNT = "VAT_PRINCILPLE_AMOUNT";


}
