package com.soma.mfinance.core.asset.model;

import java.util.List;

import javax.persistence.*;

import com.soma.mfinance.core.asset.model.appraisal.AssetAppraisal;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.ersys.core.hr.model.eref.EColor;
import com.soma.mfinance.core.asset.model.appraisal.AssetAppraisal;
import org.seuksa.frmk.model.EntityFactory;

import java.util.Date;


/**
 * 
 * @author kimsuor.seang
 *
 */
@Entity
@Table(name = "td_asset")
public class Asset extends AbstractAsset  implements MAsset{
	private static final long serialVersionUID = -2870696410691091691L;

	private List<Contract> contracts;
	private AssetAppraisal assetAppraisal;
	private EAssetYear assetYear;
	private Double tiAssetSecondHandPrice;
	private Double teAssetSecondHandPrice;
	private Double tiAssetApprPrice;
	private Double teAssetApprPrice;

	// kubuta as tructor
	private String vehicleDocument;
	private String photoDieseEngine;
	private String warrantyDiesel;

	// kubuta as walking tructor
	private String warrantyPowerTiller;
	private String powerTillerReceipt;
	private String diselEngineReceipt;

	private AssetMake assetMakeDieselEngine;
	private AssetMake assetMakePowerTiller;
	private Date importDate;
	private Date purchageDate;
	private Long odometer;
	private EColor assetColorPowerTiler;
	private EAssetYear assetYearPowerTiller;
	private Double cashPriceUsdDieselEngine;
	private Double cashPriceUsdPowerTiller;
	private String chassisNumberPowerTiller;
	private AssetComponent assetComponentDieselEngine;
	private AssetComponent assetComponentPowerTiller;
	private EPowerTillerModel powerTillerModel;
	private Double tiAssetModelPrice;

	public static  Asset createInstance(){
		Asset asset = EntityFactory.createInstance(Asset.class);
		return  asset;
	}

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ass_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

	/**
	 * @return the contracts
	 */
    @OneToMany(mappedBy="asset", fetch = FetchType.LAZY)
	public List<Contract> getContracts() {
		return contracts;
	}

	/**
	 * @param contracts the contracts to set
	 */
	public void setContracts(List<Contract> contracts) {
		this.contracts = contracts;
	}

	@ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	@JoinColumn(name = "ass_app_id")
	public AssetAppraisal getAssetAppraisal() {
		return assetAppraisal;
	}

	public void setAssetAppraisal(AssetAppraisal assetAppraisal) {
		this.assetAppraisal = assetAppraisal;
	}

	@Column(name = "ass_using_year", nullable = true)
	@Convert(converter = EAssetYear.class)
	public EAssetYear getAssetYear() {
		return assetYear;
	}

	public void setAssetYear(EAssetYear assetYear) {
		this.assetYear = assetYear;
	}

	@Column(name = "ass_am_ti_second_hand_price", nullable = true)
	public Double getTiAssetSecondHandPrice() {
		return tiAssetSecondHandPrice;
	}

	public void setTiAssetSecondHandPrice(Double tiAssetSecondHandPrice) {
		this.tiAssetSecondHandPrice = tiAssetSecondHandPrice;
	}
	@Column(name = "ass_am_te_second_hand_price", nullable = true)
	public Double getTeAssetSecondHandPrice() {
		return teAssetSecondHandPrice;
	}

	public void setTeAssetSecondHandPrice(Double teAssetSecondHandPrice) {
		this.teAssetSecondHandPrice = teAssetSecondHandPrice;
	}

	@Column(name = "ass_am_ti_appr_price", nullable = true)
	public Double getTiAssetApprPrice() {
		return tiAssetApprPrice;
	}

	public void setTiAssetApprPrice(Double tiAssetApprPrice) {
		this.tiAssetApprPrice = tiAssetApprPrice;
	}
	@Column(name = "ass_am_te_appr_price", nullable = true)
	public Double getTeAssetApprPrice() {
		return teAssetApprPrice;
	}

	public void setTeAssetApprPrice(Double teAssetApprPrice) {
		this.teAssetApprPrice = teAssetApprPrice;
	}

	@Column(name = "war_die_path", nullable = true)
	public String getWarrantyDiesel() {
		return warrantyDiesel;
	}

	public void setWarrantyDiesel(String warrantyDiesel) {
		this.warrantyDiesel = warrantyDiesel;
	}

	@Column(name = "war_path_power_till", nullable = true)
	public String getWarrantyPowerTiller() {
		return warrantyPowerTiller;
	}

	public void setWarrantyPowerTiller(String warrantyPower) {
		this.warrantyPowerTiller = warrantyPower;
	}

	@Column(name = "power_tiller_receipt", nullable = true)
	public String getPowerTillerReceipt() {
		return powerTillerReceipt;
	}

	public void setPowerTillerReceipt(String powerTillerReceipt) {
		this.powerTillerReceipt = powerTillerReceipt;
	}

	@Column(name = "scrat_die_eng_path", nullable = true)
	public String getScrathDieselEngine() {
		return diselEngineReceipt;
	}

	public void setScrathDieselEngine(String diselEngineReceipt) {
		this.diselEngineReceipt = diselEngineReceipt;
	}

	@Column(name = "pho_die_eng_path", nullable = true)
	public String getPhotoDieseEngine() {
		return photoDieseEngine;
	}

	public void setPhotoDieseEngine(String photoDieseEngine) {
		this.photoDieseEngine = photoDieseEngine;
	}

	@Column(name = "vehic_doc_die_path", nullable = true)
	public String getVehicleDocumentDiesel() {
		return vehicleDocument;
	}

	public void setVehicleDocumentDiesel(String vehicleDocument) {
		this.vehicleDocument = vehicleDocument;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ass_mak_id")
	public AssetMake getAssetMakeDieselEngine() {
		return assetMakeDieselEngine;
	}

	public void setAssetMakeDieselEngine(AssetMake assetMakeDieselEngine) {
		this.assetMakeDieselEngine = assetMakeDieselEngine;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ass_mak_power_tiler_id", referencedColumnName = "ass_mak_id")
	public AssetMake getAssetMakePowerTiller() {
		return assetMakePowerTiller;
	}

	public void setAssetMakePowerTiller(AssetMake assetMakePowerTiller) {
		this.assetMakePowerTiller = assetMakePowerTiller;
	}

	@Column(name = "ass_dt_import", nullable = true)
	public Date getImportDate() {
		return importDate;
	}

	public void setImportDate(Date importDate) {
		this.importDate = importDate;
	}

	@Column(name = "ass_dt_purch", nullable = true)
	public Date getPurchageDate() {
		return purchageDate;
	}

	public void setPurchageDate(Date purchageDate) {
		this.purchageDate = purchageDate;
	}

	@Column(name = "ass_odo_meter", nullable = true)
	public Long getOdometer() {
		return odometer;
	}

	public void setOdometer(Long odometer) {
		this.odometer = odometer;
	}

	@Column(name = "color_power_tiler", nullable = true)
	@Convert(converter = EColor.class)
	public EColor getAssetColorPowerTiler() {
		return assetColorPowerTiler;
	}

	public void setAssetColorPowerTiler(EColor assetColorPowerTiler) {
		this.assetColorPowerTiler = assetColorPowerTiler;
	}

	@Column(name = "ass_year_power_tiler", nullable = true)
	@Convert(converter = EAssetYear.class)
	public EAssetYear getAssetYearPowerTiller() {
		return assetYearPowerTiller;
	}

	public void setAssetYearPowerTiller(EAssetYear assetYearPowerTiller) {
		this.assetYearPowerTiller = assetYearPowerTiller;
	}

	@Column(name = "ass_cash_price_diesel_engin_us", nullable = true, length = 100)
	public Double getCashPriceUsdDieselEngine() {
		return cashPriceUsdDieselEngine;
	}

	public void setCashPriceUsdDieselEngine(Double cashPriceUsdDieselEngine) {
		this.cashPriceUsdDieselEngine = cashPriceUsdDieselEngine;
	}

	@Column(name = "ass_cash_price_power_tiler_us", nullable = true, length = 100)
	public Double getCashPriceUsdPowerTiller() {
		return cashPriceUsdPowerTiller;
	}

	public void setCashPriceUsdPowerTiller(Double cashPriceUsdPowerTiller) {
		this.cashPriceUsdPowerTiller = cashPriceUsdPowerTiller;
	}

	@Column(name = "ass_chassis_number_power_tiler", nullable = true, length = 100)
	public String getChassisNumberPowerTiller() {
		return chassisNumberPowerTiller;
	}

	public void setChassisNumberPowerTiller(String chassisNumberPowerTiller) {
		this.chassisNumberPowerTiller = chassisNumberPowerTiller;
	}

	@JoinColumn(name = "ass_com_disel_engin", referencedColumnName = "ass_com_id")
	public AssetComponent getAssetComponentDieselEngine() {
		return assetComponentDieselEngine;
	}

	public void setAssetComponentDieselEngine(AssetComponent assetComponentDieselEngine) {
		this.assetComponentDieselEngine = assetComponentDieselEngine;
	}

	@JoinColumn(name = "ass_com_power_tiler", referencedColumnName = "ass_com_id")
	public AssetComponent getAssetComponentPowerTiller() {
		return assetComponentPowerTiller;
	}

	public void setAssetComponentPowerTiller(AssetComponent assetComponentPowerTiller) {
		this.assetComponentPowerTiller = assetComponentPowerTiller;
	}

	@Column(name = "pow_till_mod", nullable = true)
	@Convert(converter = EPowerTillerModel.class)
	public EPowerTillerModel getPowerTillerModel() {
		return powerTillerModel;
	}

	public void setPowerTillerModel(EPowerTillerModel powerTillerModel) {
		this.powerTillerModel = powerTillerModel;
	}

	@Column(name = "ass_mod_ti_cash_price_usd", nullable = true)
	public Double getTiAssetModelPrice() {
		return tiAssetModelPrice;
	}

	public void setTiAssetModelPrice(Double tiAssetModelPrice) {
		this.tiAssetModelPrice = tiAssetModelPrice;
	}


}
