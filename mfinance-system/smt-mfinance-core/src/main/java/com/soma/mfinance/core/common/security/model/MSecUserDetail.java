package com.soma.mfinance.core.common.security.model;

/**
 * Meta data of com.soma.mfinance.core.common.security.model.SecUserDetail
 * @author kimsuor.seang
 */
public interface MSecUserDetail {
	
	public final static String SECUSER = "secUser";

}
