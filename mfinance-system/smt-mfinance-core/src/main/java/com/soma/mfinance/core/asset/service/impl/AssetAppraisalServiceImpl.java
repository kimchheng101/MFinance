package com.soma.mfinance.core.asset.service.impl;

import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.asset.model.appraisal.*;
import com.soma.mfinance.core.asset.panel.appraisal.AppraisalConstant;
import com.soma.mfinance.core.asset.service.AssetAppraisalService;
import com.soma.frmk.config.model.SettingConfig;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.asset.model.appraisal.*;
import com.soma.mfinance.core.asset.panel.appraisal.AppraisalConstant;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.dao.EntityDao;
import org.seuksa.frmk.dao.criteria.FilterMode;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Map;

import static com.soma.frmk.helper.SeuksaServicesHelper.ENTITY_SRV;
import static org.seuksa.frmk.model.entity.MEntityA.ID;

/**
 * Created by Kimsuor SEANG
 * Date  : 6/10/2017
 * Name  : 10:18 AM
 * Email : k.seang@gl-f.com
 */
@Service("assetAppraisalService")
public class AssetAppraisalServiceImpl extends BaseEntityServiceImpl implements AssetAppraisalService, AppraisalConstant {

    @Autowired
    private EntityDao dao;

    @Override
    public BaseEntityDao getDao() {
        return dao;
    }

    @Transactional(readOnly = true)
    @Override
    public List<AppraisalItem> getAppraisalItemByCategory(AppraisalCategory appraisalCategory) {
        BaseRestrictions<AppraisalItem> appraisalItemBaseRestrictions = new BaseRestrictions<>(AppraisalItem.class);
        appraisalItemBaseRestrictions.addCriterion(Restrictions.eq(APPRAISAL_CATEGORY, appraisalCategory));
        appraisalItemBaseRestrictions.addCriterion("statusRecord", FilterMode.EQUALS, new Object[]{EStatusRecord.ACTIV});
        appraisalItemBaseRestrictions.addOrder(Order.asc(SORT_INDEX));
        return list(appraisalItemBaseRestrictions);
    }

    @Override
    public Map<String, Double> getMapApprasalValue(AssetAppraisal assetAppraisal) {
        return null;
    }

    @Transactional(readOnly = true)
    @Override
    public List<AppraisalCategory> getAppraisalCategoriesByAssetRang(AssetRange assetRange) {
        BaseRestrictions<AppraisalCategory> appraisalCategoryList = new BaseRestrictions<>(AppraisalCategory.class);
        // If it is dream
        if (ASSET_DREAM_CODE.equals(assetRange.getCode())) {
            appraisalCategoryList.addCriterion(Restrictions.eq("appraisalRange", EAppraisalRange.DREAM));
        } else if (ASSET_TRACTOR_CODE.equals(assetRange.getCode())) {
            appraisalCategoryList.addCriterion(Restrictions.eq("appraisalRange", EAppraisalRange.TRACTOR));
        } else if (ASSET_COMBINE_HARVESTER_CODE.equals(assetRange.getCode())) {
            appraisalCategoryList.addCriterion(Restrictions.eq("appraisalRange", EAppraisalRange.COMBINE_HARVESTER));
        } else if (ASSET_WALKING_TRACTOR_CODE.equals(assetRange.getCode())) {
            appraisalCategoryList.addCriterion(Restrictions.eq("appraisalRange", EAppraisalRange.WALKING_TRACTOR));
        } else {
            // it is none dream
            appraisalCategoryList.addCriterion(Restrictions.eq("appraisalRange", EAppraisalRange.NONE_DREAM));
        }
        appraisalCategoryList.addCriterion("statusRecord", FilterMode.EQUALS, new Object[]{EStatusRecord.ACTIV});
        return list(appraisalCategoryList);
    }

    @Override
    public Double getAvgCoefficientByGroup(AppraisalItem appraisalItem) {
        Assert.notNull(appraisalItem.getAppraisalGroup(), "Appraisal Group could not be null..!");
        AppraisalGroup appraisalGroup = appraisalItem.getAppraisalGroup();
        BaseRestrictions<AppraisalItem> appraisalItemBaseRestrictions = new BaseRestrictions<>(AppraisalItem.class);
        appraisalItemBaseRestrictions.addCriterion("statusRecord", FilterMode.EQUALS, new Object[]{EStatusRecord.ACTIV});
        appraisalItemBaseRestrictions.addCriterion(Restrictions.eq("appraisalGroup", appraisalGroup));
        List<AppraisalItem> appraisalItemList = list(appraisalItemBaseRestrictions);
        Double result = 0D;
        if (appraisalItemList != null && appraisalItemList.size() > 0) {
            if (appraisalItem.getWeightCoefficient() != null && appraisalItem.getWeightCoefficient().doubleValue() < 1d) {
                result = appraisalGroup.getCoefficient().doubleValue();
            } else {
                result = appraisalGroup.getCoefficient().doubleValue() / appraisalItemList.size();
            }
        }
        return result;

    }

    @Override
    public List<AssetHistoryCategory> getAssetHistoryCategories(AssetRange assetRange) {
        BaseRestrictions<AssetHistoryCategory> restrictions = new BaseRestrictions<>(AssetHistoryCategory.class);
        restrictions.addCriterion(Restrictions.eq("assetAppraisalType.id", assetRange.getId()));
        restrictions.addOrder(Order.asc("sortIndex"));
        List<AssetHistoryCategory> assetHistoryCategories = ENTITY_SRV.list(restrictions);
        return assetHistoryCategories;
    }

    @Override
    public List<AssetHistoryItem> getAssetHistoryItems(AssetHistoryCategory assetHistoryCategory) {
        BaseRestrictions<AssetHistoryItem> restrictions = new BaseRestrictions<>(AssetHistoryItem.class);
        restrictions.addCriterion(Restrictions.eq("assetHistoryCategory.id", assetHistoryCategory.getId()));
        restrictions.addOrder(Order.asc("sortIndex"));
        List<AssetHistoryItem> assetHistoryItems = ENTITY_SRV.list(restrictions);
        return assetHistoryItems;
    }

    @Override
    public List<AppraisalItem> getAssetAppraisalItem(Long id) {
        BaseRestrictions<AppraisalItem> restrictions = new BaseRestrictions<>(AppraisalItem.class);
        restrictions.addCriterion(Restrictions.eq("assetAppraisalCategory.id", id));
        restrictions.addOrder(Order.asc("sortIndex"));
        List<AppraisalItem> appraisalItems = ENTITY_SRV.list(restrictions);
        return appraisalItems;
    }

    @Override
    public List<HistoryItem> getHistoryItemsByAssetHistoryItem(AssetHistoryItem assetHistoryItem) {
        BaseRestrictions<HistoryItem> restrictions = new BaseRestrictions<>(HistoryItem.class);
        restrictions.addCriterion(Restrictions.eq("assetHistoryItem.id", assetHistoryItem.getId()));
        restrictions.addOrder(Order.asc("sortIndex"));
        List<HistoryItem> historyItems = ENTITY_SRV.list(restrictions);
        return historyItems;
    }

    @Override
    public List<HistoryItem> getHistoryItemsByAssetAppraisal(AssetAppraisal assetAppraisal) {
        BaseRestrictions<HistoryItem> restrictions = new BaseRestrictions<>(HistoryItem.class);
        restrictions.addCriterion(Restrictions.eq("assetAppraisal.id", assetAppraisal.getId()));
        restrictions.addOrder(Order.asc("sortIndex"));
        List<HistoryItem> historyItems = ENTITY_SRV.list(restrictions);
        return historyItems;
    }

    @Override
    public AssetAppraisal getAssetAppraisalByAsset(Asset asset) {
        if (asset != null) {
            if (asset.getAssetAppraisal() != null) {
                BaseRestrictions<AssetAppraisal> restrictions = new BaseRestrictions<>(AssetAppraisal.class);
                restrictions.addCriterion(Restrictions.eq(ID, asset.getAssetAppraisal().getId()));
                List<AssetAppraisal> assetAppraisals = ENTITY_SRV.list(restrictions);
                if (assetAppraisals != null && !assetAppraisals.isEmpty()) {
                    return assetAppraisals.get(0);
                }
            }
        }
        return null;
    }

    @Override
    public Double getLeaseAmountPercent(String code) {
        BaseRestrictions<SettingConfig> restrictions = new BaseRestrictions<>(SettingConfig.class);
        restrictions.addCriterion(Restrictions.eq("code", code));
        List<SettingConfig> settingConfigs = ENTITY_SRV.list(restrictions);
        if (settingConfigs != null && !settingConfigs.isEmpty()) {
            return Double.parseDouble(settingConfigs.get(0).getValue());
        }
        return 0d;
    }
}
