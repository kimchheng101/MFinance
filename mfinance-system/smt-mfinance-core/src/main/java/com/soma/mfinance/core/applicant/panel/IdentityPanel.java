package com.soma.mfinance.core.applicant.panel;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.mfinance.core.applicant.model.IndividualContactInfo;
import com.soma.mfinance.core.applicant.model.IndividualSpouse;
import com.soma.mfinance.core.applicant.service.ValidateNumbers;
import com.soma.ersys.core.hr.model.address.Province;
import com.soma.ersys.core.hr.model.eref.ECivility;
import com.soma.ersys.core.hr.model.eref.EGender;
import com.soma.ersys.core.hr.model.eref.EMaritalStatus;
import com.soma.ersys.core.hr.model.eref.ENationality;
import com.soma.ersys.core.hr.model.organization.ContactInfo;
import com.soma.frmk.security.context.SecApplicationContextHolder;
import com.soma.frmk.vaadin.ui.panel.AbstractControlPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.soma.mfinance.core.applicant.service.ValidateLetter;
import com.soma.mfinance.core.applicant.service.ValidateNumbers;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.Runo;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.service.EntityService;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.soma.mfinance.core.applicant.service.ValidateLetter.isEnglishLetter;
import static com.soma.mfinance.core.applicant.service.ValidateLetter.isKhmerLetter;
import static com.soma.mfinance.core.applicant.service.ValidateNumbers.isValidOnlyNumber;
import static com.soma.mfinance.core.applicant.service.ValidateNumbers.isValidPhoneNumber;
import static com.soma.mfinance.core.helper.FinServicesHelper.INDIVI_SRV;
import static com.soma.ersys.core.hr.model.eref.ETypeContactInfo.MOBILE;


/**
 * Customer identity panel
 *
 * @author kimsuor.seang
 */
public class IdentityPanel extends AbstractControlPanel {

    private static final long serialVersionUID = 710425425958548975L;
    private EntityService entityService = (EntityService) SecApplicationContextHolder.getContext().getBean("entityService");
    private TextField txtFirstName;
    private TextField txtFirstNameOTH;
    private TextField txtFirstNameSpouse;
    private TextField txtFamilyName;
    private TextField txtFamilyNameOTH;
    private TextField txtNickName;
    private TextField txtFamilyNameSpouse;
    private TextField txtNumberOfChildren;
    private AutoDateField dfDateOfBirth;
    private EntityRefComboBox<Province> cbxProvinceOfBirth;
    private ERefDataComboBox<ECivility> cbxCivility;
    private ERefDataComboBox<EMaritalStatus> cbxMaritalStatus;
    private ERefDataComboBox<EGender> cbxGender;
    private ERefDataComboBox<ENationality> cbxNationality;

    private TextField txtMobilePhone1;
    private TextField txtMobilePhone2;
    private TextField txtSpouseMobilePhone;

    private Button btnAddPhoneNumber;
    private Button btnShowPhoneNumber;
    private Individual individual;
    ValidateNumbers vnum = new ValidateNumbers();

    public IdentityPanel(String template) {
        ArrayList valGarenty = new ArrayList<>();
        setSizeFull();
        InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
        CustomLayout customLayout = null;
        try {
            customLayout = new CustomLayout(layoutFile);
        } catch (IOException e) {
            Notification.show("Could not locate template " + template, e.getMessage(), Type.ERROR_MESSAGE);
        }

        cbxCivility = new ERefDataComboBox<ECivility>(ECivility.values());
        cbxCivility.setImmediate(true);
        cbxCivility.setWidth("140px");

        cbxGender = new ERefDataComboBox<EGender>(EGender.values());
        cbxGender.setImmediate(true);
        cbxGender.setWidth("140px");

        cbxMaritalStatus = new ERefDataComboBox<EMaritalStatus>(EMaritalStatus.values());
        cbxMaritalStatus.setImmediate(true);
        cbxMaritalStatus.setWidth("140px");

        cbxProvinceOfBirth = new EntityRefComboBox<Province>();
        cbxProvinceOfBirth.setRestrictions(new BaseRestrictions<>(Province.class));
        cbxProvinceOfBirth.setImmediate(true);
        cbxProvinceOfBirth.setWidth("140px");
        cbxProvinceOfBirth.renderer();

        cbxNationality = new ERefDataComboBox<ENationality>(ENationality.values());
        cbxNationality.setImmediate(true);
        cbxNationality.setWidth("140px");

        txtFamilyName = ComponentFactory.getTextField(60, 140);
        txtFamilyNameOTH = ComponentFactory.getTextField(60, 140);
        txtFirstName = ComponentFactory.getTextField(60, 140);
        txtFirstNameOTH = ComponentFactory.getTextField(60, 140);
        txtNickName = ComponentFactory.getTextField("", false, 60, 140);

        txtFamilyNameSpouse = ComponentFactory.getTextField("", false, 60, 140);
        txtFirstNameSpouse = ComponentFactory.getTextField("", false, 60, 140);
        txtNumberOfChildren = ComponentFactory.getTextField("", false, 60, 140);
        valGarenty.add(txtNumberOfChildren);

        txtMobilePhone1 = ComponentFactory.getTextField("", false, 60, 100);
        txtMobilePhone2 = ComponentFactory.getTextField("", false, 60, 140);
        txtSpouseMobilePhone = ComponentFactory.getTextField("", false, 60, 140);
        dfDateOfBirth = ComponentFactory.getAutoDateField("", false);
        dfDateOfBirth.setWidth(140, Unit.PIXELS);


        btnAddPhoneNumber = new Button();
        btnAddPhoneNumber.setVisible(true);
        btnAddPhoneNumber.setStyleName(Runo.BUTTON_LINK);
        btnAddPhoneNumber.setIcon(new ThemeResource("../smt-default/icons/16/add.png"));

        btnAddPhoneNumber.addClickListener(new Button.ClickListener() {
            private static final long serialVersionUID = -3348402917006996671L;

            @Override
            public void buttonClick(Button.ClickEvent event) {

                final Window winAddPhone = new Window();
                winAddPhone.setModal(true);
                winAddPhone.setWidth(300, Unit.PIXELS);
                winAddPhone.setHeight(220, Unit.PIXELS);
                winAddPhone.setCaption(I18N.message("phone.number"));

                final TextField txtPhoneNumber = ComponentFactory.getTextField("phone.number", true, 30, 130);

                Button btnCancel = new NativeButton(I18N.message("close"), new Button.ClickListener() {
                    private static final long serialVersionUID = 3975121141565713259L;

                    public void buttonClick(Button.ClickEvent event) {
                        winAddPhone.close();
                    }
                });
                btnCancel.setIcon(new ThemeResource("../smt-default/icons/16/delete.png"));
                Button btnSave = new NativeButton(I18N.message("save"), new Button.ClickListener() {
                    private static final long serialVersionUID = 8088485001713740490L;

                    public void buttonClick(Button.ClickEvent event) {
                        Boolean isValidPhoneNumber = ValidateNumbers.isValidPhoneNumber(txtPhoneNumber.getValue());
                        if (!txtPhoneNumber.getValue().equals("") && isValidPhoneNumber) {
                            if (individual != null && individual.getId() != null) {
                                ContactInfo contactInfo = new ContactInfo();
                                contactInfo.setValue(txtPhoneNumber.getValue());
                                contactInfo.setTypeInfo(MOBILE);
                                ENTITY_SRV.saveOrUpdate(contactInfo);
                                IndividualContactInfo individualContactInfoObject = new IndividualContactInfo();
                                individualContactInfoObject.setContactInfo(contactInfo);
                                individualContactInfoObject.setIndividual(individual);
                                ENTITY_SRV.saveOrUpdate(individualContactInfoObject);
                            } else {
                                btnAddPhoneNumber.setVisible(false);
                                btnShowPhoneNumber.setVisible(false);
                            }
                            /*SecUser secUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                            Comment comment = new Comment();
                            comment.setDesc("Add a new phone number " + txtPhoneNumber.getValue() + " to this applicant");
                            comment.setUser(secUser);
                            ENTITY_SRV.saveOrUpdate(comment);*/
                            winAddPhone.close();
                        } else {
                            MessageBox mb = new MessageBox(UI.getCurrent(), "320px", "140px", I18N.message("information"),
                                    MessageBox.Icon.ERROR, "The " + I18N.message("phone.number.is.not.correct"), Alignment.MIDDLE_RIGHT,
                                    new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, I18N.message("ok")));
                            mb.show();
                        }
                    }
                });
                btnSave.setIcon(new ThemeResource("../smt-default/icons/16/save.png"));

                NavigationPanel navigationPanel = new NavigationPanel();
                navigationPanel.addButton(btnSave);
                navigationPanel.addButton(btnCancel);
                VerticalLayout contentLayout = new VerticalLayout();
                contentLayout.setMargin(true);
                contentLayout.addComponent(new FormLayout(txtPhoneNumber));
                winAddPhone.setContent(new VerticalLayout(navigationPanel, contentLayout));

                UI.getCurrent().addWindow(winAddPhone);
            }
        });


        btnShowPhoneNumber = new Button();
        btnShowPhoneNumber.setVisible(true);
        btnShowPhoneNumber.setStyleName(Runo.BUTTON_LINK);
        btnShowPhoneNumber.setIcon(new ThemeResource("../smt-default/icons/16/table.png"));

        btnShowPhoneNumber.addClickListener(new Button.ClickListener() {
            private static final long serialVersionUID = -5719845559566313177L;

            @SuppressWarnings("unchecked")
            @Override
            public void buttonClick(Button.ClickEvent event) {
                final Window winAddPhone = new Window();
                winAddPhone.setModal(true);
                winAddPhone.setWidth(500, Unit.PIXELS);
                winAddPhone.setHeight(310, Unit.PIXELS);
                winAddPhone.setCaption(I18N.message("phone.numbers"));
                List<ColumnDefinition> columnDefinitions = new ArrayList<>();
                columnDefinitions.add(new ColumnDefinition("phone.number", I18N.message("phone.number"), String.class, Table.Align.LEFT, 100));
                columnDefinitions.add(new ColumnDefinition("create.user", I18N.message("create.user"), String.class, Table.Align.LEFT, 120));
                columnDefinitions.add(new ColumnDefinition("create.date", I18N.message("create.date"), Date.class, Table.Align.LEFT, 120));
                SimplePagedTable<ContactInfo> pagedTable = new SimplePagedTable<>(columnDefinitions);
                IndexedContainer indexedContainer = new IndexedContainer();
                for (ColumnDefinition column : columnDefinitions) {
                    indexedContainer.addContainerProperty(column.getPropertyId(), column.getPropertyType(), null);
                }
                List<IndividualContactInfo> individualContactInfos = INDIVI_SRV.getIndividualContractInfos(individual);
                if (individualContactInfos != null) {
                    for (IndividualContactInfo individualContactInfo : individualContactInfos) {
                        Item item = indexedContainer.addItem(individualContactInfo.getContactInfo().getId());
                        item.getItemProperty("phone.number").setValue(individualContactInfo.getContactInfo().getValue());
                        item.getItemProperty("create.user").setValue(individualContactInfo.getCreateUser());
                        item.getItemProperty("create.date").setValue(individualContactInfo.getCreateDate());
                    }
                    pagedTable.setContainerDataSource(indexedContainer);
                }
                winAddPhone.setContent(new VerticalLayout(pagedTable, pagedTable.createControls()));
                UI.getCurrent().addWindow(winAddPhone);
            }
        });


        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("civility")), "lblCivility");
        customLayout.addComponent(cbxCivility, "cbxCivility");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("familyname")), "lblFamilyName");
        customLayout.addComponent(txtFamilyName, "txtFamilyName");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("familyname.oth")), "lblFamilyNameOTH");
        customLayout.addComponent(txtFamilyNameOTH, "txtFamilyNameOTH");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("firstname")), "lblFirstName");
        customLayout.addComponent(txtFirstName, "txtFirstName");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("firstname.oth")), "lblFirstNameOTH");
        customLayout.addComponent(txtFirstNameOTH, "txtFirstNameOTH");

        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("nickname")), "lblNickname");
        customLayout.addComponent(txtNickName, "txtNickname");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("spouse.familyname")), "lblSpouseFamilyName");
        customLayout.addComponent(txtFamilyNameSpouse, "txtSpouseFamilyName");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("spouse.firstname")), "lblSpouseFirstName");
        customLayout.addComponent(txtFirstNameSpouse, "txtSpouseFirstName");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("number.of.children")), "lblNumberOfChildren");
        customLayout.addComponent(txtNumberOfChildren, "txtNumberOfChildren");

        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("gender")), "lblGender");
        customLayout.addComponent(cbxGender, "cbxGender");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("marital.status")), "lblMaritalStatus");
        customLayout.addComponent(cbxMaritalStatus, "cbxMaritalStatus");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("province.of.birth")), "lblProvinceOfBirth");
        customLayout.addComponent(cbxProvinceOfBirth, "cbxProvinceOfBirth");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("dateofbirth")), "lblDateOfBirth");
        customLayout.addComponent(dfDateOfBirth, "dfDateOfBirth");

        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("nationality")), "lblNationality");
        customLayout.addComponent(cbxNationality, "cbxNationality");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("mobile.phone1")), "lblMobilePhone1");
        customLayout.addComponent(txtMobilePhone1, "txtMobilePhone1");
        customLayout.addComponent(btnAddPhoneNumber, "btnAddPhoneNumber");
        customLayout.addComponent(btnShowPhoneNumber, "btnShowPhoneNumber");

        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("mobile.phone2")), "lblMobilePhone2");
        customLayout.addComponent(txtMobilePhone2, "txtMobilePhone2");
        customLayout.addComponent(ComponentFactory.getLabel(I18N.message("mobile.phone.spouse")), "lblSpouseMobilePhone");
        customLayout.addComponent(txtSpouseMobilePhone, "txtSpouseMobilePhone");


        cbxCivility.addValueChangeListener(event -> {
            if (cbxCivility.getSelectedEntity() == ECivility.getByCode("MR")) {
                cbxGender.setSelectedEntity(EGender.M);
            }
            if (cbxCivility.getSelectedEntity() == ECivility.getByCode("MRS") || cbxCivility.getSelectedEntity() == ECivility.getByCode("MS")) {
                cbxGender.setSelectedEntity(EGender.F);
            }
        });

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        verticalLayout.setMargin(true);
        verticalLayout.addComponent(customLayout);
        Panel contentPanel = new Panel(I18N.message("identity"));
        contentPanel.setSizeFull();
        contentPanel.setContent(verticalLayout);
        addComponent(contentPanel);
        vnum.validateNumber(valGarenty);
    }

    /**
     * @return
     */
    public Applicant getApplicant(Applicant applicant) {
        IndividualSpouse individualSpouse;
        if (applicant.getIndividual() == null) {
            this.individual = new Individual();
        } else {
            this.individual = applicant.getIndividual();
        }

        if (this.individual.getIndividualSpouse() == null) {
            individualSpouse = new IndividualSpouse();
        } else {
            individualSpouse = individual.getIndividualSpouse();
        }

        individual.setFirstName(txtFirstNameOTH.getValue().trim());
        individual.setFirstNameEn(txtFirstName.getValue());
        individual.setLastName(txtFamilyNameOTH.getValue().trim());
        individual.setLastNameEn(txtFamilyName.getValue());
        individual.setNickName(txtNickName.getValue());
        // TODO YLY
        individualSpouse.setFirstName(txtFirstNameSpouse.getValue());
        individualSpouse.setLastName(txtFamilyNameSpouse.getValue());

        individual.setNumberOfChildren(getInteger(txtNumberOfChildren));
        individual.setBirthDate(dfDateOfBirth.getValue());
        individual.setPlaceOfBirth(cbxProvinceOfBirth.getSelectedEntity());
        individual.setCivility(cbxCivility.getSelectedEntity());
        individual.setGender(cbxGender.getSelectedEntity());
        individual.setMaritalStatus(cbxMaritalStatus.getSelectedEntity());
        individual.setNationality(cbxNationality.getSelectedEntity());

        // TODO YLY
        individual.setMobilePerso(txtMobilePhone1.getValue());
        individual.setMobilePerso2(txtMobilePhone2.getValue());
        individualSpouse.setMobilePhone(txtSpouseMobilePhone.getValue());
        individual.getIndividualSpouses().add(individualSpouse);
        applicant.setIndividual(individual);
        return applicant;
    }

    /**
     * @param
     */
    public void assignValues(Individual individual) {
        this.individual = individual;
        if (individual != null) {
            cbxCivility.setSelectedEntity(individual.getCivility());
            cbxGender.setSelectedEntity(individual.getGender());
            cbxMaritalStatus.setSelectedEntity(individual.getMaritalStatus());
            cbxNationality.setSelectedEntity(individual.getNationality());
            cbxProvinceOfBirth.setSelectedEntity(individual.getPlaceOfBirth());
            txtFirstName.setValue(getDefaultString(individual.getFirstNameEn()));
            txtFirstNameOTH.setValue(getDefaultString(individual.getFirstName()));
            txtFamilyName.setValue(getDefaultString(individual.getLastNameEn()));
            txtFamilyNameOTH.setValue(getDefaultString(individual.getLastName()));
            txtNickName.setValue(getDefaultString(individual.getNickName()));
            dfDateOfBirth.setValue(individual.getBirthDate());
            txtMobilePhone1.setValue(getDefaultString(individual.getMobilePerso()));
            txtMobilePhone2.setValue(getDefaultString(individual.getMobilePerso2()));
            if (individual.getIndividualSpouse() != null) {
                txtFirstNameSpouse.setValue(getDefaultString(individual.getIndividualSpouse().getFirstName()));
                txtFamilyNameSpouse.setValue(getDefaultString(individual.getIndividualSpouse().getLastName()));
                txtSpouseMobilePhone.setValue(getDefaultString(individual.getIndividualSpouse().getMobilePhone()));
            }
            txtNumberOfChildren.setValue(getDefaultString(individual.getNumberOfChildren()));
        }
    }

    /**
     * Reset panel
     */
    public void reset() {
        assignValues(new Individual());
    }

    /**
     * @return
     */
    public List<String> validate() {
        super.reset();
        checkMandatorySelectField(cbxCivility, "civility");
        checkMandatoryField(txtFamilyName, "familyname");
        checkMandatorySelectField(cbxGender, "gender");
        checkMandatorySelectField(cbxNationality, "nationality");
        checkMandatoryField(txtFamilyNameOTH, "familyname.oth");
        checkMandatorySelectField(cbxMaritalStatus, "marital.status");
        checkMandatoryField(txtMobilePhone1, "mobile..phone.1");
        checkMandatoryField(txtFirstName, "firstname");
        checkMandatorySelectField(cbxProvinceOfBirth, "province.of.birth");
        checkMandatoryField(txtFirstNameOTH, "firstname.oth");
        checkMandatoryDateField(dfDateOfBirth, "dateofbirth");
        return errors;
    }

    //@return
    public List<String> fullValidate() {
        super.reset();
        checkMandatorySelectField(cbxCivility, "civility");
        checkMandatoryField(txtFirstName, "firstname.en");
        checkMandatoryField(txtFirstNameOTH, "firstname");
        checkMandatoryField(txtFamilyName, "lastname.en");

        if(!txtFamilyName.getValue().matches("\\w+")){
            errors.add((I18N.message("last.name.not.allow.to.input.number.or.firstspace.endspace.or.symbols.en")));
        }

        checkMandatoryField(txtFirstNameOTH, "lastname");
        checkMandatorySelectField(cbxGender, "gender");
        checkMandatorySelectField(cbxMaritalStatus, "marital.status");
        checkMandatorySelectField(cbxNationality, "nationality");
        checkMandatorySelectField(cbxProvinceOfBirth, "placeofbirth");
        checkMandatoryDateField(dfDateOfBirth, "dateofbirth");
        checkMandatoryField(txtMobilePhone1, "mobile.phone1");

        if (INDIVI_SRV.isIndividualOver18Years(dfDateOfBirth.getValue())) {
            errors.add(I18N.message("applicant.not.over.18.and.65.years"));
        }
        if (!ValidateNumbers.isValidOnlyNumber(txtMobilePhone1.getValue())) {
            errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("phone number1 * contain character"));
        }
        if (!ValidateNumbers.isValidOnlyNumber(txtMobilePhone2.getValue())) {
            errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("phone number2 contain character"));
        }
        if (!ValidateNumbers.isValidOnlyNumber(txtSpouseMobilePhone.getValue())) {
            errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("Spouse phone number contain character"));
        }
         /*
        Check Khmer Phone Number
         */
        if (!ValidateNumbers.isValidPhoneNumber(txtMobilePhone1.getValue())) {
            errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("Mobile Phone 1 is invalid!"));
        }
        if (!ValidateNumbers.isValidPhoneNumber(txtMobilePhone2.getValue())) {
            errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("Mobile Phone 2 is invalid!"));
        }
        if (!ValidateNumbers.isValidPhoneNumber(txtSpouseMobilePhone.getValue())) {
            errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("Spouse phone is invalid!"));
        }
        if (!ValidateLetter.isKhmerLetter(txtFamilyNameOTH.getValue())) {
            errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("family.name.allow.input.khmer.only"));
        }
        if (!ValidateLetter.isKhmerLetter(txtFirstNameOTH.getValue())) {
            errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("first.name.allow.input.khmer.only"));
        }
        if (!ValidateLetter.isEnglishLetter(txtFirstName.getValue())) {
            errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("first.name.not.allow.to.input.number.or.firstspace.endspace.or.symbols.en"));
        }
        return errors;
    }

    public void setEnableIdentityPanel(boolean istrue) {
        txtFamilyNameOTH.setEnabled(istrue);
        txtFirstName.setEnabled(istrue);
        txtFirstNameOTH.setEnabled(istrue);
        txtFamilyName.setEnabled(istrue);
        txtNickName.setEnabled(istrue);
        dfDateOfBirth.setEnabled(istrue);
        cbxProvinceOfBirth.setEnabled(istrue);
        cbxCivility.setEnabled(istrue);
        cbxGender.setEnabled(istrue);
        cbxMaritalStatus.setEnabled(istrue);
        cbxNationality.setEnabled(istrue);
        txtMobilePhone1.setEnabled(istrue);
        txtMobilePhone2.setEnabled(istrue);
        txtFirstNameSpouse.setEnabled(istrue);
        txtFamilyNameSpouse.setEnabled(istrue);
        txtSpouseMobilePhone.setEnabled(istrue);
        txtNumberOfChildren.setEnabled(istrue);
    }
}
