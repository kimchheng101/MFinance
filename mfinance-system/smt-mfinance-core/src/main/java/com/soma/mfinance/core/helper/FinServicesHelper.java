package com.soma.mfinance.core.helper;

import com.soma.ersys.collab.tools.helper.ErsysCollabAppServicesHelper;
import com.soma.ersys.core.hr.service.OrgStructureService;
import com.soma.ersys.core.hr.service.OrganizationService;
import com.soma.mfinance.core.accounting.services.AccountingService;
import com.soma.mfinance.core.applicant.service.*;
import com.soma.mfinance.core.asset.service.*;
import com.soma.mfinance.core.callcenter.CallCenterService;
import com.soma.mfinance.core.collection.service.*;
import com.soma.mfinance.core.contract.model.schedule.service.ContractScheduleService;
import com.soma.mfinance.core.contract.service.*;
import com.soma.mfinance.core.contract.service.aftersales.LossService;
import com.soma.mfinance.core.contract.service.cashflow.CashflowService;
import com.soma.mfinance.core.custom.loader.PropertyLoader;
import com.soma.mfinance.core.dealer.service.DealerService;
import com.soma.mfinance.core.document.service.DocumentService;
import com.soma.mfinance.core.financial.service.*;
import com.soma.mfinance.core.history.service.FinHistoryService;
import com.soma.mfinance.core.payment.service.FileIntegrationService;
import com.soma.mfinance.core.payment.service.LockSplitService;
import com.soma.mfinance.core.payment.service.PaymentAllocationService;
import com.soma.mfinance.core.payment.service.PaymentService;
import com.soma.mfinance.core.productlines.service.ProductLineService;
import com.soma.mfinance.core.quotation.QuotationService;
import com.soma.mfinance.core.shared.mplus.accounting.CashflowServiceMfp;
import com.soma.mfinance.core.shared.mplus.accounting.InstallmentServiceMfp;
import com.soma.mfinance.core.shared.mplus.accounting.PaymentServiceMfp;
import com.soma.mfinance.tools.report.service.ReportService;
import org.seuksa.frmk.tools.spring.SpringUtils;

/**
 *
 * @author kimsuor.seang
 *
 */
public interface FinServicesHelper extends ErsysCollabAppServicesHelper {
	OrganizationService ORG_SRV = SpringUtils.getBean(OrganizationService.class);
	OrgStructureService ORG_STRUC_SRV = SpringUtils.getBean(OrgStructureService.class);
	LossService LOSS_SRV = SpringUtils.getBean(LossService.class);
	QuotationService QUO_SRV = SpringUtils.getBean(QuotationService.class);
	ContractService CONT_SRV = SpringUtils.getBean(ContractService.class);
	ActivationContractService CONT_ACTIVATION_SRV = SpringUtils.getBean(ActivationContractService.class);
	DealerService DEA_SRV = SpringUtils.getBean(DealerService.class);
	FinancialProductService FIN_PROD_SRV = SpringUtils.getBean(FinancialProductService.class);
	AssetService ASS_SRV = SpringUtils.getBean(AssetService.class);
	DocumentService DOC_SRV = SpringUtils.getBean(DocumentService.class);
	ApplicantService APP_SRV = SpringUtils.getBean(ApplicantService.class);
	IndividualService INDIVI_SRV = SpringUtils.getBean(IndividualService.class);
	CompanyService COM_SRV = SpringUtils.getBean(CompanyService.class);
	LockSplitService LCK_SPL_SRV = SpringUtils.getBean(LockSplitService.class);
	FileIntegrationService FILE_INTEGRATION_SRV = SpringUtils.getBean(FileIntegrationService.class);
	ContractOtherDataService CON_OTH_SRV = SpringUtils.getBean(ContractOtherDataService.class);
	CollectionService COL_SRV = SpringUtils.getBean(CollectionService.class);
	UserInboxService INBOX_SRV = SpringUtils.getBean(UserInboxService.class);
	PaymentAllocationService PAYMENT_ALLOCATION_SRV = SpringUtils.getBean(PaymentAllocationService.class);
	CashflowService CASHFLOW_SRV = SpringUtils.getBean(CashflowService.class);
	NoteService NOTE_SRV = SpringUtils.getBean(NoteService.class);
	PaymentService PAYMENT_SRV = SpringUtils.getBean(PaymentService.class);
	FinanceCalculationService FIN_CAL_SRV = SpringUtils.getBean(FinanceCalculationService.class);
	CollectionActionService COL_ACT_SRV = SpringUtils.getBean(CollectionActionService.class);
	CallCenterService CALL_CTR_SRV = SpringUtils.getBean(CallCenterService.class);
	CampaignService CAM_SRV = SpringUtils.getBean(CampaignService.class);
	TransferApplicantService TRANSFERT_SRV = SpringUtils.getBean(TransferApplicantService.class);
	AddressService ADDRESS_SRV = SpringUtils.getBean(AddressService.class);
	AssetMakeService ASS_MAKE_SRV = SpringUtils.getBean(AssetMakeService.class);
	AssetRangeService ASS_RANGE_SRV = SpringUtils.getBean(AssetRangeService.class);
	AssetModelService ASS_MODEL_SRV = SpringUtils.getBean(AssetModelService.class);
	FinHistoryService FIN_HISTO_SRV = SpringUtils.getBean(FinHistoryService.class);
	ReminderService REMINDER_SRV = SpringUtils.getBean(ReminderService.class);
	SubsidyService SUBSIDY_SRV = SpringUtils.getBean(SubsidyService.class);
	CompensationService COMPENSATION_SRV = SpringUtils.getBean(CompensationService.class);
	ContractFlagService CON_FLAG_SRV = SpringUtils.getBean(ContractFlagService.class);
	LetterService LETTER_SRV = SpringUtils.getBean(LetterService.class);
	DriverInformationService DRIVER_SRV = SpringUtils.getBean(DriverInformationService.class);
	ReportService REPORT_SRV = SpringUtils.getBean(ReportService.class);
	//=============================moto4plus==============================================================================
	CashflowServiceMfp CASHFLOW_SERVICE_MFP = SpringUtils.getBean(CashflowServiceMfp.class);
	InstallmentServiceMfp INSTALLMENT_SERVICE_MFP = SpringUtils.getBean(InstallmentServiceMfp.class);
	PaymentServiceMfp PAYMENT_SERVICE_MFP = SpringUtils.getBean(PaymentServiceMfp.class);
	ActivationContractMfpService ACT_CON_MOTO_FOR_FLUS_SRV = SpringUtils.getBean(ActivationContractMfpService.class);
	AssetAppraisalService ASSET_APPRAISAL_SERVICE = SpringUtils.getBean(AssetAppraisalService.class);
	ContractScheduleService CSMS_SRV = SpringUtils.getBean(ContractScheduleService.class);
	ProductLineService  PRODUCT_LINE_SERVICE = SpringUtils.getBean(ProductLineService.class);
	PropertyLoader PROPERTY_LOADER_SRV = SpringUtils.getBean(PropertyLoader.class);


	AccountingService ACCOUNTING_SRV  = SpringUtils.getBean(AccountingService.class);
	CollectionIncentiveReportService COLLECTION_INCENTIVE_REPORT_SERVICE  = SpringUtils.getBean(CollectionIncentiveReportService.class);
}
