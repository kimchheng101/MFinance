package com.soma.mfinance.core.quotation.impl;

import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.quotation.SequenceGenerator;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/22/2017
 * Name  : 5:46 PM
 * Email : k.seang@gl-f.com
 */
public class SequenceGeneratorByDealerForMfpImpl implements SequenceGenerator {

    public static final String DEFAULT_PREFIX = "GLF";
    private Long sequence;
    private String prefix;
    private Dealer dealer;


    /**
     * @param dealer
     */
    public SequenceGeneratorByDealerForMfpImpl(String prefix, Dealer dealer, Long sequence) {
        this.prefix = prefix;
        this.dealer = dealer;
        this.sequence = sequence;
    }
    /**
     * @param dealer
     */
    public SequenceGeneratorByDealerForMfpImpl(Dealer dealer, Long sequence) {
        this.prefix = DEFAULT_PREFIX;
        this.dealer = dealer;
        this.sequence = sequence;
    }

    @Override
    public String generate() {
        String sequenceNumber = "70000000" + sequence;
        return prefix + "-" + dealer.getCode() + "-" + sequenceNumber.substring(sequenceNumber.length() - 8);
    }
}
