package com.soma.mfinance.core.asset.panel.appraisal.category;

import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.asset.model.appraisal.AppraisalCategory;
import com.soma.mfinance.core.asset.model.appraisal.EAppraisalRange;
import com.soma.mfinance.core.asset.model.appraisal.EAppraisalType;
import com.soma.mfinance.core.shared.component.ComponentLayoutFactory;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 
 * @author vi.sok
 *
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AppraisalCategoryFormPanel extends AbstractFormPanel {

	/** */
	private static final long serialVersionUID = -4276155516661087487L;

	private AppraisalCategory appraisalCategory;
	
	private CheckBox cbActive;
	private TextField txtCode;
    private TextField txtDescEn;
    private TextField txtSortIndex;
    private ERefDataComboBox<EAppraisalType> cbxEAppraisalType;
	private ERefDataComboBox<EAppraisalRange> cbxEappraisalRange;
    /**
     * 
     */
    @PostConstruct
	public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
		navigationPanel.addSaveClickListener(this);
	}
	
    /**
     * @see AbstractFormPanel#getEntity()
     */
	@Override
	protected Entity getEntity() {
		appraisalCategory.setCode(txtCode.getValue());
		appraisalCategory.setDesc(txtDescEn.getValue());
		appraisalCategory.setDescEn(txtDescEn.getValue());
		appraisalCategory.seteAppraisalType(cbxEAppraisalType.getSelectedEntity());
		appraisalCategory.setSortIndex(getInteger(txtSortIndex));
		appraisalCategory.setStatusRecord(cbActive.getValue() ? EStatusRecord.ACTIV : EStatusRecord.INACT);
		appraisalCategory.setAppraisalRange(cbxEappraisalRange.getSelectedEntity());
		return appraisalCategory;
	}

	/**
	 * @see AbstractFormPanel#createForm()
	 */
	@Override
	protected com.vaadin.ui.Component createForm() {
		txtSortIndex = ComponentFactory.getTextField("sortIndex", false, 60, 180);
		txtCode =  ComponentFactory.getTextField("code", true, 60, 180);
		txtDescEn = ComponentFactory.getTextField("name", true, 60, 180);
        cbActive = new CheckBox(I18N.message("active"));
        cbActive.setValue(true);

		cbxEAppraisalType = new ERefDataComboBox<>(EAppraisalType.class);
		cbxEAppraisalType.setCaption(I18N.message("appraisal.type"));
		cbxEAppraisalType.setRequired(true);

		cbxEappraisalRange = new ERefDataComboBox<>(EAppraisalRange.class);
		cbxEappraisalRange.setCaption(I18N.message("groupdetail.range"));
		cbxEappraisalRange.setRequired(true);

        FormLayout frmLayout = ComponentLayoutFactory.getFormLayoutCaptionAlignLeft();
        frmLayout.setMargin(new MarginInfo(false, false, false, true));
        frmLayout.addComponent(txtCode);
        frmLayout.addComponent(txtDescEn);
        frmLayout.addComponent(cbxEAppraisalType);
		frmLayout.addComponent(cbxEappraisalRange);
        frmLayout.addComponent(txtSortIndex);
        frmLayout.addComponent(cbActive);

        return frmLayout;
	}

	/**
	 * @param appraisalCatId
	 */
	public void assignValues(Long appraisalCatId) {
		super.reset();
		if (appraisalCatId != null) {
			appraisalCategory = ENTITY_SRV.getById(AppraisalCategory.class, appraisalCatId);
			txtCode.setValue(appraisalCategory.getCode());
			txtDescEn.setValue(appraisalCategory.getDescEn());
			cbxEAppraisalType.setSelectedEntity(appraisalCategory.geteAppraisalType());
			cbActive.setValue(appraisalCategory.getStatusRecord().equals(EStatusRecord.ACTIV));
			//cbxAssetRange.setValue(appraisalCategory.getAssetRange());
			cbxEappraisalRange.setSelectedEntity(appraisalCategory.getAppraisalRange());
			if(appraisalCategory.getSortIndex() != null){
				txtSortIndex.setValue(appraisalCategory.getSortIndex().toString());
			}
		}
	}

	/**
	 * @see AbstractFormPanel#reset()
	 */
	@Override
	public void reset() {
		super.reset();
		appraisalCategory = new AppraisalCategory();
		txtCode.setValue("");
		txtDescEn.setValue("");
		txtSortIndex.setValue("");
		cbxEAppraisalType.setSelectedEntity(null);
		cbxEappraisalRange.setSelectedEntity(null);
		cbActive.setValue(true);
	}

	/**
	 * @see AbstractFormPanel#validate()
	 */
	@Override
	protected boolean validate() {
		checkMandatoryField(txtCode, "code");		
		checkMandatoryField(txtDescEn, "name.en");		
		checkMandatorySelectField(cbxEAppraisalType, "appraisal.type");
		checkMandatorySelectField(cbxEappraisalRange,"groupdetail.range");
		return errors.isEmpty();
	}
}
