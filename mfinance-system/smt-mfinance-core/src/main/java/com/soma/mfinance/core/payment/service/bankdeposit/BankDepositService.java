package com.soma.mfinance.core.payment.service.bankdeposit;


import java.util.Date;

import org.seuksa.frmk.service.BaseEntityService;

import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.payment.model.BankDeposit;
import com.soma.mfinance.core.payment.model.BankDepositReceivedFromDealer;

/**
 * Bank Deposit Service
 * @author meng.kim
 *
 */
public interface BankDepositService extends BaseEntityService  {

	/**
	 * Get Bank Deposit Amount Received From Dealer
	 * @param dealer
	 * @param requestDate
	 * @return
	 */
	BankDepositReceivedFromDealer getBankDepositReceivedFromDealer(Dealer dealer, Date requestDate);
	BankDeposit getBankDepositByDealerAndRequestDate(Dealer dealer, Date requestDate);
	
}
