package com.soma.mfinance.core.financial.model;

/**
 * Meta data of com.soma.mfinance.core.financial.model.Term
 * @author kimsuor.seang
 */
public interface MTerm {
	
	public final static String VALUE = "value";

}
