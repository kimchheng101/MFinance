package com.soma.mfinance.glf.report.xls;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.quotation.QuotationService;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationDocument;
import com.soma.mfinance.core.shared.conf.AppConfig;
import com.soma.mfinance.core.shared.report.ReportParameter;
import com.soma.mfinance.tools.report.Report;
import com.soma.ersys.core.hr.model.address.Address;
import com.soma.ersys.core.hr.model.eref.EGender;
import com.soma.ersys.core.hr.model.eref.ENationality;
import com.soma.frmk.security.context.SecApplicationContextHolder;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.commons.lang.StringUtils;
import org.joda.time.Years;
import org.seuksa.frmk.tools.DateUtils;

import java.util.*;

/**
 * Created by Dang Dim
 * Date     : 20-Jul-17, 1:03 PM
 * Email    : d.dim@gl-f.com
 */
public class OwnershipTransference implements Report{
    protected QuotationService quotationService = (QuotationService) SecApplicationContextHolder.getContext().getBean("quotationService");
    public static String NA = "N/A";

    @Override
    public String generate(ReportParameter reportParameter) throws Exception {
        Long quotaId = (Long) reportParameter.getParameters().get("quotaId");

        Quotation quotation = quotationService.getById(Quotation.class, quotaId);
        Applicant applicant = quotation.getApplicant();
        Asset asset = quotation.getAsset();

        Map<String, String> beans = new HashMap<String, String>();

        beans.put("lesseeName", applicant.getIndividual().getLastName() + " " + applicant.getIndividual().getFirstName());
        beans.put("sex", getGenderDesc(applicant.getIndividual().getGender().getCode()));
        beans.put("dateOfBirth", DateUtils.getDateLabel(applicant.getIndividual().getBirthDate(), DateUtils.FORMAT_DDMMYYYY_SLASH));
        beans.put("age", getAgeByBirthDate(applicant.getIndividual().getBirthDate()));
        beans.put("nation", getNation(applicant.getIndividual().getNationality().getCode()));

        QuotationDocument idCardDocument = getDocument("N", quotation.getQuotationDocuments());
        beans.put("idNumber", idCardDocument != null ? getDefaultString(idCardDocument.getReference()) : NA);
        beans.put("idDay", idCardDocument != null ? String.valueOf(DateUtils.getDay(idCardDocument.getIssueDate())) : NA);
        beans.put("idMonth", idCardDocument != null ? String.valueOf(DateUtils.getMonth(idCardDocument.getIssueDate())) : NA);
        beans.put("idYear", idCardDocument != null ? String.valueOf(DateUtils.getYear(idCardDocument.getIssueDate())) : NA);

        Address address = applicant.getIndividual().getMainAddress();
        if (address != null) {
            beans.put("houseNo", StringUtils.isNotEmpty(address.getHouseNo()) ? address.getHouseNo() : NA);
            beans.put("streetNo", StringUtils.isNotEmpty(address.getStreet()) ? address.getStreet() : NA);
            beans.put("village", address.getVillage() != null ? address.getVillage().getDesc() : NA);
            beans.put("commune", address.getCommune() != null ? address.getCommune().getDesc() : NA);
            beans.put("district", address.getDistrict() != null ? address.getDistrict().getDesc() : NA);
            beans.put("province", address.getProvince() != null ? address.getProvince().getDesc() :  NA);
        } else {
            beans.put("houseNo", NA);
            beans.put("streetNo", NA);
            beans.put("village", NA);
            beans.put("commune", NA);
            beans.put("district", NA);
            beans.put("province", NA);
        }

        Date contractStartDate = quotation.getContractStartDate();
        beans.put("contractReference", quotation.getReference());
        beans.put("contractDate", (contractStartDate == null ? "" : DateUtils.getDateLabel(contractStartDate, DateUtils.FORMAT_DDMMYYYY_SLASH)));

        Date endDate = DateUtils.addMonthsDate(contractStartDate, quotation.getTerm());
        Date contracEndDate = DateUtils.addDaysDate(endDate, -1);
        beans.put("startDateContract", DateUtils.getDateLabel(contractStartDate, DateUtils.FORMAT_DDMMYYYY_SLASH));
        beans.put("endDateContract", DateUtils.getDateLabel(contracEndDate, DateUtils.FORMAT_DDMMYYYY_SLASH));

        beans.put("model", asset.getModel().getDescEn());
        beans.put("color", asset.getColor().getDesc());
        beans.put("type", getGenderDesc(asset.getAssetGender().getCode()));
        beans.put("registrationDate", DateUtils.getDateLabel(asset.getRegistrationDate(), DateUtils.FORMAT_DDMMYYYY_SLASH));

        beans.put("numberOfCC", getDefaultString(asset.getEngine().getDescEn()));
        beans.put("chassisNumber", asset.getChassisNumber());
        beans.put("engineNumber", asset.getEngineNumber());
        beans.put("plateNumber", asset.getPlateNumber());

        Date printDate = new Date();
        beans.put("day", String.valueOf(DateUtils.getDay(printDate)) );
        beans.put("month", String.valueOf(DateUtils.getMonth(printDate)));
        beans.put("year", String.valueOf(DateUtils.getYear(printDate)));

        String templatePath = AppConfig.getInstance().getConfiguration().getString("specific.templatedir");
        String templateFileName = templatePath + "/OwnershipTransference.xlsx";
        String outputPath = AppConfig.getInstance().getConfiguration().getString("specific.tmpdir");

        String prefixOutputName = "OwnershipTransference";
        String sufixOutputName = "xlsx";
        String uuid = UUID.randomUUID().toString().replace("-", "");
        String xlsFileName = outputPath + "/" + prefixOutputName + uuid + "." + sufixOutputName;

        XLSTransformer transformer = new XLSTransformer();
        transformer.transformXLS(templateFileName, beans, xlsFileName);

        return prefixOutputName + uuid + "." + sufixOutputName;
    }

    private QuotationDocument getDocument(String documCode, List<QuotationDocument> documents) {
        for (QuotationDocument document : documents) {
            if (document.getDocument().getApplicantType().equals(EApplicantType.C)
                    && documCode.equals(document.getDocument().getCode())) {
                return document;
            }
        }
        return null;
    }

    private String getAgeByBirthDate(Date birthDate) {
        long timestamp = birthDate.getTime();
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);
        Integer y = cal.get(Calendar.YEAR);
        Integer moy = cal.get(Calendar.MONTH) + 1;
        Integer dom = cal.get(Calendar.DAY_OF_MONTH);
        org.joda.time.LocalDate birthdate = new org.joda.time.LocalDate(y, moy, dom);
        Years age = Years.yearsBetween(birthdate, org.joda.time.LocalDate.now());
        return String.format("%s", age.getYears());
    }

    private String getDefaultString(String value) {
        return StringUtils.defaultString(value);
    }

    /*retrurn code*/
    private String getGenderDesc(String code) {
        if (EGender.M.getCode().equals(code)) {
            return "ប្រុស";
        } else if (EGender.F.getCode().equals(code)) {
            return "ស្រី";
        } else if (EGender.U.getCode().equals(code)) {
            return "មិនដឹង";
        }
        return "";
    }
    private String getNation(String code){
        if (ENationality.KHMER.getCode().equals(code)){
            return "ខ្មែរ";
        }
        return "";
    }
}
