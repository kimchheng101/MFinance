package com.soma.mfinance.core.asset.panel;

import com.soma.ersys.core.hr.model.eref.EColor;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.soma.frmk.vaadin.ui.widget.toolbar.event.SaveClickListener;
import com.soma.mfinance.core.asset.model.*;
import com.soma.mfinance.core.asset.panel.appraisal.AppraisalConstant;
import com.soma.mfinance.core.auction.model.AssetPrice;
import com.soma.mfinance.core.custom.component.CustomEntityRefComboBox;
import com.soma.mfinance.core.document.panel.DisplayDocumentPanel;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.asset.AssetEntityField;
import com.soma.mfinance.core.shared.financialproduct.FinancialProductEntityField;
import com.soma.mfinance.core.shared.quotation.QuotationProfileUtils;
import com.soma.mfinance.core.shared.system.AmountBigUtils;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.vaadin.data.Property;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.Runo;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.tools.DateUtils;
import org.vaadin.dialogs.ConfirmDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by cheasocheat on 1/27/17.
 */

public class AssetPanel extends AbstractTabPanel implements SaveClickListener, FinServicesHelper, AssetEntityField, Property.ValueChangeListener, AppraisalConstant, Button.ClickListener {

    private Label lblAssetRange;
    private Label lblAssetModel;
    private Label lblEngine;
    private Label lblAssetGender;
    private Label lblAssetYear;
    private Label lblAssetColor;

    private EntityRefComboBox<AssetRange> cbxAssetRange;
    protected EntityRefComboBox<AssetModel> cbxAssetModel;
    private EntityRefComboBox<EEngine> cbxEngine;
    private ERefDataComboBox<EAssetGender> cbxAssetGender;
    private ERefDataComboBox<EAssetYear> cbxYear;
    private ERefDataComboBox<EColor> cbxColor;

    private TextField txtChassisNumber;
    private TextField txtEngineNumber;
    private TextField txtPlateNumber;

    private DateField dfRegistrationDate;

    /*---------------Document-----------*/
    private TextField txtChassisNumberDc;
    private TextField txtEngineNumberDc;
    private TextField txtPlateNumberDc;
    private DateField dfRegistrationDateDc;
    private GridLayout assetDetailLayout;

    private FormLayout assetChassisLayout;
    //Change asset model
    protected long assetModelId;
    private HorizontalLayout horizontalLayout;
    private Panel dcFormLayout3Panel;
    private Quotation quotation;
    private AssetTabPanel assetTabPanel;
    private Map<String, String> hashError;

    private boolean isConfirm;

    private FileAssetUploader dielselUploaderWarranyCard;
    private Upload uploadDieselWarrantyCard;

    private GridLayout diselEngineForm;
    private Panel diselEnginePanel;

    private Upload uploadVehicleDocument;
    private FileAssetUploader warranyPowerTillUploader;
    private Upload uploadPhotoEngine;

    private Label lblAssetPrice;
    private TextField txtAssetModePrices;
    private TextField txtCurrentOdometer;
    private DateField dtOfImport;
    private DateField dtOfPurchase;

    private EntityRefComboBox<AssetComponent> cbxAssetComponent;
    private EntityRefComboBox<AssetComponent> cbxAssetComponent2;
    private EntityRefComboBox<AssetMake> cbxAssetBrand;
    private EntityRefComboBox<AssetMake> cbxAssetBrand2;
    private EntityRefComboBox<EPowerTillerModel> cbxAssetModel2;
    private ERefDataComboBox<EAssetYear> cbxYear2;
    private ERefDataComboBox<EColor> cbxColor2;

    private TextField txtUnitPrice;
    private TextField txtUnitPrice2;

    private Label lblAssetCommponent;
    private Label lblAssetBrand;
    private Label lblUnitPrice;
    private FormLayout assetMainForm;
    private Panel enginePanel;

    private Label lblCurrentOdometer, lblDateImport, lblPurchaseDate;

    private Panel powerTillerFormLayoutPanel;
    private TextField txtChassisNumber2;
    private GridLayout powerTillGridLayout;
    private Panel powerTillerPanel;
    private Upload uploadScratchPaperTiller;

    private Button bntScratPaperOfDieselEngine;
    private Button btnWarrantDieselAskWalkingTructor;

    private Button bntScatchPaperTiller;
    private Button btnWarrantyTiller;

    private Button btnVehicleDocument;
    private Button btnPhotoOfEngine;
    private Button btnWarrantyDielsel;

    private FileAssetUploader scratchPowerTill;
    private FileAssetUploader warrantyDieselAsWalkingTructor;

    private FileAssetUploader VehicleDocument;
    private Upload dieselUpload;
    private Upload UploadWarrantyTiller;
    private Upload uploadScratchPaperDieselEngine;
    private FileAssetUploader scratchPaperDieselEngine;
    private FileAssetUploader photoOfEngine;
    private Label lblReceiptScrate;
    private Label lblVihicleDocument;
    private Label lblPhotoEngine;

    private DisplayDocumentPanel displayDocumentPanel;
    private Asset asset;

    public AssetPanel() {
        super();
        this.setSizeFull();
    }

    @Override
    protected Component createForm() {
        asset  = new Asset();
        lblAssetRange = new Label("Product categories <span style=\"color:red;\">&nbsp * &nbsp</span>");
        lblAssetRange.setContentMode(ContentMode.HTML);
        cbxAssetRange = new CustomEntityRefComboBox<>();
        cbxAssetRange.setSelectedEntity(null);
        cbxAssetRange.addValueChangeListener(this);
        cbxAssetRange.setImmediate(true);

        lblAssetModel = new Label("Asset Model<span style=\"color:red;\">&nbsp*</span>");
        lblAssetModel.setContentMode(ContentMode.HTML);
        cbxAssetModel = new CustomEntityRefComboBox<>();
        cbxAssetModel.setSelectedEntity(null);
        cbxAssetModel.setImmediate(true);
        cbxAssetModel.addValueChangeListener(this);
        ((CustomEntityRefComboBox) cbxAssetRange).setFilteringComboBox(cbxAssetModel, AssetModel.class);

        lblAssetColor = new Label("Color<span style=\"color:red;\">&nbsp*</span>");
        lblAssetColor.setContentMode(ContentMode.HTML);
        cbxColor = new ERefDataComboBox<EColor>(EColor.class);
        cbxColor.setSelectedEntity(null);
        cbxColor.setImmediate(true);

        lblAssetYear = new Label("Asset Year<span style=\"color:red;\">&nbsp*</span>");
        lblAssetYear.setContentMode(ContentMode.HTML);
        cbxYear = new ERefDataComboBox<>(EAssetYear.values());
        cbxYear.setImmediate(true);
        cbxYear.setVisible(true);
        cbxYear.setWidth("200px");

        lblAssetGender = new Label("Types <span style=\"color:red;\">&nbsp*</span>");
        lblAssetGender.setContentMode(ContentMode.HTML);
        cbxAssetGender = new ERefDataComboBox<>(EAssetGender.values());
        cbxAssetGender.setImmediate(true);
        cbxAssetGender.setWidth("200px");

        lblEngine = new Label("Engine/HP <span style=\"color:red;\">&nbsp*</span>");
        lblEngine.setContentMode(ContentMode.HTML);

        cbxEngine = new CustomEntityRefComboBox(EEngine.values());
        cbxEngine.setImmediate(true);
        cbxEngine.setWidth("200px");

        txtChassisNumber = ComponentFactory.getTextField(I18N.message("chassis.number"), true, 50, 220);
        txtEngineNumber = ComponentFactory.getTextField(I18N.message("engine.number"), true, 50, 220);
        txtPlateNumber = ComponentFactory.getTextField(I18N.message("plate.number"), false, 50, 220);
        dfRegistrationDate = ComponentFactory.getAutoDateField(I18N.message("registration.date"), false);

        txtChassisNumber2 = ComponentFactory.getTextField(I18N.message("chassis.serial.number"), false, 50, 220);
        //dc
        txtChassisNumberDc = ComponentFactory.getTextField(I18N.message("chassis.number"), true, 50, 220);
        txtEngineNumberDc = ComponentFactory.getTextField(I18N.message("engine.number"), true, 50, 220);
        txtPlateNumberDc = ComponentFactory.getTextField(I18N.message("plate.number"), false, 50, 220);
        dfRegistrationDateDc = ComponentFactory.getAutoDateField(I18N.message("registration.date"), false);

        // Kubuta text fields
        lblAssetPrice = new Label("Asset Price <span style=\"color:red;\">&nbsp*</span>");
        lblAssetPrice.setContentMode(ContentMode.HTML);
        txtAssetModePrices = ComponentFactory.getTextField(50, 200);

        lblCurrentOdometer = new Label("Current Odometer <span style=\"color:red;\">&nbsp*</span>");
        lblCurrentOdometer.setContentMode(ContentMode.HTML);
        txtCurrentOdometer = ComponentFactory.getTextField(50, 200);

        lblDateImport = new Label("Date of Import <span style=\"color:red;\">&nbsp*</span>");
        lblDateImport.setContentMode(ContentMode.HTML);
        dtOfImport = ComponentFactory.getAutoDateField();

        lblPurchaseDate = new Label(I18N.message("date.purchase"));
        dtOfPurchase = ComponentFactory.getAutoDateField();

        // working trucktor
        lblAssetCommponent = new Label("Asset Component <span style=\"color:red;\">&nbsp*</span>");
        lblAssetCommponent.setContentMode(ContentMode.HTML);
        cbxAssetComponent = new EntityRefComboBox<AssetComponent>(I18N.message(""));
        cbxAssetComponent.setRestrictions(new BaseRestrictions<>(AssetComponent.class));
        cbxAssetComponent.renderer();
        cbxAssetComponent.setImmediate(true);

        cbxAssetComponent2 = new EntityRefComboBox<AssetComponent>(I18N.message(""));
        cbxAssetComponent2.setRestrictions(new BaseRestrictions<>(AssetComponent.class));
        cbxAssetComponent2.renderer();
        cbxAssetComponent2.setImmediate(true);

        cbxAssetModel2 = new EntityRefComboBox<>(I18N.message(""), EPowerTillerModel.values());

        lblAssetBrand = new Label("Asset Brand <span style=\"color:red;\">&nbsp*</span>");
        lblAssetBrand.setContentMode(ContentMode.HTML);

        cbxAssetBrand = new EntityRefComboBox<AssetMake>(I18N.message(""));
        cbxAssetBrand.setRestrictions(new BaseRestrictions<AssetMake>(AssetMake.class));
        cbxAssetBrand.renderer();
        cbxAssetBrand.setSelectedEntity(null);
        cbxAssetBrand.setImmediate(true);

        cbxAssetBrand2 = new EntityRefComboBox<AssetMake>(I18N.message(""));
        cbxAssetBrand2.setRestrictions(new BaseRestrictions<AssetMake>(AssetMake.class));
        cbxAssetBrand2.renderer();
        cbxAssetBrand2.setSelectedEntity(null);
        cbxAssetBrand2.setImmediate(true);

        cbxColor2 = new ERefDataComboBox<EColor>(EColor.class);
        cbxColor2.setCaption(I18N.message(""));
        cbxColor.setSelectedEntity(null);
        cbxColor.setImmediate(true);

        cbxYear2 = new ERefDataComboBox<>(EAssetYear.values());
        cbxYear2.setImmediate(true);
        cbxYear2.setVisible(true);
        cbxYear2.setWidth("200px");

        lblUnitPrice = new Label(I18N.message("asset.unitprice"));
        txtUnitPrice = ComponentFactory.getTextField(50, 200);

        txtUnitPrice2 = ComponentFactory.getTextField(50, 200);

        //Kubuta upload file
        btnVehicleDocument = new Button();
        VehicleDocument = new FileAssetUploader(this, btnVehicleDocument, new FileAssetUploader.FileUploadHandler() {
            @Override
            public void onSuccess(File file, String path, Button button) {
                if (path != null || !path.isEmpty()) {
                    asset.setVehicleDocumentDiesel(path);
                } else {
                    messageEmpty();
                }
            }

            @Override
            public void onFailed(String message) {
                uploadFail();
            }
        });
        btnVehicleDocument.setIcon(new ThemeResource("../smt-default/icons/16/pdf.png"));
        btnVehicleDocument.setVisible(false);
        btnVehicleDocument.setStyleName(Runo.BUTTON_LINK);
        btnVehicleDocument.addClickListener(this);

        uploadVehicleDocument = new Upload();
        uploadVehicleDocument.setReceiver(VehicleDocument);
        uploadVehicleDocument.addSucceededListener(VehicleDocument);
        uploadVehicleDocument.setButtonCaption(I18N.message("upload"));

        btnWarrantyDielsel = new Button();
        dielselUploaderWarranyCard = new FileAssetUploader(this, btnWarrantyDielsel, new FileAssetUploader.FileUploadHandler() {
            @Override
            public void onSuccess(File file, String path, Button button) {
                asset.setWarrantyDiesel(path);
            }

            @Override
            public void onFailed(String message) {
                uploadFail();
            }
        });
        btnWarrantyDielsel.setIcon(new ThemeResource("../smt-default/icons/16/pdf.png"));
        btnWarrantyDielsel.setVisible(false);
        btnWarrantyDielsel.setStyleName(Runo.BUTTON_LINK);
        btnWarrantyDielsel.addClickListener(this);

        uploadDieselWarrantyCard = new Upload();
        uploadDieselWarrantyCard.setReceiver(dielselUploaderWarranyCard);
        uploadDieselWarrantyCard.addSucceededListener(dielselUploaderWarranyCard);
        uploadDieselWarrantyCard.setButtonCaption(I18N.message("upload"));

        btnPhotoOfEngine = new Button();
        photoOfEngine = new FileAssetUploader(this, btnPhotoOfEngine, new FileAssetUploader.FileUploadHandler() {
            @Override
            public void onSuccess(File file, String path, Button button) {
                asset.setPhotoDieseEngine(path);
            }

            @Override
            public void onFailed(String message) {
                uploadFail();
            }
        });
        btnPhotoOfEngine.setIcon(new ThemeResource("../smt-default/icons/16/pdf.png"));
        btnPhotoOfEngine.setVisible(false);
        btnPhotoOfEngine.setStyleName(Runo.BUTTON_LINK);
        btnPhotoOfEngine.addClickListener(this);

        uploadPhotoEngine = new Upload();
        uploadPhotoEngine.setReceiver(photoOfEngine);
        uploadPhotoEngine.addSucceededListener(photoOfEngine);
        uploadPhotoEngine.setButtonCaption(I18N.message("upload"));

        bntScatchPaperTiller = new Button();
        scratchPowerTill = new FileAssetUploader(this, bntScatchPaperTiller, new FileAssetUploader.FileUploadHandler() {
            @Override
            public void onSuccess(File file, String path, Button button) {
                asset.setPowerTillerReceipt(path);
            }

            @Override
            public void onFailed(String message) {
                uploadFail();
            }
        });
        bntScatchPaperTiller.setIcon(new ThemeResource("../smt-default/icons/16/pdf.png"));
        bntScatchPaperTiller.setVisible(false);
        bntScatchPaperTiller.setStyleName(Runo.BUTTON_LINK);
        bntScatchPaperTiller.addClickListener(this);

        btnWarrantyTiller = new Button();
        warranyPowerTillUploader = new FileAssetUploader(this, btnWarrantyTiller, new FileAssetUploader.FileUploadHandler() {
            @Override
            public void onSuccess(File file, String path, Button button) {
                asset.setWarrantyPowerTiller(path);
            }

            @Override
            public void onFailed(String message) {
                uploadFail();
            }
        });
        btnWarrantyTiller.setIcon(new ThemeResource("../smt-default/icons/16/pdf.png"));
        btnWarrantyTiller.setVisible(false);
        btnWarrantyTiller.setStyleName(Runo.BUTTON_LINK);
        btnWarrantyTiller.addClickListener(this);

        //--------------- diesel --------------controls---------start
        btnWarrantDieselAskWalkingTructor = new Button();
        warrantyDieselAsWalkingTructor = new FileAssetUploader(this, btnWarrantDieselAskWalkingTructor, new FileAssetUploader.FileUploadHandler() {
            @Override
            public void onSuccess(File file, String path, Button button) {
                asset.setWarrantyDiesel(path);
            }

            @Override
            public void onFailed(String message) {
                uploadFail();
            }
        });
        btnWarrantDieselAskWalkingTructor.setIcon(new ThemeResource("../smt-default/icons/16/pdf.png"));
        btnWarrantDieselAskWalkingTructor.setVisible(false);
        btnWarrantDieselAskWalkingTructor.setStyleName(Runo.BUTTON_LINK);
        btnWarrantDieselAskWalkingTructor.addClickListener(this);

        dieselUpload = new Upload();
        dieselUpload.setReceiver(warrantyDieselAsWalkingTructor);
        dieselUpload.addSucceededListener(warrantyDieselAsWalkingTructor);
        dieselUpload.setButtonCaption("Upload");

        bntScratPaperOfDieselEngine = new Button();
        bntScratPaperOfDieselEngine.setIcon(new ThemeResource("../smt-default/icons/16/pdf.png"));
        bntScratPaperOfDieselEngine.setVisible(false);
        bntScratPaperOfDieselEngine.setStyleName(Runo.BUTTON_LINK);
        bntScratPaperOfDieselEngine.addClickListener(this);

        uploadScratchPaperDieselEngine = new Upload();
        scratchPaperDieselEngine = new FileAssetUploader(this, bntScratPaperOfDieselEngine, new FileAssetUploader.FileUploadHandler() {
            @Override
            public void onSuccess(File file, String path, Button button) {
                asset.setScrathDieselEngine(path);
            }

            @Override
            public void onFailed(String message) {
                uploadFail();
            }
        });
        uploadScratchPaperDieselEngine.setReceiver(scratchPaperDieselEngine);
        uploadScratchPaperDieselEngine.addSucceededListener(scratchPaperDieselEngine);
        uploadScratchPaperDieselEngine.setButtonCaption("Upload");

        //--------------- diesel --------------controls---------end
        uploadScratchPaperTiller = new Upload();
        uploadScratchPaperTiller.setReceiver(scratchPowerTill);
        uploadScratchPaperTiller.addSucceededListener(scratchPowerTill);
        uploadScratchPaperTiller.setButtonCaption(I18N.message("upload"));

        UploadWarrantyTiller = new Upload();
        UploadWarrantyTiller.setReceiver(warranyPowerTillUploader);
        UploadWarrantyTiller.addSucceededListener(warranyPowerTillUploader);
        UploadWarrantyTiller.setButtonCaption(I18N.message("upload"));

        diselEngineForm = new GridLayout(3, 4);
        diselEngineForm.setSpacing(true);
        diselEngineForm.setMargin(true);
        diselEnginePanel = new Panel();
        diselEnginePanel.setStyleName("enginePanel");

        lblVihicleDocument = new Label(I18N.message("vehicle.document"));
        diselEngineForm.addComponent(lblVihicleDocument, 0, 0);
        diselEngineForm.addComponent(uploadVehicleDocument, 1, 0);
        diselEngineForm.addComponent(btnVehicleDocument, 2, 0);

        lblPhotoEngine = new Label(I18N.message("photo.of.engine"));
        diselEngineForm.addComponent(lblPhotoEngine, 0, 1);
        diselEngineForm.addComponent(uploadPhotoEngine, 1, 1);
        diselEngineForm.addComponent(btnPhotoOfEngine, 2, 1);

        lblReceiptScrate = new Label(I18N.message("receipt.scratch.paper"));
        diselEngineForm.addComponent(lblReceiptScrate, 0, 2);
        diselEngineForm.addComponent(uploadScratchPaperDieselEngine, 1, 2);
        diselEngineForm.addComponent(bntScratPaperOfDieselEngine, 2, 2);

        diselEngineForm.addComponent(new Label(I18N.message("warranty.card")), 0, 3);
        diselEngineForm.addComponent(uploadDieselWarrantyCard, 1, 3);
        diselEngineForm.addComponent(btnWarrantyDielsel, 2, 3);


        powerTillerPanel = new Panel();
        powerTillerPanel.setStyleName("enginePanel");
        powerTillGridLayout = new GridLayout(3, 2);
        powerTillGridLayout.setMargin(true);
        powerTillGridLayout.setSpacing(true);

        powerTillGridLayout.addComponent(new Label(I18N.message("receipt.scratch.paper")), 0, 0);
        powerTillGridLayout.addComponent(uploadScratchPaperTiller, 1, 0);
        powerTillGridLayout.addComponent(bntScatchPaperTiller, 2, 0);

        powerTillGridLayout.addComponent(new Label(I18N.message("warranty.card")), 0, 1);
        powerTillGridLayout.addComponent(UploadWarrantyTiller, 1, 1);
        powerTillGridLayout.addComponent(btnWarrantyTiller, 2, 1);

        diselEnginePanel.setContent(diselEngineForm);
        powerTillerPanel.setContent(powerTillGridLayout);

        // Asset item detail-------- left ------------ layout -----

        assetDetailLayout = new GridLayout(3, 12);
        assetDetailLayout.setStyleName("setAssetRange");
        assetDetailLayout.setMargin(true);

        assetDetailLayout.addComponent(lblAssetRange);
        assetDetailLayout.addComponent(cbxAssetRange);
        assetDetailLayout.addComponent(new Label(""));

        assetDetailLayout.addComponent(lblAssetCommponent);
        assetDetailLayout.addComponent(cbxAssetComponent);
        assetDetailLayout.addComponent(cbxAssetComponent2);
        lblAssetCommponent.setStyleName("assetmodel-control");
        cbxAssetComponent2.setStyleName("assetmodel-control-margin-lef2");

        assetDetailLayout.addComponent(lblAssetBrand);
        assetDetailLayout.addComponent(cbxAssetBrand);
        assetDetailLayout.addComponent(cbxAssetBrand2);
        lblAssetBrand.setStyleName("assetmodel-control");
        cbxAssetBrand2.setStyleName("assetmodel-control-margin-lef2");

        assetDetailLayout.addComponent(lblAssetModel);
        assetDetailLayout.addComponent(cbxAssetModel);
        assetDetailLayout.addComponent(cbxAssetModel2);
        cbxAssetModel.setStyleName("assetmodel-control");
        lblAssetModel.setStyleName("assetmodel-control");
        cbxAssetModel2.setStyleName("assetmodel-control-margin-lef2");

        assetDetailLayout.addComponent(lblAssetColor);
        assetDetailLayout.addComponent(cbxColor);
        assetDetailLayout.addComponent(cbxColor2);
        lblAssetColor.setStyleName("assetmodel-control");
        cbxColor.setStyleName("assetmodel-control");
        cbxColor2.setStyleName("assetmodel-control-margin-lef2");

        assetDetailLayout.addComponent(lblAssetYear);
        assetDetailLayout.addComponent(cbxYear);
        assetDetailLayout.addComponent(cbxYear2);
        lblAssetYear.setStyleName("assetmodel-control");
        cbxYear.setStyleName("assetmodel-control");
        cbxYear2.setStyleName("assetmodel-control");
        cbxYear2.setStyleName("assetmodel-control-both");

        assetDetailLayout.addComponent(lblEngine);
        assetDetailLayout.addComponent(cbxEngine);
        assetDetailLayout.addComponent(new Label(""));
        cbxEngine.setStyleName("assetmodel-control");
        lblEngine.setStyleName("assetmodel-control");

        assetDetailLayout.addComponent(lblAssetGender);
        assetDetailLayout.addComponent(cbxAssetGender);
        assetDetailLayout.addComponent(new Label(""));
        lblAssetGender.setStyleName("assetmodel-control");
        cbxAssetGender.setStyleName("assetmodel-control");

        assetDetailLayout.addComponent(lblAssetPrice);
        assetDetailLayout.addComponent(txtAssetModePrices);
        assetDetailLayout.addComponent(new Label(""));
        lblAssetPrice.setStyleName("assetmodel-control");
        txtAssetModePrices.setStyleName("assetmodel-control");

        assetDetailLayout.addComponent(lblUnitPrice);
        assetDetailLayout.addComponent(txtUnitPrice);
        assetDetailLayout.addComponent(txtUnitPrice2);
        lblUnitPrice.setStyleName("assetmodel-control");
        txtUnitPrice.setStyleName("assetmodel-control");
        txtUnitPrice2.setStyleName("assetmodel-control");
        txtUnitPrice2.setStyleName("assetmodel-control-both");

        assetDetailLayout.addComponent(lblCurrentOdometer);
        assetDetailLayout.addComponent(txtCurrentOdometer);
        assetDetailLayout.addComponent(new Label(""));
        lblCurrentOdometer.setStyleName("assetmodel-control");
        txtCurrentOdometer.setStyleName("assetmodel-control");

        assetDetailLayout.addComponent(lblDateImport);
        assetDetailLayout.addComponent(dtOfImport);
        assetDetailLayout.addComponent(new Label(""));
        lblDateImport.setStyleName("assetmodel-control");
        dtOfImport.setStyleName("assetmodel-control");

        assetDetailLayout.addComponent(lblPurchaseDate);
        assetDetailLayout.addComponent(dtOfPurchase);
        assetDetailLayout.addComponent(new Label(""));
        lblPurchaseDate.setStyleName("assetmodel-control");
        dtOfPurchase.setStyleName("assetmodel-control");

        FormLayout assetChassisLayout2 = new FormLayout();
        assetChassisLayout2.setMargin(true);
        assetChassisLayout2.addComponent(txtChassisNumber2);

        // Asset item detail-------- left ------------ layout -----end

        assetChassisLayout = new FormLayout();
        assetChassisLayout.addComponent(txtChassisNumber);
        assetChassisLayout.addComponent(txtEngineNumber);
        assetChassisLayout.addComponent(txtPlateNumber);
        assetChassisLayout.addComponent(dfRegistrationDate);

        //Document Validate
        FormLayout formLayoutDc = new FormLayout();
        formLayoutDc.setMargin(true);
        formLayoutDc.addComponent(txtChassisNumberDc);
        formLayoutDc.addComponent(txtEngineNumberDc);
        formLayoutDc.addComponent(txtPlateNumberDc);
        formLayoutDc.addComponent(dfRegistrationDateDc);

        enginePanel = new Panel("Diesel Engine");
        enginePanel.setContent(assetChassisLayout);
        enginePanel.setStyleName("enginePanel");

        powerTillerFormLayoutPanel = new Panel(I18N.message("power.tiller"));
        powerTillerFormLayoutPanel.setContent(assetChassisLayout2);
        powerTillerFormLayoutPanel.setStyleName("enginePanel");

        dcFormLayout3Panel = new Panel(I18N.message("document.controller"));
        dcFormLayout3Panel.setContent(formLayoutDc);

        // Main painel for asset middle
        assetMainForm = new FormLayout();
        assetMainForm.addComponent(enginePanel);
        assetMainForm.addComponent(diselEnginePanel);
        assetMainForm.addComponent(powerTillerFormLayoutPanel);
        assetMainForm.addComponent(powerTillerPanel);

        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(ComponentFactory.getSpaceLayout(0, Unit.PIXELS));
        horizontalLayout.addComponent(assetDetailLayout);

        horizontalLayout.addComponent(ComponentFactory.getSpaceLayout(0, Unit.PIXELS));
        horizontalLayout.addComponent(assetMainForm);

        final Panel assetPanel = new Panel(I18N.message("asset"));
        assetPanel.setContent(horizontalLayout);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        verticalLayout.addComponent(assetPanel);
        return verticalLayout;
    }

    public void reset() {
        super.reset();
        cbxAssetRange.setSelectedEntity(null);
        cbxAssetModel.setSelectedEntity(null);
        cbxColor.setSelectedEntity(null);
        cbxYear.setSelectedEntity(null);
        cbxEngine.setSelectedEntity(null);
        cbxAssetGender.setSelectedEntity(null);
        txtChassisNumber.setValue("");
        txtEngineNumber.setValue("");
        txtPlateNumber.setValue("");
        dfRegistrationDate.setValue(null);

        cbxAssetModel2.setSelectedEntity(null);
        cbxColor2.setSelectedEntity(null);
        cbxYear2.setSelectedEntity(null);
        cbxAssetComponent.setSelectedEntity(null);
        cbxAssetComponent2.setSelectedEntity(null);
        txtCurrentOdometer.setValue("");
        txtUnitPrice.setValue("");
        txtUnitPrice2.setValue("");
        txtChassisNumber2.setValue("");
        dtOfImport.setValue(null);
        dtOfPurchase.setValue(null);

        this.setEnabledAsset(true);
        markAsDirty();
    }

    @Override
    public void saveButtonClick(Button.ClickEvent clickEvent) {
        Notification.show("LOL");
    }

    /**
     * @param quotation
     */
    public void assignValues(Quotation quotation) {
        this.quotation = quotation;
        removeAllComponents();
        super.init();

        assetRangeKubuta(quotation);
        AssetModel assetModel = null;
        if (quotation != null && quotation.getAsset() != null) {
            this.asset = quotation.getAsset();
            if (asset.getModel() != null) {
                assetModelId = asset.getModel().getId();
                assetModel = ENTITY_SRV.getById(AssetModel.class, asset.getModel().getId());
                cbxAssetRange.setSelectedEntity(assetModel.getAssetRange());
                cbxAssetModel.setSelectedEntity(assetModel);
                if (assetModel.getStatusRecord() == EStatusRecord.INACT) {
                    try {
                        cbxAssetModel.removeItem(assetModel);
                        cbxAssetModel.addItem(assetModel.getId().toString());
                    } catch (Exception e) {
                        cbxAssetModel.addItem(assetModel.getId().toString());
                    }
                    cbxAssetModel.setItemCaption(assetModel.getId().toString(), assetModel.getDescEn());
                }

            }

            cbxColor.setSelectedEntity(asset.getColor());
            cbxYear.setSelectedEntity(asset.getAssetYear());
            cbxEngine.setSelectedEntity(asset.getEngine());
            cbxAssetGender.setSelectedEntity(asset.getAssetGender());
            txtEngineNumber.setValue(asset.getEngineNumber());
            txtChassisNumber.setValue(asset.getChassisNumber());
            txtPlateNumber.setValue(asset.getPlateNumber());
            dfRegistrationDate.setValue(asset.getRegistrationDate());

            txtChassisNumberDc.setValue(asset.getChassisNumber());
            txtEngineNumberDc.setValue(asset.getEngineNumber());
            txtPlateNumberDc.setValue(asset.getPlateNumber());
            dfRegistrationDateDc.setValue(asset.getRegistrationDate());

            cbxAssetModel2.setSelectedEntity(asset.getPowerTillerModel());
            cbxColor2.setSelectedEntity(asset.getAssetColorPowerTiler());
            cbxYear2.setSelectedEntity(asset.getAssetYearPowerTiller());
            cbxAssetComponent.setSelectedEntity(asset.getAssetComponentDieselEngine());
            cbxAssetComponent2.setSelectedEntity(asset.getAssetComponentPowerTiller());
            cbxAssetBrand.setSelectedEntity(asset.getAssetMakeDieselEngine());
            cbxAssetBrand2.setSelectedEntity(asset.getAssetMakePowerTiller());
            txtChassisNumber2.setValue(asset.getChassisNumberPowerTiller());
            if (asset.getCashPriceUsdDieselEngine() != null) {
                txtUnitPrice.setValue(AmountBigUtils.format(asset.getCashPriceUsdDieselEngine()));
            }
            if (asset.getCashPriceUsdPowerTiller() != null) {
                txtUnitPrice2.setValue(AmountBigUtils.format(asset.getCashPriceUsdPowerTiller()));
            }
            txtUnitPrice2.setValue(AmountBigUtils.format(asset.getCashPriceUsdPowerTiller()));
            txtAssetModePrices.setValue(String.valueOf(asset.getTiAssetModelPrice()));
            txtCurrentOdometer.setValue(String.valueOf(asset.getOdometer()));
            dtOfImport.setValue(asset.getImportDate());

            if (asset.getPurchageDate() != null) {
                dtOfPurchase.setValue(asset.getPurchageDate());
            }

            if (QuotationProfileUtils.isDocumentControllerAvailable(quotation)) {
                horizontalLayout.addComponent(ComponentFactory.getSpaceLayout(30, Unit.PIXELS));
                horizontalLayout.addComponent(dcFormLayout3Panel);
            }

            if ((ProfileUtil.isUW() || ProfileUtil.isManager())
                    && (QuotationProfileUtils.isQuotationStatusBeforeApproval(quotation)) || ProfileUtil.isDocumentController()) {
                setEnabledAsset(false);
                if(quotation.getWkfStatus().equals(QuotationWkfStatus.WIV)){
                    dcFormLayout3Panel.setEnabled(true);
                }
            } else if ((ProfileUtil.isPOS())
                    && (quotation.getWkfStatus().equals(QuotationWkfStatus.QUO)
                    || quotation.getWkfStatus().equals(QuotationWkfStatus.RFC)
                    || quotation.getWkfStatus().equals(QuotationWkfStatus.APP))
                    || ProfileUtil.isAdmin()) {
                setEnabledAsset(true);
            } else {
                assetDetailLayout.setEnabled(quotation.getWkfStatus().equals(QuotationWkfStatus.QUO));
                assetChassisLayout.setEnabled(quotation.getWkfStatus().equals(QuotationWkfStatus.QUO));
            }

            showDieselDocumentAsTructor();
            showDocumentforWalkingTructor();
        }

        // KUbuta component for asset
        if (quotation != null && quotation.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_KUBUTA)) {

            tructorComponents(false);
            assetGenderComponents(false);

            powerTillerPanel.setVisible(false);
            powerTillerFormLayoutPanel.setVisible(false);

            uploadScratchPaperDieselEngine.setVisible(false);
            lblReceiptScrate.setVisible(false);

            isWalkingTructor();

        } else {
            enginePanel.setCaption("");
            assetMainForm.removeComponent(diselEnginePanel);
            assetMainForm.removeComponent(powerTillerFormLayoutPanel);
            assetMainForm.removeComponent(powerTillerPanel);

            assetDetailLayout.removeComponent(txtAssetModePrices);
            assetDetailLayout.removeComponent(txtCurrentOdometer);
            assetDetailLayout.removeComponent(dtOfImport);
            assetDetailLayout.removeComponent(dtOfPurchase);

            assetDetailLayout.removeComponent(lblAssetPrice);
            assetDetailLayout.removeComponent(lblCurrentOdometer);
            assetDetailLayout.removeComponent(lblDateImport);
            assetDetailLayout.removeComponent(lblPurchaseDate);

            tructorComponents(false);
            assetGenderComponents(true);
        }
        setEnableForChassiseNumber(QuotationProfileUtils.isDocumentControllerAvailable(quotation));
    }

    public void tructorComponents(boolean enable) {

        lblAssetCommponent.setVisible(enable);
        cbxAssetComponent.setVisible(enable);
        cbxAssetComponent2.setVisible(enable);
        lblAssetBrand.setVisible(enable);
        cbxAssetBrand.setVisible(enable);
        cbxAssetBrand2.setVisible(enable);

        cbxAssetModel2.setVisible(enable);
        cbxAssetBrand2.setVisible(enable);
        cbxYear2.setVisible(enable);

        cbxColor2.setVisible(enable);

        lblUnitPrice.setVisible(enable);
        txtUnitPrice.setVisible(enable);
        txtUnitPrice2.setVisible(enable);

    }

    public void assetGenderComponents(boolean enable) {
        lblAssetGender.setVisible(enable);
        cbxAssetGender.setVisible(enable);
    }


    public List<String> isValid() {
        super.reset();
        checkMandatorySelectField(cbxAssetModel, "asset.range2");
        checkMandatorySelectField(cbxColor, "color");
        checkMandatorySelectField(cbxYear, "asset.year");
        checkMandatorySelectField(cbxEngine, "engine");

        if (!this.setEnableForChassiseNumber(true)) {
            checkMandatoryField(txtEngineNumber, "engine.number");
            checkMandatoryField(txtChassisNumber, "chassis.number");

        } else if (ProfileUtil.isDocumentController()) {
            checkMandatoryField(txtChassisNumberDc, "chassis.number");
            checkMandatoryField(txtEngineNumberDc, "engine.number");

        } else {
            checkMandatoryField(txtEngineNumber, "engine.number");
            if (cbxAssetRange.getSelectedEntity() != null && !cbxAssetRange.getSelectedEntity().getCode().equals("B004")) {
                checkMandatoryField(txtChassisNumber, "chassis.number");
            }

        }
        if (hashError != null) {
            for (Map.Entry<String, String> entry : hashError.entrySet()) {
                errors.add(entry.getValue());
            }
        }
        if (quotation != null && quotation.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_KUBUTA)) {

            checkMandatoryField(txtAssetModePrices, "asset.price");
            checkMandatoryField(txtCurrentOdometer, "current.odometer");
            checkMandatoryDateField(dtOfImport, "date.import");

            if (cbxAssetRange.getSelectedEntity() != null && cbxAssetRange.getSelectedEntity().getCode().equals("B004")) {
                checkMandatorySelectField(cbxAssetComponent, "asset.component");
                checkMandatorySelectField(cbxAssetComponent2, "asset.component2");
                checkMandatorySelectField(cbxAssetBrand, "asset.brand");
                checkMandatorySelectField(cbxAssetBrand2, "asset.brand2");
                checkMandatorySelectField(cbxAssetModel2, "asset.brand2");
                checkMandatorySelectField(cbxColor2, "asset.color2");
                checkMandatorySelectField(cbxYear2, "asset.year2");
            } else {
                if (btnPhotoOfEngine.getData() == null) {
                    errors.add(I18N.message("photo.of.engine"));
                }
                if (btnVehicleDocument.getData() == null) {
                    errors.add(I18N.message("vehicle.document"));
                }
            }

        } else
            checkMandatorySelectField(cbxAssetGender, "type");

        return errors;
    }

    /* Get Asset */
    public Asset getAsset() {
        asset.setEngine(cbxEngine.getSelectedEntity());
        asset.setAssetGender(cbxAssetGender.getSelectedEntity());
        asset.setChassisNumber(txtChassisNumber.getValue());
        asset.setRegistrationDate(dfRegistrationDate.getValue());
        asset.setPlateNumber(txtPlateNumber.getValue());
        asset.setEngineNumber(txtEngineNumber.getValue());
        asset.setModel(cbxAssetModel.getSelectedEntity());
        asset.setColor(cbxColor.getSelectedEntity());
        asset.setAssetYear(cbxYear.getSelectedEntity());

        if (quotation != null && QuotationProfileUtils.isKubuta4Plus(quotation)){
            asset.setChassisNumberPowerTiller(txtChassisNumber2.getValue());
            asset.setPowerTillerModel(cbxAssetModel2.getSelectedEntity());
            asset.setAssetComponentDieselEngine(cbxAssetComponent.getSelectedEntity());
            asset.setAssetComponentPowerTiller(cbxAssetComponent2.getSelectedEntity());
            asset.setAssetMakeDieselEngine(cbxAssetBrand.getSelectedEntity());
            asset.setAssetMakePowerTiller(cbxAssetBrand2.getSelectedEntity());
            asset.setPowerTillerModel(cbxAssetModel2.getSelectedEntity());
            asset.setAssetColorPowerTiler(cbxColor2.getSelectedEntity());
            asset.setAssetYearPowerTiller(cbxYear2.getSelectedEntity());
            asset.setTiAssetModelPrice(getDouble(txtAssetModePrices, 0));
            asset.setOdometer(Long.valueOf(txtCurrentOdometer.getValue()));
            asset.setImportDate(dtOfImport.getValue());

            if (txtUnitPrice.getValue() != null) {
                asset.setCashPriceUsdDieselEngine(getDouble(txtUnitPrice, 0));
            }
            if (txtUnitPrice2.getValue() != null) {
                asset.setCashPriceUsdPowerTiller(getDouble(txtUnitPrice2, 0));
            }
            if (dtOfPurchase.getValue() != null) {
                asset.setPurchageDate(dtOfPurchase.getValue());
            }
        }

        buildAssetPrice(cbxAssetModel.getSelectedEntity());
        return asset;
    }

    private AssetPrice getAssetPriceByModel(AssetModel assetModel) {
        List<AssetPrice> lstAssetPrices = new ArrayList<>();
        AssetPrice assetPrice = null;
        if (assetModel != null) {
            BaseRestrictions<AssetPrice> restrictions = new BaseRestrictions<>(AssetPrice.class);
            restrictions.addCriterion(Restrictions.eq("assetModel", assetModel));
            lstAssetPrices = ENTITY_SRV.list(restrictions);
            if (lstAssetPrices != null && !lstAssetPrices.isEmpty()) {
                assetPrice = lstAssetPrices.get(0);
            }
        }
        return assetPrice;
    }

    private boolean setEnableForChassiseNumber(boolean enabled) {
        txtChassisNumberDc.setEnabled(enabled);
        txtEngineNumberDc.setEnabled(enabled);
        txtPlateNumberDc.setEnabled(enabled);
        dfRegistrationDateDc.setEnabled(enabled);
        return true;
    }

    public void setEnabledAsset(boolean enabled) {
        cbxAssetRange.setEnabled(enabled);
        cbxAssetModel.setEnabled(enabled);
        cbxColor.setEnabled(enabled);
        cbxYear.setEnabled(enabled);
        cbxEngine.setEnabled(enabled);
        cbxAssetGender.setEnabled(enabled);
        txtChassisNumber.setEnabled(enabled);
        txtEngineNumber.setEnabled(enabled);
        txtPlateNumber.setEnabled(enabled);
        dfRegistrationDate.setEnabled(enabled);
        assetDetailLayout.setEnabled(enabled);
        assetChassisLayout.setEnabled(enabled);
        dcFormLayout3Panel.setEnabled(enabled);
    }

    private void buildAssetPrice(AssetModel assetModel) {
        AssetPrice assetPrice = getAssetPriceByModel(assetModel);
        if (assetPrice != null) {
            asset.setTeAssetPrice(assetPrice.getPrice() != null ? assetPrice.getPrice() : 0);
            asset.setTiAssetPrice(assetPrice.getPrice() != null ? assetPrice.getPrice() : 0);
            asset.setTeAssetSecondHandPrice(assetPrice.getSecondHandPrice() != null ? assetPrice.getSecondHandPrice() : 0);
            asset.setTiAssetSecondHandPrice(assetPrice.getSecondHandPrice() != null ? assetPrice.getSecondHandPrice() : 0);
        }
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }

    public void setAssetTabPanel(AssetTabPanel assetTabPanel) {
        this.assetTabPanel = assetTabPanel;
    }

    public void setHashError(Map<String, String> hashError) {
        this.hashError = hashError;
    }

    /**
     * Confirm Validate Asset in Document Controller Profile
     */
    public boolean saveConfirmAssetInDocumentController() {
        isConfirm = false;
        if (isValid().isEmpty()) {
            if (isNotMatch(txtChassisNumber, txtChassisNumberDc)
                    || isNotMatch(txtEngineNumber, txtEngineNumberDc)
                    || isNotMatch(txtPlateNumber, txtPlateNumberDc)
                    || !isDfRegistrationDateEqualToDfRegistrationDateDc() ) {

                ConfirmDialog confirmDialog = ConfirmDialog.show(UI.getCurrent(), I18N.message("The information of DC and CO don't match. Are you sure to update it?"),
                        new ConfirmDialog.Listener() {
                            private static final long serialVersionUID = 1L;

                            public void onClose(ConfirmDialog dialog) {
                                if (dialog.isConfirmed()) {
                                    Asset asset = quotation.getAsset() == null ? new Asset() : quotation.getAsset();
                                    asset.setChassisNumber(txtChassisNumberDc.getValue());
                                    asset.setRegistrationDate(dfRegistrationDateDc.getValue());
                                    asset.setPlateNumber(txtPlateNumberDc.getValue());
                                    asset.setEngineNumber(txtEngineNumberDc.getValue());
                                    txtPlateNumber.setValue(asset.getPlateNumber());
                                    dfRegistrationDate.setValue(asset.getRegistrationDate());
                                    QUO_SRV.saveDocumentControllerDecision(quotation);
                                    assignValues(quotation);
                                    isConfirm = true;

                                } else if (dialog.isCanceled()) {
                                    txtChassisNumberDc.setValue("");
                                    txtEngineNumberDc.setValue("");
                                    txtPlateNumberDc.setValue("");
                                    dfRegistrationDateDc.setValue(null);
                                    isConfirm = false;
                                }
                            }
                        });
                confirmDialog.setWidth("400px");
                confirmDialog.setHeight("150px");
            }
        }
        return isConfirm;
    }

    private boolean isNotMatch(TextField field1, TextField field2) {
        return !getDefaultString(field1.getValue()).equals(getDefaultString(field2.getValue()));
    }

    private boolean isDfRegistrationDateEqualToDfRegistrationDateDc(){
        return dfRegistrationDate.getValue() == null || DateUtils.getDateAtBeginningOfDay(dfRegistrationDate.getValue()).equals(DateUtils.getDateAtBeginningOfDay(dfRegistrationDateDc.getValue()));
    }

    public boolean isMatchDC() {
        return !isNotMatch(txtChassisNumber, txtChassisNumberDc)
                && !isNotMatch(txtEngineNumber, txtEngineNumberDc)
                && !isNotMatch(txtPlateNumber, txtPlateNumberDc)
                && isDfRegistrationDateEqualToDfRegistrationDateDc();
    }

    @Override
    public void buttonClick(Button.ClickEvent event) {
        try {
            if (event.getButton() == btnVehicleDocument || event.getButton() == btnWarrantyDielsel
                    || event.getButton() == btnPhotoOfEngine || event.getButton() == btnWarrantyTiller
                    || event.getButton() == bntScratPaperOfDieselEngine || event.getButton() == btnWarrantDieselAskWalkingTructor
                    || event.getButton() == bntScatchPaperTiller) {

                if ((String) ((Button) event.getSource()).getData() != null) {
                    displayDocumentPanel = new DisplayDocumentPanel((String) ((Button) event.getSource()).getData());
                    displayDocumentPanel.display();
                }
                if (ProfileUtil.isPOS() || ProfileUtil.isUW() || ProfileUtil.isManager()) {
                    displayDocumentPanel.setModal(false);
                }
            }
        } catch (Exception e) {
            messageEmpty();
            e.printStackTrace();
        }
    }

    // filter asset rang by financial product
    public void assetRangeKubuta(Quotation quotation) {
        if (cbxAssetRange != null || !cbxAssetRange.getValueMap().isEmpty()) {
            cbxAssetRange.clear();
            BaseRestrictions<AssetRange> assetRangeByKubuta = new BaseRestrictions<>(AssetRange.class);
            if (quotation != null && quotation.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_KUBUTA)) {
                assetRangeByKubuta.addCriterion(Restrictions.in("id", new Long[]{10l, 11l, 12l}));
            } else {
                assetRangeByKubuta.addCriterion(Restrictions.not(Restrictions.in("id", new Long[]{10l, 11l, 12l})));
            }
            cbxAssetRange.setRestrictions(assetRangeByKubuta);
            cbxAssetRange.renderer();

        }
    }

    void isWalkingTructor() {
        if (cbxAssetRange.getSelectedEntity() != null && cbxAssetRange.getSelectedEntity().getCode().equals("B004")) {
            tructorComponents(true);
            lblReceiptScrate.setVisible(true);
            uploadScratchPaperDieselEngine.setVisible(true);

            powerTillerPanel.setVisible(true);
            powerTillerFormLayoutPanel.setVisible(true);

            txtChassisNumber.setVisible(false);
            txtPlateNumber.setVisible(false);
            dfRegistrationDate.setVisible(false);

            uploadVehicleDocument.setVisible(false);
            uploadPhotoEngine.setVisible(false);
            lblPhotoEngine.setVisible(false);
            lblVihicleDocument.setVisible(false);

        } else {
            powerTillerPanel.setVisible(false);
            powerTillerFormLayoutPanel.setVisible(false);
            tructorComponents(false);

            txtChassisNumber.setVisible(true);
            txtPlateNumber.setVisible(true);
            dfRegistrationDate.setVisible(true);

            uploadVehicleDocument.setVisible(true);
            uploadPhotoEngine.setVisible(true);
            lblPhotoEngine.setVisible(true);
            lblVihicleDocument.setVisible(true);
            lblReceiptScrate.setVisible(false);
            uploadScratchPaperDieselEngine.setVisible(false);
        }
    }

    @Override
    public void valueChange(Property.ValueChangeEvent event) {

        if (event.getProperty() == cbxAssetModel) {
            if (cbxAssetModel.getSelectedEntity() != null) {

                assetTabPanel.addAppraisalPanel(cbxAssetRange.getSelectedEntity());
                buildAssetPrice(cbxAssetModel.getSelectedEntity());

                if (cbxAssetRange.getSelectedEntity() != null && cbxAssetRange.getSelectedEntity().getCode().equals("B004")) {
                    setHpByAssetModel(cbxAssetModel.getSelectedEntity());
                }

                if (asset.getTiAssetPrice() == null) {
                    AssetModel assetModel = cbxAssetModel.getSelectedEntity();
                    txtAssetModePrices.setValue(String.valueOf(assetModel.getTiPrice()));
                }

            } else {
                if (assetTabPanel.getAppraisalPanel() != null) {
                    assetTabPanel.removeAppraisalPanel();
                }
            }
        }
        if (event.getProperty() == cbxAssetRange) {
            if (cbxAssetRange.getSelectedEntity() != null) {
                if (quotation != null && quotation.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_KUBUTA)) {
                    isWalkingTructor();
                    showDocumentforWalkingTructor();
                    showDieselDocumentAsTructor();
                }
            } else {
                if (assetTabPanel.getAppraisalPanel() != null) {
                    assetTabPanel.removeAppraisalPanel();
                }
            }
        }
    }

    public void showDocumentforWalkingTructor() {
        if (quotation != null && quotation.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_KUBUTA)) {
            if (asset != null) {
                if (cbxAssetRange.getSelectedEntity() != null) {
                    if (asset.getWarrantyDiesel() != null && cbxAssetRange.getSelectedEntity().getCode().equals("B004")) {
                        btnWarrantDieselAskWalkingTructor.setVisible(true);
                        btnWarrantDieselAskWalkingTructor.setData(asset.getWarrantyDiesel());
                    } else {
                        btnWarrantDieselAskWalkingTructor.setVisible(false);
                    }
                    if (asset.getScrathDieselEngine() != null && cbxAssetRange.getSelectedEntity().getCode().equals("B004")) {
                        bntScratPaperOfDieselEngine.setVisible(true);
                        bntScratPaperOfDieselEngine.setData(asset.getScrathDieselEngine());
                    } else {
                        bntScratPaperOfDieselEngine.setVisible(false);
                    }
                    if (asset.getPowerTillerReceipt() != null && cbxAssetRange.getSelectedEntity().getCode().equals("B004")) {
                        bntScatchPaperTiller.setVisible(true);
                        bntScatchPaperTiller.setData(asset.getPowerTillerReceipt());
                    } else {
                        bntScatchPaperTiller.setVisible(false);
                    }
                    if (asset.getWarrantyPowerTiller() != null && cbxAssetRange.getSelectedEntity().getCode().equals("B004")) {
                        btnWarrantyTiller.setVisible(true);
                        btnWarrantyTiller.setData(asset.getWarrantyPowerTiller());
                    } else {
                        btnWarrantyTiller.setVisible(false);
                    }
                }
            }


        }
    }

    public void showDieselDocumentAsTructor() {
        if (asset != null) {
            if (cbxAssetRange.getSelectedEntity() != null) {

                if (asset.getVehicleDocumentDiesel() != null && !cbxAssetRange.getSelectedEntity().getCode().equals("B004")) {
                    btnVehicleDocument.setVisible(true);
                    btnVehicleDocument.setData(asset.getVehicleDocumentDiesel());
                } else
                    btnVehicleDocument.setVisible(false);

                if (asset.getWarrantyDiesel() != null) {
                    btnWarrantyDielsel.setVisible(true);
                    btnWarrantyDielsel.setData(asset.getWarrantyDiesel());
                } else
                    btnWarrantyDielsel.setVisible(false);

                if (asset.getPhotoDieseEngine() != null && !cbxAssetRange.getSelectedEntity().getCode().equals("B004")) {
                    btnPhotoOfEngine.setVisible(true);
                    btnPhotoOfEngine.setData(asset.getPhotoDieseEngine());
                } else
                    btnPhotoOfEngine.setVisible(false);
            }
        }
    }

    void messageEmpty() {
        MessageBox mb = new MessageBox(
                UI.getCurrent(), "320px", "140px", I18N.message("information"),
                MessageBox.Icon.ERROR,
                I18N.message("File not yet chose !"), Alignment.MIDDLE_RIGHT,
                new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, I18N.message("ok")));
        mb.show();
    }

    void uploadFail() {
        MessageBox mb = new MessageBox(
                UI.getCurrent(), "320px", "140px", I18N.message("information"),
                MessageBox.Icon.ERROR,
                I18N.message("File upload fail try again !"), Alignment.MIDDLE_RIGHT,
                new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, I18N.message("ok")));
        mb.show();
    }

    // get HP for walking tructor
    private void setHpByAssetModel(AssetModel assetModel) {
        BaseRestrictions<AssetModelHp> restrictions = new BaseRestrictions<>(AssetModelHp.class);
        restrictions.addCriterion(Restrictions.eq("assetModel", assetModel));
        AssetModelHp assetModelHps = ENTITY_SRV.getFirst(restrictions);
        if (assetModelHps != null) {
            cbxEngine.setSelectedEntity(assetModelHps.getHp());
        } else {
            cbxEngine.setSelectedEntity(null);
        }
    }

    public boolean setEnableDocumentButton(boolean enable){
        bntScratPaperOfDieselEngine.setVisible(enable);
        btnWarrantDieselAskWalkingTructor.setVisible(enable);

        bntScatchPaperTiller.setVisible(enable);
        btnWarrantyTiller.setVisible(enable);

        btnVehicleDocument.setVisible(enable);
        btnPhotoOfEngine.setVisible(enable);
        btnWarrantyDielsel.setVisible(enable);
        return enable;
    }

    public AssetModel getAssetModel(){
        return cbxAssetModel.getSelectedEntity();
    }
}