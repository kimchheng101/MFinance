package com.soma.mfinance.core.applicant.panel.guarantor;

import com.soma.common.app.eref.ECountry;
import com.soma.mfinance.core.address.panel.AddressPanel;
import com.soma.mfinance.core.applicant.model.*;
import com.soma.mfinance.core.applicant.panel.CurrentEmploymentPanelMFP;
import com.soma.mfinance.core.applicant.panel.IdentityPanel;
import com.soma.mfinance.core.applicant.panel.SecondaryEmploymentPanelMFP;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationApplicant;
import com.soma.mfinance.core.shared.applicant.AddressUtils;
import com.soma.mfinance.core.shared.quotation.QuotationProfileUtils;
import com.soma.ersys.core.hr.model.address.Address;
import com.soma.ersys.core.hr.model.eref.EEmploymentType;
import com.soma.ersys.core.hr.model.eref.ERelationship;
import com.soma.ersys.core.hr.model.eref.ETypeAddress;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.SelectedTabChangeListener;
import org.apache.commons.lang.StringUtils;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.CrudAction;
import org.vaadin.dialogs.ConfirmDialog;

import java.util.List;

/**
 * Guarantor panel
 *
 * @author kimsuor.seang
 */
public class GuarantorPanel extends AbstractTabPanel{

    private static final long serialVersionUID = -8442994066145205792L;

    private IdentityPanel identityPanel;
    private AddressPanel addressFormPanel;
    private CurrentEmploymentPanelMFP currentEmploymentPanel;
    private SecondaryEmploymentPanelMFP secondaryEmploymentPanel;
    private OtherInformationPanel otherInformationPanel;

    private TabSheet guarantorTabSheet;
    private VerticalLayout identityTab;
    private VerticalLayout employmentTab;
    private VerticalLayout otherInformationTab;

    private ERefDataComboBox<ERelationship> cbxRelationship;
    private CheckBox cbSameApplicantAddress;

    private Applicant mainApplicant;
    private Applicant guarantor;
    private Address address;

    public Button btnChangeGuarantor;

    /**
     * @param
     */
    public GuarantorPanel() {
        super();
        setSizeFull();
    }

    @Override
    protected Component createForm() {

        cbxRelationship = new ERefDataComboBox<ERelationship>(I18N.message("relationship.with.applicant"), ERelationship.class);
        cbxRelationship.setRequired(true);

        cbSameApplicantAddress = new CheckBox(I18N.message("live.with.applicant"));
        cbSameApplicantAddress.setImmediate(true);
        cbSameApplicantAddress.setValue(false);

        btnChangeGuarantor = new NativeButton(I18N.message("change.guarantor"));
        btnChangeGuarantor.setIcon(new ThemeResource("../smt-default/icons/16/edit.png"));
        btnChangeGuarantor.setVisible(false);

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(new FormLayout(cbxRelationship));
        horizontalLayout.addComponent(ComponentFactory.getSpaceLayout(30, Unit.PIXELS));
        horizontalLayout.addComponent(new FormLayout(cbSameApplicantAddress));
        horizontalLayout.addComponent(ComponentFactory.getSpaceLayout(100, Unit.PIXELS));
        horizontalLayout.addComponent(btnChangeGuarantor);
        btnChangeGuarantor.addClickListener(new ClickListener() {
            private static final long serialVersionUID = 233733154766622576L;

            @Override
            public void buttonClick(ClickEvent event) {
                ConfirmDialog confirmDialog = ConfirmDialog.show(UI.getCurrent(), I18N.message("confirm.change.guarantor"),
                        new ConfirmDialog.Listener() {
                            private static final long serialVersionUID = 2380193173874927880L;

                            public void onClose(ConfirmDialog dialog) {
                                if (dialog.isConfirmed()) {
                                    // quotationFormPanel.changeGuarantor();
                                }
                            }
                        });
                confirmDialog.setWidth("400px");
                confirmDialog.setHeight("150px");
            }
        });

        guarantorTabSheet = new TabSheet();
        guarantorTabSheet.setSizeFull();

        identityTab = new VerticalLayout();
        identityPanel = new IdentityPanel("guarantorIdentity");
        final Panel addressPanel = new Panel(I18N.message("current.address"));
        addressFormPanel = new AddressPanel(true, ETypeAddress.HOME, "addressHorizontal");
        addressFormPanel.setMargin(true);
        addressPanel.setContent(addressFormPanel);

        identityTab.setSpacing(true);
        identityTab.setMargin(true);
        identityTab.addComponent(identityPanel);
        identityTab.addComponent(addressPanel);

        employmentTab = new VerticalLayout();
        currentEmploymentPanel = new CurrentEmploymentPanelMFP();
        final Panel currentEmploymentLayout = new Panel(I18N.message("current.employment"));
        currentEmploymentLayout.setContent(currentEmploymentPanel);

        secondaryEmploymentPanel = new SecondaryEmploymentPanelMFP("secondaryEmploymentHorizontal");
        final Panel secondaryEmploymentLayout = new Panel(I18N.message("secondary.employment"));
        secondaryEmploymentLayout.setContent(secondaryEmploymentPanel);

        employmentTab.setSpacing(true);
        employmentTab.setMargin(true);
        employmentTab.addComponent(currentEmploymentLayout);
        employmentTab.addComponent(secondaryEmploymentLayout);

        otherInformationTab = new VerticalLayout();
        otherInformationTab.setSpacing(true);
        otherInformationTab.setMargin(true);
        otherInformationPanel = new OtherInformationPanel();
        otherInformationTab.addComponent(otherInformationPanel);

        guarantorTabSheet.addTab(identityTab, I18N.message("identity"));
        guarantorTabSheet.addTab(employmentTab, I18N.message("employment"));
        guarantorTabSheet.addTab(otherInformationTab, I18N.message("other.information"));

        guarantorTabSheet.addSelectedTabChangeListener(new SelectedTabChangeListener() {
            private static final long serialVersionUID = -2275900953290360358L;

            @Override
            public void selectedTabChange(SelectedTabChangeEvent event) {
                address = new Address();
                if(addressFormPanel.getAddress(address) !=null){
                    address = addressFormPanel.getAddress(address);
                    currentEmploymentPanel.setAddress(address);
                }
            }
        });

        cbSameApplicantAddress.addValueChangeListener(new ValueChangeListener() {
            private static final long serialVersionUID = -2120119835501936565L;

            @Override
            public void valueChange(ValueChangeEvent event) {
                addressFormPanel.setAddressEnabled(!cbSameApplicantAddress.getValue());
                Address address = (guarantor != null) ? guarantor.getIndividual().getMainAddress() : null;
                if (address == null) {
                    address = new Address();
                    address.setCountry(ECountry.KHM);
                }
                if (mainApplicant != null) {
                    Address applicantAddress = mainApplicant.getIndividual().getMainAddress();
                    if (applicantAddress != null && cbSameApplicantAddress.getValue()) {
                        address = AddressUtils.copy(applicantAddress, address);
                    }
                }

                addressFormPanel.assignValues(address);

                if (currentEmploymentPanel.isSameApplicantAddress()) {
                    currentEmploymentPanel.assignAddressValues(address);
                }
            }
        });

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        verticalLayout.setSpacing(true);
        verticalLayout.addComponent(horizontalLayout);
        verticalLayout.addComponent(guarantorTabSheet);
        return verticalLayout;
    }

    /**
     * @return
     */
    public QuotationApplicant getQuotationApplicant(Quotation quotation) {
        if (quotation.getQuotationApplicant(EApplicantType.G) == null) {
            QuotationApplicant quotationApplicant = new QuotationApplicant();
            quotationApplicant.setRelationship(cbxRelationship.getSelectedEntity());
            quotationApplicant.setSameApplicantAddress(cbSameApplicantAddress.getValue());
            quotationApplicant.setApplicantType(EApplicantType.G);
            Applicant applicant = new Applicant();
            applicant.setApplicantCategory(EApplicantCategory.INDIVIDUAL);
            quotationApplicant.setApplicant(getApplicant(applicant));
            quotation.getQuotationApplicants().add(quotationApplicant);
            return quotationApplicant;
        } else {
            QuotationApplicant quotationApplicant = quotation.getQuotationApplicant(EApplicantType.G);
            quotationApplicant.setRelationship(cbxRelationship.getSelectedEntity());
            quotationApplicant.setSameApplicantAddress(cbSameApplicantAddress.getValue());
            Applicant applicant = getApplicant(quotationApplicant.getApplicant());
            quotationApplicant.setApplicant(applicant);
            quotation.getQuotationApplicants().add(quotationApplicant);
            return quotationApplicant;
        }
    }

    /**
     * @param mainApplicant
     */
    public void setMainApplicant(Applicant mainApplicant) {
        this.mainApplicant = mainApplicant;
    }

    /**
     * Get applicant
     *
     * @param
     * @return
     */
    public Applicant getApplicant(Applicant applicant) {
        identityPanel.getApplicant(applicant);
        Individual individual = applicant.getIndividual();

        if (cbxRelationship.getSelectedEntity() != null
                || cbSameApplicantAddress.getValue()
                || StringUtils.isNotEmpty(individual.getFirstNameEn())
                || StringUtils.isNotEmpty(individual.getLastNameEn())) {

            IndividualAddress individualAddress = individual.getIndividualAddress(ETypeAddress.MAIN);
            if (individualAddress == null || individualAddress.getId() == null) {
                Address address = addressFormPanel.getAddress(new Address());
                individual.setAddress(address, ETypeAddress.MAIN);
                individualAddress = new IndividualAddress();
            } else {
                Address address = individualAddress.getAddress();
                individual.setAddress(address, ETypeAddress.MAIN);
                addressFormPanel.getIndividualAddress(individual);
                addressFormPanel.getAddress(individualAddress.getAddress());
            }

            Employment currentEmployment = individual.getCurrentEmployment();
            if (currentEmployment != null) {
                currentEmploymentPanel.getEmployment(currentEmployment);
                individual.addEmployment(currentEmployment);
            } else {
                currentEmployment = currentEmploymentPanel.getEmployment(new Employment());
                if (currentEmployment.isPersistent()) {
                    individual.addEmployment(currentEmployment);
                }
            }
            if (currentEmployment.isSameApplicantAddress()) {
                AddressUtils.copy(individualAddress.getAddress(), currentEmployment.getAddress());
            }

            List<Employment> secondaryEmployments = individual.getEmployments(EEmploymentType.SECO);
            if (secondaryEmployments != null && !secondaryEmployments.isEmpty()) {
                Employment secondaryEmployment = secondaryEmploymentPanel.getEmployment(secondaryEmployments.get(0));
                if (secondaryEmployment == null) {
                    secondaryEmployments.get(0).setCrudAction(CrudAction.DELETE);
                }
            } else {
                Employment secondaryEmployment = secondaryEmploymentPanel.getEmployment(new Employment());
                if (secondaryEmployment != null) {
                    individual.addEmployment(secondaryEmployment);
                }
            }
            otherInformationPanel.getApplicant(applicant);
        }
        return applicant;
    }


    public void assignValues(Quotation quotation) {
        QuotationApplicant quotationApplicant = quotation.getQuotationApplicant(EApplicantType.G);
        if (quotationApplicant != null) {
            this.guarantor = quotationApplicant.getApplicant();
            cbxRelationship.setSelectedEntity(quotationApplicant.getRelationship());
            cbSameApplicantAddress.setValue(quotationApplicant.isSameApplicantAddress());

            Individual individual = guarantor.getIndividual();
            identityPanel.assignValues(individual);
            Address address = individual.getMainAddress();
            if (address == null) {
                address = new Address();
                address.setCountry(ECountry.KHM);
                individual.setAddress(address, ETypeAddress.MAIN);
            }
            addressFormPanel.assignValues(address);
            addressFormPanel.assignIndAdress(individual.getIndividualAddress(ETypeAddress.MAIN));
            Employment currentEmployment = null;
            if (individual.getEmployments() != null) {
                currentEmployment = individual.getCurrentEmployment();
                if (currentEmployment != null) {
                    currentEmploymentPanel.setApplicant(individual, EApplicantType.G);
                    currentEmploymentPanel.assignValues(currentEmployment);
                }
            }

            if (currentEmployment == null) {
                currentEmploymentPanel.reset();
            }
            List<Employment> secondaryEmployments = individual.getEmployments(EEmploymentType.SECO);
            if (secondaryEmployments != null && !secondaryEmployments.isEmpty()) {
                secondaryEmploymentPanel.assignValues(secondaryEmployments.get(0));
            } else {
                secondaryEmploymentPanel.reset();
            }
            otherInformationPanel.assignValues(guarantor);


            btnChangeGuarantor.setVisible(false);
        } else {
            this.reset();
            currentEmploymentPanel.setApplicant(null, EApplicantType.G);
            secondaryEmploymentPanel.reset();
        }
        guarantorTabSheet.setSelectedTab(identityTab);

        setEnabledGuarantor(QuotationProfileUtils.isEnabled(quotation));
    }

    /**
     * @param enabled
     */
    public void setEnabledGuarantor(boolean enabled) {
        cbxRelationship.setEnabled(enabled);
        cbSameApplicantAddress.setEnabled(enabled);

        identityPanel.setEnableIdentityPanel(enabled);
        addressFormPanel.setEnabled(enabled);
        otherInformationPanel.setEnableOtherInformation(enabled);
        currentEmploymentPanel.setEnabled(enabled);
        secondaryEmploymentPanel.setEnabled(enabled);
    }

    /**
     * @return
     */
    public boolean isValid() {
        super.removeErrorsPanel();
        errors.addAll(identityPanel.validate());
        errors.addAll(addressFormPanel.partialValidate());
        if (!errors.isEmpty()) {
            super.displayErrorsPanel();
        }
        return errors.isEmpty();
    }
    public List<String> gurantorValid() {
        super.removeErrorsPanel();
        errors.addAll(identityPanel.validate());
        return errors;
    }

    /**
     * @return
     */
    public List<String> fullValidate() {
        super.removeErrorsPanel();
        checkMandatorySelectField(cbxRelationship, "relationship.with.applicant");
        if(guarantorTabSheet.getSelectedTab() == identityTab){
            errors.addAll(identityPanel.fullValidate());
            if(!cbSameApplicantAddress.getValue()){
                errors.addAll(addressFormPanel.fullValidate());
            }
        }
        if(guarantorTabSheet.getSelectedTab() == employmentTab){
            errors.addAll(currentEmploymentPanel.isValid());
        }
        if(guarantorTabSheet.getSelectedTab() == otherInformationTab){
            errors.addAll(otherInformationPanel.fullValidate());
        }
        return errors;
    }

    /**
     * Reset panel
     */
    public void reset() {
        cbxRelationship.setSelectedEntity(null);
        cbSameApplicantAddress.setValue(false);
        identityPanel.reset();
        addressFormPanel.reset();
        currentEmploymentPanel.reset();
        otherInformationPanel.reset();
    }

    public void setAddress(Address address){
        this.address = address;
    }

    public IdentityPanel getIdentityPanel() {
        return identityPanel;
    }

    public CurrentEmploymentPanelMFP getCurrentEmploymentPanel() {
        return currentEmploymentPanel;
    }

    public OtherInformationPanel getOtherInformationPanel() {
        return otherInformationPanel;
    }
}
