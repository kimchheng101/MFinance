package com.soma.mfinance.core.collection.model;

import org.seuksa.frmk.model.entity.MEntityA;


/**
 * Meta data of com.soma.mfinance.core.collection.model.Reminder
 * @author kimsuor.seang
 */
public interface MReminder extends MEntityA {
	
	public final static String CONTRACT = "contract";
	public final static String COMMENT = "comment";
	public final static String DATE = "date";
	public final static String DISMISS = "dismiss";

}
