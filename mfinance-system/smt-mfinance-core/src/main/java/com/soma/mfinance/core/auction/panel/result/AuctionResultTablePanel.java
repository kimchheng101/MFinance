package com.soma.mfinance.core.auction.panel.result;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.auction.model.ContractAuctionData;
import com.soma.mfinance.core.auction.service.AuctionService;
import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.RowRenderer;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.data.Item;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.Entity;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.amount.Amount;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;

/**
 * Auction result table Panel
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AuctionResultTablePanel  extends AbstractTablePanel<Contract> implements FMEntityField {

	/** */
	private static final long serialVersionUID = 751176770913706888L;

	@Autowired
	private AuctionService auctionService;

	@PostConstruct
	public void PostConstruct() {
		setCaption(I18N.message("auctions"));
		setSizeFull();
		setMargin(true);
		setSpacing(true);
				
		super.init(I18N.message("auctions"));
		NavigationPanel navigationPanel = addNavigationPanel();
		navigationPanel.addEditClickListener(this);
		navigationPanel.addRefreshClickListener(this);
	}

	@Override
	protected Contract getEntity() {
		return null;
	}

	@Override
	protected PagedDataProvider<Contract> createPagedDataProvider() {
		PagedDefinition<Contract> pagedDefinition = new PagedDefinition<Contract>(searchPanel.getRestrictions());

		pagedDefinition.setRowRenderer(new AuctionResultRowRenderer());
		pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 70);
		pagedDefinition.addColumnDefinition("auction.status", I18N.message("auction.status"), String.class, Align.LEFT, 130);
		pagedDefinition.addColumnDefinition("storage.location", I18N.message("storage.location"), String.class, Align.LEFT, 130);
		pagedDefinition.addColumnDefinition("collection.stock.status", I18N.message("collection.stock.status"), String.class, Align.RIGHT, 130);
		pagedDefinition.addColumnDefinition("lastname.en", I18N.message("lastname.en"), String.class, Align.LEFT, 130);
		pagedDefinition.addColumnDefinition("firstname.en", I18N.message("firstname.en"), String.class, Align.LEFT, 130);
		pagedDefinition.addColumnDefinition("num.day.from.repossess", I18N.message("num.day.from.repossess"), Long.class, Align.RIGHT, 150);
		pagedDefinition.addColumnDefinition("num.of.auction", I18N.message("num.of.auction"), Long.class, Align.RIGHT, 130);
		pagedDefinition.addColumnDefinition("contract.reference", I18N.message("contract.reference"), String.class, Align.LEFT, 150);
		pagedDefinition.addColumnDefinition("financialProduct", I18N.message("financial.product"), String.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("buyer", I18N.message("buyer"), String.class, Align.LEFT, 150);
		pagedDefinition.addColumnDefinition("remaining.principal.balance", I18N.message("remaining.principal.balance"), Double.class, Align.RIGHT, 220);
		pagedDefinition.addColumnDefinition("remaining.unearned.income.balance", I18N.message("remaining.unearned.income.balance"), Double.class, Align.RIGHT, 220);
		pagedDefinition.addColumnDefinition("remaining.insurance.balance", I18N.message("remaining.insurance.balance"), Double.class, Align.RIGHT, 220);
		pagedDefinition.addColumnDefinition("remaining.service.income.balance", I18N.message("remaining.service.income.balance"), Double.class, Align.RIGHT, 220);
		pagedDefinition.addColumnDefinition("penalty.amount", I18N.message("penalty.amount"), Double.class, Align.RIGHT, 150);
		pagedDefinition.addColumnDefinition("repossession.fee", I18N.message("repossession.fee"), Double.class, Align.RIGHT, 150);
		pagedDefinition.addColumnDefinition("collection.fee", I18N.message("collection.fee"), Double.class, Align.RIGHT, 150);
		pagedDefinition.addColumnDefinition("total.amount", I18N.message("total.amount"), Double.class, Align.RIGHT, 150);
		pagedDefinition.addColumnDefinition("selling.price", I18N.message("selling.price"), Amount.class, Align.RIGHT, 150);

		pagedDefinition.addColumnDefinition("model", I18N.message("model"), String.class, Align.RIGHT, 150);
		pagedDefinition.addColumnDefinition("year", I18N.message("year"), Integer.class, Align.RIGHT, 100);
		pagedDefinition.addColumnDefinition("color", I18N.message("color"), String.class, Align.RIGHT, 100);
		pagedDefinition.addColumnDefinition("date.of.repossession", I18N.message("date.of.repossession"), Date.class, Align.RIGHT, 150);

		pagedDefinition.addColumnDefinition("engine.number", I18N.message("engine.number"), String.class, Align.RIGHT, 150);
		pagedDefinition.addColumnDefinition("chassis.number", I18N.message("chassis.number"), String.class, Align.RIGHT, 150);
		pagedDefinition.addColumnDefinition("asset.plate.number", I18N.message("plate.number"), String.class, Align.RIGHT, 150);

		pagedDefinition.addColumnDefinition("asset.price", I18N.message("asset.price"), Double.class, Align.RIGHT, 150);
		pagedDefinition.addColumnDefinition("contract.date", I18N.message("contract.date"), Date.class, Align.RIGHT, 150);
		pagedDefinition.addColumnDefinition("lease.amount", I18N.message("lease.amount"), Double.class, Align.LEFT, 150);

		pagedDefinition.addColumnDefinition("mileage", I18N.message("mileage"), Integer.class, Align.RIGHT, 100);
		pagedDefinition.addColumnDefinition("exterior.status", I18N.message("exterior.status"), String.class, Align.RIGHT, 150);
		pagedDefinition.addColumnDefinition("engine.status", I18N.message("engine.status"), String.class, Align.RIGHT, 150);
		pagedDefinition.addColumnDefinition("parts.status", I18N.message("parts.status"), String.class, Align.RIGHT, 150);
		pagedDefinition.addColumnDefinition("registration.status", I18N.message("registration.status"), String.class, Align.RIGHT, 150);
		pagedDefinition.addColumnDefinition("plate.number", I18N.message("plate.number"), String.class, Align.RIGHT, 150);

		EntityPagedDataProvider<Contract> pagedDataProvider = new EntityPagedDataProvider<Contract>();
		pagedDataProvider.setPagedDefinition(pagedDefinition);
		return pagedDataProvider;
	}

	private class AuctionResultRowRenderer implements RowRenderer {

		@SuppressWarnings("unchecked")
		@Override
		public void renderer(Item item, Entity entity) {
			Contract contract = (Contract) entity;
			ContractAuctionData auctionData = contract.getContractAuctionData();
			Collection collection = contract.getCollection();
			Asset asset = contract.getQuotation().getAsset();

			item.getItemProperty(ID).setValue(contract.getId());
			item.getItemProperty("auction.status").setValue(contract.getAuctionStatus() != null ? contract.getAuctionStatus().getDescEn() : "");
			item.getItemProperty("storage.location").setValue(contract.getStorageLocation() != null ? contract.getStorageLocation().getDescEn() : "");
			item.getItemProperty("collection.stock.status").setValue(contract.getStockStatus() != null ? contract.getStockStatus().getDescEn() : "");
			item.getItemProperty("num.day.from.repossess").setValue(auctionService.getDayFromRepossess(contract));
			item.getItemProperty("num.of.auction").setValue(auctionService.getNumberOfAuction(contract));
			item.getItemProperty("contract.reference").setValue(contract.getReference());
			item.getItemProperty("financialProduct").setValue(contract.getFinancialProduct().getDescEn());

			Double[] auctionBalance = auctionService.getRemainingServiceIncomeBalance(contract);
			item.getItemProperty("remaining.principal.balance").setValue(auctionService.getRemainingPrincipalBalance(collection));
			item.getItemProperty("remaining.unearned.income.balance").setValue(auctionBalance[2]);
			item.getItemProperty("remaining.insurance.balance").setValue(auctionBalance[1]);
			item.getItemProperty("remaining.service.income.balance").setValue(auctionBalance[0]);
			item.getItemProperty("penalty.amount").setValue(auctionService.getPenaltyAmount(collection));

			double reprocessFee = MyNumberUtils.getDouble(auctionData.getRepossessionFeeUsd() != null ? auctionData.getRepossessionFeeUsd() : 0);
			double collectionFee = MyNumberUtils.getDouble(auctionData.getCollectionFeeUsd() != null ? auctionData.getCollectionFeeUsd() : 0);
			if (auctionData != null) {
				auctionData = ENTITY_SRV.getById(ContractAuctionData.class, auctionData.getId());
				item.getItemProperty("buyer").setValue(auctionData.getBuyer() != null ? auctionData.getBuyer().getDescEn() : "");
				item.getItemProperty("repossession.fee").setValue(MyNumberUtils.getDouble(reprocessFee));
				item.getItemProperty("collection.fee").setValue(MyNumberUtils.getDouble(collectionFee));
				item.getItemProperty("selling.price").setValue(AmountUtils.convertToAmount(auctionData.getTiAssetSellingPrice() != null ? auctionData.getTiAssetSellingPrice() : 0d));
			}
			if (asset != null) {
				item.getItemProperty("model").setValue(asset.getModel().getDescEn());
				item.getItemProperty("year").setValue(asset.getModel().getYear());
				item.getItemProperty("color").setValue(asset.getColor().getDescEn());
				item.getItemProperty("date.of.repossession").setValue(contract.getContractAuctionData().getRequestRepossessedDate());
				item.getItemProperty("engine.number").setValue(asset.getEngineNumber());
				item.getItemProperty("chassis.number").setValue(asset.getChassisNumber());
				item.getItemProperty("asset.plate.number").setValue(asset.getPlateNumber());
				item.getItemProperty("asset.price").setValue(asset.getTiAssetApprPrice());
			}
			item.getItemProperty("contract.date").setValue(contract.getCreationDate());
			item.getItemProperty("lease.amount").setValue(MyNumberUtils.getDouble(contract.getTiFinancedAmount()));
			item.getItemProperty("total.amount").setValue(MyNumberUtils.getDouble(reprocessFee + collectionFee));

			Applicant applicant = contract.getQuotation().getMainApplicant();

			item.getItemProperty("lastname.en").setValue(applicant.getLastNameEn());
			item.getItemProperty("firstname.en").setValue(applicant.getFirstNameEn());
			item.getItemProperty("mileage").setValue(auctionData.getMilleage());

			item.getItemProperty("exterior.status").setValue(auctionData.getAssetExteriorStatus() != null ?
					auctionData.getAssetExteriorStatus().getDescEn() : "");
			item.getItemProperty("engine.status").setValue(auctionData.getAssetEnginStatus() != null ?
					auctionData.getAssetEnginStatus().getDescEn() : "");
			item.getItemProperty("parts.status").setValue(auctionData.getAssetPartsStatus() != null ?
					auctionData.getAssetPartsStatus().getDescEn() : "");
			item.getItemProperty("registration.status").setValue(auctionData.getAssetRegistrationStatus()!= null ?
					auctionData.getAssetRegistrationStatus().getDescEn() : "");
			item.getItemProperty("plate.number").setValue(auctionData.getAssetPlateNumber() != null ?
					auctionData.getAssetPlateNumber().getDescEn() : "");
		}
	}

	/**
	 * @see AbstractTablePanel#createSearchPanel()
	 */
	@Override
	protected AbstractSearchPanel<Contract> createSearchPanel() {
		return new AuctionResultSearchPanel(this);
	}
	
}
