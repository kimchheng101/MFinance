package com.soma.mfinance.core.common.security.model;

/**
 * Meta data of com.soma.mfinance.core.common.security.model.SecUserDeptLevel
 * @author kimsuor.seang
 */
public interface MSecUserDeptLevel {
	
	public final static String SECUSER = "secUser";
	public final static String DEBTLEVEL = "debtLevel";

}
