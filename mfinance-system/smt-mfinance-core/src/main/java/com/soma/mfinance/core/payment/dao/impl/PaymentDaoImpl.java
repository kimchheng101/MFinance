package com.soma.mfinance.core.payment.dao.impl;

import org.seuksa.frmk.dao.impl.BaseEntityDaoImpl;
import org.springframework.stereotype.Repository;

import com.soma.mfinance.core.payment.dao.PaymentDao;

/**
 * Payment data access implementation
 * @author kimsuor.seang
 *
 */
@Repository
public class PaymentDaoImpl extends BaseEntityDaoImpl implements PaymentDao {

}
