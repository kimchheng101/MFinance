package com.soma.mfinance.core.applicant.panel.employee;


import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.applicant.model.Employment;
import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.mfinance.core.applicant.panel.CurrentEmploymentPanelKFP;
import com.soma.mfinance.core.applicant.panel.SecondaryEmploymentPanelKFP;
import com.soma.ersys.core.hr.model.eref.EEmploymentType;
import com.soma.frmk.vaadin.ui.panel.AbstractControlPanel;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.CrudAction;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by kimsuor.seang  on 10/14/2017.
 */
public class EmploymentPanelKFP extends AbstractControlPanel {

    private static final long serialVersionUID = 8485010240363814186L;

    private CurrentEmploymentPanelKFP currentEmploymentPanelKFP;
    private SecondaryEmploymentPanelKFP secondaryEmploymentPanel1;
    private SecondaryEmploymentPanelKFP secondaryEmploymentPanel2;
    private SecondaryEmploymentPanelKFP secondaryEmploymentPanel3;
    private SecondaryEmploymentPanelKFP secondaryEmploymentPanel4;
    private final Panel currentEmploymentLayout;

    private List<Employment> employments;

    public EmploymentPanelKFP() {
        setSizeFull();
        setMargin(true);

        currentEmploymentPanelKFP = new CurrentEmploymentPanelKFP();
        secondaryEmploymentPanel1 = new SecondaryEmploymentPanelKFP();
        secondaryEmploymentPanel2 = new SecondaryEmploymentPanelKFP();

        secondaryEmploymentPanel3 = new SecondaryEmploymentPanelKFP();
        secondaryEmploymentPanel4 = new SecondaryEmploymentPanelKFP();

        employments = new ArrayList<>();

        currentEmploymentLayout = new Panel(I18N.message("current.employment.main.source"));
        currentEmploymentLayout.setContent(currentEmploymentPanelKFP);

        final HorizontalLayout secondaryEmploymentLayout = new HorizontalLayout();
        secondaryEmploymentLayout.setSpacing(true);

        final HorizontalLayout secondaryEmploymentLayout2 = new HorizontalLayout();
        secondaryEmploymentLayout.setSpacing(true);

        final Panel secondaryEmploymentPanelLayout = new Panel(I18N.message("secondary.employment") + " 1");
        secondaryEmploymentPanelLayout.setContent(secondaryEmploymentPanel1);

        final Panel secondaryEmploymentPanelLayout2 = new Panel(I18N.message("secondary.employment") + " 2");
        secondaryEmploymentPanelLayout2.setContent(secondaryEmploymentPanel2);

        final Panel secondaryEmploymentPanelLayout3 = new Panel(I18N.message("secondary.employment") + " 3");
        secondaryEmploymentPanelLayout3.setContent(secondaryEmploymentPanel3);

        final Panel secondaryEmploymentPanelLayout4 = new Panel(I18N.message("secondary.employment") + " 4");
        secondaryEmploymentPanelLayout4.setContent(secondaryEmploymentPanel4);

        secondaryEmploymentLayout.addComponent(secondaryEmploymentPanelLayout);
        secondaryEmploymentLayout.addComponent(secondaryEmploymentPanelLayout2);

        secondaryEmploymentLayout2.addComponent(secondaryEmploymentPanelLayout3);
        secondaryEmploymentLayout2.addComponent(secondaryEmploymentPanelLayout4);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        verticalLayout.setSpacing(true);
        verticalLayout.addComponent(currentEmploymentLayout);
        verticalLayout.addComponent(secondaryEmploymentLayout);
        verticalLayout.addComponent(secondaryEmploymentLayout2);
        addComponent(verticalLayout);
    }

    public void assignValues(Individual individual) {
        Employment currentEmployment = individual.getCurrentEmployment();
        currentEmploymentPanelKFP.setApplicant(individual, EApplicantType.C);
        if (currentEmployment != null) {
            currentEmploymentPanelKFP.assignValues(currentEmployment);
        } else {
            currentEmploymentPanelKFP.reset();
        }
        List<Employment> secondaryEmployments = individual.getEmployments(EEmploymentType.SECO);
        if (secondaryEmployments != null && !secondaryEmployments.isEmpty()) {
            secondaryEmploymentPanel1.assignValues(secondaryEmployments.get(0));
            if (secondaryEmployments.size() > 1) {
                secondaryEmploymentPanel2.assignValues(secondaryEmployments.get(1));
            } else if (secondaryEmployments.size() > 2) {
                secondaryEmploymentPanel3.assignValues(secondaryEmployments.get(2));
            } else if (secondaryEmployments.size() > 3) {
                secondaryEmploymentPanel4.assignValues(secondaryEmployments.get(3));
            }
        }
    }

    public void getEmployments(Individual individual) {
        Employment currentEmployment = individual.getCurrentEmployment();
        if (currentEmployment == null) {
            currentEmployment = new Employment();
        }
        employments.add(currentEmploymentPanelKFP.getEmployment(currentEmployment));

        List<Employment> secondaryEmployments = individual.getEmployments(EEmploymentType.SECO);
        if (secondaryEmployments == null) {
            secondaryEmployments = new ArrayList<>();
        }
        if (secondaryEmployments != null && !secondaryEmployments.isEmpty()) {
            getSecondaryEmployment(0, secondaryEmploymentPanel1, secondaryEmployments);
            getSecondaryEmployment(1, secondaryEmploymentPanel2, secondaryEmployments);
            getSecondaryEmployment(2, secondaryEmploymentPanel3, secondaryEmployments);
            getSecondaryEmployment(3, secondaryEmploymentPanel4, secondaryEmployments);
        } else {
            Employment prevEmployment1 = secondaryEmploymentPanel1.getEmployment(new Employment());
            if (prevEmployment1 != null) {
                employments.add(prevEmployment1);
            }
            Employment secondaryEmployment2 = secondaryEmploymentPanel2.getEmployment(new Employment());
            if (secondaryEmployment2 != null) {
                employments.add(secondaryEmployment2);
            }
            Employment secondaryEmployment3 = secondaryEmploymentPanel3.getEmployment(new Employment());
            if (secondaryEmployment3 != null) {
                employments.add(secondaryEmployment3);
            }
            Employment secondaryEmployment4 = secondaryEmploymentPanel4.getEmployment(new Employment());
            if (secondaryEmployment4 != null) {
                employments.add(secondaryEmployment4);
            }
        }
        individual.setEmployments(employments);
    }

    private void getSecondaryEmployment(int index, SecondaryEmploymentPanelKFP secondaryEmploymentPanel, List<Employment> employments) {
        if (index < employments.size()) {
            Employment secondaryEmployment = secondaryEmploymentPanel.getEmployment(employments.get(index));
            if (secondaryEmployment == null) {
                employments.get(index).setCrudAction(CrudAction.DELETE);
            }
        } else {
            Employment prevEmployment = secondaryEmploymentPanel.getEmployment(new Employment());
            if (prevEmployment != null) {
                employments.add(prevEmployment);
            }
        }
    }

    public List<String> fullValidate() {
        super.reset();
        return errors;
    }

    public void reset() {
        currentEmploymentPanelKFP.reset();
        secondaryEmploymentPanel1.reset();
        secondaryEmploymentPanel2.reset();
        secondaryEmploymentPanel3.reset();
        secondaryEmploymentPanel4.reset();
    }

    public CurrentEmploymentPanelKFP getCurrentEmploymentPanel() {
        return currentEmploymentPanelKFP;
    }

    public void setCurrentEmploymentPanel(CurrentEmploymentPanelKFP currentEmploymentPanel) {
        this.currentEmploymentPanelKFP = currentEmploymentPanel;
    }

    public Panel getCurrentEmploymentLayout() {
        return currentEmploymentLayout;
    }
}
