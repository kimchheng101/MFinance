package com.soma.mfinance.core.quotation;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.quotation.model.Comment;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationExtModule;
import com.soma.mfinance.core.shared.exception.ValidationFieldsException;
import com.soma.mfinance.core.shared.exception.quotation.ExpireQuotationException;
import com.soma.mfinance.core.shared.exception.quotation.InvalidQuotationException;
import com.soma.mfinance.third.creditbureau.exception.ErrorCreditBureauException;
import com.soma.mfinance.third.creditbureau.exception.InvokedCreditBureauException;
import com.soma.mfinance.third.creditbureau.exception.ParserCreditBureauException;
import com.soma.frmk.security.model.SecUser;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.service.BaseEntityService;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Quotation service interface
 *
 * @author kimsuor.seang
 */
public interface QuotationService extends BaseEntityService {

    /**
     * Create a new quotation
     *
     * @param quotation
     * @return
     */
    Quotation saveOrUpdateQuotation(Quotation quotation);

    /**
     * @param quotation
     * @return
     */
    Quotation saveChangeGuarantor(Quotation quotation);

    /**
     * Call credit bureau interface
     *
     * @param quotaId
     * @param parameters
     */
    Boolean invokeCreditBureau(Long quotaId, Map<String, Object> parameters) throws InvokedCreditBureauException, ErrorCreditBureauException, ParserCreditBureauException;

    /**
     * @param quotation
     * @return
     */
    boolean isGuarantorRequired(Quotation quotation);

    /**
     * Get Quotation object by it's reference
     *
     * @param reference
     * @return
     */
    Quotation getByReference(String reference);

    /**
     * Submit quotation to underwriter
     *
     * @param quotation
     * @return
     * @throws ValidationFieldsException
     * @throws InvalidQuotationException
     * @throws ExpireQuotationException
     */
    boolean submitQuotation(Quotation quotation);

    /**
     * Change quotation status
     *
     * @param quotation
     * @param newStatus
     * @return
     */
    EWkfStatus changeQuotationStatus(Quotation quotation, EWkfStatus newStatus);

    /**
     * Change quotation status
     *
     * @param quotaId
     * @param newStatus
     * @return
     */
    EWkfStatus changeQuotationStatus(Long quotaId, EWkfStatus newStatus);


    /**
     * @param quotation
     * @param comments
     */
    void saveUnderwriter(Quotation quotation, List<Comment> comments);

    /**
     * @param quotation
     * @param newStatus
     */
    void saveUnderwriterDecision(Quotation quotation, EWkfStatus newStatus, List<Comment> comments);

    /**
     * @param quotation
     * @param newStatus
     */
    void saveManagementDecision(Quotation quotation, EWkfStatus newStatus);

    /**
     * @param quotation
     */
    void saveDocumentControllerDecision(Quotation quotation);

    /**
     * @param quotaId
     */
    void activateQuotation(Long quotaId, Date contractStartDate, Date firstPaymentDate);

    /**
     * @param quotation
     */
    void decline(Quotation quotation);

    /**
     * @param quotation
     */
    void reject(Quotation quotation);

    /**
     * @param quotation
     */
    void changeAsset(Quotation quotation);

    /**
     * Save or update quotation documents
     *
     * @param quotation
     */
    void saveOrUpdateQuotationDocuments(Quotation quotation);

    /**
     * Save or update quotation support decisions
     *
     * @param quotation
     */
    void saveOrUpdateQuotationSupportDecisions(Quotation quotation);

    /**
     * @param quotaId
     * @param applicanType
     * @return
     */
    List<QuotationExtModule> getQuotationExtModules(Long quotaId, EApplicantType applicanType);

    /**
     * Get quotations list by commune address
     */
    List<Quotation> getQuotationsByCommune(Long commuId);

    /**
     * @param quotation
     * @return
     */
    boolean isPrintedPurchaseOrder(Quotation quotation);

    Quotation getQuoatationByContractReference(Long contractId);

    List<Quotation> getQuotationByContractStatus();

    List<Quotation> getQuotationByDealer(Long dealerId, Date startDate, Date endDate);

    boolean backToPOS(Quotation quotation);

    Quotation changeQuotationNextStatus(Quotation quotation);

    Comment getLastComment(Quotation quotation, boolean isUnderWriter, boolean isUnderWriterSupervisor);

    List<Quotation> getListQuotation(BaseRestrictions<Quotation> criteria);

    List<SecUser> getSecUserByProfile(long profile);

    SecUser getUnderwriterAndSupervisorToAssign(List<SecUser> secUsers);

    List<Quotation> getCapacityUWandUS(SecUser secUser, long profile, Date start, Date end);

    void autoAssignOrUpdateQuotationTOUWAndUS(Quotation quotation);

    Double calculateApplicantRatio(Quotation quotation);

    String getRatioColor(double applicantRatio);

    boolean isAlreadyApproved(Quotation quotation);
}
