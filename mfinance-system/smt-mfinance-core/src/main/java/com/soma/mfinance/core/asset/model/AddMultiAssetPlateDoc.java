package com.soma.mfinance.core.asset.model;

import com.soma.mfinance.core.quotation.model.Quotation;
import org.seuksa.frmk.model.entity.EntityA;
import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;

/**
 * @author tha.bunsath
 * 
 */
@Entity
@Table(name = "td_multi_asset_plate_doc")
public class AddMultiAssetPlateDoc extends EntityA {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6266355913081605458L;
	/** */
	private Quotation quotation;
	private String assetPlateDc;
	/**
	 * @return the
	 */
	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "amti_plate_id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}
	/* 
	 * @return
	 */
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "quota_id")
	public Quotation getQuotation() {
		return quotation;
	}
	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}
	/**
	 * 
	 * @return
	 */
	@Column(name = "amti_plate_dc", nullable = true, length = 100)
	public String getAssetPlateDc() {
		return assetPlateDc;
	}
	public void setAssetPlateDc(String assetPlateDc) {
		this.assetPlateDc = assetPlateDc;
	}
}
