package com.soma.mfinance.core.payment.service.impl;

import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.quotation.SequenceGenerator;

/**
 * Default Sequence generator 
 * @author kimsuor.seang
 *
 */
public class ReceiptGeneratorImpl implements SequenceGenerator {

	private String type;
	private Long sequence;
	private Dealer dealer;
	
	/**
	 * @param dealer
	 */
	public ReceiptGeneratorImpl(Dealer dealer, String type, Long sequence) {
		this.dealer = dealer;
		this.type = type;
		this.sequence = sequence;
	}
	
	@Override
	public String generate() {
		String sequenceNumber = "00000000" + sequence;
		return dealer.getCode() + "-" + type + "-" + sequenceNumber.substring(sequenceNumber.length() - 6);
	}
}
