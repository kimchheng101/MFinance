package com.soma.mfinance.core.document.panel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import com.soma.mfinance.core.custom.vaadin.CustomDownloadStream;
import com.vaadin.server.DownloadStream;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.Window;

public class DocumentViewver extends Window {

	private static final long serialVersionUID = 8093301531446556388L;

	/**
	 * @param tiltle
	 * @param filePathName
	 */
	public DocumentViewver(final String tiltle, final String filePathName) {
		super(tiltle);
		setModal(true);
        setResizable(true);
        setWidth("800px");
        setHeight("600px");
        center();
        BrowserFrame browserFrame = new BrowserFrame();
        File file=new File(filePathName);
        StreamSource source = new StreamSource() {
        	private static final long serialVersionUID = 3558447485541502851L;
			@Override
			public InputStream getStream() {
                InputStream is = null;
				try {
					is = new FileInputStream(file);
				} catch (FileNotFoundException e) {
					is = null;
				}
                return is;
			}
		};
		StreamResource streamResource = new StreamResource(source, file.getName()) {
			@Override
			public DownloadStream getStream() {
				final StreamSource ss = getStreamSource();
				if (ss == null) {
					return null;
				}
				final DownloadStream ds = new CustomDownloadStream(ss.getStream(),
						getMIMEType(), getFilename(), (int) file.length());
				ds.setBufferSize(getBufferSize());
				ds.setCacheTime(DownloadStream.DEFAULT_CACHETIME);
				return ds;
			}
		};
		browserFrame.setSource(streamResource);
		browserFrame.setSizeFull();
        setContent(browserFrame);
	}
}
