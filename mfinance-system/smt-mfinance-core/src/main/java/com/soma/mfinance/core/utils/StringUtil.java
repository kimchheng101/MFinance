package com.soma.mfinance.core.utils;

import org.seuksa.frmk.tools.MyStringUtils;

/**
 * @author by kimsuor.seang  on 11/28/2017.
 */
public class StringUtil extends MyStringUtils {

    public static String STRING_NULL_VALUE = "";

    public static String stringValue(String str){
        if(str != "" &&  !str.equals("")){
            return str;
        }
        return STRING_NULL_VALUE;
    }



}
