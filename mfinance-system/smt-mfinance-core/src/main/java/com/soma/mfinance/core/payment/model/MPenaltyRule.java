package com.soma.mfinance.core.payment.model;

import org.seuksa.frmk.model.entity.MEntityA;

/**
 * @author by kimsuor.seang  on 11/14/2017.
 */
public interface MPenaltyRule extends MEntityA {
    String TI_PENALTY_AMOUNT_PER_DAY_USD = "tiPenaltyAmounPerDaytUsd";
    String VAT = "vat";
}
