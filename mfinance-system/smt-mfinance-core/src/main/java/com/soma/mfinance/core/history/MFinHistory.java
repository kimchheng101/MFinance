package com.soma.mfinance.core.history;

import org.seuksa.frmk.model.entity.MEntityA;

/**
 * Meta data of com.soma.mfinance.core.history.FinHistory
 * @author kimsuor.seang
 */
public interface MFinHistory extends MEntityA {

	// For Vaadin Grid
	public final static String DATE = "date";
	public final static String TIME = "time";
	public final static String DETAIL = "detail";
	public final static String USER = "user";
	
	public final static String CONTRACT = "contract";
	public final static String TYPE = "type";
	public final static String COMMENT = "comment";
	
	public final static String EVENT = "event";
	
}
