package com.soma.mfinance.core.asset.model.appraisal;

import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;

/**
 * @author by kimsuor.seang  on 27/10/2017.
 */

@Entity
@Table(name = "td_history_item")
public class HistoryItem extends EntityRefA {

	private static final long serialVersionUID = -8697178565488091667L;
	private AssetAppraisal  assetAppraisal;
    private AssetHistoryItem assetHistoryItem;
    private Integer percent;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "his_item_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ass_app_id")
    public AssetAppraisal getAssetAppraisal() {
        return assetAppraisal;
    }
    public void setAssetAppraisal(AssetAppraisal assetAppraisal) {
        this.assetAppraisal = assetAppraisal;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ass_his_item_id")
    public AssetHistoryItem getAssetHistoryItem() {
        return assetHistoryItem;
    }

    public void setAssetHistoryItem(AssetHistoryItem assetHistoryItem) {
        this.assetHistoryItem = assetHistoryItem;
    }

    @Column(name = "his_percent", nullable = true)
    public Integer getPercent() {
        return percent;
    }

    public void setPercent(Integer percent) {
        this.percent =  percent;
    }

}
