package com.soma.mfinance.core.asset.panel.appraisal;

/**
 * Created by cheasocheat on 3/24/17.
 */
public interface AppraisalConstant {
    //Asset
    String ASS_SEC_PRICE = "SMMP";
    String ASS_ORI_PRICE = "ONSP";

    //Other
    String PHNOM_PENH_CODE = "12";
    String BLACK_COLOR_CODE = "1";
    double PHNOM_PENH_VALUE = 1;
    double BLACK_VALUE = 1;
    double OTHER = 0;
    double CONST_VAL = 1;
    String ORI_MOTO_APP_PRICE = "OMA";
    String MOTO_APP_PRICE = "MA";

    //New Appraisal Criteria Constant
    String CONSTANT = "CT";
    String SEL_PRICE = "SP";
    String MOTO_MILEAGE = "MM";
    String USAGE_PERIOD = "UP";
    String MOTO_COLOR = "MC";
    String PLATE_NUM = "PN";
    String MOTO_BATTERY = "MB";
    String EXTER_STS = "ES";
    String ENGINE = "EG";
    String SPEED_BOARD = "SB";
    String FRAME = "FR";
    String FRONT_SHOCK = "FS";
    String REAR_SHOCK = "RS";
    String TIRE = "TI";
    String REAR_MIRROR = "RM";
    String KICK_STARTER = "KS";
    String SIDE_COVER = "SC";
    String REAR_TAIL_LIGHT = "RTL";
    String SEAT_HANDLER = "SH";
    String FRONT_BASKET_STAND = "FBS";
    String EXAUST_PIPE = "EP";
    String FRONT_LAMP = "FL";
    String FRONT_FENDER= "FF";
    String REAR_FENDER = "RF";


    //Apprisal Prefix
    String REMARK = "RM_";
    String ADJUST = "AJ_";
    String CRITERIA = "CR_";


    //SettingConfig
    String IS_ROUND_UP = "ass.groupdetail.isroundup";
    String ROUND_UP_DIGIT = "ass.groupdetail.digitval";
    String MIN_PRICE = "MIN_AMOUNT";
    String APPRAISAL_CATEGORY ="appraisalCategory";
    String SORT_INDEX =  "sortIndex";
    String APPRAISAL_MOTO_COLOR = "APR_MCOL";
    String APPRAISAL_PLAT_NUMBER = "APR_PLANUM";
    String APPRAISAL_ORIG_NEW_SELLING_PRICE = "NS_PRICE";
    String APPRAISAL_CONSTANST = "CONST";
    String ASSET_DREAM_CODE = "012005";
    String ASSET_WAVE_CODE = "012008";
    String APPRAISAL_DREAM = "DREAM";
    String APPRAISAL_NON_DREAM = "NON-DREAM";

    String ASSET_TRACTOR_CODE ="B002";
    String ASSET_COMBINE_HARVESTER_CODE ="B003";
    String ASSET_WALKING_TRACTOR_CODE ="B004";
}
