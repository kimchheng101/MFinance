package com.soma.mfinance.core.accounting.services;

import com.soma.mfinance.core.accounting.dto.RemainingBalanceDTO;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.quotation.model.Quotation;
import org.seuksa.frmk.service.BaseEntityService;

import java.util.Date;
import java.util.List;

/**
 * @author by kimsuor.seang  on 11/24/2017.
 */
public interface AccountingService extends BaseEntityService {

    List<Contract> getContractsRemainingBalance(EDealerType dealerType, Dealer dealer, String reference, Date endDate);

    List<RemainingBalanceDTO> getLeaseTransactionsRemainingBalance(EDealerType dealerType, Dealer dealer, String reference, Date endDate);

    List<Cashflow> getCashflowsPaid(Contract contract);

    Double getPrincipalBalance(Quotation quotation, Date data, List<Cashflow> cashflows);

    Double getTotalInterestRate(Quotation  quotation);

    Double getTotalEarningInterest(List<Cashflow> cashflows, Date calculDate);

    Double getTotalUnearnedInterest(Double totalInterest , Double totalEarningInterest);
}
