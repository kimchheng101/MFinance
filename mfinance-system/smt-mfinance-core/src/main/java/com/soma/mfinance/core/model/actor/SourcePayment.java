package com.soma.mfinance.core.model.actor;

import com.soma.mfinance.core.applicant.model.Individual;
import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;

/**
 * Created by th.seng on 10/19/2017.
 */

@Entity
@Table(name = "td_source_payment")
public class SourcePayment extends EntityA {

    private static final long serialVersionUID = -2369046431848996302L;

    private Individual individual;

    private Double personalMoney;
    private Double otherMoney;

    private String sourceOfMoneyLabel1;
    private String sourceOfMoneyLabel2;
    private String sourceOfMoneyLabel3;
    private Double sourceOfMoney1;
    private Double sourceOfMoney2;
    private Double sourceOfMoney3;

    private String relationshipLabel1;
    private String relationshipLabel2;
    private Double relationship1;
    private Double relationship2;

    private String contactNumber1;
    private String contactNumber2;


    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "sopay_id", unique = true, nullable = false)
    public Long getId() {
        // TODO Auto-generated method stub
        return id;
    }

    /**
     * @return the personalMoney
     */
    @Column(name = "sopay_personal_money", nullable = true)
    public Double getPersonalMoney() {
        return personalMoney;
    }

    /**
     * @param personalMoney the personalMoney to set
     */
    public void setPersonalMoney(Double personalMoney) {
        this.personalMoney = personalMoney;
    }

    /**
     * @return the otherMoney
     */
    @Column(name = "sopay_other_money", nullable = true)
    public Double getOtherMoney() {
        return otherMoney;
    }

    /**
     * @param otherMoney the otherMoney to set
     */
    public void setOtherMoney(Double otherMoney) {
        this.otherMoney = otherMoney;
    }

    /**
     * @return the sourceOfMoneyLabel1
     */
    @Column(name = "sopay_source_money_label1", nullable = true)
    public String getSourceOfMoneyLabel1() {
        return sourceOfMoneyLabel1;
    }

    /**
     * @param sourceOfMoneyLabel1 the sourceOfMoneyLabel1 to set
     */
    public void setSourceOfMoneyLabel1(String sourceOfMoneyLabel1) {
        this.sourceOfMoneyLabel1 = sourceOfMoneyLabel1;
    }

    /**
     * @return the sourceOfMoneyLabel2
     */
    @Column(name = "sopay_source_money_label2", nullable = true)
    public String getSourceOfMoneyLabel2() {
        return sourceOfMoneyLabel2;
    }

    /**
     * @param sourceOfMoneyLabel2 the sourceOfMoneyLabel2 to set
     */
    public void setSourceOfMoneyLabel2(String sourceOfMoneyLabel2) {
        this.sourceOfMoneyLabel2 = sourceOfMoneyLabel2;
    }

    /**
     * @return the sourceOfMoneyLabel3
     */
    @Column(name = "sopay_source_money_label3", nullable = true)
    public String getSourceOfMoneyLabel3() {
        return sourceOfMoneyLabel3;
    }

    /**
     * @param sourceOfMoneyLabel3 the sourceOfMoneyLabel3 to set
     */
    public void setSourceOfMoneyLabel3(String sourceOfMoneyLabel3) {
        this.sourceOfMoneyLabel3 = sourceOfMoneyLabel3;
    }

    /**
     * @return the sourceOfMoney1
     */
    @Column(name = "sopay_source_money1", nullable = true)
    public Double getSourceOfMoney1() {
        return sourceOfMoney1;
    }

    /**
     * @param sourceOfMoney1 the sourceOfMoney1 to set
     */
    public void setSourceOfMoney1(Double sourceOfMoney1) {
        this.sourceOfMoney1 = sourceOfMoney1;
    }

    /**
     * @return the sourceOfMoney2
     */
    @Column(name = "sopay_source_money2", nullable = true)
    public Double getSourceOfMoney2() {
        return sourceOfMoney2;
    }

    /**
     * @param sourceOfMoney2 the sourceOfMoney2 to set
     */
    public void setSourceOfMoney2(Double sourceOfMoney2) {
        this.sourceOfMoney2 = sourceOfMoney2;
    }

    /**
     * @return the sourceOfMoney3
     */
    @Column(name = "sopay_source_money3", nullable = true)
    public Double getSourceOfMoney3() {
        return sourceOfMoney3;
    }

    /**
     * @param sourceOfMoney3 the sourceOfMoney3 to set
     */
    public void setSourceOfMoney3(Double sourceOfMoney3) {
        this.sourceOfMoney3 = sourceOfMoney3;
    }

    /**
     * @return the relationshipLabel1
     */
    @Column(name = "sopay_relationship_label1", nullable = true)
    public String getRelationshipLabel1() {
        return relationshipLabel1;
    }

    /**
     * @param relationshipLabel1 the relationshipLabel1 to set
     */
    public void setRelationshipLabel1(String relationshipLabel1) {
        this.relationshipLabel1 = relationshipLabel1;
    }

    /**
     * @return the relationshipLabel2
     */
    @Column(name = "sopay_relationship_label2", nullable = true)
    public String getRelationshipLabel2() {
        return relationshipLabel2;
    }

    /**
     * @param relationshipLabel2 the relationshipLabel2 to set
     */
    public void setRelationshipLabel2(String relationshipLabel2) {
        this.relationshipLabel2 = relationshipLabel2;
    }

    /**
     * @return the relationship1
     */
    @Column(name = "sopay_relationship1", nullable = true)
    public Double getRelationship1() {
        return relationship1;
    }

    /**
     * @param relationship1 the relationship1 to set
     */
    public void setRelationship1(Double relationship1) {
        this.relationship1 = relationship1;
    }

    /**
     * @return the relationship2
     */
    @Column(name = "sopay_relationship2", nullable = true)
    public Double getRelationship2() {
        return relationship2;
    }

    /**
     * @param relationship2 the relationship2 to set
     */
    public void setRelationship2(Double relationship2) {
        this.relationship2 = relationship2;
    }


    /**
     * @return the contactNumber1
     */
    @Column(name = "sopay_contact_number1", nullable = true)
    public String getContactNumber1() {
        return contactNumber1;
    }

    /**
     * @param contactNumber1 the contactNumber1 to set
     */
    public void setContactNumber1(String contactNumber1) {
        this.contactNumber1 = contactNumber1;
    }

    /**
     * @return the contactNumber2
     */
    @Column(name = "sopay_contact_number2", nullable = true)
    public String getContactNumber2() {
        return contactNumber2;
    }

    /**
     * @param contactNumber2 the contactNumber2 to set
     */
    public void setContactNumber2(String contactNumber2) {
        this.contactNumber2 = contactNumber2;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ind_id")
    public Individual getIndividual() {
        return individual;
    }

    public void setIndividual(Individual individual) {
        this.individual = individual;
    }
}
