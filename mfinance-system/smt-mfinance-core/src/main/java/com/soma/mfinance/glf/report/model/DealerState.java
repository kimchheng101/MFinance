package com.soma.mfinance.glf.report.model;

import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.quotation.model.Quotation;
import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;

/**
 * Created by Dang Dim
 * Date     : 26-Jul-17, 3:20 PM
 * Email    : d.dim@gl-f.com
 */
@Entity
@Table(name = "ts_dealer_state")
public class DealerState extends EntityA {

    private Dealer dealer;
    private EReportTamplate reportTamplate;
    private Long stateNumber;
    private Quotation quotation;


    public DealerState() {
    }

    public DealerState(Dealer dealer, EReportTamplate reportTamplate, Long stateNumber) {
        this.dealer = dealer;
        this.reportTamplate = reportTamplate;
        this.stateNumber = stateNumber;
    }

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "state_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @ManyToOne
    @JoinColumn(name = "dea_id")
    public Dealer getDealer() {
        return dealer;
    }

    public void setDealer(Dealer dealer) {
        this.dealer = dealer;
    }

    @Column(name = "rep_id")
    @Convert(converter = EReportTamplate.class)
    public EReportTamplate getReportTamplate() {
        return reportTamplate;
    }

    public void setReportTamplate(EReportTamplate reportTamplate) {
        this.reportTamplate = reportTamplate;
    }

    @Column(name = "state_number")
    public Long getStateNumber() {
        return stateNumber;
    }

    public void setStateNumber(Long stateNumber) {
        this.stateNumber = stateNumber;
    }

}
