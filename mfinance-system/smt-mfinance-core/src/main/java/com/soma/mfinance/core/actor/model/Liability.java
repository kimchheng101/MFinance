package com.soma.mfinance.core.actor.model;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.Individual;
import org.seuksa.frmk.model.entity.EntityA;

import javax.persistence.*;

/**
 * @author by kimsuor.seang  on 10/13/2017.
 */
@Entity
@Table(name = "td_liability")
public class Liability extends EntityA {

    private static final long serialVersionUID = -2369046431848996302L;

    private Double annualPayment;
    private Double remainingLoan;
    private Integer timingLoan;
    private String creditor;
    private String purposeOfUse;
    private Individual individual;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "liabi_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name = "liabi_remaining_loan", nullable = true)
    public Double getRemainingLoan() {
        return remainingLoan;
    }


    public void setRemainingLoan(Double remainingLoan) {
        this.remainingLoan = remainingLoan;
    }
    @Column(name = "liabi_timing_loan", nullable = true)
    public Integer getTimingLoan() {
        return timingLoan;
    }
    public void setTimingLoan(Integer timingLoan) {
        this.timingLoan = timingLoan;
    }

    @Column(name = "liabi_creditor", nullable = true)
    public String getCreditor() {
        return creditor;
    }
    public void setCreditor(String creditor) {
        this.creditor = creditor;
    }

    @Column(name = "liabi_purpose_use", nullable = true)
    public String getPurposeOfUse() {
        return purposeOfUse;
    }
    public void setPurposeOfUse(String purposeOfUse) {
        this.purposeOfUse = purposeOfUse;
    }

    @Column(name = "liabi_annual_repayment", nullable = true)
    public Double getAnnualPayment() {
        return annualPayment;
    }
    public void setAnnualPayment(Double annualPayment) {
        this.annualPayment = annualPayment;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ind_id")
    public Individual getIndividual() {
        return individual;
    }

    public void setIndividual(Individual individual) {
        this.individual = individual;
    }
}
