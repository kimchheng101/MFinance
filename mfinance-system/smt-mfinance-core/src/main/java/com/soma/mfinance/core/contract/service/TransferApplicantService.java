package com.soma.mfinance.core.contract.service;

import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.service.aftersales.TransferApplicantSimulateRequest;
import org.seuksa.frmk.service.BaseEntityService;

/**
 * Transfer Applicant Service
 * @author kimsuor.seang
 */
public interface TransferApplicantService extends BaseEntityService {

	/**
	 * @param request
	 * @return
	 */
	Contract simulate(TransferApplicantSimulateRequest request);
	
	/**
	 * @param contract
	 * @param force
	 * @return
	 */
	public Contract validate(Contract contract, boolean forceActivated);
	
	/**
	 * @param contract
	 * @return
	 */
	public Contract cancel(Contract contract);
}
