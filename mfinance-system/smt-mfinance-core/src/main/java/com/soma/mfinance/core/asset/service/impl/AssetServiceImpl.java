package com.soma.mfinance.core.asset.service.impl;

import com.soma.mfinance.core.asset.dao.AssetDao;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.asset.model.MAsset;
import com.soma.mfinance.core.asset.model.appraisal.Appraisal;
import com.soma.mfinance.core.asset.model.appraisal.AssetAppraisal;
import com.soma.mfinance.core.asset.service.AssetService;
import com.soma.mfinance.core.collection.model.ContractFlag;
import com.soma.mfinance.core.collection.service.ContractFlagRestriction;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.shared.exception.ValidationFields;
import com.soma.mfinance.core.shared.exception.ValidationFieldsException;
import com.soma.mfinance.core.shared.system.DomainType;
import com.soma.mfinance.core.asset.dao.AssetDao;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.asset.model.MAsset;
import com.soma.mfinance.core.asset.model.appraisal.Appraisal;
import com.soma.mfinance.core.asset.model.appraisal.AssetAppraisal;
import com.soma.mfinance.core.collection.model.ContractFlag;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.CrudAction;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.seuksa.frmk.tools.exception.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

/**
 * Asset service
 *
 * @author kimsuor.seang
 */
@Service("assetService")
public class AssetServiceImpl extends BaseEntityServiceImpl implements AssetService, MAsset {
    /** */
    private static final long serialVersionUID = -1027766663950218887L;

    @Autowired
    private AssetDao dao;


    @Override
    public AssetDao getDao() {
        return dao;
    }


    @Override
    public Asset saveOrUpdateAsset(Asset asset) {
        try {
            Assert.notNull(asset, "Asset could not be null.");
            if (asset.getModel() != null) {
                saveOrUpdate(asset);
                if(asset.getAssetAppraisal() != null) {
                    saveOrUpdateAppraisal(asset.getAssetAppraisal());
                }
            }
            /*else {
                Notification.show("", "Please fill asset information!", Notification.Type.HUMANIZED_MESSAGE);
            }*/
        } catch (DaoException e) {
            clear();
            saveOrUpdate(asset);
        }
        return asset;
    }

    /**
     * Control masset
     *
     * @param asset
     */
    @Override
    public void checkAsset(Asset asset) throws ValidationFieldsException {
        ValidationFields validationFields = new ValidationFields();

        validationFields.addRequired(asset.getModel() == null, DomainType.ASS, "field.required.1", I18N.message("asset.model"));
        validationFields.addRequired(asset.getTiAssetPrice() == null || asset.getTiAssetPrice() <= 0, DomainType.ASS, "field.required.1", I18N.message("asset.price"));

        if (!validationFields.getErrorMessages().isEmpty()) {
            throw new ValidationFieldsException(validationFields.getErrorMessages());
        }
    }

    /**
     * @param chassisNumber
     * @return
     */
    public List<Asset> getAssetsByChassisNumber(String chassisNumber) {
        BaseRestrictions<Asset> restrictions = new BaseRestrictions<>(Asset.class);
        restrictions.addCriterion(Restrictions.eq(CHASSISNUMBER, chassisNumber));
        return list(restrictions);
    }

    /**
     * @param enginNumber
     * @return
     */
    public List<Asset> getAssetsByEnginNumber(String enginNumber) {
        BaseRestrictions<Asset> restrictions = new BaseRestrictions<>(Asset.class);
        restrictions.addCriterion(Restrictions.eq(ENGINENUMBER, enginNumber));
        return list(restrictions);
    }


    public boolean isChassisNumberExist(String chassisNumber, Asset asset) {
        BaseRestrictions<Asset> restrictions = new BaseRestrictions<>(Asset.class);
        restrictions.addCriterion(Restrictions.eq(CHASSISNUMBER, chassisNumber));
        if (asset != null && asset.getId() != null) {
            restrictions.addCriterion(Restrictions.ne(ID, asset.getId()));
        }
        List<Asset> assets = list(restrictions);
        return assets != null && !assets.isEmpty();
    }

    /**
     * @param enginNumber
     * @param asset
     * @return
     */
    public boolean isEnginNumberExist(String enginNumber, Asset asset) {
        BaseRestrictions<Asset> restrictions = new BaseRestrictions<>(Asset.class);
        restrictions.addCriterion(Restrictions.eq(ENGINENUMBER, enginNumber));
        if (asset != null && asset.getId() != null) {
            restrictions.addCriterion(Restrictions.ne(ID, asset.getId()));
        }
        List<Asset> assets = list(restrictions);
        return assets != null && !assets.isEmpty();
    }

    /**
     * @see com.soma.mfinance.core.asset.service.AssetService#getAssetByPlateNumber(java.lang.String)
     */
    @Override
    public Asset getAssetByPlateNumber(String plateNumber) {
        BaseRestrictions<Asset> restrictions = new BaseRestrictions<>(Asset.class);
        restrictions.addCriterion(Restrictions.eq(PLATENUMBER, plateNumber));
        List<Asset> assets = list(restrictions);
        if (assets != null && !assets.isEmpty()) {
            return assets.get(0);
        }
        return null;
    }

    /**
     *
     */
    @Override
    public String getAssetStatus(Contract contract) {
        ContractFlagRestriction restrictions = new ContractFlagRestriction();
        restrictions.setConId(contract.getId());
        List<ContractFlag> contractFlags = list(restrictions);
        if (contractFlags != null && !contractFlags.isEmpty()) {
            ContractFlag contractFlag = contractFlags.get(0);
            if (contractFlag != null) {
                return contractFlag.getFlag().getDesc();
            }
        }
        return StringUtils.EMPTY;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public void saveOrUpdateAppraisal(AssetAppraisal assetAppraisal) {
        if (assetAppraisal.getAppraisals() != null){
            //List<Appraisal> appraisals = assetAppraisal.getAppraisals();
            /*Iterator<Appraisal> appraisals = assetAppraisal.getAppraisals().iterator();
            while(appraisals.hasNext()){
                Appraisal groupdetail = appraisals.next();
                if(CrudAction.DELETE.equals(groupdetail.getCrudAction())) {
                    appraisals.remove();
                    delete(groupdetail);
                } else {
                    saveOrUpdate(groupdetail);
                }

            }*/
            for (Appraisal appraisal : assetAppraisal.getAppraisals()) {
                if(CrudAction.DELETE.equals(appraisal.getCrudAction())) {
                    delete(appraisal);
                } else {
                    saveOrUpdate(appraisal);
                }
            }
        }
    }

}
