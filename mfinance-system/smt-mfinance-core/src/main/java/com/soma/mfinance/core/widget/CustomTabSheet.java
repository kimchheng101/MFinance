package com.soma.mfinance.core.widget;

import com.soma.common.app.tools.helper.AppServicesHelper;
import com.soma.mfinance.core.tab.model.TabItemEntity;
import com.soma.frmk.security.model.SecProfile;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.widget.tabsheet.TabSheet;
import com.vaadin.server.Resource;
import com.vaadin.ui.Component;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by k.seang on 3/7/2017.
 */
public class CustomTabSheet extends TabSheet implements AppServicesHelper {

    private String tabCode ;

    public CustomTabSheet(String tabCode) {
        this.tabCode = tabCode;
    }

    @Override
    public Tab addTab(Component c) {
        if(isDisplayTabByProfileLogin()) {
            return super.addTab(c);
        } else {
            return null;
        }

    }

    @Override
    public Tab addTab(Component c, String caption) {
        if(isDisplayTabByProfileLogin()) {
            return super.addTab(c, caption);
        } else {
            return null;
        }


    }

    @Override
    public Tab addTab(Component c, String caption, Resource icon) {
        if(isDisplayTabByProfileLogin()) {
            return super.addTab(c, caption, icon);
        } else {
            return null;
        }
    }

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    private boolean isDisplayTabByProfileLogin() {
        boolean isDisplay = false;
        TabItemEntity tabItemEntity = ENTITY_SRV.getByCode(TabItemEntity.class,tabCode);
        if(tabItemEntity != null) {
            if(tabItemEntity.getTabProfiles() != null && !tabItemEntity.getTabProfiles().isEmpty()) {
                SecUser loginUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                SecProfile secProfile = loginUser.getDefaultProfile();
                if(tabItemEntity.getTabProfiles().contains(secProfile)) {
                    return  true;
                }
            }
        }

        return isDisplay;
    }
}
