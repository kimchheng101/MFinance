package com.soma.mfinance.core.system;

/**
 * Created by cheasocheat on 2/27/17.
 */
public interface DataReferenceService {

    /**
     * Initialized system tables
     */
    void initialized();
}
