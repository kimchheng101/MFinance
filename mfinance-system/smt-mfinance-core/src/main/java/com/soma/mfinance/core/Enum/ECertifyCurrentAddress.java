package com.soma.mfinance.core.Enum;

import org.seuksa.frmk.model.eref.BaseERefData;

import javax.persistence.AttributeConverter;
import java.util.List;

/**
 * Created by b.chea on 2/25/2017.
 */
public class ECertifyCurrentAddress extends BaseERefData implements AttributeConverter<ECertifyCurrentAddress, Long> {

    public static final ECertifyCurrentAddress TEST = new ECertifyCurrentAddress("CURRENT_ADDRESS", 1);

    public ECertifyCurrentAddress(){}

    public  ECertifyCurrentAddress(String code, long id){ super(code, id);}

    @Override
    public Long convertToDatabaseColumn(ECertifyCurrentAddress attribute) {
        return super.convertToDatabaseColumn(attribute);
    }

    @Override
    public ECertifyCurrentAddress convertToEntityAttribute(Long id) {
        return  getById(ECertifyCurrentAddress.class, id);
    }

    public static List<ECertifyCurrentAddress> values() {
        return getValues(ECertifyCurrentAddress.class);
    }

    public static ECertifyCurrentAddress getByCode(String code) {
        return (ECertifyCurrentAddress) getByCode(ECertifyCurrentAddress.class, code);
    }

    public static ECertifyCurrentAddress getById(long id) {
        return (ECertifyCurrentAddress) getById(ECertifyCurrentAddress.class, Long.valueOf(id));
    }
}
