package com.soma.mfinance.core.quotation.panel;

import com.soma.common.app.eref.EProductLineCode;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.applicant.service.ValidateNumbers;
import com.soma.mfinance.core.document.model.Document;
import com.soma.mfinance.core.document.model.DocumentConfirmEvidence;
import com.soma.mfinance.core.document.model.DocumentUwGroup;
import com.soma.mfinance.core.document.panel.DisplayDocumentPanel;
import com.soma.mfinance.core.document.panel.DocumentUploader;
import com.soma.mfinance.core.model.system.ContactVerification;
import com.soma.mfinance.core.quotation.QuotationService;
import com.soma.mfinance.core.quotation.model.*;
import com.soma.mfinance.core.quotation.panel.include.ContactVerificationGroup;
import com.soma.mfinance.core.quotation.panel.include.DocumentUwGroupPanel;
import com.soma.mfinance.core.quotation.panel.popup.FieldCheckAssetInformationPanel;
import com.soma.mfinance.core.shared.comparator.SortIndexComparator;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.shared.system.FMProfile;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.widget.SecUserComboBox;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.frmk.helper.FrmkServicesHelper;
import com.soma.frmk.security.context.SecApplicationContextHolder;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Runo;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.CrudAction;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Field check panel
 * @author kimsuor.seang
 */
public class FieldCheckPanel extends AbstractTabPanel implements QuotationEntityField, ValueChangeListener, FrmkServicesHelper {
	
	private static final long serialVersionUID = 1163676646989549493L;
	
	private QuotationService quotationService = (QuotationService) SecApplicationContextHolder.getContext().getBean("quotationService");
	
	private SecUserComboBox cbxCreditOfficer;
	
	private TextField txtApplicantFcRevenu;
	private TextField txtApplicantFcAllowance;
	private TextField txtApplicantFcBusinessExpenses;
	private TextField txtApplicantFcNetIncome;
	private TextField txtApplicantFcPersonalExpenses;
	private TextField txtApplicantFcFamilyExpenses;
	private TextField txtApplicantFcLiability;
	
	private TextField txtGuarantorFcRevenu;
	private TextField txtGuarantorFcAllowance;
	private TextField txtGuarantorFcBusinessExpenses;
	private TextField txtGuarantorFcNetIncome;
	private TextField txtGuarantorFcPersonalExpenses;
	private TextField txtGuarantorFcFamilyExpenses;
	private TextField txtGuarantorFcLiability;


	private List<ContactVerificationGroup> applicantContactVerificationsGroup;
	private List<ContactVerificationGroup> guarantorContactVerificationsGroup;

	private VerticalLayout documentsLayout;
	private List<DocumentUwGroupPanel> documentUwGroupsPanel;
	
	private List<CheckBox> cbFieldCheckDocuments;
	private List<Button> btnPaths;

	private HorizontalLayout fieldCheckRequestLayout;
	private Panel fieldCheckRequestPanel;
	private List<CheckBox> cbSupportDecisions;

	private List<QuotationSupportDecision> quotationSupportDecisions;
	private Quotation quotation;
	List<Document> fcDocuments;

	private List<CheckBox> cbConfirms;
	private CheckBox cbConfirm;
	private List<DocumentUwGroup> documentUwGroups;
	private List<QuotationContactEvidence> quotationContactEvidences;

	private List<QuotationContactVerification> quotationContactVerifications;

	private FieldCheckAssetInformationPanel fieldCheckAssetInformationPanel;
	private VerticalLayout contentLayout;
	private Panel fckEstOrAssInfoPanel;
	private VerticalLayout fieldCheckEstimationLayout;

    @SuppressWarnings("unchecked")
	public FieldCheckPanel() {
		super();
		setSizeFull();

		fieldCheckAssetInformationPanel = new FieldCheckAssetInformationPanel();

		ValidateNumbers curentNum = new ValidateNumbers();
		ArrayList numberaList = new ArrayList();

		documentUwGroupsPanel = new ArrayList<DocumentUwGroupPanel>();
		applicantContactVerificationsGroup = new ArrayList<ContactVerificationGroup>();
        guarantorContactVerificationsGroup = new ArrayList<ContactVerificationGroup>();


		contentLayout = new VerticalLayout();
		contentLayout.setSizeFull();
		contentLayout.setSpacing(true);

		fieldCheckRequestLayout = new HorizontalLayout();
		fieldCheckRequestLayout.setMargin(true);
		fieldCheckRequestPanel = new Panel(I18N.message("fieldName.check.request"));
		cbSupportDecisions = new ArrayList<CheckBox>();
		
		List<SupportDecision> supportDecisions = DataReference.getInstance().getSupportDecisions(QuotationWkfStatus.RFC);
		if (supportDecisions != null && !supportDecisions.isEmpty()) {

			for (SupportDecision supportDecision : supportDecisions) {
				CheckBox cbSupportDecision = new CheckBox(supportDecision.getDescEn());
				cbSupportDecision.setData(supportDecision);
				cbSupportDecisions.add(cbSupportDecision);
				fieldCheckRequestLayout.addComponent(cbSupportDecision);
				fieldCheckRequestLayout.addComponent(ComponentFactory.getSpaceLayout(20, Unit.PIXELS));
			}
			fieldCheckRequestPanel.setContent(fieldCheckRequestLayout);
		}

		FormLayout fieldCheckUserLayout = new FormLayout();
		fieldCheckUserLayout.setMargin(true);
		Panel fieldCheckUserPanel = new Panel(I18N.message("staff.in.charge"));
		cbxCreditOfficer = new SecUserComboBox(I18N.message("fieldName.check.performed.by"), DataReference.getInstance().getUsers(FMProfile.CO));
		if (!ProfileUtil.isPOS() && !ProfileUtil.isAdmin()) {
			cbxCreditOfficer.setEnabled(false);
		}
		fieldCheckUserLayout.addComponent(cbxCreditOfficer);
		fieldCheckUserPanel.setContent(fieldCheckUserLayout);
		
		fckEstOrAssInfoPanel = new Panel(I18N.message("field.check.estimation"));
		txtApplicantFcRevenu = ComponentFactory.getTextField(false, 80, 70);
		txtApplicantFcRevenu.setImmediate(true);
		txtApplicantFcRevenu.addStyleName("amount");
		txtApplicantFcRevenu.addValueChangeListener(this);
		numberaList.add(txtApplicantFcRevenu);
		
		txtGuarantorFcRevenu = ComponentFactory.getTextField(false, 80, 70);
		txtGuarantorFcRevenu.setImmediate(true);
		txtGuarantorFcRevenu.addStyleName("amount");
		txtGuarantorFcRevenu.addValueChangeListener(this);
		numberaList.add(txtGuarantorFcRevenu);
		
		
		txtApplicantFcAllowance = ComponentFactory.getTextField(false, 80, 70);
		txtApplicantFcAllowance.setImmediate(true);
		txtApplicantFcAllowance.addStyleName("amount");
		txtApplicantFcAllowance.addValueChangeListener(this);
		numberaList.add(txtApplicantFcAllowance);

		
		txtGuarantorFcAllowance = ComponentFactory.getTextField(false, 80, 70);
		txtGuarantorFcAllowance.setImmediate(true);
		txtGuarantorFcAllowance.addStyleName("amount");
		txtGuarantorFcAllowance.addValueChangeListener(this);
		numberaList.add(txtGuarantorFcAllowance);
		
		txtApplicantFcBusinessExpenses = ComponentFactory.getTextField(false, 80, 70);
		txtApplicantFcBusinessExpenses.setImmediate(true);
		txtApplicantFcBusinessExpenses.addStyleName("amount");
		txtApplicantFcBusinessExpenses.addValueChangeListener(this);
		numberaList.add(txtApplicantFcBusinessExpenses);
		
		txtGuarantorFcBusinessExpenses = ComponentFactory.getTextField(false, 80, 70);
		txtGuarantorFcBusinessExpenses.setImmediate(true);
		txtGuarantorFcBusinessExpenses.addStyleName("amount");
		txtGuarantorFcBusinessExpenses.addValueChangeListener(this);
		numberaList.add(txtGuarantorFcBusinessExpenses);
		
		txtApplicantFcNetIncome = ComponentFactory.getTextField(false, 80, 70);
		txtApplicantFcNetIncome.setReadOnly(true);
		txtApplicantFcNetIncome.addStyleName("amount");
		numberaList.add(txtApplicantFcNetIncome);
		
		txtGuarantorFcNetIncome = ComponentFactory.getTextField(false, 80, 70);
		txtGuarantorFcNetIncome.setReadOnly(true);
		txtGuarantorFcNetIncome.addStyleName("amount");
		numberaList.add(txtGuarantorFcNetIncome);
		
		txtApplicantFcPersonalExpenses = ComponentFactory.getTextField(false, 80, 70);
		txtApplicantFcPersonalExpenses.addStyleName("amount");
		numberaList.add(txtApplicantFcPersonalExpenses);
		
		txtGuarantorFcPersonalExpenses = ComponentFactory.getTextField(false, 80, 70);
		txtGuarantorFcPersonalExpenses.addStyleName("amount");
		numberaList.add(txtGuarantorFcPersonalExpenses);
		
		txtApplicantFcFamilyExpenses = ComponentFactory.getTextField(false, 80, 70);
		txtApplicantFcFamilyExpenses.addStyleName("amount");
		numberaList.add(txtApplicantFcFamilyExpenses);
		
		txtGuarantorFcFamilyExpenses = ComponentFactory.getTextField(false, 80, 70);
		txtGuarantorFcFamilyExpenses.addStyleName("amount");
		numberaList.add(txtGuarantorFcFamilyExpenses);
		
		txtApplicantFcLiability = ComponentFactory.getTextField(false, 80, 70);
		txtApplicantFcLiability.addStyleName("amount");
		numberaList.add(txtApplicantFcLiability);
		
		txtGuarantorFcLiability = ComponentFactory.getTextField(false, 80, 70);
		txtGuarantorFcLiability.addStyleName("amount");
		numberaList.add(txtGuarantorFcLiability);
		
		try {
		
			InputStream fieldCheckEstimationLayoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/co_fc_estimation.html");
			CustomLayout fieldCheckEstimationGridLayout = new CustomLayout(fieldCheckEstimationLayoutFile);
			fieldCheckEstimationGridLayout.setSizeFull();
			
			fieldCheckEstimationGridLayout.addComponent(new Label(I18N.message("base.salary.total.sales")), "lblRevenu");
			fieldCheckEstimationGridLayout.addComponent(new Label(I18N.message("allowance")), "lblAllowance");
			fieldCheckEstimationGridLayout.addComponent(new Label(I18N.message("business.expenses")), "lblBusinessExpenses");
			fieldCheckEstimationGridLayout.addComponent(new Label(I18N.message("net.income")), "lblNetIncome");
			fieldCheckEstimationGridLayout.addComponent(new Label(I18N.message("personal.expenses")), "lblPersonalExpenses");
			fieldCheckEstimationGridLayout.addComponent(new Label(I18N.message("family.expenses")), "lblFamilyExpenses");
			fieldCheckEstimationGridLayout.addComponent(new Label(I18N.message("liability")), "lblLiability");
			fieldCheckEstimationGridLayout.addComponent(new Label(I18N.message("field.check")), "lblFcEstimation");
			
			fieldCheckEstimationGridLayout.addComponent(new Label(I18N.message("applicant")), "lblApplicant");
			fieldCheckEstimationGridLayout.addComponent(txtApplicantFcRevenu, "txtApplicantFcRevenu");
			fieldCheckEstimationGridLayout.addComponent(txtApplicantFcAllowance, "txtApplicantFcAllowance");
			fieldCheckEstimationGridLayout.addComponent(txtApplicantFcBusinessExpenses, "txtApplicantFcBusinessExpenses");
			fieldCheckEstimationGridLayout.addComponent(txtApplicantFcNetIncome, "txtApplicantFcNetIncome");
			fieldCheckEstimationGridLayout.addComponent(txtApplicantFcPersonalExpenses, "txtApplicantFcPersonalExpenses");
			fieldCheckEstimationGridLayout.addComponent(txtApplicantFcFamilyExpenses, "txtApplicantFcFamilyExpenses");
			fieldCheckEstimationGridLayout.addComponent(txtApplicantFcLiability, "txtApplicantFcLiability");
			
			fieldCheckEstimationGridLayout.addComponent(new Label(I18N.message("guarantor")), "lblGuarantor");
			fieldCheckEstimationGridLayout.addComponent(txtGuarantorFcRevenu, "txtGuarantorFcRevenu");
			fieldCheckEstimationGridLayout.addComponent(txtGuarantorFcAllowance, "txtGuarantorFcAllowance");
			fieldCheckEstimationGridLayout.addComponent(txtGuarantorFcBusinessExpenses, "txtGuarantorFcBusinessExpenses");
			fieldCheckEstimationGridLayout.addComponent(txtGuarantorFcNetIncome, "txtGuarantorFcNetIncome");
			fieldCheckEstimationGridLayout.addComponent(txtGuarantorFcPersonalExpenses, "txtGuarantorFcPersonalExpenses");
			fieldCheckEstimationGridLayout.addComponent(txtGuarantorFcFamilyExpenses, "txtGuarantorFcFamilyExpenses");
			fieldCheckEstimationGridLayout.addComponent(txtGuarantorFcLiability, "txtGuarantorFcLiability");
			
			fieldCheckEstimationLayout = new VerticalLayout();
			fieldCheckEstimationLayout.setMargin(true);
			fieldCheckEstimationLayout.addComponent(fieldCheckEstimationGridLayout);
			fckEstOrAssInfoPanel.setContent(fieldCheckEstimationLayout);
			
		} catch (IOException e) {
			Notification.show(e.toString());
		}
		
		documentsLayout = new VerticalLayout();
		Panel documentsPanel = new Panel();
		documentsPanel.setCaption(I18N.message("third.party.confirmation"));
		documentsPanel.setContent(documentsLayout);

		// Panel of Field Check Confirmation (Contact Verification)
		Panel fieldCheckVerificationPanel = new Panel(I18N.message("field.check.confirmation"));

		List<ContactVerification> contactVerifications = quotationService.list(ContactVerification.class);
		if (contactVerifications != null && !contactVerifications.isEmpty()) {
			CustomLayout contactVerificationsGridLayout1 = new CustomLayout("applicant");
			CustomLayout contactVerificationsGridLayout2 = new CustomLayout("guarantor");
			ContactVerificationGroup cntEvidenceVerifGroup;
			String contactVerificationTemplate1 = "<table height=\"100%\" cellspacing=\"0\" cellpadding=\"5\" >";
			contactVerificationTemplate1 += "<tr>";
			contactVerificationTemplate1 += "<td>";
			contactVerificationTemplate1 += "<table height=\"100%\" cellspacing=\"0\" cellpadding=\"5\" >";
			contactVerificationTemplate1 += "<tr>";
			contactVerificationTemplate1 += "<td colspan=\"2\">";
			contactVerificationTemplate1 += "<b>" + I18N.message("applicant") + "</b>";
			contactVerificationTemplate1 += "</td>";
			contactVerificationTemplate1 += "</tr>";

			for (ContactVerification contactVerification : contactVerifications) {
				if (contactVerification.getProfile().getId().equals(FMProfile.CO) || contactVerification.getProfile().getId().equals(FMProfile.PO)) {
					contactVerificationTemplate1 += "<tr>";

                    contactVerificationTemplate1 += "<td><div location=\"cbContactVerification"
                            + contactVerification.getId() + EApplicantType.C + "\" /></td>";
                    contactVerificationTemplate1 += "<td><div location=\"txtContactVerification"
                            + contactVerification.getId() + EApplicantType.C + "\" /></td>";
                    contactVerificationTemplate1 += "</tr>";

					cntEvidenceVerifGroup = new ContactVerificationGroup(contactVerification);
					applicantContactVerificationsGroup.add(cntEvidenceVerifGroup);
					contactVerificationsGridLayout1.addComponent(cntEvidenceVerifGroup.getCbContactVerification(),"cbContactVerification" + contactVerification.getId() + EApplicantType.C);
					contactVerificationsGridLayout1.addComponent(cntEvidenceVerifGroup.getTxtValue(), "txtContactVerification" + contactVerification.getId() + EApplicantType.C);
				}
			}

			contactVerificationTemplate1 += "</table>";
			contactVerificationTemplate1 += "</td>";

			contactVerificationTemplate1 += "<td>";

			String contactVerificationTemplate2 = "<table height=\"100%\" cellspacing=\"0\" cellpadding=\"5\" >";
			contactVerificationTemplate2 += "<tr>";
			contactVerificationTemplate2 += "<td colspan=\"2\">";
			contactVerificationTemplate2 += "<b>" + I18N.message("guarantor") + "</b>";
			contactVerificationTemplate2 += "</td>";
			contactVerificationTemplate2 += "</tr>";

			for (ContactVerification contactVerification : contactVerifications) {
				if (contactVerification.getProfile().getId().equals(FMProfile.CO)|| contactVerification.getProfile().getId().equals(FMProfile.PO)) {
					contactVerificationTemplate2 += "<tr>";
					contactVerificationTemplate2 += "<td><div location=\"cbContactVerification"
							+ contactVerification.getId() + EApplicantType.G + "\" /></td>";
					contactVerificationTemplate2 += "<td><div location=\"txtContactVerification"
							+ contactVerification.getId() + EApplicantType.G + "\" /></td>";
					contactVerificationTemplate2 += "</tr>";

					cntEvidenceVerifGroup = new ContactVerificationGroup(contactVerification);
					guarantorContactVerificationsGroup.add(cntEvidenceVerifGroup);
					contactVerificationsGridLayout2.addComponent(cntEvidenceVerifGroup.getCbContactVerification(),"cbContactVerification" + contactVerification.getId() + EApplicantType.G);
					contactVerificationsGridLayout2.addComponent(cntEvidenceVerifGroup.getTxtValue(),"txtContactVerification" + contactVerification.getId() + EApplicantType.G);
				}
			}
			contactVerificationTemplate2 += "</table>";
			contactVerificationTemplate2 += "</td>";
			contactVerificationTemplate2 += "</tr>";
			contactVerificationTemplate2 += "</table>";

			contactVerificationsGridLayout1.setTemplateContents(contactVerificationTemplate1);
			contactVerificationsGridLayout2.setTemplateContents(contactVerificationTemplate2);

			HorizontalLayout layout = new HorizontalLayout();
			layout.addComponent(contactVerificationsGridLayout1);
			layout.addComponent(contactVerificationsGridLayout2);

			fieldCheckVerificationPanel.setContent(layout);
		}

		if (ProfileUtil.isUnderwriter() || ProfileUtil.isUnderwriterSupervisor() || ProfileUtil.isManager()) {
			contentLayout.addComponent(fieldCheckRequestPanel);
		}
		curentNum.validateNumber(numberaList);
		contentLayout.addComponent(fieldCheckUserPanel);

		contentLayout.addComponent(fckEstOrAssInfoPanel);

		contentLayout.addComponent(documentsPanel);
		contentLayout.addComponent(fieldCheckVerificationPanel);

		cbFieldCheckDocuments = new ArrayList<CheckBox>();
		btnPaths = new ArrayList<Button>();
		
		List<Document> documents = DataReference.getInstance().getDocuments();
		List<Document> fcDocuments = new ArrayList<Document>();
		if (documents != null && !documents.isEmpty()) {
			for (Document document : documents) {
				if (document.isFieldCheck()) {
					fcDocuments.add(document);
				}
			}
		}
		
		if (!fcDocuments.isEmpty()) {
			Panel fieldCheckDocumentsPanel = new Panel(I18N.message("fieldName.check.documents"));
			GridLayout fieldCheckDocumentGridLayout = new GridLayout(3, fcDocuments.size() + 1);
        	fieldCheckDocumentGridLayout.setSpacing(true);
        	fieldCheckDocumentGridLayout.setMargin(true);
        	fieldCheckDocumentGridLayout.addComponent(new Label(), 0, 0);
        	fieldCheckDocumentGridLayout.addComponent(new Label("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", ContentMode.HTML), 1, 0);
        	fieldCheckDocumentGridLayout.addComponent(new Label(I18N.message("upload")), 2, 0);
			
        	int i = 1;
        	Collections.sort(fcDocuments, new DocumentComparatorByIdex());
        	for (Document document : fcDocuments) {
        		CheckBox cbDocument = new CheckBox();
        		cbDocument.setCaption(document.getApplicantType().getCode() + " - " + document.getDescEn());
        		cbDocument.setData(document);
        		cbFieldCheckDocuments.add(cbDocument);
        		
        		Button btnPath = new Button();
        		btnPath.setIcon(new ThemeResource("../smt-default/icons/16/pdf.png"));
        		btnPath.setVisible(false);
        		btnPath.setStyleName(Runo.BUTTON_LINK);
        		btnPaths.add(btnPath);
        		btnPath.addClickListener(new ClickListener() {
					private static final long serialVersionUID = 1674938731111758211L;
					@Override
					public void buttonClick(ClickEvent event) {
						new DisplayDocumentPanel((String) ((Button) event.getSource()).getData()).display();
					}
				});
        		
        		Upload uploadDocument = new Upload();
        		uploadDocument.setButtonCaption(I18N.message("upload"));
        		
        		final DocumentUploader uploader = new DocumentUploader(cbDocument, btnPath, uploadDocument); 
        		uploadDocument.setReceiver(uploader);
        		uploadDocument.addSucceededListener(uploader);
        		
        		fieldCheckDocumentGridLayout.addComponent(cbDocument, 0, i);
        		fieldCheckDocumentGridLayout.addComponent(btnPath, 1, i);
        		fieldCheckDocumentGridLayout.addComponent(uploadDocument, 2, i);
        		i++;
    		}
    		
    		fieldCheckDocumentsPanel.setContent(fieldCheckDocumentGridLayout);
    		contentLayout.addComponent(fieldCheckDocumentsPanel);
		}
		
		addComponent(contentLayout);
	}
	
	@Override
	protected com.vaadin.ui.Component createForm() {
		return new VerticalLayout();
	}

	public void updateSupportDecision() {
		if (cbSupportDecisions != null && !cbSupportDecisions.isEmpty()) {
			for (CheckBox cbSupportDecision : cbSupportDecisions) {
				boolean isAlreadyExist = false;
				SupportDecision supportDecisionInCheckbox = (SupportDecision) cbSupportDecision.getData();

				if (quotationSupportDecisions != null && !quotationSupportDecisions.isEmpty()) {
					for (QuotationSupportDecision quotationSupportDecision : quotationSupportDecisions) {
						SupportDecision supportDecision = quotationSupportDecision.getSupportDecision();
						if (supportDecision.getId() == supportDecisionInCheckbox.getId()) {
							isAlreadyExist = true;
						}
						if (supportDecision.getId() == supportDecisionInCheckbox.getId() && !cbSupportDecision.getValue()) {
							ENTITY_SRV.delete(quotationSupportDecision);
							break;
						}
					}
				}
				
				if (cbSupportDecision.getValue() && !isAlreadyExist) {
					QuotationSupportDecision quotationSupportDecision = new QuotationSupportDecision();
					quotationSupportDecision.setQuotation(quotation);
					quotationSupportDecision.setSupportDecision(supportDecisionInCheckbox);
					quotationSupportDecision.setWkfStatus(quotation.getWkfStatus());
					quotationSupportDecision.setProcessed(true);
					ENTITY_SRV.saveOrUpdate(quotationSupportDecision);
				}
			}
		}
	}

	public void addQuotationContactVerification() {
		if (applicantContactVerificationsGroup != null && !applicantContactVerificationsGroup.isEmpty()) {
			for (ContactVerificationGroup contactVerificationGroup : applicantContactVerificationsGroup) {
				boolean isAlreadyExist = false;
				ContactVerification contactVerificationIncb = (ContactVerification) contactVerificationGroup.getCbContactVerification().getData();

				quotationContactVerifications = getListQuotationContactVerify(quotation);
				for (QuotationContactVerification quotationContactVerification : quotationContactVerifications) {
					if (contactVerificationIncb.getId() == quotationContactVerification.getContactVerification().getId()
							&& quotationContactVerification.getApplicantType().equals(EApplicantType.C)) {

						quotationContactVerification.setValue(contactVerificationGroup.getTxtValue().getValue());
						ENTITY_SRV.update(quotationContactVerification);
						isAlreadyExist = true;
					}

					if (contactVerificationIncb.getId() == quotationContactVerification.getContactVerification().getId()
                            && quotationContactVerification.getApplicantType().equals(EApplicantType.C)
                            && !contactVerificationGroup.getCbContactVerification().getValue()) {

                        ENTITY_SRV.delete(quotationContactVerification);
                        break;
                    }
				}

				if (contactVerificationGroup.getCbContactVerification().getValue() && !isAlreadyExist) {
					QuotationContactVerification quotationContactVerification = new QuotationContactVerification();
					quotationContactVerification.setQuotation(quotation);
					quotationContactVerification.setContactVerification(contactVerificationIncb);
					quotationContactVerification.setValue(contactVerificationGroup.getTxtValue().getValue());
					quotationContactVerification.setApplicantType(EApplicantType.C);
					ENTITY_SRV.saveOrUpdate(quotationContactVerification);
				}
			}
		}

		if (guarantorContactVerificationsGroup != null && !guarantorContactVerificationsGroup.isEmpty()) {
			for (ContactVerificationGroup contactVerificationGroup : guarantorContactVerificationsGroup) {
				ContactVerification contactVerificationIncb = (ContactVerification) contactVerificationGroup.getCbContactVerification().getData();
                boolean isAlreadyExist = false;

				quotationContactVerifications = getListQuotationContactVerify(quotation);
                for (QuotationContactVerification quotationContactVerification : quotationContactVerifications) {
					if(contactVerificationIncb.getId() == quotationContactVerification.getContactVerification().getId()
							&& quotationContactVerification.getApplicantType().equals(EApplicantType.G)) {

						quotationContactVerification.setValue(contactVerificationGroup.getTxtValue().getValue());
						ENTITY_SRV.update(quotationContactVerification);
						isAlreadyExist = true;
					}

					if(contactVerificationIncb.getId() == quotationContactVerification.getContactVerification().getId()
                            && quotationContactVerification.getApplicantType().equals(EApplicantType.G)
                            && !contactVerificationGroup.getCbContactVerification().getValue()) {

                        ENTITY_SRV.delete(quotationContactVerification);
                        break;
                    }
                }

				if (contactVerificationGroup.getCbContactVerification().getValue() && !isAlreadyExist) {
					QuotationContactVerification quotationContactVerification = new QuotationContactVerification();
					quotationContactVerification.setQuotation(quotation);
					quotationContactVerification.setContactVerification(contactVerificationIncb);
					quotationContactVerification.setValue(contactVerificationGroup.getTxtValue().getValue());
					quotationContactVerification.setApplicantType(EApplicantType.G);
					ENTITY_SRV.saveOrUpdate(quotationContactVerification);
				}
			}
		}
	}


	public void addQuotationContactEvidence() {
		if (cbConfirms != null && !cbConfirms.isEmpty()) {
			for (CheckBox cbConfirm : cbConfirms) {
				boolean isAlreadyExist = false;
				DocumentConfirmEvidence documentConfirmEvidenceIncb = (DocumentConfirmEvidence) cbConfirm.getData();

				if (quotationContactEvidences != null && !quotationContactEvidences.isEmpty()) {
					for (QuotationContactEvidence quotationContactEvidence : quotationContactEvidences) {
						if (documentConfirmEvidenceIncb.getId() == quotationContactEvidence.getDocumentConfirmEvidence().getId()) {
							isAlreadyExist = true;
						}
						if (documentConfirmEvidenceIncb.getId() == quotationContactEvidence.getDocumentConfirmEvidence().getId() && !cbConfirm.getValue()) {
							ENTITY_SRV.delete(quotationContactEvidence);
							break;
						}
					}
				}

				if (cbConfirm.getValue() && !isAlreadyExist) {
					QuotationContactEvidence quotationContactEvidence = new QuotationContactEvidence();
					quotationContactEvidence.setQuotation(quotation);
					quotationContactEvidence.setDocumentConfirmEvidence(documentConfirmEvidenceIncb);
					quotationContactEvidence.setDocumentUwGroup(documentConfirmEvidenceIncb.getDocumentUwGroup());
					ENTITY_SRV.saveOrUpdate(quotationContactEvidence);
				}
			}
		}
	}

    private List<QuotationContactVerification> getListQuotationContactVerify(Quotation quotation) {
        BaseRestrictions<QuotationContactVerification> restrictions = new BaseRestrictions<QuotationContactVerification>(QuotationContactVerification.class);
        restrictions.addCriterion(Restrictions.eq("quotation.id", quotation.getId()));
        return ENTITY_SRV.list(restrictions);
    }

    private List<QuotationContactEvidence> getListQuotationContactEvidences(Quotation quotation) {
        BaseRestrictions<QuotationContactEvidence> restrictions = new BaseRestrictions<QuotationContactEvidence>(QuotationContactEvidence.class);
        restrictions.addCriterion(Restrictions.eq("quotation.id", quotation.getId()));
        return ENTITY_SRV.list(restrictions);
    }

	private List<QuotationSupportDecision> getListQuotationSupportDecisions(Quotation quotation) {
		BaseRestrictions<QuotationSupportDecision> restrictions = new BaseRestrictions<QuotationSupportDecision>(
				QuotationSupportDecision.class);
		restrictions.addCriterion(Restrictions.eq("quotation.id", quotation.getId()));
		return ENTITY_SRV.list(restrictions);
	}

	public void assignValues(Quotation quotation) {
		this.quotation = quotation;
		if(quotation.getFinancialProduct().getProductLine().getProductLineCode().equals(EProductLineCode.KFP)){
			fckEstOrAssInfoPanel.setCaption(I18N.message("asset.info"));
			fckEstOrAssInfoPanel.setContent(fieldCheckAssetInformationPanel.getFieldCheckAssetInfo());
			fieldCheckAssetInformationPanel.assignValue(quotation);
		}else {
			fckEstOrAssInfoPanel.setCaption(I18N.message("field.check.estimation"));
			fckEstOrAssInfoPanel.setContent(fieldCheckEstimationLayout);
		}
		this.quotationSupportDecisions = getListQuotationSupportDecisions(quotation);
		if (cbSupportDecisions != null && !cbSupportDecisions.isEmpty()) {
			for (CheckBox cbSupportDecision : cbSupportDecisions) {
				SupportDecision supportDecision = (SupportDecision) cbSupportDecision.getData();
				if (quotationSupportDecisions != null && !quotationSupportDecisions.isEmpty()) {
					for (QuotationSupportDecision quotationSupportDecision : quotationSupportDecisions) {
						if (supportDecision.getId() == quotationSupportDecision.getSupportDecision().getId()) {
							cbSupportDecision.setValue(true);
							break;
						}
					}
				}
			}
		}
		if (quotation.getFieldCheck() == null) {
			cbxCreditOfficer.setSelectedEntity(quotation.getCreditOfficer());
		} else {
			cbxCreditOfficer.setSelectedEntity(quotation.getFieldCheck());
		}
		
		txtApplicantFcRevenu.setValue(AmountUtils.format(quotation.getCoRevenuEstimation()));
		txtApplicantFcAllowance.setValue(AmountUtils.format(quotation.getCoAllowanceEstimation()));
		txtApplicantFcBusinessExpenses.setValue(AmountUtils.format(quotation.getCoBusinessExpensesEstimation()));
		txtApplicantFcNetIncome.setReadOnly(false);
		txtApplicantFcNetIncome.setValue(AmountUtils.format(quotation.getCoNetIncomeEstimation()));
		txtApplicantFcNetIncome.setReadOnly(true);
		txtApplicantFcPersonalExpenses.setValue(AmountUtils.format(quotation.getCoPersonalExpensesEstimation()));
		txtApplicantFcFamilyExpenses.setValue(AmountUtils.format(quotation.getCoFamilyExpensesEstimation()));
		txtApplicantFcLiability.setValue(AmountUtils.format(quotation.getCoLiabilityEstimation()));
		
		txtGuarantorFcRevenu.setValue(AmountUtils.format(quotation.getCoGuarantorRevenuEstimation()));
		txtGuarantorFcAllowance.setValue(AmountUtils.format(quotation.getCoGuarantorAllowanceEstimation()));
		txtGuarantorFcBusinessExpenses.setValue(AmountUtils.format(quotation.getCoGuarantorBusinessExpensesEstimation()));
		txtGuarantorFcNetIncome.setReadOnly(false);
		txtGuarantorFcNetIncome.setValue(AmountUtils.format(quotation.getCoGuarantorNetIncomeEstimation()));
		txtGuarantorFcNetIncome.setReadOnly(true);
		txtGuarantorFcPersonalExpenses.setValue(AmountUtils.format(quotation.getCoGuarantorPersonalExpensesEstimation()));
		txtGuarantorFcFamilyExpenses.setValue(AmountUtils.format(quotation.getCoGuarantorFamilyExpensesEstimation()));
		txtGuarantorFcLiability.setValue(AmountUtils.format(quotation.getCoGuarantorLiabilityEstimation()));		

		// Display List of Panel (Third Party Confirmation)
		TabSheet documentsTabSheet = new TabSheet();
		documentsLayout.removeAllComponents();
		documentUwGroupsPanel.clear();
		documentsLayout.addComponent(documentsTabSheet);
		documentUwGroups = quotationService.list(DocumentUwGroup.class);

		this.quotationContactEvidences = getListQuotationContactEvidences(quotation);
		Collections.sort(documentUwGroups, new SortIndexComparator());
		if (documentUwGroups != null && !documentUwGroups.isEmpty()) {
			cbConfirms = new ArrayList<>();
			for (DocumentUwGroup documentUwGroup : documentUwGroups) {
				if (!quotationService.isGuarantorRequired(quotation) && documentUwGroup.getApplicantType().equals(EApplicantType.G)) {
					/*do nothing*/
				}else {
					VerticalLayout  confirmEvidenceLayout = new VerticalLayout();
					List<DocumentConfirmEvidence> documentConfirmEvidences = documentUwGroup.getDocumentsConfirmEvidence();
					if (documentConfirmEvidences != null && !documentConfirmEvidences.isEmpty()) {
						for (DocumentConfirmEvidence documentConfirmEvidence : documentConfirmEvidences) {
							if(documentConfirmEvidence.getConfirmEvidence().getCode().equals("(CO) Chief of village Confirmation") || documentConfirmEvidence.getConfirmEvidence().getCode().equals("(CO) Employer Confirmation")){
								cbConfirm = new CheckBox(documentConfirmEvidence.getConfirmEvidence().getCode());
								cbConfirm.setData(documentConfirmEvidence);
								cbConfirms.add(cbConfirm);
								confirmEvidenceLayout.addComponent(cbConfirm);
								fieldCheckRequestLayout.addComponent(ComponentFactory.getSpaceLayout(20, Unit.PIXELS));

								for(QuotationContactEvidence quotationContactEvidence: quotationContactEvidences) {
									if (documentConfirmEvidence.getId() == quotationContactEvidence.getDocumentConfirmEvidence().getId()) {
										cbConfirm.setValue(true);
									}
								}
							}
						}
					}

					DocumentUwGroupPanel documentUwGroupPanel = new DocumentUwGroupPanel(documentUwGroup, quotation, false);
					documentUwGroupPanel.addComponent(confirmEvidenceLayout);
					documentsTabSheet.addTab(documentUwGroupPanel, documentUwGroup.getDescEn());

					documentUwGroupsPanel.add(documentUwGroupPanel);

				}
			}
		}

		// End Display Panel (Third Party Confirmation)

        // Display Panel (Field Check Confirmation)
		this.quotationContactVerifications = getListQuotationContactVerify(quotation);
			for (ContactVerificationGroup contactVerificationGroup : applicantContactVerificationsGroup) {
                ContactVerification contactVerification = (ContactVerification) contactVerificationGroup.getCbContactVerification().getData();
                for (QuotationContactVerification quotationContactVerification : quotationContactVerifications) {
                    boolean isApplicant = quotationContactVerification.getApplicantType().getCode().equals(EApplicantType.C.getCode());

                    if (isApplicant && quotationContactVerification.getContactVerification().getId() == contactVerification.getId()) {
                        contactVerificationGroup.getCbContactVerification().setValue(true);
                        contactVerificationGroup.getTxtValue().setValue(quotationContactVerification.getValue());
                    }
                }
                if(quotationContactVerifications.isEmpty()) {
                    contactVerificationGroup.getCbContactVerification().setValue(false);
                    contactVerificationGroup.getTxtValue().setValue("");
                }
			}

			for (ContactVerificationGroup contactVerificationGroup : guarantorContactVerificationsGroup) {
                ContactVerification contactVerification = (ContactVerification) contactVerificationGroup.getCbContactVerification().getData();
                for (QuotationContactVerification quotationContactVerification : quotationContactVerifications) {
                    boolean isApplicant = quotationContactVerification.getApplicantType().getCode().equals(EApplicantType.G.getCode());

                    if (isApplicant && quotationContactVerification.getContactVerification().getId() == contactVerification.getId()) {
                        contactVerificationGroup.getCbContactVerification().setValue(true);
                        contactVerificationGroup.getTxtValue().setValue(quotationContactVerification.getValue());
                    }
                }
                if(quotationContactVerifications.isEmpty()) {
                    contactVerificationGroup.getCbContactVerification().setValue(false);
                    contactVerificationGroup.getTxtValue().setValue("");
                }
			}
        // End Display Panel (Field Check Confirmation)

		List<QuotationDocument> documents = quotation.getQuotationDocuments();
		if (cbFieldCheckDocuments != null && !cbFieldCheckDocuments.isEmpty()) {
			for (int i = 0; i < cbFieldCheckDocuments.size(); i++) {
				CheckBox cbDocument = cbFieldCheckDocuments.get(i);
				Document document = (Document) cbDocument.getData();
				boolean found = false;
				if (documents != null) {
					for (QuotationDocument quotationDocument : documents) {
						if (document.isFieldCheck() && document.getId().equals(quotationDocument.getDocument().getId())) {
							cbDocument.setValue(true);
							if (StringUtils.isNotEmpty(quotationDocument.getPath())) {
								btnPaths.get(i).setVisible(true);
								btnPaths.get(i).setData(quotationDocument.getPath());
							} else {
								btnPaths.get(i).setVisible(false);
								btnPaths.get(i).setData(null);
							}
							found = true;
							break;
						}
					}
				}
				if (!found) {
					cbDocument.setValue(false);
					btnPaths.get(i).setVisible(false);
					btnPaths.get(i).setData(null);
				}
			}
		}
	}

	public Quotation getQuotation(Quotation quotation) {
		quotation.setFieldCheck(cbxCreditOfficer.getSelectedEntity());
		quotation.setCoRevenuEstimation(getDouble(txtApplicantFcRevenu));
		quotation.setCoAllowanceEstimation(getDouble(txtApplicantFcAllowance));
		quotation.setCoBusinessExpensesEstimation(getDouble(txtApplicantFcBusinessExpenses));
		quotation.setCoNetIncomeEstimation(getDouble(txtApplicantFcNetIncome));
		quotation.setCoPersonalExpensesEstimation(getDouble(txtApplicantFcPersonalExpenses));
		quotation.setCoFamilyExpensesEstimation(getDouble(txtApplicantFcFamilyExpenses));
		quotation.setCoLiabilityEstimation(getDouble(txtApplicantFcLiability));
		
		quotation.setCoGuarantorRevenuEstimation(getDouble(txtGuarantorFcRevenu));
		quotation.setCoGuarantorAllowanceEstimation(getDouble(txtGuarantorFcAllowance));
		quotation.setCoGuarantorBusinessExpensesEstimation(getDouble(txtGuarantorFcBusinessExpenses));
		quotation.setCoGuarantorNetIncomeEstimation(getDouble(txtGuarantorFcNetIncome));
		quotation.setCoGuarantorPersonalExpensesEstimation(getDouble(txtGuarantorFcPersonalExpenses));
		quotation.setCoGuarantorFamilyExpensesEstimation(getDouble(txtGuarantorFcFamilyExpenses));
		quotation.setCoGuarantorLiabilityEstimation(getDouble(txtGuarantorFcLiability));
		if(quotation.getFinancialProduct().getProductLine().getProductLineCode().equals(EProductLineCode.KFP)){
			fieldCheckAssetInformationPanel.getAssetInformationDocument(quotation);
		}

		// Save
		if (documentUwGroupsPanel != null && !documentUwGroupsPanel.isEmpty()) {
			addQuotationContactEvidence();

			// Save comments
			int i = 0;
			if(quotation.getComments().isEmpty()){
				List<Comment> comments = new ArrayList<>();
				for(DocumentUwGroupPanel documentUwGroupPanel : documentUwGroupsPanel){
					Comment comment = new Comment();
					comment.setQuotation(quotation);
					comment.setUser((SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
					comment.setDocumentUwGroup(documentUwGroups.get(i));
					comment.setDesc(documentUwGroupPanel.getTxtComment().getValue());

					comments.add(comment);
					ENTITY_SRV.saveOrUpdate(comment);
					i++;
				}
				quotation.setComments(comments);
			} else {
				List<Comment> comments = quotation.getComments();
				for(Comment comment : comments){
					comment.setDesc(documentUwGroupsPanel.get(i).getTxtComment().getValue());
					ENTITY_SRV.saveOrUpdate(comment);
					i++;
				}
			}

		}

		// Save data to table on Panel (Field Check Confirmation)
		addQuotationContactVerification();

		// Save Panel (Field Check Documents)
		List<QuotationDocument> quotationDocuments = quotation.getQuotationDocuments();
		if (quotationDocuments == null) {
			quotationDocuments = new ArrayList<QuotationDocument>();
		}
		if (cbFieldCheckDocuments != null && !cbFieldCheckDocuments.isEmpty()) {
			for (int i = 0; i < cbFieldCheckDocuments.size(); i++) {
				CheckBox cbDocument = cbFieldCheckDocuments.get(i);
				Document document = (Document) cbDocument.getData();
				QuotationDocument quotationDocument = quotation.getQuotationDocument(document.getId());
				if (cbDocument.getValue()) {
					if (quotationDocument == null) {
						quotationDocument = new QuotationDocument();
						quotationDocument.setCrudAction(CrudAction.CREATE);
						quotationDocuments.add(quotationDocument);
					} else {
						quotationDocument.setCrudAction(CrudAction.UPDATE);
					}
					quotationDocument.setDocument(document);
					quotationDocument.setQuotation(quotation);
					if (btnPaths.get(i).getData() != null) {
						quotationDocument.setPath((String) btnPaths.get(i).getData());
					}
				} else if (quotationDocument != null) {
					quotationDocument.setCrudAction(CrudAction.DELETE);
				}
			}
		}
		quotation.setQuotationDocuments(quotationDocuments);
		// End Save (Field Check Documents)
		return quotation;
	}
	
	/**
	 * @return
	 */
	public boolean checkValidityFiels() {
		super.removeErrorsPanel();
		if (!errors.isEmpty()) {
			super.displayErrorsPanel();
		}
		return errors.isEmpty();
	}
	
	/**
	 * Reset panel
	 */
	public void reset() {
		
		txtApplicantFcRevenu.setValue("");
		txtApplicantFcAllowance.setValue("");
		txtApplicantFcBusinessExpenses.setValue("");
		txtApplicantFcPersonalExpenses.setValue("");
		txtApplicantFcFamilyExpenses.setValue("");
		txtApplicantFcLiability.setValue("");
		
		txtGuarantorFcRevenu.setValue("");
		txtGuarantorFcAllowance.setValue("");
		txtGuarantorFcBusinessExpenses.setValue("");
		txtGuarantorFcPersonalExpenses.setValue("");
		txtGuarantorFcFamilyExpenses.setValue("");
		txtGuarantorFcLiability.setValue("");
		
		if (cbFieldCheckDocuments != null && !cbFieldCheckDocuments.isEmpty()) {
			for (int i = 0; i < cbFieldCheckDocuments.size(); i++) {
				cbFieldCheckDocuments.get(i).setValue(false);
				btnPaths.get(i).setVisible(false);
				btnPaths.get(i).setData(null);
			}
		}
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
		txtApplicantFcNetIncome.setReadOnly(false);
		txtApplicantFcNetIncome.setValue(AmountUtils.format(getDouble(txtApplicantFcRevenu, 0.d) + getDouble(txtApplicantFcAllowance, 0.d) - getDouble(txtApplicantFcBusinessExpenses, 0.d)));
		txtApplicantFcNetIncome.setReadOnly(true);

		txtGuarantorFcNetIncome.setReadOnly(false);
		txtGuarantorFcNetIncome.setValue(AmountUtils.format(getDouble(txtGuarantorFcRevenu, 0.d) + getDouble(txtGuarantorFcAllowance, 0.d) - getDouble(txtGuarantorFcBusinessExpenses, 0.d)));
		txtGuarantorFcNetIncome.setReadOnly(true);
	}
	
	/**
	 * sort index
	 * @author kimsuor.seang
	 *
	 */
	@SuppressWarnings("rawtypes")
	private static class DocumentComparatorByIdex implements Comparator {
		public int compare(Object o1, Object o2) {
			Document c1 = (Document) o1;
			Document c2 = (Document) o2;
			if (c1 == null || c1.getSortIndex() == null) {
				if (c2 == null || c2.getSortIndex() == null)
					return 0;
				return -1;
			}
			if (c2 == null || c2.getSortIndex() == null)
				return 1;
			return c1.getSortIndex().compareTo(c2.getSortIndex());
		}
	}
}
