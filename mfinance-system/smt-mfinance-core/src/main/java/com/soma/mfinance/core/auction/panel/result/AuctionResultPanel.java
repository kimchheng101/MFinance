package com.soma.mfinance.core.auction.panel.result;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * Auction Result Panel for profile Auction controller, Collection Officer and Manager
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(AuctionResultPanel.NAME)
public class AuctionResultPanel extends AbstractTabsheetPanel implements View {

	private static final long serialVersionUID = -6880851534576564934L;
	public static final String NAME = "auction.result";
	
	@Autowired
	private AuctionResultTablePanel tablePanel;
	@Autowired
	private AuctionResultFormPanel formPanel;

	@PostConstruct
	public void PostConstruct() {
		super.init();
		tablePanel.setMainPanel(this);
		formPanel.setCaption(I18N.message("auction.result"));
		getTabSheet().setTablePanel(tablePanel);
	}


	@Override
	public void enter(ViewChangeEvent event) {
	}

	@Override
	public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
		if (selectedTab == formPanel) {
			formPanel.assignValues(tablePanel.getItemSelectedId());
			formPanel.setMainPanel(this);
		} else if (selectedTab == tablePanel && getTabSheet().isNeedRefresh()) {
			tablePanel.refresh();
		}
		getTabSheet().setSelectedTab(selectedTab);
	}

	/**
	 * @see AbstractTabsheetPanel#onAddEventClick()
	 */
	@Override
	public void onAddEventClick() {
	}

	/**
	 * @see AbstractTabsheetPanel#onEditEventClick()
	 */
	@Override
	public void onEditEventClick() {
		getTabSheet().addFormPanel(formPanel);
		initSelectedTab(formPanel);
	}
	
	/**
	 * Display table panel
	 */
	public void displayTablePanel() {
		tablePanel.refresh();
		getTabSheet().setSelectedTab(tablePanel);
	}

}
