package com.soma.mfinance.third.creditbureau.cbc.model;

import com.soma.ersys.core.hr.model.address.*;

/**
 * 
 * @author tha.bunsath
 *
 */
public final class CBCWrongAddressCode {
	
	public static CBCAddressCode invalidCBCAddress(Address address){
		CBCAddressCode cbcAddressCode = null;
		if(address != null){
			Province province = address.getProvince();
			District district = address.getDistrict();
			Commune commune = address.getCommune();
			
			if(province != null && province.getCode() != null && district != null && district.getCode() != null && commune != null && commune.getCode() != null){
				if(province.getCode().equals("03") && district.getCode() != null && isDistrictCodeTboungKhmumProvince(district.getCode())){ 
	
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("25");
					cbcAddressCode.setDistrictCode(district.getCode());
					cbcAddressCode.setCommuneCode(commune.getCode());
					
							
			    }else if(province.getCode().equals("06") && district.getCode().equals("0603") && commune.getCode().equals("060208")){
							
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("06");
					cbcAddressCode.setDistrictCode("0602");
					cbcAddressCode.setCommuneCode("060208");
							
				}else if(province.getCode().equals("06") && district.getCode().equals("0601") && commune.getCode().equals("060502")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("06");
					cbcAddressCode.setDistrictCode("0605");
					cbcAddressCode.setCommuneCode("060502");
					
				} else if(province.getCode() != null && province.getCode().equals("08") && district.getCode() != null && district.getCode().equals("0808")
					&& commune.getCode() != null && commune.getCode().equals("120908")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1209");//Pur SenChey
					cbcAddressCode.setCommuneCode("120908");
					
				}else if(province.getCode().equals("08") && district.getCode().equals("0808") && commune.getCode().equals("120909")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1209");
					cbcAddressCode.setCommuneCode("120909");
					
				}else if(province.getCode().equals("08") && district.getCode().equals("0808") && commune.getCode().equals("120910")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1209"); //Pur SenChey
					cbcAddressCode.setCommuneCode("120910"); //Kantaok
					
				}else if(province.getCode().equals("08") && district.getCode().equals("0808") && commune.getCode().equals("120911")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1209"); //Pur SenChey
					cbcAddressCode.setCommuneCode("120911"); //Ovlaok
					
				}else if(province.getCode().equals("08") && district.getCode().equals("0808") && commune.getCode().equals("121105")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1211"); //Praek Pnov
					cbcAddressCode.setCommuneCode("121105"); //Ponsang
				}else if(province.getCode().equals("08") && district.getCode().equals("0808") && commune.getCode().equals("120913")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1209"); //Pur SenChey
					cbcAddressCode.setCommuneCode("120913"); //Snaor
					
				}else if(province.getCode().equals("08") && district.getCode().equals("0801") && commune.getCode().equals("120516")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1205"); //Dangkao
					cbcAddressCode.setCommuneCode("120516"); //Kong Noy
					
				}else if(province.getCode().equals("08") && district.getCode().equals("0801") && commune.getCode().equals("120517")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1205"); 
					cbcAddressCode.setCommuneCode("120517");
					
				}else if(province.getCode().equals("08") && district.getCode().equals("0801") && commune.getCode().equals("120519")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1205"); 
					cbcAddressCode.setCommuneCode("120519");
				
				}else if(province.getCode().equals("08") && district.getCode().equals("0801") && commune.getCode().equals("120520")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1205"); 
					cbcAddressCode.setCommuneCode("120520");
					
				}else if(province.getCode().equals("08") && district.getCode().equals("0802") && commune.getCode().equals("121207")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1212"); 
					cbcAddressCode.setCommuneCode("121207");
					
				}else if(province.getCode().equals("08") && district.getCode().equals("0802") && commune.getCode().equals("121206")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1212"); 
					cbcAddressCode.setCommuneCode("121206");
				}
				
				else if(province.getCode().equals("08") && district.getCode().equals("0802") && commune.getCode().equals("121208")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1212"); 
					cbcAddressCode.setCommuneCode("121208");
				}
				else if(province.getCode().equals("08") && district.getCode().equals("0802") && commune.getCode().equals("121205")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1212"); 
					cbcAddressCode.setCommuneCode("121205");
				}
				else if(province.getCode().equals("08") && district.getCode().equals("0807") && commune.getCode().equals("121005")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1210"); 
					cbcAddressCode.setCommuneCode("121005");
				}
				else if(province.getCode().equals("08") && district.getCode().equals("0807") && commune.getCode().equals("121004")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1211"); 
					cbcAddressCode.setCommuneCode("121004");
				}
				else if(province.getCode().equals("08") && district.getCode().equals("0809") && commune.getCode().equals("121102")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1211"); 
					cbcAddressCode.setCommuneCode("121102");
				} 
				
				else if(province.getCode().equals("08") && district.getCode().equals("0809") && commune.getCode().equals("121101")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1211"); 
					cbcAddressCode.setCommuneCode("121101");
				} 
				
				else if(province.getCode().equals("08") && district.getCode().equals("0809") && commune.getCode().equals("121103")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1211"); 
					cbcAddressCode.setCommuneCode("121103");
				}
				
				else if(province.getCode().equals("12") && district.getCode().equals("1205") && commune.getCode().equals("120807")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1208"); 
					cbcAddressCode.setCommuneCode("120807");
				}
				
				else if(province.getCode().equals("12") && district.getCode().equals("1205") && commune.getCode().equals("120906")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1209"); 
					cbcAddressCode.setCommuneCode("120906");
				}
				
				else if(province.getCode().equals("12") && district.getCode().equals("1207") && commune.getCode().equals("121001")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1210"); 
					cbcAddressCode.setCommuneCode("121001");
				}
				
				else if(province.getCode().equals("12") && district.getCode().equals("1207") && commune.getCode().equals("121002")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1210"); 
					cbcAddressCode.setCommuneCode("121002");
				}
				
				else if(province.getCode().equals("12") && district.getCode().equals("1207") && commune.getCode().equals("121003")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("12");
					cbcAddressCode.setDistrictCode("1210"); 
					cbcAddressCode.setCommuneCode("121003");
				}
				
				else if(province.getCode().equals("14") && district.getCode().equals("1411") && commune.getCode().equals("141003")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("14");
					cbcAddressCode.setDistrictCode("1410"); 
					cbcAddressCode.setCommuneCode("141003");
				}
				
				else if(province.getCode().equals("14") && district.getCode().equals("1410") && commune.getCode().equals("141304")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("14");
					cbcAddressCode.setDistrictCode("1413"); 
					cbcAddressCode.setCommuneCode("141304");
				}
				
				else if(province.getCode().equals("14") && district.getCode().equals("1408") && commune.getCode().equals("141107")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("14");
					cbcAddressCode.setDistrictCode("1411"); 
					cbcAddressCode.setCommuneCode("141107");
				}
				
				else if(province.getCode().equals("16") && district.getCode().equals("1603") && commune.getCode().equals("160701")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("16");
					cbcAddressCode.setDistrictCode("1607"); 
					cbcAddressCode.setCommuneCode("160701");
				}
				
				else if(province.getCode().equals("17") && district.getCode().equals("1709") && commune.getCode().equals("171011")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("17");
					cbcAddressCode.setDistrictCode("1710"); 
					cbcAddressCode.setCommuneCode("171011");
				}
				
				else if(province.getCode().equals("20") && district.getCode().equals("2007") && commune.getCode().equals("200607")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("20");
					cbcAddressCode.setDistrictCode("2006"); 
					cbcAddressCode.setCommuneCode("200607");
				}
				
				else if(province.getCode().equals("23") && district.getCode().equals("2301") && commune.getCode().equals("230203")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("23");
					cbcAddressCode.setDistrictCode("2302"); 
					cbcAddressCode.setCommuneCode("230203");
				}
				/* Kubuta*/
				else if(province.getCode().equals("02") && district.getCode().equals("0212") && commune.getCode().equals("021404")){
					cbcAddressCode = new CBCAddressCode();
					cbcAddressCode.setProvinceCode("02");
					cbcAddressCode.setDistrictCode("0214"); 
					cbcAddressCode.setCommuneCode("021404");
				}
			}
		}
		
		return cbcAddressCode;
		
	}
/**
 * 
 * @param address
 * @return
 */
public static Address checkInvalidCBCAddress(Address address){
			if(address != null){
				Province province = address.getProvince();
				District district = address.getDistrict();
				Commune commune = address.getCommune();
				Village village = address.getVillage();
				if(province != null && province.getCode() != null && district != null && district.getCode() != null && commune != null && commune.getCode() != null){
					if(province.getCode().equals("03") && district.getCode() != null && isDistrictCodeTboungKhmumProvince(district.getCode())){ 
						province.setCode("25");
								
				    }else if(province.getCode().equals("06") && district.getCode().equals("0603") && commune.getCode().equals("060208")){
						province.setCode("06");
						district.setCode("0602"); 
						commune.setCode("060208");//"060208"
							
					}else if(province.getCode().equals("06") && district.getCode().equals("0601") && commune.getCode().equals("060502")){
						province.setCode("06");
						district.setCode("0605");
						commune.setCode("060502");
						
					} else if(province.getCode() != null && province.getCode().equals("08") && district.getCode() != null && district.getCode().equals("0808")
						&& commune.getCode() != null && commune.getCode().equals("120908")){
						province.setCode("12");
						district.setCode("1209");//Pur SenChey
						commune.setCode("120908");
						
					}else if(province.getCode().equals("08") && district.getCode().equals("0808") && commune.getCode().equals("120909")){
						province.setCode("12");
						district.setCode("1209");
						commune.setCode("120909");
						
					}else if(province.getCode().equals("08") && district.getCode().equals("0808") && commune.getCode().equals("120910")){
						province.setCode("12");
						district.setCode("1209"); //Pur SenChey
						commune.setCode("120910"); //Kantaok
						
					}else if(province.getCode().equals("08") && district.getCode().equals("0808") && commune.getCode().equals("120911")){
						province.setCode("12");
						district.setCode("1209"); //Pur SenChey
						commune.setCode("120911"); //Ovlaok
						
					}else if(province.getCode().equals("08") && district.getCode().equals("0808") && commune.getCode().equals("121105")){
						province.setCode("12");
						district.setCode("1211"); //Praek Pnov
						commune.setCode("121105"); //Ponsang
					}else if(province.getCode().equals("08") && district.getCode().equals("0808") && commune.getCode().equals("120913")){
						province.setCode("12");
						district.setCode("1209"); //Pur SenChey
						commune.setCode("120913"); //Snaor
						
					}else if(province.getCode().equals("08") && district.getCode().equals("0801") && commune.getCode().equals("120516")){
						province.setCode("12");
						district.setCode("1205"); //Dangkao
						commune.setCode("120516"); //Kong Noy
						
					}else if(province.getCode().equals("08") && district.getCode().equals("0801") && commune.getCode().equals("120517")){
						province.setCode("12");
						district.setCode("1205"); 
						commune.setCode("120517");
						
					}else if(province.getCode().equals("08") && district.getCode().equals("0801") && commune.getCode().equals("120519")){
						province.setCode("12");
						district.setCode("1205"); 
						commune.setCode("120519");
					
					}else if(province.getCode().equals("08") && district.getCode().equals("0801") && commune.getCode().equals("120520")){
						province.setCode("12");
						district.setCode("1205"); 
						commune.setCode("120520");
						
					}else if(province.getCode().equals("08") && district.getCode().equals("0802") && commune.getCode().equals("121207")){
						province.setCode("12");
						district.setCode("1212"); 
						commune.setCode("121207");
						
					}else if(province.getCode().equals("08") && district.getCode().equals("0802") && commune.getCode().equals("121206")){
						province.setCode("12");
						district.setCode("1212"); 
						commune.setCode("121206");
					}
					
					else if(province.getCode().equals("08") && district.getCode().equals("0802") && commune.getCode().equals("121208")){
						province.setCode("12");
						district.setCode("1212"); 
						commune.setCode("121208");
					}
					else if(province.getCode().equals("08") && district.getCode().equals("0802") && commune.getCode().equals("121205")){
						province.setCode("12");
						district.setCode("1212"); 
						commune.setCode("121205");
					}
					else if(province.getCode().equals("08") && district.getCode().equals("0807") && commune.getCode().equals("121005")){
						province.setCode("12");
						district.setCode("1210"); 
						commune.setCode("121005");
					}
					else if(province.getCode().equals("08") && district.getCode().equals("0807") && commune.getCode().equals("121004")){
						province.setCode("12");
						district.setCode("1211"); 
						commune.setCode("121004");
					}
					else if(province.getCode().equals("08") && district.getCode().equals("0809") && commune.getCode().equals("121102")){
						province.setCode("12");
						district.setCode("1211"); 
						commune.setCode("121102");
					} 
					
					else if(province.getCode().equals("08") && district.getCode().equals("0809") && commune.getCode().equals("121101")){
						province.setCode("12");
						district.setCode("1211"); 
						commune.setCode("121101");
					} 
					
					else if(province.getCode().equals("08") && district.getCode().equals("0809") && commune.getCode().equals("121103")){
						province.setCode("12");
						district.setCode("1211"); 
						commune.setCode("121103");
					}
					
					else if(province.getCode().equals("12") && district.getCode().equals("1205") && commune.getCode().equals("120807")){
						province.setCode("12");
						district.setCode("1208"); 
						commune.setCode("120807");
					}
					
					else if(province.getCode().equals("12") && district.getCode().equals("1205") && commune.getCode().equals("120906")){
						province.setCode("12");
						district.setCode("1209"); 
						commune.setCode("120906");
					}
					
					else if(province.getCode().equals("12") && district.getCode().equals("1207") && commune.getCode().equals("121001")){
						province.setCode("12");
						district.setCode("1210"); 
						commune.setCode("121001");
					}
					
					else if(province.getCode().equals("12") && district.getCode().equals("1207") && commune.getCode().equals("121002")){
						province.setCode("12");
						district.setCode("1210"); 
						commune.setCode("121002");
					}
					
					else if(province.getCode().equals("12") && district.getCode().equals("1207") && commune.getCode().equals("121003")){
						province.setCode("12");
						district.setCode("1210"); 
						commune.setCode("121003");
					}
					
					else if(province.getCode().equals("14") && district.getCode().equals("1411") && commune.getCode().equals("141003")){
						province.setCode("14");
						district.setCode("1410"); 
						commune.setCode("141003");
					}
					
					else if(province.getCode().equals("14") && district.getCode().equals("1410") && commune.getCode().equals("141304")){
						province.setCode("14");
						district.setCode("1413"); 
						commune.setCode("141304");
					}
					
					else if(province.getCode().equals("14") && district.getCode().equals("1408") && commune.getCode().equals("141107")){
						province.setCode("14");
						district.setCode("1411"); 
						commune.setCode("141107");
					}
					
					else if(province.getCode().equals("16") && district.getCode().equals("1603") && commune.getCode().equals("160701")){
						province.setCode("16");
						district.setCode("1607"); 
						commune.setCode("160701");
					}
					
					else if(province.getCode().equals("17") && district.getCode().equals("1709") && commune.getCode().equals("171011")){
						province.setCode("17");
						district.setCode("1710"); 
						commune.setCode("171011");
					}
					
					else if(province.getCode().equals("20") && district.getCode().equals("2007") && commune.getCode().equals("200607")){
						province.setCode("20");
						district.setCode("2006"); 
						commune.setCode("200607");
					}
					
					else if(province.getCode().equals("23") && district.getCode().equals("2301") && commune.getCode().equals("230203")){
						province.setCode("23");
						district.setCode("2302"); 
						commune.setCode("230203");
					}
					/* --- Kubuta wrong address code address code*/				
					else if(province.getCode().equals("14") && district.getCode().equals("1406") && commune.getCode().equals("140708")){
						province.setCode("14");
						district.setCode("1407"); 
						commune.setCode("140708");
					}
					else if(province.getCode().equals("02") && district.getCode().equals("0212") && commune.getCode().equals("021404")){
						province.setCode("02");
						district.setCode("0214"); 
						commune.setCode("021404");
					}
					else if(province.getCode().equals("08") && district.getCode().equals("0801") && commune.getCode().equals("120518")){
						province.setCode("08");
						district.setCode("1205"); 
						commune.setCode("120518");
					}
					//"";"";""
					else if(province.getCode().equals("08") && district.getCode().equals("0802") && commune.getCode().equals("121207")){
						province.setCode("08");
						district.setCode("1212"); 
						commune.setCode("121207");
					}
					else if(province.getCode().equals("08") && district.getCode().equals("0802") && commune.getCode().equals("121206")){
						province.setCode("08");
						district.setCode("1212"); 
						commune.setCode("121206");
					}
				}
				address.setProvince(province);
				address.setDistrict(district);
				address.setCommune(commune);
				address.setVillage(village);
			}
			
			return address;
			
		}
/**
 * @param distrCode
 * @return
 */
private static boolean isDistrictCodeTboungKhmumProvince(String distrCode){
			boolean result = false;
			if(distrCode.equals("2501")
			  ||distrCode.equals("2502")
			  ||distrCode.equals("2503")
			  ||distrCode.equals("2504")
			  ||distrCode.equals("2505")
			  ||distrCode.equals("2507")
			  ||distrCode.equals("2506")
			){
				result = true;
			}
			return result;
		}
}
