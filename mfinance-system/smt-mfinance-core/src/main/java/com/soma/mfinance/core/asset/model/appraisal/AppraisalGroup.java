package com.soma.mfinance.core.asset.model.appraisal;

import org.seuksa.frmk.model.EntityFactory;
import org.seuksa.frmk.model.entity.EntityRefA;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Kimsuor SEANG
 * Date  : 7/18/2017
 * Name  : 10:09 AM
 * Email : k.seang@gl-f.com
 */
@Entity
@Table(name = "tu_appraisal_group")
public class AppraisalGroup extends EntityRefA {


    private Double coefficient;
    private List<AppraisalItem> appraisalItems;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "app_group_id", unique = true, nullable = false)
    public Long getId() {
        return this.id;
    }

    public AppraisalGroup() {
    }

    public static AppraisalGroup createInstance() {
        AppraisalGroup instance = EntityFactory.createInstance(AppraisalGroup.class);
        return instance;
    }

    @Override
    @Column(name = "app_group_code", nullable = true, length = 10)
    public String getCode() {
        return super.getCode();
    }

    @Override
    @Column(name = "app_group_desc", nullable = true, length = 255)
    public String getDesc() {
        return super.getDesc();
    }

    @Override
    @Column(name = "app_group_desc_en", length = 255)
    public String getDescEn() {
        return super.getDescEn();
    }

    @Column(name = "app_group_coeff", nullable = true)
    public Double getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(Double coefficient) {
        this.coefficient = coefficient;
    }

    @OneToMany(mappedBy = "appraisalGroup", fetch = FetchType.LAZY)
    public List<AppraisalItem> getAppraisalItems() {
        return appraisalItems;
    }

    public void setAppraisalItems(List<AppraisalItem> appraisalItems) {
        this.appraisalItems = appraisalItems;
    }
}
