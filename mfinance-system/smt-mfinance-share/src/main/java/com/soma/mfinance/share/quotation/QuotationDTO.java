package com.soma.mfinance.share.quotation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.soma.mfinance.share.document.DocumentDTO;
import com.soma.mfinance.share.quotation.applicant.ApplicantDTO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by cheasocheat on 4/7/17.
 */
public class QuotationDTO implements Serializable {

    private Long qId;
    private String createUser;
    private String updateUser;
    private String referrence;
    private Date activateDate;
    private String wrk_status_id;

    public Long getqId() {
        return qId;
    }

    public void setqId(Long qId) {
        this.qId = qId;
    }


    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }


    public String getWrk_status_id() {
        return wrk_status_id;
    }

    public void setWrk_status_id(String wrk_status_id) {
        this.wrk_status_id = wrk_status_id;
    }

    public String getReferrence() {
        return referrence;
    }

    public void setReferrence(String referrence) {
        this.referrence = referrence;
    }

    public Date getActivateDate() {
        return activateDate;
    }

    public void setActivateDate(Date activateDate) {
        this.activateDate = activateDate;
    }
}
