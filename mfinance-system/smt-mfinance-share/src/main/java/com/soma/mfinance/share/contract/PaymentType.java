package com.soma.mfinance.share.contract;


/**
 * 
 * @author kimsuor.seang
 */
public enum PaymentType {
	AUCTION,
	CLAIM,
	COMPENSATION
}