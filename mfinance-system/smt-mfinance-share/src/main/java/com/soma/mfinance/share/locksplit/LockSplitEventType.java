package com.soma.mfinance.share.locksplit;


/**
 * 
 * @author kimsuor.seang
 */
public enum LockSplitEventType {
	EAR,
	TRAN;
}