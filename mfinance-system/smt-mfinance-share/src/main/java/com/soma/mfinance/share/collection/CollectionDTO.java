package com.soma.mfinance.share.collection;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Kimsuor SEANG
 * Date  : 11/24/2017
 * Name  : 2:37 PM
 * Email : k.seang@gl-f.com
 */
public class CollectionDTO implements Serializable {

    private Long id;
    private String creationUser;
    private Date updateDate;
    private String contractNo;
    private String collectionStatus;
    private String customerAttri;
    private String actionSchedule;
    private Double amountPromiseToPay;
    private Date startPeroidPromiseToPay;
    private Date endPeroidPromiseToPay;
    private String leaseMobilePhone1;
    private String leaseMobilePhone2;
    private String guarantorMobilePhone;
    private String customer;
    private String dealer;
    private String creditOfficer;
    private Integer term;
    private String  area;
    private Integer numOverduleInDay;
    private Date lastestPaymentDate;
    private Integer numberInstallmentInOverdue;
    private Double lastestNumInstallPaid;
    private Double totalPenaltyAmountNotPaid;
    private Integer numberInstallmentPaid;
    private String paymentMethodUsedForLastPayment;
    private Date contractStartDate;
    private Double outstandingBalance;
    private String provice;
    private String district;
    private String commune;
    private String village;
    private String placeOfBirth;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getCollectionStatus() {
        return collectionStatus;
    }

    public void setCollectionStatus(String collectionStatus) {
        this.collectionStatus = collectionStatus;
    }

    public String getCustomerAttri() {
        return customerAttri;
    }

    public void setCustomerAttri(String customerAttri) {
        this.customerAttri = customerAttri;
    }

    public String getActionSchedule() {
        return actionSchedule;
    }

    public void setActionSchedule(String actionSchedule) {
        this.actionSchedule = actionSchedule;
    }

    public Double getAmountPromiseToPay() {
        return amountPromiseToPay;
    }

    public void setAmountPromiseToPay(Double amountPromiseToPay) {
        this.amountPromiseToPay = amountPromiseToPay;
    }

    public Date getStartPeroidPromiseToPay() {
        return startPeroidPromiseToPay;
    }

    public void setStartPeroidPromiseToPay(Date startPeroidPromiseToPay) {
        this.startPeroidPromiseToPay = startPeroidPromiseToPay;
    }

    public Date getEndPeroidPromiseToPay() {
        return endPeroidPromiseToPay;
    }

    public void setEndPeroidPromiseToPay(Date endPeroidPromiseToPay) {
        this.endPeroidPromiseToPay = endPeroidPromiseToPay;
    }

    public String getLeaseMobilePhone1() {
        return leaseMobilePhone1;
    }

    public void setLeaseMobilePhone1(String leaseMobilePhone1) {
        this.leaseMobilePhone1 = leaseMobilePhone1;
    }

    public String getLeaseMobilePhone2() {
        return leaseMobilePhone2;
    }

    public void setLeaseMobilePhone2(String leaseMobilePhone2) {
        this.leaseMobilePhone2 = leaseMobilePhone2;
    }

    public String getGuarantorMobilePhone() {
        return guarantorMobilePhone;
    }

    public void setGuarantorMobilePhone(String guarantorMobilePhone) {
        this.guarantorMobilePhone = guarantorMobilePhone;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getDealer() {
        return dealer;
    }

    public void setDealer(String dealer) {
        this.dealer = dealer;
    }

    public String getCreditOfficer() {
        return creditOfficer;
    }

    public void setCreditOfficer(String creditOfficer) {
        this.creditOfficer = creditOfficer;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Integer getNumOverduleInDay() {
        return numOverduleInDay;
    }

    public void setNumOverduleInDay(Integer numOverduleInDay) {
        this.numOverduleInDay = numOverduleInDay;
    }

    public Date getLastestPaymentDate() {
        return lastestPaymentDate;
    }

    public void setLastestPaymentDate(Date lastestPaymentDate) {
        this.lastestPaymentDate = lastestPaymentDate;
    }

    public Integer getNumberInstallmentInOverdue() {
        return numberInstallmentInOverdue;
    }

    public void setNumberInstallmentInOverdue(Integer numberInstallmentInOverdue) {
        this.numberInstallmentInOverdue = numberInstallmentInOverdue;
    }

    public Double getLastestNumInstallPaid() {
        return lastestNumInstallPaid;
    }

    public void setLastestNumInstallPaid(Double lastestNumInstallPaid) {
        this.lastestNumInstallPaid = lastestNumInstallPaid;
    }

    public Double getTotalPenaltyAmountNotPaid() {
        return totalPenaltyAmountNotPaid;
    }

    public void setTotalPenaltyAmountNotPaid(Double totalPenaltyAmountNotPaid) {
        this.totalPenaltyAmountNotPaid = totalPenaltyAmountNotPaid;
    }

    public Integer getNumberInstallmentPaid() {
        return numberInstallmentPaid;
    }

    public void setNumberInstallmentPaid(Integer numberInstallmentPaid) {
        this.numberInstallmentPaid = numberInstallmentPaid;
    }

    public String getPaymentMethodUsedForLastPayment() {
        return paymentMethodUsedForLastPayment;
    }

    public void setPaymentMethodUsedForLastPayment(String paymentMethodUsedForLastPayment) {
        this.paymentMethodUsedForLastPayment = paymentMethodUsedForLastPayment;
    }

    public Date getContractStartDate() {
        return contractStartDate;
    }

    public void setContractStartDate(Date contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    public Double getOutstandingBalance() {
        return outstandingBalance;
    }

    public void setOutstandingBalance(Double outstandingBalance) {
        this.outstandingBalance = outstandingBalance;
    }

    public String getProvice() {
        return provice;
    }

    public void setProvice(String provice) {
        this.provice = provice;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }
}
