package com.soma.mfinance.share.quotation.applicant;


import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by cheasocheat on 4/7/17.
 */
public class ApplicantDTO {
    @JsonProperty("APPLICANT_ID")
    private long id;
    @JsonProperty("CIVILITY")
    private String title;
    @JsonProperty("FIRST_NAME")
    private String firstName;
    @JsonProperty("FIRST_NAME_EN")
    private String firstNameEn;
    @JsonProperty("LAST_NAME")
    private String lastName;
    @JsonProperty("LAST_NAME_EN")
    private String lastNameEn;
    @JsonProperty("MOBILE_PHONE")
    private String mobilePhone;
    @JsonProperty("MOBILE_PHONE_2")
    private String mobilePhone2;
    @JsonProperty("BIRTH_DATE")
    private String birthDate;
    @JsonProperty("TOTAL_FAMILY_MEMBER")
    private Integer totalFamilyMember;
    @JsonProperty("SPOUSE_MOBILE")
    private String spouseMobile;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstNameEn() {
        return firstNameEn;
    }

    public void setFirstNameEn(String firstNameEn) {
        this.firstNameEn = firstNameEn;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastNameEn() {
        return lastNameEn;
    }

    public void setLastNameEn(String lastNameEn) {
        this.lastNameEn = lastNameEn;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getMobilePhone2() {
        return mobilePhone2;
    }

    public void setMobilePhone2(String mobilePhone2) {
        this.mobilePhone2 = mobilePhone2;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getTotalFamilyMember() {
        return totalFamilyMember;
    }

    public void setTotalFamilyMember(Integer totalFamilyMember) {
        this.totalFamilyMember = totalFamilyMember;
    }

    public String getSpouseMobile() {
        return spouseMobile;
    }

    public void setSpouseMobile(String spouseMobile) {
        this.spouseMobile = spouseMobile;
    }
}
