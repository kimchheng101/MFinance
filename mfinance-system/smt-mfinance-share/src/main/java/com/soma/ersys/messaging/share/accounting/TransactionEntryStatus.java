package com.soma.ersys.messaging.share.accounting;


/**
 * 
 * @author kimsuor.seang
 */
public enum TransactionEntryStatus {
	
	DRAFT,
	REJECTED,
	APPROVED,
	POSTED;
}