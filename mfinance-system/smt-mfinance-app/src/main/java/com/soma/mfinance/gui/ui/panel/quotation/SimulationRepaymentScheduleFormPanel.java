package com.soma.mfinance.gui.ui.panel.quotation;

import com.soma.mfinance.core.applicant.service.ValidateNumbers;
import com.soma.mfinance.core.financial.model.*;
import com.soma.mfinance.core.interestrate.InterestRate;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationService;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.finance.services.shared.system.EFrequency;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.Reindeer;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.model.entity.Entity;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyMathUtils;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.soma.mfinance.core.helper.FinServicesHelper.FIN_PROD_SRV;
import static com.soma.mfinance.core.shared.financialproduct.FinancialProductEntityField.*;
import static com.soma.mfinance.core.shared.quotation.QuotationEntityField.END_DATE;
import static com.soma.mfinance.core.shared.quotation.QuotationEntityField.START_DATE;

/**
 * Simulation re payment schedule panel
 *
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SimulationRepaymentScheduleFormPanel extends AbstractFormPanel implements ValueChangeListener {

    private static final long serialVersionUID = -3948949199018017119L;

    private TextField txtAppraisalEstimateAmount;
    private TextField txtLeaseAmount;
    private TextField txtOtherLeaseAmount;
    private TextField txtDealerLeaseAmount;
    private TextField txtInstallmentAmount;
    private TextField txtTotalInstallmentAmount;

    private EntityRefComboBox<FinProduct> cbxFinancialProduct;
    private ERefDataComboBox<EFrequency> cbxFrequency;
    private EntityRefComboBox<LeaseAmountPercent> cbxLeaseAmountPercentage;
    private EntityRefComboBox<Term> cbxTermInMonth;

    private EntityRefComboBox<InterestRate> cbxInterestRate;
    private BaseRestrictions<InterestRate> InterestRateRestrictions;

    private HorizontalLayout invalidMessageLayout;
    private VerticalLayout servicesLayout;
    private Map<String, ContentValue> services;

    private Quotation tmpQuotation;

    TextField txtTiServiceAmount;
    CheckBox cbSplitWithInstallment;
    @SuppressWarnings("unused")
    private boolean invalidQuotation;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        setCaption(I18N.message("simulation.repayment.schedule"));
        setSizeFull();
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractFormPanel#createForm()
     */
    @SuppressWarnings("serial")
    @Override
    protected com.vaadin.ui.Component createForm() {
        ValidateNumbers simulator = new ValidateNumbers();
        ArrayList validAmount = new ArrayList();
        invalidQuotation = true;

        servicesLayout = new VerticalLayout();

        Label iconInvalidMessage = new Label();
        iconInvalidMessage.setIcon(new ThemeResource("../smt-default/icons/16/danger.png"));
        Label lblInvalidMessage = new Label(I18N.message("quotation.invalid"));
        lblInvalidMessage.setStyleName("error");
        invalidMessageLayout = new HorizontalLayout(iconInvalidMessage, lblInvalidMessage);
        invalidMessageLayout.setVisible(true);

        cbxFinancialProduct = new EntityRefComboBox<FinProduct>();
        BaseRestrictions<FinProduct> restrictions = new BaseRestrictions<>(FinProduct.class);
        restrictions.addCriterion(Restrictions.le(START_DATE, DateUtils.getDateAtEndOfDay(DateUtils.todayH00M00S00())));
        restrictions.addCriterion(Restrictions.ge(END_DATE, DateUtils.todayH00M00S00()));
        restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
        cbxFinancialProduct.setRestrictions(new BaseRestrictions<>(FinProduct.class));
        cbxFinancialProduct.renderer();
        cbxFinancialProduct.setEnabled(false);
        if (ProfileUtil.isCreditOfficer() || ProfileUtil.isProductionOfficer())
            cbxFinancialProduct.setEnabled(true);
        cbxFinancialProduct.addValueChangeListener(this);
        cbxFinancialProduct.setImmediate(true);
        cbxFinancialProduct.setSelectedEntity(null);

        txtAppraisalEstimateAmount = ComponentFactory.getTextField(false, 30, 170);
        validAmount.add(txtAppraisalEstimateAmount);

        txtLeaseAmount = ComponentFactory.getTextField(false, 50, 170);
        txtLeaseAmount.setStyleName("blackdisabled");
        txtLeaseAmount.setEnabled(false);
        txtLeaseAmount.setValue(null);
        validAmount.add(txtLeaseAmount);

        txtDealerLeaseAmount = ComponentFactory.getTextField(false, 50, 170);
        txtDealerLeaseAmount.setVisible(false);
        txtOtherLeaseAmount = ComponentFactory.getTextField(false, 50, 170);
        txtOtherLeaseAmount.setEnabled(false);

        cbxTermInMonth = new EntityRefComboBox<>();
        cbxTermInMonth.setRestrictions(new BaseRestrictions<>(Term.class));
        cbxTermInMonth.renderer();

        cbxInterestRate = new EntityRefComboBox<InterestRate>();
        InterestRateRestrictions = new BaseRestrictions<>(InterestRate.class);
        cbxInterestRate.setRestrictions(InterestRateRestrictions);
        cbxInterestRate.setImmediate(true);
        cbxInterestRate.renderer();
        cbxInterestRate.setSelectedEntity(null);

        cbxLeaseAmountPercentage = new EntityRefComboBox<>();
        BaseRestrictions resLeaseAmountPer = new BaseRestrictions<>(LeaseAmountPercent.class);
        resLeaseAmountPer.addOrder(Order.desc("value"));
        cbxLeaseAmountPercentage.setRestrictions(resLeaseAmountPer);
        cbxLeaseAmountPercentage.renderer();
        cbxLeaseAmountPercentage.addValueChangeListener(this);

        cbxFrequency = new ERefDataComboBox<EFrequency>(EFrequency.listInMonth());
        cbxFrequency.setSelectedEntity(EFrequency.M);
        cbxFrequency.setEnabled(false);
        cbxFrequency.setStyleName("blackdisabled");

        txtInstallmentAmount = ComponentFactory.getTextField(false, 50, 150);
        txtInstallmentAmount.setStyleName("blackdisabled");
        txtInstallmentAmount.setEnabled(false);

        txtTotalInstallmentAmount = ComponentFactory.getTextField(false, 50, 150);
        txtTotalInstallmentAmount.setStyleName("blackdisabled");
        txtTotalInstallmentAmount.setEnabled(false);

        cbxFinancialProduct.addValueChangeListener(this);

        Button btnCalcul = new Button("");
        btnCalcul.setIcon(new ThemeResource("icons/32/calculatrice.png"));
        btnCalcul.setStyleName(Reindeer.BUTTON_LINK);


        btnCalcul.addClickListener(new ClickListener() {
            private static final long serialVersionUID = -6516775560049407997L;

            @Override
            public void buttonClick(ClickEvent event) {
                if (checkValidityField()) {
                    System.out.print("Clicked");
                    calculateInstallmentAmount();
                }
            }
        });

        String template = "simulationRepaymentSchedule";
        InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
        CustomLayout inputFieldLayout = null;
        try {
            inputFieldLayout = new CustomLayout(layoutFile);
        } catch (IOException e) {
            Notification.show("Could not locate template " + template, e.getMessage(), Type.ERROR_MESSAGE);
        }

        cbxFinancialProduct.addValueChangeListener(this);
        cbxLeaseAmountPercentage.addValueChangeListener(this);
        cbxInterestRate.addValueChangeListener(this);
        cbxTermInMonth.addValueChangeListener(this);

        inputFieldLayout.setSizeFull();
        inputFieldLayout.addComponent(new Label(I18N.message("financial.product")), "lblFinancialProduct");
        inputFieldLayout.addComponent(cbxFinancialProduct, "cbxFinancialProduct");
        inputFieldLayout.addComponent(new Label(I18N.message("groupdetail.estimate.amount")), "lblAppraisalEstimateAmount");
        inputFieldLayout.addComponent(txtAppraisalEstimateAmount, "txtAppraisalEstimateAmount");
        inputFieldLayout.addComponent(new Label(I18N.message("advance.payment.percentage")), "lblAdvancePaymentPecentage");
        inputFieldLayout.addComponent(new Label(I18N.message("lease.amount.percentage")), "lblLeaseAmountPecentage");
        inputFieldLayout.addComponent(cbxLeaseAmountPercentage, "cbxLeaseAmountPercentage");

        inputFieldLayout.addComponent(new Label(I18N.message("advance.payment")), "lblAdvancePayment");
        inputFieldLayout.addComponent(new Label(I18N.message("lease.amount")), "lblLeaseAmount");
        inputFieldLayout.addComponent(txtLeaseAmount, "txtLeaseAmount");
        inputFieldLayout.addComponent(new Label(I18N.message("frequency")), "lblFrequency");
        inputFieldLayout.addComponent(cbxFrequency, "cbxFrequency");
        inputFieldLayout.addComponent(new Label(I18N.message("term.month")), "lblTermInMonth");
        inputFieldLayout.addComponent(cbxTermInMonth, "cbxTermInMonth");
        inputFieldLayout.addComponent(new Label(I18N.message("interest.percent")), "lblInterestRate");
        inputFieldLayout.addComponent(cbxInterestRate, "cbxInterestRate");
        inputFieldLayout.addComponent(new Label(I18N.message("installment.amount")), "lblInstallmentAmont");
        inputFieldLayout.addComponent(new HorizontalLayout(txtInstallmentAmount, btnCalcul), "txtInstallmentAmount");

        final Panel financialProductPanel = new Panel(I18N.message("repayment.schedule"));
        this.ServicePanel();
        VerticalLayout contentLayout = new VerticalLayout(inputFieldLayout, servicesLayout);
        contentLayout.setMargin(true);
        financialProductPanel.setContent(contentLayout);
        setInvalidQuotationFlag(false);
        simulator.validateNumberDot(validAmount);
        return financialProductPanel;
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractFormPanel#getEntity()
     */
    @Override
    protected Entity getEntity() {
        return null;
    }

    /**
     * @see com.vaadin.data.Property.ValueChangeListener#valueChange(com.vaadin.data.Property.ValueChangeEvent)
     */

    public void ServicePanel() {
        FinProduct financialProduct;
        if ((financialProduct = cbxFinancialProduct.getSelectedEntity()) != null) {
            if (financialProduct.getFinancialProductServices() != null) {
                Panel servicePanel = new Panel(I18N.message("services"));
                final GridLayout gridServiceLayout = new GridLayout(15, financialProduct.getFinancialProductServices().size() + 1);
                gridServiceLayout.setSpacing(true);
                gridServiceLayout.setMargin(true);
                gridServiceLayout.addComponent(ComponentFactory.getLabel("", 150), 0, 0);
                gridServiceLayout.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS), 1, 0);
                gridServiceLayout.addComponent(ComponentFactory.getLabel("amount", 150), 2, 0);
                gridServiceLayout.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS), 3, 0);
                gridServiceLayout.addComponent(ComponentFactory.getLabel("split.with.installment", 150), 4, 0);
                gridServiceLayout.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS), 5, 1);
                gridServiceLayout.addComponent(new Label(I18N.message("total.installment.amount")), 6, 1);
                gridServiceLayout.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS), 7, 1);
                gridServiceLayout.addComponent(txtTotalInstallmentAmount, 8, 1);
                int row = 1;
                services = new HashMap<>();
                for (FinProductService financialService : financialProduct.getFinancialProductServices()) {
                    txtTiServiceAmount = new TextField();
                    cbSplitWithInstallment = new CheckBox();
                    cbSplitWithInstallment.setEnabled(false);
                    cbSplitWithInstallment.setValue(financialService.getService().isSplitWithInstallment());
                    txtTiServiceAmount.setData(financialService.getService());
                    txtTiServiceAmount.setEnabled(financialService.getService().isAllowChangePrice());
                    if (financialService.getService().getServiceType().equals(EServiceType.INSFEE)) {
                        txtTiServiceAmount.setValue("");
                    } else{
                        txtTiServiceAmount.setValue(AmountUtils.format(financialService.getService().getTePrice()));
                    }
                    if (!financialService.getService().getCode().equals("PRI_AMT")) {
                        gridServiceLayout.addComponent(new Label(financialService.getService().getDescEn()), 0, row);
                        gridServiceLayout.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS), 1, row);
                        gridServiceLayout.addComponent(txtTiServiceAmount, 2, row);
                        gridServiceLayout.addComponent(ComponentFactory.getSpaceLayout(15, Unit.PIXELS), 3, row);
                        gridServiceLayout.addComponent(cbSplitWithInstallment, 4, row);
                    }
                    services.put(financialService.getService().getCode(), new SimulationRepaymentScheduleFormPanel.ContentValue(txtTiServiceAmount, cbSplitWithInstallment));

                    row++;
                }
                servicePanel.setContent(gridServiceLayout);
                servicesLayout.removeAllComponents();
                servicesLayout.addComponent(servicePanel);
            }
        }
    }

    public void calculateInstallmentAmount() {
        if (checkValidityField()) {
            tmpQuotation = new Quotation();
            tmpQuotation.setFinancialProduct(cbxFinancialProduct.getSelectedEntity());
            tmpQuotation.setLeaseAmountPercentage(cbxLeaseAmountPercentage.getSelectedEntity().getValue());
            tmpQuotation.setTiAppraisalEstimateAmount(MyNumberUtils.getDouble(txtAppraisalEstimateAmount.getValue(), 0d));
            tmpQuotation.setTiFinanceAmount(MyNumberUtils.getDouble(txtLeaseAmount.getValue(), 0d));
            tmpQuotation.setFrequency(EFrequency.M);
            tmpQuotation.setInterestRate(cbxInterestRate.getSelectedEntity().getValue());
            tmpQuotation.setTerm(cbxTermInMonth.getSelectedEntity().getValue());
            tmpQuotation.setFirstDueDate(DateUtils.today());

            tmpQuotation.setVatAdvancePaymentAmount(0d);
            tmpQuotation.setTmFinanceAmount(getDouble(txtLeaseAmount, 0d));
            tmpQuotation.setNumberOfPrincipalGracePeriods(cbxFinancialProduct.getSelectedEntity().getNumberOfPrincipalGracePeriods());
            tmpQuotation.setValid(!invalidQuotation);
            tmpQuotation.setVatValue(cbxFinancialProduct.getSelectedEntity().getVat() != null ? cbxFinancialProduct.getSelectedEntity().getVat().getValue() : 0.0d);

            List<FinProduct> finProductList = FIN_PROD_SRV.list(FinProduct.class);
            FinProduct selectedProduct = cbxFinancialProduct.getSelectedEntity();
            for (FinProduct finProduct : finProductList) {
                if (FIN_CODE_PUBLIC.equals(selectedProduct.getCode())) {
                    tmpQuotation.setVatValue(finProduct.getVat().getValue());
                } else if (FIN_CODE_EXISTING.equals(selectedProduct.getCode())) {
                    tmpQuotation.setVatValue(finProduct.getVat().getValue());
                }
            }
            List<QuotationService> quotationServices = new ArrayList<>();
            // Service
            for (ContentValue contentValue : services.values()) {
                QuotationService quotationService = new QuotationService();
                if(txtTiServiceAmount.getData() != null){
                    if (contentValue.finService.getServiceType().equals(EServiceType.INSFEE)) {
                        contentValue.finService.setTePrice(calculateInsurance());
                    } else if (contentValue.finService.getServiceType().equals(EServiceType.SRVFEE)) {
                        contentValue.finService.setTePrice(MyNumberUtils.getDouble(contentValue.textField.getValue(),0d));
                    } else if (contentValue.finService.getServiceType().equals(EServiceType.TRANSFEE)) {
                        contentValue.finService.setTePrice(MyNumberUtils.getDouble(contentValue.textField.getValue(),0d));
                    }else if (contentValue.finService.getServiceType().equals(EServiceType.SRVFEEEXT)) {
                        contentValue.finService.setTePrice(MyNumberUtils.getDouble(contentValue.textField.getValue(), 0d));
                    } else if (contentValue.finService.getServiceType().equals(EServiceType.INS_SERV_VAT)) {
                        contentValue.finService.setTePrice(0d);
                    }
                }
                double serviceAmount;
                double vatAmount;
                serviceAmount = calculateService(contentValue);
                vatAmount = calculateServiceVat(serviceAmount, contentValue);
                quotationService.setQuotation(this.tmpQuotation);
                quotationService.setService(contentValue.finService);
                quotationService.setTiPrice(serviceAmount);
                quotationService.setTePrice(serviceAmount);
                quotationService.setVatPrice(vatAmount);
                quotationService.setVatValue(contentValue.finService.getVat().getValue());
                quotationService.setSplitWithInstallment(contentValue.checkBox.getValue());

                quotationServices.add(quotationService);
            }
            tmpQuotation.setQuotationServices(quotationServices);

            Map<String, Double> mapCal = FIN_PROD_SRV.calculationInstallmentAmount(tmpQuotation);
            txtTotalInstallmentAmount.setValue(AmountUtils.format(mapCal.get(TOTAL_INT_AMOUNT)));
            txtInstallmentAmount.setValue(AmountUtils.format(mapCal.get(PRINCILPLE_AMOUNT)));
        }
    }

    /**
     * @return
     */
    public boolean checkValidityField() {
        //super.removeErrorsPanel();
        checkMandatorySelectField(cbxFinancialProduct, "financial.product");
        checkMandatoryField(txtAppraisalEstimateAmount, "appraisal.estimatePrice");
        checkMandatorySelectField(cbxTermInMonth, "term.month");
        checkMandatorySelectField(cbxInterestRate, "interest.rate");
        checkMandatorySelectField(cbxLeaseAmountPercentage, "lease.amount.percentage");
        checkMandatorySelectField(cbxFrequency, "frequency");
        return errors.isEmpty();
    }

    /**
     * @param invalid
     */
    public void setInvalidQuotationFlag(boolean invalid) {
        invalidQuotation = invalid;
        invalidMessageLayout.setVisible(invalid);
    }

    @Override
    public void valueChange(ValueChangeEvent event) {
        if (event.getProperty().equals(cbxFinancialProduct)) {
            if (cbxFinancialProduct.getSelectedEntity() != null) {
                FinProduct selectedProduct = cbxFinancialProduct.getSelectedEntity();
                if (ProfileUtil.isCreditOfficer() || ProfileUtil.isAdmin() || ProfileUtil.isProductionOfficer()) {
                    if (FIN_CODE_PUBLIC.equals(selectedProduct.getCode()))
                        ServicePanel();
                    else if (FIN_CODE_EXISTING.equals(selectedProduct.getCode()))
                        ServicePanel();
                }
                setInvalidQuotationFlag(false);
            } else {
                setInvalidQuotationFlag(true);
            }
        } else if (event.getProperty().equals(cbxLeaseAmountPercentage)) {
            if (cbxLeaseAmountPercentage.getSelectedEntity() != null) {
                txtLeaseAmount.setValue(AmountUtils.format(calculateLeaseAmount()));
                valueServiceChange();
                calculateInsurance();
            } else {
                txtLeaseAmount.setValue("0.00");
            }
        } else if (event.getProperty().equals(cbxInterestRate)) {
            if (cbxInterestRate.getSelectedEntity() != null) {
                cbxInterestRate.setValue("");
            }
        } else if (event.getProperty().equals(cbxFrequency)) {
            if (cbxFrequency.getSelectedEntity() != null) {
                txtInstallmentAmount.setValue("");
            }
        } else if (event.getProperty().equals(cbxTermInMonth)) {
            if (cbxTermInMonth.getSelectedEntity() != null) {
                calculateInsurance();
                txtInstallmentAmount.setValue("");
            }
        }
    }

    private static class ContentValue {
        FinService finService;
        TextField textField;
        CheckBox checkBox;

        ContentValue(TextField textField, CheckBox checkBox) {
            this.textField = textField;
            this.checkBox = checkBox;
            if (textField != null && textField.getData() instanceof FinService)
                finService = (FinService) textField.getData();
        }
    }

    /**
     * Calculate Lease Amount
     *
     * @return
     */
    private double calculateLeaseAmount() {
        if (cbxLeaseAmountPercentage.getSelectedEntity() != null && txtAppraisalEstimateAmount.getValue() != null)
            return MyMathUtils.roundUp((cbxLeaseAmountPercentage.getSelectedEntity().getValue() / 100) * getDouble(txtAppraisalEstimateAmount, 0d),10);
        return 0.00d;
    }

    private double calculateService(ContentValue contentValue) {
        return MyMathUtils.roundAmountTo(contentValue.finService.getTePrice());
    }

    private double calculateServiceVat(double serviceAmount, ContentValue contentValue) {
        return MyMathUtils.roundAmountTo(serviceAmount * (contentValue.finService.getVat().getValue() / 100));
    }

    private Double calculateInsurance() {
        Integer term = cbxTermInMonth.getSelectedEntity() != null ? cbxTermInMonth.getSelectedEntity().getValue() : 12;
        Double leaseAmountValue = txtLeaseAmount.getValue() != null ? MyNumberUtils.getDouble(txtLeaseAmount.getValue(), 0d) : 0.0d;
        Double insuranceFee = (FIN_PROD_SRV.getInsuranceFee(leaseAmountValue) * term) / 12;
        return insuranceFee;
    }

    /**
     * Value Change
     */
    void valueServiceChange() {
        Double leaseAmountValue = 0d;

        if(services.values() != null){
            for (ContentValue contentValue : services.values()){
                if(txtTiServiceAmount.getData() != null){
                    if (contentValue.finService.getServiceType().equals(EServiceType.INSFEE)) {

                        leaseAmountValue = MyNumberUtils.getDouble(txtLeaseAmount.getValue(), 0d);
                        Double totalInsuranceFee = FIN_PROD_SRV.getInsuranceFee(leaseAmountValue);
                        contentValue.textField.setValue(MyNumberUtils.formatDoubleToString(totalInsuranceFee));
                    } else if (contentValue.finService.getServiceType().equals(EServiceType.SRVFEE)) {

                        contentValue.textField.setValue(MyNumberUtils.formatDoubleToString(contentValue.finService.getTePrice()));
                    } else if (contentValue.finService.getServiceType().equals(EServiceType.TRANSFEE)) {

                        contentValue.textField.setValue(MyNumberUtils.formatDoubleToString(contentValue.finService.getTePrice()));
                    }else if (contentValue.finService.getDescEn().equals(VAT_INSUR)) {

                        leaseAmountValue = MyNumberUtils.getDouble(txtLeaseAmount.getValue(), 0d);
                        Double totalVatIns = FIN_PROD_SRV.getVatInsurance(leaseAmountValue);
                        contentValue.textField.setValue(MyNumberUtils.formatDoubleToString(totalVatIns));
                    } else if (contentValue.finService.getDescEn().equals(VAT_SERVI)) {

                        Double totalVatServ = FIN_PROD_SRV.getVatService(VAT_SERVI);
                        contentValue.textField.setValue(MyNumberUtils.formatDoubleToString(totalVatServ));
                    } else if (contentValue.finService.getDescEn().equals(VAT_SERVI_EXT)) {

                        Double totalVatServ = FIN_PROD_SRV.getVatService(VAT_SERVI_EXT);
                        contentValue.textField.setValue(MyNumberUtils.formatDoubleToString(totalVatServ));
                    }
                }
            }
        }
    }
}

