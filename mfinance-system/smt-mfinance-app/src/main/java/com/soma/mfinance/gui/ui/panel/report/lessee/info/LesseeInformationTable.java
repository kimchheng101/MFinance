package com.soma.mfinance.gui.ui.panel.report.lessee.info;

import com.soma.mfinance.core.accounting.InstallmentVO;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.Employment;
import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.entityfield.InstallmentEntityField;
import com.soma.mfinance.core.financial.model.EServiceType;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.model.QuotationService;
import com.soma.mfinance.core.shared.financialproduct.FinancialProductEntityField;
import com.soma.ersys.core.hr.model.address.Address;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.amount.Amount;
import org.seuksa.frmk.tools.amount.AmountUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author p.ing
 */
public class LesseeInformationTable implements InstallmentEntityField, FinServicesHelper {
    private List<ColumnDefinition> columnDefinitions;
    private SimplePagedTable<Contract> pagedTable;

    public LesseeInformationTable() {
    }

    public List<ColumnDefinition> getHeader() {

        columnDefinitions = new ArrayList<ColumnDefinition>();
        columnDefinitions.add(new ColumnDefinition("lease.name", I18N.message("lease.name"), String.class, Align.LEFT, 120));
        columnDefinitions.add(new ColumnDefinition("lid", I18N.message("lid"), String.class, Align.LEFT, 140));
        columnDefinitions.add(new ColumnDefinition("dealer", I18N.message("dealer"), String.class, Align.LEFT, 140));
        columnDefinitions.add(new ColumnDefinition("financial.product", I18N.message("financial.product"), String.class, Align.LEFT, 140));
        columnDefinitions.add(new ColumnDefinition("business.industry", I18N.message("business.industry"), String.class, Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition(VILLAGE, I18N.message("village"), String.class, Align.LEFT, 120));
        columnDefinitions.add(new ColumnDefinition(COMMUNE, I18N.message("commune"), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition(DISTRICT, I18N.message("district"), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition(PROVINCE, I18N.message("province"), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("sex", I18N.message("sex"), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("asset.range", I18N.message("asset.range"), String.class, Align.LEFT, 140)); // model
        columnDefinitions.add(new ColumnDefinition("year.model", I18N.message("year.model"), String.class, Align.LEFT, 140));
        columnDefinitions.add(new ColumnDefinition("engine.number", I18N.message("engine.number"), String.class, Align.LEFT, 120));
        columnDefinitions.add(new ColumnDefinition("chassis.number", I18N.message("chassis.number"), String.class, Align.LEFT, 120));
        columnDefinitions.add(new ColumnDefinition("apply.date", I18N.message("apply.date"), Date.class, Align.LEFT, 110));
        columnDefinitions.add(new ColumnDefinition("start.date", I18N.message("start.date"), Date.class, Align.LEFT, 110));
        columnDefinitions.add(new ColumnDefinition("term", I18N.message("term"), Integer.class, Align.LEFT, 60));
        columnDefinitions.add(new ColumnDefinition("first.payment.date", I18N.message("first.payment.date"), Date.class, Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("interest.rate", I18N.message("interest.rate"), Amount.class, Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("effective.rate", I18N.message("effective.rate"), Amount.class, Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("interest", I18N.message("interest"), Amount.class, Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("lease.amount", I18N.message("lease.amount"), Amount.class, Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("asset.value", I18N.message("asset.value"), Amount.class, Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("lease.value.ratio", I18N.message("lease.value.ratio"), String.class, Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("insurance.fee", I18N.message("insurance.fee"), Amount.class, Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("servicing.fee", I18N.message("servicing.fee"), Amount.class, Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("transfer.ownership", I18N.message("transfer.ownership"), Amount.class, Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("vat.on.principle", I18N.message("vat.on.principle"), Double.class, Align.LEFT, 120));

        return columnDefinitions;
    }

    @SuppressWarnings("unchecked")
    public void getData(List<Contract> leasesInfo) {

        Indexed indexedContainer = pagedTable.getContainerDataSource();
        indexedContainer.removeAllItems();

        final int[] index = {0};
        if (leasesInfo != null && !leasesInfo.isEmpty()) {
            leasesInfo.stream().forEach(leaseContract -> {

                Applicant applicant = leaseContract.getApplicant();
                Individual individual = applicant.getIndividual();
                Address appAddress = individual.getMainAddress();
                Employment employment = individual.getCurrentEmployment();

                String assetRangeDesc = "";
                if (leaseContract.getAsset() != null) {
                    assetRangeDesc = leaseContract.getAsset().getAssetRange().getDescEn();
                }
                Item item = indexedContainer.addItem(index[0]);

                item.getItemProperty("lease.name").setValue(applicant.getLastNameEn() + " " + applicant.getFirstNameEn());
                item.getItemProperty("lid").setValue(leaseContract.getReference());
                item.getItemProperty("dealer").setValue(leaseContract.getDealer().toString());
                item.getItemProperty("financial.product").setValue(leaseContract.getFinancialProduct().getDesc());
                item.getItemProperty("business.industry").setValue(employment.getEmploymentIndustry().getDescEn());
                item.getItemProperty(VILLAGE).setValue(appAddress.getVillage().getDesc());
                item.getItemProperty(COMMUNE).setValue(appAddress.getCommune().getDesc());
                item.getItemProperty(DISTRICT).setValue(appAddress.getDistrict().getDesc());
                item.getItemProperty(PROVINCE).setValue(appAddress.getProvince().getDesc());
                item.getItemProperty("sex").setValue(applicant.getIndividual().getGender().getDesc());
                item.getItemProperty("asset.range").setValue(assetRangeDesc);
                item.getItemProperty("year.model").setValue(leaseContract.getAsset().getAssetYear().getCode());
                item.getItemProperty("engine.number").setValue(leaseContract.getAsset().getEngineNumber());
                item.getItemProperty("chassis.number").setValue(leaseContract.getAsset().getChassisNumber());
                item.getItemProperty("apply.date").setValue(leaseContract.getAsset().getRegistrationDate());
                item.getItemProperty("start.date").setValue(leaseContract.getQuotation().getContractStartDate());
                item.getItemProperty("term").setValue(leaseContract.getQuotation().getTerm());
                item.getItemProperty("first.payment.date").setValue(leaseContract.getQuotation().getFirstDueDate());
                item.getItemProperty("interest.rate").setValue(AmountUtils.convertToAmount(leaseContract.getQuotation().getInterestRate()));
                item.getItemProperty("effective.rate").setValue(AmountUtils.convertToAmount(leaseContract.getIrrRate()));

                //Interest=Interest Rate x Term x Lease Amount
                double interestRate = leaseContract.getInterestRate() / 100;
                double interest = interestRate * leaseContract.getQuotation().getTerm() * leaseContract.getQuotation().getTiFinanceAmount();
                item.getItemProperty("interest").setValue(AmountUtils.convertToAmount(interest));

                item.getItemProperty("lease.amount").setValue(AmountUtils.convertToAmount(leaseContract.getQuotation().getTiFinanceAmount()));
                item.getItemProperty("asset.value").setValue(AmountUtils.convertToAmount(leaseContract.getAsset().getTeAssetApprPrice()));
                item.getItemProperty("lease.value.ratio").setValue(MyNumberUtils.formatDoubleToString(leaseContract.getQuotation().getLeaseAmountPercentage())  +" %");

                // Insurance fee
                Double leaseAmountValue = leaseContract.getQuotation().getTiFinanceAmount();
                Double InsuranceFee = (FIN_PROD_SRV.getInsuranceFee(leaseAmountValue) * leaseContract.getTerm()) / 12;
                Double VatInFee = (FIN_PROD_SRV.getVatInsurance(leaseAmountValue) * leaseContract.getTerm()) / 12;

                item.getItemProperty("insurance.fee").setValue(AmountUtils.convertToAmount(InsuranceFee + VatInFee));

                double servicingFee = 0d; // Check service for pub or exist service
                if (leaseContract.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_PUBLIC)) {
                    servicingFee = leaseContract.getQuotation().getQuotationServiceByServiceType(EServiceType.SRVFEE).getTePrice() +
                            leaseContract.getQuotation().getQuotationServiceByServiceType(EServiceType.SRVFEE).getVatPrice();
                } else {
                    servicingFee = leaseContract.getQuotation().getQuotationServiceByServiceType(EServiceType.SRVFEEEXT).getTiPrice() +
                            leaseContract.getQuotation().getQuotationServiceByServiceType(EServiceType.SRVFEEEXT).getVatPrice();
                }
                item.getItemProperty("servicing.fee").setValue(AmountUtils.convertToAmount(servicingFee));

                QuotationService ownershipFee = leaseContract.getQuotation().getQuotationServiceByServiceType(EServiceType.TRANSFEE);
                item.getItemProperty("transfer.ownership").setValue(AmountUtils.convertToAmount(ownershipFee == null ? 0.0 : ownershipFee.getTiPrice()));

                item.getItemProperty("vat.on.principle").setValue(MyNumberUtils.getDouble((leaseContract.getQuotation().getTiFinanceAmount() * leaseContract.getQuotation().getVatValue()) / 100));
                index[0] += 1;
            });
        }

        pagedTable.refreshContainerDataSource();

    }

    /**
     * @return the pagedTable
     */
    public SimplePagedTable<Contract> getPagedTable() {
        return pagedTable;
    }

    /**
     * @param pagedTable the pagedTable to set
     */
    public void setPagedTable(SimplePagedTable<Contract> pagedTable) {
        this.pagedTable = pagedTable;
    }

}
