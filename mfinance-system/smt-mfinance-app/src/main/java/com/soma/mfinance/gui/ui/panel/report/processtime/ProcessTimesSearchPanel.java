package com.soma.mfinance.gui.ui.panel.report.processtime;


import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.mfinance.core.widget.SecUserComboBox;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.service.EntityService;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;

import java.util.List;

import static com.soma.common.app.workflow.model.EntityWkf.WKFSTATUS;

/**
 * AUTHOR : kimsuor.seang
 * EMAIL  : chhaichivon1995@gmail.com
 * DATE   : 8/12/2017
 * TIME   : 8:08 AM
 */


public class ProcessTimesSearchPanel extends AbstractSearchPanel<Quotation> implements QuotationEntityField {

	private static final long serialVersionUID = 3572459812301801126L;
	
	private DateField dfApplyDate;
	private DateField dfEndDate;
	private SecUserComboBox cbxUnderWriter;
	private SecUserComboBox cbxUnderWriterSupervisor;
	private SecUserComboBox cbxManagement;
	private DealerComboBox cbxDealer;
	private ERefDataComboBox<EDealerType> cbxDealerType;
	
	private ValueChangeListener valueChangeListener;

	
	public ProcessTimesSearchPanel(AbstractTablePanel<Quotation> tablePanel) {
		super(I18N.message("search"), tablePanel);
	}
	
	@Override
	protected Component createForm() {
		dfApplyDate = ComponentFactory.getAutoDateField(I18N.message("apply.date"), false);
		dfApplyDate.setValue(DateUtils.today());
		dfEndDate = ComponentFactory.getAutoDateField(I18N.message("end.date"), false);
		dfEndDate.setValue(DateUtils.today());
		
		cbxUnderWriter = new SecUserComboBox(I18N.message("underwriter"), getListUserByProfile("UW"));
		cbxUnderWriter.setWidth(150, Unit.PIXELS);
		
		cbxUnderWriterSupervisor = new SecUserComboBox(I18N.message("uw.supervisor"), getListUserByProfile("US"));
		cbxUnderWriterSupervisor.setWidth(150, Unit.PIXELS);
		
		cbxManagement = new SecUserComboBox(I18N.message("management"), getListUserByProfile("MA"));
		cbxManagement.setWidth(150, Unit.PIXELS);
		
		cbxDealerType = new ERefDataComboBox<>(I18N.message("dealer.type"), EDealerType.values());
		cbxDealerType.setImmediate(true);
		cbxDealerType.setWidth("150px");
		valueChangeListener = new ValueChangeListener() {
			private static final long serialVersionUID = 5719856916200703515L;
			@Override
			public void valueChange(ValueChangeEvent event) {
				BaseRestrictions<Dealer> restrictions = new BaseRestrictions<>(Dealer.class);
				if (cbxDealerType.getSelectedEntity() != null) {
					restrictions.addCriterion(Restrictions.eq("dealerType", cbxDealerType.getSelectedEntity()));
				}
				EntityService ent = SpringUtils.getBean(EntityService.class);
				cbxDealer.setDealers(ent.list(restrictions));
				cbxDealer.setSelectedEntity(null);
			}
		};
		cbxDealerType.addValueChangeListener(valueChangeListener);
		
		BaseRestrictions<Dealer> restrictions = new BaseRestrictions<>(Dealer.class);
        cbxDealer = new DealerComboBox(I18N.message("dealer"), ENTITY_SRV.list(restrictions));
        cbxDealer.setImmediate(true);
		cbxDealer.setWidth("220px");
				
		HorizontalLayout searchLayout = new HorizontalLayout();
		searchLayout.setSpacing(true);
		searchLayout.addComponent(new FormLayout(dfApplyDate, cbxUnderWriter));
		searchLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS));
		searchLayout.addComponent(new FormLayout(dfEndDate, cbxUnderWriterSupervisor));
		searchLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS));
		searchLayout.addComponent(new FormLayout(cbxDealerType, cbxManagement));
		searchLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS));
		searchLayout.addComponent(new FormLayout(cbxDealer, ComponentFactory.getVerticalLayout(25)));
		return searchLayout;
	}

	
	@Override
	public BaseRestrictions<Quotation> getRestrictions() {
		BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
		restrictions.addCriterion(Restrictions.isNotNull("firstSubmissionDate"));
		restrictions.addCriterion(Restrictions.not(Restrictions.eq(WKFSTATUS, QuotationWkfStatus.CAN)));
		restrictions.setOrder(Order.desc("firstSubmissionDate"));
		
		if (dfApplyDate.getValue() != null) {
			restrictions.addCriterion(Restrictions.ge("firstSubmissionDate", DateUtils.getDateAtBeginningOfDay(dfApplyDate.getValue())));
		}
		if (dfEndDate.getValue() != null) {
			restrictions.addCriterion(Restrictions.le("firstSubmissionDate", DateUtils.getDateAtEndOfDay(dfEndDate.getValue())));
		}
		if (cbxUnderWriter.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("underwriter.id", cbxUnderWriter.getSelectedEntity().getId()));
		}
		if (cbxUnderWriterSupervisor.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("underwriterSupervisor.id", cbxUnderWriterSupervisor.getSelectedEntity().getId()));
		}
		if (cbxManagement.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("manager.id", cbxManagement.getSelectedEntity().getId()));
		}
		if (cbxDealer.getSelectedEntity() != null) { 
			restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, cbxDealer.getSelectedEntity().getId()));
		}
		restrictions.addAssociation("dealer", "dea", JoinType.INNER_JOIN);
		if (cbxDealerType.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("dea.dealerType", cbxDealerType.getSelectedEntity()));
		}
		return restrictions;
	}

	@Override
	protected void reset() {
		dfApplyDate.setValue(null);
		dfEndDate.setValue(null);
		cbxUnderWriter.setSelectedEntity(null);
		cbxUnderWriterSupervisor.setSelectedEntity(null);
		cbxManagement.setSelectedEntity(null);
		cbxDealer.setSelectedEntity(null);
		cbxDealerType.setSelectedEntity(null);
	}

	private List<SecUser> getListUserByProfile(String profile){
		BaseRestrictions<SecUser> restrictions = new BaseRestrictions<>(SecUser.class);
		restrictions.addAssociation("profiles", "profile", JoinType.INNER_JOIN);
		restrictions.addCriterion("profile.code", profile);
		restrictions.addOrder(Order.asc("desc"));
		List<SecUser> secProfiles = ENTITY_SRV.list(restrictions);
		return secProfiles;
	}

}
