package com.soma.mfinance.gui.ui.panel.collection.supervisor.overdueperiod;

import com.soma.mfinance.core.collection.model.OverduePeriod;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.statusrecord.StatusRecordField;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.model.entity.EStatusRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by s.torn on 6/28/2017.
 */
public class OverduePeriodSearchPanel extends AbstractSearchPanel<OverduePeriod> implements FMEntityField {

    private TextField txtOverdueFrom;
    private TextField txtOverdueTo;
    private StatusRecordField statusRecordField;


    public OverduePeriodSearchPanel(OverduePeriodTablePanel overduePeriodTablePanel) {
        super("Search", overduePeriodTablePanel);
    }

    @Override
    protected void reset() {
        txtOverdueFrom.setValue("");
        txtOverdueTo.setValue("");
        statusRecordField.setActiveValue(true);
        statusRecordField.getInactiveValue(false);
    }

    @Override
    protected Component createForm() {
        final GridLayout gridLayout = new GridLayout(4,1);
        txtOverdueFrom = ComponentFactory.getNumberField("overdue.from", true, 60, 200);
        txtOverdueTo = ComponentFactory.getNumberField("overdue.to", true, 60, 200);
        statusRecordField = new StatusRecordField();
        gridLayout.setSpacing(true);
        gridLayout.addComponent(new FormLayout(txtOverdueFrom), 0, 0);
        gridLayout.addComponent(new FormLayout(txtOverdueTo), 1, 0);
        gridLayout.addComponent(new FormLayout(statusRecordField), 2, 0);
        return gridLayout;
    }

    @Override
    public BaseRestrictions<OverduePeriod> getRestrictions() {
        BaseRestrictions<OverduePeriod> restrictions = new BaseRestrictions<>(OverduePeriod.class);

        List<Criterion> criterions = new ArrayList<Criterion>();

        if (StringUtils.isNotEmpty(txtOverdueFrom.getValue())) {
            criterions.add(Restrictions.like("overdueFrom", txtOverdueFrom.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtOverdueTo.getValue())) {
            criterions.add(Restrictions.like("overdueTo", txtOverdueTo.getValue(), MatchMode.ANYWHERE));
        }

        if(!this.statusRecordField.getActiveValue().booleanValue() && !this.statusRecordField.getInactiveValue().booleanValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
            restrictions.getStatusRecordList().add(EStatusRecord.INACT);
        }
        if (this.statusRecordField.getActiveValue().booleanValue() && this.statusRecordField.getInactiveValue().booleanValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.INACT);
            restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
        }
        if(!this.statusRecordField.getActiveValue().booleanValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.INACT);
        }

        if(!this.statusRecordField.getInactiveValue().booleanValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
        }

        restrictions.setCriterions(criterions);
        return restrictions;
    }
}
