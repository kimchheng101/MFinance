package com.soma.mfinance.gui.ui.panel.insurance;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.panel.ApplicantDetailPanel;
import com.soma.mfinance.core.applicant.panel.address.AddressesPanel;
import com.soma.mfinance.core.applicant.panel.guarantor.GuarantorPanel;
import com.soma.mfinance.core.applicant.service.ApplicantService;
import com.soma.mfinance.core.application.panel.ApplicationPanel;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.asset.panel.AssetTabPanel;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.dealer.panel.DealerPanel;
import com.soma.mfinance.core.document.panel.DocumentsPanel;
import com.soma.mfinance.core.financial.panel.product.FinProductPanel;
import com.soma.mfinance.core.quotation.QuotationService;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.panel.ErrorsControlPanel;
import com.soma.mfinance.core.quotation.panel.HistoryStatusPanel;
import com.soma.mfinance.core.quotation.panel.comment.CommentsPanel;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.mfinance.core.shared.quotation.QuotationProfileUtils;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.mfinance.gui.ui.panel.contracts.ChangeStatusPopupPanel;
import com.soma.mfinance.tools.report.service.ReportService;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox.ButtonType;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.SelectedTabChangeListener;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.amount.Amount;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;

import static com.soma.mfinance.core.helper.FinServicesHelper.CONT_SRV;
import static com.soma.mfinance.core.helper.FinServicesHelper.QUO_SRV;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class InsuranceFormPanel extends AbstractFormPanel implements QuotationEntityField {

    private static final long serialVersionUID = 9156433049232084508L;

    private static final ThemeResource errorIcon = new ThemeResource("../smt-default/icons/16/close.png");

    @Autowired
    private ReportService reportService;

    @Autowired
    protected ApplicantService applicantService;

    private ApplicationPanel applicationPanel;

    private InsurancePanel insurancePanel;

    private com.vaadin.ui.Component currentSelectedTab;

    private Quotation quotation;
    private TabSheet accordionPanel;
    private DealerPanel dealerPanel;
    private ApplicantDetailPanel applicantDetailPanel;
    private AssetTabPanel assetTabPanel;

    private FinProductPanel financialProductPanel;
    private GuarantorPanel guarantorPanel;
    private DocumentsPanel documentsPanel;
    private AddressesPanel addressesPanel;
    private InsuranceStatusHistoryPanel insuranceStatusHistoryPanel;
    private ErrorsControlPanel errorsControlPanel;
    private CommentsPanel commentsPanel;
    private HistoryStatusPanel historyStatusPanel;
    private VerticalLayout navigationPanel;
    private NavigationPanel defaultNavigationPanel;

    private Panel contractPanel;
    private Window.CloseListener changeStatusPopupCloseListener;

    public Button btnSubmit;
    public Button btnCancel;
    public Button btnLockOrUnlock;
    public MenuBar reportMenu;
    private Button btnTheft;
    private Button btnAccident;
    private Contract contract;

    @Autowired
    private QuotationService quotationService;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        setCaption(I18N.message("insurance"));
        setSizeFull();

        navigationPanel = new VerticalLayout();
        addComponent(navigationPanel, 0);
        defaultNavigationPanel = new NavigationPanel();
        navigationPanel.addComponent(defaultNavigationPanel);

        //TODO: Reports
        /*
        reportMenu = new MenuBar();

 		final MenuItem reports = reportMenu.addItem(I18N.message("reports"), null);
 		reports.setIcon(new ThemeResource("../smt-default/icons/16/report.png"));

 		final MenuItem applicantItem = reports.addItem(I18N.message("applicant"), null);
 		final MenuItem guarantorItem = reports.addItem(I18N.message("guarantor"), null);


 		SettingConfig settingConfigSolar = DataReference.getInstance().getSettingConfigByCode("solar-tmp-path");
		SettingConfig settingConfigLao = DataReference.getInstance().getSettingConfigByCode("lao-tmp-path");
		if (settingConfigSolar != null && settingConfigSolar.getStatusRecord() == EStatusRecord.ACTIV) {
			applicantItem.addItem(I18N.message("payment.schedule"), new ThemeResource("../smt-default/icons/16/csv.png"), new ReportCommand(GLFPaymentScheduleKHSolar.class));
		} else if (settingConfigLao != null && settingConfigLao.getStatusRecord() == EStatusRecord.ACTIV) {
			applicantItem.addItem(I18N.message("payment.schedule"), new ThemeResource("../smt-default/icons/16/csv.png"), new ReportCommand(GLFPaymentScheduleKHLao.class));
		} else {
			applicantItem.addItem(I18N.message("payment.schedule"), new ThemeResource("../smt-default/icons/16/csv.png"), new ReportCommand(GLFPaymentScheduleKH.class));
		}

 		applicantItem.addItem(I18N.message("contract"), new ThemeResource("../smt-default/icons/16/rtf.png"), new ReportCommand(GLFLeasingContractKH.class));
 		//applicantItem.addItem(I18N.message("annex"), new ThemeResource("../smt-default/icons/16/rtf.png"), new ReportCommand(GLFLeasingContractLegalKH.class));
 		applicantItem.addItem(I18N.message("annex"), new ThemeResource("../smt-default/icons/16/rtf.png"), new ReportCommand(GLFLeasingContractLegalKH.class));

 		guarantorItem.addItem(I18N.message("guarantor"), new ThemeResource("../smt-default/icons/16/rtf.png"), new ReportCommand(GLFGuaranteeContractKH.class));
 		guarantorItem.addItem(I18N.message("annex"), new ThemeResource("../smt-default/icons/16/rtf.png"), new ReportCommand(GLFGuaranteeContractLegalKH.class));
 		// certify letter
 		final MenuItem certifyLetterItem = reports.addItem(I18N.message("certify.letter"), null);
 		certifyLetterItem.addItem(I18N.message("certify.letter"), new ThemeResource("../smt-default/icons/16/rtf.png"), new ReportCommand(GLFCertifyLetterKH.class));

 		//Bill of sale
 		final MenuItem billOfSalesItem = reports.addItem(I18N.message("bill.of.sales"), null);
 		billOfSalesItem.addItem(I18N.message("bill.of.sale"), new ThemeResource("../smt-default/icons/16/rtf.png"), new ReportCommand(GLFBillOfSaleKH.class));

 		defaultNavigationPanel.addButton(reportMenu);
 		if(ProfileUtil.isUnderwriter() || ProfileUtil.isUnderwriterSupervisor()){
 			defaultNavigationPanel.addButton(btnLockOrUnlock);
 		}*/

 		/*
          * @author tha.bunsath
 		 * add button theft
 		 */

        reportMenu = new MenuBar();
        reportMenu.addStyleName("menu-button");

        btnTheft = new NativeButton(I18N.message("theft"));
        btnTheft.setIcon(new ThemeResource("../smt-default/icons/16/tick.png"));
        defaultNavigationPanel.addButton(btnTheft);
        btnTheft.setVisible(false);
        btnTheft.addClickListener(new ClickListener() {
            private static final long serialVersionUID = 3637046509666055529L;

            @Override
            public void buttonClick(ClickEvent event) {
                contract = quotation.getContract();
                ChangeStatusPopupPanel changeStatusPopupPanel = new ChangeStatusPopupPanel(ContractWkfStatus.THE, contract);
                changeStatusPopupPanel.addCloseListener(changeStatusPopupCloseListener);
                UI.getCurrent().addWindow(changeStatusPopupPanel);
            }
        });

 		/*
          * @author tha.bunsath
 		 * add button accident
 		 */
        btnAccident = new NativeButton(I18N.message("accident"));
        btnAccident.setIcon(new ThemeResource("../smt-default/icons/16/tick.png"));
        defaultNavigationPanel.addButton(btnAccident);
        defaultNavigationPanel.addButton(reportMenu);
        btnAccident.setVisible(false);
        btnAccident.addClickListener(new ClickListener() {
            private static final long serialVersionUID = 3637046509666055529L;

            @Override
            public void buttonClick(ClickEvent event) {
                contract = quotation.getContract();
                ChangeStatusPopupPanel changeStatusPopupPanel = new ChangeStatusPopupPanel(ContractWkfStatus.ACC, contract);
                changeStatusPopupPanel.addCloseListener(changeStatusPopupCloseListener);
                UI.getCurrent().addWindow(changeStatusPopupPanel);
            }
        });

        changeStatusPopupCloseListener = new Window.CloseListener() {
            @Override
            public void windowClose(Window.CloseEvent e) {
                if(((ChangeStatusPopupPanel)e.getComponent()).isValidate()){
                    insurancePanel.displayApplicationTablePanel();
                }
            }
        };

    }

    /**
     * Set registrations main panel
     *
     * @param insurancePanel
     */
    public void setInsurancePanel(InsurancePanel insurancePanel) {
        this.insurancePanel = insurancePanel;
    }

    /**
     * @return
     */
    public ApplicationPanel getApplicationPanel() {
        return this.applicationPanel;
    }

    /**
     * @see AbstractFormPanel#getEntity()
     */
    @Override
    protected Quotation getEntity() {
        /*updateQuotationTab(accordionPanel.getSelectedTab());
		if (accordionPanel.getTab(guarantorPanel) == null) {
			QuotationApplicant guarantorApplicant = quotation.getQuotationApplicant(EApplicantType.G);
			if (guarantorApplicant != null) {
				guarantorApplicant.setCrudAction(CrudAction.DELETE);
			}
		}*/
        return quotation;
    }

    /**
     * @see AbstractFormPanel#createForm()
     */
    @Override
    protected com.vaadin.ui.Component createForm() {

        contractPanel = new Panel();
        insuranceStatusHistoryPanel = new InsuranceStatusHistoryPanel();
        accordionPanel = new TabSheet();
        dealerPanel = new DealerPanel();
        applicantDetailPanel = new ApplicantDetailPanel();
        assetTabPanel = new AssetTabPanel();
        financialProductPanel = new FinProductPanel();
        guarantorPanel = new GuarantorPanel();
        documentsPanel = new DocumentsPanel();
        errorsControlPanel = new ErrorsControlPanel();
        commentsPanel = new CommentsPanel();
        historyStatusPanel = new HistoryStatusPanel();

        addressesPanel = new AddressesPanel();

        accordionPanel.addTab(insuranceStatusHistoryPanel, I18N.message("insurance.status"));
        accordionPanel.addTab(dealerPanel, I18N.message("dealer"));
        accordionPanel.addTab(assetTabPanel, I18N.message("asset"));
        accordionPanel.addTab(financialProductPanel, I18N.message("financial.product"));
        accordionPanel.addTab(applicantDetailPanel, I18N.message("applicant"));
        accordionPanel.addTab(guarantorPanel, I18N.message("guarantor"));
        accordionPanel.addTab(documentsPanel, I18N.message("documents"));
        accordionPanel.addTab(addressesPanel, I18N.message("address"));

        accordionPanel.addSelectedTabChangeListener(new SelectedTabChangeListener() {
            private static final long serialVersionUID = -2435529941310008060L;

            @Override
            public void selectedTabChange(SelectedTabChangeEvent event) {

                quotation = ENTITY_SRV.getById(Quotation.class, quotation.getId());

                AbstractTabPanel selectedTab = (AbstractTabPanel) event.getTabSheet().getSelectedTab();
                //updateQuotationTab(currentSelectedTab);

                currentSelectedTab = selectedTab;
                if (selectedTab == dealerPanel) {
                    dealerPanel.assignValues(quotation);
                } else if (selectedTab == applicantDetailPanel) {
                    applicantDetailPanel.assignValues(quotation);
                } else if (selectedTab == assetTabPanel) {
                    assetTabPanel.assignValues(quotation);
                } else if (selectedTab == financialProductPanel) {
                    financialProductPanel.assignValues(quotation);
                } else if (selectedTab == documentsPanel) {
                    documentsPanel.assignValues(quotation);
                } else if (selectedTab == guarantorPanel) {
                    //guarantorPanel.setMainApplicant(quotation.getMainApplicant());
                    guarantorPanel.assignValues(quotation);
                } else if (selectedTab == commentsPanel) {
                    commentsPanel.assignValues(quotation);
                } else if (selectedTab == historyStatusPanel) {
                    historyStatusPanel.assignValues(quotation);
                } else if (selectedTab == addressesPanel) {
                } else if (selectedTab == insuranceStatusHistoryPanel) {
                    insuranceStatusHistoryPanel.assignValues(quotation);
                }
                if (selectedTab != errorsControlPanel) {
                    accordionPanel.removeComponent(errorsControlPanel);
                }

                selectedTab.updateNavigationPanel(navigationPanel, defaultNavigationPanel);

                navigationPanel.removeComponent(contractPanel);
                displayInfo();

            }
        });

        return accordionPanel;
    }

    /**
     * @param quotaId
     * @param firstTabSelect
     */
    public void assignValues(Long quotaId, boolean firstTabSelect) {
        if (quotaId != null) {
            Quotation quotation = ENTITY_SRV.getById(Quotation.class, quotaId);
            assignValues(quotation, firstTabSelect);
        }
    }

    /**
     * @param quotation
     * @param firstTabSelect
     */
    public void assignValues(Quotation quotation, boolean firstTabSelect) {
        this.reset();
        this.quotation = quotation;

        dealerPanel.assignValues(quotation);


        if (quotation.getAsset() == null) {
            quotation.setAsset(new Asset());
        }

        assetTabPanel.assignValues(quotation);

        financialProductPanel.assignValues(quotation);

        applicantDetailPanel.assignValues(quotation);
        if (QUO_SRV.isGuarantorRequired(quotation)) {
            addGuarantorPanel();
            guarantorPanel.assignValues(quotation);
        } else {
            if (quotation.getGuarantor() == null) {
                removeGuarantorPanel();
            }
        }


        documentsPanel.assignValues(quotation);
        insuranceStatusHistoryPanel.assignValues(quotation);

        if (quotation.getId() != null) {
            if (accordionPanel.getTab(commentsPanel) == null) {
                accordionPanel.addTab(commentsPanel, I18N.message("comments"));
            }
        }

        if (QuotationProfileUtils.isHistoryStatusTabAvailable(quotation)) {
            if (accordionPanel.getTab(historyStatusPanel) == null) {
                accordionPanel.addTab(historyStatusPanel, I18N.message("history.status"));
            }
            historyStatusPanel.assignValues(quotation);
        }

        if (firstTabSelect) {
            accordionPanel.setSelectedTab(insuranceStatusHistoryPanel);
        }

        // Display Top Panel
        navigationPanel.removeComponent(contractPanel);
        displayInfo();

        assignMenus();

        if (addressesPanel != null) {
            accordionPanel.removeComponent(addressesPanel);
        }
		/*
		//TODO: Display address Panel on Overdue Contract.

		if(quotation.getWkfStatus().equals(QuotationWkfStatus.ACT)) {
			Contract contract = ENTITY_SRV.getById(Contract.class, quotation.getContract());
			addressesPanel = new AddressesPanel(); 
			addressesPanel.setSpacing(true);
			addressesPanel.assignValues(contract.getApplicant().getApplicantAddresses());
			addressesPanel.assignValues(quotation.get);
			accordionPanel.addTab(addressesPanel, I18N.message("addresses"));
		}*/
    }


    /**
     * Assign menus
     */
    public void assignMenus() {
        //TODO : Report
        //reportMenu.setVisible(QuotationProfileUtils.isReportsAvailable(quotation));

        if (quotation.getContract() != null) {
            contract = quotation.getContract();

            btnTheft.setVisible(ProfileUtil.isDocumentController() && quotation.getWkfStatus().equals(QuotationWkfStatus.ACT)
                    && !contract.getWkfStatus().equals(ContractWkfStatus.THE) && !contract.getWkfStatus().equals(ContractWkfStatus.ACC));

            btnAccident.setVisible(ProfileUtil.isDocumentController() && quotation.getWkfStatus().equals(QuotationWkfStatus.ACT)
                    && !contract.getWkfStatus().equals(ContractWkfStatus.THE) && !contract.getWkfStatus().equals(ContractWkfStatus.ACC));
        }

    }

    /**
     * @see AbstractFormPanel#reset()
     */
    @Override
    public void reset() {
        super.reset();
        quotation = new Quotation();
        dealerPanel.reset();
        applicantDetailPanel.reset();
        assetTabPanel.reset();
        financialProductPanel.reset();
        documentsPanel.reset();
    }

    /**
     * @see AbstractFormPanel#validate()
     */
    @Override
    protected boolean validate() {
        boolean valid = true;
        // Store value in quotation
        getEntity();
        if (!dealerPanel.isValid()) {
            valid = false;
            accordionPanel.setSelectedTab(dealerPanel);
        } else if (!financialProductPanel.isValid().isEmpty() && !assetTabPanel.isValid().isEmpty()) {
            if (!assetTabPanel.isValid().isEmpty()) {
                valid = false;
                accordionPanel.setSelectedTab(assetTabPanel);

            }
        } else if (!applicantDetailPanel.isValid().isEmpty()) {
            valid = false;
            accordionPanel.setSelectedTab(applicantDetailPanel);
        }

        if (ProfileUtil.isDocumentController()) {
            if (!assetTabPanel.isValid().isEmpty()) {
                valid = false;
                accordionPanel.setSelectedTab(assetTabPanel);
            }
        }
        if (!documentsPanel.isValid()) {
            valid = false;
            accordionPanel.setSelectedTab(documentsPanel);
        }

        return valid;
    }

    public void setLockOrUnlockProcess(Quotation quotation) {
        if (quotation.getSecUser() == null) {
            btnLockOrUnlock.setCaption(I18N.message("lock"));
            btnLockOrUnlock.setIcon(new ThemeResource("icons/16/locked.png"));
        } else {
            btnLockOrUnlock.setCaption(I18N.message("unlock"));
            btnLockOrUnlock.setIcon(new ThemeResource("icons/16/unlocked.png"));
        }
    }

    /**
     * @param quotation
     */

    public void showProcessMessage(Quotation quotation) {
        String underWriterName;
        if (ProfileUtil.isUnderwriter()) {
            underWriterName = quotation.getSecUser().getDesc() + " " + "UW";
        } else underWriterName = quotation.getSecUser().getDesc() + " " + "US";
        //set message box if other underwriter select quotation on the same time
        MessageBox mb = new MessageBox(UI.getCurrent(), "320px", "140px", I18N.message("application"),
                MessageBox.Icon.ERROR, underWriterName + " " + "is processing", Alignment.MIDDLE_RIGHT,
                new MessageBox.ButtonConfig(ButtonType.OK, I18N.message("ok")));
        mb.show();
    }

    //TODO: Reports
	/*private class ReportCommand implements Command {
		private static final long serialVersionUID = -1436430428258990868L;
		private Class<? extends Report> reportClass;
		public ReportCommand(Class<? extends Report> reportClass) {
			this.reportClass = reportClass;
		}
		@Override
		public void menuSelected(MenuItem selectedItem) {
			try {
				ReportParameter reportParameter = new ReportParameter();
				reportParameter.addParameter("quotaId", quotation.getId());		
				reportParameter.addParameter("stamp", quotation.isStamp());
				
				SettingConfig settingConfigSolar = DataReference.getInstance().getSettingConfigByCode("solar-tmp-path");
				SettingConfig settingConfigLao = DataReference.getInstance().getSettingConfigByCode("lao-tmp-path");
				if (settingConfigSolar != null && settingConfigSolar.getStatusRecord() == StatusRecord.ACTIV) {
					if (reportClass == GLFPaymentScheduleKHSolar.class || reportClass == GLFPaymentScheduleKH2Solar.class) {
						if (quotation != null) {
							if (quotation.getTemplateType() == TemplateType.TEMPLATE_2) {
								reportClass = GLFPaymentScheduleKH2Solar.class;	
							} else {
								reportClass = GLFPaymentScheduleKHSolar.class;	
							}
						}
					}	
				} else if (settingConfigLao != null && settingConfigLao.getStatusRecord() == StatusRecord.ACTIV) {
					if (reportClass == GLFPaymentScheduleKHLao.class || reportClass == GLFPaymentScheduleKH2Lao.class) {
						if (quotation != null) {
							if (quotation.getTemplateType() == TemplateType.TEMPLATE_2) {
								reportClass = GLFPaymentScheduleKH2Lao.class;	
							} else {
								reportClass = GLFPaymentScheduleKHLao.class;	
							}
						}
					}
				} else {
					if (reportClass == GLFPaymentScheduleKH.class || reportClass == GLFPaymentScheduleKH2.class) {
						if (quotation != null) {
							if (quotation.getTemplateType() == TemplateType.TEMPLATE_2) {
								reportClass = GLFPaymentScheduleKH2.class;	
							} else {
								reportClass = GLFPaymentScheduleKH.class;	
							}
						}
					}
				}		

				String fileName = reportService.extract(reportClass, reportParameter);
				String fileDir = "";
				if (reportClass == GLFLeasingContractLegalKH.class
						|| reportClass == GLFGuaranteeContractLegalKH.class) {
					//fileDir = AppConfig.getInstance().getConfiguration().getString("specific.templatedir");
					fileDir = AppConfig.getInstance().getConfiguration().getString("specific.tmpdir");
				} else {
					fileDir = AppConfig.getInstance().getConfiguration().getString("specific.tmpdir");
				}
				DocumentViewver documentViewver = new DocumentViewver(I18N.message(""), fileDir + "/" + fileName); 
				UI.getCurrent().addWindow(documentViewver);
			} catch (Exception e) {
				logger.error("", e);
			}
		}
	}*/

    /**
     * Display quotation detail
     */
    public void displayInfo() {
        if (quotation.getWkfStatus().equals(QuotationWkfStatus.ACT)) {
            displayContractInfo(quotation.getContract());
        } else {
            displayQuotationInfo(quotation.getMainApplicant());
        }
    }

    private void displayContractInfo(final Contract contract) {

        contractPanel = new Panel();
        navigationPanel.setSpacing(true);
        navigationPanel.addComponent(contractPanel);

        String template = "contractInfo";
        InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
        CustomLayout inputFieldLayout = null;
        try {
            inputFieldLayout = new CustomLayout(layoutFile);
        } catch (IOException e) {
            Notification.show(I18N.message("message.error.locate.template") + template, e.getMessage(), Type.ERROR_MESSAGE);
        }

        inputFieldLayout.setSizeFull();
        inputFieldLayout.addComponent(new Label(I18N.message("applicant")), "lblApplicant");
        //inputFieldLayout.addComponent(btnApplicant, "lnkApplicant");
        inputFieldLayout.addComponent(new Label(contract.getApplicant().getLastNameEn() + " " + contract.getApplicant().getFirstNameEn()), "lnkApplicant");
        inputFieldLayout.addComponent(new Label(I18N.message("reference")), "lblReference");
        inputFieldLayout.addComponent(new Label("<b>" + contract.getReference() + "</b>", ContentMode.HTML), "lblReferenceValue");
        inputFieldLayout.addComponent(new Label(I18N.message("startdate")), "lblStartDate");
        inputFieldLayout.addComponent(new Label("<b>" + DateUtils.date2StringDDMMYYYY_SLASH(contract.getStartDate()) + "</b>", ContentMode.HTML), "lblStartDateValue");
        inputFieldLayout.addComponent(new Label(I18N.message("enddate")), "lblEndDate");
        inputFieldLayout.addComponent(new Label("<b>" + DateUtils.date2StringDDMMYYYY_SLASH(contract.getEndDate()) + "</b>", ContentMode.HTML), "lblEndDateValue");
        inputFieldLayout.addComponent(new Label(I18N.message("penalty.rule")), "lblPenaltyRule");
        inputFieldLayout.addComponent(new Label(contract.getPenaltyRule() != null ? contract.getPenaltyRule().getDescEn() : ""), "txtPenaltyRule");

        inputFieldLayout.addComponent(new Label(I18N.message("guarantor")), "lblGuarantor");
        if (contract.getGuarantor() != null) {
            inputFieldLayout.addComponent(new Label(contract.getApplicant().getLastNameEn() + " " + contract.getApplicant().getFirstNameEn()), "lnkGuarantor");
        } else {
            inputFieldLayout.addComponent(new Label("N/A"), "lnkGuarantor");
        }
        inputFieldLayout.addComponent(new Label(I18N.message("outstanding")), "lblOutstanding");
        inputFieldLayout.addComponent(new Label(I18N.message("status")), "lblStatus");
        inputFieldLayout.addComponent(new Label(contract.getWkfStatus().getDesc()), "txtStatus");
        inputFieldLayout.addComponent(new Label(I18N.message("quotation.ratio")), "lblRatio");
        double applicantRatio = quotationService.calculateApplicantRatio(quotation);
        inputFieldLayout.addComponent(new Label("<b> <font color=\""+ quotationService.getRatioColor(applicantRatio) + "\">" + AmountUtils.format(applicantRatio) + "</font></b>",ContentMode.HTML),"lblRatioValue");

        Amount outstanding = CONT_SRV.getRealOutstanding(DateUtils.todayH00M00S00(), contract.getId());
        inputFieldLayout.addComponent(new Label(AmountUtils.format(outstanding.getTiAmount())), "txtOutstanding");

        VerticalLayout vertical = new VerticalLayout();
        vertical.addComponent(inputFieldLayout);
        contractPanel.setContent(vertical);
    }

    /**
     * return Quotation info
     * Panha Morn
     */
    private void displayQuotationInfo(final Applicant applicant) {
        if (!quotation.isValid()) return;

        contractPanel = new Panel();
        navigationPanel.setSpacing(true);
        navigationPanel.addComponent(contractPanel);

        String template = "contractInfo";
        InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
        CustomLayout inputFieldLayout = null;
        try {
            inputFieldLayout = new CustomLayout(layoutFile);
        } catch (IOException e) {
            Notification.show(I18N.message("message.error.locate.template") + template, e.getMessage(), Type.ERROR_MESSAGE);
        }

        inputFieldLayout.setSizeFull();
        inputFieldLayout.addComponent(new Label(I18N.message("applicant")), "lblApplicant");
        inputFieldLayout.addComponent(new Label(applicant.getLastNameEn() + " " + applicant.getFirstNameEn()), "lnkApplicant");
        inputFieldLayout.addComponent(new Label(I18N.message("reference")), "lblReference");
        inputFieldLayout.addComponent(new Label("<b>" + "" + "</b>", ContentMode.HTML), "lblReferenceValue");
        inputFieldLayout.addComponent(new Label(I18N.message("startdate")), "lblStartDate");
        inputFieldLayout.addComponent(new Label("<b>" + DateUtils.date2StringDDMMYYYY_SLASH(quotation.getCreateDate()) + "</b>", ContentMode.HTML), "lblStartDateValue");
        inputFieldLayout.addComponent(new Label(I18N.message("enddate")), "lblEndDate");
        inputFieldLayout.addComponent(new Label("<b>" + "" + "</b>", ContentMode.HTML), "lblEndDateValue");
        inputFieldLayout.addComponent(new Label(I18N.message("penalty.rule")), "lblPenaltyRule");
        if (quotation.getContract() == null) {
            inputFieldLayout.addComponent(new Label(""), "txtPenaltyRule");
        } else {
            inputFieldLayout.addComponent(new Label(quotation.getContract().getPenaltyRule() != null ? quotation.getContract().getPenaltyRule().getDescEn() : ""), "txtPenaltyRule");
        }
        inputFieldLayout.addComponent(new Label(I18N.message("guarantor")), "lblGuarantor");
        if (quotation.getGuarantor() != null) {
            inputFieldLayout.addComponent(new Label(quotation.getGuarantor().getLastNameEn() + " " + quotation.getGuarantor().getFirstNameEn()), "lnkGuarantor");
        } else {
            inputFieldLayout.addComponent(new Label("N/A"), "lnkGuarantor");
        }
        inputFieldLayout.addComponent(new Label(I18N.message("outstanding")), "lblOutstanding");
        inputFieldLayout.addComponent(new Label(I18N.message("status")), "lblStatus");
        inputFieldLayout.addComponent(new Label(quotation.getWkfStatus().getDesc()), "txtStatus");
        inputFieldLayout.addComponent(new Label(I18N.message("quotation.radio")), "lblRatio");
        double applicantRatio = quotationService.calculateApplicantRatio(quotation);
        inputFieldLayout.addComponent(new Label("<b> <font color=\""+ quotationService.getRatioColor(applicantRatio) + "\">" + AmountUtils.format(applicantRatio) + "</font></b>",ContentMode.HTML),"lblRatioValue");

        Amount outstanding = CONT_SRV.getRealOutstanding(DateUtils.todayH00M00S00(), applicant.getId());
        inputFieldLayout.addComponent(new Label(AmountUtils.format(outstanding.getTiAmount())), "txtOutstanding");

        VerticalLayout vertical = new VerticalLayout();
        vertical.addComponent(inputFieldLayout);
        contractPanel.setContent(vertical);
    }

    public void addGuarantorPanel() {
        if (accordionPanel.getTab(guarantorPanel) == null) {
            guarantorPanel = new GuarantorPanel();
        }
        accordionPanel.addTab(guarantorPanel, I18N.message("guarantor"), null, 5);
    }

    public void removeGuarantorPanel() {
        accordionPanel.removeComponent(guarantorPanel);
    }
}
