package com.soma.mfinance.gui.ui.panel.collection.supervisor.overdueperiod;

import com.soma.mfinance.core.applicant.service.ValidateNumbers;
import com.soma.mfinance.core.collection.model.OverduePeriod;
import com.soma.mfinance.core.collection.service.CollectionService;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.data.Property;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by s.torn on 6/28/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class OverduePeriodFormPanel extends AbstractFormPanel {

    private OverduePeriod overduePeriod;
    private TextField txtOverdueFrom;
    private TextField txtOverdueTo;
    private CheckBox cbActive;

    @Autowired
    CollectionService collectionService;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addSaveClickListener(this);
    }

    @Override
    protected Entity getEntity() {
        overduePeriod.setOverdueFrom(txtOverdueFrom.getValue());
        overduePeriod.setOverdueTo(txtOverdueTo.getValue());
        if(validateOverdueFromTo(txtOverdueFrom.getValue(),txtOverdueTo.getValue())){
            overduePeriod.setStatusRecord(cbActive.getValue() ? EStatusRecord.ACTIV : EStatusRecord.INACT);
            overduePeriod.setCode(" " );
            return overduePeriod;
        }
      return null;
    }

    @Override
    protected com.vaadin.ui.Component createForm() {
        ValidateNumbers applcInfo = new ValidateNumbers();
        ArrayList validTextInfo = new ArrayList();
        txtOverdueFrom = ComponentFactory.getNumberField("overdue.from", true, 60, 200);
        validTextInfo.add(txtOverdueFrom);
        txtOverdueTo = ComponentFactory.getNumberField("overdue.to", true, 60, 200);
        validTextInfo.add(txtOverdueTo);
        cbActive = new CheckBox(I18N.message("active"));
        cbActive.setValue(true);
        FormLayout formLayout = new FormLayout();
        formLayout.setSpacing(true);
        formLayout.addComponent(txtOverdueFrom);
        formLayout.addComponent(txtOverdueTo);
        formLayout.addComponent(cbActive);
        applcInfo.validateNumber(validTextInfo);
        return formLayout;
    }

    @Override
    public void reset() {
        super.reset();
        overduePeriod = new OverduePeriod();
        txtOverdueFrom.setValue("");
        txtOverdueTo.setValue("");
        cbActive.setValue(true);
    }

    @Override
    public boolean validate() {
        checkMandatoryField(txtOverdueFrom, I18N.message("overdue.from"));
        checkMandatoryField(txtOverdueTo, I18N.message("overdue.to"));
        return errors.isEmpty();
    }

    public void assignValue(Long id) {
        if (id != null) {
            BaseRestrictions<OverduePeriod> restrictions = new BaseRestrictions<>(OverduePeriod.class);
            restrictions.addCriterion(Restrictions.eq("id", id));
            List<OverduePeriod> overduePeriodList = ENTITY_SRV.list(restrictions);

            if (overduePeriodList != null && !overduePeriodList.isEmpty()) {
                overduePeriod = overduePeriodList.get(0);
            }
            overduePeriod = ENTITY_SRV.getById(OverduePeriod.class, id);
        }

        if (overduePeriod != null) {
            txtOverdueFrom.setValue(overduePeriod.getOverdueFrom());
            txtOverdueTo.setValue(overduePeriod.getOverdueTo());
            cbActive.setValue(overduePeriod.getStatusRecord().equals(EStatusRecord.ACTIV));
        } else {
            reset();
            overduePeriod = new OverduePeriod();

        }
    }

    public boolean validateOverdueFromTo(String overdueFrom,String overdueTo){
        if(Integer.parseInt(overdueFrom)>Integer.parseInt(overdueTo)){
            errors.add("Overdue From cannot greater than Overdue To");
            return false;
        }else{
            return true;
        }
    }
}
