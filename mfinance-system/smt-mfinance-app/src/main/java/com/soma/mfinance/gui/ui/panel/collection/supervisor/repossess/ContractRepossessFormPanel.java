package com.soma.mfinance.gui.ui.panel.collection.supervisor.repossess;

import com.soma.mfinance.core.applicant.panel.ApplicantDetailPanel;
import com.soma.mfinance.core.applicant.panel.guarantor.GuarantorPanel;
import com.soma.mfinance.core.application.panel.ApplicationPanel;
import com.soma.mfinance.core.asset.panel.AssetPanelOld;
import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.service.ContractService;
import com.soma.mfinance.core.document.panel.DocumentsPanel;
import com.soma.mfinance.core.financial.panel.product.FinProductPanel;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.panel.comment.CommentsPanel;
import com.soma.mfinance.gui.ui.panel.report.contract.overdue.CollectionHistoryPanel;
import com.soma.mfinance.gui.ui.panel.report.contract.overdue.ContractDocumentsPanel;
import com.soma.mfinance.gui.ui.panel.report.contract.overdue.PaymentSchedulePanel;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.Runo;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.Entity;
import org.seuksa.frmk.tools.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;

/**
 * Contract re possess form panel
 * @author kimsuor.seang
 * @author Modified: sr.soth
 * @date 18-Nov-17
 *
 * Modified: th.seng
 * @date 28-Nov-17
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ContractRepossessFormPanel extends AbstractFormPanel implements TabSheet.SelectedTabChangeListener {

    private static final long serialVersionUID = -5011837298969453941L;

    @Autowired
    private ContractService contractService;
    private TabSheet accordionPanel;
    private Panel infoPanel;

    private CollectionHistoryPanel collectionHistoryPanel;
    private CommentsPanel commentsPanel;
    private PaymentSchedulePanel paymentSchedulePanel;
    //private UnpaidInstallmentsPanel unpaidInstallmentsPanel;
    private ApplicantDetailPanel applicantPanel;
    private GuarantorPanel guarantorPanel;
    private AssetPanelOld assetPanel;
    private FinProductPanel financialProductPanel;
    private DocumentsPanel documentsPanel;
    private ContractDocumentsPanel contractDocumentsPanel;

    private Collection collection;
    private Quotation quotation;

    @PostConstruct
    public void PostConstruct() {
        super.init();
    }

    @Override
    protected Entity getEntity() {
        return null;
    }

    @Override
    protected com.vaadin.ui.Component createForm() {
        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setSpacing(true);

        infoPanel = new Panel();
        accordionPanel = new TabSheet();

        collectionHistoryPanel = new CollectionHistoryPanel();
        commentsPanel = new CommentsPanel();
        paymentSchedulePanel = new PaymentSchedulePanel();
        //unpaidInstallmentsPanel = new UnpaidInstallmentsPanel();
        applicantPanel = new ApplicantDetailPanel();
        guarantorPanel = new GuarantorPanel();
        assetPanel = new AssetPanelOld();
        financialProductPanel = new FinProductPanel();
        documentsPanel = new DocumentsPanel();
        contractDocumentsPanel = new ContractDocumentsPanel();

        accordionPanel.addTab(collectionHistoryPanel, I18N.message("collection.history"));
        accordionPanel.addTab(commentsPanel, I18N.message("comments"));
        accordionPanel.addTab(paymentSchedulePanel, I18N.message("payment.schedule"));
        //accordionPanel.addTab(unpaidInstallmentsPanel, I18N.message("unpaid.installments"));
        accordionPanel.addTab(applicantPanel, I18N.message("applicant"));
        accordionPanel.addTab(guarantorPanel, I18N.message("guarantor"));
        accordionPanel.addTab(assetPanel, I18N.message("asset"));
        accordionPanel.addTab(financialProductPanel, I18N.message("financial.product"));
        accordionPanel.addTab(documentsPanel, I18N.message("documents"));
        accordionPanel.addTab(contractDocumentsPanel, I18N.message("collection.documents"));

        contentLayout.addComponent(infoPanel);
        contentLayout.addComponent(accordionPanel);
        accordionPanel.addSelectedTabChangeListener(this);
        return contentLayout;
    }

    public void assignValues(Long contractId) {
        Contract contract = ENTITY_SRV.getById(Contract.class, contractId);
        collection = contract.getCollection();
        quotation = contract.getQuotation();
        guarantorPanel.setMainApplicant(quotation.getApplicant());
        displayContractInfo(contract);

        accordionPanel.setSelectedTab(collectionHistoryPanel);
        collectionHistoryPanel.assignValues(collection);
    }

    private void displayContractInfo(final Contract contract) {
        Button btnApplicant = new Button(contract.getApplicant().getLastNameEn() + " " + contract.getApplicant().getFirstNameEn());
        btnApplicant.setStyleName(Runo.BUTTON_LINK);
        btnApplicant.addClickListener(new ClickListener() {
            private static final long serialVersionUID = 5451254851200324858L;
            @Override
            public void buttonClick(ClickEvent event) {
               Page.getCurrent().setUriFragment("!" + ApplicationPanel.NAME + "/" + contract.getApplicant().getId());
            }
        });

        String template = "contractInfo";
        InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
        CustomLayout inputFieldLayout = null;
        try {
            inputFieldLayout = new CustomLayout(layoutFile);
        } catch (IOException e) {
            Notification.show(I18N.message("message.error.locate.template") + template, e.getMessage(), Type.ERROR_MESSAGE);
        }

        inputFieldLayout.setSizeFull();
        inputFieldLayout.addComponent(new Label(I18N.message("applicant")), "lblApplicant");
        inputFieldLayout.addComponent(btnApplicant, "lnkApplicant");
        inputFieldLayout.addComponent(new Label(I18N.message("reference")), "lblReference");
        inputFieldLayout.addComponent(new Label("<b>" + contract.getReference() + "</b>", ContentMode.HTML), "lblReferenceValue");
        inputFieldLayout.addComponent(new Label(I18N.message("startdate")), "lblStartDate");
        inputFieldLayout.addComponent(new Label("<b>" + DateUtils.date2StringDDMMYYYY_SLASH(contract.getStartDate()) + "</b>", ContentMode.HTML), "lblStartDateValue");
        inputFieldLayout.addComponent(new Label(I18N.message("enddate")), "lblEndDate");
        inputFieldLayout.addComponent(new Label("<b>" + DateUtils.date2StringDDMMYYYY_SLASH(contract.getEndDate()) + "</b>", ContentMode.HTML), "lblEndDateValue");
        inputFieldLayout.addComponent(new Label(I18N.message("penalty.rule")), "lblPenaltyRule");
        inputFieldLayout.addComponent(new Label(contract.getPenaltyRule() != null ? contract.getPenaltyRule().getDescEn() : ""), "txtPenaltyRule");

        inputFieldLayout.addComponent(new Label(I18N.message("guarantor")), "lblGuarantor");
        if (contract.getGuarantor() != null) {
            Button btnGuarantor = new Button(contract.getGuarantor().getLastNameEn() + " " + contract.getGuarantor().getFirstNameEn());
            btnGuarantor.setStyleName(Runo.BUTTON_LINK);
            btnGuarantor.addClickListener(new ClickListener() {
                private static final long serialVersionUID = -7808726057921125458L;
                @Override
                public void buttonClick(ClickEvent event) {
                    Page.getCurrent().setUriFragment("!" + ApplicationPanel.NAME + "/" + contract.getGuarantor().getId());
                }
            });
            inputFieldLayout.addComponent(btnGuarantor, "lnkGuarantor");
        } else {
            inputFieldLayout.addComponent(new Label("N/A"), "lnkGuarantor");
        }
        inputFieldLayout.addComponent(new Label(I18N.message("outstanding")), "lblOutstanding");
        inputFieldLayout.addComponent(new Label(I18N.message("status")), "lblStatus");
        inputFieldLayout.addComponent(new Label(contract.getWkfStatus().getDescEn()), "txtStatus");

        //Amount outstanding = contractService.getRealOutstanding(DateUtils.todayH00M00S00(), contract.getId());
        inputFieldLayout.addComponent(new Label("0.00"/*AmountUtils.format(outstanding.getTiAmount())*/), "txtOutstanding");

        VerticalLayout vertical = new VerticalLayout();
        vertical.addComponent(inputFieldLayout);
        infoPanel.setContent(vertical);
    }

    @Override
    public void reset() {
        super.reset();
        this.errors.clear();
        //accordionPanel.removeComponent(cashflowsPanel);
    }

    @Override
    protected boolean validate() {
        return errors.isEmpty();
    }

    @Override
    public void selectedTabChange(TabSheet.SelectedTabChangeEvent event) {
        AbstractTabPanel selectedTab = (AbstractTabPanel) event.getTabSheet().getSelectedTab();

        if(isSelectedTab(selectedTab, collectionHistoryPanel)){
            collectionHistoryPanel.assignValues(collection);
        } else if(isSelectedTab(selectedTab, commentsPanel)){
            commentsPanel.assignValues(quotation);
        } else if(isSelectedTab(selectedTab, paymentSchedulePanel)){
            paymentSchedulePanel.assignValues(quotation);
        }/* else if(isSelectedTab(selectedTab, unpaidInstallmentsPanel)){
            unpaidInstallmentsPanel.assignValues(collection.getContract());
        }*/ else if(isSelectedTab(selectedTab, applicantPanel)){
            applicantPanel.assignValues(quotation);
            applicantPanel.getIdentityPanels().setEnabled(false);
            applicantPanel.getEmployeePanels().setEnabled(false);
            applicantPanel.getOtherInfoPanel().setEnabled(false);
            applicantPanel.getLoanTabPanel().setEnabled(false);
        } else if(isSelectedTab(selectedTab, guarantorPanel)){
            guarantorPanel.assignValues(quotation);
        } else if(isSelectedTab(selectedTab, assetPanel)){
            assetPanel.assignValues(collection.getContract());
        } else if(isSelectedTab(selectedTab, financialProductPanel)){
            financialProductPanel.assignValues(quotation);
        } else if(isSelectedTab(selectedTab, documentsPanel)){
            documentsPanel.assignValues(quotation);
        } else if(isSelectedTab(selectedTab, contractDocumentsPanel)){
            contractDocumentsPanel.assignValues(collection.getContract());
        }
    }

    private boolean isSelectedTab(AbstractTabPanel selectedTab, AbstractTabPanel tab){
        return selectedTab == tab;
    }

}
