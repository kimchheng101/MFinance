package com.soma.mfinance.gui.ui.panel.report.processtime;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * AUTHOR : kimsuor.seang
 * EMAIL  : chhaichivon1995@gmail.com
 * DATE   : 8/12/2017
 * TIME   : 8:08 AM
 */

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(ProcessTimesPanel.NAME)
public class ProcessTimesPanel extends AbstractTabsheetPanel implements View {

	private static final long serialVersionUID = 3391041271200057568L;
	
	public static final String NAME = "process.time.report";
	
	@Autowired
	ProcessTimesTablePanel processTimesTablePanel;
	
	@PostConstruct
	public void PostConstruct() {
		super.init();
		processTimesTablePanel.setMainPanel(this);
		getTabSheet().setTablePanel(processTimesTablePanel);
	}
	
	@Override
	public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
		getTabSheet().setSelectedTab(selectedTab);
	}

	@Override
	public void onAddEventClick() {
		
	}

	@Override
	public void onEditEventClick() {
		
	}

	@Override
	public void enter(ViewChangeEvent event) {
		
	}
}

