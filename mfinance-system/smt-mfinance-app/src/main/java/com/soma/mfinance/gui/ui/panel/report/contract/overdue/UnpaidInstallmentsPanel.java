package com.soma.mfinance.gui.ui.panel.report.contract.overdue;

import com.soma.mfinance.core.accounting.InstallmentVO;
import com.soma.mfinance.core.accounting.MultiAmountTypeVO;
import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.contract.service.ContractService;
import com.soma.mfinance.core.contract.service.cashflow.CashflowService;
import com.soma.mfinance.core.entityfield.InstallmentEntityField;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.shared.contract.PenaltyVO;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.Table.Align;
import com.vaadin.ui.VerticalLayout;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.amount.Amount;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author kimsuor.seang
 * @author s.torn
 */
public class UnpaidInstallmentsPanel extends AbstractTabPanel implements InstallmentEntityField, FinServicesHelper {

    /** */
    private static final long serialVersionUID = -267789039567819096L;

    private ContractService contractService = SpringUtils.getBean(ContractService.class);
    private CashflowService cashflowService = SpringUtils.getBean(CashflowService.class);

    private SimplePagedTable<Cashflow> pagedTable;
    private Contract contract;

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTabPanel#createForm()
     */
    @Override
    protected Component createForm() {
        pagedTable = new SimplePagedTable<Cashflow>(createColumnDefinitions());

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSpacing(true);

        verticalLayout.addComponent(pagedTable);
        verticalLayout.addComponent(pagedTable.createControls());

        return verticalLayout;
    }


    /**
     * @param
     */
    @SuppressWarnings("unchecked")
    private void setIndexedContainer() {
        Indexed indexedContainer = pagedTable.getContainerDataSource();
        indexedContainer.removeAllItems();
        if (contract != null) {
            Individual individual = contract.getApplicant().getIndividual();
            int term = contract.getTerm();
            int firstInstallmentNotPaid = contract.getLastPaidNumInstallment() + 1;
            for (int i = firstInstallmentNotPaid; i <= term; i++) {
                List<InstallmentVO> installmentByNumInstallments = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(contract, i);
                Date installamentDate = installmentByNumInstallments.get(0).getInstallmentDate();
                MultiAmountTypeVO multiAmountTypeVO = INSTALLMENT_SERVICE_MFP.getEachAmountTypes(contract, installmentByNumInstallments);
                double tiPrincipal = 0d;
                double tiInterestRate = 0d;
                double tiInsurance = 0d;
                double tiServiceFee = 0d;
                double otherAmount = 0d;
                double totalInstallment = 0d;
                double installmentAmount = 0d;
                double remainingBalance = 0d;
                double tiTransFee = 0d;
                double penaltyAmount = 0d;
                PenaltyVO penaltyVO;
                InstallmentVO installmentVOFirstIndex = installmentByNumInstallments.get(0);
                if (multiAmountTypeVO != null) {
                    tiPrincipal = multiAmountTypeVO.getTiPrincipal();
                    tiInterestRate = multiAmountTypeVO.getTiInterestRate();
                    tiInsurance = multiAmountTypeVO.getTiInsurance();
                    tiServiceFee = multiAmountTypeVO.getTiServiceFee();
                    otherAmount = multiAmountTypeVO.getOtherAmount();
                    totalInstallment = multiAmountTypeVO.getTotalInstallment()+multiAmountTypeVO.getVatTotalInstallment();
                    installmentAmount = multiAmountTypeVO.getInstallmentAmount();
                    remainingBalance = multiAmountTypeVO.getRemainingBalance();
                    tiTransFee = multiAmountTypeVO.getTiTransFee();
                    penaltyAmount=multiAmountTypeVO.getPenaltyAmount()+multiAmountTypeVO.getVatPenaltyAmount();
                    penaltyVO=multiAmountTypeVO.getPenaltyVO();
                }else {
                    //TODO: WHEN NO OVERDUE DAY
                    return;
                }
                /*totalInstallment = tiPrincipal + tiInterestRate + tiInsurance + tiServiceFee + otherAmount;
                installmentAmount = tiPrincipal + tiInterestRate;
                remainingBalance = installmentVOFirstIndex.getBalance();*/

                Item item = indexedContainer.addItem(i);
                item.getItemProperty(NUM_INSTALLMENT).setValue(installmentVOFirstIndex.getNumInstallment());
                item.getItemProperty(CONTRACT).setValue(contract.getReference());
                item.getItemProperty(DUE_DATE).setValue(installamentDate);
                item.getItemProperty(LAST_NAME_EN).setValue(individual.getLastNameEn());
                item.getItemProperty(FIRST_NAME_EN).setValue(individual.getFirstNameEn());
                item.getItemProperty(INSTALLMENT_AMOUNT).setValue(new Amount(installmentAmount, 0d, installmentAmount));
                item.getItemProperty(PRINCIPAL_AMOUNT).setValue(new Amount(tiPrincipal, 0d, tiPrincipal));
                item.getItemProperty(INTEREST_AMOUNT).setValue(new Amount(tiInterestRate, 0d, tiInterestRate));
                item.getItemProperty(INSURANCE_FEE).setValue(new Amount(tiInsurance, 0d, tiInsurance));
                item.getItemProperty(SERVICING_FEE).setValue(new Amount(tiServiceFee, 0d, tiServiceFee));
                item.getItemProperty(TRANSFER_FEE).setValue(AmountUtils.format(tiTransFee));
                item.getItemProperty(OTHER_AMOUNT).setValue(new Amount(otherAmount, 0d, otherAmount));
                item.getItemProperty(NUM_OVERDUE_DAY).setValue(penaltyVO!=null?penaltyVO.getNumOverdueDays():0);
                item.getItemProperty(NUM_PENALTY_DAY).setValue(penaltyVO!=null?penaltyVO.getNumPenaltyDays():0);
                item.getItemProperty(PENALTY_AMOUNT).setValue(new Amount(penaltyAmount, 0d, penaltyAmount));
                item.getItemProperty("total.installment.amount").setValue(new Amount(totalInstallment, 0d, totalInstallment));
                item.getItemProperty("remaining.balance").setValue(new Amount(remainingBalance, 0d, remainingBalance));
            }
        }
        pagedTable.refreshContainerDataSource();
    }

    /**
     * @return
     */
    private List<ColumnDefinition> createColumnDefinitions() {
        List<ColumnDefinition> columnDefinitions = new ArrayList<ColumnDefinition>();
        columnDefinitions.add(new ColumnDefinition(NUM_INSTALLMENT,I18N.message("number.installment"), Integer.class,Align.CENTER, 120));
        columnDefinitions.add(new ColumnDefinition(CONTRACT, I18N.message("contract"), String.class, Align.LEFT, 140));
        columnDefinitions.add(new ColumnDefinition(LAST_NAME_EN, I18N.message("lastname.en"), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition(FIRST_NAME_EN, I18N.message("firstname.en"), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition(DUE_DATE, I18N.message("due.date"), Date.class, Align.LEFT, 90));
        columnDefinitions.add(new ColumnDefinition(INSTALLMENT_AMOUNT, I18N.message("installment.amount"), Amount.class, Align.RIGHT, 120));
        columnDefinitions.add(new ColumnDefinition(SERVICING_FEE, I18N.message("service.amount"), Amount.class, Align.RIGHT, 90));
        columnDefinitions.add(new ColumnDefinition(TRANSFER_FEE, I18N.message("transfer.fee"), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition(INSURANCE_FEE, I18N.message("insurance.amount"), Amount.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition(OTHER_AMOUNT, I18N.message("other.amount"), Amount.class, Align.RIGHT, 90));
        columnDefinitions.add(new ColumnDefinition(PRINCIPAL_AMOUNT, I18N.message("principal.amount.with.vat"), Amount.class, Align.RIGHT, 135));
        columnDefinitions.add(new ColumnDefinition(NUM_OVERDUE_DAY, I18N.message("no.overdue.days"), Integer.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition(NUM_PENALTY_DAY, I18N.message("no.penalty.days"), Integer.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition(INTEREST_AMOUNT, I18N.message("interest.amount"), Amount.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition(PENALTY_AMOUNT, I18N.message("penalty.amount"), Amount.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition(TOTAL_INSTALLMENT_AMOUNT, I18N.message("total.installment.amount"), Amount.class, Align.RIGHT, 120));
        columnDefinitions.add(new ColumnDefinition(REMAINING_BALANCE, I18N.message("remaining.balance"), Amount.class, Align.RIGHT, 110));
        return columnDefinitions;
    }

    /**
     * @param contract
     */
    public void assignValues(Contract contract) {
        this.contract = contract;
        setIndexedContainer();
    }
}
