package com.soma.mfinance.gui.ui.panel.report.contract.data;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.applicant.model.Employment;
import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.ContractDocument;
import com.soma.mfinance.core.entityfield.InstallmentEntityField;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.model.QuotationApplicant;
import com.soma.ersys.core.hr.model.address.Address;
import com.soma.ersys.core.hr.model.eref.EMediaPromoting;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyNumberUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author p.ly
 */
public class ContractDataReportTable implements InstallmentEntityField, FinServicesHelper {
    private List<ColumnDefinition> columnDefinitions;
    private SimplePagedTable<Contract> pagedTable;
    private Double empty = 0d;
    public ContractDataReportTable() {

    }

    public List<ColumnDefinition> getHeader() {

        columnDefinitions = new ArrayList<ColumnDefinition>();
        //columnDefinitions.add(new ColumnDefinition("date.issued", I18N.message("Date Issued"), Date.class, Align.CENTER, 100));
        columnDefinitions.add(new ColumnDefinition("activation.date", I18N.message("Activation Date"), Date.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("family.name", I18N.message("Family Name"), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("first.name", I18N.message("First Name"), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("dealer", I18N.message("Dealer"), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("GLF.account.number", I18N.message("GLF account number"), String.class, Align.LEFT, 170));
        columnDefinitions.add(new ColumnDefinition("gender", "Gender", String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("age", "Age", Integer.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("phone.number1", "Phone number 1", String.class, Align.CENTER, 100));
        columnDefinitions.add(new ColumnDefinition("phone.number2", "Phone number 2", String.class, Align.CENTER, 100));
        columnDefinitions.add(new ColumnDefinition("phone.number.guarantor", "Phone number Guarantor", String.class, Align.CENTER, 100));
        columnDefinitions.add(new ColumnDefinition("marital.status", "Marital Status", String.class, Align.CENTER, 100));
        columnDefinitions.add(new ColumnDefinition("children", "Children", Integer.class, Align.CENTER, 40));
        columnDefinitions.add(new ColumnDefinition("housing", "Housing", String.class, Align.RIGHT, 60));
        columnDefinitions.add(new ColumnDefinition("district", "District", String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("provinces", "Provinces", String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("employment.status", "Employment Status", String.class, Align.RIGHT, 150));
        columnDefinitions.add(new ColumnDefinition("employment.industry", "Employment Industry", String.class, Align.RIGHT, 170));
        columnDefinitions.add(new ColumnDefinition("seniority.level", "Seniority Level", String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("position", "Position", String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("work.place.name", "Work Place Name", String.class, Align.LEFT, 140));
        columnDefinitions.add(new ColumnDefinition("way.of.knowing", "Way Of Knowing", String.class, Align.LEFT, 140));
        //columnDefinitions.add(new ColumnDefinition("work.phone", I18N.message("work.phone"), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("basic.salary", "Basic Salary", Double.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("allowance", "Allowance", Double.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("business.expenses", "Business Expenses", Double.class, Align.LEFT, 100));

        columnDefinitions.add(new ColumnDefinition("family.book", "Family Book", String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("passport", "Passport", String.class, Align.LEFT, 70));
        columnDefinitions.add(new ColumnDefinition("resident.book", "Resident Book", String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("birth.certificate", "Birth Certificate", String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("village.certified.letter", "Village Certified Letter", String.class, Align.LEFT, 140));
        columnDefinitions.add(new ColumnDefinition("voter.registration.card", "Voter Registration Card", String.class, Align.LEFT, 140));
        columnDefinitions.add(new ColumnDefinition("driving.licence", "Driving Licence", String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("utilities.bill", "Utilities Bill", String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("rent.contract", "Rent Contract", String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("salary.slip", "Salary Slip", String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("working.contract", "Working Contract", String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("bank.book", "Bank Book", String.class, Align.LEFT, 90));
        columnDefinitions.add(new ColumnDefinition("land.title", "Land Title", String.class, Align.LEFT, 90));
        columnDefinitions.add(new ColumnDefinition("other", "Other", String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("ratio", "Ratio", String.class, Align.LEFT, 40));


        columnDefinitions.add(new ColumnDefinition("guarantor.familyname", "Family Name", String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.firstname", "First Name", String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.gender", "Gender", String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.age", "Age", Integer.class, Align.LEFT, 50));
        columnDefinitions.add(new ColumnDefinition("guarantor.marital.status", "Marital Status", String.class, Align.CENTER, 170));
        columnDefinitions.add(new ColumnDefinition("guarantor.district", "District", String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.provinces", "Provinces", String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.relationship.with.applicant", "Relationship With Applicant", String.class, Align.RIGHT, 170));
        columnDefinitions.add(new ColumnDefinition("guarantor.employment.status", "Employment Status", String.class, Align.RIGHT, 170));
        columnDefinitions.add(new ColumnDefinition("guarantor.employment.industry", "Employment Industry", String.class, Align.RIGHT, 120));
        columnDefinitions.add(new ColumnDefinition("guarantor.seniority.level", "Seniority Level", String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.position", "Position", String.class, Align.RIGHT, 90));
        columnDefinitions.add(new ColumnDefinition("guarantor.work.place.name", "Work Place Name", String.class, Align.LEFT, 70));
        columnDefinitions.add(new ColumnDefinition("guarantor.basic.salary", "Basic Salary", Double.class, Align.RIGHT, 50));
        columnDefinitions.add(new ColumnDefinition("guarantor.allowance", "Allowance", Double.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.business.expenses", "Business Expenses", Double.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.live.with.applicant", "Live With Applicant", String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.family.book", "Family Book", String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.passport", "Passport", String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.resident.book", "Resident Book", String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.birth.certificate", "Birth Certificate", String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.village.certified.letter", "Village Certified Letter", String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.voter.registration.card", "Voter Registration Card", String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.driving.licence", "Driving Licence", String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.utilities.bill", "Utilities Bill", String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.rent.contract", "Rent Contract", String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.salary.slip", "Salary Slip", String.class, Align.RIGHT, 100));

        columnDefinitions.add(new ColumnDefinition("guarantor.working.contract_old", "Working Contract", String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.bank.book", "Bank Book", String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.land.title", "Land Title", String.class, Align.LEFT, 80));
        columnDefinitions.add(new ColumnDefinition("patent", "Patent", String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("guarantor.other", "Other", String.class, Align.LEFT, 40));

        columnDefinitions.add(new ColumnDefinition("asset.Range", "Asset Range", String.class, Align.LEFT, 67));
        //columnDefinitions.add(new ColumnDefinition("advance.payment", "Advance Payment", Double.class, Align.RIGHT, 70));
        //columnDefinitions.add(new ColumnDefinition("ass.sec.market.price", "Secondhand Price", Double.class, Align.RIGHT, 90));
        //columnDefinitions.add(new ColumnDefinition("advance.payment.percentage","%Advance Payment", Double.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("interest.rate","Interest Rate", Double.class, Align.RIGHT, 70));
        //columnDefinitions.add(new ColumnDefinition("original.price", "Original Price", Double.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("term", "Term", Integer.class, Align.LEFT, 50));
        columnDefinitions.add(new ColumnDefinition("insurance", "Insurance", Double.class, Align.LEFT, 50));
        columnDefinitions.add(new ColumnDefinition("asset.appraisal.price", "Asset Price", Double.class, Align.RIGHT, 109));
        columnDefinitions.add(new ColumnDefinition("year.of.motor.cycle", "Year of Motorcycle", String.class, Align.RIGHT, 109));
        columnDefinitions.add(new ColumnDefinition("lease.amount", "Lease Amount", Double.class, Align.LEFT, 70));
        columnDefinitions.add(new ColumnDefinition("percent.lease.amount", "% Lease Amount", Double.class, Align.LEFT, 110));
        columnDefinitions.add(new ColumnDefinition("base.salary", "Base Salary", Double.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("allowance2", "Allowance", Double.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("business.expense", "Business Expense", Double.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("net.income", "Net Income", Double.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("personal.expenses", "Personal Expenses", Double.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("family.expenses", "Family Expenses", Double.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("liability", "Liability", Double.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("disposable.income", "Disposable Income", Double.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("installment", "GLF Installment", Double.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("financial.product", "Financial Product", String.class, Align.RIGHT, 100));

        return columnDefinitions;
    }

    /**
     * @param
     */
    public void getData(List<Contract> contracts) {
        Indexed indexedContainer = pagedTable.getContainerDataSource();
        indexedContainer.removeAllItems();
        int index = 0;
        if (contracts != null && !contracts.isEmpty()) {
            for (Contract contract : contracts) {
                Applicant applicant = contract.getApplicant();
                Individual individual = applicant.getIndividual();
                Address appAddress = individual.getMainAddress();
                Applicant guarantorApplicant = contract.getGuarantor();
                Employment employment = individual.getCurrentEmployment();
                String assetRangeDesc = "";
                if (contract.getAsset() != null) {
                    assetRangeDesc = contract.getAsset().getAssetRange().getDescEn();
                }
                final Item item = indexedContainer.addItem(index);
                if (applicant != null) {
                    String appfamilyBook = "";
                    String appPassport = "";
                    String appResidentBook = "";
                    String appBirthCertificate = "";
                    String appVillageCertifiedLetter = "";
                    String appVoterRegistrationCard = "";
                    String appDrivingLicence = "";
                    String appUtilitiesBill = "";
                    String appRentContract = "";
                    String appSalarySlip = "";
                    String appWorkingContract = "";
                    String appBankBook = "";
                    String appLandTitle = "";
                    String appPatent = "";
                    String appOther = "";
                    List<ContractDocument> documents = contract.getContractDocuments(EApplicantType.C);
                    if (documents != null && !documents.isEmpty()) {
                        for (ContractDocument quotationDocument : documents) {
                            String documentCode = quotationDocument.getDocument().getCode();
                            if (documentCode.equals("F")) {
                                appfamilyBook = "Yes";
                            }
                            if (documentCode.equals("P")) {
                                appPassport = "Yes";
                            }
                            if (documentCode.equals("R")) {
                                appResidentBook = "Yes";
                            }
                            if (documentCode.equals("B")) {
                                appBirthCertificate = "Yes";
                            }
                            if (documentCode.equals("G")) {
                                appVillageCertifiedLetter = "Yes";
                            }
                            if (documentCode.equals("V")) {
                                appVoterRegistrationCard = "Yes";
                            }
                            if (documentCode.equals("D")) {
                                appDrivingLicence = "Yes";
                            }
                            if (documentCode.equals("BIL")) {
                                appUtilitiesBill = "Yes";
                            }
                            if (documentCode.equals("REN")) {
                                appRentContract = "Yes";
                            }
                            if (documentCode.equals("SAL")) {
                                appSalarySlip = "Yes";
                            }
                            if (documentCode.equals("WOR")) {
                                appWorkingContract = "Yes";
                            }
                            if (documentCode.equals("BAN")) {
                                appBankBook = "Yes";
                            }
                            if (documentCode.equals("LAN")) {
                                appLandTitle = "Yes";
                            }
                            if (documentCode.equals("PAT")) {
                                appPatent = "Yes";
                            }
                            if (documentCode.equals("O")) {
                                appOther = "Yes";
                            }
                        }
                    }
                    String seniorityLevel = "";
                    if (employment != null && employment.getSeniorityLevel() != null) {
                        seniorityLevel = employment.getSeniorityLevel().getDescEn();
                    }

                    //item.getItemProperty("date.issued").setValue(applicant.getCreateDate() != null ? applicant.getCreateDate() : "");
                    if (individual != null) {
                        item.getItemProperty("activation.date").setValue(contract.getQuotation().getActivationDate());
                        item.getItemProperty("family.name").setValue(individual.getLastNameEn());
                        item.getItemProperty("first.name").setValue(individual.getFirstNameEn());
                        item.getItemProperty("dealer").setValue(contract.getQuotation().getDealer().getNameEn());
                        item.getItemProperty("gender").setValue(individual.getGender().getDescEn());
                        item.getItemProperty("age").setValue(DateUtils.getAge(individual.getBirthDate()));
                        item.getItemProperty("marital.status").setValue(individual.getMaritalStatus().getDesc());
                        item.getItemProperty("phone.number1").setValue(individual.getMobilePerso());
                        item.getItemProperty("phone.number2").setValue(individual.getMobilePerso2() != null ? individual.getMobilePerso2() : "");
                        item.getItemProperty("phone.number.guarantor").setValue(contract.getQuotation().getGuarantor() == null ? "" : contract.getQuotation().getGuarantor().getIndividual().getMobilePerso());
                        item.getItemProperty("children").setValue(individual.getNumberOfChildren());
                    }
                    if (contract != null && contract.getDealer() != null) {
                        item.getItemProperty("GLF.account.number").setValue(contract.getExternalReference());
                    }
                    if (individual != null && individual.getIndividualAddresses() != null && !individual.getIndividualAddresses().isEmpty() && individual.getIndividualAddresses().get(0) != null && individual.getIndividualAddresses().get(0).getHousing() != null) {
                        item.getItemProperty("housing").setValue(individual.getIndividualAddresses().get(0).getHousing().getDescEn());
                    }
                    if (appAddress != null && appAddress.getDistrict() != null) {
                        item.getItemProperty("district").setValue(appAddress.getDistrict().getDescEn());
                    }
                    if (appAddress != null && appAddress.getProvince() != null) {
                        item.getItemProperty("provinces").setValue(appAddress.getProvince().getDescEn());
                    }
                    if (employment != null) {

                        item.getItemProperty("employment.status").setValue(employment.getEmploymentStatus().getDescEn());
                        item.getItemProperty("employment.industry").setValue(employment.getEmploymentIndustry().getDescEn());
                        item.getItemProperty("position").setValue(employment.getEmploymentPosition().getDescEn());
                        item.getItemProperty("work.place.name").setValue(employment.getWorkPlaceName());
                        item.getItemProperty("way.of.knowing").setValue(
                                contract.getQuotation().getWayOfKnowing().getDescEn()
                                + eMediaPromotingToStringWithComma(contract.getQuotation().getWayOfKnowing1())
                                + eMediaPromotingToStringWithComma(contract.getQuotation().getWayOfKnowing2())
                        );
                        //item.getItemProperty("work.phone").setValue(employment.getWorkPhone());
                        item.getItemProperty("basic.salary").setValue(employment.getRevenue());
                        item.getItemProperty("allowance").setValue(MyNumberUtils.getDouble(employment.getAllowance()));
                        item.getItemProperty("allowance2").setValue(MyNumberUtils.getDouble(employment.getAllowance()));
                        item.getItemProperty("business.expenses").setValue(employment.getAllowance() != null ? employment.getAllowance() : empty);
                    }

                    item.getItemProperty("seniority.level").setValue(seniorityLevel);
                    item.getItemProperty("family.book").setValue(appfamilyBook);
                    item.getItemProperty("passport").setValue(appPassport);
                    item.getItemProperty("resident.book").setValue(appResidentBook);
                    item.getItemProperty("birth.certificate").setValue(appBirthCertificate);
                    item.getItemProperty("village.certified.letter").setValue(appVillageCertifiedLetter);
                    item.getItemProperty("voter.registration.card").setValue(appVoterRegistrationCard);
                    item.getItemProperty("driving.licence").setValue(appDrivingLicence);
                    item.getItemProperty("utilities.bill").setValue(appUtilitiesBill);
                    item.getItemProperty("rent.contract").setValue(appRentContract);
                    item.getItemProperty("salary.slip").setValue(appSalarySlip);
                    item.getItemProperty("working.contract").setValue(appWorkingContract);
                    item.getItemProperty("bank.book").setValue(appBankBook);
                    item.getItemProperty("land.title").setValue(appLandTitle);
                    item.getItemProperty("other").setValue(appOther);
                    item.getItemProperty("patent").setValue(appPatent);
                }

                if (guarantorApplicant != null) {
                    Individual guarantor = guarantorApplicant.getIndividual();
                    Address guaAddress = guarantor.getMainAddress();
                    Employment guaEmployment = guarantor.getCurrentEmployment();
                    String relationshipWithApp = "";
                    if (guarantor.getQuotationApplicants() != null && !guarantor.getQuotationApplicants().isEmpty()) {
                        QuotationApplicant quotationApplicant = guarantor.getQuotationApplicants().get(0);
                        if (quotationApplicant.getRelationship() != null) {
                            relationshipWithApp = quotationApplicant.getRelationship().getDescEn();
                        }
                    }
                    String guaSeniorityLevel = "";
                    if (guarantorApplicant.getIndividual() != null && guarantorApplicant.getIndividual().getCurrentEmployment() != null && guarantorApplicant.getIndividual().getCurrentEmployment().getSeniorityLevel() != null) {
                        guaSeniorityLevel = guarantorApplicant.getIndividual().getCurrentEmployment().getSeniorityLevel().getDescEn();
                    }
                    String liveWithApplicant = "No";
                    if (guaEmployment != null && guaEmployment.isSameApplicantAddress()) {
                        liveWithApplicant = "Yes";
                    }
                    String guafamilyBook = "";
                    String guaPassport = "";
                    String guaResidentBook = "";
                    String guaBirthCertificate = "";
                    String guaVillageCertifiedLetter = "";
                    String guaVoterRegistrationCard = "";
                    String guaDrivingLicence = "";
                    String guaUtilitiesBill = "";
                    String guaRentContract = "";
                    String guaSalarySlip = "";
                    String guaWorkingContract = "";
                    String guaBankBook = "";
                    String guaLandTitle = "";
                    String guaPatent = "";
                    String guaOther = "";
                    List<ContractDocument> guaDocuments = contract.getContractDocuments(EApplicantType.G);
                    if (guaDocuments != null && !guaDocuments.isEmpty()) {
                        for (ContractDocument guaDocument : guaDocuments) {
                            String documentCode = guaDocument.getDocument().getCode();
                            System.out.println("documentCode : " + documentCode);
                            if (documentCode.equals("F")) {
                                guafamilyBook = "Yes";
                            }
                            if (documentCode.equals("P")) {
                                guaPassport = "Yes";
                            }
                            if (documentCode.equals("R")) {
                                guaResidentBook = "Yes";
                            }
                            if (documentCode.equals("B")) {
                                guaBirthCertificate = "Yes";
                            }
                            if (documentCode.equals("G")) {
                                guaVillageCertifiedLetter = "Yes";
                            }
                            if (documentCode.equals("V")) {
                                guaVoterRegistrationCard = "Yes";
                            }
                            if (documentCode.equals("D")) {
                                guaDrivingLicence = "Yes";
                            }
                            if (documentCode.equals("BIL")) {
                                guaUtilitiesBill = "Yes";
                            }
                            if (documentCode.equals("REN")) {
                                guaRentContract = "Yes";
                            }
                            if (documentCode.equals("SAL")) {
                                guaSalarySlip = "Yes";
                            }
                            if (documentCode.equals("WOR")) {
                                guaWorkingContract = "Yes";
                            }
                            if (documentCode.equals("BAN")) {
                                guaBankBook = "Yes";
                            }
                            if (documentCode.equals("LAN")) {
                                guaLandTitle = "Yes";
                            }
                            if (documentCode.equals("PAT")) {
                                guaPatent = "Yes";
                            }
                            if (documentCode.equals("O")) {
                                guaOther = "Yes";
                            }
                        }
                    }
                    item.getItemProperty("guarantor.familyname").setValue(guarantor.getLastNameEn());
                    item.getItemProperty("guarantor.firstname").setValue(guarantor.getFirstNameEn());
                    if (guarantor.getGender() != null) {
                        item.getItemProperty("guarantor.gender").setValue(guarantor.getGender().getDescEn());
                    }
                    if (guarantor.getBirthDate() != null) {
                        item.getItemProperty("guarantor.age").setValue(MyNumberUtils.getInteger(DateUtils.getAge(guarantor.getBirthDate())));
                    }
                    if (guarantor.getMaritalStatus() != null) {
                        item.getItemProperty("guarantor.marital.status").setValue(guarantor.getMaritalStatus().getDesc());
                    }
                    if (guaAddress != null && guaAddress.getDistrict() != null) {
                        item.getItemProperty("guarantor.district").setValue(guaAddress.getDistrict().getDescEn());
                    }
                    if (guaAddress != null && guaAddress.getProvince() != null) {
                        item.getItemProperty("guarantor.provinces").setValue(guaAddress.getProvince().getDescEn());
                    }

                    if (guaEmployment != null && guaEmployment.getEmploymentStatus() != null) {
                        item.getItemProperty("guarantor.employment.status").setValue(guaEmployment.getEmploymentStatus().getDescEn());
                    }
                    if (guaEmployment != null && guaEmployment.getEmploymentIndustry() != null) {
                        item.getItemProperty("guarantor.employment.industry").setValue(guaEmployment.getEmploymentIndustry().getDescEn());
                    }
                    if (guaEmployment != null) {
                        item.getItemProperty("guarantor.position").setValue(guaEmployment.getEmploymentPosition() != null ? guaEmployment.getEmploymentPosition().getDescEn(): " ");
                        item.getItemProperty("guarantor.work.place.name").setValue(guaEmployment.getWorkPlaceName() != null ? guaEmployment.getWorkPlaceName() : " ");
                        item.getItemProperty("guarantor.basic.salary").setValue(guaEmployment.getRevenue() != null ? guaEmployment.getRevenue() : 0d);
                        item.getItemProperty("guarantor.allowance").setValue(guaEmployment.getAllowance() != null ? guaEmployment.getAllowance() : 0d);
                        item.getItemProperty("guarantor.business.expenses").setValue(guaEmployment.getBusinessExpense() != null ? guaEmployment.getBusinessExpense() : 0d);
                    }

                    item.getItemProperty("guarantor.relationship.with.applicant").setValue(relationshipWithApp);
                    item.getItemProperty("guarantor.seniority.level").setValue(guaSeniorityLevel);
                    item.getItemProperty("guarantor.live.with.applicant").setValue(liveWithApplicant);
                    item.getItemProperty("guarantor.family.book").setValue(guafamilyBook);
                    item.getItemProperty("guarantor.passport").setValue(guaPassport);
                    item.getItemProperty("guarantor.resident.book").setValue(guaResidentBook);
                    item.getItemProperty("guarantor.birth.certificate").setValue(guaBirthCertificate);
                    item.getItemProperty("guarantor.village.certified.letter").setValue(guaVillageCertifiedLetter);
                    item.getItemProperty("guarantor.voter.registration.card").setValue(guaVoterRegistrationCard);
                    item.getItemProperty("guarantor.driving.licence").setValue(guaDrivingLicence);
                    item.getItemProperty("guarantor.utilities.bill").setValue(guaUtilitiesBill);
                    item.getItemProperty("guarantor.rent.contract").setValue(guaRentContract);
                    item.getItemProperty("guarantor.salary.slip").setValue(guaSalarySlip);
                    item.getItemProperty("guarantor.bank.book").setValue(guaBankBook);
                    item.getItemProperty("guarantor.other").setValue(guaOther);
                }

                //masset
                double totalBaseSalary = 0d;
                double assetPrice = 0d;
                double leaseAmount = 0d;
                double totalAllowance = 0d;
                double totalBusinessExpense = 0d;
                double netIncome = 0d;

                List<Employment> employments = individual.getEmployments();
                for (Employment employment1 : employments) {
                    totalBaseSalary += MyNumberUtils.getDouble(employment1.getRevenue());
                    totalAllowance += MyNumberUtils.getDouble(employment1.getAllowance());
                    totalBusinessExpense += MyNumberUtils.getDouble(employment1.getBusinessExpense());
                }

                /**
                 * Calculate the filed
                 * assetPrice,leaseAmount,baseSalary,Allowance,,netIncome,personalExpense,
                 * familyExpense,liability,disposableIncome,glfInstallment
                 */

                //assetPrice = MyNumberUtils.getDouble(contract.getAsset().getTiAssetPrice());
                if (contract.getQuotation() != null) {
                    leaseAmount = MyNumberUtils.getDouble(contract.getQuotation().getTiFinanceAmount());
                }

                netIncome = totalBaseSalary + totalAllowance - totalBusinessExpense;
                double personalExpense = MyNumberUtils.getDouble(individual.getMonthlyPersonalExpenses());
                double familyExpense = MyNumberUtils.getDouble(individual.getMonthlyFamilyExpenses());
                double liability = MyNumberUtils.getDouble(individual.getTotalDebtInstallment());
                double totalDebtInstallment = MyNumberUtils.getDouble(individual.getTotalDebtInstallment());
                double totalExpenses = MyNumberUtils.getDouble(individual.getMonthlyPersonalExpenses())
                        + MyNumberUtils.getDouble(individual.getMonthlyFamilyExpenses())
                        + totalDebtInstallment;
                //double disposableIncome = netIncome - totalExpenses;
                double disposableIncome = totalBaseSalary + totalAllowance - totalBusinessExpense - personalExpense - familyExpense - liability;
                double glfInstallment = MyNumberUtils.getDouble(contract.getTiInstallmentAmount());

                item.getItemProperty("asset.Range").setValue(assetRangeDesc);
                //item.getItemProperty("ass.sec.market.price").setValue(contract.getAsset().getTiAssetSecondHandPrice());
                //item.getItemProperty("advance.payment").setValue(contract.getQuotation().getTiAdvancePaymentAmount());
                //item.getItemProperty("advance.payment.percentage").setValue(contract.getQuotation().getAdvancePaymentPercentage());
                item.getItemProperty("interest.rate").setValue(contract.getQuotation().getInterestRate());
                //item.getItemProperty("original.price").setValue(contract.getAsset().getTeAssetPrice());
                item.getItemProperty("term").setValue(MyNumberUtils.getInteger(contract.getTerm()));
                item.getItemProperty("insurance").setValue(FIN_PROD_SRV.getInsuranceFee(leaseAmount) + FIN_PROD_SRV.getVatInsurance(leaseAmount));
                item.getItemProperty("percent.lease.amount").setValue(MyNumberUtils.getDouble(contract.getQuotation().getLeaseAmountPercentage()));
                item.getItemProperty("asset.appraisal.price").setValue(MyNumberUtils.getDouble(contract.getAsset().getTeAssetApprPrice()));
                item.getItemProperty("year.of.motor.cycle").setValue(contract.getAsset().getAssetYear().getCode());
                item.getItemProperty("lease.amount").setValue(MyNumberUtils.getDouble(leaseAmount));
                item.getItemProperty("base.salary").setValue(MyNumberUtils.getDouble(employment.getRevenue()));
                item.getItemProperty("business.expense").setValue(MyNumberUtils.getDouble(employment.getBusinessExpense()));
                item.getItemProperty("net.income").setValue(MyNumberUtils.getDouble(netIncome));
                item.getItemProperty("personal.expenses").setValue(MyNumberUtils.getDouble(personalExpense));
                item.getItemProperty("family.expenses").setValue(MyNumberUtils.getDouble(familyExpense));
                item.getItemProperty("liability").setValue(MyNumberUtils.getDouble(liability));
                item.getItemProperty("disposable.income").setValue(MyNumberUtils.getDouble(disposableIncome));
                item.getItemProperty("installment").setValue(contract.getQuotation().getTotalInstallmentAmount());
                item.getItemProperty("financial.product").setValue(contract.getQuotation().getFinancialProduct().getDescEn());
                index++;
            }
        }
        pagedTable.refreshContainerDataSource();
    }

    /**
     * @param intValue
     * @return
     */
    private int getInteger(Integer intValue) {
        if (intValue == null) {
            return 0;
        } else {
            return intValue;
        }
    }

    private String eMediaPromotingToStringWithComma(EMediaPromoting eMediaPromoting){
        return (eMediaPromoting != null && !eMediaPromoting.getDescEn().isEmpty()) ? (", " + eMediaPromoting.getDescEn()) : "";
    }

    /**
     * @return the pagedTable
     */
    public SimplePagedTable<Contract> getPagedTable() {
        return pagedTable;
    }

    /**
     * @param pagedTable the pagedTable to set
     */
    public void setPagedTable(SimplePagedTable<Contract> pagedTable) {
        this.pagedTable = pagedTable;
    }

}
