package com.soma.mfinance.gui.ui.panel.report.contract.data;

import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.entityfield.InstallmentEntityField;
import com.soma.mfinance.core.financial.model.FinProduct;
import com.soma.mfinance.core.financial.model.ProductLine;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.tools.DateUtils;

import java.util.List;
/**
 * 
 * @author p.ly
 *
 */
 
public class ContractDataReportSearchPanel extends VerticalLayout implements FinServicesHelper, InstallmentEntityField{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ERefDataComboBox<EDealerType> cbxDealerType;
	private EntityRefComboBox<FinProduct> cbxFinancialProduct;
	private DealerComboBox cbxDealer;
	private AutoDateField dfStartDate;
	private AutoDateField dfEndDate;
	private EntityRefComboBox<ProductLine> cbxproductLine;
	
	public ContractDataReportSearchPanel(){
	}
	
	public GridLayout getSearchForm(){
		final GridLayout gridLayout = new GridLayout(12, 3);
		gridLayout.setSpacing(true);
		cbxDealerType = new ERefDataComboBox<>(EDealerType.values());
	    cbxDealerType.setImmediate(true);
	    cbxDealerType.setWidth("220px");
	        
	    cbxFinancialProduct = new EntityRefComboBox<>();
        BaseRestrictions<FinProduct> restrictionsFinancial = new BaseRestrictions<>(FinProduct.class);
        restrictionsFinancial.getStatusRecordList().add(EStatusRecord.ACTIV);
        cbxFinancialProduct.setWidth("220px");
        cbxFinancialProduct.setRestrictions(restrictionsFinancial);
        cbxFinancialProduct.setImmediate(true);
        cbxFinancialProduct.renderer();
        cbxFinancialProduct.setSelectedEntity(null);
	    
		cbxDealer = new DealerComboBox(null, ENTITY_SRV.list(Dealer.class), I18N.message("all"));
		cbxDealer.setSelectedEntity(null);
		cbxDealer.setWidth("220px");
		
		dfStartDate = ComponentFactory.getAutoDateField("",false);
		dfStartDate.setValue(DateUtils.today());
		dfEndDate = ComponentFactory.getAutoDateField("", false);    
		dfEndDate.setValue(DateUtils.today());
		
		cbxproductLine = new EntityRefComboBox<ProductLine>(DataReference.getInstance().getProductLines());
        
		int iCol = 0;
		gridLayout.addComponent(new Label(I18N.message("dealer.type")), iCol++, 0);
        gridLayout.addComponent(cbxDealerType, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("dealer")), iCol++, 0);
        gridLayout.addComponent(cbxDealer, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("financial.product")), iCol++, 0);
        gridLayout.addComponent(cbxFinancialProduct, iCol++, 0);
        
        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("productline")), iCol++, 1);
        gridLayout.addComponent(cbxproductLine, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("start.date")), iCol++, 1);
        gridLayout.addComponent(dfStartDate, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("end.date")), iCol++, 1);
        gridLayout.addComponent(dfEndDate, iCol++, 1);
        return gridLayout;
	}
	/**
	 * 
	 * @return list quotations
	 */
	public List<Contract> getContracts() {
		BaseRestrictions<Contract> restrictions = new BaseRestrictions<Contract>(Contract.class);
		restrictions.addAssociation("dealer", "dea", JoinType.INNER_JOIN);
		restrictions.addAssociation("quotations", "quo", JoinType.INNER_JOIN);
	    if (cbxDealer.getSelectedEntity() != null) {
	        restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, cbxDealer.getSelectedEntity().getId()));
	    }
	    if (cbxDealerType.getSelectedEntity() != null) {
	        restrictions.addCriterion(Restrictions.eq("dea.dealerType", cbxDealerType.getSelectedEntity()));
	    }
	    if (cbxFinancialProduct.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("financialProduct."+ ID, cbxFinancialProduct.getSelectedEntity().getId()));
		}
		if (cbxproductLine.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("productLine." + ID, cbxproductLine.getSelectedEntity().getId()));
		}
		if (dfStartDate.getValue() != null && dfEndDate.getValue() != null){
			restrictions.addCriterion(Restrictions.between("quo.activationDate", DateUtils.getDateAtBeginningOfDay(dfStartDate.getValue()), DateUtils.getDateAtEndOfDay(dfEndDate.getValue())));
		} else {
			if (dfStartDate.getValue() != null) {
				restrictions.addCriterion(Restrictions.ge("quo.activationDate", DateUtils.getDateAtBeginningOfDay(dfStartDate.getValue())));
			}
			if (dfEndDate.getValue() != null) {
				restrictions.addCriterion(Restrictions.le("quo.activationDate", DateUtils.getDateAtEndOfDay(dfEndDate.getValue())));
			}
		}
		
		return ENTITY_SRV.list(restrictions);
	}
}
