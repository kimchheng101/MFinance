package com.soma.mfinance.gui.ui.panel.report.outstanding;

import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.quotation.model.EInsuranceYear;
import com.soma.mfinance.core.registrations.model.ProfileContractStatus;
import com.soma.mfinance.core.shared.accounting.LeaseAdjustment;
import com.soma.mfinance.core.shared.contract.ContractEntityField;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.mfinance.glf.accounting.service.GLFLeasingAccountingService;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import java.util.*;

import static com.soma.mfinance.core.quotation.model.MQuotation.TERM;

/**
 * AUTHOR : kimsuor.seang
 * EMAIL  : chhaichivon1995@gmail.com
 * DATE   : 8/16/2017
 * TIME   : 4:39 PM
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(LeaseAdjustmentsReportPanel.NAME)
public class LeaseAdjustmentsReportPanel extends AbstractTabPanel implements View, ContractEntityField {
    public static final String NAME = "lease.adjustments.report";

    @Autowired
    private GLFLeasingAccountingService accountingService;

    private TabSheet tabSheet;
    private SimplePagedTable<LeaseAdjustment> pagedTable;
    private List<ColumnDefinition> columnDefinitions;
    private ERefDataComboBox<EDealerType> cbxDealerType;
    private EntityComboBox<ProfileContractStatus> cbxContractStatus;
    private DealerComboBox cbxDealer;
    private TextField txtContractReference;
    private AutoDateField dfStartDate;
    private AutoDateField dfEndDate;

    private double first_year_rate = 0.90;
    private double second_year_rate = 0.72;
    private double third_year_rate = 0.50;
    private double forth_year_rate = 0.50;
    private double fifth_year_rate = 0.50;
    private double refundPremiumRate = 0.90;
    private AutoDateField dfPayStartDate;
    private ERefDataComboBox<EInsuranceYear> cbxInsuranceYear;

    @Override
    protected com.vaadin.ui.Component createForm() {

        SecUser user = ProfileUtil.getCurrentUser();
        BaseRestrictions<ProfileContractStatus> restrictions = new BaseRestrictions<>(ProfileContractStatus.class);
        restrictions.addCriterion("profile", user.getDefaultProfile());
        List<ProfileContractStatus> list = ENTITY_SRV.list(restrictions);
        cbxContractStatus = new EntityComboBox<>(ProfileContractStatus.class, "wkfStatus.descEn");
        cbxContractStatus.setEntities(list);
        cbxContractStatus.setImmediate(true);
        cbxContractStatus.setWidth("220px");


        tabSheet = new TabSheet();
        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setSpacing(true);
        contentLayout.setMargin(true);

        VerticalLayout gridLayoutPanel = new VerticalLayout();
        VerticalLayout searchLayout = new VerticalLayout();
        HorizontalLayout buttonsLayout = new HorizontalLayout();
        Button btnSearch = new Button(I18N.message("search"));
        btnSearch.setIcon(new ThemeResource("../smt-default/icons/16/search.png"));
        btnSearch.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                search();
            }
        });

        Button btnReset = new Button(I18N.message("reset"));
        btnReset.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                reset();
            }
        });
        buttonsLayout.setSpacing(true);
        buttonsLayout.addComponent(btnSearch);
        buttonsLayout.addComponent(btnReset);

        final GridLayout gridLayout = new GridLayout(13, 3);
        gridLayout.setSpacing(true);
        cbxDealer = new DealerComboBox(null, DataReference.getInstance().getDealers());
        cbxDealer.setSelectedEntity(null);
        cbxDealer.setWidth("220px");

        cbxDealerType = new ERefDataComboBox<>(EDealerType.values());
        cbxDealerType.setImmediate(true);
        cbxDealerType.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                BaseRestrictions<Dealer> restrictions = new BaseRestrictions<>(Dealer.class);
                if (cbxDealerType.getSelectedEntity() != null) {
                    restrictions.addCriterion(Restrictions.eq("dealerType", cbxDealerType.getSelectedEntity()));
                }
                cbxDealer.setDealers(ENTITY_SRV.list(restrictions));
                cbxDealer.setSelectedEntity(null);
            }
        });

        txtContractReference = ComponentFactory.getTextField(false, 20, 150);
        dfStartDate = ComponentFactory.getAutoDateField("", false);
        dfStartDate.setValue(DateUtils.getDateAtBeginningOfMonth(DateUtils.todayH00M00S00()));
        dfEndDate = ComponentFactory.getAutoDateField("", false);
        dfEndDate.setValue(DateUtils.getDateAtEndOfMonth(DateUtils.todayH00M00S00()));

        dfPayStartDate = ComponentFactory.getAutoDateField("", false);
        dfPayStartDate.setValue(DateUtils.getDateAtBeginningOfMonth(DateUtils.todayH00M00S00()));

        List<EInsuranceYear> eInsuranceYears = new ArrayList<>();
        for(EInsuranceYear eInsuranceYear : EInsuranceYear.values()){
            /**
             *   EInsuranceYear.en.YEAR1 = Year1
                 EInsuranceYear.en.YEAR2 = Year2
                 EInsuranceYear.en.YEAR3 = Year3
                 EInsuranceYear.en.YEAR4 = Year4
             */
            if(eInsuranceYear.getDescEn().equals("EInsuranceYear.en.YEAR1")){
                eInsuranceYear.setDesc("YEAR1");
                eInsuranceYear.setDescEn("YEAR1");
            }else if(eInsuranceYear.getDescEn().equals("EInsuranceYear.en.YEAR2")){
                eInsuranceYear.setDesc("YEAR2");
                eInsuranceYear.setDescEn("YEAR2");
            }else if(eInsuranceYear.getDescEn().equals("EInsuranceYear.en.YEAR3")){
                eInsuranceYear.setDesc("YEAR3");
                eInsuranceYear.setDescEn("YEAR3");
            }else if(eInsuranceYear.getDescEn().equals("EInsuranceYear.en.YEAR4")){
                eInsuranceYear.setDesc("YEAR4");
                eInsuranceYear.setDescEn("YEAR4");
            }
            eInsuranceYears.add(eInsuranceYear);
        }
        cbxInsuranceYear = new ERefDataComboBox<>(eInsuranceYears);
        cbxInsuranceYear.setImmediate(true);
        cbxInsuranceYear.setWidth("120px");



        int iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("contract.reference")), iCol++, 0);
        gridLayout.addComponent(txtContractReference, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("dealer.type")), iCol++, 0);
        gridLayout.addComponent(cbxDealerType, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("dealer")), iCol++, 0);
        gridLayout.addComponent(cbxDealer, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("contract.status")), iCol++, 0);
        gridLayout.addComponent(cbxContractStatus, iCol++, 0);

        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("insurance.date")), iCol++, 1);
        gridLayout.addComponent(dfPayStartDate, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label("Year"), iCol++, 1);
        gridLayout.addComponent(cbxInsuranceYear, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("startdate")), iCol++, 1);
        gridLayout.addComponent(dfStartDate, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("enddate")), iCol++, 1);
        gridLayout.addComponent(dfEndDate, iCol++, 1);

        gridLayoutPanel.addComponent(gridLayout);

        searchLayout.setMargin(true);
        searchLayout.setSpacing(true);
        searchLayout.addComponent(gridLayoutPanel);
        searchLayout.addComponent(buttonsLayout);

        Panel searchPanel = new Panel();
        searchPanel.setCaption(I18N.message("search"));
        searchPanel.setContent(searchLayout);

        this.columnDefinitions = createColumnDefinitions();
        pagedTable = new SimplePagedTable<>(this.columnDefinitions);
        pagedTable.setFooterVisible(true);
        pagedTable.setColumnFooter(FIRST_NAME_EN, I18N.message("total"));

        contentLayout.addComponent(searchPanel);
        contentLayout.addComponent(pagedTable);
        contentLayout.addComponent(pagedTable.createControls());

        tabSheet.addTab(contentLayout, I18N.message("lease.adjustments"));

        return tabSheet;
    }

    public void reset() {
        cbxDealer.setSelectedEntity(null);
        cbxDealerType.setSelectedEntity(null);
        txtContractReference.setValue("");
        dfStartDate.setValue(DateUtils.getDateAtBeginningOfMonth(DateUtils.todayH00M00S00()));
        dfEndDate.setValue(DateUtils.getDateAtEndOfMonth(DateUtils.todayH00M00S00()));
        cbxContractStatus.setSelectedEntity(null);
        dfPayStartDate.setValue(null);
        cbxInsuranceYear.setSelectedEntity(null);
    }

    private void search() {
        if (isValid()) {
            List<LeaseAdjustment> leaseAdjustments = accountingService
                    .getLeaseAdjustments(cbxDealerType.getSelectedEntity(), cbxDealer.getSelectedEntity(),
                            cbxContractStatus.getSelectedEntity(), txtContractReference.getValue(),
                            dfStartDate.getValue(), dfEndDate.getValue(), dfPayStartDate.getValue(),
                            cbxInsuranceYear.getSelectedEntity());
            if (leaseAdjustments != null) {
                setIndexedContainer(leaseAdjustments);
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void setIndexedContainer(List<LeaseAdjustment> leaseAdjustments) {
        Container.Indexed indexedContainer = pagedTable.getContainerDataSource();
        indexedContainer.removeAllItems();
        for (LeaseAdjustment leaseAdjustment : leaseAdjustments) {
            if (leaseAdjustment.getInsuranceStartDate() != null) {
                item(leaseAdjustment, indexedContainer);
            }
        }
        pagedTable.refreshContainerDataSource();
    }

    protected List<ColumnDefinition> createColumnDefinitions() {
        columnDefinitions = new ArrayList<>();
        columnDefinitions.add(new ColumnDefinition(ID, I18N.message("id").toUpperCase(), Long.class, Table.Align.LEFT, 70));
        columnDefinitions.add(new ColumnDefinition(REFERENCE, I18N.message("contract.reference").toUpperCase(), String.class, Table.Align.LEFT, 140));
        columnDefinitions.add(new ColumnDefinition(LAST_NAME_EN, I18N.message("lastname.en").toUpperCase(), String.class, Table.Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition(FIRST_NAME_EN, I18N.message("firstname.en").toUpperCase(), String.class, Table.Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition(CONTRACT_STATUS, I18N.message("contract.status").toUpperCase(), String.class, Table.Align.LEFT, 120));
        columnDefinitions.add(new ColumnDefinition(START_DATE, I18N.message("contract.start.date").toUpperCase(), Date.class, Table.Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("chassis.number", I18N.message("chassis.number").toUpperCase(), String.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition("engine.number", I18N.message("engine.number").toUpperCase(), String.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition("asset.price", I18N.message("asset.price").toUpperCase(), Double.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition(INSURANCE_START_DATE, I18N.message("insurance.start.date").toUpperCase(), Date.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition(INSURANCE_END_DATE, I18N.message("insurance.end.date").toUpperCase(), Date.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition(TERM, I18N.message("term").toUpperCase(), Integer.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition("contractYear", I18N.message("contract.year").toUpperCase(), Integer.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition("firstInstallmentDate", I18N.message("first.payment.date").toUpperCase(), Date.class, Table.Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("changeStatusDate", I18N.message("change.status.date").toUpperCase(), Date.class, Table.Align.LEFT, 100));
        return columnDefinitions;
    }

    public boolean isValid() {
        removeErrorsPanel();
        checkMandatoryDateField(dfStartDate, "startdate");
        checkMandatoryDateField(dfEndDate, "enddate");
        if (!errors.isEmpty()) {
            displayErrorsPanel();
        }
        return errors.isEmpty();
    }

    public void refresh() {
        search();
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }

    public Item item(LeaseAdjustment leaseAdjustment, Container.Indexed indexedContainer) {
        if (leaseAdjustment.getInsuranceStartDate() != null) {
            Item item = indexedContainer.addItem(getObject(leaseAdjustment.getId()));
            item.getItemProperty(ID).setValue(getObject(leaseAdjustment.getQuotationID()));
            item.getItemProperty(REFERENCE).setValue(getObject(leaseAdjustment.getReference()));
            item.getItemProperty(LAST_NAME_EN).setValue(getObject(leaseAdjustment.getLastNameEn()));
            item.getItemProperty(FIRST_NAME_EN).setValue(getObject(leaseAdjustment.getFirstNameEn()));
            item.getItemProperty(CONTRACT_STATUS).setValue(leaseAdjustment.getContractStatus().getDesc());
            item.getItemProperty(START_DATE).setValue(leaseAdjustment.getContractStartDate());
            item.getItemProperty("chassis.number").setValue(getObject(leaseAdjustment.getChassisNumber()));
            item.getItemProperty("engine.number").setValue(getObject(leaseAdjustment.getEngineNumber()));
            item.getItemProperty("asset.price").setValue(getObject(leaseAdjustment.getAssetPrice()));
            item.getItemProperty(INSURANCE_START_DATE).setValue(getObject(leaseAdjustment.getInsuranceStartDate()));
            item.getItemProperty(INSURANCE_END_DATE).setValue(getObject(leaseAdjustment.getInsuranceEndYear()));
            item.getItemProperty(TERM).setValue(getObject(leaseAdjustment.getTerm()));
            item.getItemProperty("contractYear").setValue(getObject(leaseAdjustment.getNoYearInsurance()));
            item.getItemProperty("firstInstallmentDate").setValue(getObject(leaseAdjustment.getFirstInstallmentDate()));
            item.getItemProperty("changeStatusDate").setValue(getObject(leaseAdjustment.getChangeStatusDate()));
            return item;
        }
        return null;
    }

    public Object getObject(Object object) {
        if (object != null) {
            return object;
        }
        return null;
    }
}
