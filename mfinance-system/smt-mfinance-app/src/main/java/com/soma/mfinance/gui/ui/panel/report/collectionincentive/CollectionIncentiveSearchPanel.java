package com.soma.mfinance.gui.ui.panel.report.collectionincentive;

import com.soma.mfinance.core.collection.model.CollectionIncentiveReport;
import com.soma.mfinance.core.collection.model.EColTask;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.shared.system.FMProfile;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.mfinance.core.widget.SecUserComboBox;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.tools.DateUtils;

import java.util.List;

import static com.soma.mfinance.core.helper.FinServicesHelper.DEA_SRV;

/**
 * Search panel for Collection Incentive Report
 *
 * @author kimsuor.seang
 */
public class CollectionIncentiveSearchPanel extends AbstractSearchPanel<CollectionIncentiveReport> {

    private static final long serialVersionUID = -4527788882260581871L;

    private ValueChangeListener valueChangeListener;
    private ERefDataComboBox<EDealerType> cbxDealerType;
    private DealerComboBox cbxDealer;
    private SecUserComboBox cbxCollectionOfficer;
    private ERefDataComboBox<EColTask> cbxCollectionTask;
    private AutoDateField dfStartDate;
    private AutoDateField dfEndDate;

    public CollectionIncentiveSearchPanel(CollectionIncentiveTablePanel tablePanel) {
        super(I18N.message("search"), tablePanel);
    }

    @Override
    protected Component createForm() {
        cbxDealer = new DealerComboBox(I18N.message("dealer"), ENTITY_SRV.list(DEA_SRV.getBaseRestrictionsDealer()), I18N.message("all"));
        cbxDealer.setSelectedEntity(null);
        cbxDealer.setWidth("220px");
        List<EDealerType> dealerTypes = EDealerType.values();
        cbxDealerType = new ERefDataComboBox<>(dealerTypes);
        cbxDealerType.setCaption(I18N.message("dealer.type"));
        cbxDealerType.setImmediate(true);
        cbxDealerType.setWidth("220px");
        valueChangeListener = new ValueChangeListener() {
            private static final long serialVersionUID = -7442302732430560056L;

            @Override
            public void valueChange(ValueChangeEvent event) {
                cbxDealer.setDealers(ENTITY_SRV.list(DEA_SRV.getRestrictionsDealerByDealerType(cbxDealerType.getSelectedEntity())));
                cbxDealer.setSelectedEntity(null);
            }
        };
        cbxDealerType.addValueChangeListener(valueChangeListener);
        cbxCollectionTask = new ERefDataComboBox<>(I18N.message("collection.task"), EColTask.class);
        cbxCollectionOfficer = new SecUserComboBox(DataReference.getInstance().getUsers(FMProfile.CC, EStatusRecord.ACTIV));
        cbxCollectionOfficer.setCaption(I18N.message("collection.officer"));
        dfStartDate = ComponentFactory.getAutoDateField("StartDate", false);
        dfStartDate.setValue(DateUtils.todayH00M00S00());
        dfStartDate.setWidth("95px");
        dfEndDate = ComponentFactory.getAutoDateField("EndDate", false);
        dfEndDate.setValue(DateUtils.todayH00M00S00());
        dfEndDate.setWidth("95px");
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);
        FormLayout formLayout = new FormLayout();
        formLayout.addComponent(cbxDealerType);
        formLayout.addComponent(cbxDealer);
        horizontalLayout.addComponent(formLayout);
        formLayout = new FormLayout();
        formLayout.addComponent(cbxCollectionOfficer);
        formLayout.addComponent(cbxCollectionTask);
        horizontalLayout.addComponent(formLayout);
        formLayout = new FormLayout();
        formLayout.addComponent(dfStartDate);
        horizontalLayout.addComponent(formLayout);
        formLayout = new FormLayout();
        formLayout.addComponent(dfEndDate);
        horizontalLayout.addComponent(formLayout);
        return horizontalLayout;
    }

    @Override
    public BaseRestrictions<CollectionIncentiveReport> getRestrictions() {
        BaseRestrictions<CollectionIncentiveReport> restrictions = new BaseRestrictions<>(CollectionIncentiveReport.class);
        restrictions.addAssociation("collection", "col", JoinType.INNER_JOIN);
        restrictions.addAssociation("col.contract", "con", JoinType.INNER_JOIN);
        if (cbxDealerType.getSelectedEntity() != null) {
            restrictions.addAssociation("con.dealer", "indeal", JoinType.INNER_JOIN);
            restrictions.addCriterion(Restrictions.eq("indeal.dealerType", cbxDealerType.getSelectedEntity()));
        }
        if (cbxDealer.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq("con.dealer", cbxDealer.getSelectedEntity()));
        }
        if (dfStartDate.getValue() != null ) {
            restrictions.addCriterion(Restrictions.ge("date", DateUtils.getDateAtBeginningOfDay(dfStartDate.getValue())));
        }
        if (dfEndDate.getValue() != null) {
            restrictions.addCriterion(Restrictions.le("date", DateUtils.getDateAtEndOfDay(dfEndDate.getValue())));
        }
        /*if (cbxCollectionOfficer.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.and(
                    Restrictions.isNotNull("collectionOfficer"),
                    Restrictions.eq("collectionOfficer.id", cbxCollectionOfficer.getSelectedEntity().getId())
            ));
        }*/
        if (cbxCollectionTask.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.and(
                    Restrictions.isNotNull("collectionTask"),
                    Restrictions.eq("collectionTask", cbxCollectionTask.getSelectedEntity())
            ));
        }
        restrictions.addOrder(Order.asc("date"));
        return restrictions;
    }

    @Override
    protected void reset() {
        cbxDealerType.setSelectedEntity(null);
        cbxDealer.setSelectedEntity(null);
        cbxCollectionOfficer.setSelectedEntity(null);
        cbxCollectionTask.setSelectedEntity(null);
    }
}
