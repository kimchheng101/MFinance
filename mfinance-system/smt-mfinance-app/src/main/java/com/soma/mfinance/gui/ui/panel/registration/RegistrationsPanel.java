package com.soma.mfinance.gui.ui.panel.registration;

import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.asset.AssetEntityField;
import com.soma.mfinance.core.shared.report.ReportUtils;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import org.apache.commons.lang.StringUtils;
import org.seuksa.frmk.service.EntityService;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

import static com.soma.mfinance.gui.ui.panel.registration.RegistrationFormPanel.reportMenu;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(RegistrationsPanel.NAME)
public class RegistrationsPanel extends AbstractTabsheetPanel implements View, AssetEntityField{
	public static final String NAME = "registration";

	private static final long serialVersionUID = 7840786221078742948L;
	private EntityService entityService = SpringUtils.getBean(EntityService.class);
	
	@Autowired
	private RegistrationTablePanel registrationTablePanel;
	@Autowired
	private RegistrationFormPanel registrationFormPanel;

	/**
	 * @return the registrationFormPanel
	 */
	public RegistrationFormPanel getRegistrationFormPanel() {
		return registrationFormPanel;
	}
	@PostConstruct
	public void PostConstruct() {
		super.init();
		registrationTablePanel.setMainPanel(this);

		registrationFormPanel.setRegistrationsPanel(this);
		getTabSheet().setTablePanel(registrationTablePanel);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		String quotaId = event.getParameters();
		if (StringUtils.isNotEmpty(quotaId)) {
			getTabSheet().addFormPanel(registrationFormPanel);
			registrationFormPanel.assignValues(new Long(quotaId), true);
			getTabSheet().setForceSelected(true);
			getTabSheet().setSelectedTab(registrationFormPanel);
		}
		
	}

	@Override
	public void onAddEventClick() {

	}

	@Override
	public void onEditEventClick() {

		SecUser secUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Quotation quotation = entityService.getById(Quotation.class, registrationTablePanel.getItemSelectedId());
		registrationFormPanel.reset();
		ReportUtils.buildMenuReport(quotation, reportMenu);

		if(ProfileUtil.isUnderwriter() || ProfileUtil.isUnderwriterSupervisor()){
			if(quotation.getSecUser() == null || quotation.getSecUser().equals(secUser)){
				getTabSheet().addFormPanel(registrationFormPanel);
				initSelectedTab(registrationFormPanel);
				registrationFormPanel.setLockOrUnlockProcess(quotation);
			}else{
				registrationFormPanel.showProcessMessage(quotation);
			}
		}else{
			getTabSheet().addFormPanel(registrationFormPanel);
			initSelectedTab(registrationFormPanel);
		}
		
	}
	
	@Override
	public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
		if (selectedTab == registrationFormPanel) {
			registrationFormPanel.assignValues(registrationTablePanel.getItemSelectedId(), true);
		} else if (selectedTab == registrationTablePanel && getTabSheet().isNeedRefresh()) {
			registrationTablePanel.refresh();
		}
		getTabSheet().setSelectedTab(selectedTab);
	}

}
