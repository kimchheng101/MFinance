package com.soma.mfinance.gui.ui.panel.registration;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.common.security.model.SecUserDetail;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.financial.model.FinProduct;
import com.soma.mfinance.core.quotation.QuotationService;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.registrations.model.ProfileContractStatus;
import com.soma.mfinance.core.registrations.model.RegistrationStatus;
import com.soma.mfinance.core.registrations.model.RegistrationStorageLocation;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.ersys.core.hr.model.address.Province;
import com.soma.frmk.security.context.SecApplicationContextHolder;
import com.soma.frmk.security.model.SecProfile;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.*;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.service.EntityService;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.List;

import static com.soma.mfinance.core.helper.FinServicesHelper.FIN_PROD_SRV;

/**
 *
 * @author Riya.Pov
 */
public class RegistrationSearchPanel extends AbstractSearchPanel<Quotation> implements QuotationEntityField {

private QuotationService quotationService = (QuotationService) SecApplicationContextHolder.getContext().getBean("quotationService");

	private TextField txtReference;
	private TextField txtFirstNameEn;
	private TextField txtLastNameEn;
	private TextField txtChassisNumber;
	private TextField txtEngineNumber;
	private TextField txtPlateNumber;
	private AutoDateField dfStartDate;
	private AutoDateField dfEndDate;
	private AutoDateField dfRegistrationDeadLine;
	private ERefDataComboBox<EDealerType> cbxDealerType;
	private EntityRefComboBox<Province> cbxProvince;
	private EntityRefComboBox<RegistrationStorageLocation> cbxRegistrationLocation;
	private EntityComboBox<ProfileContractStatus> cbxContractStatus;
	private EntityRefComboBox<RegistrationStatus> cbxRegistrationStatus;
	private EntityRefComboBox<RegistrationStatus> cbxRegistrationStatus2;
	private EntityRefComboBox<RegistrationStatus> cbxRegistrationStatus3;
	private EntityRefComboBox<RegistrationStatus> cbxRegistrationStatus4;
	private DealerComboBox cbxDealer;
	private SecUserDetail usrDetail;
	private ValueChangeListener valueChangeListener;

	private EntityRefComboBox<FinProduct> cbxFinancialProduct;

	/**
	 *
	 * @param registrationTablePanel
	 */
	public RegistrationSearchPanel(RegistrationTablePanel registrationTablePanel) {
		super(I18N.message("registration.search"), registrationTablePanel);
	}

	/**
	 *
	 */
	private static final long serialVersionUID = 8123058073975294567L;

	@Override
	protected com.vaadin.ui.Component createForm() {
		txtReference = ComponentFactory.getTextField(I18N.message("reference"), false, 60, 220);
		txtFirstNameEn = ComponentFactory.getTextField(I18N.message("firstname.en"), false, 60, 220);
		txtLastNameEn = ComponentFactory.getTextField(I18N.message("lastname.en"), false, 60, 220);
		txtChassisNumber = ComponentFactory.getTextField(I18N.message("chassis.number"), false, 50, 220);
		txtEngineNumber = ComponentFactory.getTextField(I18N.message("engine.number"), false, 50, 220);
		txtPlateNumber = ComponentFactory.getTextField(I18N.message("plate.number"), false, 50, 220);
		dfStartDate = ComponentFactory.getAutoDateField(I18N.message("startdate"), false);
		dfEndDate = ComponentFactory.getAutoDateField(I18N.message("enddate"), false);
		dfRegistrationDeadLine = ComponentFactory.getAutoDateField(I18N.message("registration.deadline"), false);

		SecUser secUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		usrDetail = ENTITY_SRV.getByField(SecUserDetail.class, "secUser.id", secUser.getId());

		cbxDealerType = new ERefDataComboBox<EDealerType>(I18N.message("dealer.type"), EDealerType.class);
		cbxDealerType.setImmediate(true);
		cbxDealerType.setWidth("220px");
		valueChangeListener = new ValueChangeListener() {
			/** */
			private static final long serialVersionUID = 5719856916200703515L;
			@Override
			public void valueChange(ValueChangeEvent event) {
				BaseRestrictions<Dealer> restrictions = new BaseRestrictions<Dealer>(Dealer.class);
				restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
				if (cbxDealerType.getSelectedEntity() != null) {
					restrictions.addCriterion(Restrictions.eq("dealerType", cbxDealerType.getSelectedEntity()));
				}
				EntityService ent = SpringUtils.getBean(EntityService.class);
				cbxDealer.setDealers(ent.list(restrictions));
				cbxDealer.setSelectedEntity(null);
			}
		};
		cbxDealerType.addValueChangeListener(valueChangeListener);

		SecUser user = ProfileUtil.getCurrentUser();
		BaseRestrictions<ProfileContractStatus> restrictions = new BaseRestrictions<>(ProfileContractStatus.class);
		restrictions.addCriterion("profile",user.getDefaultProfile());
		List<ProfileContractStatus> list = ENTITY_SRV.list(restrictions);

		cbxContractStatus = new EntityComboBox<>(ProfileContractStatus.class, "wkfStatus.descEn");
		cbxContractStatus.setCaption(I18N.message("contract.status"));
		cbxContractStatus.setEntities(list);
		cbxContractStatus.setImmediate(true);
		cbxContractStatus.setWidth("220px");

		cbxRegistrationStatus = new EntityRefComboBox<RegistrationStatus>(I18N.message("registration.status"));
		cbxRegistrationStatus.setRestrictions(new BaseRestrictions<RegistrationStatus>(RegistrationStatus.class));
		cbxRegistrationStatus.renderer();
		cbxRegistrationStatus.setImmediate(true);
		cbxRegistrationStatus.setWidth("220px");

		cbxRegistrationStatus2 = new EntityRefComboBox<RegistrationStatus>(I18N.message("registration.status"));
		cbxRegistrationStatus2.setRestrictions(new BaseRestrictions<RegistrationStatus>(RegistrationStatus.class));
		cbxRegistrationStatus2.renderer();
		cbxRegistrationStatus2.setImmediate(true);
		cbxRegistrationStatus2.setWidth("220px");

		cbxRegistrationStatus3 = new EntityRefComboBox<RegistrationStatus>(I18N.message("registration.status"));
		cbxRegistrationStatus3.setRestrictions(new BaseRestrictions<RegistrationStatus>(RegistrationStatus.class));
		cbxRegistrationStatus3.renderer();
		cbxRegistrationStatus3.setImmediate(true);
		cbxRegistrationStatus3.setWidth("220px");

		cbxRegistrationStatus4 = new EntityRefComboBox<RegistrationStatus>(I18N.message("registration.status"));
		cbxRegistrationStatus4.setRestrictions(new BaseRestrictions<RegistrationStatus>(RegistrationStatus.class));
		cbxRegistrationStatus4.renderer();
		cbxRegistrationStatus4.setImmediate(true);
		cbxRegistrationStatus4.setWidth("220px");


		cbxRegistrationLocation = new EntityRefComboBox<RegistrationStorageLocation>(I18N.message("registration.location"));
		cbxRegistrationLocation.setRestrictions(new BaseRestrictions<RegistrationStorageLocation>(RegistrationStorageLocation.class));
		cbxRegistrationLocation.renderer();
		cbxRegistrationLocation.setImmediate(true);
		cbxRegistrationLocation.setWidth("220px");

        BaseRestrictions<Dealer> resCon = new BaseRestrictions<Dealer>(Dealer.class);
		resCon.getStatusRecordList().add(EStatusRecord.ACTIV);
        cbxDealer = new DealerComboBox(I18N.message("dealer"), ENTITY_SRV.list(resCon));
        if (ProfileUtil.isCreditOfficer() || ProfileUtil.isProductionOfficer()) {
        	cbxDealer.setEnabled(false);
        	cbxDealerType.setEnabled(false);
        }
        if(ProfileUtil.isCreditOfficerMovable()){
        	cbxDealer.setEnabled(true);
        	cbxDealerType.setEnabled(true);
        }

        cbxDealer.setImmediate(true);
		cbxDealer.setWidth("220px");
		if (ProfileUtil.isPOS() && usrDetail != null && usrDetail.getDealer() != null) {
			cbxDealer.setSelectedEntity(usrDetail.getDealer());
			cbxDealerType.removeValueChangeListener(valueChangeListener);
			cbxDealerType.setSelectedEntity(cbxDealer.getSelectedEntity() != null ? cbxDealer.getSelectedEntity().getDealerType() : null);
			cbxDealerType.addValueChangeListener(valueChangeListener);
		}

        cbxProvince = new EntityRefComboBox<Province>(I18N.message("province"));
		cbxProvince.setRestrictions(new BaseRestrictions<Province>(Province.class));
		cbxProvince.renderer();
		cbxProvince.setImmediate(true);
		cbxProvince.setWidth("220px");

		cbxFinancialProduct = new EntityRefComboBox<>(I18N.message("financial.product"));
		cbxFinancialProduct.setWidth("220px");
		cbxFinancialProduct.setRestrictions(FIN_PROD_SRV.getFinProductRestrictions());
		cbxFinancialProduct.setImmediate(true);
		cbxFinancialProduct.renderer();
		cbxFinancialProduct.setSelectedEntity(null);

		final GridLayout gridLayout = new GridLayout(10, 4);

		FormLayout formLayoutLeft = new FormLayout();
		formLayoutLeft.addComponent(txtLastNameEn);
		formLayoutLeft.addComponent(cbxProvince);
		formLayoutLeft.addComponent(cbxRegistrationStatus);
		formLayoutLeft.addComponent(dfRegistrationDeadLine);


		FormLayout formLayoutMiddleLeft = new FormLayout();
		formLayoutMiddleLeft.addComponent(txtFirstNameEn);
		formLayoutMiddleLeft.addComponent(cbxDealerType);
		formLayoutMiddleLeft.addComponent(cbxRegistrationStatus2);
		formLayoutMiddleLeft.addComponent(dfStartDate);

		FormLayout formLayoutMiddle = new FormLayout();
		formLayoutMiddle.addComponent(txtReference);
		formLayoutMiddle.addComponent(cbxDealer);
		formLayoutMiddle.addComponent(cbxRegistrationStatus3);
		formLayoutMiddle.addComponent(dfEndDate);

		FormLayout formLayoutMiddleRight = new FormLayout();
		/*formLayoutMiddleRight.addComponent(txtChassisNumber);*/
		formLayoutMiddleRight.addComponent(txtPlateNumber);
		formLayoutMiddleRight.addComponent(cbxRegistrationStatus4);
		formLayoutMiddleRight.addComponent(cbxRegistrationLocation);

		FormLayout formLayoutRight = new FormLayout();
		/*formLayoutRight.addComponent(txtEngineNumber);*/
		formLayoutRight.addComponent(cbxFinancialProduct);
		formLayoutRight.addComponent(cbxContractStatus);

		int iCol = 0;
		gridLayout.addComponent(formLayoutLeft, iCol++, 0);
		gridLayout.addComponent(ComponentFactory.getSpaceLayout(20, Unit.PIXELS), iCol++, 0);
		gridLayout.addComponent(formLayoutMiddleLeft, iCol++, 0);
		gridLayout.addComponent(ComponentFactory.getSpaceLayout(20, Unit.PIXELS), iCol++, 0);
		gridLayout.addComponent(formLayoutMiddle, iCol++, 0);
		gridLayout.addComponent(ComponentFactory.getSpaceLayout(20, Unit.PIXELS), iCol++, 0);
		gridLayout.addComponent(formLayoutMiddleRight, iCol++, 0);
		gridLayout.addComponent(ComponentFactory.getSpaceLayout(20, Unit.PIXELS), iCol++, 0);
		gridLayout.addComponent(formLayoutRight, iCol++, 0);

		return gridLayout;
	}



	@Override
	protected void reset() {
		cbxProvince.setSelectedEntity(null);
		cbxRegistrationStatus.setSelectedEntity(null);
		cbxRegistrationStatus2.setSelectedEntity(null);
		cbxRegistrationStatus3.setSelectedEntity(null);
		cbxRegistrationStatus4.setSelectedEntity(null);
		cbxRegistrationLocation.setSelectedEntity(null);
		cbxContractStatus.setSelectedEntity(null);
		txtReference.setValue("");
		txtFirstNameEn.setValue("");
		txtLastNameEn.setValue("");
		txtChassisNumber.setValue("");
		txtEngineNumber.setValue("");
		dfStartDate.setValue(null);
		dfEndDate.setValue(null);
		cbxFinancialProduct.setSelectedEntity(null);
	}

	/**
	 * @see AbstractSearchPanel#getRestrictions()
	 */
	@Override
	public BaseRestrictions<Quotation> getRestrictions() {

		SecUser secUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Disjunction or = Restrictions.or();
		Conjunction and = Restrictions.and();

		BaseRestrictions<Quotation> restrictions = new BaseRestrictions<Quotation>(Quotation.class);

		restrictions.addAssociation("asset", "qouasset", JoinType.INNER_JOIN);

		if (StringUtils.isNotEmpty(txtReference.getValue())) {
			restrictions.addCriterion(Restrictions.like(REFERENCE, txtReference.getValue(), MatchMode.ANYWHERE));
		}

		if(cbxContractStatus.getSelectedEntity() != null){
			restrictions.addAssociation("contract", "cotra", JoinType.LEFT_OUTER_JOIN);
			if(cbxContractStatus.getSelectedEntity().getWkfStatus().equals(QuotationWkfStatus.ACT)){
				restrictions.addCriterion(Restrictions.eq("cotra."+ WKF_STATUS, ContractWkfStatus.FIN));
			} else if(cbxContractStatus.getSelectedEntity().getWkfStatus().equals(QuotationWkfStatus.APV)
					||cbxContractStatus.getSelectedEntity().getWkfStatus().equals(QuotationWkfStatus.PPO)
					||cbxContractStatus.getSelectedEntity().getWkfStatus().equals(QuotationWkfStatus.WIV)
					||cbxContractStatus.getSelectedEntity().getWkfStatus().equals(QuotationWkfStatus.WCA)
					){

				restrictions.addCriterion(Restrictions.eq(WKF_STATUS, cbxContractStatus.getSelectedEntity().getWkfStatus()));
			}else {
				restrictions.addCriterion(Restrictions.eq("cotra."+ WKF_STATUS, cbxContractStatus.getSelectedEntity().getWkfStatus()));
			}
		}else if (!cbxContractStatus.getValueMap().values().isEmpty()){

			SecUser user = ProfileUtil.getCurrentUser();
			List<EWkfStatus> defaultContractStatusList = getDefaultContractStatus(user.getDefaultProfile());
			restrictions.addCriterion(Restrictions.in(WKF_STATUS, defaultContractStatusList));

		}else {
			restrictions.addCriterion(Restrictions.isNull(WKF_STATUS));
		}

		if (cbxRegistrationStatus.getSelectedEntity() != null
				|| cbxRegistrationStatus2.getSelectedEntity() != null
				|| cbxRegistrationStatus3.getSelectedEntity() != null
				|| cbxRegistrationStatus4.getSelectedEntity() != null) {
		Disjunction orJunction = Restrictions.or();

		if(cbxRegistrationStatus.getSelectedEntity() != null){
			orJunction.add(Restrictions.eq("registrationStatus.id", cbxRegistrationStatus.getSelectedEntity().getId()));
		}
		if(cbxRegistrationStatus2.getSelectedEntity() != null){
			orJunction.add(Restrictions.eq("registrationStatus.id", cbxRegistrationStatus2.getSelectedEntity().getId()));
		}
		if(cbxRegistrationStatus3.getSelectedEntity() != null){
			orJunction.add(Restrictions.eq("registrationStatus.id", cbxRegistrationStatus3.getSelectedEntity().getId()));
		}
		if(cbxRegistrationStatus4.getSelectedEntity() != null){
			orJunction.add(Restrictions.eq("registrationStatus.id", cbxRegistrationStatus4.getSelectedEntity().getId()));
		}
		restrictions.addCriterion(orJunction);
		}

		if(cbxRegistrationLocation.getSelectedEntity() != null){
			restrictions.addCriterion(Restrictions.eq("registrationStorageLocation.id", cbxRegistrationLocation.getSelectedEntity().getId()));
		}

		if (cbxProvince.getSelectedEntity() != null || StringUtils.isNotEmpty(txtLastNameEn.getValue())
				|| StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
			restrictions.addAssociation("quotationApplicants", "quoapp", JoinType.INNER_JOIN);
			restrictions.addAssociation("quoapp.applicant", "app", JoinType.INNER_JOIN);
			restrictions.addAssociation("app.individual", "ind", JoinType.INNER_JOIN);
			restrictions.addCriterion("quoapp.applicantType", EApplicantType.C);
		}

		if (cbxProvince.getSelectedEntity() != null) {
			restrictions.addAssociation("ind.individualAddresses", "appaddr", JoinType.INNER_JOIN);
			restrictions.addAssociation("appaddr.address", "addr", JoinType.INNER_JOIN);
			restrictions.addCriterion("addr.province.id", cbxProvince.getSelectedEntity().getId());
		}


		if (StringUtils.isNotEmpty(txtLastNameEn.getValue())) {
			restrictions.addCriterion(Restrictions.ilike("ind." + LAST_NAME_EN, txtLastNameEn.getValue(), MatchMode.ANYWHERE));
		}

		if (StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
			restrictions.addCriterion(Restrictions.ilike("ind." + FIRST_NAME_EN, txtFirstNameEn.getValue(), MatchMode.ANYWHERE));
		}

		/*if (StringUtils.isNotEmpty(txtChassisNumber.getValue())) {
			restrictions.addCriterion(Restrictions.ilike("qouasset.chassisNumber", txtChassisNumber.getValue(), MatchMode.ANYWHERE));
		}*/
		/*if (StringUtils.isNotEmpty(txtEngineNumber.getValue())) {
			restrictions.addCriterion(Restrictions.ilike("qouasset.engineNumber", txtEngineNumber.getValue(), MatchMode.ANYWHERE));
		}*/

		if(StringUtils.isNotEmpty(txtPlateNumber.getValue())){
			restrictions.addCriterion(Restrictions.ilike("qouasset.plateNumber", txtPlateNumber.getValue(), MatchMode.ANYWHERE));
		}

		if (cbxDealer.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, cbxDealer.getSelectedEntity().getId()));
		}


		Disjunction orJunction = Restrictions.or();
		Conjunction andJunction = Restrictions.and();
		/*if(isFilterByChangeStatusDate(cbxRegistrationStatus.getSelectedEntity()) ||
				isFilterByChangeStatusDate(cbxRegistrationStatus2.getSelectedEntity()) ||
				isFilterByChangeStatusDate(cbxRegistrationStatus3.getSelectedEntity()) ||
				isFilterByChangeStatusDate(cbxRegistrationStatus4.getSelectedEntity())){
			if(dfStartDate.getValue() != null){
				andJunction.add(Restrictions.ge("changeStatusDate", DateUtils.getDateAtBeginningOfDay(dfStartDate.getValue())));
			}
			if (dfEndDate.getValue() != null) {
				andJunction.add(Restrictions.le("changeStatusDate", DateUtils.getDateAtEndOfDay(dfEndDate.getValue())));
			}
		} else {
			if(dfStartDate.getValue() != null){
				andJunction.add(Restrictions.ge("contractStartDate", DateUtils.getDateAtBeginningOfDay(dfStartDate.getValue())));
			}
			if (dfEndDate.getValue() != null) {
				andJunction.add(Restrictions.le("contractStartDate", DateUtils.getDateAtEndOfDay(dfEndDate.getValue())));
			}
		}*/


		if(dfRegistrationDeadLine!=null && dfRegistrationDeadLine.getValue()!=null){
			restrictions.addCriterion(Restrictions.ge("registrationDeadline", DateUtils.getDateAtBeginningOfDay(dfRegistrationDeadLine.getValue())));
			restrictions.addCriterion(Restrictions.le("registrationDeadline", DateUtils.getDateAtEndOfDay(dfRegistrationDeadLine.getValue())));

		}

		if(dfStartDate.getValue() != null || dfEndDate.getValue() != null ){
		restrictions.addOrder(Order.desc("registrationDeadline"));
		}


		restrictions.addAssociation("dealer", "dea", JoinType.INNER_JOIN);
		if (cbxDealerType.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("dea.dealerType", cbxDealerType.getSelectedEntity()));
		}

		//restrictions.addOrder(Order.desc("changeStatusDate"));

		if(cbxFinancialProduct.getSelectedEntity() != null){
			restrictions.addCriterion(Restrictions.eq(FINANCIAL_PRODUCT, cbxFinancialProduct.getSelectedEntity()));
		}
		return restrictions;
	}

	private List<ProfileContractStatus> getProfileContractStatus(){
		BaseRestrictions<ProfileContractStatus> restrictions = new BaseRestrictions<ProfileContractStatus>(ProfileContractStatus.class);
		return quotationService.list(restrictions);
	}
	private List<EWkfStatus> getDefaultContractStatus(SecProfile profile) {
		List<EWkfStatus> quotationStatusList = new ArrayList<EWkfStatus>();
		List<ProfileContractStatus> profileContractStatusList = getProfileContractStatus();
		if (profileContractStatusList != null && !profileContractStatusList.isEmpty()) {
			for (ProfileContractStatus profileContractStatus : profileContractStatusList) {
				if (profile.getId().equals(profileContractStatus.getProfile().getId())) {
					quotationStatusList.add(profileContractStatus.getWkfStatus());
				}
			}
		}
		return quotationStatusList;
	}

	public List<String> getMessageErrorBeforeSearching() {
		List<String> errMsg = new ArrayList<String>();
		if(ProfileUtil.isDocumentController()) {
			if (cbxRegistrationStatus.getSelectedEntity() == null
					&& cbxRegistrationStatus2.getSelectedEntity() == null
					&& cbxRegistrationStatus3.getSelectedEntity() == null
					&& cbxRegistrationStatus4.getSelectedEntity() == null
					&& cbxRegistrationLocation.getSelectedEntity() == null) {
				String messageFields = I18N.message("registration.status") + " , " + I18N.message("registration.location");
				errMsg.add(I18N.message("please.enter.one.there.searching.criteria", new String[] { messageFields}));
			}
		}
		return errMsg;
	}
	/**
	 *
	 * @param registrationStatus
	 * @return
	 */
	private boolean isFilterByChangeStatusDate(
			RegistrationStatus registrationStatus) {
		List<RegistrationStatus> registrationStatuses = new ArrayList<RegistrationStatus>();
		RegistrationStatus registrationStatus18 = quotationService.getById(RegistrationStatus.class, new Long(27));
		RegistrationStatus registrationStatus17 = quotationService.getById(RegistrationStatus.class, new Long(24));
		if (registrationStatus18 != null) {
			registrationStatuses.add(registrationStatus18);
		}
		if (registrationStatus17 != null) {
			registrationStatuses.add(registrationStatus17);
		}
		if (registrationStatus != null) {
			return registrationStatuses.contains(registrationStatus);
		}
		return false;
	}
}
