package com.soma.mfinance.gui.ui.panel.report.last.payment.contract;


import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.financial.model.FinService;
import com.soma.mfinance.core.quotation.model.EInstallmentType;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Table.Align;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.model.meta.NativeColumn;
import org.seuksa.frmk.model.meta.NativeRow;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.exception.NativeQueryException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.soma.mfinance.core.quotation.model.EInstallmentType.getEInstallmentTypeValue;

;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(LastPaymentContractReportPanel.NAME)
public class LastPaymentContractReportPanel extends AbstractTabPanel implements View, QuotationEntityField {



	private static final long serialVersionUID = 6717468559252118258L;

	public static final String NAME = "last.payment.contract";

	private TabSheet tabSheet;
	private SimplePagedTable<Contract> pagedTable;
	private List<ColumnDefinition> columnDefinitions;
	private ERefDataComboBox<EDealerType> cbxDealerType;
	private ERefDataComboBox<EInstallmentType> cbxInstallmentType;
	private DealerComboBox cbxDealer;
	private TextField txtReference;
	private AutoDateField dfStartDate;
	private AutoDateField dfEndDate;
	private TextField txtFirstNameEn;
	private TextField txtLastNameEn;
	private FinService serviceVatINSFEE;
	private FinService serviceVatSERFEE;
	private ERefDataComboBox<EWkfStatus> cbxContractStatus;

	public LastPaymentContractReportPanel() {
		super();
		setSizeFull();
	}

	@Override
	protected com.vaadin.ui.Component createForm() {
		tabSheet = new TabSheet();
		VerticalLayout contentLayout = new VerticalLayout();
		contentLayout.setSpacing(true);
		contentLayout.setMargin(true);

		VerticalLayout gridLayoutPanel = new VerticalLayout();
		VerticalLayout searchLayout = new VerticalLayout();
		HorizontalLayout buttonsLayout = new HorizontalLayout();
		Button btnSearch = new Button(I18N.message("search"));
		btnSearch.setIcon(new ThemeResource("../smt-default/icons/16/search.png"));
		/**
		 * USAGE LAMBDA
		 */
		btnSearch.addClickListener((ClickListener) event -> search());

		Button btnReset = new Button(I18N.message("reset"));
		/**
		 * USAGE LAMBDA
		 */
		btnReset.addClickListener((ClickListener) event -> reset());
		buttonsLayout.setSpacing(true);
		buttonsLayout.addComponent(btnSearch);
		buttonsLayout.addComponent(btnReset);
		final GridLayout gridLayout = new GridLayout(15, 3);
		gridLayout.setSpacing(true);
		cbxDealer = new DealerComboBox(null, DataReference.getInstance().getDealers(), I18N.message("all"));
		cbxDealer.setSelectedEntity(null);
		cbxDealer.setWidth("150px");

		cbxDealerType = new ERefDataComboBox<EDealerType>(EDealerType.values());
		cbxDealerType.setImmediate(true);
		/**
		 * USAGE LAMBDA
		 */
		cbxDealerType.addValueChangeListener((ValueChangeListener) event -> {
            BaseRestrictions<Dealer> restrictions = new BaseRestrictions<>(Dealer.class);
            restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
            if (cbxDealerType.getSelectedEntity() != null) {
                restrictions.addCriterion(Restrictions.eq("dealerType", cbxDealerType.getSelectedEntity()));
            }
            cbxDealer.setDealers(ENTITY_SRV.list(restrictions));
            cbxDealer.setSelectedEntity(null);
        });
		cbxInstallmentType = new ERefDataComboBox<>(getEInstallmentTypeValue());
		cbxInstallmentType.setSelectedEntity(EInstallmentType.LAST_INSTALLMENT);

		cbxContractStatus = new ERefDataComboBox<>(ContractWkfStatus.listSummaryStatus());
		
		txtReference = ComponentFactory.getTextField(false, 20, 150);
		txtFirstNameEn = ComponentFactory.getTextField(false, 20, 150);
		txtLastNameEn = ComponentFactory.getTextField(false, 20, 150);
		dfStartDate = ComponentFactory.getAutoDateField("",false);
		dfStartDate.setValue(DateUtils.todayH00M00S00());
		dfEndDate = ComponentFactory.getAutoDateField("", false);
		dfEndDate.setValue(DateUtils.todayH00M00S00());

		int iCol = 0;
		gridLayout.addComponent(new Label(I18N.message("lastname.en")), iCol++, 0);
		gridLayout.addComponent(txtLastNameEn, iCol++, 0);
		gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
		gridLayout.addComponent(new Label(I18N.message("firstname.en")), iCol++, 0);
		gridLayout.addComponent(txtFirstNameEn, iCol++, 0);
		gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
		gridLayout.addComponent(new Label(I18N.message("reference")), iCol++, 0);
		gridLayout.addComponent(txtReference, iCol++, 0);
		gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
		gridLayout.addComponent(new Label("Installment Type"), iCol++, 0);
		gridLayout.addComponent(cbxInstallmentType, iCol++, 0);
		gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);

		iCol = 0;
		gridLayout.addComponent(new Label(I18N.message("dealer")), iCol++, 1);
		gridLayout.addComponent(cbxDealer, iCol++, 1);
		gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
		gridLayout.addComponent(new Label(I18N.message("start.due.date")), iCol++, 1);
		gridLayout.addComponent(dfStartDate, iCol++, 1);
		gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
		gridLayout.addComponent(new Label(I18N.message("end.due.date")), iCol++, 1);
		gridLayout.addComponent(dfEndDate, iCol++, 1);
		gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
		gridLayout.addComponent(new Label(I18N.message("contract.status")), iCol++, 1);
		gridLayout.addComponent(cbxContractStatus, iCol++, 1);
		gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);

		gridLayoutPanel.addComponent(gridLayout);

		searchLayout.setMargin(true);
		searchLayout.setSpacing(true);
		searchLayout.addComponent(gridLayoutPanel);
		searchLayout.addComponent(buttonsLayout);

		Panel searchPanel = new Panel();
		searchPanel.setCaption(I18N.message("search"));
		searchPanel.setContent(searchLayout);

		this.columnDefinitions = createColumnDefinitions();
		pagedTable = new SimplePagedTable<>(this.columnDefinitions);
		pagedTable.setFooterVisible(true);
		pagedTable.setColumnFooter(FIRST_NAME_EN, I18N.message("total"));

		contentLayout.addComponent(searchPanel);
		contentLayout.addComponent(pagedTable);
		contentLayout.addComponent(pagedTable.createControls());

		tabSheet.addTab(contentLayout, I18N.message("last.payment.contract"));
		return tabSheet;
	}	

	public void reset() {
		cbxDealerType.setSelectedEntity(null);
		txtReference.setValue("");
		dfStartDate.setValue(DateUtils.todayH00M00S00());
		dfEndDate.setValue(DateUtils.todayH00M00S00());
		cbxContractStatus.setSelectedEntity(null);
	}

	private void search() {
		List<LastPaymentInfoDTO> lastPaymentInfoVOs = getContracts();
		setIndexedContainer(lastPaymentInfoVOs);
	}

	private List<LastPaymentInfoDTO> getContracts() {
		List<LastPaymentInfoDTO> lastPaymentInfoVOs = new ArrayList<>();
			Date startDate1 = DateUtils.getDateAtBeginningOfDay(DateUtils.addMonthsDate(dfStartDate.getValue(), -11));
			Date endDate1 = DateUtils.getDateAtEndOfDay(DateUtils.addMonthsDate(dfEndDate.getValue(), -11));
			Date startDate2 = DateUtils.getDateAtBeginningOfDay(DateUtils.addMonthsDate(dfStartDate.getValue(), -23));
			Date endDate2 = DateUtils.getDateAtEndOfDay(DateUtils.addMonthsDate(dfEndDate.getValue(), -23));
			Date startDate3 = DateUtils.getDateAtBeginningOfDay(DateUtils.addMonthsDate(dfStartDate.getValue(), -35));
			Date endDate3 = DateUtils.getDateAtEndOfDay(DateUtils.addMonthsDate(dfEndDate.getValue(), -35));
			Date startDate4 = DateUtils.getDateAtBeginningOfDay(DateUtils.addMonthsDate(dfStartDate.getValue(), -47));
			Date endDate4 = DateUtils.getDateAtEndOfDay(DateUtils.addMonthsDate(dfEndDate.getValue(), -47));
			Date startDate5 = DateUtils.getDateAtBeginningOfDay(DateUtils.addMonthsDate(dfStartDate.getValue(), -59));
			Date endDate5 = DateUtils.getDateAtEndOfDay(DateUtils.addMonthsDate(dfEndDate.getValue(), -59));

		String gOneSql = "SELECT "+
					" tdq.quo_id,"+
					" tdq.quo_va_reference,"+
					" tdind.per_lastname_en,"+
					" tdind.per_firstname_en,"+
					" tdind.per_lastname,"+
					" tdind.per_firstname,"+
					" tuasmod.ass_mod_desc,"+
					" tdcon.con_dt_first_due,"+
					" tdq.quo_nu_term,"+
					" tddelr.com_name_en,"+
					" tswkst.ref_desc_en,"+
					" tupro.pro_desc_en,"+
					" tudis.dis_desc_en,"+
					" tucom.com_desc_en,"+
					" tuvil.vil_desc_en,"+
					" tdind.per_mobile_perso,"+
					" tdind.per_mobile_perso2"+
					" FROM"+
					" td_quotation tdq"+
					" INNER JOIN td_applicant tdappli ON tdappli.app_id = tdq.app_id"+
					" INNER JOIN td_quotation_applicant tdqa ON  tdqa.quo_id = tdq.quo_id"+
					" INNER JOIN td_individual tdind ON tdind.ind_id = tdappli.ind_id"+
					" INNER JOIN td_asset tdasse ON tdasse.ass_id = tdq.ass_id"+
					" INNER JOIN tu_asset_model tuasmod ON tuasmod.ass_mod_id = tdasse.ass_mod_id"+
					" INNER JOIN td_contract tdcon ON tdcon.con_id = tdq.con_id"+
					" INNER JOIN ts_wkf_status tswkst ON tswkst.wkf_sta_id = tdcon.wkf_sta_id"+
					" INNER JOIN tu_dealer tddelr ON tddelr.dea_id = tdq.dea_id"+
					" INNER JOIN td_individual_address tdapadd ON tdapadd.ind_id = tdappli.ind_id"+
					" INNER JOIN td_address tdtdd ON tdtdd.add_id = tdapadd.add_id"+
					" INNER JOIN tu_province tupro ON  tupro.pro_id = tdtdd.pro_id"+
					" INNER JOIN tu_district tudis ON tudis.dis_id = tdtdd.dis_id"+
					" INNER JOIN tu_commune tucom ON tucom.com_id = tdtdd.com_id"+
					" INNER JOIN tu_village tuvil ON tuvil.vil_id = tdtdd.vil_id"+
					" WHERE tdqa.app_typ_id = 1 ";

		if (cbxContractStatus.getSelectedEntity() != null) {
			gOneSql += " AND tdcon.wkf_sta_id = '" + cbxContractStatus.getSelectedEntity().getId() +"'";
		}else{
			gOneSql += " AND tdcon.wkf_sta_id in ( 202 , 207) ";
		}

		if (dfStartDate.getValue() != null && dfEndDate.getValue() != null
				&& !isInstallment()) {
			gOneSql += "	AND (tdcon.con_dt_first_due >= '"+startDate1+"' AND tdcon.con_dt_first_due < '"+endDate1+"'"
					+"	OR tdcon.con_dt_first_due >= '"+startDate2+"' AND tdcon.con_dt_first_due < '"+endDate2+"'"
					+"	OR tdcon.con_dt_first_due >= '"+startDate3+"' AND tdcon.con_dt_first_due < '"+endDate3+"'"
					+"	OR tdcon.con_dt_first_due >= '"+startDate4+"' AND tdcon.con_dt_first_due < '"+endDate4+"'"
					+"	OR tdcon.con_dt_first_due >= '"+startDate5+"' AND tdcon.con_dt_first_due < '"+endDate5+"')";
		}

		if (StringUtils.isNotEmpty(txtLastNameEn.getValue())) {
			gOneSql += "AND tdind.per_lastname_en='"+txtLastNameEn.getValue()+"'";
		}
		if (StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
			gOneSql += "AND tdind.per_firstname_en='"+txtFirstNameEn.getValue()+"'";
		}
		if (StringUtils.isNotEmpty(txtReference.getValue())) {
			gOneSql += "AND tdq.quo_va_reference like '%"+txtReference.getValue()+"'";
		}
		if (cbxDealer.getSelectedEntity() != null) {
			gOneSql += "AND tddelr.dea_id="+cbxDealer.getSelectedEntity().getId();
		}
		try {
			List<NativeRow> lastPaymentInfoVORows = ENTITY_SRV.executeSQLNativeQuery(gOneSql);
			for(NativeRow row : lastPaymentInfoVORows){
				List<NativeColumn> columns = row.getColumns();
				int i = 0;
				LastPaymentInfoDTO lastPaymentInfoVO =  new LastPaymentInfoDTO();
				lastPaymentInfoVO.setId((Long) columns.get(i++).getValue());
				lastPaymentInfoVO.setReference( columns.get(i++).getValue().toString());
				lastPaymentInfoVO.setLastnameEn( columns.get(i++).getValue().toString());
				lastPaymentInfoVO.setFirstNameEn( columns.get(i++).getValue().toString());
				lastPaymentInfoVO.setLastnameKh( columns.get(i++).getValue().toString());
				lastPaymentInfoVO.setFirstNameKh( columns.get(i++).getValue().toString());
				lastPaymentInfoVO.setAssetDescEn( columns.get(i++).getValue().toString());
				lastPaymentInfoVO.setFirstPaymentDate((Date) columns.get(i++).getValue());
				lastPaymentInfoVO.setTerm((Integer) columns.get(i++).getValue());
				lastPaymentInfoVO.setDealer(columns.get(i++).getValue().toString());
				lastPaymentInfoVO.setQuotationStatus(columns.get(i++).getValue().toString());
				lastPaymentInfoVO.setProvice(columns.get(i++).getValue().toString());
				lastPaymentInfoVO.setDistrict(columns.get(i++).getValue().toString());
				lastPaymentInfoVO.setCommune(columns.get(i++).getValue().toString());
				lastPaymentInfoVO.setVillage(columns.get(i++).getValue().toString());
				lastPaymentInfoVO.setPhone1(columns.get(i).getValue()!=null ? columns.get(i).getValue().toString():"");
				i++;
				lastPaymentInfoVO.setPhone2(columns.get(i).getValue()!=null ? columns.get(i).getValue().toString():"");
				lastPaymentInfoVOs.add(lastPaymentInfoVO);
			}
		} catch (NativeQueryException e) {
			logger.error(String.valueOf(e));
		}
		return lastPaymentInfoVOs;

	}
	
	private Boolean isInstallment(){
		return cbxInstallmentType.getSelectedEntity() != null && cbxInstallmentType.getSelectedEntity() == EInstallmentType.INSTALLMENT;
	}

	public boolean isValid() {
		removeErrorsPanel();
		checkMandatoryDateField(dfStartDate, "startdate");
		checkMandatoryDateField(dfEndDate, "enddate");
		if (!errors.isEmpty()) {
			displayErrorsPanel();
		}
		return errors.isEmpty();
	}

	@SuppressWarnings("unchecked")
	private void setIndexedContainer(List<LastPaymentInfoDTO> lastPaymentInfoVOs) {
		Indexed indexedContainer = pagedTable.getContainerDataSource();
		indexedContainer.removeAllItems();
//		serviceVatINSFEE = serviceVatService.getVatService("INSFEE");
//		serviceVatSERFEE = serviceVatService.getVatService("SERFEE");
		if (lastPaymentInfoVOs != null && !lastPaymentInfoVOs.isEmpty()) {
			int index = 0;
			int size = lastPaymentInfoVOs.size();
			for (LastPaymentInfoDTO lastPaymentInfoVO : lastPaymentInfoVOs) {
				Date endDate = DateUtils.addMonthsDate(lastPaymentInfoVO.getFirstPaymentDate(), lastPaymentInfoVO.getTerm()-1);
				endDate = DateUtils.getDateAtBeginningOfDay(endDate);
				Date startSearchDate = DateUtils.getDateAtBeginningOfDay(dfStartDate.getValue());
				Date endSearchDate = DateUtils.getDateAtEndOfDay(dfEndDate.getValue());
				index++;
				if (isInstallment()) {
					int numFirstInstallmentDate = Integer.parseInt(DateFormatUtils.format(lastPaymentInfoVO.getFirstPaymentDate(), "dd"));
					int numStart = Integer.parseInt(DateFormatUtils.format(dfStartDate.getValue(), "dd"));
					int numEnd = Integer.parseInt(DateFormatUtils.format(dfEndDate.getValue(), "dd"));
					int numDateEndOfMonth = DateUtils.getDay(DateUtils.getDateAtEndOfMonth(dfStartDate.getValue()));
					Date dueDateZero = DateUtils.addDaysDate(dfStartDate.getValue(), -numStart);
					if (numFirstInstallmentDate > numDateEndOfMonth) {
						numFirstInstallmentDate = numDateEndOfMonth;
					}
					Date dueDate = DateUtils.addDaysDate(dueDateZero, numFirstInstallmentDate);
					if ((numStart <= numFirstInstallmentDate
							&& numFirstInstallmentDate <= numEnd) || numFirstInstallmentDate == 31) {
						renderItemTable(indexedContainer,index, size, lastPaymentInfoVO, endDate, dueDate);
					}
				} else {
					if ((DateUtils.getDateAtBeginningOfDay(endDate).after(startSearchDate) || DateUtils.getDateAtBeginningOfDay(endDate).equals(startSearchDate))
							&& DateUtils.getDateAtBeginningOfDay(endDate).before(endSearchDate)) {
						renderItemTable(indexedContainer,index, size, lastPaymentInfoVO, endDate, DateUtils.today());
					}
				}
			}
		}

		pagedTable.refreshContainerDataSource();
	}

	private void renderItemTable(Indexed indexedContainer, int index, int size, LastPaymentInfoDTO lastPaymentInfoVO, Date endDate, Date dueDate) {
		final Item item = indexedContainer.addItem(lastPaymentInfoVO.getId());
		if(item != null){
			item.getItemProperty(ID).setValue(lastPaymentInfoVO.getId());
			item.getItemProperty(REFERENCE).setValue(lastPaymentInfoVO.getReference());
			item.getItemProperty(CUSTOMER).setValue(lastPaymentInfoVO.getLastnameEn() +" "+lastPaymentInfoVO.getFirstNameEn());
			item.getItemProperty("assetDescEn").setValue(lastPaymentInfoVO.getAssetDescEn());
			if (isInstallment()) {
				item.getItemProperty("due.date").setValue(dueDate);
			} else {
				item.getItemProperty("due.date").setValue(endDate);
			}
			item.getItemProperty("first.installment.date").setValue(lastPaymentInfoVO.getFirstPaymentDate());
			item.getItemProperty("last.installment.date").setValue(endDate);
			item.getItemProperty("term").setValue(lastPaymentInfoVO.getTerm());
			item.getItemProperty("dealer").setValue(lastPaymentInfoVO.getDealer());
			item.getItemProperty("contractStatus").setValue(lastPaymentInfoVO.getQuotationStatus());
			item.getItemProperty("province").setValue(lastPaymentInfoVO.getProvice());
			item.getItemProperty("district").setValue(lastPaymentInfoVO.getDistrict());
			item.getItemProperty("commune").setValue(lastPaymentInfoVO.getCommune());
			item.getItemProperty("village").setValue(lastPaymentInfoVO.getVillage());
			item.getItemProperty("applicant.phone").setValue(getPhone(lastPaymentInfoVO));
		}
	}
	private String getPhone(LastPaymentInfoDTO lastPaymentInfoVO){
		String phone = lastPaymentInfoVO.getPhone1();
		if (!lastPaymentInfoVO.getPhone2().equals("")){
			phone += " - "+lastPaymentInfoVO.getPhone2();
		}
		return phone;
	}

	protected List<ColumnDefinition> createColumnDefinitions() {
		columnDefinitions = new ArrayList<>();
		columnDefinitions.add(new ColumnDefinition(ID, I18N.message("id").toUpperCase(), Long.class, Align.LEFT, 70));
		columnDefinitions.add(new ColumnDefinition(REFERENCE, I18N.message("contract.reference").toUpperCase(), String.class, Align.LEFT, 140));
		columnDefinitions.add(new ColumnDefinition(CUSTOMER, I18N.message("customer").toUpperCase(), String.class, Align.LEFT, 100));
		columnDefinitions.add(new ColumnDefinition("assetDescEn", I18N.message("asset").toUpperCase(), String.class, Align.LEFT, 150));
		/*columnDefinitions.add(new ColumnDefinition(INSURANCE_START_DATE, I18N.message("insurance.start.date"), Date.class, Align.LEFT, 150));
		columnDefinitions.add(new ColumnDefinition("down.payments", I18N.message("down.payments"), Double.class, Align.LEFT, 150));
		columnDefinitions.add(new ColumnDefinition("down.payment.percentage", I18N.message("down.payment.percentage"), Double.class, Align.LEFT, 150));*/
		columnDefinitions.add(new ColumnDefinition("due.date", I18N.message("due.date").toUpperCase(), Date.class, Align.LEFT, 150));
		columnDefinitions.add(new ColumnDefinition("first.installment.date", I18N.message("first.installment.date").toUpperCase(), Date.class, Align.LEFT, 150));
		columnDefinitions.add(new ColumnDefinition("last.installment.date", I18N.message("last.installment.date").toUpperCase(), Date.class, Align.LEFT, 150));
		//columnDefinitions.add(new ColumnDefinition("overDueDays", "Over Due Days", Integer.class, Align.LEFT, 70));
		columnDefinitions.add(new ColumnDefinition("term", I18N.message("term").toUpperCase(), Integer.class, Align.LEFT, 80));
		columnDefinitions.add(new ColumnDefinition("dealer", I18N.message("dealer").toUpperCase(), String.class, Align.LEFT, 200));
		//columnDefinitions.add(new ColumnDefinition("installment.amount", I18N.message("installment.amount"), Double.class, Align.LEFT, 200));
		columnDefinitions.add(new ColumnDefinition("contractStatus", I18N.message("contract.status").toUpperCase(), String.class, Align.LEFT, 140));
		columnDefinitions.add(new ColumnDefinition("province", I18N.message("province").toUpperCase(), String.class, Align.LEFT, 140));
		columnDefinitions.add(new ColumnDefinition("district", I18N.message("district").toUpperCase(), String.class, Align.LEFT, 140));
		columnDefinitions.add(new ColumnDefinition("commune", I18N.message("commune").toUpperCase(), String.class, Align.LEFT, 140));
		columnDefinitions.add(new ColumnDefinition("village", I18N.message("village").toUpperCase(), String.class, Align.LEFT, 140));
		columnDefinitions.add(new ColumnDefinition("applicant.phone", I18N.message("applicant.phone").toUpperCase(), String.class, Align.LEFT, 200));
		return columnDefinitions;
	}

	public void refresh() {
		search();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		search();
	}

}
