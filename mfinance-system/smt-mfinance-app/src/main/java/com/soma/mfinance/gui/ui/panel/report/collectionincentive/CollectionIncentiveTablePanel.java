package com.soma.mfinance.gui.ui.panel.report.collectionincentive;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.collection.model.CollectionIncentiveReport;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowType;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.payment.model.EPaymentMethod;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.ersys.core.hr.model.address.*;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.RowRenderer;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.data.Item;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.Entity;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

import static com.soma.mfinance.core.helper.FinServicesHelper.CASHFLOW_SRV;

/**
 * Collection Incentive Report
 *
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CollectionIncentiveTablePanel extends AbstractTablePanel<CollectionIncentiveReport> implements FMEntityField {

    private static final long serialVersionUID = 6941674894700099825L;

    @PostConstruct
    public void PostConstruct() {
        setCaption(I18N.message("collection.incentives"));
        setSizeFull();
        setMargin(true);
        setSpacing(true);
        super.init("");
    }

    @Override
    protected PagedDataProvider<CollectionIncentiveReport> createPagedDataProvider() {
        PagedDefinition<CollectionIncentiveReport> pagedDefinition = new PagedDefinition<>(searchPanel.getRestrictions());
        pagedDefinition.setRowRenderer(new CollectionIncentiveRowRenderer());

        pagedDefinition.addColumnDefinition(REFERENCE, I18N.message("reference").toUpperCase(), String.class, Align.LEFT, 140, false);
        pagedDefinition.addColumnDefinition("payment.date", I18N.message("payment.date").toUpperCase(), Date.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("collection.status", I18N.message("collection.status").toUpperCase(), String.class, Align.LEFT, 130);
        pagedDefinition.addColumnDefinition("customer.attribute", I18N.message("customer.attribute").toUpperCase(), String.class, Align.LEFT, 130);
        pagedDefinition.addColumnDefinition("start.period.promise.to.pay", I18N.message("start.period.promise.to.pay").toUpperCase(), Date.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("end.period.promise.to.pay", I18N.message("end.period.promise.to.pay").toUpperCase(), Date.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("num.overdue.in.days", I18N.message("num.overdue.in.days").toUpperCase(), Integer.class, Align.RIGHT, 150);
        pagedDefinition.addColumnDefinition(DEALER, I18N.message("dealer").toUpperCase(), String.class, Align.LEFT, 200);
        pagedDefinition.addColumnDefinition("installment.amount", I18N.message("installment.amount").toUpperCase(), String.class, Align.RIGHT, 100);
        pagedDefinition.addColumnDefinition("interest.amount", I18N.message("interest.amount").toUpperCase(), String.class, Align.RIGHT, 100);
        pagedDefinition.addColumnDefinition("principal.amount", I18N.message("principal.amount").toUpperCase(), String.class, Align.RIGHT, 100);
        pagedDefinition.addColumnDefinition("insurance.fee", I18N.message("insurance fee").toUpperCase(), String.class, Align.RIGHT, 100);
        pagedDefinition.addColumnDefinition("servicing.fee", I18N.message("servicing.fee").toUpperCase(), String.class, Align.RIGHT, 100);
        pagedDefinition.addColumnDefinition("commission", I18N.message("commission").toUpperCase(), String.class, Align.RIGHT, 100);
        pagedDefinition.addColumnDefinition("penalty.amount", I18N.message("penalty.amount").toUpperCase(), String.class, Align.RIGHT, 120);
        pagedDefinition.addColumnDefinition("payment.method", I18N.message("payment.method").toUpperCase(), String.class, Align.LEFT, 120);
        pagedDefinition.addColumnDefinition("collection.officer", I18N.message("collection.officer").toUpperCase(), String.class, Align.LEFT, 140);
        pagedDefinition.addColumnDefinition("collection.task", I18N.message("collection.task").toUpperCase(), String.class, Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("province", I18N.message("province").toUpperCase(), String.class, Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("district", I18N.message("district").toUpperCase(), String.class, Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("commune", I18N.message("commune").toUpperCase(), String.class, Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("village", I18N.message("village").toUpperCase(), String.class, Align.LEFT, 150);

        EntityPagedDataProvider<CollectionIncentiveReport> pagedDataProvider = new EntityPagedDataProvider<>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }


    @Override
    protected CollectionIncentiveReport getEntity() {
        return null;
    }

    @Override
    protected AbstractSearchPanel<CollectionIncentiveReport> createSearchPanel() {
        return new CollectionIncentiveSearchPanel(this);
    }

    private class CollectionIncentiveRowRenderer implements RowRenderer {

        @SuppressWarnings("unchecked")
        @Override
        public void renderer(Item item, Entity entity) {
            try {
                CollectionIncentiveReport collectionIncentiveReport = (CollectionIncentiveReport) entity;
                Collection collection = collectionIncentiveReport.getCollection();
                Contract contract = collection.getContract();
                List<Cashflow> cashflows = null;
                EPaymentMethod ePaymentMethod = null;
                Province province = null;
                District district = null;
                Commune commune = null;
                Village village = null;
                Dealer dealer = null;
                double tiInstallmentAmount = 0d;
                double tiInsuranceFee = 0d;
                double tiServicingFee = 0d;
                double tiPenalty = 0d;
                double tiPrincipal = 0d;
                double tiInterest = 0d;
                double commission = 0d;
                if (collection != null) {
                    tiPenalty = collection.getTiPenaltyAmount();
                }
                if (contract != null) {
                    Quotation quotation = contract.getQuotation();
                    Address applicantAddress = null;
                    if (quotation != null) {
                        Applicant applicant = quotation.getMainApplicant();
                        if (applicant != null) {
                            applicantAddress = applicant.getIndividual().getMainAddress();
                        } else {
                            applicantAddress = new Address();
                        }
                    }
                    if (applicantAddress != null) {
                        province = applicantAddress.getProvince();
                        district = applicantAddress.getDistrict();
                        commune = applicantAddress.getCommune();
                        village = applicantAddress.getVillage();
                    }
                    dealer = contract.getDealer();
                    tiInstallmentAmount = contract.getTiFinancedAmount();
                    cashflows = CASHFLOW_SRV.getCashflowsByNumberInstallment(contract.getReference(), collection.getLastNumInstallmentPaid());
                    if (cashflows != null && cashflows.size() > 0) {
                        for (Cashflow cashflow : cashflows) {
                            if (cashflow.getCashflowType() != null) {
                                if (cashflow.getCashflowType().equals(ECashflowType.FEE)) {
                                    if (cashflow.getService() != null) {
                                        if (cashflow.getService().getCode().equals(Cashflow.INSFEE)) {
                                            tiInsuranceFee = cashflow.getTiInstallmentAmount();
                                        } else if (cashflow.getService().getCode().equals(Cashflow.SERFEE)) {
                                            tiServicingFee = cashflow.getTiInstallmentAmount();
                                        } else if (cashflow.getService().getCode().equals(Cashflow.WINFEE) || cashflow.getService().getCode().equals(Cashflow.PAYGO)) {
                                            commission  = cashflow.getTiInstallmentAmount();
                                        }
                                    }
                                } else if (cashflow.getCashflowType().equals(ECashflowType.PEN)) {
                                    tiPenalty = cashflow.getTiInstallmentAmount();
                                } else if (cashflow.getCashflowType().equals(ECashflowType.CAP) || cashflow.getCashflowType().equals(ECashflowType.IAP)) {
                                    tiInstallmentAmount = cashflow.getTiInstallmentAmount();
                                    if (cashflow.getCashflowType().equals(ECashflowType.CAP)) {
                                        tiPrincipal = cashflow.getTiInstallmentAmount();
                                    } else {
                                        tiInterest = cashflow.getTiInstallmentAmount();
                                    }
                                }
                            }
                            ePaymentMethod = cashflow.getPaymentMethod();
                        }
                    }
                }
                item.getItemProperty(REFERENCE).setValue(contract != null ? contract.getReference() : "");
                item.getItemProperty("payment.date").setValue(collection != null ? collection.getLastPaymentDate() : null);
                item.getItemProperty("collection.status").setValue(collectionIncentiveReport.getColResult() != null ? collectionIncentiveReport.getColResult().getDescEn(): "");
                item.getItemProperty("customer.attribute").setValue(collectionIncentiveReport.getCustomerAttribute() != null ?  collectionIncentiveReport.getCustomerAttribute().getDescEn() : "");
                item.getItemProperty("start.period.promise.to.pay").setValue(collectionIncentiveReport.getStartPeriodPromiseToPay());
                item.getItemProperty("end.period.promise.to.pay").setValue(collectionIncentiveReport.getEndPeriodPromiseToPay());
                item.getItemProperty("num.overdue.in.days").setValue(collectionIncentiveReport.getNbOverdueInDay());
                item.getItemProperty(DEALER).setValue(dealer != null ? dealer.getNameEn() : "");
                item.getItemProperty("installment.amount").setValue(AmountUtils.format(tiInstallmentAmount));
                item.getItemProperty("interest.amount").setValue(AmountUtils.format(tiInterest));
                item.getItemProperty("principal.amount").setValue(AmountUtils.format(tiPrincipal));
                item.getItemProperty("insurance.fee").setValue(AmountUtils.format(tiInsuranceFee));
                item.getItemProperty("servicing.fee").setValue(AmountUtils.format(tiServicingFee));
                item.getItemProperty("commission").setValue(AmountUtils.format(commission));
                item.getItemProperty("penalty.amount").setValue(AmountUtils.format(tiPenalty));
                item.getItemProperty("payment.method").setValue(ePaymentMethod != null ? ePaymentMethod.getDescEn() : "");
                item.getItemProperty("collection.officer").setValue(collection.getCollectionOfficer() != null ? collection.getCollectionOfficer().getDesc() : "");
                item.getItemProperty("collection.task").setValue(collectionIncentiveReport.getCollectionTask() != null ? collectionIncentiveReport.getCollectionTask().getDescEn(): "");

                item.getItemProperty("province").setValue(province != null ? province.getDescEn() : "");
                item.getItemProperty("district").setValue(district != null ? district.getDescEn() : "");
                item.getItemProperty("commune").setValue(commune != null ? commune.getDescEn() : "");
                item.getItemProperty("village").setValue(village != null ? village.getDescEn() : "");
            } catch (Exception e) {
                System.out.println("ERROR :" + e);
                e.printStackTrace();
            }
        }
    }
}
