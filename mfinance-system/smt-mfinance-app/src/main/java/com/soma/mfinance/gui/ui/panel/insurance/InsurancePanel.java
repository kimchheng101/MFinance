package com.soma.mfinance.gui.ui.panel.insurance;

import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.asset.AssetEntityField;
import com.soma.mfinance.core.shared.report.ReportUtils;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(InsurancePanel.NAME)
public class InsurancePanel extends AbstractTabsheetPanel implements View, AssetEntityField {

    private static final long serialVersionUID = -2524492684028191473L;
    public static final String NAME = "insurance";

    @Autowired
    private InsuranceTablePanel insuranceTablePanel;

    @Autowired
    private InsuranceFormPanel insuranceFormPanel;

    public InsuranceFormPanel getInsuranceFormPanel() {
        return insuranceFormPanel;
    }

    @PostConstruct
    public void PostConstruct() {
        super.init();
        insuranceTablePanel.setMainPanel(this);

        insuranceFormPanel.setInsurancePanel(this);
        getTabSheet().setTablePanel(insuranceTablePanel);
    }

    @Override
    public void enter(ViewChangeEvent event) {
        String quotaId = event.getParameters();
        if (StringUtils.isNotEmpty(quotaId)) {
            getTabSheet().addFormPanel(insuranceFormPanel);
            insuranceFormPanel.assignValues(new Long(quotaId), true);
            getTabSheet().setForceSelected(true);
            getTabSheet().setSelectedTab(insuranceFormPanel);

        }

    }

    @Override
    public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
        if (selectedTab == insuranceFormPanel) {
            insuranceFormPanel.assignValues(insuranceTablePanel.getItemSelectedId(), true);
        } else if (selectedTab == insuranceTablePanel && getTabSheet().isNeedRefresh()) {
            insuranceTablePanel.refresh();
        }
        getTabSheet().setSelectedTab(selectedTab);
    }

    @Override
    public void onAddEventClick() {
        getTabSheet().removeFormsPanel();
        //getTabSheet().addFormPanel(identificationPanel, I18N.message("applicant.identification"));
        //getTabSheet().setSelectedTab(identificationPanel);
    }

    @Override
    public void onEditEventClick() {
        SecUser secUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Quotation quotation = ENTITY_SRV.getById(Quotation.class, insuranceTablePanel.getItemSelectedId());
        if (ProfileUtil.isUnderwriter() || ProfileUtil.isUnderwriterSupervisor()) {
            if (quotation.getSecUser() == null || quotation.getSecUser().equals(secUser)) {
                getTabSheet().addFormPanel(insuranceFormPanel);
                initSelectedTab(insuranceFormPanel);
                insuranceFormPanel.setLockOrUnlockProcess(quotation);
            } else {
                insuranceFormPanel.showProcessMessage(quotation);
            }
        } else {
            getTabSheet().addFormPanel(insuranceFormPanel);
            initSelectedTab(insuranceFormPanel);
        }

        ReportUtils.buildMenuReport(quotation, insuranceFormPanel.reportMenu);

    }

    public void displayApplicationTablePanel() {
        insuranceTablePanel.refresh();
        getTabSheet().setSelectedTab(insuranceTablePanel);
    }

}
