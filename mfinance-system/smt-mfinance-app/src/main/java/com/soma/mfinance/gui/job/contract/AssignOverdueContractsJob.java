package com.soma.mfinance.gui.job.contract;

import com.soma.mfinance.core.collection.service.CollectionService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.soma.mfinance.core.shared.contract.ContractEntityField;

/**
 * @author kimsuor.seang
 */
public class AssignOverdueContractsJob extends QuartzJobBean implements ContractEntityField {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
		
	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		// TODO PYI
		CollectionService collectionService = SpringUtils.getBean(CollectionService.class);
		try {
			logger.info(">> Start AssignOverdueContractsJob");
			collectionService.assignOverdueContracts();
			logger.info("<< End AssignOverdueContractsJob");
		} catch (DataAccessException e) {
			logger.error(e.getMessage(), e);
		}
	}
	
}
