package com.soma.mfinance.gui.ui.panel.report.outstanding.contract.insurance;

import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.quotation.model.EInsuranceYear;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.data.Property;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.tools.DateUtils;

import java.util.Date;

/**
 * @author: th.seng
 * @date : 12/12/2017
 */
public class ContractInsuranceSearchPanel extends AbstractSearchPanel<Quotation> implements FMEntityField {

    private static final long serialVersionUID = 1L;

    private ERefDataComboBox<EDealerType> cbxDealerType;
    private ERefDataComboBox<EInsuranceYear> cbxInsuranceYear;
    private DealerComboBox cbxDealer;
    private TextField txtReference;
    private AutoDateField dfStartDate;
    //private AutoDateField dfEndDate;
    private TextField txtFirstNameEn;
    private TextField txtLastNameEn;
    private TextField txtChassisNumber;

    private Disjunction orJunction;

    public ContractInsuranceSearchPanel(ContractInsuranceTablePanel contractInsuranceTablePanel) {
        super(I18N.message("search"), contractInsuranceTablePanel);
    }

    @Override
    protected Component createForm() {
        TabSheet tabSheet = new TabSheet();
        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setSpacing(true);
        contentLayout.setMargin(true);

        VerticalLayout gridLayoutPanel = new VerticalLayout();
        VerticalLayout searchLayout = new VerticalLayout();
        HorizontalLayout buttonsLayout = new HorizontalLayout();
        Button btnSearch = new Button(I18N.message("search"));
        btnSearch.setIcon(new ThemeResource("../smt-default/icons/16/search.png"));
        btnSearch.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                //search();
            }
        });

        Button btnReset = new Button(I18N.message("reset"));
        btnReset.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                reset();
            }
        });
        buttonsLayout.setSpacing(true);
        buttonsLayout.addComponent(btnSearch);
        buttonsLayout.addComponent(btnReset);
        final GridLayout gridLayout = new GridLayout(15, 3);
        gridLayout.setSpacing(true);
        cbxDealer = new DealerComboBox(null, DataReference.getInstance().getDealers(), I18N.message("all"));
        cbxDealer.setSelectedEntity(null);
        cbxDealer.setWidth("150px");

        cbxDealerType = new ERefDataComboBox<>(EDealerType.class);
        cbxDealerType.setImmediate(true);
        cbxDealerType.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                BaseRestrictions<Dealer> restrictions = getDealerRestriction();
                if (cbxDealerType.getSelectedEntity() != null) {
                    restrictions.addCriterion(Restrictions.eq("dealerType", cbxDealerType.getSelectedEntity()));
                }
                cbxDealer.setDealers(ENTITY_SRV.list(restrictions));
                cbxDealer.setSelectedEntity(null);
            }
        });

        cbxInsuranceYear = new ERefDataComboBox<EInsuranceYear>(EInsuranceYear.class);
        cbxInsuranceYear.setImmediate(true);
        cbxInsuranceYear.setSelectedEntity(cbxInsuranceYear.getValueMap().get("1"));
        txtReference = ComponentFactory.getTextField(false, 20, 150);
        txtFirstNameEn = ComponentFactory.getTextField(false, 20, 150);
        txtLastNameEn = ComponentFactory.getTextField(false, 20, 150);
        txtChassisNumber = ComponentFactory.getTextField(false, 20, 150);
        dfStartDate = ComponentFactory.getAutoDateField("", false);
        dfStartDate.setValue(DateUtils.todayH00M00S00());
        //dfEndDate = ComponentFactory.getAutoDateField("", false);
        //dfEndDate.setValue(DateUtils.getDateAtEndOfMonth(DateUtils.todayH00M00S00()));

        int iCol = 0;
        int row = 0;
        gridLayout.addComponent(new Label(I18N.message("lastname.en")), iCol++, row);
        gridLayout.addComponent(txtLastNameEn, iCol++, row);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, row);
        gridLayout.addComponent(new Label(I18N.message("firstname.en")), iCol++, row);
        gridLayout.addComponent(txtFirstNameEn, iCol++, row);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, row);
        gridLayout.addComponent(new Label(I18N.message("reference")), iCol++, row);
        gridLayout.addComponent(txtReference, iCol++, row);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, row);
        gridLayout.addComponent(new Label("Insurance Date"), iCol++, row);
        gridLayout.addComponent(dfStartDate, iCol++, row);
        gridLayout.addComponent(new Label("Year"), iCol++, row);
        gridLayout.addComponent(cbxInsuranceYear, iCol++, row);

        iCol = 0;
        row++;
        gridLayout.addComponent(new Label(I18N.message("chassis.number")), iCol++, row);
        gridLayout.addComponent(txtChassisNumber, iCol++, row);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, row);
        gridLayout.addComponent(new Label(I18N.message("dealer.type")), iCol++, row);
        gridLayout.addComponent(cbxDealerType, iCol++, row);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, row);
        gridLayout.addComponent(new Label(I18N.message("dealer")), iCol++, row);
        gridLayout.addComponent(cbxDealer, iCol++, row);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, row);
        gridLayoutPanel.addComponent(gridLayout);
        searchLayout.setMargin(true);
        searchLayout.setSpacing(true);
        searchLayout.addComponent(gridLayoutPanel);

        contentLayout.addComponent(gridLayout);
        return contentLayout;
    }

    @Override
    public BaseRestrictions<Quotation> getRestrictions() {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        orJunction = Restrictions.or();

        if (StringUtils.isNotEmpty(txtLastNameEn.getValue()) || StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
            restrictions.addAssociation("quotationApplicants", "quoapp", JoinType.INNER_JOIN);
            restrictions.addAssociation("quoapp.applicant", "app", JoinType.INNER_JOIN);
            restrictions.addAssociation("app.individual", "idv", JoinType.INNER_JOIN);
            restrictions.addCriterion("quoapp.applicantType", EApplicantType.C);
        }

        // Join with Contract
        restrictions.addAssociation("contract", "con", JoinType.INNER_JOIN);
        //restrictions.addCriterion(Restrictions.eq("con.wkfStatus", ContractWkfStatus.FIN));

        if (StringUtils.isNotEmpty(txtLastNameEn.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("idv." + LAST_NAME_EN, txtLastNameEn.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("idv." + FIRST_NAME_EN, txtFirstNameEn.getValue(), MatchMode.ANYWHERE));
        }

        /*if (cbxInsuranceYear.getSelectedEntity() != null) {
            int numberInsurance = Integer.parseInt(cbxInsuranceYear.getValue().toString()) - 1;
            restrictions.addCriterion(Restrictions.between("insuranceStartDate", DateUtils.getDateAtBeginningOfDay(DateUtils.addYearsDate(dfStartDate.getValue(), -numberInsurance)), DateUtils.getDateAtEndOfDay(DateUtils.addYearsDate(dfStartDate.getValue(), -numberInsurance))));
        }*/

        if (cbxInsuranceYear.getSelectedEntity() == null) {
            filterInsuranceByYear(0);
            filterInsuranceByYear(1);
            filterInsuranceByYear(2);
            filterInsuranceByYear(3);
            filterInsuranceByYear(4);
        } else {
            /*Date startDate = dfStartDate.getValue();
            orJunction.add(Restrictions.between("insuranceStartDate",DateUtils.getDateAtBeginningOfDay(DateUtils.addYearsDate(startDate, -(Integer.parseInt(cbxInsuranceYear.getValue().toString()))) ), DateUtils.getDateAtEndOfDay(DateUtils.addYearsDate(startDate, (Integer.parseInt(cbxInsuranceYear.getValue().toString()))))));*/
            int numberInsurance = Integer.parseInt(cbxInsuranceYear.getValue().toString()) - 1;
            //restrictions.addCriterion(Restrictions.between("insuranceStartDate", DateUtils.getDateAtBeginningOfDay(DateUtils.addYearsDate(dfStartDate.getValue(), -numberInsurance)), DateUtils.getDateAtEndOfDay(DateUtils.addYearsDate(dfStartDate.getValue(), -numberInsurance))));
            filterInsuranceByYear(numberInsurance);
        }

        if (StringUtils.isNotEmpty(txtChassisNumber.getValue())) {
            restrictions.addAssociation("con.asset", "asset", JoinType.INNER_JOIN);
            restrictions.addCriterion(Restrictions.like("asset.chassisNumber", txtChassisNumber.getValue(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotEmpty(txtReference.getValue())) {
            restrictions.addCriterion(Restrictions.like(REFERENCE, txtReference.getValue(), MatchMode.ANYWHERE));
        }
        if (cbxDealer.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, cbxDealer.getSelectedEntity().getId()));
        }

        restrictions.addCriterion(orJunction);
        return restrictions;
    }

    public AutoDateField getDfStartDate(){
        return dfStartDate;
    }

    public ERefDataComboBox<EInsuranceYear> getCbxInsuranceYear(){
        return cbxInsuranceYear;
    }

    @Override
    protected void reset() {
        cbxDealerType.setSelectedEntity(null);
        cbxInsuranceYear.setSelectedEntity(null);
        txtReference.setValue("");
        txtFirstNameEn.setValue("");
        txtLastNameEn.setValue("");
        txtChassisNumber.setValue("");
        dfStartDate.setValue(DateUtils.todayH00M00S00());
        //dfEndDate.setValue(DateUtils.todayH00M00S00());
    }

    private BaseRestrictions<Dealer> getDealerRestriction() {
        BaseRestrictions<Dealer> restrictions = new BaseRestrictions<>(Dealer.class);
        restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
        return restrictions;
    }

    private void filterInsuranceByYear(int yearNumber){
        Date startDate = dfStartDate.getValue();
        Date actualDate = DateUtils.addYearsDate(startDate, -yearNumber);
        orJunction.add(Restrictions.between("insuranceStartDate", DateUtils.getDateAtBeginningOfDay(actualDate), DateUtils.getDateAtEndOfDay(actualDate)) );
    }

}
