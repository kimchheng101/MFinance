package com.soma.mfinance.gui.ui.panel.report.last.payment.installment;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;

import com.soma.mfinance.core.asset.model.ENumMonthBeforeMaturity;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.entityfield.InstallmentEntityField;
import com.soma.mfinance.core.financial.model.ProductLine;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.payment.model.Payment;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
/**
 * 
 * @author p.ly
 *
 */
public class LastInstallmentPaymentReportSearchPanel extends VerticalLayout implements FinServicesHelper, InstallmentEntityField{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5507245349235207480L;
	private TextField txtFirstNameEn;
	private TextField txtLastNameEn;
	private DealerComboBox cbxDealer;
	private TextField txtContractReference;
	private AutoDateField dfStartDate;
	private AutoDateField dfEndDate;
	private EntityRefComboBox<ProductLine> cbxproductLine;
	private ERefDataComboBox<ENumMonthBeforeMaturity> cbxNumMonthBeforeMaturity;
	
	public LastInstallmentPaymentReportSearchPanel(){
	}
	public GridLayout getSearchForm(){
		final GridLayout gridLayout = new GridLayout(13, 3);
		gridLayout.setSpacing(true);
		cbxDealer = new DealerComboBox(null, ENTITY_SRV.list(Dealer.class), I18N.message("all"));
		cbxDealer.setSelectedEntity(null);
		cbxDealer.setWidth("220px");
	
		txtContractReference = ComponentFactory.getTextField(false, 20, 150);
		txtFirstNameEn = ComponentFactory.getTextField(false, 60, 150);        
		txtLastNameEn = ComponentFactory.getTextField(false, 60, 150);
		
		dfStartDate = ComponentFactory.getAutoDateField("",false);
		dfStartDate.setValue(DateUtils.today());
		dfEndDate = ComponentFactory.getAutoDateField("", false);    
		dfEndDate.setValue(DateUtils.today());
		cbxproductLine = new EntityRefComboBox<ProductLine>(DataReference.getInstance().getProductLines());
        
		cbxNumMonthBeforeMaturity = new ERefDataComboBox<ENumMonthBeforeMaturity>(ENumMonthBeforeMaturity.class);
		cbxNumMonthBeforeMaturity.setImmediate(true);
		cbxNumMonthBeforeMaturity.setWidth("220px");

		int iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("contract.reference")), iCol++, 0);
        gridLayout.addComponent(txtContractReference, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("lastname.en")), iCol++, 0);
        gridLayout.addComponent(txtLastNameEn, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("firstname.en")), iCol++, 0);
        gridLayout.addComponent(txtFirstNameEn, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("productline")), iCol++, 0);
        gridLayout.addComponent(cbxproductLine, iCol++, 0);
        
        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("num.months.before.maturity")), iCol++, 1);
        gridLayout.addComponent(cbxNumMonthBeforeMaturity, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("dealer")), iCol++, 1);
        gridLayout.addComponent(cbxDealer, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("payment.start.date")), iCol++, 1);
        gridLayout.addComponent(dfStartDate, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("payment.end.date")), iCol++, 1);
        gridLayout.addComponent(dfEndDate, iCol++, 1);
        return gridLayout;
	}
	/**
	 * 
	 * @return list contracts
	 */
	public List<Contract> getContracts() {
		BaseRestrictions<Contract> restrictions = new BaseRestrictions<Contract>(Contract.class);
		
		if (StringUtils.isNotEmpty(txtContractReference.getValue())) { 
			restrictions.addCriterion(Restrictions.like(REFERENCE, txtContractReference.getValue(), MatchMode.ANYWHERE));
		}
		if (cbxDealer.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, cbxDealer.getSelectedEntity().getId()));
		}
		if (cbxproductLine.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq(PRODUCT_LINE + "." + ID, cbxproductLine.getSelectedEntity().getId()));
		}
		if(StringUtils.isNotEmpty(txtLastNameEn.getValue()) || StringUtils.isNotEmpty(txtFirstNameEn.getValue())){
			restrictions.addAssociation("applicant", "app", JoinType.INNER_JOIN);
			restrictions.addAssociation("app.individual", "indi", JoinType.INNER_JOIN);
		}
		if (StringUtils.isNotEmpty(txtLastNameEn.getValue())) {
			restrictions.addCriterion(Restrictions.ilike("indi" + "." + LAST_NAME_EN, txtLastNameEn.getValue(), MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
			restrictions.addCriterion(Restrictions.ilike("indi" + "." + FIRST_NAME_EN, txtFirstNameEn.getValue(), MatchMode.ANYWHERE));
		}
		restrictions.addOrder(Order.desc(ID));
		return ENTITY_SRV.list(restrictions);
	}
	
	/**
	 * 
	 * @return startDate
	 */
	public Date startDate(){
		return dfStartDate.getValue();
	}
	/**
	 * 
	 * @return
	 */
	public Date endDate(){
		
		return dfEndDate.getValue();
	}
	/**
	 * 
	 * @return
	 */
	public Integer getNumMonthBeforeMaturity(){
		if (cbxNumMonthBeforeMaturity.getSelectedEntity()!=null){
			return Integer.parseInt(cbxNumMonthBeforeMaturity.getSelectedEntity().getDescEn());
		}
		return null;
	}
}
