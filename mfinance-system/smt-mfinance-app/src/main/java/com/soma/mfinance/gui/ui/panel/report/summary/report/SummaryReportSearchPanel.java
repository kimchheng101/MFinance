package com.soma.mfinance.gui.ui.panel.report.summary.report;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.tools.DateUtils;import com.soma.mfinance.core.asset.model.EFrequencyType;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.entityfield.InstallmentEntityField;
import com.soma.mfinance.core.financial.model.FinProduct;
import com.soma.mfinance.core.financial.model.ProductLine;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
/**
 * 
 * @author p.ly
 *
 */
public class SummaryReportSearchPanel extends VerticalLayout implements FinServicesHelper, InstallmentEntityField{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5507245349235207480L;
	private ERefDataComboBox<EDealerType> cbxDealerType;
	private DealerComboBox cbxDealer;
	private EntityRefComboBox<FinProduct> cbxFinancialProduct;
	private EntityRefComboBox<ProductLine> cbxproductLine;
	private ERefDataComboBox<EFrequencyType> cbxEFrequencyType;
	private AutoDateField dfStartDate;
	private AutoDateField dfEndDate;
	
	public SummaryReportSearchPanel(){
		
	}
	
	public GridLayout getSearchForm(){
		final GridLayout gridLayout = new GridLayout(13, 3);
		gridLayout.setSpacing(true);
		
		cbxDealerType = new ERefDataComboBox<>(EDealerType.values());
	    cbxDealerType.setImmediate(true);
	    cbxDealerType.setWidth("220px");
		
		cbxDealer = new DealerComboBox(null, ENTITY_SRV.list(Dealer.class), I18N.message("all"));
		cbxDealer.setSelectedEntity(null);
		cbxDealer.setWidth("220px");
	
		cbxFinancialProduct = new EntityRefComboBox<>();
        BaseRestrictions<FinProduct> restrictionsFinancial = new BaseRestrictions<>(FinProduct.class);
        restrictionsFinancial.getStatusRecordList().add(EStatusRecord.ACTIV);
        cbxFinancialProduct.setWidth("220px");
        cbxFinancialProduct.setRestrictions(restrictionsFinancial);
        cbxFinancialProduct.setImmediate(true);
        cbxFinancialProduct.renderer();
        cbxFinancialProduct.setSelectedEntity(null);
        
        cbxproductLine = new EntityRefComboBox<ProductLine>(DataReference.getInstance().getProductLines());
        
        cbxEFrequencyType = new ERefDataComboBox<EFrequencyType>(EFrequencyType.class);
		cbxEFrequencyType.setImmediate(true);
		cbxEFrequencyType.setWidth("220px");
		
		dfStartDate = ComponentFactory.getAutoDateField("",false);
		dfStartDate.setValue(DateUtils.today());
		dfEndDate = ComponentFactory.getAutoDateField("", false);    
		dfEndDate.setValue(DateUtils.today());

		int iCol = 0;
		gridLayout.addComponent(new Label(I18N.message("dealer.type")), iCol++, 0);
        gridLayout.addComponent(cbxDealerType, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("dealer")), iCol++, 0);
        gridLayout.addComponent(cbxDealer, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("financial.product")), iCol++, 0);
        gridLayout.addComponent(cbxFinancialProduct, iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("productline")), iCol++, 0);
        gridLayout.addComponent(cbxproductLine, iCol++, 0);
        
        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("type")), iCol++, 1);
        gridLayout.addComponent(cbxEFrequencyType, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("start.date")), iCol++, 1);
        gridLayout.addComponent(dfStartDate, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("end.date")), iCol++, 1);
        gridLayout.addComponent(dfEndDate, iCol++, 1);
        return gridLayout;
	}
	/**
	 * 
	 * @return list contracts
	 */
	public List<Contract> getContracts() {
		BaseRestrictions<Contract> restrictions = new BaseRestrictions<Contract>(Contract.class);
		restrictions.addAssociation("dealer", "dea", JoinType.INNER_JOIN);
		if (cbxDealer.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, cbxDealer.getSelectedEntity().getId()));
		}
		if (cbxproductLine.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("productLine." + ID, cbxproductLine.getSelectedEntity().getId()));
		}
		if (cbxDealerType.getSelectedEntity() != null) {
		        restrictions.addCriterion(Restrictions.eq("dea.dealerType", cbxDealerType.getSelectedEntity()));
		}
		if (cbxFinancialProduct.getSelectedEntity() != null) {
				restrictions.addCriterion(Restrictions.eq("financialProduct." + ID, cbxFinancialProduct.getSelectedEntity().getId()));
		}
		if (dfStartDate.getValue() != null) {
			restrictions.addCriterion(Restrictions.ge("startDate", DateUtils.getDateAtBeginningOfDay(dfStartDate.getValue())));
		}
		if (dfEndDate.getValue() != null) {
			restrictions.addCriterion(Restrictions.le("startDate", DateUtils.getDateAtEndOfDay(dfEndDate.getValue())));
		}
		restrictions.addOrder(Order.desc(ID));
		return ENTITY_SRV.list(restrictions);
	}
	/**
	 * 
	 * @return errors
	 */
	public List<String> getErrorMessagesBeforSearch() {
		List<String> errors = new ArrayList<String>();
		if(dfEndDate.getValue()!=null && dfStartDate.getValue()!=null){
			Long nbDays = DateUtils.getDiffInDays(dfEndDate.getValue(),dfStartDate.getValue());
			if(cbxEFrequencyType.getSelectedEntity()!=null){
				if(nbDays != null) {
					if(cbxEFrequencyType.getSelectedEntity().getDescEn().equalsIgnoreCase("Daily")||cbxEFrequencyType.getSelectedEntity().getDescEn().equalsIgnoreCase("Weekly")){
						if (nbDays.longValue() > 31) {
							errors.add(I18N.message("no.day.end.date.start.date.can.not.filter.more.then.days",new String[] { "31" }));
							return errors;
						}
					}else if(cbxEFrequencyType.getSelectedEntity().getDescEn().equalsIgnoreCase("Monthly")){
						if (nbDays.longValue() > 365) {
							errors.add(I18N.message("no.day.end.date.start.date.can.not.filter.more.then.days",new String[] { "365" }));
							return errors;
						}
					}
						
				} else {
					String messageFields = I18N.message("start.date") + " , " + I18N.message("end.date") ;
					errors.add(I18N.message("please.enter.one.there.searching.criteria", new String[] { messageFields}));
					return errors;
				}
			}else{
				errors.add("Please Select Type");
				return errors;
			}
		
		}else if(dfEndDate.getValue()==null || dfStartDate.getValue()==null){
			errors.add("Please input date");
			return errors;
		}
		return errors;
	}
	/**
	 * reset()
	 */
	void reset(){
        cbxDealerType.setSelectedEntity(null);
        cbxDealer.setSelectedEntity(null);
        cbxFinancialProduct.setSelectedEntity(null);
        cbxproductLine.setSelectedEntity(null);
        cbxEFrequencyType.setSelectedEntity(null);
	}
	/**
	 * 
	 * @return startDate
	 */
	public Date startDate(){
		return DateUtils.getDateAtBeginningOfDay(dfStartDate.getValue());
	}
	/**
	 * 
	 * @return endDate
	 */
	public Date endDate(){
		return DateUtils.getDateAtEndOfDay(dfEndDate.getValue());
	}
	/**
	 * 
	 * @return getType
	 */
	public EFrequencyType getType(){
		return cbxEFrequencyType.getSelectedEntity();
	}
}
