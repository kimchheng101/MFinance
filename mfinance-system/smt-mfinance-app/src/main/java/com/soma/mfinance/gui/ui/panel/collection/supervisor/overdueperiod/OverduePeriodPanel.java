package com.soma.mfinance.gui.ui.panel.collection.supervisor.overdueperiod;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.soma.frmk.vaadin.util.i18n.I18N;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * Created by s.torn on 6/28/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(OverduePeriodPanel.NAME)
public class OverduePeriodPanel extends AbstractTabsheetPanel implements View {

    public static final String NAME = "overdueperiod";

    @Autowired
    private OverduePeriodTablePanel overduePeriodTablePanel;

    @Autowired
    private OverduePeriodFormPanel overduePeriodFormPanel;


    @PostConstruct
    public void PostConstruct(){
        super.init();
        overduePeriodTablePanel.setMainPanel(this);
        overduePeriodFormPanel.setCaption(I18N.message("team.collection.officer"));
        getTabSheet().setTablePanel(overduePeriodTablePanel);
    }

    @Override
    public void onAddEventClick() {
        overduePeriodFormPanel.reset();
        getTabSheet().addFormPanel(overduePeriodFormPanel);
        getTabSheet().setSelectedTab(overduePeriodFormPanel);
    }

    @Override
    public void onEditEventClick() {
        getTabSheet().addFormPanel(overduePeriodFormPanel);
        initSelectedTab(overduePeriodFormPanel);
    }

    @Override
    public void initSelectedTab(com.vaadin.ui.Component selectTab) {
        if(selectTab == overduePeriodFormPanel)
            overduePeriodFormPanel.assignValue(overduePeriodTablePanel.getItemSelectedId());
        else if (selectTab == overduePeriodTablePanel && getTabSheet().isNeedRefresh())
            overduePeriodTablePanel.refresh();

        getTabSheet().setSelectedTab(selectTab);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}
