package com.soma.mfinance.gui.ui.panel.contracts;

import com.soma.mfinance.core.application.panel.ApplicationPanel;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.service.ContractService;
import com.soma.mfinance.core.document.panel.DocumentsPanel;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.loanaccount.model.LoanAccount;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimpleTable;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.Runo;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.Entity;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.amount.Amount;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.vaadin.dialogs.ConfirmDialog;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Color form panel
 * @author soth sreyrath
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ContractsFormPanel extends AbstractFormPanel implements TabSheet.SelectedTabChangeListener, FinServicesHelper, Button.ClickListener, FMEntityField {

	private static final long serialVersionUID = -2602408442583719738L;

	@Autowired
	private ContractService contractService;

	private ContractsPanel contractsPanel;
	private TabSheet accordionPanel;

	private SimpleTable<LoanAccount> simpleTable;
	private CashflowsPanel cashflowsPanel;
//	private VerticalLayout riskPanel;
	private Panel infoPanel;
	private DocumentsPanel documentsPanel;

//	public MenuBar afterSaleMenu;
	private NativeButton btnDisbursement;
	private List<ColumnDefinition> columnDefinitions;

	private Contract contract;

    @PostConstruct
	public void PostConstruct() {
        super.init();
	}
	
	@Override
	protected Entity getEntity() {
		return null;
	}
	
	@Override
	protected com.vaadin.ui.Component createForm() {
		VerticalLayout contentLayout = new VerticalLayout();
		contentLayout.setSpacing(true);

		// Button Display when Contract Status is WAIT_TRANSACTION_SETTLEMENT
		btnDisbursement = new NativeButton(I18N.message("disbursement"));
		btnDisbursement.setIcon(new ThemeResource("../smt-default/icons/16/tick.png"));
		btnDisbursement.addClickListener(this);

		// Button Display when Contract Status is FIN(ACTIVATED)
		/*afterSaleMenu = new MenuBar();
		afterSaleMenu.setWidth(110, Unit.PIXELS);
		final MenuItem afterSaleEvent = afterSaleMenu.addItem(I18N.message("after.sale.event"), null);
		afterSaleEvent.addItem(I18N.message("paid.off"), new FMCommandContract(EarlySettlementsPanel.NAME));
		//afterSaleEvent.addItem(I18N.message("repossessed"),new ChangeStatusContractCommand(ContractStatus.REP));
		afterSaleEvent.addItem(I18N.message("theft"),new ChangeStatusContractCommand(ContractWkfStatus.THE));
		afterSaleEvent.addItem(I18N.message("accident"),new ChangeStatusContractCommand(ContractWkfStatus.ACC));
		//afterSaleEvent.addItem(I18N.message("fraud"),new ChangeStatusContractCommand(ContractStatus.FRA));
		afterSaleEvent.addItem(I18N.message("write.off"), new ChangeStatusContractCommand(ContractWkfStatus.WRI));*/


		infoPanel = new Panel();
		accordionPanel = new TabSheet();
		columnDefinitions = createColumnDefinitions();
		simpleTable = new SimpleTable(I18N.message("loan.account"), columnDefinitions);
		cashflowsPanel = new CashflowsPanel();
		documentsPanel = new DocumentsPanel();
		accordionPanel.addTab(simpleTable);

		accordionPanel.addTab(cashflowsPanel, I18N.message("cashflows"));
		accordionPanel.addTab(documentsPanel, I18N.message("documents"));
        contentLayout.addComponent(btnDisbursement);
//        contentLayout.addComponent(afterSaleMenu);
        contentLayout.addComponent(infoPanel);
        contentLayout.addComponent(accordionPanel);
		return contentLayout;
	}
	
	/**
	 * @param cotraId
	 */
	public void assignValues(Long cotraId) {	
		contract = ENTITY_SRV.getById(Contract.class, cotraId);
//		afterSaleMenu.setVisible(contract.getWkfStatus().equals(ContractWkfStatus.FIN));
		btnDisbursement.setVisible(contract.getWkfStatus().equals(ContractWkfStatus.WTD));
		this.simpleTable.setContainerDataSource(getIndexedContainer(contract.getApplicant().getLoanAccount()));
		accordionPanel.setSelectedTab(simpleTable);
		Quotation quotation = QUO_SRV.getQuoatationByContractReference(cotraId);
		documentsPanel.assignValues(quotation);
		cashflowsPanel.assignValues(contract);
		if(contract.getWkfStatus().equals(ContractWkfStatus.WTD))
			reset();
		else
			accordionPanel.addTab(cashflowsPanel, I18N.message("cashflows"));

		displayContractInfo(contract);
	}
	public ContractsPanel getContractsPanel() {
		return contractsPanel;
	}
	public void setContractsPanel(ContractsPanel contractsPanel) {
		this.contractsPanel = contractsPanel;
	}
	/**
	 * @param contract
	 */
	private void displayContractInfo(final Contract contract) {
		Button btnApplicant = new Button(contract.getApplicant().getLastNameEn() + " " + contract.getApplicant().getFirstNameEn());
		btnApplicant.setStyleName(Runo.BUTTON_LINK);
		btnApplicant.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 5451254851200324858L;
			@Override
			public void buttonClick(ClickEvent event) {
				Page.getCurrent().setUriFragment("!" + ApplicationPanel.NAME + "/" + contract.getApplicant().getId());
			}
		});	
		
		String template = "contractInfo";
		InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
		CustomLayout inputFieldLayout = null;
		try {
			inputFieldLayout = new CustomLayout(layoutFile);
		} catch (IOException e) {
			Notification.show("Could not locate template " + template, e.getMessage(), Type.ERROR_MESSAGE);
		}
		
		inputFieldLayout.setSizeFull();
		inputFieldLayout.addComponent(new Label(I18N.message("applicant")), "lblApplicant");
		inputFieldLayout.addComponent(btnApplicant, "lnkApplicant");
		inputFieldLayout.addComponent(new Label(I18N.message("reference")), "lblReference");
		inputFieldLayout.addComponent(new Label("<b>" + contract.getReference() + "</b>", ContentMode.HTML), "lblReferenceValue");
		inputFieldLayout.addComponent(new Label(I18N.message("startdate") ), "lblStartDate");
		inputFieldLayout.addComponent(new Label("<b>" + DateUtils.date2StringDDMMYYYY_SLASH(contract.getStartDate()) + "</b>", ContentMode.HTML), "lblStartDateValue");
		inputFieldLayout.addComponent(new Label(I18N.message("enddate")), "lblEndDate");
		inputFieldLayout.addComponent(new Label("<b>" + DateUtils.date2StringDDMMYYYY_SLASH(contract.getEndDate()) + "</b>", ContentMode.HTML), "lblEndDateValue");
		inputFieldLayout.addComponent(new Label(I18N.message("penalty.rule")), "lblPenaltyRule");
		inputFieldLayout.addComponent(new Label(contract.getPenaltyRule() != null ? contract.getPenaltyRule().getDescEn() : ""), "txtPenaltyRule");
        
		inputFieldLayout.addComponent(new Label(I18N.message("guarantor")), "lblGuarantor");
		if (contract.getGuarantor() != null) {
			Button btnGuarantor = new Button(contract.getGuarantor().getLastNameEn() + " " + contract.getGuarantor().getFirstNameEn());
			btnGuarantor.setStyleName(Runo.BUTTON_LINK);
			btnGuarantor.addClickListener(new ClickListener() {
				private static final long serialVersionUID = -7808726057921125458L;
				@Override
				public void buttonClick(ClickEvent event) {
					Page.getCurrent().setUriFragment("!" + ApplicationPanel.NAME + "/" + contract.getGuarantor().getId());
				}
			});
			inputFieldLayout.addComponent(btnGuarantor, "lnkGuarantor");
		} else {
			inputFieldLayout.addComponent(new Label("N/A"), "lnkGuarantor");
		}
		inputFieldLayout.addComponent(new Label(I18N.message("outstanding")), "lblOutstanding");
		inputFieldLayout.addComponent(new Label(I18N.message("status")), "lblStatus");
		inputFieldLayout.addComponent(new Label(contract.getWkfStatus().getDescEn()), "txtStatus");
		
		Amount outstanding = contractService.getRealOutstanding(DateUtils.todayH00M00S00(), contract.getId());
		inputFieldLayout.addComponent(new Label(AmountUtils.format(outstanding.getTiAmount())), "txtOutstanding");

        VerticalLayout vertical = new VerticalLayout();
        vertical.addComponent(inputFieldLayout);
        infoPanel.setContent(vertical);
	}
	
	/**
	 * Reset
	 */
	@Override
	public void reset() {
		super.reset();
		accordionPanel.removeComponent(cashflowsPanel);
	}
	
	/**
	 * @return
	 */
	@Override
	protected boolean validate() {
		return errors.isEmpty();
	}

	@Override
	public void buttonClick(ClickEvent event) {
		try {
			NativeButton btn = (NativeButton) event.getSource();
			if(btn == btnDisbursement) {
				ConfirmDialog confirmDialog = ConfirmDialog.show(UI.getCurrent(), I18N.message("Are you sure to settle contract to Activated?"),
						new ConfirmDialog.Listener() {
							private static final long serialVersionUID = 1L;

							public void onClose(ConfirmDialog dialog) {
								if (dialog.isConfirmed()) {
									contract.setWkfStatus(ContractWkfStatus.FIN);
									CONT_SRV.update(contract);
									contractsPanel.displayContractTablePanel();
									Notification.show("", "Activated Contract", Notification.Type.HUMANIZED_MESSAGE);
								}
							}
						});
				confirmDialog.setWidth("400px");
				confirmDialog.setHeight("150px");
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void selectedTabChange(TabSheet.SelectedTabChangeEvent event) {

	}

    private IndexedContainer getIndexedContainer(LoanAccount loanAccount) {
        IndexedContainer indexedContainer = new IndexedContainer();
        Iterator var2 = this.columnDefinitions.iterator();
        while (var2.hasNext()) {
            ColumnDefinition column = (ColumnDefinition) var2.next();
            indexedContainer.addContainerProperty(column.getPropertyId(), column.getPropertyType(), (Object) null);
            this.simpleTable.setColumnHeader(column.getPropertyId(), column.getPropertyCaption());
            this.simpleTable.setColumnWidth(column.getPropertyId(), column.getPropertyWidth());
            this.simpleTable.setColumnAlignment(column.getPropertyId(), column.getPropertyAlignment());
        }
        Item item = indexedContainer.addItem(loanAccount.getId());
        item.getItemProperty(ACCOUNT_ID).setValue(loanAccount.getAccountId());
        item.getItemProperty(ACCOUNT_NAME).setValue(loanAccount.getAccountName());
        item.getItemProperty(PHONE_NUMBER).setValue(loanAccount.getPhoneNumber());
        if(isDisplayLeaseAmount(contract)){
            item.getItemProperty(LEASE_AMOUNT).setValue(loanAccount.getLeaseAmount());
        }
        return indexedContainer;
    }

    protected List<ColumnDefinition> createColumnDefinitions() {
        List<ColumnDefinition> columnDefinitions = new ArrayList();
        columnDefinitions.add(new ColumnDefinition(ACCOUNT_ID, I18N.message("account.id").toUpperCase(), String.class, Table.Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition(ACCOUNT_NAME, I18N.message("account.name").toUpperCase(), String.class, Table.Align.LEFT, 130));
        columnDefinitions.add(new ColumnDefinition(PHONE_NUMBER, I18N.message("phone.number").toUpperCase(), String.class, Table.Align.LEFT, 130));
        columnDefinitions.add(new ColumnDefinition(LEASE_AMOUNT, I18N.message("lease.amount").toUpperCase(), String.class, Table.Align.LEFT, 130));
        return columnDefinitions;
    }
    public boolean isDisplayLeaseAmount(Contract contract){
        if (contract != null) {
            if (contract.getWkfStatus().equals(ContractWkfStatus.WTD) ||
                    contract.getWkfStatus().equals(ContractWkfStatus.FIN) ||
                    contract.getWkfStatus().equals(ContractWkfStatus.ACC) ||
                    contract.getWkfStatus().equals(ContractWkfStatus.WRI) ||
                    contract.getWkfStatus().equals(ContractWkfStatus.THE) ||
                    contract.getWkfStatus().equals(ContractWkfStatus.LOS) ||
                    contract.getWkfStatus().equals(ContractWkfStatus.CLO) ||
                    contract.getWkfStatus().equals(ContractWkfStatus.EAR) ||
                    contract.getWkfStatus().equals(ContractWkfStatus.REP)
                    ) {
                return true;
            }
        }
        return false;
    }
}
