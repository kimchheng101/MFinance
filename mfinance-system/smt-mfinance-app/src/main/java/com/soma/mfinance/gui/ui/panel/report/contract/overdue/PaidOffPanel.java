package com.soma.mfinance.gui.ui.panel.report.contract.overdue;

import com.soma.mfinance.core.accounting.PayOffVO;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.payment.model.EPaymentMethod;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.gui.ui.panel.accounting.installment.InstallmentDetailPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;

import java.util.List;

/**
 * EarlySettlement panel
 * @author kimsuor.seang
 * @author Chakriya.POV (modified)
 * @author s.torn
 */
public class PaidOffPanel extends AbstractTabPanel implements FinServicesHelper{

	/** */
	private static final long serialVersionUID = -8911289076695224618L;
	
	private Panel contentPanel;
	private Button btnPaidOff;
	private AutoDateField dfEarlySettlement;
	private CheckBox cbIncludePenalty;
	private InstallmentDetailPanel installmentDetailPanel;
	private Button btnSearch;
	private List<Cashflow> cashflows;
	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractTabPanel#createForm()
	 */
	@Override
	protected Component createForm() {
		VerticalLayout contentLayout = new VerticalLayout();
		contentLayout.setSpacing(true);
		contentPanel = new Panel();
        contentLayout.addComponent(contentPanel);
		return contentLayout;
	}
	
	/**
	 * 
	 * @param contract
	 */
	public void assignValues(final Contract contract) {
		
		FormLayout formLayout = new FormLayout();
		
		dfEarlySettlement = new AutoDateField();
		dfEarlySettlement.setValue(DateUtils.today());
		dfEarlySettlement.setEnabled(true);
		dfEarlySettlement.setVisible(ProfileUtil.isCollectionController() || ProfileUtil.isCollectionSupervisor());
		
		cbIncludePenalty = new CheckBox(I18N.message("include.penalty"));
		cbIncludePenalty.setValue(true);
		cbIncludePenalty.setVisible(ProfileUtil.isCollectionController() || ProfileUtil.isCollectionSupervisor());
		
		btnPaidOff = new Button(I18N.message("paid.off"));

		btnPaidOff.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				Integer numInstallment = contract.getLastPaidNumInstallment();
				installmentDetailPanel = new InstallmentDetailPanel(btnSearch, ProfileUtil.isCollectionSupervisor() || ProfileUtil.isCollectionStaff(), cbIncludePenalty.getValue());
				installmentDetailPanel.assignValues(contract.getId(), numInstallment + 1);
				if(ProfileUtil.isCollectionSupervisor() || ProfileUtil.isCollectionStaff()) {
					/*PayOffVO payOffVO = INSTALLMENT_SERVICE_MFP.getInstallmentPayOffVos(contract, DateUtils.todayDate(), EPaymentMethod.CASH);
					cashflows = payOffVO.getCashflows();
					installmentDetailPanel.assignValuePayOff(payOffVO);*/
					installmentDetailPanel.setPaymentMethod(EPaymentMethod.CASH);
					installmentDetailPanel.btnPayOffClick();
				}
				getPopup(installmentDetailPanel);
			}
		});
        
		formLayout.addComponent(dfEarlySettlement);
		formLayout.addComponent(cbIncludePenalty);
		formLayout.addComponent(btnPaidOff);
		
		VerticalLayout earlySettmentLayout = new VerticalLayout();
		earlySettmentLayout.setMargin(true);
		earlySettmentLayout.addComponent(formLayout);
        contentPanel.setContent(earlySettmentLayout);
}
	/**
	 *
	 */
	private void getPopup(InstallmentDetailPanel installmentDetailPanel) {
		Window window = null;
		if (window != null) {
			window.close();
		}
		window = new Window();
		window.setImmediate(true);
		window.setModal(true);
		window.setContent(installmentDetailPanel);
		window.setCaption(I18N.message("installment.detail"));
		window.setWidth(900, Unit.PIXELS);
		window.setHeight(300, Unit.PIXELS);
		window.center();
		UI.getCurrent().addWindow(window);
	}
}
