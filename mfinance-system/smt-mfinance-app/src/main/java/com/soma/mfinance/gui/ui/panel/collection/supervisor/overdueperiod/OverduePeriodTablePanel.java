package com.soma.mfinance.gui.ui.panel.collection.supervisor.overdueperiod;

import com.soma.mfinance.core.collection.model.OverduePeriod;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.soma.frmk.vaadin.util.i18n.I18N;
import com.vaadin.ui.Table;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * ’
 * Created by s.torn on 6/28/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class OverduePeriodTablePanel extends AbstractTablePanel<OverduePeriod> implements FMEntityField {

    @PostConstruct
    public void PostConstruct() {
        super.init(I18N.message("collection.overdueperiod"));
        setCaption(I18N.message("collection.overdueperiod"));
        setSizeFull();
        setMargin(true);
        addDefaultNavigation();
    }

    @Override
    protected OverduePeriod getEntity() {
        final Long id = getItemSelectedId();
        if (id != null) {
            return ENTITY_SRV.getById(OverduePeriod.class, id);
        }
        return null;
    }

    @Override
    protected PagedDataProvider<OverduePeriod> createPagedDataProvider() {
        BaseRestrictions<OverduePeriod> restrictions = new BaseRestrictions<>(OverduePeriod.class);
        PagedDefinition<OverduePeriod> pagedDefinition = new PagedDefinition<>(restrictions);
        pagedDefinition.addColumnDefinition(ID, I18N.message("id").toUpperCase(), Long.class, Table.Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("overdueFrom", I18N.message("overdue.from").toUpperCase(), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("overdueTo", I18N.message("overdue.to").toUpperCase(), String.class, Table.Align.LEFT, 150);
        EntityPagedDataProvider<OverduePeriod> pagedDataProvider = new EntityPagedDataProvider<>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }
    @Override
    protected OverduePeriodSearchPanel createSearchPanel() {
        return new OverduePeriodSearchPanel(this);
    }
}
