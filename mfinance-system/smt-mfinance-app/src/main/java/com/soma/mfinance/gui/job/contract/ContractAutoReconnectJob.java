package com.soma.mfinance.gui.job.contract;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.soma.mfinance.core.contract.service.ContractService;
import com.soma.mfinance.core.shared.contract.ContractEntityField;

/**
 * @author kimsuor.seang
 */
public class ContractAutoReconnectJob extends QuartzJobBean implements ContractEntityField {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		
		ContractService contractService = SpringUtils.getBean(ContractService.class);
		
		logger.info(">> Start ContractAutoReconnectJob");
		contractService.getByReference("GLF-BTB-04-00015327");
		logger.info("<< End ContractAutoReconnectJob");
	}
	
}
