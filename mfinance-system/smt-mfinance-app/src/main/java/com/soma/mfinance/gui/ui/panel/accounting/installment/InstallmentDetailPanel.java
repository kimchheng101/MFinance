package com.soma.mfinance.gui.ui.panel.accounting.installment;

import com.soma.mfinance.core.accounting.InstallmentVO;
import com.soma.mfinance.core.accounting.PayOffVO;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowType;
import com.soma.mfinance.core.contract.model.cashflow.ETreasuryType;
import com.soma.mfinance.core.contract.service.cashflow.impl.CashflowUtils;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.entityfield.InstallmentEntityField;
import com.soma.mfinance.core.financial.model.EServiceType;
import com.soma.mfinance.core.financial.model.FinService;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.payment.model.EPaymentMethod;
import com.soma.mfinance.core.shared.contract.PenaltyVO;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.data.Property;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification.Type;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.dialogs.ConfirmDialog;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * @author vi.sok
 */
@SuppressWarnings("serial")
public class InstallmentDetailPanel extends VerticalLayout implements InstallmentEntityField, FinServicesHelper {
    protected Logger LOG = LoggerFactory.getLogger(getClass());
    double comision = 0d;
    double vatPlusCommission = 0d;
    double vatOfCommission = 0d;
    double otherAmount = 0d;
    private TextField txtFirstName;
    private TextField txtLastName;
    private AutoDateField dfPaymentDate;
    private AutoDateField dfInstallmentDate;
    private TextField txtInternalCode;
    private TextField txtPaymentAmount;
    private TextField txtPrincipalAmount;
    private TextField txtInterestAmount;
    private TextField txtOtherAmount;
    private TextField txtNumberInstallment;
    private TextField txtPenaltyAmount;
    private TextField txtServicingFee;
    private TextField txtInsuranceFee;
    private TextField txtInsurPercentReturn;
    private DealerComboBox cbxDealer;
    private EntityRefComboBox<EPaymentMethod> cbxPaymentMethod;
    private Button btnReceive;
    private Button btnPayOff;
    private VerticalLayout contentLayout;
    private Contract contra;
    private int numInstallment;
    private List<Cashflow> cashflows;
    private int penaltyDay;
    private String errorMessage;
    private Button btnSearch;
    private String nameButton;
    private boolean isPaidOff;
    private double temTotal = 0d;
    private List<InstallmentVO> installmentVOs;
    private double penaltyAmount;
    private boolean isIncludePenalty;
    private boolean isCollection;

    public InstallmentDetailPanel(Button btnSearch) {
        setMargin(true);
        this.btnSearch = btnSearch;
        addComponent(createForm());
    }

    public InstallmentDetailPanel(Button btnSearch, boolean isCollection, boolean isIncludePenalty){
        this(btnSearch);
        this.isIncludePenalty = isIncludePenalty;
        this.isCollection = isCollection;
        hideButtons();
    }

    /**
     * @return
     */
    protected Component createForm() {
        nameButton = "pay.off";
        btnPayOff = new Button(I18N.message(nameButton));
        btnPayOff.setImmediate(true);
        btnPayOff.setIcon(new ThemeResource("../smt-default/icons/16/add.png"));
        btnPayOff.addClickListener(new ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
                isPaidOff = true;
                if (isValidate(true)) {
                    PayOffVO payOffVO = INSTALLMENT_SERVICE_MFP.getInstallmentPayOffVos(contra, dfPaymentDate.getValue(), cbxPaymentMethod.getSelectedEntity());
                    cashflows = payOffVO.getCashflows();
                    assignValuePayOff(payOffVO);
                    nameButton = "receive.pay.off";
                    btnReceive.setCaption(I18N.message(nameButton));
                    btnPayOff.setVisible(false);
                    cbxPaymentMethod.setEnabled(false);
                    cbxDealer.setEnabled(false);
                    dfPaymentDate.setEnabled(false);
                    isPaidOff = true;
                } else {
                    isPaidOff = false;
                    if (errorMessage != null) {
                        Notification.show(I18N.message(errorMessage), Type.ERROR_MESSAGE);
                    }
                }
            }
        });
        btnReceive = new Button(I18N.message("receive"));
        btnReceive.setImmediate(true);
        btnReceive.setIcon(new ThemeResource("../smt-default/icons/16/add.png"));
        btnReceive.addClickListener(new ClickListener() {
            public void buttonClick(ClickEvent event) {
                if (isPaidOff) {
                    receivePaidOff();
                } else {
                    receiveInstallment();
                }
            }
        });

        contentLayout = new VerticalLayout();
        contentLayout.setSpacing(true);
        final GridLayout gridLayout = new GridLayout(12, 5);
        gridLayout.setSpacing(true);

        txtFirstName = ComponentFactory.getTextField(false, 60, 150);
        txtFirstName.setEnabled(false);
        txtFirstName.setStyleName("v-disabled");

        txtLastName = ComponentFactory.getTextField(60, 150);
        txtLastName.setEnabled(false);
        txtLastName.setStyleName("v-disabled");

        dfPaymentDate = ComponentFactory.getAutoDateField("", true);
        dfPaymentDate.setValue(DateUtils.today());
        dfPaymentDate.setWidth("120px");
        dfPaymentDate.setImmediate(true);
        dfPaymentDate.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {

                if (dfPaymentDate.getValue() != null) {
                    try {
                        Cashflow cashflowPenalty=calculatePenalty(dfPaymentDate.getValue(), getDouble(txtPaymentAmount) - getDouble(txtPenaltyAmount));
                        if (cashflowPenalty!=null){
                            /*cashflows.removeIf((Cashflow cashflow) -> (cashflow.getCashflowType().equals(ECashflowType.PEN)));*/
                            cashflows.add(cashflowPenalty);
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
                if (!isValidate(false) && errorMessage != null) {
                    Notification.show(I18N.message(errorMessage), Type.ERROR_MESSAGE);
                }
            }
        });

        cbxPaymentMethod = new EntityRefComboBox<>(EPaymentMethod.list());
        cbxPaymentMethod.setRequired(true);
        cbxPaymentMethod.setImmediate(true);
        cbxPaymentMethod.setWidth(150, Unit.PIXELS);

        cbxPaymentMethod.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if (cbxPaymentMethod.getSelectedEntity() != null) {
                    setPaymentMethodCommission();
                }
                if (!isValidate(false) && errorMessage != null) {
                    Notification.show(I18N.message(errorMessage), Type.ERROR_MESSAGE);
                }
            }
        });

        dfInstallmentDate = ComponentFactory.getAutoDateField("", false);
        dfInstallmentDate.setEnabled(false);

        txtInternalCode = ComponentFactory.getTextField(60, 150);
        txtInternalCode.setEnabled(false);

        txtPaymentAmount = ComponentFactory.getTextField(60, 120);
        txtPaymentAmount.setEnabled(false);

        txtPrincipalAmount = ComponentFactory.getTextField(60, 120);
        txtPrincipalAmount.setEnabled(false);
        txtInterestAmount = ComponentFactory.getTextField(60, 120);
        txtInterestAmount.setEnabled(false);

        txtPenaltyAmount = ComponentFactory.getTextField(60, 120);
        txtPenaltyAmount.setEnabled(false);

        txtServicingFee = ComponentFactory.getTextField(30, 150);
        txtServicingFee.setEnabled(false);
        txtInsuranceFee = ComponentFactory.getTextField(30, 150);
        txtInsuranceFee.setEnabled(false);

        txtInsurPercentReturn = ComponentFactory.getTextField(30, 150);
        txtInsurPercentReturn.setEnabled(false);

        txtOtherAmount = ComponentFactory.getTextField(60, 120);
        txtOtherAmount.setEnabled(false);

        txtNumberInstallment = ComponentFactory.getTextField(60, 150);
        txtNumberInstallment.setEnabled(false);
        cbxDealer = new DealerComboBox(null, ENTITY_SRV.list(Dealer.class), I18N.message("all"));
        cbxDealer.setSelectedEntity(null);
        cbxDealer.setWidth("220px");
        cbxDealer.setRequired(true);

        int iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("dealer")), iCol++, 0);
        gridLayout.addComponent(cbxDealer, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("num.installment")), iCol++, 0);
        gridLayout.addComponent(txtNumberInstallment, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("principal")), iCol++, 0);
        gridLayout.addComponent(txtPrincipalAmount, iCol++, 0);

        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("lastname.en")), iCol++, 1);
        gridLayout.addComponent(txtLastName, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("firstname.en")), iCol++, 1);
        gridLayout.addComponent(txtFirstName, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("interest.rate")), iCol++, 1);
        gridLayout.addComponent(txtInterestAmount, iCol++, 1);

        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("servicing.fee")), iCol++, 2);
        gridLayout.addComponent(txtServicingFee, iCol++, 2);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 2);
        gridLayout.addComponent(new Label(I18N.message("insurance.fee")), iCol++, 2);
        gridLayout.addComponent(txtInsuranceFee, iCol++, 2);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 2);
        gridLayout.addComponent(new Label(I18N.message("penalty.amount")), iCol++, 2);
        gridLayout.addComponent(txtPenaltyAmount, iCol++, 2);

        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("installment.date")), iCol++, 3);
        gridLayout.addComponent(dfInstallmentDate, iCol++, 3);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 3);

        gridLayout.addComponent(new Label(I18N.message("insurance.return.percent")), iCol++, 3);
        gridLayout.addComponent(txtInsurPercentReturn, iCol++, 3);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 3);
        gridLayout.addComponent(new Label(I18N.message("other.amount")), iCol++, 3);
        gridLayout.addComponent(txtOtherAmount, iCol++, 3);

        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("payment.date")), iCol++, 4);
        gridLayout.addComponent(dfPaymentDate, iCol++, 4);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 4);
        gridLayout.addComponent(new Label(I18N.message("payment.method")), iCol++, 4);
        gridLayout.addComponent(cbxPaymentMethod, iCol++, 4);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 4);
        gridLayout.addComponent(new Label(I18N.message("total.amount")), iCol++, 4);
        gridLayout.addComponent(txtPaymentAmount, iCol++, 4);

        contentLayout.addComponent(gridLayout);
        HorizontalLayout btnLayout = new HorizontalLayout();
        btnLayout.setSpacing(true);
        btnLayout.addComponent(ComponentFactory.getSpaceLayout(360, Unit.PIXELS));
        btnLayout.addComponent(btnReceive);
        btnLayout.addComponent(btnPayOff);
        contentLayout.addComponent(btnLayout);
        return contentLayout;
    }

    /**
     * @param value to the txtPaymentAmount to set
     */
    public void setTxtPaymentAmount(String value) {
        txtPaymentAmount.setValue(value);
    }

    public void assignValues(Long contractId, int numInstallment) {
        this.numInstallment = numInstallment;
        contra = INSTALLMENT_SERVICE_MFP.getById(Contract.class, contractId);
        installmentVOs = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(contra, numInstallment);
        cashflows = setInstallmentDetail(installmentVOs);
    }

    /**
     *
     */
    private void receivePaidOff() {
        if (isValidate(true)) {
            ConfirmDialog.show(UI.getCurrent(), I18N.message("confirm.receive.paid.off.payment",
                    txtLastName.getValue() + " " + txtFirstName.getValue()),
                    new ConfirmDialog.Listener() {
                        @Override
                        public void onClose(ConfirmDialog dialog) {
                            if (dialog.isConfirmed()) {
                                PAYMENT_SERVICE_MFP.createPaidOffPayment(cashflows, dfPaymentDate.getValue(), penaltyDay, cbxDealer.getSelectedEntity(), cbxPaymentMethod.getSelectedEntity());
                                btnReceive.setVisible(false);
                                Notification notification = new Notification("", Type.HUMANIZED_MESSAGE);
                                notification.setDescription(I18N.message("this.paid.off.was.received"));
                                notification.setDelayMsec(5000);
                                notification.show(Page.getCurrent());
                                btnSearch.click();
                            }
                        }
                    });
        } else {
            if (errorMessage != null) {
                Notification.show(I18N.message(errorMessage), Type.ERROR_MESSAGE);
            }
        }
    }

    private void receiveInstallment() {
        if ((((contra.getLastPaidNumInstallment() == null || contra.getLastPaidNumInstallment() == 0) && numInstallment == 1)
                || (contra.getLastPaidNumInstallment() != null && (numInstallment - 1) == contra.getLastPaidNumInstallment()))
                && isValidate(true)) {
            ConfirmDialog.show(UI.getCurrent(), I18N.message("confirm.receive.installment.payment",
                    txtLastName.getValue() + " " + txtFirstName.getValue()),
                    new ConfirmDialog.Listener() {
                        @Override
                        public void onClose(ConfirmDialog dialog) {
                            if (dialog.isConfirmed()) {
                                FinService finService = cbxPaymentMethod.getSelectedEntity().getService();
                                if (finService != null && EServiceType.COMM.getCode().equals(finService.getServiceType().getCode())) {
                                    addCashflowCommission(cbxPaymentMethod.getSelectedEntity());
                                }
                                PAYMENT_SERVICE_MFP.createPayment(cashflows, dfPaymentDate.getValue(), penaltyDay, cbxDealer.getSelectedEntity(), cbxPaymentMethod.getSelectedEntity());
                                btnReceive.setVisible(false);
                                btnPayOff.setVisible(false);
                                Notification notification = new Notification("", Type.HUMANIZED_MESSAGE);
                                notification.setDescription(I18N.message("this.installment.was.received"));
                                notification.setDelayMsec(5000);
                                notification.show(Page.getCurrent());
                                btnSearch.click();
                            }
                        }
                    });
        } else {
            Date lastPaidDate = contra.getLastPaidDateInstallment();
            if (lastPaidDate == null) {
                lastPaidDate = contra.getFirstDueDate();
            } else {
                lastPaidDate = DateUtils.addMonthsDate(lastPaidDate, 1);
            }
            if (!isValidate(true)) {
                if (errorMessage != null) {
                    Notification.show(I18N.message(errorMessage), Type.ERROR_MESSAGE);
                }
            } else {
                String installmentDate = DateUtils.date2String(lastPaidDate, DateUtils.FORMAT_DDMMYYYY_SLASH);
                Notification.show(I18N.message("payment.installment.not.order") + installmentDate, Type.ERROR_MESSAGE);
            }
        }
    }

    public void reset() {
        isPaidOff = false;
        txtFirstName.setValue("");
        txtLastName.setValue("");
        //dfPaymentDate.setValue(DateUtils.today());
        txtInternalCode.setValue("");
        txtNumberInstallment.setValue("");
        //cbxDealer.setSelectedEntity(null);
        txtInterestAmount.setValue("");
    }

    private List<Cashflow> setInstallmentDetail(List<InstallmentVO> installmentVOs) {
        reset();
        List<Cashflow> cashflows = new ArrayList<Cashflow>();
        txtFirstName.setValue(contra.getApplicant().getFirstNameEn());
        txtLastName.setValue(contra.getApplicant().getLastNameEn());
        dfPaymentDate.setValue(dfPaymentDate.getValue());
        dfInstallmentDate.setValue(installmentVOs.get(0).getInstallmentDate());
        txtInternalCode.setValue("");
        //cbxDealer.setSelectedEntity(contra.getDealer());
        double tiPrincipal = 0d;
        double tiInterestRate = 0d;
        double tiInsurance = 0d;
        double tiServiceFee = 0d;
        double otherAmount = 0d;
        double totalInstallment = 0d;
        for (InstallmentVO installmentVO : installmentVOs) {
            if (installmentVO.getTiamount() == 0d) {
                continue;
            }
            Contract contra = installmentVO.getContract();
            Cashflow cashflow = CashflowUtils.createCashflow(contra.getProductLine(),
                    null, contra, 0d,
                    installmentVO.getCashflowType(), ETreasuryType.APP, null, cbxPaymentMethod.getSelectedEntity(),
                    installmentVO.getTiamount(), installmentVO.getVatAmount(), installmentVO.getTiamount(),
                    installmentVO.getInstallmentDate(), installmentVO.getPeriodStartDate(), installmentVO.getPeriodEndDate(), installmentVO.getNumInstallment());
            cashflow.setService(installmentVO.getService());
            cashflows.add(cashflow);
            totalInstallment += installmentVO.getTiamount() + installmentVO.getVatAmount();
            if (installmentVO.getCashflowType() == ECashflowType.CAP) {
                tiPrincipal = installmentVO.getTiamount() + installmentVO.getVatAmount();
            } else if (installmentVO.getCashflowType() == ECashflowType.IAP) {
                tiInterestRate = installmentVO.getTiamount() + installmentVO.getVatAmount();
            } else if (installmentVO.getService() != null && EServiceType.INSFEE.getCode().equals(installmentVO.getService().getCode())) {
                tiInsurance = installmentVO.getTiamount() + installmentVO.getVatAmount();
            } else if (installmentVO.getService() != null && EServiceType.SRVFEE.getCode().equals(installmentVO.getService().getCode())) {
                tiServiceFee = installmentVO.getTiamount() + installmentVO.getVatAmount();
            } else if (installmentVO.getService() != null && EServiceType.SRVFEEEXT.getCode().equals(installmentVO.getService().getCode())) {
                tiServiceFee = installmentVO.getTiamount() + installmentVO.getVatAmount();
            } else {
                otherAmount += installmentVO.getTiamount() + installmentVO.getVatAmount();
            }
        }

        txtPrincipalAmount.setValue(AmountUtils.format(tiPrincipal));
        txtOtherAmount.setValue(AmountUtils.format(otherAmount));
        txtNumberInstallment.setValue(String.valueOf(installmentVOs.get(0).getNumInstallment()));
        txtServicingFee.setValue(AmountUtils.format(tiServiceFee));
        txtInsuranceFee.setValue(AmountUtils.format(tiInsurance));
        txtInterestAmount.setValue(AmountUtils.format(tiInterestRate));
        double totalAmount = 0d;
        try {
            Cashflow cashflowPenalty = calculatePenalty(dfPaymentDate.getValue(), totalInstallment);
            double penaltyAmnt = cashflowPenalty.getTiInstallmentAmount();
            double vatPenalty = cashflowPenalty.getVatInstallmentAmount();
            totalAmount = totalInstallment + (penaltyAmnt + vatPenalty);
            temTotal = totalAmount;
            /*cashflows.removeIf((Cashflow cashflow) -> (cashflow.getCashflowType().equals(ECashflowType.PEN)));*/
            cashflows.add(cashflowPenalty);
        } catch (NullPointerException e) {
            totalAmount = totalInstallment;
            temTotal = totalAmount;
            txtPaymentAmount.setValue(AmountUtils.format(totalAmount));
        }

        return cashflows;
    }

    /**
     * @param payOffVO
     */
    public void assignValuePayOff(PayOffVO payOffVO) {
        reset();
        txtFirstName.setValue(contra.getApplicant().getFirstNameEn());
        txtLastName.setValue(contra.getApplicant().getLastNameEn());
        dfPaymentDate.setValue(dfPaymentDate.getValue());
        dfInstallmentDate.setValue(payOffVO.getInstallmentDate());
        txtInternalCode.setValue("");
        //cbxDealer.setSelectedEntity(cbxDealer.getSelectedEntity());
        txtNumberInstallment.setValue(String.valueOf(payOffVO.getNumInstallment()));
        txtPrincipalAmount.setValue(AmountUtils.format(payOffVO.getPricipal().getTiAmount() + payOffVO.getPricipal().getVatAmount()));
        if(cbxPaymentMethod.getSelectedEntity() == null){
            cbxPaymentMethod.setSelectedEntity(cbxPaymentMethod.getValueMap().get("1"));
        }
        calulateCommission(cbxPaymentMethod.getSelectedEntity());
        vatPlusCommission = payOffVO.getCommissionFee() != null ? payOffVO.getCommissionFee().getTiAmount() + payOffVO.getCommissionFee().getVatAmount() : 0d;
        double transferOwnerShipFee = payOffVO.getTransferOwnerShip() != null ? payOffVO.getTransferOwnerShip().getTiAmount() + payOffVO.getTransferOwnerShip().getVatAmount() : 0d;
        double another = payOffVO.getOtherAmount().getTiAmount() + payOffVO.getOtherAmount().getVatAmount() + vatPlusCommission + transferOwnerShipFee;
        txtOtherAmount.setValue(AmountUtils.format(another));
        txtPenaltyAmount.setValue(isIncludePenalty && isCollection ? AmountUtils.format(payOffVO.getPenaltyAmount().getTiAmount() + payOffVO.getPenaltyAmount().getVatAmount()) : "0.00");
        txtServicingFee.setValue(AmountUtils.format(payOffVO.getServiceFee().getTiAmount() + payOffVO.getServiceFee().getVatAmount()));
        txtInsuranceFee.setValue(AmountUtils.format(payOffVO.getInsuranceFee().getTiAmount() + payOffVO.getInsuranceFee().getVatAmount()));
        txtInterestAmount.setValue(AmountUtils.format(payOffVO.getInterest().getTiAmount() + payOffVO.getInterest().getVatAmount()));
        txtInsurPercentReturn.setValue(AmountUtils.format(payOffVO.getInsuranceFeeReturn()));
        double totalAmount = getDouble(txtPrincipalAmount) + getDouble(txtOtherAmount) + (isIncludePenalty && isCollection ? getDouble(txtPenaltyAmount) : 0d)
                + getDouble(txtServicingFee) + getDouble(txtInsuranceFee) + getDouble(txtInterestAmount);
        temTotal = totalAmount;
        penaltyDay = 0;
        penaltyAmount = payOffVO.getPenaltyAmount().getTiAmount() + payOffVO.getPenaltyAmount().getVatAmount();
        if (payOffVO.getPenaltyVOs() != null) {
            for (PenaltyVO penaltyVO : payOffVO.getPenaltyVOs()) {
                if (penaltyVO.getNumPenaltyDays() != null && penaltyVO.getNumPenaltyDays() != 0) {
                    penaltyDay += penaltyVO.getNumPenaltyDays();
                }
            }
        }
        txtPaymentAmount.setValue(AmountUtils.format(totalAmount));
    }

    /**
     * @param field
     * @return
     */
    private Double getDouble(AbstractTextField field) {
        try {
            return Double.parseDouble((String) field.getValue());
        } catch (Exception arg2) {
            return 0d;
        }
    }

    /**
     * @return
     */
    private boolean isValidate(boolean isButton) {
        errorMessage = "";
        if (cbxDealer.getSelectedEntity() == null) {
            errorMessage = "dealer.is.required";
            return false;
        } else if (cbxPaymentMethod.getSelectedEntity() == null) {
            errorMessage = "payment.method.is.required";
            return false;
        } else if (dfPaymentDate.getValue() == null) {
            errorMessage = isButton ? "payment.date.is.required" : null;
            return false;
        }
        return true;
    }

    /***
     * @author p.leap
     * @param ePaymentMethod
     */
    private void addCashflowCommission(EPaymentMethod ePaymentMethod) {

        FinService finService = ePaymentMethod.getService();
        calulateCommission(ePaymentMethod);
        InstallmentVO installmentVO = installmentVOs.get(0);
        Cashflow cashflowCommission = CashflowUtils.createCashflow(contra.getProductLine(),
                null, contra, 0d,
                ECashflowType.FEE, ETreasuryType.APP, null, cbxPaymentMethod.getSelectedEntity(),
                comision, vatOfCommission, comision,
                installmentVO.getInstallmentDate(), installmentVO.getPeriodStartDate(), installmentVO.getPeriodEndDate(), installmentVO.getNumInstallment());
        cashflowCommission.setService(finService);
        cashflows.add(cashflowCommission);
    }

    public Cashflow calculatePenalty(Date paymentDate, double totalInstallment) {
        double totalAmount = 0d;
        penaltyAmount = 0d;
        cashflows.removeIf((Cashflow cashflow) -> (cashflow.getCashflowType().equals(ECashflowType.PEN)));
        PenaltyVO penaltyVO = CONT_SRV.calculatePenalty(contra, installmentVOs.get(0).getInstallmentDate(), paymentDate, contra.getTiInstallmentAmount());
        if (penaltyVO != null && penaltyVO.getNumPenaltyDays() != null && penaltyVO.getNumPenaltyDays() > 0) {
            penaltyDay = penaltyVO.getNumPenaltyDays();
            InstallmentVO installmentVO = installmentVOs.get(0);
            Cashflow cashflowPenalty = CashflowUtils.createCashflow(contra.getProductLine(),
                    null, contra, 0d,
                    ECashflowType.PEN, ETreasuryType.APP, null, cbxPaymentMethod.getSelectedEntity(),
                    penaltyVO.getPenaltyAmount().getTiAmount(), penaltyVO.getPenaltyAmount().getVatAmount(), penaltyVO.getPenaltyAmount().getTiAmount(),
                    installmentVO.getInstallmentDate(), installmentVO.getPeriodStartDate(), installmentVO.getPeriodEndDate(), installmentVO.getNumInstallment());


            if (penaltyVO.getPenaltyAmount() != null) {
                txtPenaltyAmount.setValue(AmountUtils.format(penaltyVO.getPenaltyAmount().getTiAmount() + penaltyVO.getPenaltyAmount().getVatAmount()));
                penaltyAmount = penaltyVO.getPenaltyAmount().getTiAmount() + penaltyVO.getPenaltyAmount().getVatAmount();
                temTotal = totalAmount;
            }
            totalAmount = totalInstallment + penaltyAmount;
            txtPaymentAmount.setValue(AmountUtils.format(totalAmount));
            return cashflowPenalty;
        } else {
            penaltyDay = 0;
            txtPenaltyAmount.setValue(AmountUtils.format(0d));
            txtPaymentAmount.setValue(AmountUtils.format(totalInstallment));
            return null;
        }

    }

    public void btnPayOffClick(){
        btnPayOff.click();
    }

    public void setPaymentMethod(EPaymentMethod ePaymentMethod){
        cbxPaymentMethod.setSelectedEntity(ePaymentMethod);
    }

    private void calulateCommission(EPaymentMethod ePaymentMethod) {
        if (ePaymentMethod.getService() != null) {
            FinService finService = ePaymentMethod.getService();
            comision = finService.getTiPrice();
            if (finService.getVat() != null) {
                vatOfCommission = finService.getVat().getValue() * comision / 100;
            }
            vatPlusCommission = comision + vatOfCommission;
        }
    }

    private void setPaymentMethodCommission() {
        FinService finService = cbxPaymentMethod.getSelectedEntity().getService();
        cashflows = setInstallmentDetail(installmentVOs);
        if (finService != null && EServiceType.COMM.getCode().equals(finService.getServiceType().getCode())) {

            calulateCommission(cbxPaymentMethod.getSelectedEntity());
            if (txtOtherAmount.getValue() != null) {
                otherAmount = getDouble(txtOtherAmount);
            }
            txtOtherAmount.setValue(AmountUtils.format(otherAmount + vatPlusCommission));
            temTotal = getDouble(txtPaymentAmount) + vatPlusCommission;
            setTxtPaymentAmount(AmountUtils.format(getDouble(txtPaymentAmount) + vatPlusCommission));
        }
    }

    private int numberOfDaysInMonth(int month, int year) {
        java.util.Calendar monthStart = new GregorianCalendar(year, month - 1, 1);
        return monthStart.getActualMaximum(java.util.Calendar.DAY_OF_MONTH);
    }

    private void hideButtons(){
        btnReceive.setVisible(!isCollection);
        btnPayOff.setVisible(!isCollection);
        if(isCollection){
            cbxDealer.setSelectedEntity(ENTITY_SRV.list(Dealer.class).get(0));
        }
    }

}