package com.soma.mfinance.gui.ui.panel.report.contract.overdue;

import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.collection.model.ContractCollectionHistory;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.mfinance.core.shared.cashflow.CashflowEntityField;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.EntityColumnRenderer;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.data.Item;
import com.vaadin.ui.Table;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.amount.Amount;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;

/**
 * Contract Overdue Table panel
 * @author soth sreyrath
 * @author s.torn
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class OverdueContractTablePanel extends AbstractTablePanel<Collection>
        implements FMEntityField, CashflowEntityField, FinServicesHelper {

    /** */
    private static final long serialVersionUID = 6941674894700099825L;

    /** */
    @PostConstruct
    public void PostConstruct() {
        setCaption(I18N.message("overdue.contracts"));
        setSizeFull();
        setMargin(true);
        setSpacing(true);
        if (ProfileUtil.isManager()) {
            initialize(I18N.message("overdue.contracts"));
        }
        else {
            super.init(I18N.message("overdue.contracts"));
        }

        getPagedTable().addStyleName("colortable");
        getPagedTable().setCellStyleGenerator(new Table.CellStyleGenerator() {
            private static final long serialVersionUID = 6242667432758981026L;

            @Override
            public String getStyle(Table source, Object itemId,
                                   Object propertyId) {
                if (propertyId == null) {
                    Item item = source.getItem(itemId);
                    Date endPeriodPromiseToPay = (Date) item.getItemProperty(
                            "end.period.promise.to.pay").getValue();
                    if (endPeriodPromiseToPay != null) {
                        long timeCompare = getHour(DateUtils.todayH00M00S00()
                                .getTime() - endPeriodPromiseToPay.getTime());
                        if (timeCompare >= 0) {
                            return "highligh-red";
                        } else if (timeCompare >= -48) {
                            return "highligh";
                        }
                    }
                }
                return null;
            }
        });
    }

    public void initialize(String caption) {
        searchPanel = createSearchPanel();
        if (searchPanel != null) {
            addComponent(searchPanel);
        }

        beforeTablePanel = createBeforeTablePanel();
        if (beforeTablePanel != null) {
            addComponent(beforeTablePanel);
        }

        pagedTable = createPagedTable(caption);

        addComponent(pagedTable);
        addComponent(pagedTable.createControls());

    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTablePanel#createPagedDataProvider()
     */
    @Override
    protected PagedDataProvider<Collection> createPagedDataProvider() {
        PagedDefinition<Collection> pagedDefinition = new PagedDefinition<Collection>(searchPanel.getRestrictions());
        pagedDefinition.addColumnDefinition(ID, I18N.message("id").toUpperCase(), Long.class, Table.Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("updateDate", I18N.message("date.updated").toUpperCase(), Date.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("contract.reference", I18N.message("contract"), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("colResult.descEn", I18N.message("collection.status").toUpperCase(), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("customer.attribute", I18N.message("customer.attribute").toUpperCase(), String.class, Table.Align.LEFT, 150,new CustomerAttriRenderer());
        pagedDefinition.addColumnDefinition("lastAction.descEn", I18N.message("action.schedule").toUpperCase(), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("amount.promise.to.pay", I18N.message("amount.promise.to.pay").toUpperCase(), Amount.class, Table.Align.RIGHT, 150,new AmountPromiseToPayRenderer());
        pagedDefinition.addColumnDefinition("start.period.promise.to.pay", I18N.message("start.period.promise.to.pay").toUpperCase(), Date.class, Table.Align.LEFT, 150,new StartPeriodPromiseToPayRenderer());
        pagedDefinition.addColumnDefinition("end.period.promise.to.pay", I18N.message("end.period.promise.to.pay").toUpperCase(), Date.class, Table.Align.LEFT, 150,new EndPeroidPromiseToPayRenderer());
        pagedDefinition.addColumnDefinition("leaseMobilePhone1", I18N.message("lessee.mobile.phone.1").toUpperCase(), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("leaseMobilePhone1", I18N.message("lessee.mobile.phone.2").toUpperCase(), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("guarantorMobilePhone", I18N.message("guarantor.mobile.phone").toUpperCase(), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition(CUSTOMER, I18N.message("customer").toUpperCase(), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition(DEALER, I18N.message("dealer").toUpperCase(), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("creditOfficer.desc", I18N.message("credit.officer").toUpperCase(), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("currentTerm", I18N.message("term").toUpperCase(), Integer.class, Table.Align.LEFT, 60);
        //pagedDefinition.addColumnDefinition("area", I18N.message("area").toUpperCase(), String.class, Table.Align.LEFT, 150,new AreaRenderer());
        pagedDefinition.addColumnDefinition("nbOverdueInDays", I18N.message("num.overdue.in.days").toUpperCase(), Integer.class, Table.Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("lastPaymentDate", I18N.message("lastest.payment.date").toUpperCase(), Date.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("nbInstallmentsInOverdue", I18N.message("num.installment.in.overdue").toUpperCase(), Integer.class, Table.Align.LEFT, 130);
        pagedDefinition.addColumnDefinition("lastNumInstallmentPaid", I18N.message("latest.num.installment.paid").toUpperCase(), Integer.class, Table.Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("tePenaltyAmount", I18N.message("total.penalty.amount.in.overdue").toUpperCase(), Double.class, Table.Align.RIGHT, 160);
        pagedDefinition.addColumnDefinition("TOTAL_AMOUNT_NOT_PAID", I18N.message("total.amount.in.overdue").toUpperCase(), Double.class, Table.Align.RIGHT, 150,new TotalAmountNotPaidPlusVAT());
        pagedDefinition.addColumnDefinition("nbInstallmentsPaid", I18N.message("num.installment.paid").toUpperCase(), Integer.class, Table.Align.RIGHT, 160);
        pagedDefinition.addColumnDefinition("lastPaidPaymentMethod.descEn", I18N.message("last.paid.payment.method").toUpperCase(), String.class, Table.Align.LEFT, 160);
        pagedDefinition.addColumnDefinition("tiBalanceCapital", I18N.message("outstanding.balance").toUpperCase(), Double.class, Table.Align.RIGHT, 140);
        pagedDefinition.addColumnDefinition("contract.startDate", I18N.message("contract.start.date").toUpperCase(), Date.class, Table.Align.LEFT, 160);
        pagedDefinition.addColumnDefinition("province.descEn", I18N.message("province").toUpperCase(), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("district.descEn", I18N.message("district").toUpperCase(), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("commune.descEn", I18N.message("commune").toUpperCase(), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("village.descEn", I18N.message("village").toUpperCase(), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("placeofbirth", I18N.message("placeofbirth").toUpperCase(), String.class, Table.Align.LEFT, 150);

        EntityPagedDataProvider<Collection> pagedDataProvider = new EntityPagedDataProvider<>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTablePanel#getEntity()
     */
    @Override
    protected Collection getEntity() {
        final Long id = getItemSelectedId();
        if (id != null) {
            //return ENTITY_SRV.getById(Collection.class, id);
        }
        return null;    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTablePanel#createSearchPanel()
     */
    @Override
    protected AbstractSearchPanel<Collection> createSearchPanel() {
        if (ProfileUtil.isCollectionController()) {
            return new OverdueContractSearch2Panel(this);
        }
        return new OverdueContractSearchPanel(this);
    }

    private class CustomerAttriRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            ContractCollectionHistory lastCollectionContractHistory = ((Collection) getEntity()).getLastCollectionContractHistory();
            if(lastCollectionContractHistory != null) {
                return  lastCollectionContractHistory.getCustField() != null ? lastCollectionContractHistory.getCustField().getDescEn() : "";
            }
            return "";
        }
    }
    private class AmountPromiseToPayRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            ContractCollectionHistory lastCollectionContractHistory = ((Collection) getEntity()).getLastCollectionContractHistory();
            if(lastCollectionContractHistory != null) {
                return AmountUtils.convertToAmount(lastCollectionContractHistory.getAmountPromiseToPayUsd());
            }
            return null;
        }
    }
    private class StartPeriodPromiseToPayRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            ContractCollectionHistory lastCollectionContractHistory = ((Collection) getEntity()).getLastCollectionContractHistory();
            if(lastCollectionContractHistory != null) {
                return  lastCollectionContractHistory.getStartPeriodPromiseToPay() != null ? lastCollectionContractHistory.getStartPeriodPromiseToPay() : null;
            }
            return null;
        }
    }
    private class EndPeroidPromiseToPayRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            ContractCollectionHistory lastCollectionContractHistory = ((Collection) getEntity()).getLastCollectionContractHistory();
            if(lastCollectionContractHistory != null) {
                return  lastCollectionContractHistory.getEndPeriodPromiseToPay() != null ? lastCollectionContractHistory.getEndPeriodPromiseToPay() : null;
            }
            return null;
        }
    }


    private long getHour(Long millis) {
        if (millis != null) {
            long h = (millis / (1000 * 60 * 60));
            return h;
        }
        return 0;
    }

    private class TotalAmountNotPaidPlusVAT extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            Collection collection=((Collection) getEntity());
            Double tiTotalAmountNotPaid=0d;
            Double vatTotalAmountNotPaid=0d;
            if(collection.getTiTotalAmountNotPaid()!=null){
                tiTotalAmountNotPaid= collection.getTiTotalAmountNotPaid();
            }
            if(collection.getVatTotalAmountNotPaid()!=null){
                vatTotalAmountNotPaid=collection.getVatTotalAmountNotPaid();
            }
            return tiTotalAmountNotPaid+vatTotalAmountNotPaid;
        }
    }
}
