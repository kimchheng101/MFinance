package com.soma.mfinance.gui.ui.panel.report.lessee.info;

import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.service.ContractService;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.shared.accounting.LeasesReport;
import com.soma.mfinance.core.shared.contract.ContractEntityField;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.mfinance.glf.accounting.service.GLFLeasingAccountingService;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.amount.Amount;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by t.daun on 7/7/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(LeaseInformationAnalysisPanel.NAME)
public class LeaseInformationAnalysisPanel extends AbstractTabPanel implements View,
        ContractEntityField {

    public static final String NAME = "lease.info.analysis.report";

    @Autowired
    private GLFLeasingAccountingService accountingService = SpringUtils.getBean(GLFLeasingAccountingService.class);
    @Autowired
    private ContractService contractService;
    private TabSheet tabSheet;

    private SimplePagedTable<LeasesReport> pagedTable;
    private List<ColumnDefinition> columnDefinitions;
    private ERefDataComboBox<EDealerType> cbxDealerType;

    private DealerComboBox cbxDealer;
    private TextField txtReference;
    private TextField txtFamilyName;
    private TextField txtFirstName;
    private AutoDateField dfCalculDate;
    private AutoDateField dfStartDate;
    private AutoDateField dfEndDate;
    private VerticalLayout errorMsgSearchingPanel;

    /**
     *
     */
    public LeaseInformationAnalysisPanel() {
        super();
        setSizeFull();
    }


    /**
     * @return
     */
    @SuppressWarnings("serial")
    @Override
    protected com.vaadin.ui.Component createForm() {
        tabSheet = new TabSheet();

        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setSpacing(true);
        contentLayout.setMargin(true);

        VerticalLayout gridLayoutPanel = new VerticalLayout();
        VerticalLayout searchLayout = new VerticalLayout();
        HorizontalLayout buttonsLayout = new HorizontalLayout();
        Button btnSearch = new Button(I18N.message("search"));
        btnSearch.setClickShortcut(ShortcutAction.KeyCode.ENTER, null); // null it means we
        // don't modify key
        // of shortcut
        // Enter(default =
        // 13)
        btnSearch.setIcon(new ThemeResource(
                "../smt-default/icons/16/search.png"));
        btnSearch.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                search();
            }
        });

        Button btnReset = new Button(I18N.message("reset"));
        btnReset.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                reset();
            }
        });
        buttonsLayout.setSpacing(true);
        buttonsLayout.addComponent(btnSearch);
        buttonsLayout.addComponent(btnReset);

        final GridLayout gridLayout = new GridLayout(20, 3);
        gridLayout.setSpacing(true);
        cbxDealer = new DealerComboBox(null, DataReference.getInstance()
                .getDealers(), I18N.message("all"));
        cbxDealer.setSelectedEntity(null);
        cbxDealer.setWidth("220px");

        cbxDealerType = new ERefDataComboBox<EDealerType>(EDealerType.class);
        cbxDealerType.setImmediate(true);
        cbxDealerType.addValueChangeListener(new Property.ValueChangeListener() {
            /** */
            private static final long serialVersionUID = -6774641791917653706L;

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                BaseRestrictions<Dealer> restrictions = new BaseRestrictions<Dealer>(
                        Dealer.class);
                restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
                if (cbxDealerType.getSelectedEntity() != null) {
                    restrictions.addCriterion(Restrictions.eq("dealerType",
                            cbxDealerType.getSelectedEntity()));
                }
                cbxDealer.setDealers(ENTITY_SRV.list(restrictions));
                cbxDealer.setSelectedEntity(null);
            }
        });
        txtFamilyName = ComponentFactory.getTextField(false, 60, 200);
        txtFirstName = ComponentFactory.getTextField(false, 60, 200);

        txtReference = ComponentFactory.getTextField(false, 20, 150);
        dfCalculDate = ComponentFactory.getAutoDateField("", false);
        dfCalculDate.setValue(DateUtils.today());
        dfStartDate = ComponentFactory.getAutoDateField("", false);
        dfStartDate.setValue(DateUtils.today());
        dfEndDate = ComponentFactory.getAutoDateField("", false);
        dfEndDate.setValue(DateUtils.today());

        int iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("contract.reference")), iCol++, 0);
        gridLayout.addComponent(txtReference, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("dealer.type")), iCol++, 0);
        gridLayout.addComponent(cbxDealerType, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("dealer")), iCol++, 0);
        gridLayout.addComponent(cbxDealer, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("calcul.date")), iCol++, 0);
        gridLayout.addComponent(dfCalculDate, iCol++, 0);

        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("lastname.en")), iCol++, 1);
        gridLayout.addComponent(txtFamilyName, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("firstname.en")), iCol++, 1);
        gridLayout.addComponent(txtFirstName, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("start.date")), iCol++, 1);
        gridLayout.addComponent(dfStartDate, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("end.date")), iCol++, 1);
        gridLayout.addComponent(dfEndDate, iCol++, 1);

        gridLayoutPanel.addComponent(gridLayout);

        searchLayout.setMargin(true);
        searchLayout.setSpacing(true);
        searchLayout.addComponent(gridLayoutPanel);
        searchLayout.addComponent(buttonsLayout);

        Panel searchPanel = new Panel();
        searchPanel.setCaption(I18N.message("search"));
        searchPanel.setContent(searchLayout);

        this.columnDefinitions = createColumnDefinitions();
        pagedTable = new SimplePagedTable<LeasesReport>(this.columnDefinitions);

        errorMsgSearchingPanel = new VerticalLayout();
        errorMsgSearchingPanel.setMargin(true);
        errorMsgSearchingPanel.setVisible(false);
        errorMsgSearchingPanel.addStyleName("message");
        contentLayout.addComponent(errorMsgSearchingPanel);

        contentLayout.addComponent(searchPanel);
        contentLayout.addComponent(pagedTable);
        contentLayout.addComponent(pagedTable.createControls());

        tabSheet.addTab(contentLayout, I18N.message("Lease Information Analysis Report"));

        return tabSheet;
    }

    private BaseRestrictions<Contract> getRestrictions() {

        BaseRestrictions<Contract> restrictions = new BaseRestrictions<Contract>(Contract.class);
        restrictions.addAssociation("contractApplicants", "contractapp", JoinType.INNER_JOIN);
        restrictions.addAssociation("contractapp.applicant", "app", JoinType.INNER_JOIN);
        restrictions.addAssociation("dealer", "dea", JoinType.INNER_JOIN);
        restrictions.addAssociation("quotations", "quo", JoinType.INNER_JOIN);
        restrictions.addCriterion("contractapp.applicantType", EApplicantType.C);
        restrictions.addCriterion(Restrictions.eq("quo.wkfStatus", QuotationWkfStatus.ACT));

        if (org.apache.commons.lang3.StringUtils.isNotEmpty(txtReference.getValue())) {
            restrictions.addCriterion(Restrictions.like(REFERENCE, txtReference.getValue(), MatchMode.ANYWHERE));
        }
        if (cbxDealerType.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq("dea.dealerType", cbxDealerType.getSelectedEntity()));
        }
        if (dfStartDate.getValue() != null) {
            restrictions.addCriterion(Restrictions.ge(START_DATE, DateUtils.getDateAtBeginningOfDay(dfStartDate.getValue())));
        }
        if (dfEndDate.getValue() != null) {
            restrictions.addCriterion(Restrictions.le(START_DATE, DateUtils.getDateAtEndOfDay(dfEndDate.getValue())));
        }
        if (cbxDealer.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, cbxDealer.getSelectedEntity().getId()));
        }

        if (org.apache.commons.lang3.StringUtils.isNotEmpty(txtFamilyName.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("app." + LAST_NAME_EN, txtFamilyName.getValue(), MatchMode.ANYWHERE));
        }
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(txtFirstName.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("app." + FIRST_NAME_EN, txtFirstName.getValue(), MatchMode.ANYWHERE));
        }

        restrictions.addOrder(Order.desc(START_DATE));
        return restrictions;
    }

    /**
     * reset
     */
    public void reset() {
        cbxDealerType.setSelectedEntity(null);
        cbxDealer.setSelectedEntity(null);
        txtReference.setValue("");
        txtFamilyName.setValue("");
        txtFirstName.setValue("");
        dfCalculDate.setValue(DateUtils.today());
        dfStartDate.setValue(DateUtils.today());
        dfEndDate.setValue(DateUtils.today());
    }

    /**
     * Search
     */
    private void search() {
        if (!validateBeforSearch()) {
            displayErrorMsgSearchingPanel();
            return;
        } else {
            removeErrorMsgSearchingPanel();
        }
        List<LeasesReport> leasesReports = accountingService.getLeaseReports(dfCalculDate.getValue(), getRestrictions());
        setIndexedContainer(leasesReports);

    }

    /**
     * @param leases
     */
    @SuppressWarnings("unchecked")
    private void setIndexedContainer(List<LeasesReport> leases) {
        Container.Indexed indexedContainer = pagedTable.getContainerDataSource();
        indexedContainer.removeAllItems();

        for (LeasesReport lease : leases) {

            Item item = indexedContainer.addItem(lease.getId());
            item.getItemProperty(ID).setValue(lease.getQuotationID());
            item.getItemProperty(REFERENCE).setValue(lease.getLidNo());
            Date endDate = dfEndDate.getValue();
            String contractStatus = lease.getContractStatus().getDesc();
            if (endDate == null) {
                endDate = DateUtils.today();
            }
            /*if (lease.getChangeStatusDate().after(endDate) && lease.getPreviouseContractStatus() != null) {
                contractStatus = lease.getPreviouseContractStatus().getDesc();
            }*/

            item.getItemProperty(CONTRACT_STATUS).setValue(contractStatus);
            item.getItemProperty(FULL_NAME).setValue(lease.getFullName());
            item.getItemProperty("business.type").setValue(lease.getBusinessType());
            item.getItemProperty("business.industry").setValue(lease.getBusinessIndustry());
            item.getItemProperty("Occupation").setValue(lease.getOccupation());
            item.getItemProperty("position").setValue(lease.getPosition());
            item.getItemProperty("marital.status").setValue(lease.getMaritalStatus());
            item.getItemProperty("age").setValue(lease.getAge());
            item.getItemProperty("household").setValue(lease.getPropertyAddressType());
            item.getItemProperty(MOBILEPHONE).setValue(lease.getTel());
            item.getItemProperty(VILLAGE).setValue(lease.getVillage());
            item.getItemProperty(COMMUNE).setValue(lease.getCommune());
            item.getItemProperty(DISTRICT).setValue(lease.getDistrict());
            item.getItemProperty(PROVINCE).setValue(lease.getProvince());
            item.getItemProperty("sex").setValue(lease.getSex());
            item.getItemProperty("dealer" + NAME_EN).setValue(lease.getDealerName());
            item.getItemProperty(ASSET_MODEL).setValue(lease.getAssetModel());
            item.getItemProperty("credit.officer").setValue(lease.getCoName());
            item.getItemProperty("production.officer").setValue(lease.getPoNo());
            item.getItemProperty("application.type").setValue(lease.getApplicationType());
            item.getItemProperty("lease.amount").setValue(AmountUtils.convertToAmount(lease.getLeasAmount()));
            item.getItemProperty("lease.amount.percentage").setValue(AmountUtils.convertToAmount(lease.getLeaseAmountPercentage()));
            item.getItemProperty(START_DATE).setValue(lease.getDateOfContract());
            if (lease.getLastPaidInstallmentDate() != null) {
                item.getItemProperty("num.installment").setValue(lease.getNumInstallment());
            } else {
                item.getItemProperty("num.installment").setValue(0);
            }
            item.getItemProperty("term").setValue(lease.getTerm());
            //item.getItemProperty("advance.payment.percentage").setValue(AmountUtils.convertToAmount(lease.getAdvPaymentPer()));
            item.getItemProperty("nbOverdueInDays").setValue(lease.getNbOverdueInDays());
            item.getItemProperty("realPrinBalance").setValue(AmountUtils.convertToAmount(lease.getRealPrinBalanceAnaly()));
            item.getItemProperty("underwriter").setValue(lease.getUnderwriter());
            item.getItemProperty("underwriterSupervisor").setValue(lease.getUnderwriterSupervisor());
            item.getItemProperty("ratio.pos").setValue(AmountUtils.convertToAmount(lease.getRatio()));
            item.getItemProperty("ratio.uw").setValue(AmountUtils.convertToAmount(lease.getRatioUw()));
            item.getItemProperty("applicant.income").setValue(AmountUtils.convertToAmount(lease.getSalary()));
            item.getItemProperty("allowance").setValue(AmountUtils.convertToAmount(lease.getAllowance()));
            item.getItemProperty("businessExpense").setValue(AmountUtils.convertToAmount(lease.getBusinessExpense()));
            item.getItemProperty("personalExpense").setValue(AmountUtils.convertToAmount(lease.getPersonalExpense()));
            item.getItemProperty("familyExpense").setValue(AmountUtils.convertToAmount(lease.getFamilyExpense()));
            item.getItemProperty("liabilities").setValue(AmountUtils.convertToAmount(lease.getLiabilities()));
            item.getItemProperty("authoritiesConfirmation").setValue(lease.getAuthoritiesConfirmation());
            item.getItemProperty("workPlaceConfirmation").setValue(lease.getWorkPlaceConfirmation());
            item.getItemProperty("collectionOfficer").setValue(lease.getCollectionOfficer());
            item.getItemProperty("collection.status").setValue(lease.getCollectionStatus());

        }
        pagedTable.refreshContainerDataSource();
    }

    /**
     * @return List of ColumnDefinition
     */
    protected List<ColumnDefinition> createColumnDefinitions() {
        columnDefinitions = new ArrayList<ColumnDefinition>();
        columnDefinitions.add(new ColumnDefinition(ID, I18N.message("id"), Long.class, Table.Align.LEFT, 70));
        columnDefinitions.add(new ColumnDefinition(REFERENCE, I18N.message("contract.reference"), String.class, Table.Align.LEFT, 140));
        columnDefinitions.add(new ColumnDefinition(CONTRACT_STATUS, I18N.message("contract.status"), String.class, Table.Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition(FULL_NAME, I18N.message("fullname"), String.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition("business.type", I18N.message("business.type"), String.class, Table.Align.LEFT, 140));
        columnDefinitions.add(new ColumnDefinition("business.industry", I18N.message("business.industry"), String.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition("Occupation", I18N.message("Occupation"), String.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition("position", I18N.message("position"), String.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition("marital.status", I18N.message("marital.status"), String.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition("age", I18N.message("age"), Integer.class, Table.Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("household", I18N.message("household"), String.class, Table.Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition(MOBILEPHONE, I18N.message("mobile.phone"), String.class, Table.Align.LEFT, 80));
        columnDefinitions.add(new ColumnDefinition(VILLAGE, I18N.message("village"), String.class, Table.Align.LEFT, 120));
        columnDefinitions.add(new ColumnDefinition(COMMUNE, I18N.message("commune"), String.class, Table.Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition(DISTRICT, I18N.message("district"), String.class, Table.Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition(PROVINCE, I18N.message("province"), String.class, Table.Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("sex", I18N.message("gender"), String.class, Table.Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("dealer" + NAME_EN, I18N.message("dealer"), String.class, Table.Align.LEFT, 140));
        columnDefinitions.add(new ColumnDefinition(ASSET_MODEL, I18N.message("Asset Model"), String.class, Table.Align.LEFT, 140));
        columnDefinitions.add(new ColumnDefinition("credit.officer", I18N.message("credit.officer"), String.class, Table.Align.LEFT, 120));
        columnDefinitions.add(new ColumnDefinition("production.officer", I18N.message("production.officer"), String.class, Table.Align.LEFT, 120));
        columnDefinitions.add(new ColumnDefinition("application.type", I18N.message("application.type"), String.class, Table.Align.LEFT, 120));
        columnDefinitions.add(new ColumnDefinition("lease.amount", I18N.message("lease.amount"), Amount.class, Table.Align.LEFT, 120));
        columnDefinitions.add(new ColumnDefinition("lease.amount.percentage", I18N.message("lease.amount.percentage"), Amount.class, Table.Align.LEFT, 120));
        columnDefinitions.add(new ColumnDefinition(START_DATE, I18N.message("start.date"), Date.class, Table.Align.LEFT, 110));
        columnDefinitions.add(new ColumnDefinition("num.installment", I18N.message("num.installment"), Integer.class, Table.Align.LEFT, 110));
        columnDefinitions.add(new ColumnDefinition("term", I18N.message("term"), Integer.class, Table.Align.LEFT, 60));
        columnDefinitions.add(new ColumnDefinition("nbOverdueInDays", I18N.message("nb.overdue.in.days"), Integer.class, Table.Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("realPrinBalance", I18N.message("real.principal.balance"), Amount.class, Table.Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("underwriter", I18N.message("underwriter"), String.class, Table.Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("underwriterSupervisor", I18N.message("Underwriter Supervisor"), String.class, Table.Align.RIGHT, 150));
        //columnDefinitions.add(new ColumnDefinition("advance.payment.percentage", I18N.message("advance.payment.percentage"), Amount.class, Table.Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("ratio.pos", I18N.message("ratio.pos"), Amount.class, Table.Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("ratio.uw", I18N.message("ratio.uw"), Amount.class, Table.Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("applicant.income", I18N.message("applicant.income"), Amount.class, Table.Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("allowance", I18N.message("allowance"), Amount.class, Table.Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("businessExpense", I18N.message("business.expense"), Amount.class, Table.Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("personalExpense", I18N.message("personal.expenses"), Amount.class, Table.Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("familyExpense", I18N.message("family.expenses"), Amount.class, Table.Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("liabilities", I18N.message("liabilities"), Amount.class, Table.Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("authoritiesConfirmation", I18N.message("authorities.confirmation"), String.class, Table.Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("workPlaceConfirmation", I18N.message("work.place.confirmation"), String.class, Table.Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("collectionOfficer", I18N.message("collection.officer"), String.class, Table.Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition("collection.status", I18N.message("collection.status"), String.class, Table.Align.RIGHT, 80));
        return columnDefinitions;
    }

    /**
     * Refresh
     */
    public void refresh() {
        search();
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }

    private boolean validateBeforSearch() {
        removeErrorMsgSearchingPanel();
        return errors.isEmpty();
    }

    private void displayErrorMsgSearchingPanel() {
        for (String error : errors) {
            Label messageLabel = new Label(error);
            messageLabel.addStyleName("error");
            errorMsgSearchingPanel.addComponent(messageLabel);
            errorMsgSearchingPanel.setVisible(true);
        }

    }

    public void removeErrorMsgSearchingPanel() {
        errorMsgSearchingPanel.removeAllComponents();
        errorMsgSearchingPanel.setVisible(false);
        errors.clear();
    }


}
