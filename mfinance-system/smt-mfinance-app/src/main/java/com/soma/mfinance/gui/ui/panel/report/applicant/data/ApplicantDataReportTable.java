package com.soma.mfinance.gui.ui.panel.report.applicant.data;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.applicant.model.Employment;
import com.soma.mfinance.core.applicant.model.Individual;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.entityfield.InstallmentEntityField;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationApplicant;
import com.soma.mfinance.core.quotation.model.QuotationDocument;
import com.soma.mfinance.core.utils.StringUtil;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.ersys.core.hr.model.address.Address;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.amount.AmountUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author p.ly
 */
public class ApplicantDataReportTable implements InstallmentEntityField, FinServicesHelper {

    private List<ColumnDefinition> columnDefinitions;
    private SimplePagedTable<Quotation> pagedTable;

    public ApplicantDataReportTable() {

    }

    public List<ColumnDefinition> getHeader() {

        columnDefinitions = new ArrayList<>();
        columnDefinitions.add(new ColumnDefinition("date.issued", I18N.message("Date Issued").toUpperCase(), Date.class, Align.CENTER, 140));
        columnDefinitions.add(new ColumnDefinition("family.name", I18N.message("Family Name").toUpperCase(), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("first.name", I18N.message("First Name").toUpperCase(), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("dealer", I18N.message("dealer").toUpperCase(), String.class, Align.LEFT, 170));
        columnDefinitions.add(new ColumnDefinition("GLF account number", I18N.message("GLF account number").toUpperCase(), String.class, Align.LEFT, 170));
        columnDefinitions.add(new ColumnDefinition("gender", I18N.message("gender").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("age", I18N.message("age").toUpperCase(), Integer.class, Align.LEFT, 50));
        columnDefinitions.add(new ColumnDefinition("phone.number1", I18N.message("Phone number 1").toUpperCase(), String.class, Align.CENTER, 100));
        columnDefinitions.add(new ColumnDefinition("phone.number2", I18N.message("Phone number 2").toUpperCase(), String.class, Align.CENTER, 100));
        columnDefinitions.add(new ColumnDefinition("phone.number.guarantor", I18N.message("Phone number Guarantor").toUpperCase(), String.class, Align.CENTER, 100));
        columnDefinitions.add(new ColumnDefinition("marital.status", I18N.message("Marital Status").toUpperCase(), String.class, Align.CENTER, 100));
        columnDefinitions.add(new ColumnDefinition("children", I18N.message("Children").toUpperCase(), Integer.class, Align.CENTER, 40));
        columnDefinitions.add(new ColumnDefinition("housing", I18N.message("Housing").toUpperCase(), String.class, Align.RIGHT, 40));
        columnDefinitions.add(new ColumnDefinition("district", I18N.message("District").toUpperCase(), String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("provinces", I18N.message("Provinces").toUpperCase(), String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("employment.status", I18N.message("Employment Status").toUpperCase(), String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("employment.industry", I18N.message("Employment Industry").toUpperCase(), String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("seniority.level", I18N.message("Seniority Level").toUpperCase(), String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("position", I18N.message("Position").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("work.place.name", I18N.message("Work Place Name").toUpperCase(), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("way.of.knowing", I18N.message("way.of.knowing"), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("basic.salary", I18N.message("Basic Salary").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("allowance", I18N.message("Allowance").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("business.expenses", I18N.message("Business Expenses").toUpperCase(), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("family.book", I18N.message("Family Book").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("passport", I18N.message("Passport").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("resident.book", I18N.message("Resident Book").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("birth.certificate", I18N.message("Birth Certificate").toUpperCase(), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("village.certified.letter", I18N.message("Village Certified Letter").toUpperCase(), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("voter.registration.card", I18N.message("Voter Registration Card").toUpperCase(), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("driving.licence", I18N.message("Driving Licence").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("utilities.bill", I18N.message("Utilities Bill").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("rent.contract", I18N.message("Rent Contract").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("salary.slip", I18N.message("Salary Slip").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("working.contract", I18N.message("Working Contract").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("bank.book", I18N.message("Bank Book").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("land.title", I18N.message("Land Title").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("patent", I18N.message("Patent").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("other", I18N.message("Other").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("ratio", I18N.message("Ratio").toUpperCase(), String.class, Align.LEFT, 40));

        columnDefinitions.add(new ColumnDefinition("guarantor.familyname", I18N.message("Family Name").toUpperCase(), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.firstname", I18N.message("First Name").toUpperCase(), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.gender", I18N.message("Gender").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("guarantor.age", I18N.message("Age").toUpperCase(), Integer.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("guarantor.marital.status", I18N.message("Marital Status").toUpperCase(), String.class, Align.CENTER, 70));
        columnDefinitions.add(new ColumnDefinition("guarantor.district", I18N.message("District").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("guarantor.provinces", I18N.message("Provinces").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("guarantor.relationship.with.applicant", I18N.message("Relationship With Applicant").toUpperCase(), String.class, Align.RIGHT, 150));
        columnDefinitions.add(new ColumnDefinition("guarantor.employment.status", I18N.message("Employment Status").toUpperCase(), String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.employment.industry", I18N.message("Employment Industry").toUpperCase(), String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.seniority.level", I18N.message("Seniority Level").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("guarantor.position", I18N.message("Position").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("guarantor.work.place.name", I18N.message("Work Place Name").toUpperCase(), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.basic.salary", I18N.message("Basic Salary").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("guarantor.allowance", I18N.message("Allowance").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("guarantor.business.expenses", I18N.message("Business Expenses").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("guarantor.live.with.applicant", I18N.message("Live With Applicant").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("guarantor.family.book", I18N.message("Family Book").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("guarantor.passport", I18N.message("Passport").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("guarantor.resident.book", I18N.message("Resident Book").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("guarantor.birth.certificate", I18N.message("Birth Certificate").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("guarantor.village.certified.letter", I18N.message("Village Certified Letter").toUpperCase(), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.voter.registration.card", I18N.message("Voter Registration Card").toUpperCase(), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("guarantor.driving.licence", I18N.message("Driving Licence").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("guarantor.utilities.bill", I18N.message("Utilities Bill").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("guarantor.rent.contract", I18N.message("Rent contract").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("guarantor.salary.slip", I18N.message("Salary Slip").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("guarantor.working.contract", I18N.message("Working Contract").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("guarantor.bank.book", I18N.message("Bank Book").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("guarantor.land.title", I18N.message("Land Title").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("guarantor.patent", I18N.message("Patent").toUpperCase(), String.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("guarantor.other", I18N.message("Other").toUpperCase(), String.class, Align.LEFT, 40));

        columnDefinitions.add(new ColumnDefinition("financial.product", I18N.message("Financial Product").toUpperCase(), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("asset.Range", I18N.message("Asset Range").toUpperCase(), String.class, Align.LEFT, 70));
        columnDefinitions.add(new ColumnDefinition("lease.amount.percentage", I18N.message("% Lease Amount").toUpperCase(), Double.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("interest.rate", I18N.message("Interest Rate").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("term", I18N.message("Term").toUpperCase(), Integer.class, Align.LEFT, 40));
        columnDefinitions.add(new ColumnDefinition("insurance", I18N.message("insurance").toUpperCase(), String.class, Align.LEFT, 50));
        columnDefinitions.add(new ColumnDefinition("closed", I18N.message("closed").toUpperCase(), String.class, Align.LEFT, 50));
        columnDefinitions.add(new ColumnDefinition("asset.price", I18N.message("Asset Price").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("year.of.motor.cycle", I18N.message("Year of Motor Cycle").toUpperCase(), String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("lease.amount", I18N.message("Lease Amount").toUpperCase(), String.class, Align.LEFT, 80));
        columnDefinitions.add(new ColumnDefinition("base.salary", I18N.message("Base Salary").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("allowance.asset", I18N.message("allowance").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("business.expend", I18N.message("Business Expand").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("net.income", I18N.message("Net Income").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("personal.expenses", I18N.message("Personal Expenses").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("family.expenses", I18N.message("Family Expenses").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("liability", I18N.message("Liability").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("disposable.income", I18N.message("Disposable Income").toUpperCase(), String.class, Align.RIGHT, 70));
        columnDefinitions.add(new ColumnDefinition("installment", I18N.message("GLF Installment").toUpperCase(), String.class, Align.RIGHT, 70));

        return columnDefinitions;
    }

    public void getApplicantDataReport(List<Quotation> quotations) {
        Indexed indexedContainer = pagedTable.getContainerDataSource();
        indexedContainer.removeAllItems();
        int index = 0;
        if (quotations != null && !quotations.isEmpty()) {
            for (Quotation quotation : quotations) {
                Applicant applicant = quotation.getApplicant();
                Applicant guarantorApplicant = quotation.getGuarantor();
                if (applicant != null) {
                    Individual individual = applicant.getIndividual();
                    Address appAddress = individual.getMainAddress();
                    Employment employment = individual.getCurrentEmployment();
                    String assetRangeDesc = "";
                    if (quotation.getAsset() != null) {
                        assetRangeDesc = quotation.getAsset().getAssetRange().getDescEn();
                    }
                    final Item item = indexedContainer.addItem(index);
                    /**
                     * RENDER APPLICANT
                     */
                    renderItemApplicant(item, applicant, quotation, individual, appAddress, employment);

                    /**
                     * RENDER ONE
                     */
                    renderItemGuarantor(item, guarantorApplicant, quotation);

                    /**
                     * ASSET
                     */
                    renderItemAsset(item, individual, quotation, assetRangeDesc);

                    index++;
                }
            }
        }
        pagedTable.refreshContainerDataSource();

    }

    private void renderItemApplicant(Item item, Applicant applicant, Quotation quotation, Individual individual, Address appAddress, Employment employment) {
        if (applicant != null) {
            String appfamilyBook = "";
            String appPassport = "";
            String appResidentBook = "";
            String appBirthCertificate = "";
            String appVillageCertifiedLetter = "";
            String appVoterRegistrationCard = "";
            String appDrivingLicence = "";
            String appUtilitiesBill = "";
            String appRentContract = "";
            String appSalarySlip = "";
            String appWorkingContract = "";
            String appBankBook = "";
            String appLandTitle = "";
            String appPatent = "";
            String appOther = "";
            List<QuotationDocument> documents = quotation.getQuotationDocuments(EApplicantType.C);
            if(documents != null &&  !documents.isEmpty()){
                String documentCodeApp = getDocumentCode(documents);
                if (documentCodeApp != null) {

                    if (documentCodeApp.equals("F")) {
                        appfamilyBook = "Yes";
                    }
                    if (documentCodeApp.equals("P")) {
                        appPassport = "Yes";
                    }
                    if (documentCodeApp.equals("R")) {
                        appResidentBook = "Yes";
                    }
                    if (documentCodeApp.equals("B")) {
                        appBirthCertificate = "Yes";
                    }
                    if (documentCodeApp.equals("G")) {
                        appVillageCertifiedLetter = "Yes";
                    }
                    if (documentCodeApp.equals("V")) {
                        appVoterRegistrationCard = "Yes";
                    }
                    if (documentCodeApp.equals("D")) {
                        appDrivingLicence = "Yes";
                    }
                    if (documentCodeApp.equals("BIL")) {
                        appUtilitiesBill = "Yes";
                    }
                    if (documentCodeApp.equals("REN")) {
                        appRentContract = "Yes";
                    }
                    if (documentCodeApp.equals("SAL")) {
                        appSalarySlip = "Yes";
                    }
                    if (documentCodeApp.equals("WOR")) {
                        appWorkingContract = "Yes";
                    }
                    if (documentCodeApp.equals("BAN")) {
                        appBankBook = "Yes";
                    }
                    if (documentCodeApp.equals("LAN")) {
                        appLandTitle = "Yes";
                    }
                    if (documentCodeApp.equals("PAT")) {
                        appPatent = "Yes";
                    }
                    if (documentCodeApp.equals("O")) {
                        appOther = "Yes";
                    }
                }
            }

            String seniorityLevel = "";
            if (employment != null && employment.getSeniorityLevel() != null) {
                seniorityLevel = employment.getSeniorityLevel().getDescEn();
            }
            if (individual != null) {
                item.getItemProperty("family.name").setValue(individual.getLastNameEn());
            }
            if (individual != null) {
                item.getItemProperty("first.name").setValue(individual.getFirstNameEn());
            }
            if (individual != null && individual.getGender() != null) {
                item.getItemProperty("gender").setValue(StringUtil.stringValue(individual.getGender().getDescEn()));
            }
            if (individual != null && individual.getBirthDate() != null) {
                item.getItemProperty("age").setValue(DateUtils.getAge(individual.getBirthDate()));
            }
            if (individual != null && individual.getMobilePerso() != null) {
                item.getItemProperty("phone.number1").setValue(StringUtil.stringValue(individual.getMobilePerso()));
            }
            if (individual != null && individual.getMobilePerso2() != null) {
                item.getItemProperty("phone.number2").setValue(StringUtil.stringValue(individual.getMobilePerso2()));
            }
            if (individual != null && individual.getMaritalStatus() != null) {
                item.getItemProperty("marital.status").setValue(individual.getMaritalStatus().getDesc());
            }
            if (individual != null && individual.getNumberOfChildren() != null) {
                item.getItemProperty("children").setValue(individual.getNumberOfChildren());
            }
            if (individual != null && individual.getIndividualAddresses() != null && !individual.getIndividualAddresses().isEmpty() && individual.getIndividualAddresses().get(0) != null && individual.getIndividualAddresses().get(0).getHousing() != null) {
                item.getItemProperty("housing").setValue(individual.getIndividualAddresses().get(0).getHousing().getDescEn());
            }
            if (quotation.getDealer() != null) {
                item.getItemProperty("dealer").setValue(quotation.getDealer().getNameEn());
            }
            if (quotation.getReference() != null) {
                item.getItemProperty("GLF account number").setValue(quotation.getReference());
            }
            if (appAddress != null && appAddress.getDistrict() != null) {
                item.getItemProperty("district").setValue(appAddress.getDistrict().getDescEn());
            }
            if (appAddress != null && appAddress.getProvince() != null) {
                item.getItemProperty("provinces").setValue(appAddress.getProvince().getDescEn());
            }
            if (employment != null && employment.getEmploymentStatus() != null) {
                item.getItemProperty("employment.status").setValue(employment.getEmploymentStatus().getDescEn());
            }
            if (employment != null && employment.getEmploymentIndustry() != null) {
                item.getItemProperty("employment.industry").setValue(employment.getEmploymentIndustry().getDescEn());
            }
            if (employment != null && employment.getEmploymentPosition() != null) {
                item.getItemProperty("position").setValue(employment.getEmploymentPosition().getDescEn());
            }
            if (employment != null && employment.getWorkPlaceName() != null) {
                item.getItemProperty("work.place.name").setValue(employment.getWorkPlaceName());
            }
            if (quotation != null && quotation.getWayOfKnowing() != null) {
                item.getItemProperty("way.of.knowing").setValue(quotation.getWayOfKnowing().getDesc());
            }
            if (employment != null && employment.getRevenue() != null) {
                item.getItemProperty("basic.salary").setValue(AmountUtils.format(employment.getRevenue()));
            }
            if (employment != null && employment.getAllowance() != null) {
                item.getItemProperty("allowance").setValue(AmountUtils.format(employment.getAllowance()));
            }
            if (employment != null && employment.getBusinessExpense() != null) {
                item.getItemProperty("business.expenses").setValue(AmountUtils.format(employment.getBusinessExpense()));
            }
            item.getItemProperty("date.issued").setValue(applicant.getCreateDate() != null ? applicant.getCreateDate() : "");
            item.getItemProperty("seniority.level").setValue(seniorityLevel);
            item.getItemProperty("family.book").setValue(appfamilyBook);
            item.getItemProperty("passport").setValue(appPassport);
            item.getItemProperty("resident.book").setValue(appResidentBook);
            item.getItemProperty("birth.certificate").setValue(appBirthCertificate);
            item.getItemProperty("village.certified.letter").setValue(appVillageCertifiedLetter);
            item.getItemProperty("voter.registration.card").setValue(appVoterRegistrationCard);
            item.getItemProperty("driving.licence").setValue(appDrivingLicence);
            item.getItemProperty("utilities.bill").setValue(appUtilitiesBill);
            item.getItemProperty("rent.contract").setValue(appRentContract);
            item.getItemProperty("salary.slip").setValue(appSalarySlip);
            item.getItemProperty("working.contract").setValue(appWorkingContract);
            item.getItemProperty("bank.book").setValue(appBankBook);
            item.getItemProperty("land.title").setValue(appLandTitle);
            item.getItemProperty("patent").setValue(appPatent);
            item.getItemProperty("other").setValue(appOther);

        }
    }

    private void renderItemGuarantor(Item item, Applicant applicant, Quotation quotation) {
        if (applicant != null) {
            Individual guarantor = applicant.getIndividual();
            Address guaAddress = guarantor.getMainAddress();
            Employment guaEmployment = guarantor.getCurrentEmployment();
            String relationshipWithApp = "";
            if (guarantor.getQuotationApplicants() != null && !guarantor.getQuotationApplicants().isEmpty()) {
                QuotationApplicant quotationApplicant = guarantor.getQuotationApplicants().get(0);
                if (quotationApplicant.getRelationship() != null) {
                    relationshipWithApp = quotationApplicant.getRelationship().getDescEn();
                }
            }
            String guaSeniorityLevel = "";
            if (applicant.getIndividual() != null && applicant.getIndividual().getCurrentEmployment() != null && applicant.getIndividual().getCurrentEmployment().getSeniorityLevel() != null) {
                guaSeniorityLevel = applicant.getIndividual().getCurrentEmployment().getSeniorityLevel().getDescEn();
            }
            String liveWithApplicant = "No";
            if (guaEmployment != null && guaEmployment.isSameApplicantAddress()) {
                liveWithApplicant = "Yes";
            }
            String guafamilyBook = "";
            String guaPassport = "";
            String guaResidentBook = "";
            String guaBirthCertificate = "";
            String guaVillageCertifiedLetter = "";
            String guaVoterRegistrationCard = "";
            String guaDrivingLicence = "";
            String guaUtilitiesBill = "";
            String guaRentContract = "";
            String guaSalarySlip = "";
            String guaWorkingContract = "";
            String guaBankBook = "";
            String guaLandTitle = "";
            String guaPatent = "";
            String guaOther = "";
            List<QuotationDocument> guaDocuments = quotation.getQuotationDocuments(EApplicantType.G);
            if(guaDocuments != null &&  !guaDocuments.isEmpty()){
                String documentCodeGua =  getDocumentCode(guaDocuments);
                if(documentCodeGua != null){
                    if (documentCodeGua.equals("F")) {
                        guafamilyBook = "Yes";
                    }
                    if (documentCodeGua.equals("P")) {
                        guaPassport = "Yes";
                    }
                    if (documentCodeGua.equals("R")) {
                        guaResidentBook = "Yes";
                    }
                    if (documentCodeGua.equals("B")) {
                        guaBirthCertificate = "Yes";
                    }
                    if (documentCodeGua.equals("G")) {
                        guaVillageCertifiedLetter = "Yes";
                    }
                    if (documentCodeGua.equals("V")) {
                        guaVoterRegistrationCard = "Yes";
                    }
                    if (documentCodeGua.equals("D")) {
                        guaDrivingLicence = "Yes";
                    }
                    if (documentCodeGua.equals("BIL")) {
                        guaUtilitiesBill = "Yes";
                    }
                    if (documentCodeGua.equals("REN")) {
                        guaRentContract = "Yes";
                    }
                    if (documentCodeGua.equals("SAL")) {
                        guaSalarySlip = "Yes";
                    }
                    if (documentCodeGua.equals("WOR")) {
                        guaWorkingContract = "Yes";
                    }
                    if (documentCodeGua.equals("BAN")) {
                        guaBankBook = "Yes";
                    }
                    if (documentCodeGua.equals("LAN")) {
                        guaLandTitle = "Yes";
                    }
                    if (documentCodeGua.equals("PAT")) {
                        guaPatent = "Yes";
                    }
                    if (documentCodeGua.equals("O")) {
                        guaOther = "Yes";
                    }
                }
        }

            item.getItemProperty("guarantor.familyname").setValue(StringUtil.stringValue(guarantor.getLastNameEn()));
            item.getItemProperty("guarantor.firstname").setValue(StringUtil.stringValue(guarantor.getFirstNameEn()));
            if (guarantor.getGender() != null) {
                item.getItemProperty("guarantor.gender").setValue(StringUtil.stringValue(guarantor.getGender().getDescEn()));
            }
            if (guarantor.getBirthDate() != null) {
                item.getItemProperty("guarantor.age").setValue(MyNumberUtils.getInteger(DateUtils.getAge(guarantor.getBirthDate())));
            }
            if (guarantor.getMobilePerso() != null) {
                item.getItemProperty("phone.number.guarantor").setValue(StringUtil.stringValue(guarantor.getMobilePerso()));
            }
            if (guarantor.getMaritalStatus() != null) {
                item.getItemProperty("guarantor.marital.status").setValue(StringUtil.stringValue(guarantor.getMaritalStatus().getDesc()));
            }
            if (guaAddress != null && guaAddress.getDistrict() != null) {
                item.getItemProperty("guarantor.district").setValue(StringUtil.stringValue(guaAddress.getDistrict().getDescEn()));
            }
            if (guaAddress != null && guaAddress.getProvince() != null) {
                item.getItemProperty("guarantor.provinces").setValue(StringUtil.stringValue(guaAddress.getProvince().getDescEn()));
            }
            if (guaEmployment != null && guaEmployment.getEmploymentStatus() != null) {
                item.getItemProperty("guarantor.employment.status").setValue(StringUtil.stringValue(guaEmployment.getEmploymentStatus().getDescEn()));
            }
            if (guaEmployment != null && guaEmployment.getEmploymentIndustry() != null) {
                item.getItemProperty("guarantor.employment.industry").setValue(StringUtil.stringValue(guaEmployment.getEmploymentIndustry().getDescEn()));
            }
            if (guaEmployment != null && guaEmployment.getEmploymentPosition() != null) {
                item.getItemProperty("guarantor.position").setValue(StringUtil.stringValue(guaEmployment.getEmploymentPosition().getDescEn()));
            }
            if (guaEmployment != null && guaEmployment.getWorkPlaceName() != null) {
                item.getItemProperty("guarantor.work.place.name").setValue(StringUtil.stringValue(guaEmployment.getWorkPlaceName()));
            }
            if (guaEmployment != null && guaEmployment.getRevenue() != null) {
                item.getItemProperty("guarantor.basic.salary").setValue(AmountUtils.format(guaEmployment.getRevenue()));
            }
            if (guaEmployment != null && guaEmployment.getAllowance() != null) {
                item.getItemProperty("guarantor.allowance").setValue(AmountUtils.format(guaEmployment.getAllowance()));
            }
            if (guaEmployment != null && guaEmployment.getBusinessExpense() != null) {
                item.getItemProperty("guarantor.business.expenses").setValue(AmountUtils.format(guaEmployment.getBusinessExpense()));
            }

            item.getItemProperty("guarantor.relationship.with.applicant").setValue(StringUtil.stringValue(relationshipWithApp));
            item.getItemProperty("guarantor.seniority.level").setValue(StringUtil.stringValue(guaSeniorityLevel));
            item.getItemProperty("guarantor.live.with.applicant").setValue(StringUtil.stringValue(liveWithApplicant));
            item.getItemProperty("guarantor.family.book").setValue(StringUtil.stringValue(guafamilyBook));
            item.getItemProperty("guarantor.passport").setValue(StringUtil.stringValue(guaPassport));
            item.getItemProperty("guarantor.resident.book").setValue(StringUtil.stringValue(guaResidentBook));
            item.getItemProperty("guarantor.birth.certificate").setValue(StringUtil.stringValue(guaBirthCertificate));
            item.getItemProperty("guarantor.village.certified.letter").setValue(StringUtil.stringValue(guaVillageCertifiedLetter));
            item.getItemProperty("guarantor.voter.registration.card").setValue(StringUtil.stringValue(guaVoterRegistrationCard));
            item.getItemProperty("guarantor.driving.licence").setValue(StringUtil.stringValue(guaDrivingLicence));
            item.getItemProperty("guarantor.utilities.bill").setValue(StringUtil.stringValue(guaUtilitiesBill));
            item.getItemProperty("guarantor.rent.contract").setValue(StringUtil.stringValue(guaRentContract));
            item.getItemProperty("guarantor.salary.slip").setValue(StringUtil.stringValue(guaSalarySlip));
            item.getItemProperty("guarantor.working.contract").setValue(StringUtil.stringValue(guaWorkingContract));
            item.getItemProperty("guarantor.bank.book").setValue(StringUtil.stringValue(guaBankBook));
            item.getItemProperty("guarantor.land.title").setValue(StringUtil.stringValue(guaLandTitle));
            item.getItemProperty("guarantor.patent").setValue(StringUtil.stringValue(guaPatent));
            item.getItemProperty("guarantor.other").setValue(StringUtil.stringValue(guaOther));
        }
    }

    private void renderItemAsset(Item item, Individual individual, Quotation quotation, String assetRangeDesc) {
        double totalBaseSalary = 0d;
        double assetPrice = 0d;
        double leaseAmount = 0d;
        double totalAllowance = 0d;
        double totalBusinessExpense = 0d;
        double netIncome = 0d;

        List<Employment> employments = individual.getEmployments();
        for (Employment employment1 : employments) {
            totalBaseSalary += MyNumberUtils.getDouble(employment1.getRevenue());
            totalAllowance += MyNumberUtils.getDouble(employment1.getAllowance());
            totalBusinessExpense += MyNumberUtils.getDouble(employment1.getBusinessExpense());
        }

        assetPrice = MyNumberUtils.getDouble(quotation.getAsset().getTiAssetApprPrice());
        leaseAmount = MyNumberUtils.getDouble(quotation.getTiFinanceAmount());
        netIncome = (totalBaseSalary + totalAllowance) - totalBusinessExpense;

        double personalExpense = MyNumberUtils.getDouble(individual.getMonthlyPersonalExpenses());
        double familyExpense = MyNumberUtils.getDouble(individual.getMonthlyFamilyExpenses());
        double liability = MyNumberUtils.getDouble(individual.getTotalDebtInstallment());
        double totalDebtInstallment = MyNumberUtils.getDouble(individual.getTotalDebtInstallment());
        double totalExpenses = MyNumberUtils.getDouble(individual.getMonthlyPersonalExpenses())
                + MyNumberUtils.getDouble(individual.getMonthlyFamilyExpenses())
                + totalDebtInstallment;
        double disposableIncome = netIncome - totalExpenses;
        double glfInstallment = MyNumberUtils.getDouble(quotation.getTotalInstallmentAmount());
        double totalExpend = totalExpenses + liability;
        double ratio = (netIncome - totalExpend) / glfInstallment;
        Double InsuranceFee = 0d;
        if (quotation != null && quotation.getTiFinanceAmount() != null) {
            Double leaseAmountValue = quotation.getTiFinanceAmount();
            InsuranceFee = (FIN_PROD_SRV.getInsuranceFee(leaseAmountValue) * quotation.getTerm()) / 12;
        }

        item.getItemProperty("financial.product").setValue(quotation.getFinancialProduct().getDesc());
        item.getItemProperty("asset.Range").setValue(assetRangeDesc);
        item.getItemProperty("lease.amount.percentage").setValue(quotation.getLeaseAmountPercentage());
        item.getItemProperty("interest.rate").setValue(AmountUtils.format(quotation.getInterestRate()));
        item.getItemProperty("term").setValue(MyNumberUtils.getInteger(quotation.getTerm()));
        item.getItemProperty("insurance").setValue(AmountUtils.format(InsuranceFee));
        item.getItemProperty("closed").setValue(getContractStatus(quotation));
        item.getItemProperty("asset.price").setValue(AmountUtils.format(assetPrice));
        item.getItemProperty("year.of.motor.cycle").setValue(quotation.getAsset().getAssetYear().getDesc());
        item.getItemProperty("lease.amount").setValue(AmountUtils.format(leaseAmount));
        item.getItemProperty("base.salary").setValue(AmountUtils.format(totalBaseSalary));
        item.getItemProperty("allowance.asset").setValue(AmountUtils.format(totalAllowance));
        item.getItemProperty("business.expend").setValue(AmountUtils.format(totalBaseSalary));
        item.getItemProperty("net.income").setValue(AmountUtils.format(netIncome));
        item.getItemProperty("personal.expenses").setValue(AmountUtils.format(personalExpense));
        item.getItemProperty("family.expenses").setValue(AmountUtils.format(familyExpense));
        item.getItemProperty("liability").setValue(AmountUtils.format(liability));
        item.getItemProperty("disposable.income").setValue(AmountUtils.format(disposableIncome));
        item.getItemProperty("installment").setValue(AmountUtils.format(glfInstallment));
        item.getItemProperty("ratio").setValue(AmountUtils.format(ratio > 0 ? ratio : 0d));
    }

    private String getDocumentCode(List<QuotationDocument> quotationDocuments){
        if (quotationDocuments != null && !quotationDocuments.isEmpty()) {
            for (QuotationDocument quotationDocument : quotationDocuments) {
                if(quotationDocument.getDocument() != null){
                    return StringUtil.stringValue(quotationDocument.getDocument().getCode());
                }
            }
        }
        return null;
    }

    private String getContractStatus(Quotation quotation) {
        String statusContract = "N";
        if (quotation != null && quotation.getReference() != null) {
            if (quotation.getContract() != null) {
                Contract contract = CONT_SRV.getById(Contract.class, quotation.getContract().getId());
                if (contract != null) {
                    if (contract.getWkfStatus().equals(ContractWkfStatus.CLO) || contract.getWkfStatus().equals(ContractWkfStatus.EAR)) {
                        statusContract = "Y";
                    }
                }
            }
        }
        return statusContract;
    }

    public SimplePagedTable<Quotation> getPagedTable() {
        return pagedTable;
    }

    public void setPagedTable(SimplePagedTable<Quotation> pagedTable) {
        this.pagedTable = pagedTable;
    }

}
