package com.soma.mfinance.gui.ui.panel.report.last.payment.installment;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.seuksa.frmk.i18n.I18N;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ru.xpoft.vaadin.VaadinView;

import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.financial.service.FinanceCalculationService;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.payment.model.Payment;
import com.soma.mfinance.core.shared.mplus.accounting.InstallmentServiceMfp;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox.ButtonType;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
/**
 * 
 * @author p.ly
 *
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(LastInstallmentPaymentReportPanel.NAME)
public class LastInstallmentPaymentReportPanel extends AbstractTabPanel implements View, FinServicesHelper {
	
	private static final long serialVersionUID = 6227740006388204118L;

	protected Logger logger = LoggerFactory.getLogger(getClass());
	public static final String NAME = "last.installment.payment";
	private TabSheet tabSheet;
	private SimplePagedTable<Payment> pagedTable;
	private List<ColumnDefinition> columnDefinitions;
	private LastInstallmentPaymentReportSearchPanel lastInstallmentPaymentReportSearchPanel;
	private LastInstallmentPaymentReportTable lastInstallmentPaymentReportTable;
	private List<Contract> contracts;
	private Button btnSearch;
	private Date dfStartDate;
	private Date dfEndDate ;
	private int  numMonthBeforeMaturity;
	@PostConstruct
	public void PostConstruct() {
	}
	@Override
	public void enter(ViewChangeEvent event) {
		
	}

	@Override
	protected com.vaadin.ui.Component createForm() {
		lastInstallmentPaymentReportSearchPanel = new LastInstallmentPaymentReportSearchPanel();
		tabSheet = new TabSheet();
		VerticalLayout contentLayout = new VerticalLayout();
		contentLayout.setSpacing(true);
		contentLayout.setMargin(true);
		
		VerticalLayout gridLayoutPanel = new VerticalLayout();
		VerticalLayout searchLayout = new VerticalLayout();
		HorizontalLayout buttonsLayout = new HorizontalLayout();
		btnSearch = new Button(I18N.message("search"));
		btnSearch.setClickShortcut(KeyCode.ENTER, null); // null it means we don't modify key of shortcut Enter(default = 13)
		btnSearch.setIcon(new ThemeResource("../smt-default/icons/16/search.png"));
		btnSearch.addClickListener(new ClickListener() {		
			private static final long serialVersionUID = -3403059921454308342L;
			@Override
			public void buttonClick(ClickEvent event) {
				loadData();
			}
		});
		
		Button btnReset = new Button(I18N.message("reset"));
		btnReset.setIcon(new ThemeResource("../smt-default/icons/16/reset.png"));
		btnReset.addClickListener(new ClickListener() {
			private static final long serialVersionUID = -7165734546798826698L;
			@Override
			public void buttonClick(ClickEvent event) {
				reset();
			}
		});
		buttonsLayout.setSpacing(true);
		buttonsLayout.setStyleName("panel-search-center");
		buttonsLayout.addComponent(btnSearch);
		buttonsLayout.addComponent(btnReset);
        gridLayoutPanel.addComponent(lastInstallmentPaymentReportSearchPanel.getSearchForm());
        searchLayout.setMargin(true);
        searchLayout.setSpacing(true);
        searchLayout.addComponent(gridLayoutPanel);
        searchLayout.addComponent(buttonsLayout);
        
        Panel searchPanel = new Panel();
        searchPanel.setCaption(I18N.message("search"));
        searchPanel.setContent(searchLayout);
        
        lastInstallmentPaymentReportTable = new LastInstallmentPaymentReportTable(btnSearch);
        this.columnDefinitions = lastInstallmentPaymentReportTable.getHeader();
        pagedTable = new SimplePagedTable<Payment>(this.columnDefinitions);
        lastInstallmentPaymentReportTable.setPagedTable(pagedTable);
        contentLayout.addComponent(searchPanel);
        contentLayout.addComponent(pagedTable);
        contentLayout.addComponent(pagedTable.createControls());
        
        tabSheet.addTab(contentLayout, I18N.message("last.installment.payment"));
		
        return tabSheet;
	}
	/**
	 * 
	 */
	public void loadData(){
		contracts = lastInstallmentPaymentReportSearchPanel.getContracts();
		dfStartDate = lastInstallmentPaymentReportSearchPanel.startDate();
		dfEndDate = lastInstallmentPaymentReportSearchPanel.endDate();
		if (lastInstallmentPaymentReportSearchPanel.getNumMonthBeforeMaturity()!=null){
			numMonthBeforeMaturity = lastInstallmentPaymentReportSearchPanel.getNumMonthBeforeMaturity();
		}else{
			numMonthBeforeMaturity = 0;
		}
		lastInstallmentPaymentReportTable.getData(contracts ,dfStartDate ,dfEndDate ,numMonthBeforeMaturity);
	}
	
}
