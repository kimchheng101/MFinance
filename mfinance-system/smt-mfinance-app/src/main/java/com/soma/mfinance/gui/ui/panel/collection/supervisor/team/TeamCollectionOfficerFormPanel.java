package com.soma.mfinance.gui.ui.panel.collection.supervisor.team;

import com.soma.mfinance.core.address.model.Area;
import com.soma.mfinance.core.collection.model.ColAssignment;
import com.soma.mfinance.core.collection.model.EColResult;
import com.soma.mfinance.core.collection.model.EColTask;
import com.soma.mfinance.core.collection.model.OverduePeriod;
import com.soma.mfinance.core.common.IProfileCode;
import com.soma.mfinance.core.custom.component.TwinEntityRefListSelect;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.shared.system.FMProfile;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.widget.SecUserComboBox;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

import static com.soma.mfinance.core.helper.FinServicesHelper.COL_SRV;

/**
 * Created by sr.soth on 5/10/2017
 * @author s.torn
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TeamCollectionOfficerFormPanel extends AbstractFormPanel {
    private SecUser secUser;
    private SecUserComboBox cbxCollectionOfficer;
    private EntityRefComboBox<OverduePeriod> cbxOverduePeriod;
    private ERefDataComboBox<EColTask> cbxCollectionTask;

    private CheckBox cbDefaultAssignee;
    private CheckBox cbActive;
    private ColAssignment colAssignment;

    private TwinEntityRefListSelect<EColResult> twinCollectionStatus;
    private TwinEntityRefListSelect<Area> twinArea;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        setCaption("Team Collection Officer");
        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addSaveClickListener(this);

    }

    @Override
    protected com.vaadin.ui.Component createForm() {

     //   cbxCollectionOfficer = new SecUserComboBox(I18N.message("collection.officer"), COL_SRV.getCollectionUsers(new String[]{IProfileCode.CC}));
        cbxCollectionOfficer = new SecUserComboBox(I18N.message("collection.officer"),DataReference.getInstance().getUsers(FMProfile.CC, EStatusRecord.ACTIV));
        cbxCollectionOfficer.setImmediate(true);
        cbxCollectionOfficer.setRequired(true);

        cbxOverduePeriod = new EntityRefComboBox<OverduePeriod>("Overdue Period");

        BaseRestrictions<OverduePeriod> restrictions = new BaseRestrictions<>(OverduePeriod.class);
        List<OverduePeriod> overduePeriodList = ENTITY_SRV.list(restrictions);
        if (overduePeriodList != null && !overduePeriodList.isEmpty()) {
            cbxOverduePeriod.setRestrictions(restrictions);
            cbxOverduePeriod.setWidth(150, Unit.PIXELS);
            cbxOverduePeriod.setRequired(true);
            cbxOverduePeriod.renderer();
        }
        cbxCollectionTask = new ERefDataComboBox<EColTask>(I18N.message("task"), EColTask.class);
        cbxCollectionTask.setRequired(true);

        twinCollectionStatus = new TwinEntityRefListSelect("collection.status");
        twinCollectionStatus.setWidth(300);
        twinCollectionStatus.setRestrictions(new BaseRestrictions<EColResult>(EColResult.class));
        twinCollectionStatus.getResultListSelect().clear();
        twinCollectionStatus.renderer();

        twinArea = new TwinEntityRefListSelect("area");
        twinArea.setWidth(300);
        twinArea.setRestrictions(new BaseRestrictions<Area>(Area.class));
        twinArea.renderer();
        twinArea.getResultListSelect().clear();

        cbDefaultAssignee = new CheckBox("default.assignee");
        cbActive = new CheckBox("active",true);

        FormLayout formLayout = new FormLayout();
        formLayout.setSpacing(true);
        TeamCollectionOfficerComponent(formLayout);
        return formLayout;
    }

    private void TeamCollectionOfficerComponent(FormLayout formLayout) {
        formLayout.addComponent(cbxCollectionOfficer);
        formLayout.addComponent(cbxOverduePeriod);
        formLayout.addComponent(cbxCollectionTask);
        formLayout.addComponent(twinCollectionStatus);
        formLayout.addComponent(twinArea);
        formLayout.addComponent(cbDefaultAssignee);
        formLayout.addComponent(cbActive);
    }

    @Override
    protected ColAssignment getEntity() {
        if (colAssignment == null)
            colAssignment = new ColAssignment();
            colAssignment.setOverduePeriod(cbxOverduePeriod.getSelectedEntity());
            colAssignment.setCollectionTask(cbxCollectionTask.getSelectedEntity());

            secUser = ProfileUtil.getCurrentUser();
            colAssignment.setAssignedBy(secUser);
            colAssignment.setAssignee(cbxCollectionOfficer.getSelectedEntity());

            colAssignment.setStatuses(twinCollectionStatus.getResultListSelect().getAllEntities());
            colAssignment.setAreas(twinArea.getResultListSelect().getAllEntities());

            colAssignment.setDefaultAssignee(cbDefaultAssignee.getValue());
            colAssignment.setActive(cbActive.getValue());
        return colAssignment;

    }

    @Override
    public void saveEntity() {
        try {
            ColAssignment colAssignment = getEntity();
            ENTITY_SRV.saveOrUpdate(colAssignment);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void reset() {
        super.reset();

        colAssignment = new ColAssignment();
        cbxCollectionOfficer.setValue(null);
        secUser = null;
        cbxOverduePeriod.setSelectedEntity(null);
        cbxCollectionTask.setSelectedEntity(null);
        twinCollectionStatus.getResultListSelect().clear();
        twinArea.getResultListSelect().clear();
        twinCollectionStatus.renderer();
        twinArea.renderer();
    }

    @Override
    public boolean validate() {

        checkMandatorySelectField(cbxOverduePeriod, I18N.message("overdueperiod"));
        checkMandatorySelectField(cbxCollectionOfficer, I18N.message("collection.officer"));
        checkMandatorySelectField(cbxCollectionTask, I18N.message("collection.task"));
        if (twinCollectionStatus.isEmpty()){
            errors.add(I18N.message("Collection Status is required"));
        }
        if (twinArea.isEmpty()){
            errors.add(I18N.message("Area is required"));
        }
        return errors.isEmpty();
    }

    public void assignValue(Long id) {
        if (id != null) {
            BaseRestrictions<ColAssignment> restrictions = new BaseRestrictions<>(ColAssignment.class);
            restrictions.addCriterion(Restrictions.eq("id", id));
            List<ColAssignment> colAssignments = ENTITY_SRV.list(restrictions);

            if (colAssignments != null && !colAssignments.isEmpty()) {
                colAssignment = colAssignments.get(0);
            }
            colAssignment = ENTITY_SRV.getById(ColAssignment.class, id);
        }

        if (colAssignment != null) {
            cbxCollectionOfficer.setSelectedEntity(colAssignment.getAssignee());
            cbxOverduePeriod.setSelectedEntity(colAssignment.getOverduePeriod());
            cbxCollectionTask.setSelectedEntity(colAssignment.getCollectionTask());
            cbDefaultAssignee.setValue(colAssignment.isDefaultAssignee());
            cbActive.setValue(colAssignment.isActive());

            if (colAssignment.getStatuses() != null ){
                twinCollectionStatus.getResultListSelect().clear();
                twinCollectionStatus.resultSelectAddEntities(colAssignment.getStatuses());
                twinCollectionStatus.setSourceSelectEntities(colAssignment.getStatuses());
            }
            if(colAssignment.getAreas() != null ){
                twinArea.getResultListSelect().clear();
                twinArea.resultSelectAddEntities(colAssignment.getAreas());
                twinArea.setSourceSelectEntities(colAssignment.getAreas());
            }

        } else {
            reset();
            colAssignment = new ColAssignment();

        }
    }

}
