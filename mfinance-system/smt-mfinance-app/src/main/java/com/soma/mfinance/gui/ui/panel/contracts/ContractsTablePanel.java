package com.soma.mfinance.gui.ui.panel.contracts;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.ColumnRenderer;
import com.soma.frmk.vaadin.ui.widget.table.EntityColumnRenderer;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;

/**
 * 
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ContractsTablePanel extends AbstractTablePanel<Contract> implements FMEntityField {

	private static final long serialVersionUID = 4644194602140870212L;
	
	@PostConstruct
	public void PostConstruct() {
		setCaption(I18N.message("contracts"));
		setSizeFull();
		setMargin(true);
		setSpacing(true);

		//TODO : Contracts (Sreyrath)
		/*if(ProfileUtil.isAccountingController()) {
			setDisplayExportBar(false);
		}*/
		super.init(I18N.message("contracts"));
		
		NavigationPanel navigationPanel = addNavigationPanel();
		navigationPanel.addEditClickListener(this);
		navigationPanel.addRefreshClickListener(this);
	}
	
	/**
	 * Get Paged definition
	 * @return
	 */
	@Override
	protected PagedDataProvider<Contract> createPagedDataProvider() {
		PagedDefinition<Contract> pagedDefinition = new PagedDefinition<Contract>(searchPanel.getRestrictions());
		pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 70);
		pagedDefinition.addColumnDefinition(REFERENCE, I18N.message("reference"), String.class, Align.LEFT, 150);
		pagedDefinition.addColumnDefinition("financialProduct" + "." + DESC_EN, I18N.message("financial.product"), String.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition(CUSTOMER, I18N.message("customer"), String.class, Align.LEFT, 140, new CustomerNameColumnRender());
		pagedDefinition.addColumnDefinition(ASSET + ".model." + DESC_EN, I18N.message("asset.model"), String.class, Align.LEFT, 150);
		pagedDefinition.addColumnDefinition(ASSET + ".assetYear." + DESC_EN, I18N.message("asset.year"), String.class, Align.LEFT, 150);
		pagedDefinition.addColumnDefinition("financialProduct." + DESC_EN, I18N.message("financial.product"), String.class, Align.LEFT, 150);
		pagedDefinition.addColumnDefinition("dealer." + NAME_EN, I18N.message("dealer"), String.class, Align.LEFT, 150);
		pagedDefinition.addColumnDefinition(START_DATE, I18N.message("start.date"), Date.class, Align.LEFT, 100);	
		pagedDefinition.addColumnDefinition("term", I18N.message("term"), Integer.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("interestRate", I18N.message("rate"), Double.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("quotation.tiFinanceAmount", I18N.message("lease.amount"), Double.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("wkfStatus." + DESC_EN, I18N.message("contract.status"), String.class, Align.LEFT, 120);

		EntityPagedDataProvider<Contract> pagedDataProvider = new EntityPagedDataProvider<Contract>();
		pagedDataProvider.setPagedDefinition(pagedDefinition);
		return pagedDataProvider;
	}	
		
	/**
	 * @see AbstractTablePanel#getEntity()
	 */
	@Override
	protected Contract getEntity() {
		final Long id = getItemSelectedId();
		if (id != null) {
		    return ENTITY_SRV.getById(Contract.class, id);
		}
		return null;
	}
	
	@Override
	protected ContractsSearchPanel createSearchPanel() {
		return new ContractsSearchPanel(this);
	}


	public void reloadContract() {
		createSearchPanel();
	}
	
	private class CustomerNameColumnRender extends EntityColumnRenderer {
		@Override
		public Object getValue() {
			Applicant applicant = ((Contract) getEntity()).getApplicant();
			return applicant != null ? getDefaultString(applicant.getIndividual().getLastNameEn()) + " " + getDefaultString(applicant.getIndividual().getFirstNameEn()) : "";
		}
	}

	private String getDefaultString(String value) {
		return value == null?"":value;
	}

}
