package com.soma.mfinance.gui.ui.panel.report.outstanding.contract.insurance;

import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.EntityColumnRenderer;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.ui.Table;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;

/**
 * @author th.seng
 * date 12/12/2017
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ContractInsuranceTablePanel extends AbstractTablePanel<Quotation> implements QuotationEntityField {

	private static final long serialVersionUID = -6675694004563946811L;

	@PostConstruct
	public void PostConstruct() {
		setCaption(I18N.message("contract.insurance"));
		setSizeFull();
		setMargin(true);
		setSpacing(true);

		super.init(I18N.message("contract.insurance"));
		//addDefaultNavigation();
	}

	@Override
	protected PagedDataProvider<Quotation> createPagedDataProvider() {
		PagedDefinition<Quotation> pagedDefinition = new PagedDefinition<>(searchPanel.getRestrictions());

		pagedDefinition.addColumnDefinition(ID, I18N.message("id").toUpperCase(), Long.class, Table.Align.LEFT, 70);
		pagedDefinition.addColumnDefinition(REFERENCE, I18N.message("contract.reference").toUpperCase(), String.class, Table.Align.LEFT, 140);
		pagedDefinition.addColumnDefinition("customer", I18N.message("customer").toUpperCase(), String.class, Table.Align.LEFT, 100, new CustomerColumnRenderer());
		pagedDefinition.addColumnDefinition("asset.model.descEn", I18N.message("asset").toUpperCase(), String.class, Table.Align.LEFT, 150);
		pagedDefinition.addColumnDefinition("contract.asset.chassisNumber", I18N.message("asset.chassis.number").toUpperCase(), String.class, Table.Align.LEFT, 150);
		pagedDefinition.addColumnDefinition("contract.asset.engineNumber", I18N.message("asset.engine.number").toUpperCase(), String.class, Table.Align.LEFT, 150);
		pagedDefinition.addColumnDefinition("tiAppraisalEstimateAmount", I18N.message("asset.price").toUpperCase(), Double.class, Table.Align.LEFT, 150);
		pagedDefinition.addColumnDefinition("insuranceStartDate", I18N.message("insurance.start.date").toUpperCase(), Date.class, Table.Align.LEFT, 150);
		pagedDefinition.addColumnDefinition("payDate", I18N.message("pay.date").toUpperCase(), Date.class, Table.Align.LEFT, 150, new PayDateColumnRenderer());
		pagedDefinition.addColumnDefinition("yearNumber", I18N.message("year.number").toUpperCase(), Integer.class, Table.Align.LEFT, 150, new YearNumberColumnRenderer());
		pagedDefinition.addColumnDefinition("contract.term", I18N.message("term").toUpperCase(), Integer.class, Table.Align.LEFT, 150);
		pagedDefinition.addColumnDefinition("insuranceFee", I18N.message("insurance.fee").toUpperCase(), Double.class, Table.Align.LEFT, 150, new InsuranceFeeColumnRenderer());
		pagedDefinition.addColumnDefinition("dealer.dealerType.descEn", I18N.message("dealer.type").toUpperCase(), String.class, Table.Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("dealer.nameEn", I18N.message("dealer").toUpperCase(), String.class, Table.Align.LEFT, 200);
		pagedDefinition.addColumnDefinition("insuranceStatus.descEn", I18N.message("insurance.status").toUpperCase(), String.class, Table.Align.LEFT, 150);
		pagedDefinition.addColumnDefinition("insuranceCompany", I18N.message("insurance.company").toUpperCase(), String.class, Table.Align.LEFT, 150, new InsuranceCompanyColumnRenderer());
		pagedDefinition.addColumnDefinition(WKF_STATUS + ".descEn", I18N.message("contract.status").toUpperCase(), String.class, Table.Align.LEFT, 140);
		pagedDefinition.addColumnDefinition(CONTRACT_START_DATE, I18N.message("contract.date").toUpperCase(), Date.class, Table.Align.LEFT, 80);
		pagedDefinition.addColumnDefinition("insuranceEndYear", I18N.message("insurance.end.date").toUpperCase(), Date.class, Table.Align.LEFT, 150, new InsuranceEndYearColumnRenderer());
		//pagedDefinition.addColumnDefinition("insuranceDuration", I18N.message("insurance.year").toUpperCase(), String.class, Table.Align.LEFT, 150);

		EntityPagedDataProvider<Quotation> pagedDataProvider = new EntityPagedDataProvider<>();
		pagedDataProvider.setPagedDefinition(pagedDefinition);
		return pagedDataProvider;
	}

	@Override
	protected Quotation getEntity() {
		/*final Long id = getItemSelectedId();
		if (id != null) {
		    return ENTITY_SRV.getById(Quotation.class, id);
		}*/
		return null;
	}
	
	@Override
	protected ContractInsuranceSearchPanel createSearchPanel() {
		return new ContractInsuranceSearchPanel(this);
	}

	private class InsuranceEndYearColumnRenderer extends EntityColumnRenderer {
		@Override
		public Object getValue() {
			Quotation quotation = (Quotation) getEntity();
			return quotation.getInsuranceStartDate() != null ? DateUtils.addMonthsDate(quotation.getInsuranceStartDate(), quotation.getContract().getTerm()) : null;
		}
	}

	private class InsuranceCompanyColumnRenderer extends EntityColumnRenderer {
		@Override
		public Object getValue() {
			Quotation quotation = (Quotation) getEntity();
			return quotation.getContract().getInsuranceCompany() != null ? quotation.getContract().getInsuranceCompany().getDescEn() : "CPMI";
		}
	}

	private class InsuranceFeeColumnRenderer extends EntityColumnRenderer {
		@Override
		public Object getValue() {
			return FinServicesHelper.FIN_PROD_SRV.getInsuranceFee(MyNumberUtils.getDouble(((Quotation) getEntity()).getTiFinanceAmount()));
		}
	}

	private class PayDateColumnRenderer extends EntityColumnRenderer {
		@Override
		public Object getValue() {
			ContractInsuranceSearchPanel contractInsuranceSearchPanel = ((ContractInsuranceSearchPanel) searchPanel);
			//return DateUtils.addYearsDate(contractInsuranceSearchPanel.getDfStartDate().getValue(), Integer.parseInt(contractInsuranceSearchPanel.getCbxInsuranceYear().getValue().toString()) - 1);
			return contractInsuranceSearchPanel.getDfStartDate().getValue();
		}
	}

	private class YearNumberColumnRenderer extends EntityColumnRenderer {
		@Override
		public Object getValue() {
			ContractInsuranceSearchPanel contractInsuranceSearchPanel = ((ContractInsuranceSearchPanel) searchPanel);
			Date startDate = contractInsuranceSearchPanel.getDfStartDate().getValue();
			int yearNumber = getYearNo(startDate, ((Quotation) getEntity()).getInsuranceStartDate());
			if (contractInsuranceSearchPanel.getCbxInsuranceYear().getSelectedEntity() != null) {
				yearNumber = Integer.parseInt(contractInsuranceSearchPanel.getCbxInsuranceYear().getValue().toString());
			}
			return yearNumber;
		}
	}

	private class CustomerColumnRenderer extends EntityColumnRenderer {
		@Override
		public Object getValue() {
			Quotation quotation = (Quotation) getEntity();
			return quotation.getQuotationApplicants().get(0).getApplicant().getLastNameEn()
					+ " "
					+ quotation.getQuotationApplicants().get(0).getApplicant().getFirstNameEn();
		}
	}

	private int getYearNo(Date date, Date insuranceDate) {
		int yearNumber = DateUtils.getNumberYearOfTwoDates(DateUtils.getDateAtEndOfDay(date), DateUtils.getDateAtBeginningOfDay(insuranceDate)) + 1;
		return yearNumber;
	}
}
