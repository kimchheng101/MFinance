package com.soma.mfinance.gui.ui.panel.report.contract.overdue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.soma.mfinance.core.accounting.InstallmentVO;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowType;
import com.soma.mfinance.core.entityfield.InstallmentEntityField;
import com.soma.mfinance.core.financial.model.EServiceType;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.shared.contract.PenaltyVO;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyMathUtils;
import org.seuksa.frmk.tools.amount.Amount;
import org.seuksa.frmk.tools.spring.SpringUtils;

import com.soma.mfinance.core.financial.service.FinanceCalculationService;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table.Align;
import com.vaadin.ui.VerticalLayout;

/**
 * Payment schedule panel
 *
 * @author kimsuor.seang
 * @author s.torn
 */
public class PaymentSchedulePanel extends AbstractTabPanel implements FMEntityField, FinServicesHelper ,InstallmentEntityField {

    /**     */
    private static final long serialVersionUID = -3266945821182447130L;

    private FinanceCalculationService financeCalculationService = SpringUtils.getBean(FinanceCalculationService.class);

    private SimplePagedTable<Quotation> pagedTable;
    private Quotation quotation;
    private VerticalLayout verticalLayout;
    private HorizontalLayout tableControl;

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTabPanel#createForm()
     */
    @Override
    protected Component createForm() {
        verticalLayout = new VerticalLayout();
        verticalLayout.setSpacing(true);

        return verticalLayout;
    }

    /** */
    @SuppressWarnings("unchecked")
    private void setIndexedContainer() {
        Indexed indexedContainer = pagedTable.getContainerDataSource();
        indexedContainer.removeAllItems();
        int term = quotation.getTerm();
        for (int i = 1; i <= term; i++) {
            List<InstallmentVO> allInstallmentList = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(quotation.getContract(), i);
            double tiPrincipal = 0d;
            double tiInterestRate = 0d;
            double tiInsurance = 0d;
            double tiServiceFee = 0d;
            double otherAmount = 0d;
            double totalInstallment = 0d;
            double installmentAmount = 0d;
            double remainingBalance = 0d;
            double tiTransAmount = 0d;
            InstallmentVO installmentVOFirstIndex = allInstallmentList.get(0);
            for (InstallmentVO installmentVO : allInstallmentList) {
                if (installmentVO.getCashflowType() == ECashflowType.CAP) {
                    tiPrincipal = installmentVO.getTiamount() + installmentVO.getVatAmount();
                } else if (installmentVO.getCashflowType() == ECashflowType.IAP) {
                    tiInterestRate = installmentVO.getTiamount() + installmentVO.getVatAmount();
                } else if (installmentVO.getService() != null && EServiceType.INSFEE.getCode().equals(installmentVO.getService().getCode())) {
                    tiInsurance = installmentVO.getTiamount() + installmentVO.getVatAmount();
                } else if (installmentVO.getService() != null &&
                        (EServiceType.SRVFEE.getCode().equals(installmentVO.getService().getCode())
                            || EServiceType.SRVFEEEXT.getCode().equals(installmentVO.getService().getCode()))) {
                    tiServiceFee = installmentVO.getTiamount() + installmentVO.getVatAmount();
                } else if (installmentVO.getService() != null && EServiceType.TRANSFEE.getCode().equals(installmentVO.getService().getCode())) {
                    tiTransAmount = installmentVO.getTiamount() + installmentVO.getVatAmount();
                } else {
                    otherAmount += installmentVO.getTiamount() + installmentVO.getVatAmount();
                }
            }
            PenaltyVO penaltyVO = CONT_SRV.calculatePenalty(quotation.getContract(), installmentVOFirstIndex.getInstallmentDate(), DateUtils.todayH00M00S00(), quotation.getContract().getTiInstallmentAmount());
            double penaltyAmount = 0d;
            if (penaltyVO.getPenaltyAmount() != null) {
                penaltyAmount += penaltyVO.getPenaltyAmount().getTiAmount() + penaltyVO.getPenaltyAmount().getVatAmount();
            }
            totalInstallment = tiPrincipal + tiInterestRate + tiInsurance + tiServiceFee + otherAmount;
            installmentAmount = tiPrincipal + tiInterestRate;
            remainingBalance = installmentVOFirstIndex.getBalance();

            Item item = indexedContainer.addItem(i);
            item.getItemProperty(NUM_INSTALLMENT).setValue(installmentVOFirstIndex.getNumInstallment());
            item.getItemProperty(DUE_DATE).setValue(installmentVOFirstIndex.getInstallmentDate());
            item.getItemProperty(INSTALLMENT_AMOUNT).setValue(new Amount(installmentAmount, 0d, installmentAmount));
            item.getItemProperty(PRINCIPAL_AMOUNT).setValue(new Amount(tiPrincipal, 0d, tiPrincipal));
            item.getItemProperty(INTEREST_AMOUNT).setValue(new Amount(tiInterestRate, 0d, tiInterestRate));
            item.getItemProperty(INSURANCE_FEE).setValue(new Amount(tiInsurance, 0d, tiInsurance));
            item.getItemProperty(SERVICING_FEE).setValue(new Amount(tiServiceFee, 0d, tiServiceFee));
            item.getItemProperty(TRANSFER_AMOUNT).setValue(tiTransAmount);
            //item.getItemProperty(OTHER_AMOUNT).setValue(new Amount(otherAmount,0d,otherAmount));
            //item.getItemProperty(PENALTY_AMOUNT).setValue(new Amount(penaltyAmount, 0d, penaltyAmount));
            item.getItemProperty("total.installment.amount").setValue(new Amount(totalInstallment,0d,totalInstallment));
            item.getItemProperty("remaining.balance").setValue(new Amount(remainingBalance,0d,remainingBalance));
        }
        pagedTable.refreshContainerDataSource();
    }

    /**
     * @param term
     * @param serviceCode
     * @param services
     * @return
     */
    private Double getServiceFee(int term, String serviceCode,
                                 List<com.soma.mfinance.core.quotation.model.QuotationService> services) {
        for (com.soma.mfinance.core.quotation.model.QuotationService service : services) {
            if (service.getService().getCode().equals(serviceCode)) {
                if (service.getService().getFrequency() != null) {
                    int nbMonths = service.getService().getFrequency().getNbMonths();
                    return MyMathUtils.roundAmountTo(((term / nbMonths) * service.getTiPrice()) / term);
                } else {
                    return MyMathUtils.roundAmountTo(service.getTiPrice() / term);
                }
            }
        }
        return 0d;
    }

    /**
     * @param serviceCode
     * @param services
     * @return
     */
    private Double getServiceFee(String serviceCode, List<com.soma.mfinance.core.quotation.model.QuotationService> services) {
        for (com.soma.mfinance.core.quotation.model.QuotationService service : services) {
            if (service.getService().getCode().equals(serviceCode)) {
                return service.getTiPrice();
            }
        }
        return 0d;
    }

    /**
     * @return
     */
    private List<ColumnDefinition> createColumnDefinitions() {
        List<ColumnDefinition> columnDefinitions = new ArrayList<ColumnDefinition>();

        columnDefinitions.add(new ColumnDefinition(NUM_INSTALLMENT, I18N.message("num.installment"), Integer.class, Align.RIGHT, 120));
        columnDefinitions.add(new ColumnDefinition(DUE_DATE, I18N.message("due.date"), Date.class, Align.RIGHT, 80));
        columnDefinitions.add(new ColumnDefinition(INSTALLMENT_AMOUNT, I18N.message("installment.amount"), Amount.class, Align.RIGHT, 140));
        columnDefinitions.add(new ColumnDefinition(SERVICING_FEE, I18N.message("service.amount"), Amount.class, Align.RIGHT, 110));
        columnDefinitions.add(new ColumnDefinition(INSURANCE_FEE, I18N.message("insurance.amount"), Amount.class, Align.RIGHT, 130));
        columnDefinitions.add(new ColumnDefinition(PRINCIPAL_AMOUNT,I18N.message("principal.amount"), Amount.class,Align.RIGHT,125));
        columnDefinitions.add(new ColumnDefinition(INTEREST_AMOUNT,I18N.message("interest.amount"), Amount.class,Align.RIGHT,120));
        columnDefinitions.add(new ColumnDefinition(TRANSFER_AMOUNT,I18N.message("transfer.amount"), Double.class,Align.RIGHT,120));
        //columnDefinitions.add(new ColumnDefinition(OTHER_AMOUNT,I18N.message("other.amount"), Amount.class,Align.RIGHT,120));
        //columnDefinitions.add(new ColumnDefinition(PENALTY_AMOUNT,I18N.message("penalty.amount"),Amount.class,Align.RIGHT,120));
        columnDefinitions.add(new ColumnDefinition("total.installment.amount",I18N.message("total.installment.amount"),Amount.class,Align.RIGHT,120));
        columnDefinitions.add(new ColumnDefinition("remaining.balance",I18N.message("remaining.balance"), Amount.class,Align.RIGHT,170));
        return columnDefinitions;
    }

    /**
     * @param quotation
     */
    public void assignValues(Quotation quotation) {
        this.quotation = quotation;

        if (pagedTable != null) {
            verticalLayout.removeComponent(pagedTable);
            verticalLayout.removeComponent(tableControl);
        }

        pagedTable = new SimplePagedTable<Quotation>(createColumnDefinitions());
        verticalLayout.addComponent(pagedTable);
        tableControl = pagedTable.createControls();
        verticalLayout.addComponent(tableControl);
        setIndexedContainer();
    }
}
