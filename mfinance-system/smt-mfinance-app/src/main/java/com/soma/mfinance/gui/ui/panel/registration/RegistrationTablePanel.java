package com.soma.mfinance.gui.ui.panel.registration;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.common.app.workflow.service.WkfHistoryItemRestriction;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationWkfHistoryItem;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.EntityColumnRenderer;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.data.Item;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.Align;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

/**
 * @author Riya.Pov
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RegistrationTablePanel extends AbstractTablePanel<Quotation> implements QuotationEntityField {

    private static final long serialVersionUID = 6909820118834032008L;

    private static final String DEALER_TYPE = "dealerType";
    private RegistrationSearchPanel registrationSearchPanel;
    //private List <FinancialProductAssetCheck> financialProductAssetChecks;

    @PostConstruct
    public void PostConstruct() {
        setCaption(I18N.message("registration"));
        setSizeFull();
        setHeight("200%");
        setMargin(true);
        setSpacing(true);

        super.init(I18N.message("registration"));

        NavigationPanel navigationPanel = addNavigationPanel();
        if (ProfileUtil.isDocumentController()) {
            navigationPanel.addAddClickListener(this);
        }
        navigationPanel.addEditClickListener(this);
        navigationPanel.addRefreshClickListener(this);

        getPagedTable().addStyleName("colortable");
        getPagedTable().setCellStyleGenerator(new Table.CellStyleGenerator() {
            private static final long serialVersionUID = 6242667432758981026L;

            @Override
            public String getStyle(Table source, Object itemId, Object propertyId) {
                if (propertyId == null) {
                    Item item = source.getItem(itemId);
                    Date registrationDeadline = (Date) item.getItemProperty("registrationDeadline").getValue();
                    if (registrationDeadline != null) {
                        Long diff = DateUtils.getDiffInDays(registrationDeadline, DateUtils.today());
                        if (diff <= 7) {
                            return "highligh-red";
                        }
                    }
                }
                return null;
            }
        });

    }

    /**
     * Get Paged definition
     *
     * @return
     */
    @Override
    protected PagedDataProvider<Quotation> createPagedDataProvider() {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        if (ProfileUtil.isDocumentController()) {
            // If the user is Document controller the fist loading is not search
            restrictions.addCriterion(Restrictions.eq(ID, 0L));
        } else {
            restrictions = searchPanel.getRestrictions();
        }
        PagedDefinition<Quotation> pagedDefinition = new PagedDefinition<>(restrictions);

        pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 60);
        pagedDefinition.addColumnDefinition(REFERENCE, I18N.message("reference"), String.class, Align.LEFT, 125);
        pagedDefinition.addColumnDefinition("financialProduct.descEn", I18N.message("financial.product").toUpperCase(), String.class, Align.LEFT, 150);
        pagedDefinition.addColumnDefinition(CUSTOMER, I18N.message("customer"), String.class, Align.LEFT, 150, new CustomerFullNameColumnRenderer());
        pagedDefinition.addColumnDefinition(ASSET + ".model.descEn", I18N.message("asset"), String.class, Align.LEFT, 150);

        pagedDefinition.addColumnDefinition("insuranceStartDate", I18N.message("insurance.start.date"), Date.class, Align.LEFT, 150);
        pagedDefinition.addColumnDefinition(DEALER + "." + DEALER_TYPE + "." + DESC, I18N.message("dealer.type"), String.class, Align.LEFT, 60);
        pagedDefinition.addColumnDefinition(DEALER + "." + NAME_EN, I18N.message("dealer"), String.class, Align.LEFT, 180);
        pagedDefinition.addColumnDefinition("applicantPhone", I18N.message("applicant.phone"), String.class, Align.LEFT, 160, new ApplicantPhoneColumnRenderer());
        pagedDefinition.addColumnDefinition("registrationStatus." + DESC_EN, I18N.message("registration.status"), String.class, Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("registrationStorageLocation." + DESC_EN, I18N.message("registration.location"), String.class, Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("registrationDeadline", I18N.message("registration.deadline"), Date.class, Align.LEFT, 150);
        pagedDefinition.addColumnDefinition(WKF_STATUS, I18N.message("contract.status"), String.class, Align.LEFT, 140, new StatusColumnRenderer());
        pagedDefinition.addColumnDefinition("submissionDate", I18N.message("change.status.date"), Date.class, Align.LEFT, 140, new changeStatusDateColRenderer());

        if ((ProfileUtil.isDocumentController())) {
            pagedDefinition.addColumnDefinition(ASSET + "." + "chassisNumber", I18N.message("chassis.number"), String.class, Align.LEFT, 200);
            pagedDefinition.addColumnDefinition(ASSET + "." + "engineNumber", I18N.message("engine.number"), String.class, Align.LEFT, 200);
            pagedDefinition.addColumnDefinition(ASSET + "." + "plateNumber", I18N.message("plate.number"), String.class, Align.LEFT, 200);
        }
        pagedDefinition.addColumnDefinition(QUOTATION_DATE, I18N.message("quotation.date"), Date.class, Align.LEFT, 80);
        pagedDefinition.addColumnDefinition(FIRST_SUBMISSION_DATE, I18N.message("first.submission.date"), Date.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition(UNDERWRITER + "." + DESC, I18N.message("underwriter"), String.class, Align.LEFT, 120);
        pagedDefinition.addColumnDefinition(UNDERWRITER_SUPERVISOR + "." + DESC, I18N.message("underwriter.supervisor"), String.class, Align.LEFT, 120);
        pagedDefinition.addColumnDefinition(ACCEPTATION_DATE, I18N.message("acceptation.date"), Date.class, Align.LEFT, 90);
        pagedDefinition.addColumnDefinition(CONTRACT_START_DATE, I18N.message("contract.date"), Date.class, Align.LEFT, 90);
        EntityPagedDataProvider<Quotation> pagedDataProvider = new EntityPagedDataProvider<Quotation>();

        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    /**
     * @see AbstractTablePanel#getEntity()
     */
    @Override
    protected Quotation getEntity() {
        final Long id = getItemSelectedId();
        if (id != null) {
            return ENTITY_SRV.getById(Quotation.class, id);
        }
        return null;
    }

    @Override
    protected RegistrationSearchPanel createSearchPanel() {
        registrationSearchPanel = new RegistrationSearchPanel(this);
        return registrationSearchPanel;
    }

    private class registerStatusColRenderer extends EntityColumnRenderer {

        @Override
        public Object getValue() {

            return null;
        }
    }

    private class changeStatusDateColRenderer extends EntityColumnRenderer {

        @Override
        public Object getValue() {
            Date changeStatusDate = null;
            Quotation quotation = (Quotation) getEntity();

            WkfHistoryItemRestriction restrictions = new WkfHistoryItemRestriction(QuotationWkfHistoryItem.class);
            restrictions.setEntityId(quotation.getId());
            restrictions.addOrder(Order.desc("changeDate"));
            restrictions.setPropertyName("wkfStatus");
            List<QuotationWkfHistoryItem> wkfBaseHistoryItems = WKF_SRV.getHistories(restrictions);
            for (QuotationWkfHistoryItem historyItem : wkfBaseHistoryItems) {
                if (quotation.getWkfStatus().getCode().equals(historyItem.getNewValue())) {
                    changeStatusDate = historyItem.getChangeDate();
                    break;
                }
            }
            return changeStatusDate;
        }
    }

    private class StatusColumnRenderer extends EntityColumnRenderer {

        @Override
        public Object getValue() {
            EWkfStatus quotationWkfStatus = ((Quotation) getEntity()).getWkfStatus();
            Contract contractStatus = ((Quotation) getEntity()).getContract();

            if (contractStatus == null) {
                return quotationWkfStatus.getDescEn();
            } else if (contractStatus.getWkfStatus().equals(ContractWkfStatus.REP)
                    || contractStatus.getWkfStatus().equals(ContractWkfStatus.THE)
                    || contractStatus.getWkfStatus().equals(ContractWkfStatus.ACC)
                    || contractStatus.getWkfStatus().equals(ContractWkfStatus.WRI)
                    || contractStatus.getWkfStatus().equals(ContractWkfStatus.EAR)
                    || contractStatus.getWkfStatus().equals(ContractWkfStatus.CLO)
                    || contractStatus.getWkfStatus().equals(ContractWkfStatus.WTD)
                    ) {
                return contractStatus.getWkfStatus().getDescEn();
            }

            return quotationWkfStatus.getDescEn();
        }
    }

    private class CustomerFullNameColumnRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            Applicant customer = ((Quotation) getEntity()).getMainApplicant();
            return customer.getLastNameEn() + " " + customer.getFirstNameEn();
        }
    }

    private class ApplicantPhoneColumnRenderer extends EntityColumnRenderer {

        @Override
        public Object getValue() {
            Applicant customer = ((Quotation) getEntity()).getMainApplicant();
            return customer.getIndividual() != null ? customer.getIndividual().getMobilePerso() : "";
        }

    }
}