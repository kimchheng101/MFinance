package com.soma.mfinance.gui.report.xls;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.soma.common.app.workflow.model.EWkfStatus;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyMathUtils;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.conf.AppConfig;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.mfinance.core.shared.report.ReportParameter;
import com.soma.mfinance.core.shared.util.DateFilterUtil;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.mfinance.glf.report.model.DailyReport;
import com.soma.mfinance.glf.report.service.DailyReportService;
import com.soma.mfinance.glf.statistic.model.StatisticConfig;
import com.soma.mfinance.tools.report.Report;
import com.soma.mfinance.tools.report.XLSAbstractReportExtractor;

import static com.soma.frmk.helper.SeuksaServicesHelper.ENTITY_SRV;

/**
 * @author sok.vina
 * @author kimsuor.seang (modified)
 */
public class GLFDailyReport extends XLSAbstractReportExtractor implements Report, GLFApplicantFields, QuotationEntityField {

    protected DailyReportService dailyReportService = SpringUtils.getBean(DailyReportService.class);
    protected Logger logger = LoggerFactory.getLogger(getClass());

    private Map<String, CellStyle> styles = null;

    private static String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
    private static String LEFT_BORDER_ALIGN_LEFT = "LEFT_BORDER_ALIGN_LEFT";
    private static String RIGHT_BORDER = "RIGHT_BORDER";
    private static String TOP_LEFT_BOTTOM_BORDER_ALIGN_LEFT = "TOP_LEFT_BOTTOM_BORDER_ALIGN_LEFT";
    private static String TOP_RIGHT_BOTTOM_BORDER_ALIGN_RIGHT = "TOP_RIGHT_BOTTOM_BORDER_ALIGN_RIGHT";
    private static String TOP_BOTTOM_BORDER_ALIGN_RIGHT_PERCENTAGE = "TOP_BOTTOM_BORDER_ALIGN_RIGHT_PERCENTAGE";
    private static String TOP_BOTTOM_BORDER_ALIGN_RIGHT = "TOP_BOTTOM_BORDER_ALIGN_RIGHT";
    private static String NONE_BORDER_ALIGN_RIGHT_PERCENTAGE = "NONE_BORDER_ALIGN_RIGHT_PERCENTAGE";
    private static String NONE_BORDER_ALIGN_RIGHT = "NONE_BORDER_ALIGN_RIGHT";

    /**
     * Background color format
     */
    static short BG_LIGHT_BLUE = IndexedColors.LIGHT_BLUE.getIndex();
    static short BG_WHITE = IndexedColors.WHITE.getIndex();
    static short BG_GREEN = IndexedColors.GREEN.getIndex();

    /**
     * Font color
     */
    static short FC_WHITE = IndexedColors.WHITE.getIndex();
    static short FC_BLACK = IndexedColors.BLACK.getIndex();
    static short FC_BLUE = 48;
    static short FC_GREEN = IndexedColors.GREEN.getIndex();
    static short FC_ORANGE = IndexedColors.ORANGE.getIndex();
    static short FC_RED = IndexedColors.RED.getIndex();
    static short FC_DARK_BLUE = IndexedColors.DARK_BLUE.getIndex();

    private Long numberDay;
    private Dealer paramDealer;
    private EDealerType paramDealerType;
    private Date selectDate;

    //CONSTRUCTOR
    public GLFDailyReport() {
    }

    /**
     * @see Report#generate(com.soma.mfinance.core.shared.report.ReportParameter)
     */
    @Override
    public String generate(ReportParameter reportParameter) throws Exception {
        Map<String, Object> parameters = reportParameter.getParameters();
        selectDate = (Date) parameters.get("dateValue");
        paramDealerType = (EDealerType) parameters.get("dealerType");
        paramDealer = (Dealer) parameters.get("dealer");
        numberDay = DateUtils.getDiffInDaysPlusOneDay(selectDate, DateUtils.getDateAtBeginningOfMonth(selectDate)) - 1;

        createWorkbook(null);
        XSSFSheet sheet = wb.createSheet();
        sheet.lockDeleteColumns();
        sheet.lockDeleteRows();
        sheet.lockFormatCells();
        sheet.lockFormatColumns();
        sheet.lockFormatRows();
        sheet.lockInsertColumns();
        sheet.lockInsertRows();
        CellStyle style = wb.createCellStyle();
        styles = new HashMap<String, CellStyle>();
        createStyles();
        sheet.setColumnWidth(0, 6000);
        sheet.setColumnWidth(1, 5500);
        sheet.setColumnWidth(2, 5500);
        sheet.setColumnWidth(3, 1700);
        sheet.setColumnWidth(4, 4300);
        sheet.setColumnWidth(5, 5000);
        sheet.setColumnWidth(6, 3900);
        sheet.setColumnWidth(7, 3900);
        sheet.setColumnWidth(8, 5000);
        sheet.setColumnWidth(9, 5000);
        sheet.setColumnWidth(10, 5000);
        sheet.setColumnWidth(11, 3850);
        sheet.setColumnWidth(12, 4000);
        sheet.setColumnWidth(13, 6500);
        sheet.setColumnWidth(14, 5600);
        sheet.setColumnWidth(15, 3700);
        sheet.setColumnWidth(16, 3300);
        sheet.setColumnWidth(17, 3300);
        sheet.setColumnWidth(18, 2000);
        sheet.setColumnWidth(19, 2500);
        sheet.setColumnWidth(20, 3500);
        sheet.setZoom(9, 10);
        final PrintSetup printSetup = sheet.getPrintSetup();

        printSetup.setPaperSize(HSSFPrintSetup.A4_PAPERSIZE);
        printSetup.setLandscape(true);

        printSetup.setScale((short) 60);
        sheet.setAutobreaks(true);
        sheet.setFitToPage(true);
        printSetup.setFitWidth((short) 1);
        printSetup.setFitHeight((short) 0);

        // Setup the Page margins - Left, Right, Top and Bottom
        sheet.setMargin(Sheet.LeftMargin, 0);
        sheet.setMargin(Sheet.RightMargin, 0);
        sheet.setMargin(Sheet.TopMargin, 0.25);
        sheet.setMargin(Sheet.BottomMargin, 0.25);
        sheet.setMargin(Sheet.HeaderMargin, 0.25);
        sheet.setMargin(Sheet.FooterMargin, 0.25);
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BIG_SPOTS);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        int iRow = 1;
        iRow = iRow + 1;

        if (selectDate == null) {
            selectDate = DateUtils.todayH00M00S00();
        } else {
            selectDate = DateUtils.getDateAtBeginningOfDay(selectDate);
        }

        BaseRestrictions<Dealer> restrictions = new BaseRestrictions<>(Dealer.class);
        restrictions.addAssociation("dealerAddresses", "deaAddress", JoinType.INNER_JOIN);
        restrictions.addAssociation("deaAddress.address", "address", JoinType.INNER_JOIN);
        restrictions.addAssociation("address.province", "province", JoinType.INNER_JOIN);

        if (paramDealer != null) {
            restrictions.addCriterion(Restrictions.eq("id", paramDealer.getId()));
        }
        if (paramDealerType != null) {
            restrictions.addCriterion(Restrictions.eq("dealerType", paramDealerType));
        }
        restrictions.addOrder(Order.asc("province.descEn"));
        List<Dealer> dealers = dailyReportService.list(restrictions);
        List<DealerReport> dealerReports = new ArrayList<>();

        if (dealers != null && !dealers.isEmpty()) {
            for (Dealer dealer : dealers) {

                dailyReportService.calculateDailyReportByDealer(dealer, selectDate);
                boolean includeInDailyReport = true;
                if (includeInDailyReport) {
                    DealerReport dealerReport = new DealerReport();
                    dealerReport.setDealer(dealer);
                    BaseRestrictions<StatisticConfig> statRestrictions = new BaseRestrictions<>(StatisticConfig.class);
                    statRestrictions.addCriterion(Restrictions.eq("dealer.id", dealer.getId()));
                    statRestrictions.addCriterion(Restrictions.eq("startDate", DateUtils.getDateAtBeginningOfMonth(selectDate)));
                    List<StatisticConfig> statisticConfigs = dailyReportService.list(statRestrictions);

                    int lowTarget = 0;
                    if (statisticConfigs != null && !statisticConfigs.isEmpty()) {
                        lowTarget = statisticConfigs.get(0).getTargetLow();
                    }
                    dealerReport.setLowTarget(lowTarget);

                    BaseRestrictions<DailyReport> dailyRestrictions = new BaseRestrictions<>(DailyReport.class);
                    dailyRestrictions.addCriterion(Restrictions.eq("dealer.id", dealer.getId()));
                    dailyRestrictions.addCriterion(Restrictions.ge("date", DateUtils.getDateAtBeginningOfMonth(selectDate)));
                    dailyRestrictions.addCriterion(Restrictions.le("date", DateUtils.getDateAtEndOfMonth(selectDate)));

                    restrictions.addOrder(Order.asc("date"));
                    List<DailyReport> dailyReports = dailyReportService.list(dailyRestrictions);

                    dealerReport.setDailyReports(dailyReports);
                    dealerReports.add(dealerReport);
                }
            }
        }

        // Display overview table
        iRow = overviewTable(sheet, iRow, dealerReports, selectDate);

        // add space three rows
        iRow = iRow + 3;

        if (dealerReports != null && !dealerReports.isEmpty()) {

            // Display Total table
            sheet.setRowBreak(iRow);
            iRow = dataTable(sheet, iRow, getTotalDealerReport(dealerReports, selectDate), selectDate);
            // add space one row
            iRow = iRow + 1;
            // Display one by one Dealer table
            for (DealerReport dealerReport : dealerReports) {
                sheet.setRowBreak(iRow);
                iRow = dataTable(sheet, iRow, dealerReport, selectDate);
                iRow = iRow + 1;  // add space one row
            }
        }

        String fileName = writeXLSData("Daily_Report" + DateUtils.getDateLabel(DateUtils.today(), "yyyyMMddHHmmssSSS") + ".xlsx");
        return fileName;
    }

    /**
     * @param dealerReports
     * @return
     */
    private DealerReport getTotalDealerReport(List<DealerReport> dealerReports, Date selectDate) {
        DealerReport totalDealerReport = new DealerReport();
        List<DailyReport> dailyReports = new ArrayList<>();

        for (DealerReport dealerReport : dealerReports) {
            totalDealerReport.setLowTarget(MyNumberUtils.getInteger(totalDealerReport.getLowTarget())
                    + MyNumberUtils.getInteger(dealerReport.getLowTarget()));
            totalDealerReport.setHighTarget(MyNumberUtils.getInteger(totalDealerReport.getHighTarget())
                    + MyNumberUtils.getInteger(dealerReport.getHighTarget()));

            for (DailyReport dest : dealerReport.getDailyReports()) {
                DailyReport dailyReport = getDailyReport(dailyReports, dest);
                if (dailyReport == null) {
                    dailyReport = new DailyReport();
                    dailyReport.setDate(dest.getDate());
                    dailyReports.add(dailyReport);
                }
                dailyReport.plus(dest);

                if (DateUtils.isSameDay(selectDate, dest.getDate())) {
                    dailyReport.setDealerAccumulateNewContracts(MyNumberUtils.getLong(dailyReport.getDealerAccumulateNewContracts())
                            + MyNumberUtils.getLong(dest.getDealerAccumulateNewContracts()));

                    dailyReport.setNbDealerContractsUntilLastMonth(MyNumberUtils.getLong(dailyReport.getNbDealerContractsUntilLastMonth())
                            + MyNumberUtils.getLong(dest.getNbDealerContractsUntilLastMonth()));

                    dailyReport.setNbDealerApplicationsFromBeginOfMonth(MyNumberUtils.getLong(dailyReport.getNbDealerApplicationsFromBeginOfMonth())
                            + MyNumberUtils.getLong(dest.getNbDealerApplicationsFromBeginOfMonth()));

                    dailyReport.setNbDealerApplicationsLastMonthFromBeginToDate(MyNumberUtils.getLong(dailyReport
                            .getNbDealerApplicationsLastMonthFromBeginToDate())
                            + MyNumberUtils.getLong(dest.getNbDealerApplicationsLastMonthFromBeginToDate()));

                    dailyReport.setNbDealerContractsFromBeginOfMonth(MyNumberUtils.getLong(dailyReport.getNbDealerContractsFromBeginOfMonth())
                            + MyNumberUtils.getLong(dest.getNbDealerContractsFromBeginOfMonth()));

                    dailyReport.setNbDealerContractsLastMonthFromBeginToDate(MyNumberUtils.getLong(dailyReport.getNbDealerContractsLastMonthFromBeginToDate())
                            + MyNumberUtils.getLong(dest.getNbDealerContractsLastMonthFromBeginToDate()));

                    dailyReport.setNbDealerAppraisalFromBeginOfMonth(MyNumberUtils.getLong(dailyReport.getNbDealerAppraisalFromBeginOfMonth())
                            + MyNumberUtils.getLong(dest.getNbDealerAppraisalFromBeginOfMonth()));

                    dailyReport.setNumberAppraisalDealerLastMonthFromBeginTodate(MyNumberUtils.getLong(dailyReport.getNumberAppraisalDealerLastMonthFromBeginTodate())
                            + MyNumberUtils.getLong(dest.getNumberAppraisalDealerLastMonthFromBeginTodate()));
                }
            }
        }
        Collections.sort(dailyReports, new DailyReportComparator());
        totalDealerReport.setDailyReports(dailyReports);
        return totalDealerReport;
    }

    /**
     * @param dailyReports
     * @param dest
     * @return
     */
    private DailyReport getDailyReport(List<DailyReport> dailyReports, DailyReport dest) {
        for (DailyReport dailyReport : dailyReports) {
            if (DateUtils.isSameDay(dailyReport.getDate(), dest.getDate())) {
                return dailyReport;
            }
        }
        return null;
    }

    /**
     * @param reasonCode
     * @param selectDate
     * @param dealer
     * @param dealerType
     * @return
     */
    private int getNbRejectByReasonCode(String reasonCode, Date selectDate, Dealer dealer, EDealerType dealerType) {
        BaseRestrictions<Quotation> criteria = new BaseRestrictions<>(Quotation.class);
        criteria.addAssociation("quotationSupportDecisions", "qsd", JoinType.INNER_JOIN);
        criteria.addAssociation("qsd.supportDecision", "sup", JoinType.INNER_JOIN);
        criteria.addAssociation("dealer", "quodeal", JoinType.INNER_JOIN);
        criteria.addCriterion(Restrictions.eq(WKF_STATUS, QuotationWkfStatus.REJ));
        criteria.addCriterion(Restrictions.ge("rejectDate", DateFilterUtil.getStartDate(DateUtils.getDateAtBeginningOfMonth(selectDate))));
        criteria.addCriterion(Restrictions.le("rejectDate", DateFilterUtil.getEndDate(selectDate)));
        criteria.addCriterion(Restrictions.eq("sup.code", reasonCode));
        if (dealer != null) {
            criteria.addCriterion(Restrictions.eq("quodeal.id", dealer.getId()));
        }
        if (dealerType != null) {
            criteria.addCriterion(Restrictions.eq("quodeal.dealerType", dealerType));
        }
        return (int) dailyReportService.count(criteria);
    }

    /**
     * @param reasonCode
     * @param selectDate
     * @return
     */
    private int getNbDeclineByReasonCode(String reasonCode, Date selectDate, Dealer dealer, EDealerType dealerType) {
        BaseRestrictions<Quotation> criteria = new BaseRestrictions<>(Quotation.class);
        criteria.addAssociation("quotationSupportDecisions", "qsup", JoinType.INNER_JOIN);
        criteria.addAssociation("qsup.supportDecision", "sup", JoinType.INNER_JOIN);
        criteria.addAssociation("dealer", "quodeal", JoinType.INNER_JOIN);
        criteria.addCriterion(Restrictions.eq(WKF_STATUS, QuotationWkfStatus.DEC));
        criteria.addCriterion(Restrictions.ge("declineDate", DateFilterUtil.getStartDate(DateUtils.getDateAtBeginningOfMonth(selectDate))));
        criteria.addCriterion(Restrictions.le("declineDate", DateFilterUtil.getEndDate(selectDate)));
        criteria.addCriterion(Restrictions.eq("sup.code", reasonCode));
        if (dealer != null) {
            criteria.addCriterion(Restrictions.eq("quodeal.id", dealer.getId()));
        }
        if (dealerType != null) {
            criteria.addCriterion(Restrictions.eq("quodeal.dealerType", dealerType));
        }
        return (int) dailyReportService.count(criteria);
    }

    /**
     * @param date
     * @return
     */
    private long getNbRejectsFromBeginOfMonth(Date date) {
        Date dateBeginningOfMonth = DateUtils.getDateAtBeginningOfMonth(date);
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addAssociation(DEALER, "quodeal", JoinType.INNER_JOIN);
        restrictions.addCriterion(Restrictions.eq(WKF_STATUS, QuotationWkfStatus.REJ));
        restrictions.addCriterion(Restrictions.ge("rejectDate", DateFilterUtil.getStartDate(dateBeginningOfMonth)));
        restrictions.addCriterion(Restrictions.le("rejectDate", DateFilterUtil.getEndDate(date)));
        if (paramDealer != null) {
            restrictions.addCriterion(Restrictions.eq("quodeal" + "." + ID, paramDealer.getId()));
        }
        if (paramDealerType != null) {
            restrictions.addCriterion(Restrictions.eq("quodeal.dealerType", paramDealerType));
        }
        return dailyReportService.count(restrictions);
    }

    /**
     * @param date
     * @return
     */
    private long getNbDeclinesFromBeginOfMonth(Date date) {
        Date dateBeginningOfMonth = DateUtils.getDateAtBeginningOfMonth(date);
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addAssociation("dealer", "quodeal", JoinType.INNER_JOIN);
        restrictions.addCriterion(Restrictions.eq(WKF_STATUS, QuotationWkfStatus.DEC));
        restrictions.addCriterion(Restrictions.ge("declineDate", DateFilterUtil.getStartDate(dateBeginningOfMonth)));
        restrictions.addCriterion(Restrictions.le("declineDate", DateFilterUtil.getEndDate(date)));
        if (paramDealer != null) {
            restrictions.addCriterion(Restrictions.eq("quodeal.id", paramDealer.getId()));
        }
        if (paramDealerType != null) {
            restrictions.addCriterion(Restrictions.eq("quodeal.dealerType", paramDealerType));
        }
        return dailyReportService.count(restrictions);
    }

    /**
     * @param date
     * @param isBefore
     * @return
     */
    private long getNbDeclinesBeforeAndAfterApproval(Date date, boolean isBefore) {
        Date dateBeginningOfMonth = DateUtils.getDateAtBeginningOfMonth(date);
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addAssociation("dealer", "quodeal", JoinType.INNER_JOIN);
        restrictions.addCriterion(Restrictions.eq(WKF_STATUS, QuotationWkfStatus.DEC));
        if (isBefore) {
            restrictions.addCriterion(Restrictions.isNull(ACCEPTATION_DATE));
        } else {
            restrictions.addCriterion(Restrictions.isNotNull(ACCEPTATION_DATE));
        }
        restrictions.addCriterion(Restrictions.ge("declineDate", DateFilterUtil.getStartDate(dateBeginningOfMonth)));
        restrictions.addCriterion(Restrictions.le("declineDate", DateFilterUtil.getEndDate(date)));

        if (paramDealerType != null) {
            restrictions.addCriterion(Restrictions.eq("quodeal.dealerType", paramDealerType));
        }
        if (paramDealer != null) {
            restrictions.addCriterion(Restrictions.eq("quodeal.id", paramDealer.getId()));
        }
        return dailyReportService.count(restrictions);
    }

    /**
     * @param sheet
     * @param iRow
     * @param dealerReport
     * @param selectDate
     * @return
     * @throws Exception
     */
    private int dataTable(final Sheet sheet, int iRow, DealerReport dealerReport, Date selectDate) throws Exception {
        String month = DateUtils.getDateLabel(selectDate, "MMMM");
        Row headerRow = sheet.createRow(iRow + 1);

        Dealer dealer = dealerReport.getDealer();
        iRow++;
        Drawing drawing = sheet.createDrawingPatriarch();
        ClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 0, iRow, iRow + 2, iRow);
        String templatePath = AppConfig.getInstance().getConfiguration().getString("specific.templatedir");
        String templateFileName = templatePath + "/GLF-logo-small.png";
        File image = new File(templateFileName);
        InputStream fStream = new FileInputStream(image);
        byte[] bytes = IOUtils.toByteArray(fStream);
        int pictureIdx = wb.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
        Picture picture = drawing.createPicture(anchor, pictureIdx);
        picture.resize();
        fStream.close();

        DailyReport selectDailyReport = dealerReport.getDailyReportSelectDate(selectDate);
        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 0, 20));

        for (int j = 0; j <= 19; j++) {
            createCell(headerRow, j, "", 14, false, false, true,
                    CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, false);
        }
        createCell(headerRow, 20, "", 14, true, false, true,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, false);

        headerRow = sheet.createRow(iRow);
        sheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 0, 20));
        iRow = iRow + 1;

        createCell(headerRow, 0, (dealer == null ? "Total" : dealer.getNameEn()), 18, false,
                false, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, false);
        createCell(headerRow, 20, "", 14, false, false, false,
                CellStyle.ALIGN_LEFT, true, BG_WHITE, FC_BLACK, false);

        headerRow = sheet.createRow(iRow);
        sheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 0, 20));
        iRow = iRow + 1;

        createCell(headerRow, 0, "DAILY REPORT " + month + " " + DateUtils.getDateLabel(selectDate, "yyyy"), 16,
                false, false, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, false);
        createCell(headerRow, 20, "", 14, false, false, false,
                CellStyle.ALIGN_LEFT, true, BG_WHITE, FC_BLACK, false);

        headerRow = sheet.createRow(iRow);
        sheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 0, 20));
        iRow = iRow + 1;

        createCell(headerRow, 20, "", 14, false, false, false,
                CellStyle.ALIGN_LEFT, true, BG_WHITE, FC_BLACK, false);

        headerRow = sheet.createRow(iRow);
        sheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 0, 18));
        iRow = iRow + 1;

        createCell(headerRow, 20, "", 14, false, false, false,
                CellStyle.ALIGN_LEFT, true, BG_WHITE, FC_BLACK, false);

        headerRow = sheet.createRow(iRow);
        headerRow.setHeight((short) 550);

        sheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 0, 1));
        createCell(headerRow, 0, "Appraisal", 14, false, false, false,
                CellStyle.ALIGN_RIGHT, true, BG_GREEN, FC_WHITE, false);

        createCell(headerRow, 2, roundDouble(selectDailyReport.getDealerAppraisalPercentage()), 15, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_GREEN, FC_WHITE, false);

        sheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 3, 4));
        createCell(headerRow, 3, "Applications", 14, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_GREEN, FC_WHITE, false);

        createCell(headerRow, 5, roundDouble(selectDailyReport.getApplicationsPercentage()), 15, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_GREEN, FC_WHITE, false);

        sheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 6, 7));
        createCell(headerRow, 6, "TARGET", 14, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_GREEN, FC_WHITE, false);

        createCell(headerRow, 8, dealerReport.getLowTarget() == 0 ? 0 : dealerReport.getLowTarget(), 15, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_GREEN, FC_WHITE, false);

        sheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 9, 10));
        createCell(headerRow, 9, "Contracts ", 14, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_GREEN, FC_WHITE, false);

        createCell(headerRow, 11, roundDouble(selectDailyReport.getContractsPercentage()), 15, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_GREEN, FC_WHITE, false);

        sheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 12, 15));
        createCell(headerRow, 12, "Accumulated New Contracts ", 15, false,
                false, false, CellStyle.ALIGN_CENTER, true, BG_GREEN, FC_WHITE, true);

        createCell(headerRow, 16, selectDailyReport.getDealerAccumulateNewContracts(), 15, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_GREEN, FC_WHITE, false);

        iRow = iRow + 1;
        headerRow = sheet.createRow(iRow);
        sheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 0, 7));
        sheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 8, 9));

        sheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 12, 15));
        createCell(headerRow, 12, "End of previous month ", 15, false, false,
                false, CellStyle.ALIGN_CENTER, true, BG_GREEN, FC_WHITE, true);

        createCell(headerRow, 13, "", 14, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_WHITE, false);

        createCell(headerRow, 14, "", 14, false, false, false,
                CellStyle.ALIGN_LEFT, true, BG_WHITE, FC_BLACK, false);

        createCell(headerRow, 16, selectDailyReport.getNbDealerContractsUntilLastMonth(), 15,
                false, false, false, CellStyle.ALIGN_CENTER, true, BG_GREEN, FC_WHITE, false);
        iRow = iRow + 1;

        int iCol = 0;
        Row tmpRow = sheet.createRow(iRow++);
        createCell(tmpRow, iCol++, DateUtils.getDateLabel(selectDate, "yyyy"), 16, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, false);
        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow, iCol - 1, iCol - 1));

        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 1, 4));
        createCell(tmpRow, 1, "SALES", 14, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_LIGHT_BLUE, FC_WHITE, false);
        createCell(tmpRow, 2, "", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_LIGHT_BLUE, FC_BLACK, false);
        createCell(tmpRow, 3, "", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_LIGHT_BLUE, FC_BLACK, false);
        createCell(tmpRow, 4, "", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_LIGHT_BLUE, FC_BLACK, false);

        createCell(tmpRow, 5, "Appraisal", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, FC_ORANGE, FC_BLACK, false);

        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 6, 12));
        createCell(tmpRow, 6, "UNDERWRITING", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_BLUE, FC_WHITE, false);
        createCell(tmpRow, 7, "", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_BLUE, FC_BLACK, false);
        createCell(tmpRow, 8, "", 14, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_BLUE, FC_WHITE, false);
        createCell(tmpRow, 9, "", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_BLUE, FC_BLACK, false);
        createCell(tmpRow, 10, "", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_BLUE, FC_BLACK, false);
        createCell(tmpRow, 11, "", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_BLUE, FC_BLACK, false);
        createCell(tmpRow, 12, "", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_BLUE, FC_BLACK, false);

        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 13, 16));
        createCell(tmpRow, 13, "OPERATION", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_GREEN, FC_WHITE, false);
        createCell(tmpRow, 14, "", 14, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_GREEN, FC_WHITE, false);
        createCell(tmpRow, 15, "", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_GREEN, FC_BLACK, false);
        createCell(tmpRow, 16, "", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_GREEN, FC_BLACK, false);

        iCol = 1;
        tmpRow = sheet.createRow(iRow++);
        int iColBorder = 0;
        createCellBorderBottomAndRight(tmpRow, iColBorder++, "");

        createCell(tmpRow, iCol++, "Appraisal", 14, false,
                true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);

        createCell(tmpRow, iCol++, "Apply", 14, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);

        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 3, 4));
        createCell(tmpRow, iCol++, "  Apply / Appraisal   ", 14, false, true,
                false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
        createCell(tmpRow, iCol++, "", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);

        createCell(tmpRow, iCol++, "In Process at Appraisal Team", 14, false, true,
                false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);

        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 6, 7));
        createCell(tmpRow, iCol++, "In Process at PoS", 14, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
        createCell(tmpRow, iCol++, "", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);

        createCell(tmpRow, iCol++, "In Process at UW", 14, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);

        createCell(tmpRow, iCol++, "Reject", 12, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);

        createCell(tmpRow, iCol++, "Reject / Application", 14, false, true,
                false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);

        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 11, 12));
        createCell(tmpRow, iCol++, "Approve", 14, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
        createCell(tmpRow, iCol++, "", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
        createCell(tmpRow, iCol++, "Pending New Contract", 14, false, true,
                false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
        createCell(tmpRow, iCol++, "Decline", 14, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
        createCell(tmpRow, iCol++, "Decline / Approve", 14, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
        createCell(tmpRow, iCol++, "New Contract", 14, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
        List<DailyReport> dailyReports = dealerReport.getDailyReports();
        dailyReports.add(dealerReport.getTotalDailyReport());

        //logger.info("----------One dealer table report part--------------------");

        if (dailyReports != null && !dailyReports.isEmpty()) {
            DailyReport totalDailyReport = getDefaultDailyReport();

            for (int index = 0; index < numberDay; index++) {
                int nbDay = index;
                nbDay++;
                //logger.info("Day : " + nbDay + " / Total : " + numberDay);
                Date date = (DateUtils.addDaysDate(DateUtils.getDateAtBeginningOfMonth(selectDate), index));
                DailyReport dailyReport = getDailyReport(dailyReports, date);
                mergedCellRow(sheet, iRow);
                tmpRow = sheet.createRow(iRow++);
                iCol = 0;

                if (dailyReport != null) {
                    if (dailyReport.getDate() != null) {
                        /*
                         * createNumericCellLeftBorderAlignLeft(tmpRow, iCol++,
						 * DateUtils.formatDate(dailyReport.getDate(),
						 * DEFAULT_DATE_FORMAT));
						 */
                        createNumericCell(tmpRow, iCol++, DateUtils.formatDate(dailyReport.getDate(), DEFAULT_DATE_FORMAT), LEFT_BORDER_ALIGN_LEFT, false);
                        createRowDataPerDay(tmpRow, iCol++, dailyReport, false);
                        totalDailyReport.plus(dailyReport);
                    }
                } else {
                    // out put zero
                    rowOutputZero(tmpRow, iCol++, date);
                }
            }
            //logger.info("--------End One dealer table report part-----------------------");
            mergedCellRow(sheet, iRow);
            tmpRow = sheet.createRow(iRow++);
            iCol = 0;
            createNumericCell(tmpRow, iCol++, "Total-Average", TOP_LEFT_BOTTOM_BORDER_ALIGN_LEFT, false);
            /*Long paddingNewContract = totalDailyReport.getPendingNewContract() == null ? 0 : totalDailyReport.getPendingNewContract();
            totalDailyReport.setInProcessAtPoS(roundLong((double) totalDailyReport.getInProcessAtPoS() / numberDay));
            totalDailyReport.setInProcessAtUW(roundLong((double) totalDailyReport.getInProcessAtUW() / numberDay));
            totalDailyReport.setPendingNewContract(roundLong((double) paddingNewContract / numberDay));*/
            createRowDataPerDay(tmpRow, iCol++, totalDailyReport, true);
        }

        iRow = iRow + 4;
        return iRow;

    }

    /**
     * @param tmpRow
     * @param iCol
     * @param date
     */
    private void rowOutputZero(final Row tmpRow, int iCol, Date date) {

        createNumericCell(tmpRow, iCol++, DateUtils.formatDate(date, DEFAULT_DATE_FORMAT), LEFT_BORDER_ALIGN_LEFT, false);

        createNumericCell(tmpRow, iCol++, 0, NONE_BORDER_ALIGN_RIGHT, false);
        createNumericCell(tmpRow, iCol++, 0, NONE_BORDER_ALIGN_RIGHT, false);
        createNumericCell(tmpRow, iCol++, 0, NONE_BORDER_ALIGN_RIGHT_PERCENTAGE, false);
        createNumericCell(tmpRow, iCol++, "", NONE_BORDER_ALIGN_RIGHT_PERCENTAGE, false);
        createNumericCell(tmpRow, iCol++, 0, NONE_BORDER_ALIGN_RIGHT, false);
        createNumericCell(tmpRow, iCol++, 0, NONE_BORDER_ALIGN_RIGHT, false);
        createNumericCell(tmpRow, iCol++, "", NONE_BORDER_ALIGN_RIGHT_PERCENTAGE, false);
        createNumericCell(tmpRow, iCol++, 0, NONE_BORDER_ALIGN_RIGHT, false);
        createNumericCell(tmpRow, iCol++, 0, NONE_BORDER_ALIGN_RIGHT, false);
        createNumericCell(tmpRow, iCol++, 0, NONE_BORDER_ALIGN_RIGHT_PERCENTAGE, false);
        createNumericCell(tmpRow, iCol++, 0, NONE_BORDER_ALIGN_RIGHT, false);
        createNumericCell(tmpRow, iCol++, "", NONE_BORDER_ALIGN_RIGHT, false);
        createNumericCell(tmpRow, iCol++, 0, NONE_BORDER_ALIGN_RIGHT, false);
        createNumericCell(tmpRow, iCol++, 0, NONE_BORDER_ALIGN_RIGHT, false);
        createNumericCell(tmpRow, iCol++, 0, NONE_BORDER_ALIGN_RIGHT_PERCENTAGE, false);
        createNumericCell(tmpRow, iCol++, 0, RIGHT_BORDER, false);

    }

    /**
     * @param sheet
     * @param iRow
     */
    private void mergedCellRow(final Sheet sheet, int iRow) {
        sheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 3, 4));
        sheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 6, 7));
        sheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 11, 12));
        sheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 17, 18));
        sheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 19, 20));
    }

    /**
     * @return
     */
    private DailyReport getDefaultDailyReport() {
        DailyReport d = new DailyReport();
        d.setNbDealerAppraisal(0l);
        d.setNumberProcessAppraisal(0l);
        d.setApply(0l);
        d.setInProcessAtPoS(0l);
        d.setInProcessAtUW(0l);
        d.setReject(0l);
        d.setApprove(0l);
        d.setPendingNewContract(0l);
        d.setDecline(0l);
        d.setNewContract(0l);
        return d;
    }

    /**
     * @param dailyReports
     * @param date
     * @return
     */
    private DailyReport getDailyReport(List<DailyReport> dailyReports, Date date) {
        if (dailyReports != null && !dailyReports.isEmpty()) {
            for (DailyReport dailyReport : dailyReports) {
                if (dailyReport.getDate() != null && DateUtils.isSameDay(DateUtils.getDateAtBeginningOfDay(dailyReport.getDate()), date)) {
                    return dailyReport;
                }
            }
        }
        return null;
    }

    /**
     * @param tmpRow
     * @param iCol
     * @param dailyReport
     */
    private void createRowDataPerDay(Row tmpRow, int iCol, DailyReport dailyReport, boolean isTotalRow) {

        createNumericCell(tmpRow, iCol++, MyNumberUtils.getLong(dailyReport.getNbDealerAppraisal()), NONE_BORDER_ALIGN_RIGHT, isTotalRow);
        createNumericCell(tmpRow, iCol++, MyNumberUtils.getLong(dailyReport.getApply()), NONE_BORDER_ALIGN_RIGHT, isTotalRow);
        double applyAppraisal = 0;
        if (MyNumberUtils.getLong(dailyReport.getNbDealerAppraisal()) > 0) {
            applyAppraisal = MyNumberUtils.getLong(dailyReport.getApply() * 100) / (double) MyNumberUtils.getLong(dailyReport.getNbDealerAppraisal());
        }
        createNumericCell(tmpRow, iCol++, roundDouble(applyAppraisal), NONE_BORDER_ALIGN_RIGHT_PERCENTAGE, isTotalRow);
        createNumericCell(tmpRow, iCol++, "", NONE_BORDER_ALIGN_RIGHT_PERCENTAGE, isTotalRow);

        createNumericCell(tmpRow, iCol++, MyNumberUtils.getLong(dailyReport.getNumberProcessAppraisal()), NONE_BORDER_ALIGN_RIGHT, isTotalRow);
        createNumericCell(tmpRow, iCol++, MyNumberUtils.getLong(dailyReport.getInProcessAtPoS()), NONE_BORDER_ALIGN_RIGHT, isTotalRow);
        createNumericCell(tmpRow, iCol++, "", NONE_BORDER_ALIGN_RIGHT, isTotalRow);

        createNumericCell(tmpRow, iCol++, MyNumberUtils.getLong(dailyReport.getInProcessAtUW()), NONE_BORDER_ALIGN_RIGHT, isTotalRow);
        createNumericCell(tmpRow, iCol++, MyNumberUtils.getLong(dailyReport.getReject()), NONE_BORDER_ALIGN_RIGHT, isTotalRow);

        double rejectApplicant = 0;
        if (MyNumberUtils.getLong(dailyReport.getApply()) > 0) {
            rejectApplicant = roundDouble(MyNumberUtils.getLong((dailyReport.getReject()) * 100) / (double) MyNumberUtils.getLong(dailyReport.getApply()));
        }

        createNumericCell(tmpRow, iCol++, roundDouble(rejectApplicant), NONE_BORDER_ALIGN_RIGHT_PERCENTAGE, isTotalRow);
        createNumericCell(tmpRow, iCol++, MyNumberUtils.getLong(dailyReport.getApprove()), NONE_BORDER_ALIGN_RIGHT, isTotalRow);
        createNumericCell(tmpRow, iCol++, "", NONE_BORDER_ALIGN_RIGHT, isTotalRow);

        createNumericCell(tmpRow, iCol++, MyNumberUtils.getLong(dailyReport.getPendingNewContract()), NONE_BORDER_ALIGN_RIGHT, isTotalRow);

        createNumericCell(tmpRow, iCol++, MyNumberUtils.getLong(dailyReport.getDecline()), NONE_BORDER_ALIGN_RIGHT, isTotalRow);
        double declinedApproved = 0;
        if (MyNumberUtils.getLong(dailyReport.getApprove()) > 0) {
            declinedApproved = roundDouble(MyNumberUtils.getLong((dailyReport.getDecline()) * 100) / (double) MyNumberUtils.getLong(dailyReport.getApprove()));
        }
        createNumericCell(tmpRow, iCol++, roundDouble(declinedApproved), NONE_BORDER_ALIGN_RIGHT_PERCENTAGE, isTotalRow);
        createNumericCell(tmpRow, iCol++, MyNumberUtils.getLong(dailyReport.getNewContract()), RIGHT_BORDER, isTotalRow);
    }

    /**
     * @param sheet
     * @param iRow
     * @param selectDate
     * @return
     * @throws Exception
     */
    protected int overviewTable(final Sheet sheet, int iRow, List<DealerReport> dealerReports, Date selectDate) throws Exception {
		/* Create total data header */
        String month = DateUtils.getDateLabel(selectDate, "MMMM");
        Row headerRow = sheet.createRow(iRow - 1);

        Drawing drawing = sheet.createDrawingPatriarch();
        ClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 0, iRow, iRow + 2, iRow);
        String templatePath = AppConfig.getInstance().getConfiguration().getString("specific.templatedir");
        String templateFileName = templatePath + "/GLF-logo-small.png";
        File image = new File(templateFileName);
        InputStream fStream = new FileInputStream(image);
        byte[] bytes = IOUtils.toByteArray(fStream);
        int pictureIdx = wb.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
        Picture picture = drawing.createPicture(anchor, pictureIdx);
        picture.resize();
        fStream.close();

        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 0, 15));
        for (int j = 0; j <= 14; j++) {
            createCell(headerRow, j, "", 14, false, false, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, false);
        }
        createCell(headerRow, 15, "", 14, false, false, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, false);

        headerRow = sheet.createRow(iRow);
        sheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 0, 15));
        iRow = iRow + 1;
        createCell(headerRow, 0, "OVERVIEW", 18, false, false, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, false);
        createCell(headerRow, 15, "", 14, false, false, false, CellStyle.ALIGN_LEFT, true, BG_WHITE, FC_BLACK, false);

        headerRow = sheet.createRow(iRow);
        sheet.addMergedRegion(new CellRangeAddress(iRow, iRow, 0, 15));
        iRow = iRow + 1;
        createCell(headerRow, 0, "DAILY REPORT " + month + " " + DateUtils.getDateLabel(selectDate, "yyyy"), 16, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, false);
        createCell(headerRow, 15, "", 14, false, false, false,
                CellStyle.ALIGN_LEFT, true, BG_WHITE, FC_BLACK, false);

        iRow = iRow + 1;
        headerRow = sheet.createRow(iRow - 1);
        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 0, 15));
        createCell(headerRow, 15, "", 14, false, false, false,
                CellStyle.ALIGN_LEFT, true, BG_WHITE, FC_BLACK, false);

        int iCol = 0;
        Row tmpRow = sheet.createRow(iRow++);
        createCell(tmpRow, iCol++, "", 9, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_WHITE, true);
        createCell(tmpRow, iCol++, "", 9, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_WHITE, true);
        createCell(tmpRow, iCol++, "", 9, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_WHITE, true);
        createCell(tmpRow, iCol++, "", 9, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_WHITE, true);

        createCell(tmpRow, iCol++, "Appraisal", 14, false, false, false,
                CellStyle.ALIGN_CENTER, true, SKY_BLUE, FC_WHITE, true);

        createCell(tmpRow, iCol++, "Applications", 14, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_BLUE, FC_WHITE, true);

        createCell(tmpRow, iCol++, "Contracts", 14, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_GREEN, FC_WHITE, true);

        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, iCol, iCol + 1));
        createCell(tmpRow, iCol++, " Contracts / Applications.", 14, false,
                false, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
        createCell(tmpRow, iCol++, "", 9, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_WHITE, true);

        createCell(tmpRow, iCol++, "Target", 14, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);

        createCell(tmpRow, iCol++, "Target", 14, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);

        createCell(tmpRow, iCol++, "", 9, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_WHITE, true);
        createCell(tmpRow, iCol++, "", 9, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_WHITE, true);
        createCell(tmpRow, iCol++, "", 9, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_WHITE, true);
        createCell(tmpRow, iCol++, "", 9, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_WHITE, true);

        // today application term
        int[] numberApplicationTodayInter = getNumApplicationsByTerm(selectDate, paramDealer, paramDealerType, QuotationWkfStatus.QUO);
        int numApplicationsTerm12 = numberApplicationTodayInter[0];
        int numApplicationsTerm24 = numberApplicationTodayInter[1];
        int numApplicationsTerm36 = numberApplicationTodayInter[2];

        // per month application term
        int[] numberApplicationForMothInTerm = getDetailNumApplicationsOfMonthByTerm(selectDate, paramDealer, paramDealerType, QuotationWkfStatus.QUO);
        int numberApplication4MothTerm12 = numberApplicationForMothInTerm[0];
        int numberApplication4MothTerm14 = numberApplicationForMothInTerm[1];
        int numberApplication4MothTerm36 = numberApplicationForMothInTerm[2];

        // APPLICANT PER MOTH
        int totalApplicantPerMoth = numberApplicationForMothInTerm[0] + numberApplicationForMothInTerm[1] + numberApplicationForMothInTerm[2];
        double percentageOfApplicantPerMonth12 = calculatePercentageForAdvanceAndTerm(numberApplication4MothTerm12, totalApplicantPerMoth);
        double percentageOfApplicantPerMonth24 = calculatePercentageForAdvanceAndTerm(numberApplication4MothTerm14, totalApplicantPerMoth);
        double percentageOfApplicantPerMonth36 = calculatePercentageForAdvanceAndTerm(numberApplication4MothTerm36, totalApplicantPerMoth);

        // CONTRACT TODAY
        int[] numberContractPerDay = getNumApplicationsByTerm(selectDate, paramDealer, paramDealerType, QuotationWkfStatus.ACT);
        int contractIn12TermPerDay = numberContractPerDay[0];
        int contractIn24TermPerDay = numberContractPerDay[1];
        int contractIn36TermPerDay = numberContractPerDay[2];

        // CONTRACT PER MONTH
        int[] contractPerMonth = getDetailNumApplicationsOfMonthByTerm(selectDate, paramDealer, paramDealerType, QuotationWkfStatus.ACT);
        int contractPerMontForTerm12 = contractPerMonth[0];
        int contractPerMontForTerm24 = contractPerMonth[1];
        int contractPerMontForTerm36 = contractPerMonth[2];

        // TOTAL CONTRACT
        int totalContractPerMonth = contractPerMonth[0] + contractPerMonth[1] + contractPerMonth[2];
        double percentageContractForTerm12 = calculatePercentageForAdvanceAndTerm(contractPerMontForTerm12, totalContractPerMonth);
        double percentageContractForTerm24 = calculatePercentageForAdvanceAndTerm(contractPerMontForTerm24, totalContractPerMonth);
        double percentageContractForTerm36 = calculatePercentageForAdvanceAndTerm(contractPerMontForTerm36, totalContractPerMonth);


        iCol = 0;
        if (dealerReports != null && !dealerReports.isEmpty()) {
            //int nbRows = dealerReports.size() > 50 ? dealerReports.size() : 51;
            int nbRows = dealerReports.size() > 55 ? dealerReports.size() : 56;
            int totalTargetLow = 0;
            int totalNbApplications = 0;
            int totalNbContracts = 0;
            int totalNbAppraisal = 0;

            double totalRejects = getNbRejectsFromBeginOfMonth(selectDate);
            double totalDeclines = getNbDeclinesFromBeginOfMonth(selectDate);

            long totalDeclineBeforeApproval = getNbDeclinesBeforeAndAfterApproval(selectDate, true);
            long totalDeclineAfterApproval = getNbDeclinesBeforeAndAfterApproval(selectDate, false);

            int totalIRow = iRow + 1;
            Row totalRow = null;
            int accumlateIRow = 0;
            Row accumlateRow = null;
            //logger.info("----------------Overview table report part--------------------");
            //logger.info("Total dealer : " + dealerReports.size());
            for (int i = 0; i < nbRows; i++) {
                tmpRow = sheet.createRow(iRow++);
                iCol = 0;
                if (dealerReports.size() < 46 && i == dealerReports.size()) {
                    totalRow = tmpRow;
                }
                if (i < dealerReports.size()) {
                    DealerReport dealerReport = dealerReports.get(i);
                    Dealer dealer = dealerReport.getDealer();
                    int targetLow = dealerReport.getLowTarget();
                    totalTargetLow += targetLow;

                    DailyReport selectDailyReport = dealerReport.getSelectDailyReport(selectDate);
                    long nbAppraisal = selectDailyReport.getNbDealerAppraisal() == null ? 0 : selectDailyReport.getNbDealerAppraisal();
                    long nbApplications = selectDailyReport.getApply() == null ? 0 : selectDailyReport.getApply();
                    long nbContracts = selectDailyReport.getNewContract() == null ? 0 : selectDailyReport.getNewContract();
                    totalNbAppraisal += nbAppraisal;
                    totalNbApplications += nbApplications;
                    totalNbContracts += nbContracts;

                    // APPRAISAL APPLICATION .... PERDEALER
                    createCell(tmpRow, iCol++, dealer.getDealerAddresses().get(0).getAddress().getProvince().getDescEn(), 13, false, false, false,
                            CellStyle.ALIGN_RIGHT, true, BG_WHITE, FC_BLACK, false);
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, iCol, iCol + 2));
                    createCell(tmpRow, iCol++, dealer.getNameEn(), 14, false, false, false, CellStyle.ALIGN_LEFT, true, BG_WHITE, FC_BLACK, false);
                    createCell(tmpRow, iCol++, "", 9, false, false, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);

                    createCell(tmpRow, iCol++, nbAppraisal, 15, false, false, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);

                    createCell(tmpRow, iCol++, nbApplications, 15, false, false, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, nbContracts, 15, false, false, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, iCol, iCol + 1));
                    createCell(tmpRow, iCol++, (nbApplications > 0 ? roundDouble((100 * nbContracts) / nbApplications) : 0d), 15, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);

                    createCell(tmpRow, iCol++, (targetLow > 0 ? roundDouble((100 * nbContracts) / targetLow) : 0d), 15, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);

                    createCell(tmpRow, iCol++, targetLow, 15, false, false, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);

                }
                iCol = 12;
                if (i == 0) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "APPLICATIONS", 14, false, false, false,
                            CellStyle.ALIGN_RIGHT, false, BG_WHITE, FC_BLUE, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLUE, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLUE, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_GREEN, true);
                } else if (i == 1) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "AVERAGE ALL POS", 14, false, false, false,
                            CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, getAverageApplications(selectDate, dealerReports), 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 2) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "BEST PERFORMING", 14, false, false, false,
                            CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    long bestPerformingApplications = getBestPerformingApplications(selectDate, dealerReports);
                    createCell(tmpRow, iCol++, bestPerformingApplications, 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);

                    createCell(tmpRow, iCol++, getBestPerformingApplicationsPOS(bestPerformingApplications, selectDate, dealerReports), 14, false, true, false,
                            CellStyle.ALIGN_LEFT, true, BG_WHITE, FC_BLACK, false);
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 16, 18));
                    createCell(tmpRow, iCol++, "", 9, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 3) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "WORSE PERFORMING", 14, false, false, false,
                            CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    long worsePerformingApplications = getWorsePerformingApplications(selectDate, dealerReports);
                    createCell(tmpRow, iCol++, worsePerformingApplications, 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);

                    createCell(tmpRow, iCol++, getWorsePerformingApplicationsPOS(worsePerformingApplications, selectDate, dealerReports), 14, false, true, false,
                            CellStyle.ALIGN_LEFT, true, BG_WHITE, FC_BLACK, false);

                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 16, 18));
                    createCell(tmpRow, iCol++, "", 9, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 4) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "CONTRACTS", 14, false, false, false,
                            CellStyle.ALIGN_RIGHT, false, BG_WHITE, FC_GREEN, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, false, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, false, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, true, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_GREEN, true);
                } else if (i == 5) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "AVERAGE ALL POS", 14, false, false, false,
                            CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, getAverageContracts(selectDate, dealerReports), 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 6) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1,
                            iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "BEST PERFORMING", 14, false, false, false,
                            CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    long bestPerformingContracts = getBestPerformingContracts(
                            selectDate, dealerReports);
                    createCell(tmpRow, iCol++, bestPerformingContracts, 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, getBestPerformingContractsPOS(bestPerformingContracts, selectDate, dealerReports), 14, false, true, false,
                            CellStyle.ALIGN_LEFT, true, BG_WHITE, FC_BLACK, false);
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 16, 18));
                    createCell(tmpRow, iCol++, "", 9, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 7) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "WORSE PERFORMING", 14, false, false, false,
                            CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    long worsePerformingContracts = getWorsePerformingContracts(
                            selectDate, dealerReports);
                    createCell(tmpRow, iCol++, worsePerformingContracts, 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, getWorsePerformingContractsPOS(worsePerformingContracts, selectDate, dealerReports), 14, false, true, false,
                            CellStyle.ALIGN_LEFT, true, BG_WHITE, FC_BLACK, false);
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 16, 18));
                    createCell(tmpRow, iCol++, "", 9, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 8) {
                    cellSpaceBorderRight(tmpRow, iCol++);
                } else if (i == 9) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "REJECT REASONS", 14, false, false, false,
                            CellStyle.ALIGN_RIGHT, false, BG_WHITE, FC_DARK_BLUE, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, false, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, false, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, true, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_GREEN, true);
                } else if (i == 10) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "Out of policy", 14, false, false, false,
                            CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, totalRejects == 0d ? 0d : roundDouble((getNbRejectByReasonCode("OUP", selectDate, paramDealer, paramDealerType)
                                    / totalRejects) * 100), 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 11) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "No repayment capacity after CBC", 14, false, false, false,
                            CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, totalRejects == 0d ? 0d : roundDouble((getNbRejectByReasonCode("CBC", selectDate, paramDealer,
                            paramDealerType) / totalRejects) * 100), 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 12) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "Bad History in CBC", 14, false, false, false,
                            CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, totalRejects == 0d ? 0d : roundDouble((getNbRejectByReasonCode("BPH", selectDate, paramDealer,
                            paramDealerType) / totalRejects) * 100), 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 13) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "No Repayment Capacity", 14,
                            false, false, false, CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, totalRejects == 0d ? 0d : roundDouble((getNbRejectByReasonCode("NC", selectDate, paramDealer,
                            paramDealerType) / totalRejects) * 100), 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 14) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "Untrustworthy applicant", 14, false, false, false,
                            CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, totalRejects == 0d ? 0d : roundDouble((getNbRejectByReasonCode("UA", selectDate, paramDealer,
                            paramDealerType) / totalRejects) * 100), 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 15) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "Impossible to assess income", 14, false, false, false, CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, totalRejects == 0d ? 0d : roundDouble((getNbRejectByReasonCode("ITAI", selectDate, paramDealer,
                            paramDealerType) / totalRejects) * 100), 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 16) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "Automatic reject", 14, false, false, false,
                            CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, totalRejects == 0d ? 0d : roundDouble((getNbRejectByReasonCode("AT", selectDate, paramDealer,
                            paramDealerType) / totalRejects) * 100), 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK,
                            true);
                } else if (i == 17) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "Other", 14, false, false, false,
                            CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, totalRejects == 0d ? 0d : roundDouble((getNbRejectByReasonCode("Other", selectDate, paramDealer,
                            paramDealerType) / totalRejects) * 100), 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 18) {
                    cellSpaceBorderRight(tmpRow, iCol++);
                    // decline
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow, 16, 16));
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow, 17, 18));
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow, 19, 20));
                    //
                    createCell(tmpRow, 16, "", 13, false, true, false, CellStyle.ALIGN_CENTER,
                            true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, 17, "Decline before approval", 13, false, true, false, CellStyle.ALIGN_CENTER,
                            true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, 18, "", 9, false, true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, 19, "Decline after approval", 13, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, 20, "", 9, false, true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 19) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "DECLINE REASONS", 14, false, false, false,
                            CellStyle.ALIGN_RIGHT, false, BG_WHITE, FC_ORANGE, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, false, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, false, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, true, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_GREEN, true);
                    createCell(tmpRow, 16, "", 9, true, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_GREEN, true);
                    createCell(tmpRow, 18, "", 9, true, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_GREEN, true);
                    createCell(tmpRow, 20, "", 9, true, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_GREEN, true);
                } else if (i == 20) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "Don't need cash for now", 14, false, false, false,
                            CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, totalDeclines == 0d ? 0d : roundDouble((getNbDeclineByReasonCode("14", selectDate, paramDealer,
                            paramDealerType) / totalDeclines) * 100), 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    // total decline
                    createCell(tmpRow, 16, "Total", 13, false, true, false, CellStyle.ALIGN_CENTER,
                            true, BG_WHITE, FC_BLACK, true);
                    // decline before approval
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 17, 18));
                    createCell(tmpRow, 17, totalDeclineBeforeApproval, 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, 18, "", 9, false, true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    // decline after approval
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 19, 20));
                    createCell(tmpRow, 19, totalDeclineAfterApproval, 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, 20, "", 9, false, true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 21) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "Higher interest rate", 14,
                            false, false, false, CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, totalDeclines == 0d ? 0d : roundDouble((getNbDeclineByReasonCode("15", selectDate, paramDealer,
                            paramDealerType) / totalDeclines) * 100), 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 22) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "Need cash for next time", 12, false, false, false,
                            CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, false);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, totalDeclines == 0d ? 0d : roundDouble((getNbDeclineByReasonCode("16", selectDate, paramDealer,
                            paramDealerType) / totalDeclines) * 100), 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 23) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "Late Process", 12, false, false, false,
                            CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, totalDeclines == 0d ? 0d : roundDouble((getNbDeclineByReasonCode("17", selectDate, paramDealer,
                            paramDealerType) / totalDeclines) * 100), 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 24) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "Appraisal Price Lower than Customer's Demand", 14, false, false, false,
                            CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, totalDeclines == 0d ? 0d : roundDouble((getNbDeclineByReasonCode("18", selectDate, paramDealer,
                            paramDealerType) / totalDeclines) * 100), 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 25) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "Appraisal Price Higher than Customer's Demand",
                            14, false, false, false, CellStyle.ALIGN_RIGHT,
                            true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, totalDeclines == 0d ? 0d : roundDouble((getNbDeclineByReasonCode("19", selectDate, paramDealer,
                            paramDealerType) / totalDeclines) * 100), 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 26) {
                    cellSpaceBorderRight(tmpRow, iCol++);
                } else if (i == 27) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "OVERDUE IN % OF PORTFOLIO", 14,
                            false, false, false, CellStyle.ALIGN_RIGHT, false, BG_WHITE, FC_RED, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, false, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, false, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, true, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_GREEN, true);
                } else if (i == 28) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "PAR > 30 days", 14, false,
                            false, false, CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    // PAR > 30 days
                    createCell(tmpRow, iCol++, "", 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 29) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "PAR > 90 days", 14, false,
                            false, false, CellStyle.ALIGN_RIGHT, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    // PAR > 90 days
                    createCell(tmpRow, iCol++, "", 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 30) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "PAR > 180 days", 14, false,
                            false, false, CellStyle.ALIGN_RIGHT, true, BG_GREY,
                            FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    // PAR > 180 days
                    createCell(tmpRow, iCol++, "", 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 31) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1,
                            iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "Total Overdue", 14, false,
                            false, false, CellStyle.ALIGN_RIGHT, true, BG_GREY,
                            FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK,
                            true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 15, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 32) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "APPRAISAL TODAY", 14, false,
                            true, false, CellStyle.ALIGN_RIGHT, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, getNbAppraisalToday(selectDate, dealerReports),
                            15, false, true, false, CellStyle.ALIGN_CENTER,
                            true, BG_WHITE, FC_BLACK, true);
                } else if (i == 33) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "APPLICATIONS TODAY", 14, false,
                            true, false, CellStyle.ALIGN_RIGHT, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, getNbApplicationsToday(selectDate, dealerReports),
                            15, false, true, false, CellStyle.ALIGN_CENTER,
                            true, BG_WHITE, FC_BLACK, true);
                } else if (i == 34) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "CONTRACTS TODAY", 14, false,
                            true, false, CellStyle.ALIGN_RIGHT, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, getNbContractsSelectDate(selectDate, dealerReports), 15, false,
                            true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 35) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 14));
                    createCell(tmpRow, iCol++, "TOTAL CONTRACTS THIS MONTH",
                            14, false, true, false, CellStyle.ALIGN_RIGHT,
                            true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, getNbContractsOfMonth(selectDate, dealerReports),
                            15, false, true, false, CellStyle.ALIGN_CENTER,
                            true, BG_WHITE, FC_BLACK, true);
                } else if (i == 36) {
                    cellSpaceBorderRight(tmpRow, iCol++);
                } else if (i == 37) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1,
                            iRow - 1, 12, 15));
                    createCell(tmpRow, iCol++, "Detail of application today",
                            13, false, true, false, CellStyle.ALIGN_CENTER,
                            true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK,
                            true);
                    createCell(tmpRow, iCol++, "", 9, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, true, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    // contract_old in term
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1,
                            iRow - 1, 16, 20));
                    createCell(tmpRow, iCol++, "Detail of Contract today",
                            13, false, true, false, CellStyle.ALIGN_CENTER,
                            true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, true, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, true, true, true,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 38) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 13));
                    createCell(tmpRow, iCol++, "Application Term", 13, false,
                            true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, true, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 14, 15));
                    createCell(tmpRow, iCol++, "Accumulated for the month", 13,
                            false, true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, true, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    // contract_old in term
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 16, 17));
                    createCell(tmpRow, iCol++, "Contract term", 13, false,
                            true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, true, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 18, 20));
                    createCell(tmpRow, iCol++, "Accumulated for the month", 13,
                            false, true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, true, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, true, true, true,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 39) {
                    createCell(tmpRow, iCol++, "Today", 13, true, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, numApplicationsTerm12 + numApplicationsTerm24 + numApplicationsTerm36, 15, true,
                            true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, numberApplication4MothTerm12 + numberApplication4MothTerm14 + numberApplication4MothTerm36,
                            15, true, true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    // total of advance payment by percentage
                    createCell(tmpRow, iCol++, roundDouble(percentageOfApplicantPerMonth12 + percentageOfApplicantPerMonth24 + percentageOfApplicantPerMonth36),
                            15, true, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    // contract_old in term
                    createCell(tmpRow, iCol++, "Today", 14, true, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, contractIn12TermPerDay + contractIn24TermPerDay + contractIn36TermPerDay, 15, true,
                            true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 18, 19));
                    createCell(tmpRow, iCol++, contractPerMontForTerm12 + contractPerMontForTerm24 + contractPerMontForTerm36,
                            15, true, true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, true, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    // total of term by percentage
                    createCell(tmpRow, iCol++, roundDouble(percentageContractForTerm12 + percentageContractForTerm24 + percentageContractForTerm36),
                            15, true, true, true, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 40) {
                    createCell(tmpRow, iCol++, "12", 15, true, true, false,
                            CellStyle.ALIGN_RIGHT, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, numApplicationsTerm12, 15, true, true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, numberApplication4MothTerm12, 14, true, true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    // advance payment 10% by percentage
                    createCell(tmpRow, iCol++, roundDouble(percentageOfApplicantPerMonth12), 15, true, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    // contract_old in term
                    createCell(tmpRow, iCol++, "12", 15, true, true, false,
                            CellStyle.ALIGN_RIGHT, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, contractIn12TermPerDay, 15,
                            true, true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 18, 19));
                    //
                    createCell(tmpRow, iCol++, contractPerMontForTerm12,
                            15, true, true, false, CellStyle.ALIGN_CENTER,
                            true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, true, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    // 12 term by percentage
                    createCell(tmpRow, iCol++, roundDouble(percentageContractForTerm12), 15, true, true, true,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 41) {
                    createCell(tmpRow, iCol++, "24", 15, true, true, false,
                            CellStyle.ALIGN_RIGHT, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, numApplicationsTerm24, 15, true, true, false, CellStyle.ALIGN_CENTER,
                            true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, numberApplication4MothTerm14, 15, true,
                            true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    // advance payment 20% by percentage
                    createCell(tmpRow, iCol++, roundDouble(percentageOfApplicantPerMonth24), 15, true, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    // contract_old in term
                    createCell(tmpRow, iCol++, "24", 15, true, true, false,
                            CellStyle.ALIGN_RIGHT, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, contractIn24TermPerDay, 15,
                            true, true, false, CellStyle.ALIGN_CENTER, true,
                            BG_WHITE, FC_BLACK, true);
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 18, 19));
                    //
                    createCell(tmpRow, iCol++, contractPerMontForTerm24, 15, true, true, false, CellStyle.ALIGN_CENTER,
                            true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, true, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    // 24 term by percentage
                    createCell(tmpRow, iCol++, roundDouble(percentageContractForTerm24), 15, true, true, true,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 42) {
                    createCell(tmpRow, iCol++, "36", 15, true, true, false,
                            CellStyle.ALIGN_RIGHT, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, numApplicationsTerm36,
                            15, true, true, false, CellStyle.ALIGN_CENTER,
                            true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, numberApplication4MothTerm36, 15, true,
                            true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    // advance payment 30% by percentage
                    createCell(tmpRow, iCol++, roundDouble(percentageOfApplicantPerMonth36), 15, true, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    // contract_old in term
                    createCell(tmpRow, iCol++, "36", 15, true, true, false,
                            CellStyle.ALIGN_RIGHT, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, contractIn36TermPerDay, 15,
                            true, true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 18, 19));
                    //
                    createCell(tmpRow, iCol++, contractPerMontForTerm36,
                            15, true, true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, true, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    // 36 term by percentage
                    createCell(tmpRow, iCol++, roundDouble(percentageContractForTerm36), 15, true, true, true,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 43) {
                    cellSpaceBorderRight(tmpRow, iCol++);
                } else if (i == 44) {
                    cellSpaceBorderRight(tmpRow, iCol++);
                } else if (i == 45) {
                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 12, 13));
                    createCell(tmpRow, iCol++, "Accumulated contract for the month", 13, false,
                            true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, true, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);

                    sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 14, 15));
                    createCell(tmpRow, iCol++, "Target", 13,
                            false, true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 13, true, true, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                } else if (i == 46) {
                    accumlateRow = tmpRow;
                    accumlateIRow = iRow;
                } else {
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                    createCell(tmpRow, iCol++, "", 9, false, false, false,
                            CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
                }
            }

            sheet.addMergedRegion(new CellRangeAddress(accumlateIRow - 1, accumlateIRow - 1, 12, 13));
            createCell(accumlateRow, 12, totalNbContracts, 15, false, true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
            createCell(accumlateRow, 13, "", 15, true, true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);

            sheet.addMergedRegion(new CellRangeAddress(accumlateIRow - 1, accumlateIRow - 1, 14, 15));
            createCell(accumlateRow, 14, (totalTargetLow > 0 ? roundDouble((100 * totalNbContracts) / totalTargetLow) : 0d), 15, false, true, false,
                    CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
            createCell(accumlateRow, 15, "", 15, true, true, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);

            tmpRow = totalRow == null ? sheet.createRow(iRow++) : totalRow;
            totalIRow = totalRow != null ? totalIRow + dealerReports.size() : iRow;
            iCol = 0;

            // TOTAL FOR PER DEALER FIRST SHEET
            createCell(tmpRow, iCol++, "Total", 14, false, false, false,
                    CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_GREEN, true);
            createCell(tmpRow, iCol++, "All POS", 14, false, false, false,
                    CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_GREEN, true);
            createCell(tmpRow, iCol++, "", 9, false, false, false,
                    CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_GREEN, true);
            createCell(tmpRow, iCol++, "", 9, false, false, false,
                    CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_GREEN, true);
            createCell(tmpRow, iCol++, totalNbAppraisal, 15, false, false,
                    false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_GREEN, true);
            createCell(tmpRow, iCol++, totalNbApplications, 15, false, false,
                    false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_GREEN, true);
            createCell(tmpRow, iCol++, totalNbContracts, 15, false, false,
                    false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_GREEN, true);
            sheet.addMergedRegion(new CellRangeAddress(totalIRow - 1, totalIRow - 1, iCol, iCol + 1));
            createCell(tmpRow, iCol++, (totalNbApplications > 0 ? roundDouble((100 * totalNbContracts) / totalNbApplications) : 0d),
                    15, false, false, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_GREEN, true);
            createCell(tmpRow, iCol++, "", 9, false, false, false, CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_GREEN, true);
            createCell(tmpRow, iCol++, (totalTargetLow > 0 ? roundDouble((100 * totalNbContracts) / totalTargetLow) : 0d), 15, false, false, false,
                    CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_GREEN, true);
            createCell(tmpRow, iCol++, totalTargetLow, 15, false, false, false,
                    CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_GREEN, true);
        }

        logger.info("----------------End overview table report part----------------");
        return iRow;
    }

    /**
     * @param tmpRow
     * @param iCol
     */
    private void cellSpaceBorderRight(Row tmpRow, int iCol) {
        createCell(tmpRow, iCol++, "", 9, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
        createCell(tmpRow, iCol++, "", 9, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
        createCell(tmpRow, iCol++, "", 9, false, false, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
        createCell(tmpRow, iCol++, "", 9, true, false, false,
                CellStyle.ALIGN_CENTER, true, BG_WHITE, FC_BLACK, true);
    }

    /**
     * @param value1
     * @param value2
     * @return
     */
    private double calculatePercentageForAdvanceAndTerm(int value1, int value2) {
        if (value2 > 0) {
            return (100 * value1) / (double) value2;
        }
        return 0d;
    }

    /**
     * Detail of accumulated application per advance payment and contract_old term for
     * month
     *
     * @param selectDate
     * @return
     */
    private int[] getDetailNumApplicationsOfMonthByTerm(Date selectDate, Dealer dealer, EDealerType dealerType, EWkfStatus status) {
        int numApplicationsTerm12 = 0;
        int numApplicationsTerm24 = 0;
        int numApplicationsTerm36 = 0;

        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addAssociation("dealer", "quodeal", JoinType.INNER_JOIN);
        restrictions.addCriterion(Restrictions.ge("quotationDate", DateFilterUtil.getStartDate(DateUtils.getDateAtBeginningOfMonth(selectDate))));
        restrictions.addCriterion(Restrictions.le("quotationDate", DateFilterUtil.getEndDate(selectDate)));
        if (dealer != null) {
            restrictions.addCriterion(Restrictions.eq("quodeal.id", dealer.getId()));
        }

        if (dealerType != null) {
            restrictions.addCriterion(Restrictions.eq("quodeal.dealerType", dealerType));
        }
        if (status != null && status.equals(QuotationWkfStatus.ACT)) {
            restrictions.addCriterion(Restrictions.eq(WKF_STATUS, QuotationWkfStatus.ACT));
            restrictions.addCriterion(Restrictions.ge("activationDate", DateFilterUtil.getStartDate(DateUtils.getDateAtBeginningOfMonth(selectDate))));
            restrictions.addCriterion(Restrictions.le("activationDate", DateFilterUtil.getEndDate(selectDate)));
        }

        List<Quotation> quotations = ENTITY_SRV.list(restrictions);
        if (quotations != null && !quotations.isEmpty()) {
            for (Quotation quotation : quotations) {
                if (quotation.getTerm() != null) {
                    if (quotation.getTerm() <= 12) {
                        numApplicationsTerm12++;
                    } else if (quotation.getTerm() <= 24) {
                        numApplicationsTerm24++;
                    } else if (quotation.getTerm() <= 36) {
                        numApplicationsTerm36++;
                    }
                }

            }
        }
        return new int[]{numApplicationsTerm12, numApplicationsTerm24, numApplicationsTerm36};
    }

    /**
     * Detail of accumulated application per advance payment and contract_old term for
     * @param selectDate
     * @return
     */
    private int[] getNumApplicationsByTerm(Date selectDate, Dealer dealer, EDealerType dealerType, EWkfStatus status) {
        int numApplicationsTerm12 = 0;
        int numApplicationsTerm24 = 0;
        int numApplicationsTerm36 = 0;
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addAssociation("dealer", "quodeal", JoinType.INNER_JOIN);
        restrictions.addCriterion(Restrictions.ge("quotationDate", DateFilterUtil.getStartDate(selectDate)));
        restrictions.addCriterion(Restrictions.le("quotationDate", DateFilterUtil.getEndDate(selectDate)));

        if (dealer != null) {
            restrictions.addCriterion(Restrictions.eq("quodeal.id", dealer.getId()));
        }
        if (dealerType != null) {
            restrictions.addCriterion(Restrictions.eq("quodeal.dealerType", dealerType));
        }
        if (status != null && status.equals(QuotationWkfStatus.ACT)) {
            restrictions.addCriterion(Restrictions.eq(WKF_STATUS, QuotationWkfStatus.ACT));
        }
        List<Quotation> quotations = ENTITY_SRV.list(restrictions);
        if (quotations != null && !quotations.isEmpty()) {
            for (Quotation quotation : quotations) {
                if (quotation.getTerm() != null) {
                    if (quotation.getTerm() <= 12) {
                        numApplicationsTerm12++;
                    } else if (quotation.getTerm() <= 24) {
                        numApplicationsTerm24++;
                    } else if (quotation.getTerm() <= 36) {
                        numApplicationsTerm36++;
                    }
                }

            }
        }
        return new int[]{numApplicationsTerm12, numApplicationsTerm24, numApplicationsTerm36};
    }

    /**
     * @param selectDate
     * @param dealerReports
     * @return
     */
    private long getAverageApplications(Date selectDate, List<DealerReport> dealerReports) {
        long averageApplications = 0;
        for (DealerReport dealerReport : dealerReports) {
            DailyReport selectDailyReport = dealerReport.getSelectDailyReport(selectDate);
            long nbApplications = selectDailyReport.getApply() == null ? 0 : selectDailyReport.getApply();
            averageApplications += nbApplications;
        }
        return roundInt((double) averageApplications / dealerReports.size());
    }

    /**
     * @param selectDate
     * @param dealerReports
     * @return
     */
    private long getBestPerformingApplications(Date selectDate, List<DealerReport> dealerReports) {
        long bestPerformingApplications = 0;
        for (DealerReport dealerReport : dealerReports) {
            DailyReport selectDailyReport = dealerReport.getSelectDailyReport(selectDate);
            long nbApplications = selectDailyReport.getApply() == null ? 0 : selectDailyReport.getApply();
            if (bestPerformingApplications < nbApplications) {
                bestPerformingApplications = nbApplications;
            }
        }
        return bestPerformingApplications;
    }

    /**
     * @param selectDate
     * @param dealerReports
     * @return
     */
    private long getWorsePerformingApplications(Date selectDate, List<DealerReport> dealerReports) {
        long worsePerformingApplications = 0;
        int index = 0;
        for (DealerReport dealerReport : dealerReports) {
            DailyReport selectDailyReport = dealerReport.getSelectDailyReport(selectDate);
            long nbApplications = selectDailyReport.getApply() == null ? 0 : selectDailyReport.getApply();
            if (index == 0 || worsePerformingApplications > nbApplications) {
                worsePerformingApplications = nbApplications;
            }
            index++;
        }
        return worsePerformingApplications;
    }

    /**
     * @param selectDate
     * @param dealerReports
     * @return
     */
    private long getAverageContracts(Date selectDate, List<DealerReport> dealerReports) {
        long averageContracts = 0;
        for (DealerReport dealerReport : dealerReports) {
            DailyReport selectDailyReport = dealerReport.getSelectDailyReport(selectDate);
            long nbContracts = selectDailyReport.getNewContract() == null ? 0 : selectDailyReport.getNewContract();
            averageContracts += nbContracts;
        }
        return roundInt((double) averageContracts / dealerReports.size());
    }

    /**
     * @param selectDate
     * @param dealerReports
     * @return
     */
    private long getBestPerformingContracts(Date selectDate, List<DealerReport> dealerReports) {
        long bestPerformingContracts = 0;
        for (DealerReport dealerReport : dealerReports) {
            DailyReport selectDailyReport = dealerReport.getSelectDailyReport(selectDate);
            long nbContracts = selectDailyReport.getNewContract() == null ? 0 : selectDailyReport.getNewContract();
            if (bestPerformingContracts < nbContracts) {
                bestPerformingContracts = nbContracts;
            }
        }
        return bestPerformingContracts;
    }

    /**
     * @param selectDate
     * @param dealerReports
     * @return
     */
    private long getWorsePerformingContracts(Date selectDate,
                                             List<DealerReport> dealerReports) {
        long worsePerformingContracts = 0;
        int index = 0;
        for (DealerReport dealerReport : dealerReports) {
            DailyReport selectDailyReport = dealerReport.getSelectDailyReport(selectDate);
            long nbContracts = selectDailyReport.getNewContract() == null ? 0 : selectDailyReport.getNewContract();
            if (index == 0 || worsePerformingContracts > nbContracts) {
                worsePerformingContracts = nbContracts;
            }
            index++;
        }
        return worsePerformingContracts;
    }

    /**
     * @param bestPerformingContracts
     * @param selectDate
     * @param dealerReports
     * @return
     */
    private String getBestPerformingContractsPOS(long bestPerformingContracts, Date selectDate, List<DealerReport> dealerReports) {
        String noBestPerformancePOS = "No dealer";
        String bestPerformancePOS = "";
        for (DealerReport dealerReport : dealerReports) {
            DailyReport selectDailyReport = dealerReport.getSelectDailyReport(selectDate);
            long nbContracts = selectDailyReport.getNewContract() == null ? 0 : selectDailyReport.getNewContract();
            if (bestPerformingContracts == nbContracts) {
                bestPerformancePOS = dealerReport.getDealer().getNameEn();
            }
        }
        return (bestPerformancePOS.length() == 0) ? noBestPerformancePOS : bestPerformancePOS.toString();
    }

    /**
     * @param worsePerformingContracts
     * @param selectDate
     * @param dealerReports
     * @return
     */
    private String getWorsePerformingContractsPOS(long worsePerformingContracts, Date selectDate,
                                                  List<DealerReport> dealerReports) {
        String noWorstPerformancePOS = "No dealer";
        String worstPerformancePOS = "";
        for (DealerReport dealerReport : dealerReports) {
            DailyReport selectDailyReport = dealerReport.getSelectDailyReport(selectDate);
            long nbContracts = selectDailyReport.getNewContract() == null ? 0 : selectDailyReport.getNewContract();
            if (worsePerformingContracts == nbContracts) {
                worstPerformancePOS = dealerReport.getDealer().getNameEn();
            }
        }
        return (worstPerformancePOS.length() == 0) ? noWorstPerformancePOS : worstPerformancePOS.toString();
    }

    /**
     * @param worsePerformingApplications
     * @param selectDate
     * @param dealerReports
     * @return
     */
    private String getWorsePerformingApplicationsPOS(long worsePerformingApplications, Date selectDate,
                                                     List<DealerReport> dealerReports) {
        String noWorstPerformancePOS = "No dealer";
        String worstPerformancePOS = "";
        for (DealerReport dealerReport : dealerReports) {
            DailyReport selectDailyReport = dealerReport.getSelectDailyReport(selectDate);
            long nbApplications = selectDailyReport.getApply() == null ? 0 : selectDailyReport.getApply();
            if (worsePerformingApplications == nbApplications) {
                worstPerformancePOS = dealerReport.getDealer().getNameEn();
            }
        }
        return (worstPerformancePOS.length() == 0) ? noWorstPerformancePOS : worstPerformancePOS.toString();
    }

    /**
     * @param bestPerformingApplications
     * @param selectDate
     * @param dealerReports
     * @return
     */
    private String getBestPerformingApplicationsPOS(long bestPerformingApplications, Date selectDate, List<DealerReport> dealerReports) {
        String noBestPerformancePOS = "No dealer";
        String bestPerformancePOS = "";
        for (DealerReport dealerReport : dealerReports) {
            DailyReport selectDailyReport = dealerReport.getSelectDailyReport(selectDate);
            long nbApplications = selectDailyReport.getApply() == null ? 0 : selectDailyReport.getApply();
            if (bestPerformingApplications == nbApplications) {
                bestPerformancePOS = dealerReport.getDealer().getNameEn();
            }
        }
        return (bestPerformancePOS.length() == 0) ? noBestPerformancePOS : bestPerformancePOS.toString();
    }

    /**
     * @param ref
     * @return
     */
    private int roundInt(double ref) {
        return (int) MyMathUtils.roundTo(ref, 0);
    }

    /**
     * @param ref
     * @return
     */
    private long roundLong(double ref) {
        return (long) MyMathUtils.roundTo(ref, 0);
    }

    /**
     * @param ref
     * @return
     */
    private double roundDouble(double ref) {
        return (double) MyMathUtils.roundTo(ref, 0) / 100;
    }

    /**
     * @param row
     * @param iCol
     * @param value
     * @param fontsize
     * @param isBold
     * @param hasBorder
     * @param leftRight
     * @param alignment
     * @param setBgColor
     * @param bgColor
     * @param fonCorlor
     * @param wrapText
     * @return
     */
    protected Cell createCell(final Row row, final int iCol,
                              final Object value, final int fontsize, final boolean isBold,
                              final boolean hasBorder, final boolean leftRight,
                              final short alignment, final boolean setBgColor,
                              final short bgColor, final short fonCorlor, boolean wrapText) {

        final Cell cell = row.createCell(iCol);
        final Font itemFont = wb.createFont();
        itemFont.setFontHeightInPoints((short) fontsize);
        itemFont.setFontName("Arial");
        itemFont.setColor(fonCorlor);

        CellStyle style = wb.createCellStyle();
        DataFormat format = wb.createDataFormat();
        style.setAlignment(alignment);
        style.setFont(itemFont);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

        cell.setCellStyle(styles.get(HEADER));

        if (value == null) {
            cell.setCellValue("");
        } else if (value instanceof Integer) {
            style.setDataFormat(format.getFormat("0"));
            cell.setCellValue(Integer.valueOf(value.toString()));
            cell.setCellStyle(style);
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
        } else if (value instanceof Long) {
            style.setDataFormat(format.getFormat("0"));
            cell.setCellValue(Long.valueOf(value.toString()));
            cell.setCellStyle(style);
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
        } else if (value instanceof Double) {
            style.setDataFormat(format.getFormat("##0%"));
            cell.setCellValue((Double) value);
            cell.setCellStyle(style);
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
        } else if (value instanceof String) {
            cell.setCellValue(value.toString());
        }

        if (hasBorder) {
            style.setBorderTop(CellStyle.BORDER_THIN);
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setBorderBottom(CellStyle.BORDER_THIN);
            style.setBottomBorderColor(FC_BLACK);
        }
        if (isBold) {
            style.setBorderRight(CellStyle.BORDER_THIN);
        }
        if (leftRight) {
            style.setBorderTop(CellStyle.BORDER_THIN);
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setBorderRight(CellStyle.BORDER_THIN);
        }
        if (setBgColor) {
            style.setFillForegroundColor(bgColor);
            style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        }
        if (wrapText) {
            style.setWrapText(wrapText);
        }
        cell.setCellStyle(style);
        return cell;
    }

    /**
     * @param row
     * @param iCol
     * @param value
     * @return
     */
    private Cell createCellBorderBottomAndRight(final Row row, final int iCol, final Object value) {
        final Cell cell = row.createCell(iCol);

        if (value instanceof Integer) {
            cell.setCellValue(Integer.valueOf(value.toString()));
        } else {
            cell.setCellValue((value == null ? "" : value.toString()));
        }

        cell.setCellStyle(styles.get(HEADER));
        return cell;
    }

    /**
     * @param row
     * @param iCol
     * @param value
     * @param hasBorder
     * @param isTotalRow
     * @return
     */
    private Cell createNumericCell(final Row row, final int iCol, final Object value, String hasBorder, boolean isTotalRow) {
        final Cell cell = row.createCell(iCol);
        // convert from general to number in excel for all number (integer,
        // double, long)
        if (value == null) {
            cell.setCellValue("");
        } else if (value instanceof Integer) {
            cell.setCellValue(Integer.valueOf(value.toString()));
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
        } else if (value instanceof Long) {
            cell.setCellValue(Long.valueOf(value.toString()));
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
        } else if (value instanceof String) {
            cell.setCellValue(value.toString());
        }
        if (value instanceof Double) {
            cell.setCellValue((Double) value);
            cell.setCellStyle(styles.get(AMOUNT));
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
        } else {
            cell.setCellStyle(styles.get(BODY));
        }

        if (hasBorder.equals(LEFT_BORDER_ALIGN_LEFT)) {

            cell.setCellStyle(styles.get(LEFT_BORDER_ALIGN_LEFT));

        } else if (hasBorder.equals(RIGHT_BORDER) && !isTotalRow) {

            cell.setCellStyle(styles.get(RIGHT_BORDER));

        } else if (hasBorder.equals(RIGHT_BORDER) && isTotalRow) {

            cell.setCellStyle(styles.get(TOP_RIGHT_BOTTOM_BORDER_ALIGN_RIGHT));

        } else if (hasBorder.equals(TOP_LEFT_BOTTOM_BORDER_ALIGN_LEFT)) {

            cell.setCellStyle(styles.get(TOP_LEFT_BOTTOM_BORDER_ALIGN_LEFT));

        } else if (hasBorder.equals(NONE_BORDER_ALIGN_RIGHT_PERCENTAGE) && !isTotalRow) {

            cell.setCellStyle(styles.get(NONE_BORDER_ALIGN_RIGHT_PERCENTAGE));

        } else if (hasBorder.equals(NONE_BORDER_ALIGN_RIGHT_PERCENTAGE) && isTotalRow) {

            cell.setCellStyle(styles.get(TOP_BOTTOM_BORDER_ALIGN_RIGHT_PERCENTAGE));

        } else if (hasBorder.equals(NONE_BORDER_ALIGN_RIGHT) && !isTotalRow) {

            cell.setCellStyle(styles.get(NONE_BORDER_ALIGN_RIGHT));
        } else if (hasBorder.equals(NONE_BORDER_ALIGN_RIGHT) && isTotalRow) {

            cell.setCellStyle(styles.get(TOP_BOTTOM_BORDER_ALIGN_RIGHT));

        }

        return cell;
    }

    /**
     * @return
     */
    private Map<String, CellStyle> createStyles() {
        final Font itemFont = wb.createFont();
        itemFont.setFontHeightInPoints((short) 15);
        itemFont.setFontName("Arial");

        CellStyle style;
        style = wb.createCellStyle();
        style.setFont(itemFont);
        styles.put(BODY, style);

        DataFormat format = wb.createDataFormat();
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(itemFont);
        style.setDataFormat(format.getFormat("#,##0.00"));
        styles.put(AMOUNT, style);

        style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BORDER_DOUBLE);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setLocked(true);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(itemFont);
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBottomBorderColor(FC_BLACK);
        styles.put(HEADER, style);

        style = wb.createCellStyle();
        cellStyleBorder(style, itemFont, LEFT_BORDER_ALIGN_LEFT);
        cellStyleBorder(style, itemFont, RIGHT_BORDER);
        cellStyleBorder(style, itemFont, TOP_LEFT_BOTTOM_BORDER_ALIGN_LEFT);
        cellStyleBorder(style, itemFont, TOP_RIGHT_BOTTOM_BORDER_ALIGN_RIGHT);
        cellStyleBorder(style, itemFont, TOP_BOTTOM_BORDER_ALIGN_RIGHT_PERCENTAGE);
        cellStyleBorder(style, itemFont, TOP_BOTTOM_BORDER_ALIGN_RIGHT);
        cellStyleBorder(style, itemFont, NONE_BORDER_ALIGN_RIGHT_PERCENTAGE);
        cellStyleBorder(style, itemFont, NONE_BORDER_ALIGN_RIGHT);

        return styles;
    }

    /**
     * @param style
     * @param itemFont
     * @param hasBorder
     */
    private void cellStyleBorder(CellStyle style, Font itemFont, String hasBorder) {
        DataFormat format = wb.createDataFormat();
        style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BIG_SPOTS);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setLocked(true);
        style.setFont(itemFont);
        if (hasBorder.equals(LEFT_BORDER_ALIGN_LEFT)) {

            style.setAlignment(CellStyle.ALIGN_LEFT);
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setDataFormat(format.getFormat(DEFAULT_DATE_FORMAT));

        } else if (hasBorder.equals(RIGHT_BORDER)) {

            style.setAlignment(CellStyle.ALIGN_RIGHT);
            style.setBorderRight(CellStyle.BORDER_THIN);

        } else if (hasBorder.equals(TOP_LEFT_BOTTOM_BORDER_ALIGN_LEFT)) {

            style.setAlignment(CellStyle.ALIGN_LEFT);
            style.setBorderTop(CellStyle.BORDER_THIN);
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setBorderBottom(CellStyle.BORDER_THIN);
            style.setBottomBorderColor(FC_BLACK);

        } else if (hasBorder.equals(TOP_RIGHT_BOTTOM_BORDER_ALIGN_RIGHT)) {

            style.setAlignment(CellStyle.ALIGN_RIGHT);
            style.setBorderTop(CellStyle.BORDER_THIN);
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setBorderBottom(CellStyle.BORDER_THIN);
            style.setBottomBorderColor(FC_BLACK);

        } else if (hasBorder.endsWith(TOP_BOTTOM_BORDER_ALIGN_RIGHT_PERCENTAGE)) {

            style.setAlignment(CellStyle.ALIGN_RIGHT);
            style.setBorderTop(CellStyle.BORDER_THIN);
            style.setBorderBottom(CellStyle.BORDER_THIN);
            style.setBottomBorderColor(FC_BLACK);
            style.setDataFormat(format.getFormat("##0%"));

        } else if (hasBorder.equals(TOP_BOTTOM_BORDER_ALIGN_RIGHT)) {

            style.setAlignment(CellStyle.ALIGN_RIGHT);
            style.setBorderTop(CellStyle.BORDER_THIN);
            style.setBorderBottom(CellStyle.BORDER_THIN);
            style.setBottomBorderColor(FC_BLACK);
            style.setDataFormat(format.getFormat("0"));

        } else if (hasBorder.equals(NONE_BORDER_ALIGN_RIGHT_PERCENTAGE)) {

            style.setAlignment(CellStyle.ALIGN_RIGHT);
            style.setDataFormat(format.getFormat("##0%"));

        } else if (hasBorder.equals(NONE_BORDER_ALIGN_RIGHT)) {

            style.setAlignment(CellStyle.ALIGN_RIGHT);
            style.setDataFormat(format.getFormat("0"));
        }
        styles.put(hasBorder, style);
    }

    /**
     * @param selectDate
     * @param dealerReports
     * @return getDailyReportSelectDate
     */
    private long getNbAppraisalToday(Date selectDate, List<DealerReport> dealerReports) {
        long numberAppraisalToday = 0;
        for (DealerReport dealerReport : dealerReports) {
            numberAppraisalToday += MyNumberUtils.getLong(dealerReport.getDailyReportSelectDate(selectDate).getNbDealerAppraisal());
        }
        return numberAppraisalToday;
    }

    /**
     * @param selectDate
     * @param dealerReports
     * @return
     */
    private long getNbApplicationsToday(Date selectDate, List<DealerReport> dealerReports) {
        long nbNbApplicationsToday = 0;
        for (DealerReport dealerReport : dealerReports) {
            nbNbApplicationsToday += MyNumberUtils.getLong(dealerReport.getDailyReportSelectDate(selectDate).getApply());
        }
        return nbNbApplicationsToday;
    }

    /**
     * @param selectDate
     * @param dealerReports
     * @return
     */
    private long getNbContractsSelectDate(Date selectDate, List<DealerReport> dealerReports) {
        long nbContractsToday = 0;
        for (DealerReport dealerReport : dealerReports) {
            nbContractsToday += MyNumberUtils.getLong(dealerReport.getDailyReportSelectDate(selectDate).getNewContract());
        }
        return nbContractsToday;
    }

    /**
     * @param selectDate
     * @param dealerReports
     * @return
     */
    private long getNbContractsOfMonth(Date selectDate, List<DealerReport> dealerReports) {
        long nbContractsToday = 0;
        for (DealerReport dealerReport : dealerReports) {
            nbContractsToday += MyNumberUtils.getLong(dealerReport.getSelectDailyReport(selectDate).getNbDealerContractsFromBeginOfMonth());
        }
        return nbContractsToday;
    }

    /**
     * @see XLSAbstractReportExtractor#getCellStyle(java.lang.String)
     */
    protected CellStyle getCellStyle(final String styleName) {
        return styles.get(styleName);
    }

    /**
     * @author kimsuor.seang
     */
    private class DealerReport {

        private Dealer dealer;
        private int lowTarget;
        private int highTarget;
        private List<DailyReport> dailyReports;

        /**
         * @return the dealer
         */
        public Dealer getDealer() {
            return dealer;
        }

        /**
         * @param dealer the dealer to set
         */
        public void setDealer(Dealer dealer) {
            this.dealer = dealer;
        }

        /**
         * @return the lowTarget
         */
        public int getLowTarget() {
            return lowTarget;
        }

        /**
         * @param lowTarget the lowTarget to set
         */
        public void setLowTarget(int lowTarget) {
            this.lowTarget = lowTarget;
        }

        /**
         * @return the highTarget
         */
        public int getHighTarget() {
            return highTarget;
        }

        /**
         * @param highTarget the highTarget to set
         */
        public void setHighTarget(int highTarget) {
            this.highTarget = highTarget;
        }

        /**
         * @return the dailyReports
         */
        public List<DailyReport> getDailyReports() {
            return dailyReports;
        }

        /**
         * @param dailyReports the dailyReports to set
         */
        public void setDailyReports(List<DailyReport> dailyReports) {
            this.dailyReports = dailyReports;
        }

        /**
         * @return
         */
        public DailyReport getTotalDailyReport() {
            DailyReport totalDailyReport = new DailyReport();
            for (DailyReport dailyReport : dailyReports) {
                totalDailyReport = totalDailyReport.plus(dailyReport);
            }
            return totalDailyReport;
        }

        /**
         * @return
         */
        public DailyReport getSelectDailyReport(Date selectDate) {
            DailyReport selectDailyReport = getDefaultDailyReport();
            Date startDate = DateUtils.getDateAtBeginningOfMonth(selectDate);
            for (DailyReport dailyReport : dailyReports) {
				/*
				 * if (DateUtils.isAfterDay(dailyReport.getDate(), startDate) &&
				 * DateUtils.isBeforeDay(dailyReport.getDate(), selectDate)) {
				 * selectDailyReport.plus(dailyReport); }
				 */
                if (DateUtils.getDateWithoutTime(dailyReport.getDate()).compareTo(DateUtils.getDateWithoutTime(startDate)) >= 0
                        && DateUtils.getDateWithoutTime(dailyReport.getDate()).compareTo(DateUtils.getDateWithoutTime(selectDate)) <= 0) {
                    selectDailyReport.plus(dailyReport);
                }
            }
            return selectDailyReport;
        }

        /**
         * @param selectDate
         * @return
         */
        public DailyReport getDailyReportSelectDate(Date selectDate) {
            DailyReport selectDailyReport = getDefaultDailyReport();
            for (DailyReport dailyReport : dailyReports) {
                if (DateUtils.isSameDay(selectDate, dailyReport.getDate())) {
                    selectDailyReport = dailyReport;
                    break;
                }
            }
            return selectDailyReport;
        }
    }

    /**
     * @author kimsuor.seang
     */
    private class DailyReportComparator implements Comparator<DailyReport> {
        @Override
        public int compare(DailyReport o1, DailyReport o2) {

            if (o1.getDate() == null && o2.getDate() != null) {
                return -1;
            }
            if (o1.getDate() != null && o2.getDate() == null) {
                return 1;
            }
            if (o1.getDate() == null && o2.getDate() == null) {
                return 0;
            }
            return o1.getDate().compareTo(o2.getDate());
        }
    }
}
