package com.soma.mfinance.gui.ui.panel.report.outstanding;

import com.soma.mfinance.core.accounting.dto.RemainingBalanceDTO;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.shared.accounting.LeaseTransaction;
import com.soma.mfinance.core.shared.contract.ContractEntityField;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.frmk.helper.FrmkServicesHelper;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.soma.mfinance.core.helper.FinServicesHelper.ACCOUNTING_SRV;
import static com.soma.mfinance.core.helper.FinServicesHelper.DEA_SRV;

/**
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(RemainingBalanceReportPanel.NAME)
public class RemainingBalanceReportPanel extends AbstractTabPanel implements View, ContractEntityField, FrmkServicesHelper {

    private static final long serialVersionUID = 8019051395954081483L;

    public static final String NAME = "remaining.balance.report";

    private TabSheet tabSheet;

    private SimplePagedTable<RemainingBalanceDTO> pagedTable;
    private List<ColumnDefinition> columnDefinitions;
    private ERefDataComboBox<EDealerType> cbxDealerType;
    private DealerComboBox cbxDealer;
    private TextField txtContractReference;
    private AutoDateField dfEndDate;

    @SuppressWarnings("serial")
    @Override
    protected com.vaadin.ui.Component createForm() {
        setSizeFull();
        tabSheet = new TabSheet();

        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setSpacing(true);
        contentLayout.setMargin(true);

        VerticalLayout gridLayoutPanel = new VerticalLayout();
        VerticalLayout searchLayout = new VerticalLayout();
        HorizontalLayout buttonsLayout = new HorizontalLayout();
        Button btnSearch = new Button(I18N.message("search"));
        btnSearch.setIcon(new ThemeResource("../smt-default/icons/16/search.png"));
        btnSearch.addClickListener((ClickListener) event -> search());

        Button btnReset = new Button(I18N.message("reset"));
        btnReset.addClickListener((ClickListener) event -> reset());
        buttonsLayout.setSpacing(true);
        buttonsLayout.addComponent(btnSearch);
        buttonsLayout.addComponent(btnReset);

        final GridLayout gridLayout = new GridLayout(8, 3);
        gridLayout.setSpacing(true);
        cbxDealer = new DealerComboBox(null, DataReference.getInstance().getDealers(), I18N.message("all"));
        cbxDealer.setSelectedEntity(null);
        cbxDealer.setWidth("220px");

        cbxDealerType = new ERefDataComboBox<>(EDealerType.class);
        cbxDealerType.setImmediate(true);
        cbxDealerType.addValueChangeListener((ValueChangeListener) event -> {
            cbxDealer.setDealers(ENTITY_SRV.list(DEA_SRV.getRestrictionsDealerByDealerType(cbxDealerType.getSelectedEntity())));
            cbxDealer.setSelectedEntity(null);
        });

        txtContractReference = ComponentFactory.getTextField(false, 20, 150);

        dfEndDate = ComponentFactory.getAutoDateField("", false);
        dfEndDate.setValue(DateUtils.getDateAtEndOfMonth(DateUtils.todayH00M00S00()));

        int iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("contract.reference")), iCol++, 0);
        gridLayout.addComponent(txtContractReference, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("dealer.type")), iCol++, 0);
        gridLayout.addComponent(cbxDealerType, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("dealer")), iCol++, 0);
        gridLayout.addComponent(cbxDealer, iCol++, 0);

        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("enddate")), iCol++, 1);
        gridLayout.addComponent(dfEndDate, iCol++, 1);

        gridLayoutPanel.addComponent(gridLayout);

        searchLayout.setMargin(true);
        searchLayout.setSpacing(true);
        searchLayout.addComponent(gridLayoutPanel);
        searchLayout.addComponent(buttonsLayout);

        Panel searchPanel = new Panel();
        searchPanel.setCaption(I18N.message("search"));
        searchPanel.setContent(searchLayout);

        this.columnDefinitions = createColumnDefinitions();
        pagedTable = new SimplePagedTable<>(this.columnDefinitions);
        pagedTable.setFooterVisible(true);
        pagedTable.setColumnFooter("irr.rate", I18N.message("total"));

        contentLayout.addComponent(searchPanel);
        contentLayout.addComponent(pagedTable);
        contentLayout.addComponent(pagedTable.createControls());

        tabSheet.addTab(contentLayout, I18N.message("remaining.balance"));

        return tabSheet;
    }

    public void reset() {
        cbxDealerType.setSelectedEntity(null);
        cbxDealer.setSelectedEntity(null);
        txtContractReference.setValue("");
        dfEndDate.setValue(null);
    }

    private void search() {
        if (isValid()) {
            List<RemainingBalanceDTO> remainingBalanceDTOS = ACCOUNTING_SRV.getLeaseTransactionsRemainingBalance(cbxDealerType.getSelectedEntity(),
                    cbxDealer.getSelectedEntity(),
                    txtContractReference.getValue(),
                    dfEndDate.getValue());
            setIndexedContainer(remainingBalanceDTOS);
        }
    }

    @SuppressWarnings("unchecked")
    private void setIndexedContainer(List<RemainingBalanceDTO> remainingBalanceDTOS) {
        Indexed indexedContainer = pagedTable.getContainerDataSource();
        indexedContainer.removeAllItems();
        double totalPrincipalBalance = 0d;
        double totalUnearnedInterestBalance = 0d;
        double totalBalance = 0d;

        for (RemainingBalanceDTO remainingBalanceDTO : remainingBalanceDTOS) {
            Item item = indexedContainer.addItem(remainingBalanceDTO.getId());
            item.getItemProperty(REFERENCE).setValue(remainingBalanceDTO.getReference());
            item.getItemProperty(START_DATE).setValue(remainingBalanceDTO.getContractStartDate());
            item.getItemProperty("firstInstallmentDate").setValue(remainingBalanceDTO.getFirstInstallmentDate());
            item.getItemProperty(LAST_NAME_EN).setValue(remainingBalanceDTO.getLastNameEn());
            item.getItemProperty(FIRST_NAME_EN).setValue(remainingBalanceDTO.getFirstNameEn());
            item.getItemProperty(INTEREST_RATE).setValue(remainingBalanceDTO.getInterestRate());
            item.getItemProperty("irr.rate").setValue(remainingBalanceDTO.getIrrRate());
            item.getItemProperty("principal.balance").setValue(AmountUtils.format(remainingBalanceDTO.getPrincipalBalance()));
            item.getItemProperty("unearned.interest.balance").setValue(AmountUtils.format(remainingBalanceDTO.getUnearnedInterestBalance()));
            item.getItemProperty("total.balance").setValue(AmountUtils.format(remainingBalanceDTO.getPrincipalBalance() + remainingBalanceDTO.getUnearnedInterestBalance()));

            totalPrincipalBalance += MyNumberUtils.getDouble(Math.abs(remainingBalanceDTO.getPrincipalBalance()));
            totalUnearnedInterestBalance += MyNumberUtils.getDouble(Math.abs(remainingBalanceDTO.getUnearnedInterestBalance()));
            totalBalance += MyNumberUtils.getDouble(Math.abs(totalPrincipalBalance + totalUnearnedInterestBalance));

        }

        pagedTable.setColumnFooter("principal.balance", AmountUtils.format(totalPrincipalBalance));
        pagedTable.setColumnFooter("unearned.interest.balance", AmountUtils.format(totalUnearnedInterestBalance));
        pagedTable.setColumnFooter("total.balance", AmountUtils.format(totalBalance));
        pagedTable.refreshContainerDataSource();
    }

    protected List<ColumnDefinition> createColumnDefinitions() {
        columnDefinitions = new ArrayList<>();
        columnDefinitions.add(new ColumnDefinition(REFERENCE, I18N.message("contract.reference"), String.class, Align.LEFT, 140));
        columnDefinitions.add(new ColumnDefinition(START_DATE, I18N.message("contract.start.date"), Date.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("firstInstallmentDate", I18N.message("first.payment.date"), Date.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition(LAST_NAME_EN, I18N.message("lastname.en"), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition(FIRST_NAME_EN, I18N.message("firstname.en"), String.class, Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition(INTEREST_RATE, I18N.message("interest.rate"), Double.class, Align.LEFT, 60));
        columnDefinitions.add(new ColumnDefinition("irr.rate", I18N.message("irr.rate"), Double.class, Align.LEFT, 60));
        columnDefinitions.add(new ColumnDefinition("principal.balance", I18N.message("principal.balance"), String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("unearned.interest.balance", I18N.message("balance.unearned.income"), String.class, Align.RIGHT, 100));
        columnDefinitions.add(new ColumnDefinition("total.balance", I18N.message("total.balance"), String.class, Align.RIGHT, 100));
        return columnDefinitions;
    }

    public boolean isValid() {
        removeErrorsPanel();
        checkMandatoryDateField(dfEndDate, "enddate");
        if (!errors.isEmpty()) {
            displayErrorsPanel();
        }
        return errors.isEmpty();
    }

    public void refresh() {
        search();
    }

    @Override
    public void enter(ViewChangeEvent event) {

    }
}
