package com.soma.mfinance.gui.ui.panel.insurance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.soma.mfinance.core.insurance.model.InsuranceStatus;
import com.soma.mfinance.core.insurance.model.QuotationInsuranceStatusHistory;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;

import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.soma.frmk.vaadin.ui.widget.toolbar.event.AddClickListener;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.Table.Align;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class InsuranceStatusHistoryPanel extends AbstractTabPanel implements AddClickListener{

	private static final long serialVersionUID = 465560322234999024L;
	
	private EntityRefComboBox<InsuranceStatus> cbxInsuranceStatus;
	private SimplePagedTable<QuotationInsuranceStatusHistory> tableHistory;
	private NavigationPanel navigationPanel;
	
	private Quotation quotation;
	
	@Override
	public void addButtonClick(ClickEvent arg0) {
		final Window window = new Window();
		window.setModal(true);
		window.setClosable(false);
		window.setResizable(false);
		window.setWidth(550, Unit.PIXELS);
		window.setHeight(300, Unit.PIXELS);
		window.setCaption(I18N.message("insurance.history"));
		
		cbxInsuranceStatus = new EntityRefComboBox<InsuranceStatus>(I18N.message("insurance.status"));
		cbxInsuranceStatus.setRestrictions(new BaseRestrictions<InsuranceStatus>(InsuranceStatus.class));
		cbxInsuranceStatus.renderer();
		cbxInsuranceStatus.setImmediate(true);
		
		Button btnSave = new NativeButton(I18N.message("save"));
		btnSave.setIcon(new ThemeResource("../smt-default/icons/16/save.png"));
		btnSave.addClickListener(new ClickListener() {

			private static final long serialVersionUID = -2674572347165741839L;

			@Override
			public void buttonClick(ClickEvent event) {
				if (quotation == null) {
					return;
				}
				QuotationInsuranceStatusHistory history = new QuotationInsuranceStatusHistory();
				history.setQuotation(quotation);
				
				if(cbxInsuranceStatus.getSelectedEntity() != null){
					history.setInsuranceStatus(cbxInsuranceStatus.getSelectedEntity());
					quotation.setInsuranceStatus(cbxInsuranceStatus.getSelectedEntity());
				}
				
				ENTITY_SRV.saveOrUpdate(history);
				quotation.getQuotationInsuranceStatusHistory().add(history);
				ENTITY_SRV.saveOrUpdate(quotation);
				setIndexedContainer();
				window.close();
				
			}
		
		});
		Button btnCancel = new NativeButton(I18N.message("cancel"));
		btnCancel.setIcon(new ThemeResource("../smt-default/icons/16/delete.png"));
		btnCancel.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 8902578830364522457L;
			@Override
			public void buttonClick(ClickEvent event) {
				window.close();
			}
		});
		NavigationPanel navigationPanel = new NavigationPanel();
		navigationPanel.addButton(btnSave);
		navigationPanel.addButton(btnCancel);
		
		FormLayout formLayout = new FormLayout();
		formLayout.setMargin(true);
		formLayout.addComponent(cbxInsuranceStatus);
		
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setSpacing(true);
		
		verticalLayout.addComponent(navigationPanel);
		verticalLayout.addComponent(formLayout);
		
		window.setContent(verticalLayout);
		UI.getCurrent().addWindow(window);
	}

	@Override
	protected Component createForm() {
		navigationPanel = new NavigationPanel();
		navigationPanel.addAddClickListener(this);
		
		tableHistory = new SimplePagedTable<QuotationInsuranceStatusHistory>(createColumnDefinitions());
		
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setSpacing(true);
		verticalLayout.addComponent(navigationPanel);
		verticalLayout.addComponent(tableHistory);
		verticalLayout.addComponent(tableHistory.createControls());
		
		return verticalLayout;
	}
	
	/**
	 * ColumnDefinitions
	 * @return
	 */
	private List<ColumnDefinition> createColumnDefinitions() {
		List<ColumnDefinition> columnDefinitions = new ArrayList<ColumnDefinition>();
		
		columnDefinitions.add(new ColumnDefinition("date", I18N.message("date"), Date.class, Align.LEFT, 80));
		columnDefinitions.add(new ColumnDefinition("insurance.status", I18N.message("insurance.status"), String.class, Align.LEFT, 200));
		columnDefinitions.add(new ColumnDefinition("updateUser", I18N.message("update.user"), String.class, Align.LEFT, 200));
		
		return columnDefinitions;
	}

	@SuppressWarnings("unchecked")
	private void setIndexedContainer() {
		Indexed indexedContainer = tableHistory.getContainerDataSource();
		indexedContainer.removeAllItems();
		if (quotation.getQuotationInsuranceStatusHistory() == null || quotation.getQuotationInsuranceStatusHistory().isEmpty()) {
			return;
		}
		
		Collections.sort(quotation.getQuotationInsuranceStatusHistory(),	new Comparator<QuotationInsuranceStatusHistory>() {

			private static final int REVERSE = -1;
			/**
			 * @see Comparator#compare(Object, Object)
			 */
			@Override
			public int compare(QuotationInsuranceStatusHistory o1, QuotationInsuranceStatusHistory o2) {
				if (o1 == null || o2 == null) {
					return 0;
				}
				return o1.getCreateDate().compareTo(o2.getCreateDate()) * REVERSE;
			}
			
		});
		
		for (QuotationInsuranceStatusHistory history : quotation.getQuotationInsuranceStatusHistory()) {
			Item item = indexedContainer.addItem(history.getId());
			item.getItemProperty("date").setValue(history.getCreateDate());
			item.getItemProperty("insurance.status").setValue(history.getInsuranceStatus() != null ? history.getInsuranceStatus().getDescEn() : "");
			item.getItemProperty("updateUser").setValue(history.getUpdateUser());

		}
		tableHistory.refreshContainerDataSource();
	}
	/**
	 * @param quotation
	 */
	public void assignValues (Quotation quotation) {
		this.quotation = quotation;
		setIndexedContainer();
	}

}
