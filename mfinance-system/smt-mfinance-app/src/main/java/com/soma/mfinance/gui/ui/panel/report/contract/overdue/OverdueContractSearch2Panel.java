package com.soma.mfinance.gui.ui.panel.report.contract.overdue;

import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.collection.model.EColResult;
import com.soma.mfinance.core.collection.model.MCollection;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.payment.model.EPaymentMethod;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.shared.system.FMProfile;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.mfinance.core.widget.SecUserComboBox;
import com.soma.ersys.core.hr.model.address.District;
import com.soma.ersys.core.hr.model.address.Province;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.*;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;

import java.util.List;

/**
 * Search Panel for Collection Officer
 *
 * @author kimsuor.seang
 * @author s.torn
 */
public class OverdueContractSearch2Panel extends AbstractSearchPanel<Collection> implements MCollection {

    private EntityRefComboBox<Province> cbxProvince;
    private EntityRefComboBox<District> cbxDistrict;
    private ERefDataComboBox<EDealerType> cbxDealerType;
    private DealerComboBox cbxDealer;
    private SecUserComboBox cbxCreditOfficer;
    private TextField txtContractReference;
    private EntityRefComboBox<EColResult> cbxCollectionStatus;
    private EntityRefComboBox<EPaymentMethod> cbxPaymentMethod;
    private TextField txtFamilyNameEn;
    private TextField txtFirstNameEn;
    private ValueChangeListener valueChangeListener;

    /**
     * @param tablePanel
     */
    public OverdueContractSearch2Panel(OverdueContractTablePanel tablePanel) {
        super(I18N.message("search"), tablePanel);
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#createForm()
     */
    @Override
    protected Component createForm() {

        cbxDealer = new DealerComboBox(ENTITY_SRV.list(getDealerRestriction()));
        cbxDealer.setSelectedEntity(null);
        cbxDealer.setWidth("220px");
        List<EDealerType> dealerTypes = EDealerType.values();
        cbxDealerType = new ERefDataComboBox<EDealerType>(dealerTypes);
        cbxDealerType.setImmediate(true);
        cbxDealerType.setWidth("220px");
        valueChangeListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                BaseRestrictions<Dealer> restrictions = getDealerRestriction();
                if (cbxDealerType.getSelectedEntity() != null) {
                    restrictions.addCriterion(Restrictions.eq(DEALER_TYPE, cbxDealerType.getSelectedEntity()));
                }
                cbxDealer.setDealers(ENTITY_SRV.list(restrictions));
                cbxDealer.setSelectedEntity(null);
            }
        };
        cbxDealerType.addValueChangeListener(valueChangeListener);

        cbxCreditOfficer = new SecUserComboBox(DataReference.getInstance().getUsers(FMProfile.CO, EStatusRecord.ACTIV));
        txtContractReference = new TextField();
        txtContractReference.setWidth("150");
        txtFamilyNameEn = new TextField();
        txtFirstNameEn = new TextField();
        cbxCollectionStatus = new EntityRefComboBox<EColResult>();
        cbxCollectionStatus.setRestrictions(new BaseRestrictions<EColResult>(EColResult.class));
        cbxCollectionStatus.renderer();

        cbxPaymentMethod = new EntityRefComboBox<>();
        cbxPaymentMethod.setRestrictions(new BaseRestrictions<>(EPaymentMethod.class));
        cbxPaymentMethod.renderer();
        cbxPaymentMethod.setImmediate(true);
        cbxPaymentMethod.setWidth("150px");

        cbxProvince = new EntityRefComboBox<Province>();
        cbxProvince.setRestrictions(new BaseRestrictions<Province>(Province.class));
        cbxProvince.renderer();
        cbxProvince.setImmediate(true);
        cbxProvince.setWidth("150px");

        cbxDistrict = new EntityRefComboBox<District>();
        cbxDistrict.setRestrictions(new BaseRestrictions<District>(District.class));
        cbxDistrict.renderer();
        cbxDistrict.setImmediate(true);
        cbxDistrict.setWidth("150px");

        GridLayout gridLayout = new GridLayout(10, 2);
        gridLayout.setSpacing(true);
        gridLayout.setMargin(true);
        int col = 0;
        int row = 0;
        gridLayout.addComponent(new Label(I18N.message("dealer.type")), col++, row);
        gridLayout.addComponent(cbxDealerType, col++, row);
        gridLayout.addComponent(new Label(I18N.message("lastname.en")), col++, row);
        gridLayout.addComponent(txtFamilyNameEn, col++, row);
        gridLayout.addComponent(new Label(I18N.message("credit.officer")), col++, row);
        gridLayout.addComponent(cbxCreditOfficer, col++, row);
        gridLayout.addComponent(new Label(I18N.message("contract.reference")), col++, row);
        gridLayout.addComponent(txtContractReference, col++, row);
        gridLayout.addComponent(new Label(I18N.message("province")), col++, row);
        gridLayout.addComponent(cbxProvince, col++, row);
        col = 0;
        row++;
        gridLayout.addComponent(new Label(I18N.message("dealer")), col++, row);
        gridLayout.addComponent(cbxDealer, col++, row);
        gridLayout.addComponent(new Label(I18N.message("firstname.en")), col++, row);
        gridLayout.addComponent(txtFirstNameEn, col++, row);
        gridLayout.addComponent(new Label(I18N.message("collection.status")), col++, row);
        gridLayout.addComponent(cbxCollectionStatus, col++, row);
        gridLayout.addComponent(new Label(I18N.message("payment.method")), col++, row);
        gridLayout.addComponent(cbxPaymentMethod, col++, row);
        gridLayout.addComponent(new Label(I18N.message("district")), col++, row);
        gridLayout.addComponent(cbxDistrict, col++, row);

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);
        horizontalLayout.addComponent(gridLayout);

        return horizontalLayout;
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#getRestrictions()
     */
    @Override
    public BaseRestrictions<Collection> getRestrictions() {

        BaseRestrictions<Collection> collectionBaseRestrictions = new BaseRestrictions<>(Collection.class);
        collectionBaseRestrictions.addAssociation("contract", "con", JoinType.INNER_JOIN);
        collectionBaseRestrictions.addCriterion(Restrictions.eq(COLLECTION_OFFICER_LOGIN,ProfileUtil.getCurrentUser()));


        if (cbxCreditOfficer.getSelectedEntity() != null) {
            collectionBaseRestrictions.addCriterion(Restrictions.eq(CREDIT_OFFICER, cbxCreditOfficer.getSelectedEntity()));
        }

        if (StringUtils.isNotEmpty(txtFamilyNameEn.getValue())) {
            collectionBaseRestrictions.addAssociation("con.applicant", "conapp", JoinType.INNER_JOIN);
            collectionBaseRestrictions.addAssociation("conapp.individual", "appidl", JoinType.INNER_JOIN);
            collectionBaseRestrictions.addCriterion(Restrictions.ilike("appidl." + LAST_NAME_EN, txtFamilyNameEn.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
            collectionBaseRestrictions.addAssociation("con.applicant", "conapp", JoinType.INNER_JOIN);
            collectionBaseRestrictions.addAssociation("conapp.individual", "appidl", JoinType.INNER_JOIN);
            collectionBaseRestrictions.addCriterion(Restrictions.ilike("appidl." + FIRST_NAME_EN, txtFirstNameEn.getValue(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotEmpty(txtContractReference.getValue())) {
            collectionBaseRestrictions.addCriterion(Restrictions.ilike(CONTRACT_REFERENCE, txtContractReference.getValue(), MatchMode.ANYWHERE));
        }

        if (cbxProvince.getSelectedEntity() != null) {
            collectionBaseRestrictions.addCriterion(Restrictions.eq(PROVINCE, cbxProvince.getSelectedEntity()));
        }

        if (cbxDealer.getSelectedEntity() != null) {
            collectionBaseRestrictions.addCriterion(Restrictions.eq(DEALER, cbxDealer.getSelectedEntity().getNameEn()));
        }

        if (cbxCollectionStatus.getSelectedEntity() != null) {
            collectionBaseRestrictions.addCriterion(Restrictions.eq(COLLECTION_STATUS, cbxCollectionStatus.getSelectedEntity()));
        }
        if (cbxPaymentMethod.getSelectedEntity() != null) {
            collectionBaseRestrictions.addCriterion(Restrictions.eq(LAST_PAID_PAYMENT_MEHTOD, cbxPaymentMethod.getSelectedEntity()));
        }

        if (cbxDistrict.getSelectedEntity() != null) {
            collectionBaseRestrictions.addCriterion(Restrictions.eq(DISTRICT, cbxDistrict.getSelectedEntity()));
        }





        return collectionBaseRestrictions;
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#reset()
     */
    @Override
    protected void reset() {
        cbxDealer.setSelectedEntity(null);
        cbxDealerType.setSelectedEntity(null);
        cbxCreditOfficer.setSelectedEntity(null);
        txtContractReference.setValue("");
        cbxCollectionStatus.setSelectedEntity(null);
        cbxPaymentMethod.setSelectedEntity(null);
        txtFamilyNameEn.setValue("");
        txtFirstNameEn.setValue("");
        cbxProvince.setSelectedEntity(null);
        cbxDistrict.setSelectedEntity(null);
    }

    /**
     * Get all Dealer except DealerType = OTH
     *
     * @return List of Dealers
     */
    private BaseRestrictions<Dealer> getDealerRestriction() {
        BaseRestrictions<Dealer> restrictions = new BaseRestrictions<Dealer>(Dealer.class);
        restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
        return restrictions;
    }
}
