package com.soma.mfinance.gui.ui.panel.report.lessee.info;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.entityfield.InstallmentEntityField;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.mfinance.glf.accounting.service.GLFLeasingAccountingService;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/**
 * @author p.ing
 */
public class LesseeInformationSearchPanel extends VerticalLayout implements FinServicesHelper, InstallmentEntityField {

    /**
     *
     */
    private static final long serialVersionUID = 5507245349235207480L;
    private TextField txtFirstNameEn;
    private TextField txtLastNameEn;
    private DealerComboBox cbxDealer;
    private TextField txtContractReference;
    private AutoDateField dfStartDate;
    private AutoDateField dfEndDate;
    private AutoDateField dfCalculDate;

    @Autowired
    private GLFLeasingAccountingService gLFLeasingAccountingService = SpringUtils.getBean(GLFLeasingAccountingService.class);

    public LesseeInformationSearchPanel() {
    }

    public GridLayout getSearchForm() {
        final GridLayout gridLayout = new GridLayout(12, 3);
        gridLayout.setSpacing(true);
        cbxDealer = new DealerComboBox(null, ENTITY_SRV.list(Dealer.class), I18N.message("all"));
        cbxDealer.setSelectedEntity(null);
        cbxDealer.setWidth("220px");

        txtContractReference = ComponentFactory.getTextField(false, 20, 150);
        txtFirstNameEn = ComponentFactory.getTextField(false, 60, 150);
        txtLastNameEn = ComponentFactory.getTextField(false, 60, 150);

        dfStartDate = ComponentFactory.getAutoDateField("", false);
        dfStartDate.setValue(DateUtils.today());
        dfEndDate = ComponentFactory.getAutoDateField("", false);
        dfEndDate.setValue(DateUtils.today());
        dfCalculDate = ComponentFactory.getAutoDateField("", false);
        dfCalculDate.setValue(DateUtils.today());
        int iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("contract.reference")), iCol++, 0);
        gridLayout.addComponent(txtContractReference, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("lastname.en")), iCol++, 0);
        gridLayout.addComponent(txtLastNameEn, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("firstname.en")), iCol++, 0);
        gridLayout.addComponent(txtFirstNameEn, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("calcul.date")), iCol++, 0);
        gridLayout.addComponent(dfCalculDate, iCol++, 0);


        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("dealer")), iCol++, 1);
        gridLayout.addComponent(cbxDealer, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("start.date")), iCol++, 1);
        gridLayout.addComponent(dfStartDate, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("end.date")), iCol++, 1);
        gridLayout.addComponent(dfEndDate, iCol++, 1);
        return gridLayout;
    }

    /**
     * @return list contracts
     */
    public List<Contract> getLeaseInformation() {
        BaseRestrictions<Contract> restrictions = new BaseRestrictions<>(Contract.class);

        if (StringUtils.isNotEmpty(txtContractReference.getValue())) {
            restrictions.addCriterion(Restrictions.like(REFERENCE, txtContractReference.getValue(), MatchMode.ANYWHERE));
        }
        if (dfStartDate.getValue() != null) {
            restrictions.addCriterion(Restrictions.ge(START_DATE, DateUtils.getDateAtBeginningOfDay(dfStartDate.getValue())));
        }
        if (dfEndDate.getValue() != null) {
            restrictions.addCriterion(Restrictions.le(START_DATE, DateUtils.getDateAtEndOfDay(dfEndDate.getValue())));
        }
        restrictions.addAssociation("dealer", "dea", JoinType.INNER_JOIN);
        if (cbxDealer.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, cbxDealer.getSelectedEntity().getId()));
        }
        if (StringUtils.isNotEmpty(txtLastNameEn.getValue()) || StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
            restrictions.addAssociation("contractApplicants", "contractapp", JoinType.INNER_JOIN);
            restrictions.addAssociation("contractapp.applicant", "app", JoinType.INNER_JOIN);
            restrictions.addCriterion("contractapp.applicantType", EApplicantType.C);
            restrictions.addAssociation("app.individual", "ind", JoinType.INNER_JOIN);
        }


        if (StringUtils.isNotEmpty(txtLastNameEn.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("ind." + LAST_NAME_EN, txtLastNameEn.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("ind." + FIRST_NAME_EN, txtFirstNameEn.getValue(), MatchMode.ANYWHERE));
        }

        return ENTITY_SRV.list(restrictions);
    }

    /**
     * @return the dfStartDate
     */
    public AutoDateField getDfStartDate() {
        return dfStartDate;
    }

    /**
     * @param dfStartDate the dfStartDate to set
     */
    public void setDfStartDate(AutoDateField dfStartDate) {
        this.dfStartDate = dfStartDate;
    }

    /**
     * @return the dfEndDate
     */
    public AutoDateField getDfEndDate() {
        return dfEndDate;
    }

    /**
     * @param dfEndDate the dfEndDate to set
     */
    public void setDfEndDate(AutoDateField dfEndDate) {
        this.dfEndDate = dfEndDate;
    }

}
