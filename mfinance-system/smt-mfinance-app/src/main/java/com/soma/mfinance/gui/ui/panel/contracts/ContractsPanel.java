package com.soma.mfinance.gui.ui.panel.contracts;

import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import org.apache.commons.lang.StringUtils;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * Contract panel
 * @author kimsuor.seang
 *
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(ContractsPanel.NAME)
public class ContractsPanel extends AbstractTabsheetPanel  implements View, FinServicesHelper {

	private static final long serialVersionUID = -470403725943364622L;

	public static final String NAME = "contracts";

	@Autowired
	private ContractsTablePanel contractTablePanel;
	@Autowired
	private ContractsFormPanel contractFormPanel;
	
	@PostConstruct
	public void PostConstruct() {
		super.init();
		contractTablePanel.setMainPanel(this);
		contractFormPanel.setCaption(I18N.message("contracts"));
		contractFormPanel.setContractsPanel(this);
		getTabSheet().setTablePanel(contractTablePanel);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		String cotraId = event.getParameters();
		if (StringUtils.isNotEmpty(cotraId)) {
			getTabSheet().addFormPanel(contractFormPanel);
			contractFormPanel.assignValues(new Long(cotraId));
			getTabSheet().setForceSelected(true);
			getTabSheet().setSelectedTab(contractFormPanel);
		}
	}
	
	@Override
	public void onAddEventClick() {
		contractFormPanel.reset();
		getTabSheet().addFormPanel(contractFormPanel);
		getTabSheet().setSelectedTab(contractFormPanel);
	}

	@Override
	public void onEditEventClick() {
		getTabSheet().addFormPanel(contractFormPanel);
		initSelectedTab(contractFormPanel);
	}
	
	@Override
	public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
		if (selectedTab == contractFormPanel) {
			contractFormPanel.assignValues(contractTablePanel.getItemSelectedId());
		}
//		else if (selectedTab == contractTablePanel && getTabSheet().isNeedRefresh()) {
//			contractTablePanel.refresh();
//		} else  if(selectedTab == contractTablePanel) {
//			contractTablePanel.refresh();
//		}
		getTabSheet().setSelectedTab(selectedTab);
	}

	public void displayContractTablePanel() {
		contractTablePanel.refresh();
		getTabSheet().setSelectedTab(contractTablePanel);
	}
}
