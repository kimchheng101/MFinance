package com.soma.mfinance.gui.ui.panel.report.contract.overdue;

import com.soma.mfinance.core.accounting.InstallmentVO;
import com.soma.mfinance.core.applicant.panel.ApplicantDetailPanel;
import com.soma.mfinance.core.applicant.panel.address.AddressesPanel;
import com.soma.mfinance.core.applicant.panel.guarantor.GuarantorPanel;
import com.soma.mfinance.core.asset.panel.AssetPanelOld;
import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.collection.panel.RepossessedPopupPanel;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.document.panel.DocumentsPanel;
import com.soma.mfinance.core.financial.panel.product.FinProductPanel;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.panel.comment.CommentsPanel;
import com.soma.mfinance.core.shared.financialproduct.FinancialProductEntityField;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.Notification.Type;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.Entity;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Form panel
 * @author kimsuor.seang
 * @author s.torn
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class OverdueContractFormPanel extends AbstractFormPanel implements FinServicesHelper, TabSheet.SelectedTabChangeListener {

    private NavigationPanel navigationPanel;
    private Button btnRepossessed;

    private Panel contractInfoPanel;
    private TabSheet accordionPanel;
    //private ApplicantPanel applicantPanel;
    private ApplicantDetailPanel applicantPanel;
    private GuarantorPanel guarantorPanel;
    private AssetPanelOld assetPanel;
    private CommentsPanel commentsPanel;
    private CollectionHistoryPanel collectionHistoryPanel;
    private FinProductPanel financialProductPanel;
    private ContractDocumentsPanel contractDocumentsPanel;
    private ChangeCollectionOfficerPanel changeCollectionOfficerPanel;
    private PaidOffPanel paidOffPanel;
    private AddressesPanel addressesPanel;
    private DocumentsPanel documentsPanel;
    private PaymentSchedulePanel paymentSchedulePanel;
    private UnpaidInstallmentsPanel unpaidInstallmentsPanel;
    //private Contract contract;
    private Collection collection;
    private Quotation quotation;
    private OverdueContractsPanel overdueContractsPanel;
    private Contract contract;

    /** */
    @PostConstruct
    public void PostConstruct() {
        super.init();
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractFormPanel#createForm()
     */
    @Override
    protected com.vaadin.ui.Component createForm() {

        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setSpacing(true);

        contractInfoPanel = new Panel();
        navigationPanel = new NavigationPanel();
        btnRepossessed = new NativeButton(I18N.message("repossess"));
        btnRepossessed.setIcon(new ThemeResource("../smt-default/icons/16/tick.png"));
        navigationPanel.addButton(btnRepossessed);

        btnRepossessed.addClickListener(event -> {
            UI.getCurrent().addWindow(new RepossessedPopupPanel(ContractWkfStatus.REP, contract,getOverdueContractsPanel(),getOverdueContractsPanel().getOverdueContractTablePanel()));
        });

        accordionPanel = new TabSheet();
        applicantPanel = new ApplicantDetailPanel();
        guarantorPanel = new GuarantorPanel();
        assetPanel = new AssetPanelOld();
        commentsPanel = new CommentsPanel();
        collectionHistoryPanel = new CollectionHistoryPanel();
        financialProductPanel = new FinProductPanel();
        contractDocumentsPanel = new ContractDocumentsPanel();
        changeCollectionOfficerPanel = new ChangeCollectionOfficerPanel();
        paidOffPanel = new PaidOffPanel();
        addressesPanel = new AddressesPanel();
        documentsPanel = new DocumentsPanel();
        paymentSchedulePanel = new PaymentSchedulePanel();
        unpaidInstallmentsPanel = new UnpaidInstallmentsPanel();

        accordionPanel.addTab(collectionHistoryPanel, I18N.message("collection.history"));
        accordionPanel.addTab(commentsPanel, I18N.message("comments"));
        if (ProfileUtil.isCollectionSupervisor()) {
            accordionPanel.addTab(changeCollectionOfficerPanel, I18N.message("change.officer"));
        }
        accordionPanel.addTab(paymentSchedulePanel, I18N.message("payment.schedule"));
        accordionPanel.addTab(unpaidInstallmentsPanel, I18N.message("unpaid.installments"));
        accordionPanel.addTab(applicantPanel, I18N.message("applicant"));
        accordionPanel.addTab(guarantorPanel, I18N.message("guarantor"));
        accordionPanel.addTab(assetPanel, I18N.message("asset"));
        accordionPanel.addTab(financialProductPanel, I18N.message("financial.product"));
        accordionPanel.addTab(documentsPanel, I18N.message("documents"));
        accordionPanel.addTab(contractDocumentsPanel, I18N.message("collection.documents"));
        accordionPanel.addTab(paidOffPanel, I18N.message("paid.off"));
        accordionPanel.addTab(addressesPanel, I18N.message("addresses"));

        if (ProfileUtil.isCollectionSupervisor()) {
            contentLayout.addComponent(navigationPanel);
        }

        contentLayout.addComponent(contractInfoPanel);
        accordionPanel.addSelectedTabChangeListener(this);
        contentLayout.addComponent(accordionPanel);


        return contentLayout;
    }

    /**
     * @param collection
     */
    private void displayContractInfo(final Collection collection) {
        String template = "contractInfo";
        InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
        CustomLayout inputFieldLayout = null;
        try {
            inputFieldLayout = new CustomLayout(layoutFile);
        } catch (IOException e) {
            Notification.show("Could not locate template " + template, e.getMessage(), Type.ERROR_MESSAGE);
        }

        inputFieldLayout.setSizeFull();
        inputFieldLayout.addComponent(new Label(I18N.message("applicant")), "lblApplicant");
        inputFieldLayout.addComponent(new Label("<b>" + collection.getContract().getApplicant().getIndividual().getLastNameEn() + " " +
                collection.getContract().getApplicant().getIndividual().getFirstNameEn() + "</b>", ContentMode.HTML), "lnkApplicant");
        inputFieldLayout.addComponent(new Label(I18N.message("reference")), "lblReference");
        inputFieldLayout.addComponent(new Label("<b>" + collection.getContract().getReference() + "</b>", ContentMode.HTML), "lblReferenceValue");
        inputFieldLayout.addComponent(new Label(I18N.message("startdate")), "lblStartDate");
        inputFieldLayout.addComponent(new Label("<b>" + DateUtils.date2StringDDMMYYYY_SLASH(collection.getContract().getStartDate()) + "</b>", ContentMode.HTML), "lblStartDateValue");
        inputFieldLayout.addComponent(new Label(I18N.message("enddate")), "lblEndDate");
        inputFieldLayout.addComponent(new Label("<b>" + DateUtils.date2StringDDMMYYYY_SLASH(collection.getContract().getEndDate()) + "</b>", ContentMode.HTML), "lblEndDateValue");
        inputFieldLayout.addComponent(new Label(I18N.message("penalty.rule")), "lblPenaltyRule");
        inputFieldLayout.addComponent(new Label(collection.getContract().getPenaltyRule() != null ? collection.getContract().getPenaltyRule().getDescEn() : ""), "txtPenaltyRule");

        inputFieldLayout.addComponent(new Label(I18N.message("guarantor")), "lblGuarantor");
        if (collection.getContract().getGuarantor() != null) {
            inputFieldLayout.addComponent(new Label("<b>" + collection.getContract().getGuarantor().getIndividual().getLastNameEn() + " " +
                    collection.getContract().getGuarantor().getIndividual().getFirstNameEn() + "</b>", ContentMode.HTML), "lnkGuarantor");
        } else {
            inputFieldLayout.addComponent(new Label("N/A"), "lnkGuarantor");
        }
        inputFieldLayout.addComponent(new Label(I18N.message("outstanding")), "lblOutstanding");
        //TODO : get outstanding amount
        inputFieldLayout.addComponent(new Label(I18N.message("status")), "lblStatus");
        inputFieldLayout.addComponent(new Label(collection.getContract().getWkfStatus().getDesc()), "txtStatus");


        List<InstallmentVO> nextInstallmentVOList = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(collection.getContract(), collection.getContract().getLastPaidNumInstallment() + 1);
        if (nextInstallmentVOList != null) {
            inputFieldLayout.addComponent(new Label(AmountUtils.format(nextInstallmentVOList.get(0).getBalance())), "txtOutstanding");
        }
        VerticalLayout vertical = new VerticalLayout();
        vertical.addComponent(inputFieldLayout);

        contractInfoPanel.setContent(vertical);
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractFormPanel#getEntity()
     */
    @Override
    protected Entity getEntity() {
        return null;
    }

    /**
     * @param id
     */
    public void assignValues(Long id, boolean isFirstLoad, OverdueContractsPanel overdueContractsPanel) {
        collection = ENTITY_SRV.getById(Collection.class, id);
        contract = collection.getContract();
        quotation = collection.getContract().getQuotation();
        guarantorPanel.setMainApplicant(quotation.getApplicant());
        //QuotationApplicant guarantor = quotation.getQuotationApplicant(EApplicantType.G);
        setOverdueContractsPanel(overdueContractsPanel);
        if (quotation.getFinancialProduct().getCode().equals(FinancialProductEntityField.FIN_CODE_PUBLIC)) {
            addGuarantorPanel();
            guarantorPanel.assignValues(quotation);
        } else {
            removeGuarantorPanel();
        }
        displayContractInfo(collection);

        if(isFirstLoad){
            accordionPanel.setSelectedTab(collectionHistoryPanel);
            collectionHistoryPanel.assignValues(collection);
        }
    }

    @Override
    public void selectedTabChange(TabSheet.SelectedTabChangeEvent event) {
        AbstractTabPanel selectedTab = (AbstractTabPanel) event.getTabSheet().getSelectedTab();

        if(isSelectedTab(selectedTab, collectionHistoryPanel)){
            collectionHistoryPanel.assignValues(collection);
        } else if(isSelectedTab(selectedTab, commentsPanel)){
            commentsPanel.assignValues(quotation);
        } else if(isSelectedTab(selectedTab, changeCollectionOfficerPanel)){
            changeCollectionOfficerPanel.assignValues(collection);
        } else if(isSelectedTab(selectedTab, paymentSchedulePanel)){
            paymentSchedulePanel.assignValues(quotation);
        } else if(isSelectedTab(selectedTab, unpaidInstallmentsPanel)){
            unpaidInstallmentsPanel.assignValues(collection.getContract());
        } else if(isSelectedTab(selectedTab, applicantPanel)){
            applicantPanel.assignValues(quotation);
            applicantPanel.getIdentityPanels().setEnabled(false);
            applicantPanel.getEmployeePanels().setEnabled(false);
            applicantPanel.getOtherInfoPanel().setEnabled(false);
            applicantPanel.getLoanTabPanel().setEnabled(false);
        } else if(isSelectedTab(selectedTab, assetPanel)){
            assetPanel.assignValues(collection.getContract());
        } else if(isSelectedTab(selectedTab, financialProductPanel)){
            financialProductPanel.assignValues(quotation);
        } else if(isSelectedTab(selectedTab, documentsPanel)){
            documentsPanel.setFromOverdueContract(true);
            documentsPanel.assignValues(quotation);
        } else if(isSelectedTab(selectedTab, contractDocumentsPanel)){
            contractDocumentsPanel.assignValues(collection.getContract());
        } else if(isSelectedTab(selectedTab, paidOffPanel)){
            paidOffPanel.assignValues(collection.getContract());
        } else if(isSelectedTab(selectedTab, addressesPanel)){
            addressesPanel.assignValues(collection.getContract().getApplicant().getIndividual().getIndividualAddresses());
        }
    }

    private boolean isSelectedTab(AbstractTabPanel selectedTab, AbstractTabPanel tab){
        return selectedTab == tab;
    }

    public void setOverdueContractsPanel(OverdueContractsPanel overdueContractsPanel){
        this.overdueContractsPanel=overdueContractsPanel;
    }

    public OverdueContractsPanel getOverdueContractsPanel(){
        return overdueContractsPanel;
    }

    public void addGuarantorPanel() {
        if (accordionPanel.getTab(guarantorPanel) == null) {
            guarantorPanel = new GuarantorPanel();
        }
        accordionPanel.addTab(guarantorPanel, I18N.message("guarantor"), null, 3);
    }

    public void removeGuarantorPanel() {
        accordionPanel.removeComponent(guarantorPanel);
    }
}
