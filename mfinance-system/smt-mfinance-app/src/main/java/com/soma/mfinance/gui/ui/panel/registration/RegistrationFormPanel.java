package com.soma.mfinance.gui.ui.panel.registration;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.service.ApplicantService;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.asset.panel.AssetRegistrationPanel;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.service.ContractService;
import com.soma.mfinance.core.document.panel.DocumentsPanel;
import com.soma.mfinance.core.quotation.QuotationService;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationSupportDecision;
import com.soma.mfinance.core.quotation.panel.comment.CommentsPanel;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.mfinance.core.shared.quotation.QuotationProfileUtils;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.mfinance.tools.report.service.ReportService;
import com.soma.frmk.security.context.SecApplicationContextHolder;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox.ButtonType;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.SelectedTabChangeListener;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.service.EntityService;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static com.soma.mfinance.core.helper.FinServicesHelper.INSTALLMENT_SERVICE_MFP;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RegistrationFormPanel extends AbstractFormPanel implements QuotationEntityField, ClickListener {

    /**
     *
     */
    private static final long serialVersionUID = -2658646633827914440L;

    private static final ThemeResource errorIcon = new ThemeResource("../smt-default/icons/16/close.png");
    private EntityService entityService = (EntityService) SecApplicationContextHolder.getContext().getBean("entityService");
    @Autowired
    private QuotationService quotationService;

    @Autowired
    private ReportService reportService;

    @Autowired
    protected ApplicantService applicantService;

    @Autowired
    private ContractService contractService;

    private RegistrationsPanel registrationPanel;

    private com.vaadin.ui.Component currentSelectedTab;

    private Quotation quotation;
    private TabSheet accordionPanel;

    private AssetRegistrationPanel assetRegistrationPanel;
    private DocumentsPanel documentsPanel;
    private RegistrationStatusHistoryPanel registrationStatusHistoryPanel;
    private RegistrationStorageLocationHistoryPanel registrationStorageLocationHistoryPanel;
    private CommentsPanel commentsPanel;
    private VerticalLayout navigationPanel;
    private NavigationPanel defaultNavigationPanel;
    private VerticalLayout informationPanel;

    private Panel contractPanel;

    private List<CheckBox> cbSupportDecisions;
    List<QuotationSupportDecision> quotationSupportDecisions;

    private SecUser secUser;

    public Button btnBackPos;
    public Button btnBackUw;
    public Button btnSubmit;
    public Button btnApprove;
    public Button btnReject;
    public Button btnDecline;
    public Button btnActivate;
    public Button btnCancel;
    public Button btnRequestFieldCheck;
    public Button btnChangeAsset;
    public Button btnRequestChangeGuarantor;
    public Button btnChangeGuarantor;
    public Button btnLockOrUnlock;
    public static MenuBar reportMenu;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        setCaption(I18N.message("registration"));
        setSizeFull();
        secUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        navigationPanel = new VerticalLayout();
        addComponent(navigationPanel, 0);
        defaultNavigationPanel = new NavigationPanel();
        defaultNavigationPanel.addSaveClickListener(this);
        navigationPanel.addComponent(defaultNavigationPanel);

        reportMenu = new MenuBar();
        reportMenu.setStyleName("menu-button");
        defaultNavigationPanel.addButton(reportMenu);

        if (ProfileUtil.isUnderwriter() || ProfileUtil.isUnderwriterSupervisor()) {
            defaultNavigationPanel.addButton(btnLockOrUnlock);
        }

        informationPanel = new VerticalLayout();
        cbSupportDecisions = new ArrayList<CheckBox>();
        addComponent(informationPanel, 1);

    }

    /**
     * Set registrations main panel
     *
     * @param registrationPanel
     */
    public void setRegistrationsPanel(RegistrationsPanel registrationPanel) {
        this.registrationPanel = registrationPanel;
    }


    /**
     * @see AbstractFormPanel#getEntity()
     */
    @Override
    protected Quotation getEntity() {
        updateQuotationTab(accordionPanel.getSelectedTab());
        return quotation;
    }

    /**
     * @see AbstractFormPanel#createForm()
     */
    @Override
    protected com.vaadin.ui.Component createForm() {

        contractPanel = new Panel();
        registrationStatusHistoryPanel = new RegistrationStatusHistoryPanel();
        registrationStorageLocationHistoryPanel = new RegistrationStorageLocationHistoryPanel();
        accordionPanel = new TabSheet();
        documentsPanel = new DocumentsPanel();
        commentsPanel = new CommentsPanel();
        assetRegistrationPanel = new AssetRegistrationPanel();

        accordionPanel.addTab(registrationStatusHistoryPanel, I18N.message("registration.status"));
        if (ProfileUtil.isDocumentController()) {
            accordionPanel.addTab(registrationStorageLocationHistoryPanel, I18N.message("registration.location"));
        }
        accordionPanel.addTab(assetRegistrationPanel, I18N.message("asset"));
        accordionPanel.addTab(documentsPanel, I18N.message("documents"));

        accordionPanel.addSelectedTabChangeListener(new SelectedTabChangeListener() {
            private static final long serialVersionUID = -2435529941310008060L;

            @Override
            public void selectedTabChange(SelectedTabChangeEvent event) {

                AbstractTabPanel selectedTab = (AbstractTabPanel) event.getTabSheet().getSelectedTab();
                updateQuotationTab(currentSelectedTab);
                navigationPanel.removeComponent(contractPanel);
                currentSelectedTab = selectedTab;
                if (selectedTab == assetRegistrationPanel) {
                    assetRegistrationPanel.assignValues(quotation);
                    if (ProfileUtil.isDocumentController()) {
                        defaultNavigationPanel.getSaveClickButton().setVisible(false);
                    }
                } else if (selectedTab == documentsPanel) {
                    documentsPanel.assignValues(quotation);
                    setVisibleSaveClickButtonForAdminProfile(true);
                } else if (selectedTab == commentsPanel) {
                    commentsPanel.assignValues(quotation);
                    setVisibleSaveClickButtonForAdminProfile(false);
                } else if (selectedTab == registrationStatusHistoryPanel) {
                    registrationStatusHistoryPanel.assignValues(quotation);
                } else if (selectedTab == registrationStorageLocationHistoryPanel) {
                    registrationStorageLocationHistoryPanel.assignValues(quotation);
                }

                selectedTab.updateNavigationPanel(navigationPanel, defaultNavigationPanel);

                if (quotation.getWkfStatus().equals(QuotationWkfStatus.ACT)) {
                    displayContractInfo(quotation.getContract());
                } else {
                    displayQuotationInfo(quotation.getMainApplicant());
                }
            }
        });
        return accordionPanel;
    }

    /**
     * @param isTrue
     */
    private void setVisibleSaveClickButtonForAdminProfile(boolean isTrue) {
        if (ProfileUtil.isAdmin()) {
            defaultNavigationPanel.getSaveClickButton().setVisible(isTrue);
        }
    }

    /**
     * Update quotation tab
     *
     * @param selectedTab
     */
    private void updateQuotationTab(com.vaadin.ui.Component selectedTab) {
        if (selectedTab != null) {
            super.reset();
            if (selectedTab == assetRegistrationPanel) {
                assetRegistrationPanel.getAsset();
            } else if (selectedTab == documentsPanel) {
                documentsPanel.getDocuments(quotation);
            }
        }
    }

    /**
     * @param quotaId
     * @param firstTabSelect
     */
    public void assignValues(Long quotaId, boolean firstTabSelect) {
        if (quotaId != null) {
            Quotation quotation = entityService.getById(Quotation.class, quotaId);
            assignValues(quotation, firstTabSelect);
        }
    }

    /**
     * @param quotation
     * @param firstTabSelect
     */
    public void assignValues(Quotation quotation, boolean firstTabSelect) {
        this.reset();
        cbSupportDecisions.clear();
        this.quotation = quotation;

        navigationPanel.removeComponent(contractPanel);
        informationPanel.removeAllComponents();
        registrationStatusHistoryPanel.assignValues(quotation);
        registrationStorageLocationHistoryPanel.assignValues(quotation);
        if (quotation.getAsset() == null) {
            quotation.setAsset(new Asset());
        }
        assetRegistrationPanel.assignValues(quotation);
        documentsPanel.assignValues(quotation);

        if (quotation.getId() != null) {
            if (accordionPanel.getTab(commentsPanel) == null) {
                accordionPanel.addTab(commentsPanel, I18N.message("comments"));
            }
        }

        displayQuotation();
        assignMenus();

        if (firstTabSelect) {
            accordionPanel.setSelectedTab(registrationStatusHistoryPanel);
        }
    }

    /**
     * @see AbstractFormPanel#saveEntity()
     */
    @Override
    public void saveEntity() {
        quotation = quotationService.saveOrUpdateQuotation(getEntity());
        quotationService.refresh(quotation);
        assignValues(quotation.getId(), false);
    }

    /**
     * Assign menus
     */
    public void assignMenus() {
        defaultNavigationPanel.getSaveClickButton().setVisible(QuotationProfileUtils.isSaveUpdateAvailable(quotation));
        reportMenu.setVisible(QuotationProfileUtils.isReportsAvailable(quotation));
    }

    /**
     * @see AbstractFormPanel#reset()
     */
    @Override
    public void reset() {
        super.reset();
        quotation = new Quotation();
        assetRegistrationPanel.reset();
        documentsPanel.reset();
    }

    /**
     * @see AbstractFormPanel#validate()
     */
    @Override
    protected boolean validate() {
        boolean valid = true;
        getEntity();
        if (!documentsPanel.isValid()) {
            valid = false;
            accordionPanel.setSelectedTab(documentsPanel);
        }
        return valid;
    }

    /**
     * @see ClickListener#buttonClick(ClickEvent)
     */
    @Override
    public void buttonClick(ClickEvent event) {

    }

    public void setLockOrUnlockProcess(Quotation quotation) {
        if (quotation.getSecUser() == null) {
            btnLockOrUnlock.setCaption(I18N.message("lock"));
            btnLockOrUnlock.setIcon(new ThemeResource("icons/16/locked.png"));
        } else {
            btnLockOrUnlock.setCaption(I18N.message("unlock"));
            btnLockOrUnlock.setIcon(new ThemeResource("icons/16/unlocked.png"));
        }
    }

    /**
     * @param quotation
     */

    public void showProcessMessage(Quotation quotation) {
        String underWriterName;
        if (ProfileUtil.isUnderwriter()) {
            underWriterName = quotation.getSecUser().getDesc() + " " + "UW";
        } else underWriterName = quotation.getSecUser().getDesc() + " " + "US";
        //set message box if other underwriter select quotation on the same time
        MessageBox mb = new MessageBox(UI.getCurrent(), "320px", "140px", I18N.message("application"),
                MessageBox.Icon.ERROR, underWriterName + " " + "is processing", Alignment.MIDDLE_RIGHT,
                new MessageBox.ButtonConfig(ButtonType.OK, I18N.message("ok")));
        mb.show();
    }

    public void setNullSecUser(Quotation quotation) {
        quotation.setSecUser(null);
        entityService.saveOrUpdate(quotation);
    }

    public Quotation getQuotation() {
        return quotation;
    }

    /**
     * Display quotation detail
     */
    public void displayQuotation() {
        if (quotation.getWkfStatus().equals(QuotationWkfStatus.ACT)) {
            displayContractInfo(quotation.getContract());
        } else {
            displayQuotationInfo(quotation.getMainApplicant());
        }
    }

    /**
     * return contract info
     * Panha Morn
     */
    private void displayContractInfo(final Contract contract) {

        contractPanel = new Panel();
        navigationPanel.setSpacing(true);
        navigationPanel.addComponent(contractPanel);

        String template = "contractInfo";
        InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
        CustomLayout inputFieldLayout = null;
        try {
            inputFieldLayout = new CustomLayout(layoutFile);
        } catch (IOException e) {
            Notification.show(I18N.message("message.error.locate.template") + template, e.getMessage(), Type.ERROR_MESSAGE);
        }

        inputFieldLayout.setSizeFull();
        inputFieldLayout.addComponent(new Label(I18N.message("applicant")), "lblApplicant");
        //inputFieldLayout.addComponent(btnApplicant, "lnkApplicant");
        inputFieldLayout.addComponent(new Label(contract.getApplicant().getLastNameEn() + " " + contract.getApplicant().getFirstNameEn()), "lnkApplicant");
        inputFieldLayout.addComponent(new Label(I18N.message("reference")), "lblReference");
        inputFieldLayout.addComponent(new Label("<b>" + contract.getReference() + "</b>", ContentMode.HTML), "lblReferenceValue");
        inputFieldLayout.addComponent(new Label(I18N.message("startdate")), "lblStartDate");
        inputFieldLayout.addComponent(new Label("<b>" + DateUtils.date2StringDDMMYYYY_SLASH(contract.getStartDate()) + "</b>", ContentMode.HTML), "lblStartDateValue");
        inputFieldLayout.addComponent(new Label(I18N.message("enddate")), "lblEndDate");
        inputFieldLayout.addComponent(new Label("<b>" + DateUtils.date2StringDDMMYYYY_SLASH(contract.getEndDate()) + "</b>", ContentMode.HTML), "lblEndDateValue");
        inputFieldLayout.addComponent(new Label(I18N.message("penalty.rule")), "lblPenaltyRule");
        inputFieldLayout.addComponent(new Label(contract.getPenaltyRule() != null ? contract.getPenaltyRule().getDescEn() : ""), "txtPenaltyRule");

        inputFieldLayout.addComponent(new Label(I18N.message("guarantor")), "lblGuarantor");
        if (contract.getGuarantor() != null) {
            inputFieldLayout.addComponent(new Label(contract.getQuotation().getGuarantor().getLastNameEn() + " " + contract.getQuotation().getGuarantor().getFirstNameEn()), "lnkGuarantor");
        } else {
            inputFieldLayout.addComponent(new Label("N/A"), "lnkGuarantor");
        }
        inputFieldLayout.addComponent(new Label(I18N.message("outstanding")), "lblOutstanding");
        inputFieldLayout.addComponent(new Label(I18N.message("status")), "lblStatus");
        inputFieldLayout.addComponent(new Label(contract.getWkfStatus().getDesc()), "txtStatus");
        inputFieldLayout.addComponent(new Label(I18N.message("quotation.ratio")), "lblRatio");
        double applicantRatio = quotationService.calculateApplicantRatio(quotation);
        inputFieldLayout.addComponent(new Label("<b> <font color=\""+ quotationService.getRatioColor(applicantRatio) + "\">" + AmountUtils.format(applicantRatio) + "</font></b>",ContentMode.HTML),"lblRatioValue");

        //Amount outstanding = CONT_SRV.getRealOutstanding(DateUtils.todayH00M00S00(), contract.getId());
        inputFieldLayout.addComponent(new Label(AmountUtils.format(INSTALLMENT_SERVICE_MFP.getOutStanding(quotation))), "txtOutstanding");

        VerticalLayout vertical = new VerticalLayout();
        vertical.addComponent(inputFieldLayout);
        contractPanel.setContent(vertical);
    }

    /**
     * return Quotation info
     * Panha Morn
     */
    private void displayQuotationInfo(final Applicant applicant) {
        if (!quotation.isValid()) return;

        Contract contract = applicant.getContract();
        contractPanel = new Panel();
        navigationPanel.setSpacing(true);
        navigationPanel.addComponent(contractPanel);

        String template = "contractInfo";
        InputStream layoutFile = getClass().getResourceAsStream("/VAADIN/themes/mfinance/layouts/" + template + ".html");
        CustomLayout inputFieldLayout = null;
        try {
            inputFieldLayout = new CustomLayout(layoutFile);
        } catch (IOException e) {
            Notification.show(I18N.message("message.error.locate.template") + template, e.getMessage(), Type.ERROR_MESSAGE);
        }

        inputFieldLayout.setSizeFull();
        inputFieldLayout.addComponent(new Label(I18N.message("applicant")), "lblApplicant");
        //inputFieldLayout.addComponent(btnApplicant, "lnkApplicant");
        inputFieldLayout.addComponent(new Label(contract.getApplicant().getLastNameEn() + " " + contract.getApplicant().getFirstNameEn()), "lnkApplicant");
        inputFieldLayout.addComponent(new Label(I18N.message("reference")), "lblReference");
        inputFieldLayout.addComponent(new Label("<b>" + contract.getReference() + "</b>", ContentMode.HTML), "lblReferenceValue");
        inputFieldLayout.addComponent(new Label(I18N.message("startdate")), "lblStartDate");
        inputFieldLayout.addComponent(new Label("<b>" + DateUtils.date2StringDDMMYYYY_SLASH(contract.getStartDate()) + "</b>", ContentMode.HTML), "lblStartDateValue");
        inputFieldLayout.addComponent(new Label(I18N.message("enddate")), "lblEndDate");
        inputFieldLayout.addComponent(new Label("<b>" + DateUtils.date2StringDDMMYYYY_SLASH(contract.getEndDate()) + "</b>", ContentMode.HTML), "lblEndDateValue");
        inputFieldLayout.addComponent(new Label(I18N.message("penalty.rule")), "lblPenaltyRule");
        inputFieldLayout.addComponent(new Label(contract.getPenaltyRule() != null ? contract.getPenaltyRule().getDescEn() : ""), "txtPenaltyRule");

        inputFieldLayout.addComponent(new Label(I18N.message("guarantor")), "lblGuarantor");
        if (contract.getGuarantor() != null) {
            inputFieldLayout.addComponent(new Label(quotation.getGuarantor() != null ? quotation.getGuarantor().getNameEn() : ""), "lnkGuarantor");
        } else {
            inputFieldLayout.addComponent(new Label("N/A"), "lnkGuarantor");
        }
        inputFieldLayout.addComponent(new Label(I18N.message("outstanding")), "lblOutstanding");
        inputFieldLayout.addComponent(new Label(I18N.message("status")), "lblStatus");
        inputFieldLayout.addComponent(new Label(quotation.getWkfStatus().getDesc()), "txtStatus");
        inputFieldLayout.addComponent(new Label(I18N.message("quotation.ratio")), "lblRatio");
        double applicantRatio = quotationService.calculateApplicantRatio(quotation);
        inputFieldLayout.addComponent(new Label("<b> <font color=\""+ quotationService.getRatioColor(applicantRatio) + "\">" + AmountUtils.format(applicantRatio) + "</font></b>",ContentMode.HTML),"lblRatioValue");

        //Amount outstanding = CONT_SRV.getRealOutstanding(DateUtils.todayH00M00S00(), contract.getId());
        inputFieldLayout.addComponent(new Label(AmountUtils.format(INSTALLMENT_SERVICE_MFP.getOutStanding(quotation))), "txtOutstanding");

        VerticalLayout vertical = new VerticalLayout();
        vertical.addComponent(inputFieldLayout);
        contractPanel.setContent(vertical);
    }

    /**
     * @return string color name
     */
    // TODO: Calc Ratio Color
    /*private String getRatioColor(){
        String color = "";
		Double applicantCoRatio = calApplicantRatioFromCo(); 	//calCoRatioValue();
		Configuration config = UWScoreConfig.getInstance().getConfiguration();

		double maxRedGrossIncomeRatio = config
				.getDouble("score.liability.gross_income_ratio[@maxred]");
		if (applicantCoRatio < maxRedGrossIncomeRatio) {
			 color="red";
		} else {
			double maxOrangeGrossIncomeRatio = config
					.getDouble("score.liability.gross_income_ratio[@maxorange]");
			if (applicantCoRatio < maxOrangeGrossIncomeRatio) {
				color="orange";
			} else {
				color="green";
			}
		}
		return color;
	}*/


    /**
     * @return Ration value of CO Review
     */
    // TODO: Calc Ratio
    /*private Double calApplicantRatioFromCo(){
		
		double monthlyInstallment = 0;
		Applicant customer = quotation.getMainApplicant();
		double totalNetIncome = 0d;
		double totalRevenus = 0d;
		double totalAllowance = 0d;
		double totalBusinessExpenses = 0d;
		double totalDebtInstallment = DoubleUtils.getDouble(customer
				.getTotalDebtInstallment());
		List<Employment> employments = customer.getEmployments();
		for (Employment employment : employments) {
			totalRevenus += DoubleUtils.getDouble(employment.getRevenue());
			totalAllowance += DoubleUtils.getDouble(employment.getAllowance());
			totalBusinessExpenses += DoubleUtils.getDouble(employment
					.getBusinessExpense());
		}
		totalNetIncome = totalRevenus + totalAllowance - totalBusinessExpenses;

		double totalExpenses = DoubleUtils.getDouble(customer
				.getMonthlyPersonalExpenses())
				+ DoubleUtils.getDouble(customer.getMonthlyFamilyExpenses())
				+ totalDebtInstallment;

		double disposableIncome = totalNetIncome - totalExpenses;
		monthlyInstallment = calTotalInstallmentAmount(quotation); //quotation.getTiInstallmentUsd();
		double customerRatio = disposableIncome / monthlyInstallment;
				
		return customerRatio;
	}*/
}
