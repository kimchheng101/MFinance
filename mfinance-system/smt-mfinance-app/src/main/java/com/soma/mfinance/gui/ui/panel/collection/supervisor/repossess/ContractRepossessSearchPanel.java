package com.soma.mfinance.gui.ui.panel.collection.supervisor.repossess;

import com.soma.mfinance.core.address.model.Area;
import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.collection.model.EColResult;
import com.soma.mfinance.core.collection.model.EColTask;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.payment.model.EPaymentMethod;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.shared.system.FMProfile;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.mfinance.core.widget.SecUserComboBox;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.component.NumberField;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.tools.DateUtils;

/**
 * Search panel for Collection Supervisor
 * @author sr.soth
 * @Date 25-Nov-2017
 */
public class ContractRepossessSearchPanel extends AbstractSearchPanel<Collection> implements FMEntityField {

	/** */
	private static final long serialVersionUID = -4527788882260581871L;

	private ERefDataComboBox<EDealerType> cbxDealerType;
	private DealerComboBox cbxDealer;
	private TextField txtContractReference;
	private EntityRefComboBox<EColResult> cbxCollectionStatus;
    private SecUserComboBox cbxCreditOfficer;
    private SecUserComboBox cbxCollectionOfficer;
	private ERefDataComboBox<EColTask> cbxCollectionTask;
	private EntityRefComboBox<Area> cbxCollectionArea;
	private EntityRefComboBox<EPaymentMethod> cbxPaymentMethod;
	private NumberField txtOverdueFrom;
	private NumberField txtOverdueTo;
	private AutoDateField dfStartDate;
	private AutoDateField dfEndDate;

	/**
	 * @param tablePanel
	 */
	public ContractRepossessSearchPanel(ContractRepossessTablePanel tablePanel) {
		super(I18N.message("search"), tablePanel);
	}

	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#createForm()
	 */
	@Override
	protected Component createForm() {
		cbxDealer = new DealerComboBox(I18N.message("dealer"), ENTITY_SRV.list(getDealerRestriction()), I18N.message("all"));
		cbxDealer.setSelectedEntity(null);
		cbxDealer.setWidth("220px");

		cbxDealerType = new ERefDataComboBox<>(I18N.message("dealer.type"), EDealerType.values());
		cbxDealerType.setImmediate(true);
		cbxDealerType.setWidth("220px");

		/*valueChangeListener = new ValueChangeListener() {
			*//** *//*
			private static final long serialVersionUID = -7442302732430560056L;
			@Override
			public void valueChange(ValueChangeEvent event) {
				BaseRestrictions<Dealer> restrictions = getDealerRestriction();
				if (cbxDealerType.getSelectedEntity() != null) {
					restrictions.addCriterion(Restrictions.eq("dealerType", cbxDealerType.getSelectedEntity()));
				}
				cbxDealer.setDealers(entityService.list(restrictions));
				cbxDealer.setSelectedEntity(null);
			}
		};
		cbxDealerType.addValueChangeListener(valueChangeListener);*/

		txtContractReference = ComponentFactory.getTextField("contract.reference", false, 20, 150);

		cbxCollectionStatus = new EntityRefComboBox<EColResult>(I18N.message("collection.status"));
		cbxCollectionStatus.setRestrictions(new BaseRestrictions<EColResult>(EColResult.class));
		cbxCollectionStatus.renderer();

        cbxCreditOfficer = new SecUserComboBox(DataReference.getInstance().getUsers(FMProfile.CO, EStatusRecord.ACTIV));
        cbxCreditOfficer.setCaption(I18N.message("credit.officer"));
        cbxCollectionOfficer = new SecUserComboBox(DataReference.getInstance().getUsers(FMProfile.CC, EStatusRecord.ACTIV));
        cbxCollectionOfficer.setCaption(I18N.message("collection.officer"));
		cbxCollectionTask = new ERefDataComboBox<EColTask>(I18N.message("collection.task"), EColTask.class);

		cbxCollectionArea = new EntityRefComboBox<Area>(I18N.message("areas"));
		cbxCollectionArea.setRestrictions(new BaseRestrictions<Area>(Area.class));
		cbxCollectionArea.renderer();

		dfStartDate = ComponentFactory.getAutoDateField(I18N.message("startdate"),false);
		dfStartDate.setValue(DateUtils.getDateAtBeginningOfMonth(DateUtils.todayH00M00S00()));
		dfEndDate = ComponentFactory.getAutoDateField(I18N.message("enddate"), false);
		dfEndDate.setValue(DateUtils.getDateAtEndOfMonth(DateUtils.todayH00M00S00()));

		cbxPaymentMethod = new EntityRefComboBox<EPaymentMethod>(I18N.message("payment.method"));
		cbxPaymentMethod.setRestrictions(new BaseRestrictions<EPaymentMethod>(EPaymentMethod.class));
		cbxPaymentMethod.renderer();
		cbxPaymentMethod.setImmediate(true);

		txtOverdueFrom = ComponentFactory.getNumberField("overdue.from", false, 100, 150);
		txtOverdueTo = ComponentFactory.getNumberField("overdue.to", false, 100, 150);

		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSpacing(true);

		FormLayout formLayout = new FormLayout();
		formLayout.addComponent(cbxDealerType);
		formLayout.addComponent(cbxDealer);
		formLayout.addComponent(txtOverdueFrom);
		horizontalLayout.addComponent(formLayout);

		formLayout = new FormLayout();
		formLayout.addComponent(cbxCreditOfficer);
		formLayout.addComponent(cbxCollectionOfficer);
		formLayout.addComponent(txtOverdueTo);
		formLayout.addComponent(dfStartDate);
		horizontalLayout.addComponent(formLayout);

		formLayout = new FormLayout();
		formLayout.addComponent(txtContractReference);
		formLayout.addComponent(cbxCollectionStatus);
		formLayout.addComponent(cbxPaymentMethod);
		formLayout.addComponent(dfEndDate);
		horizontalLayout.addComponent(formLayout);

		formLayout = new FormLayout();
		formLayout.addComponent(cbxCollectionTask);
		formLayout.addComponent(cbxCollectionArea);
		horizontalLayout.addComponent(formLayout);

		return horizontalLayout;
	}

	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#getRestrictions()
	 */
	@Override
	public BaseRestrictions<Collection> getRestrictions() {
		BaseRestrictions<Collection> restrictions = new BaseRestrictions<Collection>(Collection.class);
        restrictions.addAssociation("contract", "con", JoinType.INNER_JOIN);

		restrictions.addCriterion(Restrictions.eq("con."+WKF_STATUS, ContractWkfStatus.REP));
		restrictions.addAssociation("con.contractAuctionData", "conauda", JoinType.INNER_JOIN);
		if (cbxDealerType.getSelectedEntity() != null) {
			restrictions.addAssociation("dealer", "dea", JoinType.INNER_JOIN);
			restrictions.addCriterion(Restrictions.eq("dea.dealerType", cbxDealerType.getSelectedEntity()));
		}

		if (cbxDealer.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("dealer", cbxDealer.getSelectedEntity().getNameEn()));
		}


		if (StringUtils.isNotEmpty(txtContractReference.getValue())) {
			restrictions.addCriterion(Restrictions.ilike("con.reference", txtContractReference.getValue(), MatchMode.ANYWHERE));
		}

		if (StringUtils.isNotEmpty(txtOverdueFrom.getValue())) {
			restrictions.addCriterion(Restrictions.ge("nbOverdueInDays", Integer.valueOf(txtOverdueFrom.getValue())));
		}

		if (StringUtils.isNotEmpty(txtOverdueTo.getValue())) {
			restrictions.addCriterion(Restrictions.le("nbOverdueInDays", Integer.valueOf(txtOverdueTo.getValue())));
		}

		if (cbxCollectionStatus.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("colResult.id", cbxCollectionStatus.getSelectedEntity().getId()));
		}

        //TODO-When Collection has ColASsignment
        if (cbxCreditOfficer.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq("creditOfficer.id", cbxCreditOfficer.getSelectedEntity().getId()));
        }

        if (cbxCollectionOfficer.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq("collectionOfficer.id", cbxCollectionOfficer.getSelectedEntity().getId()));
        }

        if (cbxCollectionTask.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("colTask.id", cbxCollectionTask.getSelectedEntity().getId()));
		}

		if (cbxCollectionArea.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("area.id", cbxCollectionArea.getSelectedEntity().getId()));
		}

		if (cbxPaymentMethod.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("lastPaidPaymentMethod", cbxPaymentMethod.getSelectedEntity().getId()));
		}

		if (dfStartDate.getValue() != null) {
			restrictions.addCriterion(Restrictions.ge("conauda.requestRepossessedDate" , DateUtils.getDateAtBeginningOfDay(dfStartDate.getValue())));
		}
		if (dfEndDate.getValue() != null) {
			restrictions.addCriterion(Restrictions.le("conauda.requestRepossessedDate" , DateUtils.getDateAtEndOfDay(dfEndDate.getValue())));
		}

		restrictions.addOrder(Order.asc("con."+START_DATE));

		return restrictions;
	}

	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#reset()
	 */
	@Override
	protected void reset() {
		cbxDealerType.setSelectedEntity(null);
		cbxDealer.setSelectedEntity(null);
		cbxCreditOfficer.setSelectedEntity(null);
		cbxCollectionOfficer.setSelectedEntity(null);
		txtContractReference.setValue("");
		cbxCollectionArea.setSelectedEntity(null);
		cbxCollectionStatus.setSelectedEntity(null);
		cbxCollectionTask.setSelectedEntity(null);
		cbxPaymentMethod.setSelectedEntity(null);
		txtOverdueFrom.setValue("");
		txtOverdueTo.setValue("");
		dfStartDate.setValue(null);
		dfEndDate.setValue(null);
	}

	/**
	 * Get all Dealer except DealerType = OTH
	 * @return List of Dealers
	 */
	private BaseRestrictions<Dealer> getDealerRestriction () {
		BaseRestrictions<Dealer> restrictions = new BaseRestrictions<Dealer>(Dealer.class);
		restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
		restrictions.addCriterion(Restrictions.ne("dealerType", EDealerType.OTH));
		return restrictions;
	}

}
