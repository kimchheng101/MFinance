package com.soma.mfinance.gui.ui.panel.report.contract.overdue;

import com.soma.mfinance.core.collection.model.*;
import com.soma.mfinance.core.collection.model.Collection;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.component.NumberField;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.soma.frmk.vaadin.ui.widget.toolbar.event.AddClickListener;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Table.Align;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.formula.functions.Now;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.*;

/**
 * Collection history panel
 * @author s.torn
 */
public class CollectionHistoryPanel extends AbstractTabPanel implements AddClickListener {

    private EntityRefComboBox<EColResult> cbxCollecitonStatus;
    private EntityRefComboBox<ColCustField> cbxcustomerAttribute;
    private NumberField txtAmountPromiseToPay;
    private DateField dfStartPeriodPromiseToPay;
    private DateField dfEndPeriodPromiseToPay;
    private SimplePagedTable<ContractCollectionHistory> tableHistory;
    private NavigationPanel navigationPanel;
    private Collection collection;
    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTabPanel#createForm()
     */
    @Override
    protected Component createForm() {
        navigationPanel = new NavigationPanel();
        navigationPanel.addAddClickListener(this);

        tableHistory = new SimplePagedTable<ContractCollectionHistory>(createColumnDefinitions());

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSpacing(true);
        verticalLayout.addComponent(navigationPanel);
        verticalLayout.addComponent(tableHistory);
        verticalLayout.addComponent(tableHistory.createControls());
  /*      tableHistory.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(ItemClickEvent event) {
                boolean isDoubleClick = event.isDoubleClick() || SecApplicationContextHolder.getContext().clientDeviceIsMobileOrTablet();
                if (isDoubleClick) {
                    Long itemId = (Long) event.getItemId();
                    ContractCollectionHistory contractCollectionHistory = entityService.getById(ContractCollectionHistory.class, itemId);
                    CollectionPhotoHistoryFormPanel collectionHistoryPanel=new CollectionPhotoHistoryFormPanel();
                    collectionHistoryPanel.assignValue(contractCollectionHistory);
                    Window dialog = new Window();
                    dialog.setCaption(I18N.message("collection.history.photo"));
                    dialog.setWidth(740, Unit.PIXELS);
                    dialog.setHeight(440, Unit.PIXELS);
                    dialog.setModal(true);
                    dialog.setContent(contractCollectionHistory);
                    dialog.center();
                    UI.getCurrent().addWindow(dialog);

                }
            }
        });*/
        return verticalLayout;
    }

    /**
     * ColumnDefinitions
     *
     * @return
     */
    private List<ColumnDefinition> createColumnDefinitions() {
        List<ColumnDefinition> columnDefinitions = new ArrayList<ColumnDefinition>();

        columnDefinitions.add(new ColumnDefinition("date", I18N.message("date").toUpperCase(), Date.class, Align.LEFT, 80));
        columnDefinitions.add(new ColumnDefinition("num.overdue.in.days", I18N.message("num.overdue.in.days").toUpperCase(), Integer.class, Align.LEFT, 90));
        columnDefinitions.add(new ColumnDefinition("collection.status", I18N.message("collection.status").toUpperCase(), String.class, Align.LEFT, 200));
        columnDefinitions.add(new ColumnDefinition("customer.attribute", I18N.message("customer.attribute").toUpperCase(), String.class, Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition("action.shedule",I18N.message("action.shedule").toUpperCase(),String.class,Align.LEFT,150));
        columnDefinitions.add(new ColumnDefinition("collection.task", I18N.message("collection.task").toUpperCase(), String.class, Align.LEFT, 120));
        columnDefinitions.add(new ColumnDefinition("collection.officer", I18N.message("collection.officer").toUpperCase(), String.class, Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition("amount.promise.to.pay", I18N.message("amount.promise.to.pay").toUpperCase(), String.class, Align.RIGHT, 130));
        columnDefinitions.add(new ColumnDefinition("start.period.promise.to.pay", I18N.message("start.period.promise.to.pay").toUpperCase(), Date.class, Align.LEFT, 120));
        columnDefinitions.add(new ColumnDefinition("end.period.promise.to.pay", I18N.message("end.period.promise.to.pay").toUpperCase(), Date.class, Align.LEFT, 120));

        return columnDefinitions;
    }

    /** */
    @SuppressWarnings("unchecked")
    private void setIndexedContainer() {
        Indexed indexedContainer = tableHistory.getContainerDataSource();
        indexedContainer.removeAllItems();
        // TODO PYI
        if (collection.getContractCollectionHistories() == null || collection.getContractCollectionHistories().isEmpty()) {
            return;
        }

        Collections.sort(collection.getContractCollectionHistories(), new Comparator<ContractCollectionHistory>() {

            private static final int REVERSE = -1;

            /*
             * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
             */
            @Override
            public int compare(ContractCollectionHistory o1, ContractCollectionHistory o2) {
                if (o1 == null || o2 == null) {
                    return 0;
                }
                return o1.getCreateDate().compareTo(o2.getCreateDate()) * REVERSE;
            }

        });

        for (ContractCollectionHistory history : collection.getContractCollectionHistories()) {
            Item item = indexedContainer.addItem(history.getId());
            item.getItemProperty("date").setValue(history.getCreateDate());
            item.getItemProperty("num.overdue.in.days").setValue(history.getNbOverdueInDays());
            item.getItemProperty("collection.status").setValue(history.getColResult() != null ? history.getColResult().getDescEn() : "");
            item.getItemProperty("customer.attribute").setValue(history.getCustField() != null ? history.getCustField().getDescEn() : "");
            item.getItemProperty("collection.task").setValue(history.getTask() != null ? history.getTask().getDescEn() : "");
            item.getItemProperty("collection.officer").setValue(history.getAssignee() != null ? history.getAssignee().getDesc() : "");
            item.getItemProperty("amount.promise.to.pay").setValue(AmountUtils.format(history.getAmountPromiseToPayUsd()));
            item.getItemProperty("start.period.promise.to.pay").setValue(history.getStartPeriodPromiseToPay());
            item.getItemProperty("end.period.promise.to.pay").setValue(history.getEndPeriodPromiseToPay());
        }
        tableHistory.refreshContainerDataSource();
    }

    /**
     * @param collection
     */
    public void assignValues(Collection collection) {
        this.collection = collection;
        setIndexedContainer();
    }

    /**
     * @see com.soma.frmk.vaadin.ui.widget.toolbar.event.AddClickListener#addButtonClick(com.vaadin.ui.Button.ClickEvent)
     */
    @Override
    public void addButtonClick(ClickEvent paramClickEvent) {
        final Window window = new Window();
        window.setModal(true);
        window.setClosable(false);
        window.setResizable(false);
        window.setWidth(550, Unit.PIXELS);
        window.setHeight(300, Unit.PIXELS);
        window.setCaption(I18N.message("collection.history"));

        cbxCollecitonStatus = new EntityRefComboBox<EColResult>(I18N.message("collection.status"));
        cbxCollecitonStatus.setRestrictions(new BaseRestrictions<EColResult>(EColResult.class));
        cbxCollecitonStatus.renderer();
        cbxCollecitonStatus.setRequired(true);
        cbxcustomerAttribute = new EntityRefComboBox<ColCustField>(I18N.message("customer.attributes"));
        cbxcustomerAttribute.setRestrictions(new BaseRestrictions<ColCustField>(ColCustField.class));
        cbxcustomerAttribute.renderer();
        txtAmountPromiseToPay = ComponentFactory.getNumberField("amount.promise.to.pay", false, 50, 150);
        dfStartPeriodPromiseToPay = ComponentFactory.getAutoDateField("start.period.promise.to.pay", false);
        dfEndPeriodPromiseToPay = ComponentFactory.getAutoDateField("end.period.promise.to.pay", false);

        dfStartPeriodPromiseToPay.setValue(null);
        dfEndPeriodPromiseToPay.setValue(null);

        Button btnSave = new NativeButton(I18N.message("save"));
        btnSave.setIcon(new ThemeResource("../smt-default/icons/16/save.png"));
        btnSave.addClickListener(new ClickListener() {
            /** */
            private static final long serialVersionUID = 8902578830364522457L;

            @Override
            public void buttonClick(ClickEvent event) {
                // TODO PYI

                if (collection == null) {
                    return;
                }
                // set as foreign key to td_contract_collection_history   as col_id
                ContractCollectionHistory history = new ContractCollectionHistory();
                history.setCollection(collection);

                if (cbxcustomerAttribute.getSelectedEntity() != null) {
                    history.setCustField(cbxcustomerAttribute.getSelectedEntity());
                }
                if (cbxCollecitonStatus.getSelectedEntity() != null) {
                    history.setColResult(cbxCollecitonStatus.getSelectedEntity());
                }

                if (StringUtils.isNotEmpty(txtAmountPromiseToPay.getValue())) {
                    history.setAmountPromiseToPayUsd(Double.valueOf(txtAmountPromiseToPay.getValue()));
                }
                if (dfStartPeriodPromiseToPay.getValue() != null) {
                    history.setStartPeriodPromiseToPay(dfStartPeriodPromiseToPay.getValue());
                }
                if (dfEndPeriodPromiseToPay.getValue() != null) {
                    history.setEndPeriodPromiseToPay(dfEndPeriodPromiseToPay.getValue());
                }
                if (collection != null) {
                    history.setNbOverdueInDays(collection.getNbOverdueInDays());
                }

                if (!validate()) {
                    MessageBox mb = new MessageBox(UI.getCurrent(), "400px", "160px", I18N.message("information"),
                            MessageBox.Icon.ERROR, I18N.message("the.field.require.can't.null.or.empty"), Alignment.MIDDLE_RIGHT,
                            new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, I18N.message("ok")));
                    mb.show();
                } else {
                    SecUser secUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                    history.setAssignee(secUser);
                    ENTITY_SRV.saveOrUpdate(history);
                    collection.addContractCollectionHistory(history);
                    collection.setColResult(cbxCollecitonStatus.getSelectedEntity());
                    ENTITY_SRV.saveOrUpdate(collection);
                    setIndexedContainer();
                    window.close();
                }
            }
        });
        Button btnCancel = new NativeButton(I18N.message("cancel"));
        btnCancel.setIcon(new ThemeResource("../smt-default/icons/16/delete.png"));
        btnCancel.addClickListener(new ClickListener() {
            /** */
            private static final long serialVersionUID = 8902578830364522457L;

            @Override
            public void buttonClick(ClickEvent event) {
                window.close();
            }
        });

        NavigationPanel navigationPanel = new NavigationPanel();
        navigationPanel.addButton(btnSave);
        navigationPanel.addButton(btnCancel);

        FormLayout formLayout = new FormLayout();
        formLayout.setMargin(true);
        formLayout.addComponent(cbxCollecitonStatus);
        formLayout.addComponent(cbxcustomerAttribute);
        formLayout.addComponent(txtAmountPromiseToPay);
        formLayout.addComponent(dfStartPeriodPromiseToPay);
        formLayout.addComponent(dfEndPeriodPromiseToPay);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSpacing(true);

        verticalLayout.addComponent(navigationPanel);
        verticalLayout.addComponent(formLayout);

        window.setContent(verticalLayout);
        UI.getCurrent().addWindow(window);
    }

    private boolean validate() {
        boolean isValide = true;
        if (cbxCollecitonStatus.getSelectedEntity() == null) {
            isValide = false;
        }
        return isValide;
    }
}
