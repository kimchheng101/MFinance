package com.soma.mfinance.gui.ui.panel.registration;

import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.registrations.model.QuotationRegistrationLocationHistory;
import com.soma.mfinance.core.registrations.model.RegistrationStorageLocation;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.soma.frmk.vaadin.ui.widget.toolbar.event.AddClickListener;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;

import java.util.*;

/**
 * 
 * @author Riya.Pov
 */
public class RegistrationStorageLocationHistoryPanel extends AbstractTabPanel implements AddClickListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3812214171296159862L;
	
	private EntityRefComboBox<RegistrationStorageLocation> cbxStorageLocation;
	private SimplePagedTable<QuotationRegistrationLocationHistory> tableHistory;
	private NavigationPanel navigationPanel;
	
	private Quotation quotation;
	
	/**
	 * @see AddClickListener#addButtonClick(ClickEvent)
	 */

	@Override
	public void addButtonClick(ClickEvent arg0) {
		final Window window = new Window();
		window.setModal(true);
		window.setClosable(false);
		window.setResizable(false);
		window.setWidth(550, Unit.PIXELS);
		window.setHeight(300, Unit.PIXELS);
		window.setCaption(I18N.message("registration.location"));
		
		cbxStorageLocation = new EntityRefComboBox<RegistrationStorageLocation>(I18N.message("registration.location"));
		cbxStorageLocation.setRestrictions(new BaseRestrictions<RegistrationStorageLocation>(RegistrationStorageLocation.class));
		cbxStorageLocation.setWidth(250, Unit.PIXELS);
		cbxStorageLocation.renderer();
		
		Button btnSave = new NativeButton(I18N.message("save"));
		btnSave.setIcon(new ThemeResource("../smt-default/icons/16/save.png"));
		btnSave.addClickListener(new ClickListener(){

			/**
			 * 
			 */
			private static final long serialVersionUID = -4275098432417695479L;
		
			@Override
			public void buttonClick(ClickEvent event) {
				if (quotation == null) {
					return;
				}
				QuotationRegistrationLocationHistory history = new QuotationRegistrationLocationHistory();
				history.setQuotation(quotation);
				if(cbxStorageLocation.getSelectedEntity() != null){
					history.setRegistrationStorageLocation(cbxStorageLocation.getSelectedEntity());
					quotation.setRegistrationStorageLocation(cbxStorageLocation.getSelectedEntity());
				}
				
				ENTITY_SRV.saveOrUpdate(history);
				quotation.getQuotationRegistrationLocationHistory().add(history);
				ENTITY_SRV.saveOrUpdate(quotation);
				setIndexedContainer();
				window.close();
				
			}
		});
		Button btnCancel = new NativeButton(I18N.message("cancel"));
		btnCancel.setIcon(new ThemeResource("../smt-default/icons/16/delete.png"));
		btnCancel.addClickListener(new ClickListener() {
			/** */
			private static final long serialVersionUID = 8902578830364522457L;
			@Override
			public void buttonClick(ClickEvent event) {
				window.close();
			}
		});
		NavigationPanel navigationPanel = new NavigationPanel();
		navigationPanel.addButton(btnSave);
		navigationPanel.addButton(btnCancel);
		
		FormLayout formLayout = new FormLayout();
		formLayout.setMargin(true);
		formLayout.addComponent(cbxStorageLocation);
		
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setSpacing(true);
		
		verticalLayout.addComponent(navigationPanel);
		verticalLayout.addComponent(formLayout);
		
		window.setContent(verticalLayout);
		UI.getCurrent().addWindow(window);
	}
	/**
	 * @see AbstractTabPanel#createForm()
	 */
	@Override
	protected Component createForm() {
		navigationPanel = new NavigationPanel();
		navigationPanel.addAddClickListener(this);
		
		tableHistory = new SimplePagedTable<QuotationRegistrationLocationHistory>(createColumnDefinitions());
		
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setSpacing(true);
		verticalLayout.addComponent(navigationPanel);
		verticalLayout.addComponent(tableHistory);
		verticalLayout.addComponent(tableHistory.createControls());
		
		return verticalLayout;
	}
	
	/**
	 * ColumnDefinitions
	 * @return
	 */
	private List<ColumnDefinition> createColumnDefinitions() {
		List<ColumnDefinition> columnDefinitions = new ArrayList<ColumnDefinition>();
		
		columnDefinitions.add(new ColumnDefinition("date", I18N.message("date"), Date.class, Align.LEFT, 80));
		columnDefinitions.add(new ColumnDefinition("registration.location", I18N.message("registration.location"), String.class, Align.LEFT, 200));
		columnDefinitions.add(new ColumnDefinition("updateUser", I18N.message("update.user"), String.class, Align.LEFT, 200));

		return columnDefinitions;
	}
	
	/** */
	@SuppressWarnings("unchecked")
	private void setIndexedContainer() {
		Container.Indexed indexedContainer = tableHistory.getContainerDataSource();
		indexedContainer.removeAllItems();
		if (quotation.getQuotationRegistrationLocationHistory() == null || quotation.getQuotationRegistrationLocationHistory().isEmpty()) {
			return;
		}

		Collections.sort(quotation.getQuotationRegistrationLocationHistory(),	new Comparator<QuotationRegistrationLocationHistory>() {

			private static final int REVERSE = -1;

			@Override
			public int compare(QuotationRegistrationLocationHistory o1, QuotationRegistrationLocationHistory o2) {
				if (o1 == null || o2 == null) {
					return 0;
				}
				return o1.getCreateDate().compareTo(o2.getCreateDate()) * REVERSE;
			}

		});

		for (QuotationRegistrationLocationHistory history : quotation.getQuotationRegistrationLocationHistory()) {
			Item item = indexedContainer.addItem(history.getId());
			item.getItemProperty("date").setValue(history.getCreateDate());
			item.getItemProperty("registration.location").setValue(history.getRegistrationStorageLocation() != null ? history.getRegistrationStorageLocation().getDescEn() : "");
			item.getItemProperty("updateUser").setValue(history.getUpdateUser());

		}
		tableHistory.refreshContainerDataSource();
	}
	
	/**
	 * @param quotation
	 */
	public void assignValues (Quotation quotation) {
		this.quotation = quotation;
		setIndexedContainer();
	}

}
