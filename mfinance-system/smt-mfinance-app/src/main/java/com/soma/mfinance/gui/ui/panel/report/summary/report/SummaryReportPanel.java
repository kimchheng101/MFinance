package com.soma.mfinance.gui.ui.panel.report.summary.report;

import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.soma.mfinance.core.asset.model.EFrequencyType;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import ru.xpoft.vaadin.VaadinView;
/**
 * 
 * @author p.ly
 *
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(SummaryReportPanel.NAME)
public class SummaryReportPanel extends AbstractTabPanel implements View, FinServicesHelper {
	
	private static final long serialVersionUID = 6227740006388204118L;

	public static final String NAME = "summary.report";
	private TabSheet tabSheet;
	private SimplePagedTable<Contract> pagedTable;
	private List<ColumnDefinition> columnDefinitions;
	private SummaryReportSearchPanel summaryReportSearchPanel;
	private SummaryReportTable summaryReportTable;
	private VerticalLayout contentLayout;
	private  Panel searchPanel;
	private List<Contract> contracts;
	private EFrequencyType type;
	private Button btnSearch;
	private Date startDate;
	private Date endDate;
	
	@PostConstruct
	public void PostConstruct() {
	}
	@Override
	public void enter(ViewChangeEvent event) {
		
	}

	@Override
	protected com.vaadin.ui.Component createForm() {
		summaryReportSearchPanel = new SummaryReportSearchPanel();
		tabSheet = new TabSheet();
		contentLayout = new VerticalLayout();
		contentLayout.setSpacing(true);
		contentLayout.setMargin(true);
		
		VerticalLayout gridLayoutPanel = new VerticalLayout();
		VerticalLayout searchLayout = new VerticalLayout();
		HorizontalLayout buttonsLayout = new HorizontalLayout();
		btnSearch = new Button(I18N.message("search"));
		btnSearch.setClickShortcut(KeyCode.ENTER, null); // null it means we don't modify key of shortcut Enter(default = 13)
		btnSearch.setIcon(new ThemeResource("../smt-default/icons/16/search.png"));
		btnSearch.addClickListener(new ClickListener() {		
			private static final long serialVersionUID = -3403059921454308342L;
			@Override
			public void buttonClick(ClickEvent event) {
				validatBeforeSearching();
			}
		});
		
		Button btnReset = new Button(I18N.message("reset"));
		btnReset.setIcon(new ThemeResource("../smt-default/icons/16/reset.png"));
		btnReset.addClickListener(new ClickListener() {
			private static final long serialVersionUID = -7165734546798826698L;
			@Override
			public void buttonClick(ClickEvent event) {
				summaryReportSearchPanel.reset();
			}
		});
		buttonsLayout.setSpacing(true);
		buttonsLayout.setStyleName("panel-search-center");
		buttonsLayout.addComponent(btnSearch);
		buttonsLayout.addComponent(btnReset);
        gridLayoutPanel.addComponent(summaryReportSearchPanel.getSearchForm());
        searchLayout.setMargin(true);
        searchLayout.setSpacing(true);
        searchLayout.addComponent(gridLayoutPanel);
        searchLayout.addComponent(buttonsLayout);
        
        searchPanel = new Panel();
        searchPanel.setCaption(I18N.message("search"));
        searchPanel.setContent(searchLayout);
        
        summaryReportTable = new SummaryReportTable(btnSearch);
        this.columnDefinitions = summaryReportTable.getHeader(type);
        pagedTable = new SimplePagedTable<Contract>(this.columnDefinitions);
        summaryReportTable.setPagedTable(pagedTable);
        contentLayout.addComponent(searchPanel);
        contentLayout.addComponent(pagedTable);
        contentLayout.addComponent(pagedTable.createControls());
        
        tabSheet.addTab(contentLayout, I18N.message("summary.report"));
		
        return tabSheet;
	}
	/**
	 * loadData()
	 */
	public void loadData(){
		contracts = summaryReportSearchPanel.getContracts();
		startDate = summaryReportSearchPanel.startDate();
		endDate = summaryReportSearchPanel.endDate();
		type = summaryReportSearchPanel.getType();

        this.columnDefinitions = summaryReportTable.getHeader(type);
        pagedTable = new SimplePagedTable<Contract>(this.columnDefinitions);
        contentLayout.removeAllComponents();
        summaryReportTable.setPagedTable(pagedTable);
        contentLayout.addComponent(searchPanel);
        contentLayout.addComponent(pagedTable);
        contentLayout.addComponent(pagedTable.createControls());
        pagedTable.refreshContainerDataSource();
        
		summaryReportTable.getData(contracts, startDate, endDate ,type);
	}
	/**
	 * validatBeforeSearching()
	 */
	protected void validatBeforeSearching() {
		errors = summaryReportSearchPanel.getErrorMessagesBeforSearch();
		if (!errors.isEmpty()) {
			for (String error : errors) {
				Notification.show("", error, Type.WARNING_MESSAGE);
			}
		}else{
			loadData();
		}
	}
	
}
