package com.soma.mfinance.gui.ui.panel.report.qualityincentive;

import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.RowRenderer;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.data.Item;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.Entity;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;

import static com.soma.mfinance.core.helper.FinServicesHelper.INSTALLMENT_SERVICE_MFP;

/**
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class QualityIncentiveTablePanel extends AbstractTablePanel<Contract> implements FMEntityField {

    private static final long serialVersionUID = 5204101495689053450L;
    private static final String QUALITY_INCENTIVE = "quality.incentive.data";

    @PostConstruct
    public void PostConstruct() {
        setSizeFull();
        setMargin(true);
        setSpacing(true);
        setCaption(I18N.message(QUALITY_INCENTIVE));
        super.init("");
    }

    @Override
    protected PagedDataProvider<Contract> createPagedDataProvider() {

        PagedDefinition<Contract> pagedDefinition = new PagedDefinition<>(searchPanel.getRestrictions());
        pagedDefinition.setRowRenderer(new QualityIncentiveRowRenderer());
        pagedDefinition.addColumnDefinition("reference", I18N.message("reference").toUpperCase(), String.class, Align.LEFT, 230);
        pagedDefinition.addColumnDefinition("dealer", I18N.message("dealer").toUpperCase(), String.class, Align.LEFT, 230);
        pagedDefinition.addColumnDefinition("term", I18N.message("term").toUpperCase(), Integer.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("contract.status", I18N.message("contract.status").toUpperCase(), String.class, Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("start.date", I18N.message("contract.start.date").toUpperCase(), Date.class, Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("ti.financed.amount", I18N.message("contract.amount").toUpperCase(), Double.class, Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("co.name", I18N.message("co.name").toUpperCase(), String.class, Align.LEFT, 180);
        pagedDefinition.addColumnDefinition("po.name", I18N.message("po.name").toUpperCase(), String.class, Align.LEFT, 180);
        pagedDefinition.addColumnDefinition("co.who.make.field.check", I18N.message("co.who.make.field.check").toUpperCase(), String.class, Align.LEFT, 180);
        pagedDefinition.addColumnDefinition("overdue.day", I18N.message("overdue.day").toUpperCase(), Integer.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("outstanding", I18N.message("outstanding").toUpperCase(), String.class, Align.LEFT, 180);
        EntityPagedDataProvider<Contract> pagedDataProvider = new EntityPagedDataProvider<>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    private class QualityIncentiveRowRenderer implements RowRenderer {

        @SuppressWarnings("unchecked")
        @Override
        public void renderer(Item item, Entity entity) {
            Contract contract = (Contract) entity;
            if (contract != null) {
                Collection otherData = contract.getCollection();
                Integer nbOverdueDays = otherData != null ? otherData.getNbOverdueInDays() : Integer.valueOf(0);
                Double outStanding = otherData != null ? otherData.getTiBalanceCapital() : Double.valueOf(0.0);
                //Amount outStanding = contractService.getRealOutstanding(DateUtils.todayH00M00S00(), contract_old.getId());
                item.getItemProperty("reference").setValue(contract.getReference());
                item.getItemProperty("dealer").setValue(contract.getDealer().getNameEn());
                item.getItemProperty("term").setValue(contract.getTerm());
                item.getItemProperty("contract.status").setValue(contract.getWkfStatus().getDescEn());
                item.getItemProperty("start.date").setValue(contract.getStartDate());
                item.getItemProperty("ti.financed.amount").setValue(contract.getTiFinancedAmount());
                if (contract.getQuotation() != null) {
                    if (contract.getQuotation().getCreditOfficer() != null) {
                        item.getItemProperty("co.name").setValue(contract.getQuotation().getCreditOfficer().getDesc());
                    }
                    if (contract.getQuotation().getProductionOfficer() != null) {
                        item.getItemProperty("po.name").setValue(contract.getQuotation().getProductionOfficer().getDesc());
                    }
                    if (contract.getQuotation().getFieldCheck() != null) {
                        item.getItemProperty("co.who.make.field.check").setValue(contract.getQuotation().getFieldCheck().getDesc());
                    } else if (contract.getQuotation().getCreditOfficer() != null) {
                        item.getItemProperty("co.who.make.field.check").setValue(contract.getQuotation().getCreditOfficer().getDesc());
                    }
                }
                item.getItemProperty("overdue.day").setValue(nbOverdueDays);
                if(contract.getQuotation() != null){
                    item.getItemProperty("outstanding").setValue(AmountUtils.format(INSTALLMENT_SERVICE_MFP.getOutStanding(contract.getQuotation())));
                }
            }

        }
    }

    @Override
    protected Contract getEntity() {
        return null;
    }

    @Override
    protected AbstractSearchPanel<Contract> createSearchPanel() {
        return new QualityIncentiveSearchPanel(this);
    }
}
