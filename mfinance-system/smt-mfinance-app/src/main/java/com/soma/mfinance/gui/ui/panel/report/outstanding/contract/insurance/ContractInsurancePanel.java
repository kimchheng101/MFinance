package com.soma.mfinance.gui.ui.panel.report.outstanding.contract.insurance;

import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;


@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(ContractInsurancePanel.NAME)
                                                                                    // TODO : remote this
public class ContractInsurancePanel extends AbstractTabsheetPanel implements View, FinServicesHelper {

    private static final long serialVersionUID = 1L;
    public static final String NAME = "contract.insurance.by.year";

    @Autowired
    private ContractInsuranceTablePanel contractInsuranceTablePanel;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        contractInsuranceTablePanel.setMainPanel(this);
        getTabSheet().setTablePanel(contractInsuranceTablePanel);
    }

    @Override
    public void enter(ViewChangeEvent event) {

    }

    @Override
    public void onAddEventClick() {

    }

    @Override
    public void onEditEventClick() {

    }

    @Override
    public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
        getTabSheet().setSelectedTab(selectedTab);
        contractInsuranceTablePanel.refresh();
    }

}
