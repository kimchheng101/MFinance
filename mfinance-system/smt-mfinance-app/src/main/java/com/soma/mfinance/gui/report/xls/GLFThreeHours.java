package com.soma.mfinance.gui.report.xls;

import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.financial.model.FinProduct;
import com.soma.mfinance.core.quotation.QuotationService;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.mfinance.core.shared.report.ReportParameter;
import com.soma.mfinance.core.shared.util.DateFilterUtil;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.mfinance.glf.statistic.model.StatisticConfig;
import com.soma.mfinance.tools.report.Report;
import com.soma.mfinance.tools.report.XLSAbstractReportExtractor;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author kimsuor.seang
 */
public class GLFThreeHours extends XLSAbstractReportExtractor implements Report, GLFApplicantFields, QuotationEntityField {

    protected QuotationService quotationService = SpringUtils.getBean(QuotationService.class);

    private Map<String, CellStyle> styles = null;

    public static String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
    /**
     * Background color format
     */
    static short BG_BLUE = IndexedColors.DARK_BLUE.getIndex();// IndexedColors.DARK_TEAL.getIndex();
    static short BG_RED = IndexedColors.RED.getIndex();
    static short BG_CYAN = IndexedColors.LIGHT_TURQUOISE.getIndex();
    static short BG_YELLOW = IndexedColors.LIGHT_YELLOW.getIndex();
    static short BG_GREY22 = 22;
    static short BG_GREY = IndexedColors.GREY_25_PERCENT.getIndex();
    static short BG_LIGHT_BLUE = IndexedColors.LIGHT_BLUE.getIndex();
    static short BG_WHITE = IndexedColors.WHITE.getIndex();
    static short BG_GREEN = IndexedColors.GREEN.getIndex();

    /**
     * Font color
     */
    static short FC_WHITE = IndexedColors.WHITE.getIndex();
    static short FC_BLACK = IndexedColors.BLACK.getIndex();
    static short FC_BLUE = 48;
    static short FC_GREY = IndexedColors.GREY_80_PERCENT.getIndex();
    static short FC_GREEN = IndexedColors.GREEN.getIndex();

    public GLFThreeHours() {

    }

    @Override
    public String generate(ReportParameter reportParameter) throws Exception {
        Map<String, Object> parameters = reportParameter.getParameters();
        Date date = (Date) parameters.get("dateValue");
        EDealerType dealerType = (EDealerType) parameters.get("dealerType");
        Dealer dealer = (Dealer) parameters.get("dealer");
        FinProduct finProduct = (FinProduct) parameters.get("financialProduct");

        BaseRestrictions<Dealer> restrictions = new BaseRestrictions<>(Dealer.class);
        restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
        if (dealerType != null) {
            restrictions.addCriterion("dealerType", dealerType);
        }
        if (dealer != null) {
            restrictions.addCriterion("id", dealer.getId());
        }

        restrictions.addOrder(Order.asc("createDate"));
        List<Dealer> dealers = quotationService.list(restrictions);

        createWorkbook(null);
        XSSFSheet sheet = wb.createSheet();
        sheet.lockDeleteColumns();
        sheet.lockDeleteRows();
        sheet.lockFormatCells();
        sheet.lockFormatColumns();
        sheet.lockFormatRows();
        sheet.lockInsertColumns();
        sheet.lockInsertRows();
        CellStyle style = wb.createCellStyle();
        styles = new HashMap<String, CellStyle>();
        createStyles();
        sheet.setColumnWidth(0, 1200);
        sheet.setColumnWidth(1, 6400);
        sheet.setColumnWidth(2, 3550);
        sheet.setColumnWidth(3, 3550);
        sheet.setColumnWidth(4, 3550);
        sheet.setColumnWidth(5, 3550);
        sheet.setColumnWidth(6, 5600);
        sheet.setColumnWidth(7, 5600);
        sheet.setColumnWidth(8, 2300);
        sheet.setColumnWidth(9, 2200);
        sheet.setColumnWidth(10, 2200);
        sheet.setColumnWidth(11, 2200);
        sheet.setColumnWidth(12, 2200);
        sheet.setColumnWidth(13, 2200);
        sheet.setColumnWidth(14, 1900);
        sheet.setColumnWidth(15, 2000);
        sheet.setColumnWidth(16, 2100);
        sheet.setColumnWidth(17, 2100);
        sheet.setColumnWidth(18, 1800);
        sheet.setColumnWidth(19, 2200);
        sheet.setColumnWidth(20, 2100);
        sheet.setColumnWidth(21, 2100);
        sheet.setColumnWidth(22, 2000);

        sheet.setZoom(7, 10);
        final PrintSetup printSetup = sheet.getPrintSetup();

        printSetup.setPaperSize(HSSFPrintSetup.A4_PAPERSIZE);
        printSetup.setLandscape(true);

        printSetup.setScale((short) 75);

        // Setup the Page margins - Left, Right, Top and Bottom
        sheet.setMargin(Sheet.LeftMargin, 0.25);
        sheet.setMargin(Sheet.RightMargin, 0.25);
        sheet.setMargin(Sheet.TopMargin, 0.25);
        sheet.setMargin(Sheet.BottomMargin, 0.25);
        sheet.setMargin(Sheet.HeaderMargin, 0.25);
        sheet.setMargin(Sheet.FooterMargin, 0.25);
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BIG_SPOTS);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        Row headerRow = sheet.createRow(0);
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 18));
        createCell(headerRow, 0, "Three-Hours Report", 14, true, false, false, CellStyle.ALIGN_LEFT, true, BG_WHITE, FC_BLACK, false);
        headerRow = sheet.createRow(1);
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, 18));
        createCell(headerRow, 0, "Date :" + DateUtils.getDateLabel(date, "dd-MMM-yyyy"), 14, true, false, false, CellStyle.ALIGN_LEFT, true, BG_WHITE, FC_BLACK, false);
        headerRow = sheet.createRow(2);
        sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, 18));
        createCell(headerRow, 0, "Time :" + DateUtils.formatDate(DateUtils.today(), "HH:mm a"), 14, true, false, false, CellStyle.ALIGN_LEFT, true, BG_WHITE, FC_BLACK, false);

        int iRow = 2;
        int iCol = 0;
        iCol = iCol + 2;
        iCol = 0;
        iRow = iRow + 1;
        if (date == null) {
            date = DateUtils.today();
        }
        iRow = dataTable(sheet, iRow, style, dealers, date, finProduct);

//		Row tmpRowEnd = sheet.createRow(iRow++);
//		tmpRowEnd.setRowStyle(style);

        iRow = iRow + 1;

        String fileName = writeXLSData("Three-Hours_" + DateUtils.getDateLabel(DateUtils.today(), "yyyyMMddHHmmssSSS") + ".xlsx");

        return fileName;
    }

    private int dataTable(final Sheet sheet, int iRow, final CellStyle style, List<Dealer> dealers, Date date, FinProduct finProduct) throws Exception {
        /* Create total data header */
        Format formatter = new SimpleDateFormat("MMMM");
        String month = formatter.format(date);

        int iCol = 0;
        Row tmpRow = sheet.createRow(iRow++);

        createCell(tmpRow, iCol++, "NO.", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow, iCol - 1, iCol - 1));

        createCell(tmpRow, iCol++, "BRANCH", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow, iCol - 1, iCol - 1));

        // bucky edit 1
        createCell(tmpRow, iCol++, "TODAY APPRAISAL", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow, iCol - 1, iCol - 1));

        createCell(tmpRow, iCol++, "TOTAL APPRAISAL", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow, iCol - 1, iCol - 1));


        createCell(tmpRow, iCol++, "TODAY APPLY", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow, iCol - 1, iCol - 1));

        createCell(tmpRow, iCol++, "TOTAL APPLIED", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow, iCol - 1, iCol - 1));

        // bucky edit 4
        createCell(tmpRow, iCol++, "NEW CONTRACTS TODAY", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow, iCol - 1, iCol - 1));


        createCell(tmpRow, iCol++, "NEW CONTRACTS IN " + month.toUpperCase(), 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow, iCol - 1, iCol - 1));


        createCell(tmpRow, iCol++, "TARGET", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow, iCol - 1, iCol - 1));

        //OLD TARGET COLUMN
        /*createCell(tmpRow, iCol++, "TARGET", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 8, 8));*/
        // sub target
        /*createCell(tmpRow, 9, "", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);*/

        createCell(tmpRow, 9, "Appraisal", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 9, 12));
        createCell(tmpRow, 10, "", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
        createCell(tmpRow, 11, "", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
        createCell(tmpRow, 12, "", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);

        createCell(tmpRow, 13, "Applied", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);
        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 13, 16));
        createCell(tmpRow, 14, "", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);
        createCell(tmpRow, 15, "", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);
        createCell(tmpRow, 16, "", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);

        createCell(tmpRow, 17, "CONTRACT", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, true);
        sheet.addMergedRegion(new CellRangeAddress(iRow - 1, iRow - 1, 17, 20));
        createCell(tmpRow, 18, "", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);
        createCell(tmpRow, 19, "", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);
        createCell(tmpRow, 20, "", 10, false, true, true,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);

        iCol = 9;
        tmpRow = sheet.createRow(iRow++);
        int iColBorder = 0;
        createCellBorderBottomAndRight(tmpRow, iColBorder++, "", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);
        createCellBorderBottomAndRight(tmpRow, iColBorder++, "", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);

        //bucky edit 2 -- add row for appraisal
        createCellBorderBottomAndRight(tmpRow, iColBorder++, "", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);
        createCellBorderBottomAndRight(tmpRow, iColBorder++, "", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);

        createCellBorderBottomAndRight(tmpRow, iColBorder++, "", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);
        createCellBorderBottomAndRight(tmpRow, iColBorder++, "", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);
        createCellBorderBottomAndRight(tmpRow, iColBorder++, "", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);
        createCellBorderBottomAndRight(tmpRow, iColBorder++, "", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);

        createCellBorderBottomAndRight(tmpRow, iColBorder++, "", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);

        // LOW AND HIGH FOR TARGET old
        /*createCell(tmpRow, iCol++, "LOW", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);
        createCell(tmpRow, iCol++, "HIGH", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);*/

        createCell(tmpRow, iCol++, "11:00 AM", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);
        createCell(tmpRow, iCol++, "14:00 PM", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);
        createCell(tmpRow, iCol++, "17:00 PM", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);

        createCell(tmpRow, iCol++, "TOTAL", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);

        createCell(tmpRow, iCol++, "11:00 AM", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);
        createCell(tmpRow, iCol++, "14:00 PM", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);
        createCell(tmpRow, iCol++, "17:00 PM", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);

        createCell(tmpRow, iCol++, "TOTAL", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);

        createCell(tmpRow, iCol++, "11:00 AM", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);
        createCell(tmpRow, iCol++, "14:00 PM", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);
        createCell(tmpRow, iCol++, "17:00 PM", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);

        createCell(tmpRow, iCol++, "TOTAL", 10, false, true, false,
                CellStyle.ALIGN_CENTER, true, BG_GREY, FC_BLACK, false);

        if (dealers != null && !dealers.isEmpty()) {
            int nbRows = dealers.size();
            int totalNumQuotationApplyToday = 0;
            int totalNumQuotationTotalApply = 0;
            int totalNumNewContractInMonth = 0;
            int totalNumNewContractToday = 0;

            int totalAppraisal_11 = 0;
            int totalAppraisal_14 = 0;
            int totalAppraisal_17 = 0;
            int totalForTotalAppraisal = 0;

            int totalContract_11 = 0;
            int totalContract_14 = 0;
            int totalContract_17 = 0;
            int totalForTotalContract = 0;

            int totalNumberAppraisalToday = 0;
            int totalAppraisal = 0;
            int totalTargetLow = 0;

            int totalNumApplied_11 = 0;
            int totalNumApplied_14 = 0;
            int totalNumApplied_17 = 0;
            int totalForTotalApply = 0;

            for (int i = 0; i < nbRows; i++) {
                Dealer dealer = dealers.get(i);
                BaseRestrictions<StatisticConfig> restrictions = new BaseRestrictions<>(StatisticConfig.class);
                restrictions.addCriterion(Restrictions.eq("dealer.id", dealer.getId()));
                restrictions.addCriterion(Restrictions.eq("startDate", DateUtils.getDateAtBeginningOfMonth(date)));
                List<StatisticConfig> statisticConfigs = quotationService.list(restrictions);

                int targetLow = 0;
                if (statisticConfigs != null && !statisticConfigs.isEmpty()) {
                    targetLow = statisticConfigs.get(0).getTargetLow();
                    totalTargetLow += targetLow;
                }

                tmpRow = sheet.createRow(iRow++);
                iCol = 0;

                int numApplied_11 = 0;
                int numApplied_14 = 0;
                int numApplied_17 = 0;

                List<Quotation> applied3Hours = getNumApplied(dealer, date, finProduct);
                if (applied3Hours != null && !applied3Hours.isEmpty()) {
                    for (Quotation applied3Hour : applied3Hours) {
                        Calendar calendar11 = Calendar.getInstance();
                        calendar11.setTime(applied3Hour.getFirstApplyDate());
                        calendar11.set(Calendar.HOUR_OF_DAY, 11);
                        calendar11.set(Calendar.MINUTE, 0);

                        Calendar calendar14 = Calendar.getInstance();
                        calendar14.setTime(applied3Hour.getFirstApplyDate());
                        calendar14.set(Calendar.HOUR_OF_DAY, 14);
                        calendar14.set(Calendar.MINUTE, 0);

                        if (applied3Hour.getFirstApplyDate().before(calendar11.getTime())) {
                            numApplied_11 += 1;
                        } else if (applied3Hour.getFirstApplyDate().before(calendar14.getTime())) {
                            numApplied_14 += 1;
                        } else {
                            numApplied_17 += 1;
                        }
                    }
                }

                // CALCULATE APPRAISAL IN HOURS
                int numAppraisal_11 = 0;
                int numAppraisal_14 = 0;
                int numAppraisal_17 = 0;
                List<Quotation> numAppraisalInHours = getNumAppraisalInHours(dealer, date, finProduct);
                if (numAppraisalInHours != null && !numAppraisalInHours.isEmpty()) {
                    for (Quotation quotationAppraisal : numAppraisalInHours) {
                        Calendar calendar11 = Calendar.getInstance();
                        calendar11.setTime(quotationAppraisal.getFirstSubmissionDate());
                        calendar11.set(Calendar.HOUR_OF_DAY, 11);
                        calendar11.set(Calendar.MINUTE, 0);

                        Calendar calendar14 = Calendar.getInstance();
                        calendar14.setTime(quotationAppraisal.getFirstSubmissionDate());
                        calendar14.set(Calendar.HOUR_OF_DAY, 14);
                        calendar14.set(Calendar.MINUTE, 0);

                        if (quotationAppraisal.getFirstSubmissionDate().before(calendar11.getTime())) {
                            numAppraisal_11 += 1;
                        } else if (quotationAppraisal.getFirstSubmissionDate().before(calendar14.getTime())) {
                            numAppraisal_14 += 1;
                        } else {
                            numAppraisal_17 += 1;
                        }
                    }
                }

                // CALCULATE CONTRACT IN HOURS
                int contract_11 = 0;
                int contract_14 = 0;
                int contract_17 = 0;
                List<Quotation> listContract = getContractInHours(dealer, date, finProduct);
                if (listContract != null && !listContract.isEmpty()) {
                    for (Quotation quotationActivate : listContract) {
                        Calendar calendar11 = Calendar.getInstance();
                        calendar11.setTime(quotationActivate.getContractStartDate());
                        calendar11.set(Calendar.HOUR_OF_DAY, 11);
                        calendar11.set(Calendar.MINUTE, 0);

                        Calendar calendar14 = Calendar.getInstance();
                        calendar14.setTime(quotationActivate.getContractStartDate());
                        calendar14.set(Calendar.HOUR_OF_DAY, 14);
                        calendar14.set(Calendar.MINUTE, 0);

                        if (quotationActivate.getContractStartDate().before(calendar11.getTime())) {
                            contract_11 += 1;
                        } else if (quotationActivate.getContractStartDate().before(calendar14.getTime())) {
                            contract_14 += 1;
                        } else {
                            contract_17 += 1;
                        }
                    }
                }

                //APPRAISAL TODAY AND COUNT TO TOTAL APPRAISAL TODAY
                long mumOfAppraisalToday = getAppraisalToday(dealer, date, finProduct);
                totalNumberAppraisalToday += mumOfAppraisalToday;

                //TOTAL APPRAISAL AND COUNT TO TOTAL APPRAISAL
                long numTotalAppraisal = getTotalAppraisal(dealer, date, finProduct);
                totalAppraisal += numTotalAppraisal;

                //APPLY TODAY AND COUNT TO TOTAL APPLY TODAY
                long numOfQuotationApplyToday = getNumQuotationApplyToday(dealer, date, finProduct);
                totalNumQuotationApplyToday += numOfQuotationApplyToday;

                long numQuotationTotalApply = getNumQuotationTotalApply(dealer, date, finProduct);
                totalNumQuotationTotalApply += numQuotationTotalApply;

                long numNewContractInMonth = getNumNewContractInMonth(dealer, date, finProduct);
                totalNumNewContractInMonth += numNewContractInMonth;

                long numNewContractToday = getNumContractInDay(dealer, date, finProduct);
                totalNumNewContractToday += numNewContractToday;

                totalNumApplied_11 += numApplied_11;
                totalNumApplied_14 += numApplied_14;
                totalNumApplied_17 += numApplied_17;
                totalForTotalApply += numApplied_11 + numApplied_14 + numApplied_17;

                totalAppraisal_11 += numAppraisal_11;
                totalAppraisal_14 += numAppraisal_14;
                totalAppraisal_17 += numAppraisal_17;
                totalForTotalAppraisal += numAppraisal_11 + numAppraisal_14 + numAppraisal_17;

                totalContract_11 += contract_11;
                totalContract_14 += contract_14;
                totalAppraisal_17 += totalContract_17;
                totalForTotalContract += contract_11 + contract_14 + contract_17;

                // ALL FIELDS VALUES FOR TABLE
                createNumericCell(tmpRow, iCol++, (i + 1), true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
                createNumericCell(tmpRow, iCol++, getDefaultString(dealer.getNameEn()), true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
                createNumericCell(tmpRow, iCol++, mumOfAppraisalToday, true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
                createNumericCell(tmpRow, iCol++, numTotalAppraisal, true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
                createNumericCell(tmpRow, iCol++, numOfQuotationApplyToday, true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
                createNumericCell(tmpRow, iCol++, numQuotationTotalApply, true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
                createNumericCell(tmpRow, iCol++, numNewContractToday, true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
                createNumericCell(tmpRow, iCol++, numNewContractInMonth, true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
                createNumericCell(tmpRow, iCol++, (targetLow == 0 ? "" : targetLow), true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);

                // HIGH TARGET FOR OLD
                /*createNumericCell(tmpRow, iCol++, (targetHigh == 0 ? "" : targetHigh), true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);*/

                //APPRAISAL
                createNumericCell(tmpRow, iCol++, numAppraisal_11, true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
                createNumericCell(tmpRow, iCol++, numAppraisal_14, true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
                createNumericCell(tmpRow, iCol++, numAppraisal_17, true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
                createNumericCell(tmpRow, iCol++, numAppraisal_11 + numAppraisal_14 + numAppraisal_17, true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);

                //APPLIED
                createNumericCell(tmpRow, iCol++, numApplied_11, true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
                createNumericCell(tmpRow, iCol++, numApplied_14, true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
                createNumericCell(tmpRow, iCol++, numApplied_17, true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
                createNumericCell(tmpRow, iCol++, numApplied_11 + numApplied_14 + numApplied_17, true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);

                //CONTRACT
                createNumericCell(tmpRow, iCol++, contract_11, true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
                createNumericCell(tmpRow, iCol++, contract_14, true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
                createNumericCell(tmpRow, iCol++, contract_17, true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
                createNumericCell(tmpRow, iCol++, contract_11 + contract_14 + contract_17, true,
                        CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);

            }

            tmpRow = sheet.createRow(iRow++);
            iCol = 0;
            createNumericCell(tmpRow, iCol++, "", true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
            createNumericCell(tmpRow, iCol++, "Total", true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);

            createNumericCell(tmpRow, iCol++, totalNumberAppraisalToday, true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
            createNumericCell(tmpRow, iCol++, totalAppraisal, true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);

            createNumericCell(tmpRow, iCol++, totalNumQuotationApplyToday, true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
            createNumericCell(tmpRow, iCol++, totalNumQuotationTotalApply, true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);

            createNumericCell(tmpRow, iCol++, totalNumNewContractToday, true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
            createNumericCell(tmpRow, iCol++, totalNumNewContractInMonth, true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);

            createNumericCell(tmpRow, iCol++, totalTargetLow, true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
            createNumericCell(tmpRow, iCol++, totalAppraisal_11, true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);

            createNumericCell(tmpRow, iCol++, totalAppraisal_14, true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
            createNumericCell(tmpRow, iCol++, totalAppraisal_17, true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);

            createNumericCell(tmpRow, iCol++, totalForTotalAppraisal, true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
            createNumericCell(tmpRow, iCol++, totalNumApplied_11, true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);

            createNumericCell(tmpRow, iCol++, totalNumApplied_14, true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
            createNumericCell(tmpRow, iCol++, totalNumApplied_17, true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);

            createNumericCell(tmpRow, iCol++, totalForTotalApply, true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
            createNumericCell(tmpRow, iCol++, totalContract_11, true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);

            createNumericCell(tmpRow, iCol++, totalContract_14, true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
            createNumericCell(tmpRow, iCol++, totalContract_17, true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);
            createNumericCell(tmpRow, iCol++, totalForTotalContract, true,
                    CellStyle.ALIGN_CENTER, 10, false, true, BG_WHITE, FC_BLACK);

            iRow = iRow + 1;
        }

        return iRow;

    }

    /**
     * @param dealer
     * @param startDate
     * @param finProduct
     * @return get number for apply in a day (approved cs -> proposal)
     */
    private int getNumQuotationApplyToday(Dealer dealer, Date startDate, FinProduct finProduct) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<Quotation>(Quotation.class);
        restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        restrictions.addCriterion(Restrictions.ge("firstApplyDate", DateFilterUtil.getStartDate(startDate)));
        restrictions.addCriterion(Restrictions.le("firstApplyDate", DateFilterUtil.getEndDate(startDate)));
        if (finProduct != null) {
            restrictions.addCriterion(Restrictions.eq("financialProduct.id", finProduct.getId()));
        }
        return (int) quotationService.count(restrictions);
    }

    /**
     * @param dealer
     * @param startDate
     * @param finProduct
     * @return
     */

    //NUMBERS APPLIED PER HOURS
    private List<Quotation> getNumApplied(Dealer dealer, Date startDate, FinProduct finProduct) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<Quotation>(Quotation.class);
        restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        restrictions.addCriterion(Restrictions.ge("firstApplyDate", DateFilterUtil.getStartDate(startDate)));
        restrictions.addCriterion(Restrictions.le("firstApplyDate", DateFilterUtil.getEndDate(startDate)));
        if (finProduct != null)
            restrictions.addCriterion(Restrictions.eq("financialProduct.id", finProduct.getId()));
        return quotationService.list(restrictions);
    }

    /**
     * @param dealer
     * @param date
     * @param finProduct
     * @return
     */
    private long getNumQuotationTotalApply(Dealer dealer, Date date, FinProduct finProduct) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<Quotation>(Quotation.class);
        restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        restrictions.addCriterion(Restrictions.ge("firstApplyDate", DateFilterUtil.getStartDate(DateUtils.getDateAtBeginningOfMonth(date))));
        restrictions.addCriterion(Restrictions.le("firstApplyDate", DateFilterUtil.getEndDate(date)));
        if (finProduct != null)
            restrictions.addCriterion(Restrictions.eq("financialProduct.id", finProduct.getId()));
        return quotationService.count(restrictions);
    }

    /**
     * @param dealer
     * @param date
     * @param finProduct
     * @return
     */
    private long getNumNewContractInMonth(Dealer dealer, Date date, FinProduct finProduct) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        restrictions.addCriterion(Restrictions.eq(WKF_STATUS, QuotationWkfStatus.ACT));
        restrictions.addCriterion(Restrictions.ge("contractStartDate", DateFilterUtil.getStartDate(DateUtils.getDateAtBeginningOfMonth(date))));
        restrictions.addCriterion(Restrictions.le("contractStartDate", DateFilterUtil.getEndDate(date)));
        if (finProduct != null)
            restrictions.addCriterion(Restrictions.eq("financialProduct.id", finProduct.getId()));
        return quotationService.count(restrictions);
    }

    //NEW CONTRACT IN A DAY
    private int getNumContractInDay(Dealer dealer, Date date, FinProduct finProduct) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        restrictions.addCriterion(Restrictions.eq(WKF_STATUS, QuotationWkfStatus.ACT));
        restrictions.addCriterion(Restrictions.ge("contractStartDate", DateFilterUtil.getStartDate(date)));
        restrictions.addCriterion(Restrictions.le("contractStartDate", DateFilterUtil.getEndDate(date)));
        if (finProduct != null)
            restrictions.addCriterion(Restrictions.eq("financialProduct.id", finProduct.getId()));
        return (int) quotationService.count(restrictions);
    }

    //APPRAISAL TODAY
    private int getAppraisalToday(Dealer dealer, Date date, FinProduct finProduct) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        restrictions.addCriterion(Restrictions.ge("firstSubmissionDate", DateFilterUtil.getStartDate(date)));
        restrictions.addCriterion(Restrictions.le("firstSubmissionDate", DateFilterUtil.getEndDate(date)));
        if (finProduct != null)
            restrictions.addCriterion(Restrictions.eq("financialProduct.id", finProduct.getId()));
        return (int) quotationService.count(restrictions);
    }


    //GET TOTAL APPRAISAL
    private int getTotalAppraisal(Dealer dealer, Date date, FinProduct finProduct) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        restrictions.addCriterion(Restrictions.ge("firstSubmissionDate", DateFilterUtil.getStartDate(DateUtils.getDateAtBeginningOfMonth(date))));
        restrictions.addCriterion(Restrictions.le("firstSubmissionDate", DateFilterUtil.getEndDate(date)));
        if (finProduct != null)
            restrictions.addCriterion(Restrictions.eq("financialProduct.id", finProduct.getId()));
        return (int) quotationService.count(restrictions);
    }

    //GET APPRAISAL IN HOURS
    private List<Quotation> getNumAppraisalInHours(Dealer dealer, Date date, FinProduct finProduct) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        restrictions.addCriterion(Restrictions.ge("firstSubmissionDate", DateFilterUtil.getStartDate(date)));
        restrictions.addCriterion(Restrictions.le("firstSubmissionDate", DateFilterUtil.getEndDate(date)));
        if (finProduct != null)
            restrictions.addCriterion(Restrictions.eq("financialProduct.id", finProduct.getId()));
        return quotationService.list(restrictions);
    }

    // GET CONTRACT / HOURS
    private List<Quotation> getContractInHours(Dealer dealer, Date date, FinProduct finProduct) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, dealer.getId()));
        restrictions.addCriterion(Restrictions.eq(WKF_STATUS, QuotationWkfStatus.ACT));
        restrictions.addCriterion(Restrictions.ge("contractStartDate", DateFilterUtil.getStartDate(date)));
        restrictions.addCriterion(Restrictions.le("contractStartDate", DateFilterUtil.getEndDate(date)));
        if (finProduct != null)
            restrictions.addCriterion(Restrictions.eq("financialProduct.id", finProduct.getId()));
        return quotationService.list(restrictions);
    }


    /**
     * @param strValue
     * @return
     */
    private String getDefaultString(String strValue) {
        if (strValue == null) {
            return "";
        } else {
            return strValue;
        }
    }


    protected Cell createCell(final Row row, final int iCol, final String value, final CellStyle style) {
        final Cell cell = row.createCell(iCol);
        cell.setCellValue((value == null ? "" : value));
        cell.setCellStyle(style);
        return cell;
    }

    protected Cell createCell(final Row row, final int iCol,
                              final String value, final int fontsize, final boolean isBold,
                              final boolean hasBorder, final boolean leftRight, final short alignment,
                              final boolean setBgColor, final short bgColor, final short fonCorlor, boolean wrapText) {

        final Cell cell = row.createCell(iCol);
        final Font itemFont = wb.createFont();
        itemFont.setFontHeightInPoints((short) fontsize);
        if (isBold) {
            itemFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
        }
        itemFont.setFontName("Khmer OS Battambang");

        final CellStyle style = wb.createCellStyle();
        style.setAlignment(alignment);
        style.setFont(itemFont);
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BIG_SPOTS);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setLocked(true);
        itemFont.setColor(fonCorlor);
        style.setFont(itemFont);


        if (hasBorder) {
            style.setBorderTop(CellStyle.BORDER_THIN);
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setBorderBottom(CellStyle.BORDER_THIN);
            style.setBottomBorderColor(FC_BLACK);
        }
        if (leftRight) {
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setBorderRight(CellStyle.BORDER_THIN);
        }
        if (setBgColor) {
            style.setFillForegroundColor(bgColor);
            style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        }
        if (wrapText) {
            style.setWrapText(wrapText);
        }
        cell.setCellValue((value == null ? "" : value));
        cell.setCellStyle(style);
        return cell;
    }

    protected Cell createCellBorderBottomAndRight(final Row row, final int iCol,
                                                  final String value, final int fontsize, final boolean isBold,
                                                  final boolean hasBorder, final boolean leftRight, final short alignment,
                                                  final boolean setBgColor, final short bgColor, final short fonCorlor, boolean wrapText) {

        final Cell cell = row.createCell(iCol);
        final Font itemFont = wb.createFont();
        itemFont.setFontHeightInPoints((short) fontsize);
        if (isBold) {
            itemFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
        }
        itemFont.setFontName("Khmer OS Battambang");

        final CellStyle style = wb.createCellStyle();
        style.setAlignment(alignment);
        style.setFont(itemFont);
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BIG_SPOTS);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setLocked(true);
        itemFont.setColor(fonCorlor);
        style.setFont(itemFont);
        if (hasBorder) {
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setBorderBottom(CellStyle.BORDER_THIN);
            style.setBottomBorderColor(FC_BLACK);
        }
        if (leftRight) {
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setBorderRight(CellStyle.BORDER_THIN);
        }
        if (setBgColor) {
            style.setFillForegroundColor(bgColor);
            style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        }
        if (wrapText) {
            style.setWrapText(wrapText);
        }
        cell.setCellValue((value == null ? "" : value));
        cell.setCellStyle(style);
        return cell;
    }

    private Cell createNumericCell(final Row row, final int iCol,
                                   final Object value, final boolean hasBorder, final short alignment,
                                   final int fontsize, final boolean isBold, final boolean setBgColor,
                                   final short bgColor, final short fontColor) {

        final Cell cell = row.createCell(iCol);
        CellStyle style;
        final Font itemFont = wb.createFont();
        DataFormat format = wb.createDataFormat();
        style = wb.createCellStyle();
        style.setDataFormat(format.getFormat("0"));
        itemFont.setFontHeightInPoints((short) fontsize);

        if (value == null) {
            cell.setCellValue("");
        } else if (value instanceof Integer) {
            cell.setCellValue(Integer.valueOf(value.toString()));
            cell.setCellStyle(style);
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
        } else if (value instanceof Long) {
            cell.setCellValue(Long.valueOf(value.toString()));
            cell.setCellStyle(style);
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
        } else if (value instanceof String) {
            cell.setCellValue(value.toString());
        } else if (value instanceof Double) {
            cell.setCellValue((Double) value);
            cell.setCellStyle(styles.get(AMOUNT));
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
        } else {
            cell.setCellStyle(styles.get(BODY));
        }
        return cell;
    }

    public String getDateLabel(final Date date, final String formatPattern) {
        if (date != null && formatPattern != null) {
            return DateFormatUtils.format(date, formatPattern);
        }
        return null;
    }

    private Map<String, CellStyle> createStyles() {
        CellStyle style;
        style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BORDER_DOUBLE);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderLeft(CellStyle.BORDER_MEDIUM);
        style.setBorderTop(CellStyle.BORDER_MEDIUM);
        style.setLocked(true);
        styles.put(TOP_LEFT_BORDER, style);

        style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BORDER_DOUBLE);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderTop(CellStyle.BORDER_MEDIUM);
        style.setLocked(true);
        styles.put(TOP_BORDER, style);

        style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BORDER_DOUBLE);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderLeft(CellStyle.BORDER_MEDIUM);
        style.setLocked(true);
        styles.put(LEFT_BORDER, style);

        style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BORDER_DOUBLE);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderRight(CellStyle.BORDER_MEDIUM);
        style.setLocked(true);
        styles.put(RIGHT_BORDER, style);

        style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BORDER_DOUBLE);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderBottom(CellStyle.BORDER_MEDIUM);
        style.setLocked(true);
        styles.put(BUTTOM_BORDER, style);

        style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BORDER_DOUBLE);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderRight(CellStyle.BORDER_MEDIUM);
        style.setBorderTop(CellStyle.BORDER_MEDIUM);
        style.setLocked(true);
        styles.put(TOP_RIGHT_BORDER, style);

        style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BORDER_DOUBLE);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderRight(CellStyle.BORDER_MEDIUM);
        style.setBorderBottom(CellStyle.BORDER_MEDIUM);
        style.setLocked(true);
        styles.put(BUTTOM_RIGHT_BORDER, style);

        style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.BORDER_DOUBLE);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderLeft(CellStyle.BORDER_MEDIUM);
        style.setBorderBottom(CellStyle.BORDER_MEDIUM);
        style.setLocked(true);
        styles.put(BUTTOM_LEFT_BORDER, style);

        style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.GREEN.getIndex());
        style.setFillPattern(HSSFCellStyle.BORDER_DOUBLE);
        style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setLocked(true);
        styles.put(HEADER, style);

        final Font itemFont = wb.createFont();
        itemFont.setFontHeightInPoints((short) 14);
        itemFont.setFontName("Khmer OS Battambang");
        style = wb.createCellStyle();
        style.setFont(itemFont);
        styles.put(BODY, style);

        DataFormat format = wb.createDataFormat();
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(itemFont);
        style.setDataFormat(format.getFormat("#,##0.00"));
        styles.put(AMOUNT, style);

        return styles;
    }

    protected CellStyle getCellStyle(final String styleName) {
        return styles.get(styleName);
    }
}
