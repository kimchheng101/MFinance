package com.soma.mfinance.gui.ui.panel.report.installment.mfp;

import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.payment.model.Payment;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox.ButtonType;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import org.seuksa.frmk.i18n.I18N;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;
import java.util.List;
/**
 * 
 * @author vi.sok
 * @since 05/05/2017
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(InstallmentPaymentPanel.NAME)
public class InstallmentPaymentPanel extends AbstractTabPanel implements View, FinServicesHelper {
	
	private static final long serialVersionUID = 6227740006388204118L;

	protected Logger logger = LoggerFactory.getLogger(getClass());
	public static final String NAME = "installment.payment";
	private TabSheet tabSheet;
	private SimplePagedTable<Payment> pagedTable;
	private List<ColumnDefinition> columnDefinitions;
	private InstallmentPaymentSearchPanel installmentSearchPanel;
	private InstallmentPaymentTable installmentTable;
	private List<Payment> payments;
	private Button btnSearch;
	private Button btnSave;
	private TextField txtTotalAmount;
	@PostConstruct
	public void PostConstruct() {
	}
	@Override
	public void enter(ViewChangeEvent event) {
		//loadData();
	}

	@Override
	protected com.vaadin.ui.Component createForm() {
		installmentSearchPanel = new InstallmentPaymentSearchPanel();
		tabSheet = new TabSheet();
		VerticalLayout contentLayout = new VerticalLayout();
		contentLayout.setSpacing(true);
		contentLayout.setMargin(true);
		
		VerticalLayout gridLayoutPanel = new VerticalLayout();
		VerticalLayout searchLayout = new VerticalLayout();
		HorizontalLayout buttonsLayout = new HorizontalLayout();
		btnSearch = new Button(I18N.message("search"));
		btnSearch.setClickShortcut(KeyCode.ENTER, null); // null it means we don't modify key of shortcut Enter(default = 13)
		btnSearch.setIcon(new ThemeResource("../smt-default/icons/16/search.png"));
		btnSearch.addClickListener(new ClickListener() {		
			private static final long serialVersionUID = -3403059921454308342L;
			@Override
			public void buttonClick(ClickEvent event) {
				loadData();
			}
		});
		
		Button btnReset = new Button(I18N.message("reset"));
		btnReset.setIcon(new ThemeResource("../smt-default/icons/16/reset.png"));
		btnReset.addClickListener(new ClickListener() {
			private static final long serialVersionUID = -7165734546798826698L;
			@Override
			public void buttonClick(ClickEvent event) {
				reset();
			}
		});
		buttonsLayout.setSpacing(true);
		buttonsLayout.setStyleName("panel-search-center");
		buttonsLayout.addComponent(btnSearch);
		buttonsLayout.addComponent(btnReset);
        gridLayoutPanel.addComponent(installmentSearchPanel.getSearchForm());
        searchLayout.setMargin(true);
        searchLayout.setSpacing(true);
        searchLayout.addComponent(gridLayoutPanel);
        searchLayout.addComponent(buttonsLayout);
        
        Panel searchPanel = new Panel();
        searchPanel.setCaption(I18N.message("search"));
        searchPanel.setContent(searchLayout);
        
		txtTotalAmount = ComponentFactory.getNumberField("", false, 100, 100);
		txtTotalAmount.setEnabled(false);
		installmentTable = new InstallmentPaymentTable(btnSearch,txtTotalAmount);
        this.columnDefinitions = installmentTable.getHeader();
        pagedTable = new SimplePagedTable<Payment>(this.columnDefinitions);
        installmentTable.setPagedTable(pagedTable);
        contentLayout.addComponent(searchPanel);
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSpacing(true);
		btnSave = ComponentFactory.getButton("save");
		btnSave.setIcon(new ThemeResource("../smt-default/icons/16/save.png"));
		btnSave.addClickListener(getButtonClickListener());
		horizontalLayout.addComponent(btnSave);
		horizontalLayout.addComponent(ComponentFactory.getLabel("total.amount"));
		horizontalLayout.addComponent(txtTotalAmount);
		contentLayout.addComponent(horizontalLayout);
		contentLayout.setComponentAlignment(horizontalLayout, Alignment.MIDDLE_RIGHT);
        contentLayout.addComponent(pagedTable);
        contentLayout.addComponent(pagedTable.createControls());
        
        tabSheet.addTab(contentLayout, I18N.message("installment.payments"));
		
        return tabSheet;
	}
	/**
	 * 
	 */
	public void loadData(){
		payments = installmentSearchPanel.getPayments();
		installmentTable.getData(payments);
	}
	/**
	 * 
	 * @return
	 */
	private ClickListener getButtonClickListener() {
		return new ClickListener() {
			private static final long serialVersionUID = 7782893276902343244L;
			@Override
			public void buttonClick(ClickEvent event) {
				List<Long> paymentIds = installmentTable.getPaymentIds();
				if (paymentIds != null && !paymentIds.isEmpty()) {
					for (Long paymentId : paymentIds) {
						Payment payment = PAYMENT_SERVICE_MFP.getById(Payment.class, paymentId);
						PAYMENT_SERVICE_MFP.cancelInstallment(payment);
					}
					MessageBox mb = new MessageBox(UI.getCurrent(), "400px", "160px", I18N.message("information"),
							MessageBox.Icon.INFO, I18N.message("save.successfully"), Alignment.MIDDLE_RIGHT,
							new MessageBox.ButtonConfig(ButtonType.OK, I18N.message("ok")));
					mb.show();
					loadData();
				}
			}
		};
	}
	
}
