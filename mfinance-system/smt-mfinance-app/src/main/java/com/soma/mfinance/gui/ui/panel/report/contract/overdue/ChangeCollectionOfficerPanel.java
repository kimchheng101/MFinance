package com.soma.mfinance.gui.ui.panel.report.contract.overdue;

import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.shared.system.FMProfile;
import com.soma.mfinance.core.widget.SecUserComboBox;
import com.soma.frmk.helper.FrmkServicesHelper;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.soma.frmk.vaadin.ui.widget.toolbar.event.SaveClickListener;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.springframework.util.Assert;

/**
 * Change the collection officer of the contract
 * @author s.torn
 */
public class ChangeCollectionOfficerPanel extends AbstractTabPanel implements SaveClickListener, FinServicesHelper {

    /** */
    private static final long serialVersionUID = -397606368573263057L;
    private SecUserComboBox cbxCollectionOfficer;
    private NavigationPanel navigationPanel;
    private Collection collection;

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTabPanel#createForm()
     */
    @Override
    protected Component createForm() {
        navigationPanel = new NavigationPanel();
        navigationPanel.addSaveClickListener(this);
        cbxCollectionOfficer = new SecUserComboBox(DataReference.getInstance().getUsers(FMProfile.CC, EStatusRecord.ACTIV));
        cbxCollectionOfficer.setCaption(I18N.message("collection.officer"));

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSpacing(true);

        verticalLayout.addComponent(navigationPanel);
        verticalLayout.addComponent(cbxCollectionOfficer);

        return verticalLayout;
    }

    /**
     * @param collection
     */
    public void assignValues(Collection collection) {
        this.collection = collection;
        if (this.collection != null) {
            cbxCollectionOfficer.setSelectedEntity(collection.getCollectionOfficer());
        }
    }

    /**
     * @see com.soma.frmk.vaadin.ui.widget.toolbar.event.SaveClickListener#saveButtonClick(com.vaadin.ui.Button.ClickEvent)
     */
    @Override
    public void saveButtonClick(ClickEvent arg0) {
        Assert.notNull(collection,"Collection object could not be null");
        SecUser secUser = cbxCollectionOfficer.getSelectedEntity();
        // Collection collectionToUpdate  = COL_SRV.getById(Collection.class,collection.getId());
        collection.setCollectionOfficer(secUser);
        ENTITY_SRV.saveOrUpdate(collection);
    }
}
