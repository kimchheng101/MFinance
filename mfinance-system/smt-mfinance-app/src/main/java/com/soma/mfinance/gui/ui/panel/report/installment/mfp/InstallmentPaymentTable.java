package com.soma.mfinance.gui.ui.panel.report.installment.mfp;

import com.soma.mfinance.core.accounting.InstallmentVO;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowType;
import com.soma.mfinance.core.entityfield.InstallmentEntityField;
import com.soma.mfinance.core.financial.model.EServiceType;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.payment.model.Payment;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.frmk.security.context.SecApplicationContextHolder;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox.ButtonType;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.*;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.amount.Amount;
import org.seuksa.frmk.tools.amount.AmountUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author vi.sok
 *
 */
public class InstallmentPaymentTable implements InstallmentEntityField, FinServicesHelper {
	private List<ColumnDefinition> columnDefinitions;
	private SimplePagedTable<Payment> pagedTable;
	private Button btnSearch;
	private TextField txtTotalAmount;
	private double totalAmount;
	private List<Long> paymentIds;
	private MessageBox mb;
	public InstallmentPaymentTable(Button btnSearch, TextField txtTotalAmount){
		this.btnSearch = btnSearch;
		this.txtTotalAmount = txtTotalAmount;
		paymentIds = new ArrayList<Long>();
	}
	public List<ColumnDefinition> getHeader(){
		columnDefinitions = new ArrayList<ColumnDefinition>();
		columnDefinitions.add(new ColumnDefinition("check", I18N.message("check"), CheckBox.class, Align.CENTER, 45));
		columnDefinitions.add(new ColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 140, false));
		columnDefinitions.add(new ColumnDefinition(CONTRACT, I18N.message("contract"), String.class, Align.LEFT, 140));
		columnDefinitions.add(new ColumnDefinition(CUSTOMER, I18N.message("customer"), String.class, Align.LEFT, 140));
		columnDefinitions.add(new ColumnDefinition(DEALER + "." + NAME_EN, I18N.message("dealer"), String.class, Align.LEFT, 140));
		columnDefinitions.add(new ColumnDefinition("motor.model", I18N.message("motor.model"), String.class, Align.LEFT, 100));
		columnDefinitions.add(new ColumnDefinition(TERM, I18N.message("term"), Integer.class, Align.CENTER, 60));
		columnDefinitions.add(new ColumnDefinition(CONTRACT_STATUS, I18N.message("contract.status"), String.class, Align.LEFT, 110));
		columnDefinitions.add(new ColumnDefinition(DUE_DATE, I18N.message("due.date"), Date.class, Align.LEFT, 110));
		columnDefinitions.add(new ColumnDefinition(NUM_INSTALLMENT, I18N.message("No"), Integer.class, Align.CENTER, 60));
		columnDefinitions.add(new ColumnDefinition(PAYMENT_METHOD, I18N.message("payment.method"), String.class, Align.CENTER, 60));
		columnDefinitions.add(new ColumnDefinition(PAYMENT_DATE, I18N.message("payment.date"), Date.class, Align.CENTER, 70));
		columnDefinitions.add(new ColumnDefinition(RECEIVER, I18N.message("receiver"), String.class, Align.CENTER, 70));
		columnDefinitions.add(new ColumnDefinition(INSTALLMENT_AMOUNT, I18N.message("installment.amount"), Amount.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(PRINCIPAL_AMOUNT, I18N.message("principal.amount"), Amount.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(INTEREST_AMOUNT, I18N.message("interest.amount"), Amount.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(INSURANCE_FEE, I18N.message("insurance.fee"), Amount.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(SERVICING_FEE, I18N.message("servicing.fee"), Amount.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(TRANSFER_FEE, I18N.message("transfer.fee"), String.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(TOTAL_FEE_VAT, I18N.message("total.fee.vat"), String.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(COMMISSION, I18N.message("commission"), Amount.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(OTHER_AMOUNT, I18N.message("other.amount"), Amount.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(NUM_PENALTY_DAY, I18N.message("no.penalty.days"), Integer.class, Align.LEFT, 40));
		columnDefinitions.add(new ColumnDefinition(PENALTY_AMOUNT, I18N.message("penalty.amount"), Amount.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(TOTAL_PAYMENT, I18N.message("total.payment"), Amount.class, Align.RIGHT, 100));
		return columnDefinitions;
	}

	/**
	 *
	 * @param payments
	 */
	public void getData(List<Payment> payments){
		paymentIds.clear();
		Indexed indexedContainer = pagedTable.getContainerDataSource();
		indexedContainer.removeAllItems();
		eventTable();
		int index = 0;
		totalAmount = 0d;
		if (payments != null && !payments.isEmpty()) {
			for (Payment payment : payments) {
				Contract contract  = payment.getContract();
				Applicant applicant = contract.getApplicant();
				List<Cashflow> cashflows = payment.getCashflows();
				double installmentAmount = 0d;
				double tiPrincipal = 0d;
				double tiInterestRate = 0d;
				double tiInsurance = 0d;
				double tiServiceFee = 0d;
				double tiTransFee = 0d;
				double otherAmount = 0d;
				double totalInstallment =0d;
				double penaltyAmount = 0d;
				double totalFeeVat = 0d;//,vatIns = 0d, vatService = 0d, otherVat = 0d;
				double vatPenalty=0d;
				double commission=0d;
				List<InstallmentVO> installmentByNumInstallments = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(cashflows);
				InstallmentVO installmentVOFirstIndex = installmentByNumInstallments.get(0);
				for (InstallmentVO installmentVO : installmentByNumInstallments) {
					if(!ECashflowType.PEN.getCode().equals(installmentVO.getCashflowType().getCode())){
						installmentAmount += installmentVO.getTiamount() + installmentVO.getVatAmount();
					}
					if (ECashflowType.CAP.getCode().equals(installmentVO.getCashflowType().getCode())) {
						tiPrincipal = installmentVO.getTiamount();
						totalFeeVat+= installmentVO.getVatAmount();
					} else if(ECashflowType.IAP.getCode().equals(installmentVO.getCashflowType().getCode())) {
						tiInterestRate = installmentVO.getTiamount();
						totalFeeVat+= installmentVO.getVatAmount();
					} else if(installmentVO.getService() != null && EServiceType.INSFEE.getCode().equals(installmentVO.getService().getCode())){
						tiInsurance = installmentVO.getTiamount();
						totalFeeVat+=installmentVO.getVatAmount();
					} else if(installmentVO.getService() != null
							&& (EServiceType.SRVFEE.getCode().equals(installmentVO.getService().getCode())
								|| EServiceType.SRVFEEEXT.getCode().equals(installmentVO.getService().getCode()))){
						tiServiceFee = installmentVO.getTiamount();
						totalFeeVat+=installmentVO.getVatAmount();
					} else if(installmentVO.getService() != null && EServiceType.TRANSFEE.getCode().equals(installmentVO.getService().getCode())) {
						tiTransFee = installmentVO.getTiamount();
						totalFeeVat+=installmentVO.getVatAmount();
					}else if(ECashflowType.PEN.getCode().equals(installmentVO.getCashflowType().getCode())){
						penaltyAmount+= installmentVO.getTiamount();
						vatPenalty+=installmentVO.getVatAmount();
						totalFeeVat+=installmentVO.getVatAmount();
					}else if(installmentVO.getService()!=null &&EServiceType.COMM.getCode().equals(installmentVO.getService().getServiceType().getCode())){
						commission += installmentVO.getTiamount();
						totalFeeVat+=installmentVO.getVatAmount();
					}else {
						otherAmount += installmentVO.getTiamount();
						totalFeeVat+=installmentVO.getVatAmount();
					}
				}
				totalInstallment = installmentAmount +  (penaltyAmount + vatPenalty);
				final Item item = indexedContainer.addItem(index);
				final CheckBox check = new CheckBox();
				check.setValue(true);
				check.setImmediate(true);
				check.addValueChangeListener(new ValueChangeListener() {
					@Override
					public void valueChange(ValueChangeEvent event) {
						Amount totalInstallment = (Amount) item.getItemProperty(TOTAL_PAYMENT).getValue();
						long paymentId = (long) item.getItemProperty(ID).getValue();
						Payment payment = PAYMENT_SERVICE_MFP.getById(Payment.class, paymentId);
						if(mb != null){
							mb.close();
						}
						if (payment.getContract().getWkfStatus() != ContractWkfStatus.FIN) {
							check.setValue(true);
							if(mb != null){
								mb.close();
							}
							mb = new MessageBox(UI.getCurrent(), "400px", "160px", I18N.message("information"),
									MessageBox.Icon.INFO, I18N.message("this.contract.status.is.not.activated"), Alignment.MIDDLE_RIGHT,
									new MessageBox.ButtonConfig(ButtonType.OK, I18N.message("ok")));
							mb.show();
						} else if(payment.getContract().getLastPaidNumInstallment() > payment.getCashflows().get(0).getNumInstallment()){
							mb = new MessageBox(UI.getCurrent(), "400px", "160px", I18N.message("information"),
									MessageBox.Icon.INFO, I18N.message("please.cancel.by.order.installment"), Alignment.MIDDLE_RIGHT,
									new MessageBox.ButtonConfig(ButtonType.OK, I18N.message("ok")));
							mb.show();
							check.setValue(true);
						} else {
							if (check.getValue()) {
								totalAmount += totalInstallment.getTiAmount();
								paymentIds.remove(paymentId);
							} else {
								totalAmount -= totalInstallment.getTiAmount();
								paymentIds.add(paymentId);
							}
							txtTotalAmount.setValue(AmountUtils.format(totalAmount));
						}
					}
				});
				item.getItemProperty("check").setValue(check);
				item.getItemProperty(ID).setValue(payment.getId());
				item.getItemProperty(CONTRACT).setValue(contract.getReference());
				item.getItemProperty(CUSTOMER).setValue(applicant.getLastNameEn() +" "+applicant.getFirstNameEn());
				item.getItemProperty(DEALER + "." + NAME_EN).setValue(payment.getDealer()!=null?payment.getDealer().getNameEn():"");
				item.getItemProperty("motor.model").setValue(contract.getAsset().getModel().getDesc());
				item.getItemProperty(TERM).setValue(contract.getTerm());
				item.getItemProperty(CONTRACT_STATUS).setValue(contract.getWkfStatus().getDescEn());
				item.getItemProperty(DUE_DATE).setValue(installmentVOFirstIndex.getInstallmentDate());
				item.getItemProperty(NUM_INSTALLMENT).setValue(installmentVOFirstIndex.getNumInstallment());
				item.getItemProperty(PAYMENT_METHOD).setValue(payment.getPaymentMethod().getDescEn());
				item.getItemProperty(PAYMENT_DATE).setValue(payment.getPaymentDate());
				item.getItemProperty(RECEIVER).setValue(payment.getReceivedUser().getDesc());
				item.getItemProperty(INSTALLMENT_AMOUNT).setValue(new Amount(installmentAmount, 0d, installmentAmount));
				item.getItemProperty(PRINCIPAL_AMOUNT).setValue(new Amount(tiPrincipal, 0d, tiPrincipal));
				item.getItemProperty(INTEREST_AMOUNT).setValue(new Amount(tiInterestRate, 0d, tiInterestRate));
				item.getItemProperty(INSURANCE_FEE).setValue(new Amount(tiInsurance, 0d, tiInsurance));
				item.getItemProperty(SERVICING_FEE).setValue(new Amount(tiServiceFee, 0d, tiServiceFee));
				item.getItemProperty(TRANSFER_FEE).setValue(AmountUtils.format(tiTransFee));
				item.getItemProperty(TOTAL_FEE_VAT).setValue(AmountUtils.format(totalFeeVat));
				item.getItemProperty(COMMISSION).setValue(new Amount(commission, 0d, commission));
				item.getItemProperty(OTHER_AMOUNT).setValue(new Amount(otherAmount, 0d, otherAmount));
				item.getItemProperty(NUM_PENALTY_DAY).setValue(getInteger(payment.getNumPenaltyDays()));
				item.getItemProperty(PENALTY_AMOUNT).setValue(new Amount(penaltyAmount, 0d, penaltyAmount));
				item.getItemProperty(TOTAL_PAYMENT).setValue(new Amount(totalInstallment, 0d, totalInstallment));
				index++;
				totalAmount += totalInstallment;

			}
		}
		txtTotalAmount.setValue(AmountUtils.format(totalAmount));
		pagedTable.refreshContainerDataSource();

	}
	/**
	 *
	 * @param intValue
	 * @return
	 */
	private int getInteger(Integer intValue){
		if(intValue == null){
			return 0;
		} else {
			return intValue;
		}
	}
	/**
	 *
	 */
	private void eventTable() {
		pagedTable.addItemClickListener(new ItemClickListener() {
			private static final long serialVersionUID = -6676228064499031341L;

			@Override
			public void itemClick(ItemClickEvent event) {
				boolean isDoubleClick = event.isDoubleClick() || SecApplicationContextHolder.getContext().clientDeviceIsMobileOrTablet();
				if (isDoubleClick) {
					Item item = event.getItem();
					Long paymentId = (Long) item.getItemProperty(ID).getValue();
					Integer numInstallment = (Integer) item.getItemProperty(NUM_INSTALLMENT).getValue();
				}
			}
		});
	}
	/**
	 * @return the pagedTable
	 */
	public SimplePagedTable<Payment> getPagedTable() {
		return pagedTable;
	}
	/**
	 * @param pagedTable the pagedTable to set
	 */
	public void setPagedTable(SimplePagedTable<Payment> pagedTable) {
		this.pagedTable = pagedTable;
	}
	/**
	 * @return the paymentIds
	 */
	public List<Long> getPaymentIds() {
		return paymentIds;
	}
	/**
	 * @param paymentIds the paymentIds to set
	 */
	public void setPaymentIds(List<Long> paymentIds) {
		this.paymentIds = paymentIds;
	}

}
