package com.soma.mfinance.gui.ui.panel.report.uwdeclinereport;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.service.EntityService;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;

import com.soma.common.app.workflow.model.EntityWkf;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.SupportDecision;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;

/**
 * @modified p.ly
 * @author kimsuor.seang
 */
public class UWDeclineReportSearchPanel extends AbstractSearchPanel<Quotation> implements FMEntityField {

	/** */
	private static final long serialVersionUID = -2663311603012223797L;
	private EntityService entityService = SpringUtils.getBean(EntityService.class);
	
	private ERefDataComboBox<EDealerType> cbxDealerType;
	private DealerComboBox cbxDealer;
	private ComboBox cbxDeclineTermFrom;
	private ComboBox cbxDeclineTermTo;
	private ComboBox cbxDeclineReson;
	private ComboBox cbxFieldCheck;
	private ComboBox cbxLeaseAmountPercentage;
	private ComboBox cbxTerm;
	private TextField txtId;
	private AutoDateField dfStartDate;
	private AutoDateField dfEndDate;
	private AutoDateField dfSubmitDate;

	/**
	 * @param tablePanel
	 */
	public UWDeclineReportSearchPanel(UWDeclineReportTablePanel tablePanel) {
		super(I18N.message("search"), tablePanel);
	}

	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#createForm()
	 */
	@Override
	protected Component createForm() {

		cbxDeclineTermFrom = new ComboBox(I18N.message("decline.term.from"));
		cbxDeclineTermFrom.setWidth("150px");
		cbxDeclineTermFrom.addItem(12);
		cbxDeclineTermFrom.setItemCaption(12, I18N.message("12month"));
		cbxDeclineTermFrom.addItem(24);
		cbxDeclineTermFrom.setItemCaption(24, I18N.message("24month"));
		cbxDeclineTermFrom.addItem(36);
		cbxDeclineTermFrom.setItemCaption(36, I18N.message("36month"));

		cbxDeclineTermTo = new ComboBox(I18N.message("decline.term.to"));
		cbxDeclineTermTo.setWidth("150px");
		cbxDeclineTermTo.addItem(12);
		cbxDeclineTermTo.setItemCaption(12, I18N.message("12month"));
		cbxDeclineTermTo.addItem(24);
		cbxDeclineTermTo.setItemCaption(24, I18N.message("24month"));
		cbxDeclineTermTo.addItem(36);
		cbxDeclineTermTo.setItemCaption(36, I18N.message("36month"));

		txtId = new TextField(I18N.message("ID"));
		
		cbxDealer = new DealerComboBox(null, ENTITY_SRV.list(Dealer.class), I18N.message("all"));
		cbxDealer.setSelectedEntity(null);
		cbxDealer.setCaption(I18N.message("dealer"));
		cbxDealer.setWidth("220px");
		cbxDealerType = new ERefDataComboBox<EDealerType>(EDealerType.values());
		cbxDealerType.setCaption(I18N.message("dealer.type"));
		cbxDealerType.setImmediate(true);
		cbxDealerType.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = -1121619816068986468L;
			@Override
			public void valueChange(ValueChangeEvent event) {
				BaseRestrictions<Dealer> restrictions = new BaseRestrictions<Dealer>(Dealer.class);
				restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
				if (cbxDealerType.getSelectedEntity() != null) {
					restrictions.addCriterion(Restrictions.eq("dealerType", cbxDealerType.getSelectedEntity()));
				}
				cbxDealer.setDealers(entityService.list(restrictions));
				cbxDealer.setSelectedEntity(null);
			}
		});
		
		cbxDeclineReson = new ComboBox(I18N.message("decline.reasons"));
		cbxDeclineReson.setImmediate(true);
		cbxDeclineReson.setWidth("220px");
		cbxDeclineReson.setItemCaption(null, I18N.message("all"));
		List<SupportDecision> supportDecisions = DataReference.getInstance().getSupportDecisions(QuotationWkfStatus.DEC);
		for (SupportDecision supportDecision : supportDecisions) {
			cbxDeclineReson.addItem(supportDecision);
			cbxDeclineReson.setItemCaption(supportDecision, supportDecision.getDescEn());
		}
		
		cbxFieldCheck = new ComboBox(I18N.message("field.check"));
		cbxFieldCheck.setImmediate(true);
		cbxFieldCheck.setItemCaption(true, I18N.message("yes"));
		cbxFieldCheck.addItem(true);
		cbxFieldCheck.setItemCaption(false, I18N.message("no"));
		cbxFieldCheck.addItem(false);

		cbxLeaseAmountPercentage = new ComboBox(I18N.message("lease.amount.percentage"));
		cbxLeaseAmountPercentage.setImmediate(true);
		cbxLeaseAmountPercentage.setItemCaption(null, I18N.message("all"));
		cbxLeaseAmountPercentage.addItem(100.0);
		cbxLeaseAmountPercentage.setItemCaption(100.0, I18N.message("100%"));
		cbxLeaseAmountPercentage.addItem(90.0);
		cbxLeaseAmountPercentage.setItemCaption(90.0, I18N.message("90%"));
		cbxLeaseAmountPercentage.addItem(80.0);
		cbxLeaseAmountPercentage.setItemCaption(80.0, I18N.message("80%"));
		cbxLeaseAmountPercentage.addItem(70.0);
		cbxLeaseAmountPercentage.setItemCaption(70.0, I18N.message("70%"));
		cbxLeaseAmountPercentage.addItem(60.0);
		cbxLeaseAmountPercentage.setItemCaption(60.0, I18N.message("60%"));

		cbxTerm = new ComboBox(I18N.message("terms"));
		cbxTerm.setImmediate(true);
		cbxTerm.setItemCaption(null, I18N.message("all"));
		cbxTerm.addItem(12);
		cbxTerm.setItemCaption(12, I18N.message("12month"));
		cbxTerm.addItem(24);
		cbxTerm.setItemCaption(24, I18N.message("24month"));
		cbxTerm.addItem(36);
		cbxTerm.setItemCaption(36, I18N.message("36month"));

		dfStartDate = ComponentFactory.getAutoDateField("",false);
		dfStartDate.setValue(DateUtils.today());
		dfStartDate.setCaption(I18N.message("start.date"));
		
		dfEndDate = ComponentFactory.getAutoDateField("", false);    
		dfEndDate.setValue(DateUtils.today());
		dfEndDate.setCaption(I18N.message("end.date"));
		
		dfSubmitDate = ComponentFactory.getAutoDateField("", false);    
		dfSubmitDate.setCaption(I18N.message("submit.date"));
		
		//reset();
		final GridLayout gridLayout = new GridLayout(10, 3);
		gridLayout.setSpacing(true);
		
		FormLayout formLayoutLeft = new FormLayout();
			formLayoutLeft.addComponent(cbxDeclineTermFrom);
			formLayoutLeft.addComponent(cbxDealerType);
			formLayoutLeft.addComponent(cbxFieldCheck);
			formLayoutLeft.addComponent(dfStartDate);
		FormLayout formLayoutMiddle = new FormLayout();
			formLayoutMiddle.addComponent(cbxDeclineTermTo);
            formLayoutMiddle.addComponent(cbxDealer);
			formLayoutMiddle.addComponent(cbxLeaseAmountPercentage);
            formLayoutMiddle.addComponent(dfEndDate);
        FormLayout formLayoutRight = new FormLayout();
			formLayoutRight.addComponent(txtId);
			formLayoutRight.addComponent(cbxDeclineReson);
			formLayoutRight.addComponent(cbxTerm);
			formLayoutRight.addComponent(dfSubmitDate);
			
		int iCol = 0;
		gridLayout.addComponent(formLayoutLeft, iCol++, 0);
		gridLayout.addComponent(ComponentFactory.getSpaceLayout(20, Unit.PIXELS), iCol++, 0);
		gridLayout.addComponent(formLayoutMiddle, iCol++, 0);
		gridLayout.addComponent(ComponentFactory.getSpaceLayout(20, Unit.PIXELS), iCol++, 0);
		gridLayout.addComponent(formLayoutRight, iCol++, 0);
	  
		return gridLayout;
	}

	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#getRestrictions()
	 */
	@Override
	public BaseRestrictions<Quotation> getRestrictions() {
		BaseRestrictions<Quotation> restrictions = new BaseRestrictions<Quotation>(Quotation.class);
		restrictions.addCriterion(Restrictions.eq(EntityWkf.WKFSTATUS, QuotationWkfStatus.DEC));
		if(cbxDeclineTermFrom.getValue() != null){
			restrictions.addCriterion(Restrictions.ge("term", cbxDeclineTermFrom.getValue()));
		}
		if(cbxDeclineTermTo.getValue() != null){
			restrictions.addCriterion(Restrictions.le("term", cbxDeclineTermTo.getValue()));
		}
		if(txtId.getValue() != null && !txtId.getValue().isEmpty()){
			long searchID = Long.valueOf(txtId.getValue().replaceAll(",", "").toString());
			restrictions.addCriterion(Restrictions.eq("id", Long.valueOf(searchID)));
		}
		
		if (cbxDealerType.getSelectedEntity() != null) {
			restrictions.addAssociation(DEALER, "indeal", JoinType.INNER_JOIN);
			restrictions.addCriterion(Restrictions.eq("indeal.dealerType", cbxDealerType.getSelectedEntity()));
		}
		if (cbxDealer.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq(DEALER, cbxDealer.getSelectedEntity()));
		}
		if(cbxLeaseAmountPercentage.getValue() != null){
			restrictions.addCriterion(Restrictions.eq("leaseAmountPercentage", cbxLeaseAmountPercentage.getValue()));
		}
		if(cbxDeclineReson.getValue() != null){
			restrictions.addAssociation("quotationSupportDecisions", "quosudec", JoinType.INNER_JOIN);
			restrictions.addAssociation("quosudec.supportDecision", "sudec", JoinType.INNER_JOIN);
			restrictions.addCriterion("quosudec.supportDecision", (Serializable) cbxDeclineReson.getValue());
		}
		if(cbxTerm.getValue() != null){
			restrictions.addCriterion(Restrictions.eq("term", cbxTerm.getValue()));
		}
		if(cbxFieldCheck.getValue() != null ){
			restrictions.addCriterion(Restrictions.eq("fieldCheckPerformed", cbxFieldCheck.getValue()));
		}
		if(dfStartDate.getValue() != null){
			restrictions.addCriterion(Restrictions.ge("declineDate", DateUtils.getDateAtBeginningOfDay(dfStartDate.getValue())));
		}
		if(dfEndDate.getValue() != null){
			restrictions.addCriterion(Restrictions.le("declineDate", DateUtils.getDateAtEndOfDay(dfEndDate.getValue())));
		}
		if(dfSubmitDate.getValue() != null){
			restrictions.addCriterion(Restrictions.between("submissionDate", DateUtils.getDateAtBeginningOfDay(dfSubmitDate.getValue()), DateUtils.getDateAtEndOfDay(dfSubmitDate.getValue())));
		}
		
		return restrictions;
	}

	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#reset()
	 */
	@Override
	protected void reset() {
		cbxDealerType.setSelectedEntity(null);
		cbxDealer.setSelectedEntity(null);
		cbxDeclineTermFrom.setValue(null);
		cbxDeclineTermTo.setValue(null);
		txtId.setValue("");
		cbxDeclineReson.setValue(null);
		cbxFieldCheck.setValue(null);
		cbxTerm.setValue(null);
		cbxLeaseAmountPercentage.setValue(null);
	}
}
