package com.soma.mfinance.gui.ui.panel.contracts;

import com.soma.mfinance.core.contract.model.Contract;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.service.EntityService;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.security.context.SecApplicationContextHolder;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox.ButtonType;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

/**
 * Customer identity panel
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class OtherInfoPanel extends AbstractTabPanel implements FMEntityField {
	
	private static final long serialVersionUID = -3020972492621000892L;
	
	 protected EntityService entityService = (EntityService) SecApplicationContextHolder.getContext().getBean("entityService");

	private Panel contentPanel;
	private NavigationPanel navigationPanel;
	private VerticalLayout verticalLayout;
	private FormLayout formLayout;
	
	private Contract contract;
	//private EntityRefComboBox<ThirdPartyCompany> cbxThirdPartyCompany;
		
	public OtherInfoPanel() {
		super();
		setMargin(true);
		setSizeFull();
	}
	
	@Override
	protected com.vaadin.ui.Component createForm() {
		contentPanel = new Panel(I18N.message("other.info"));
		contentPanel.setSizeFull();
		navigationPanel = new NavigationPanel();
		Button btnSave = new NativeButton(I18N.message("save"), new Button.ClickListener() {
			
					private static final long serialVersionUID = 7148778578554352522L;

					public void buttonClick(ClickEvent event) {
						/*if (cbxThirdPartyCompany.getSelectedEntity() != null) {
							contract.setInsuranceCompany(cbxThirdPartyCompany.getSelectedEntity());
							entityService.saveOrUpdate(contract);
							Notification notification = new Notification("", Type.HUMANIZED_MESSAGE);
							notification.setDescription(I18N.message("save.successfully"));
							notification.setDelayMsec(3000);
							notification.show(Page.getCurrent());
						} else {

							MessageBox mb = new MessageBox(UI.getCurrent(), "320px", "140px", I18N.message("information"),
									MessageBox.Icon.ERROR, I18N.message("the.field.require.can't.null.or.empty"), Alignment.MIDDLE_RIGHT,
									new MessageBox.ButtonConfig(ButtonType.OK, I18N.message("ok")));
							mb.show();

						}*/
					}
				});
        btnSave.setIcon(new ThemeResource("../smt-default/icons/16/save.png"));
		navigationPanel.addButton(btnSave);

		// TODO: Contracts
		/*cbxThirdPartyCompany = new EntityRefComboBox<ThirdPartyCompany>(I18N.message("insurance.company"));
		cbxThirdPartyCompany.setRequired(true);
		BaseRestrictions<ThirdPartyCompany> restrictions = new BaseRestrictions<>(ThirdPartyCompany.class);
		restrictions.getStatusRecordList().add(StatusRecord.ACTIV);
		cbxThirdPartyCompany.setRestrictions(restrictions);
		cbxThirdPartyCompany.renderer();
		*/
		formLayout = new FormLayout();
		formLayout.setMargin(true);
		//formLayout.addComponent(cbxThirdPartyCompany);

		
		verticalLayout = new VerticalLayout();
		verticalLayout.setSizeFull();
		verticalLayout.addComponent(navigationPanel);
		verticalLayout.addComponent(formLayout);
		contentPanel.setContent(verticalLayout);
		addComponent(contentPanel);		
		return contentPanel;
	}

	
	/**
	 * Reset panel
	 */
	/*public void reset() {
		cbxThirdPartyCompany.setSelectedEntity(null);
	}*/
	/**
	 * 
	 * @param contract
	 */
	public void assignValues(Contract contract) {
		reset();
		this.contract = contract;
		//cbxThirdPartyCompany.setSelectedEntity(contract.getInsuranceCompany());
	}
}
