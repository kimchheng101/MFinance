package com.soma.mfinance.gui.ui.panel.insurance;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.EntityColumnRenderer;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.data.Item;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.Align;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class InsuranceTablePanel extends AbstractTablePanel<Quotation> implements QuotationEntityField{
	
	
	private static final long serialVersionUID = -8893480135582631824L;
	private static final String DEALER_TYPE = "dealerType";
	private InsuranceSearchPanel insuranceSearchPanel;
	
	@PostConstruct
	public void PostConstruct() {
		setCaption(I18N.message("insurance"));
		setSizeFull();
		setHeight("200%");
		setMargin(true);
		setSpacing(true);
				
		super.init(I18N.message("insurance"));
		
		NavigationPanel navigationPanel = addNavigationPanel();
		if (ProfileUtil.isDocumentController()) {
			navigationPanel.addAddClickListener(this);
		}
		navigationPanel.addEditClickListener(this);
		navigationPanel.addRefreshClickListener(this);

		getPagedTable().addStyleName("colortable");
		getPagedTable().setCellStyleGenerator(new Table.CellStyleGenerator() {
			private static final long serialVersionUID = 6242667432758981026L;
			@Override
			public String getStyle(Table source, Object itemId, Object propertyId) {
				if (propertyId == null) {
					Item item = source.getItem(itemId);
					Date contractDeadline = (Date) item.getItemProperty("contract.endDate").getValue();
					if(contractDeadline != null){
					Long diff = DateUtils.getDiffInDays(contractDeadline, DateUtils.today());
						if(diff <= 7){
							return "highligh-red";
						}
					}	
				}
				return null;
			}
		});
	}	

	/**
	 * Get Paged definition
	 * @return
	 */

	@Override
	protected PagedDataProvider<Quotation> createPagedDataProvider() {
		BaseRestrictions<Quotation> restrictions = new BaseRestrictions<Quotation>(Quotation.class);
		if(ProfileUtil.isDocumentController()) {
			// If the user is Document controller the fist loading is not search 
			restrictions.addCriterion(Restrictions.eq(ID, 0L));		
		} else {
			restrictions = searchPanel.getRestrictions();
		}
		PagedDefinition<Quotation> pagedDefinition = new PagedDefinition<Quotation>(restrictions);
		pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 60);
		pagedDefinition.addColumnDefinition(REFERENCE, I18N.message("reference"), String.class, Align.LEFT, 125);
		pagedDefinition.addColumnDefinition(CUSTOMER, I18N.message("customer"), String.class, Align.LEFT, 150, new CustomerFullNameColumnRenderer());
		pagedDefinition.addColumnDefinition(ASSET + ".model.descEn", I18N.message("asset"), String.class, Align.LEFT, 150);

		pagedDefinition.addColumnDefinition("insuranceStartDate", I18N.message("insurance.start.date"), Date.class, Align.LEFT, 150);
		pagedDefinition.addColumnDefinition(DEALER + "." + DEALER_TYPE + "." + DESC, I18N.message("dealer.type"), String.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition(DEALER + "." + NAME_EN, I18N.message("dealer"), String.class, Align.LEFT, 200);
		pagedDefinition.addColumnDefinition("insuranceStatus." + DESC_EN, I18N.message("insurance.status"), String.class, Align.LEFT, 150);
		//pagedDefinition.addColumnDefinition("insuranceDuration", I18N.message("insurance.year"), String.class, Align.LEFT, 150, new InsuranceDurationRenderer());
		pagedDefinition.addColumnDefinition(WKF_STATUS +"."+ DESC_EN, I18N.message("contract.status"), String.class, Align.LEFT, 140, new StatusColumnRenderer());
		pagedDefinition.addColumnDefinition(ASSET + "." + "chassisNumber", I18N.message("chassis.number"), String.class, Align.LEFT, 200);
		pagedDefinition.addColumnDefinition(ASSET + "." + "engineNumber", I18N.message("engine.number"), String.class, Align.LEFT, 200);
		pagedDefinition.addColumnDefinition(ASSET + "." + "plateNumber", I18N.message("plate.number"), String.class, Align.LEFT, 200);
		pagedDefinition.addColumnDefinition(FIRST_SUBMISSION_DATE, I18N.message("first.submission.date"), Date.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition(ACCEPTATION_DATE, I18N.message("acceptation.date"), Date.class, Align.LEFT, 80);
		pagedDefinition.addColumnDefinition(CONTRACT_START_DATE, I18N.message("contract.date"), Date.class, Align.LEFT, 80);
		pagedDefinition.addColumnDefinition("contract.endDate", I18N.message("end.date"), Date.class, Align.LEFT, 80,false);
		//pagedDefinition.addColumnDefinition("insuranceEndYear",I18N.message("insurance.ongoing.year"), Date.class, Align.LEFT, 200);
		EntityPagedDataProvider<Quotation> pagedDataProvider = new EntityPagedDataProvider<Quotation>();
		pagedDataProvider.setPagedDefinition(pagedDefinition);
		return pagedDataProvider;
	}
		
	/**
	 * @see AbstractTablePanel#getEntity()
	 */	
	@Override
	protected Quotation getEntity() {
		final Long id = getItemSelectedId();
		if (id != null) {
		    return ENTITY_SRV.getById(Quotation.class, id);
		}
		return null;
	}
	
	@Override
	protected InsuranceSearchPanel createSearchPanel() {
		insuranceSearchPanel = new InsuranceSearchPanel(this); 
		return 	insuranceSearchPanel;	
	}
	
	private class StatusColumnRenderer extends EntityColumnRenderer {
		
		@Override
		public Object getValue() {
			EWkfStatus quotationStatus = ((Quotation) getEntity()).getWkfStatus();
			Contract contractStatus = ((Quotation) getEntity()).getContract();
			
			if(contractStatus == null){
				return quotationStatus.getDescEn();
			}else if(contractStatus.getWkfStatus().equals(ContractWkfStatus.REP)
				|| contractStatus.getWkfStatus().equals(ContractWkfStatus.THE)
				|| contractStatus.getWkfStatus().equals(ContractWkfStatus.ACC)
				|| contractStatus.getWkfStatus().equals(ContractWkfStatus.WRI)
				|| contractStatus.getWkfStatus().equals(ContractWkfStatus.EAR)
				|| contractStatus.getWkfStatus().equals(ContractWkfStatus.CLO)
				|| contractStatus.getWkfStatus().equals(ContractWkfStatus.WTD)){
				   return contractStatus.getWkfStatus().getDescEn();
			 }
			    
			  return quotationStatus.getDescEn();
			}
	}
	
	private class CustomerFullNameColumnRenderer extends EntityColumnRenderer {
		@Override
		public Object getValue() {
			Applicant customer = ((Quotation) getEntity()).getMainApplicant();
			return customer.getLastNameEn() + " " + customer.getFirstNameEn();
		}
	}
	/*private class InsuranceDurationRenderer extends EntityColumnRenderer{

		@Override
		public Object getValue() {
			Quotation quotation = (Quotation) getEntity();
			Frequency frequency = quotation.getFrequency();
			Contract contract = quotation.getContract(); 
			if(contract == null){
				return "";
			}
			
			BaseRestrictions<Cashflow> restrictions = new BaseRestrictions<Cashflow>(Cashflow.class);
			restrictions.addAssociation("contract", "contract", JoinType.INNER_JOIN);
			restrictions.addCriterion(Restrictions.eq("contract.id", contract.getId()));
			restrictions.addAssociation("payment", "payment", JoinType.INNER_JOIN);
			restrictions.addCriterion(Restrictions.isNotNull("payment.id"));
			restrictions.addCriterion(Restrictions.eq("paid", true));
			restrictions.addCriterion(Restrictions.eq("cancel", false));
			restrictions.addOrder(Order.desc("numInstallment"));
			restrictions.setMaxResults(1);
			
			Cashflow cashflow = entityService.getFirst(restrictions);
			if(cashflow == null){
				return "";
			}
			
			int numInstallment = cashflow.getNumInstallment();
			long insuranceYearPaid = 0;
			long frequencyAmount = 12;
			switch(frequency){
				case M: frequencyAmount = 12;break;
				case Q: frequencyAmount = 3;break;
				case H: frequencyAmount = 6;break;
				case A:break;
				case D:	break;
				case W:	break;
				default:break;
			}
			
			if(numInstallment % frequencyAmount == 0){
				insuranceYearPaid = (long)(numInstallment/frequencyAmount) ;
			}else{
				insuranceYearPaid = (long)(numInstallment/frequencyAmount) + 1;
			}
			
			if(insuranceYearPaid == 0 && contract != null){
				insuranceYearPaid = 1;
			}
			return insuranceYearPaid + "";
		}
		
	}*/
}
