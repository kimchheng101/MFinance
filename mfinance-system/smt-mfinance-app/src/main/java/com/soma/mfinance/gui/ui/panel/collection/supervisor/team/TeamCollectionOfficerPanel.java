package com.soma.mfinance.gui.ui.panel.collection.supervisor.team;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.soma.frmk.vaadin.util.i18n.I18N;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * Created by v.sai on 5/10/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(TeamCollectionOfficerPanel.NAME)
public class TeamCollectionOfficerPanel extends AbstractTabsheetPanel implements View{

    public static final String NAME = "collections.team";

    @Autowired
    private TeamCollectionOfficerTablePanel teamCollectionOfficerTablePanel;

    @Autowired
    private TeamCollectionOfficerFormPanel teamCollectionOfficerFormPanel;

    @PostConstruct
    public void PostConstruct(){
        super.init();
        teamCollectionOfficerTablePanel.setMainPanel(this);
        teamCollectionOfficerFormPanel.setCaption(I18N.message("team.collection.officer"));
        getTabSheet().setTablePanel(teamCollectionOfficerTablePanel);
    }

    @Override
    public void onAddEventClick() {
        teamCollectionOfficerFormPanel.reset();
        getTabSheet().addFormPanel(teamCollectionOfficerFormPanel);
        getTabSheet().setSelectedTab(teamCollectionOfficerFormPanel);

    }

    @Override
    public void onEditEventClick() {
        getTabSheet().addFormPanel(teamCollectionOfficerFormPanel);
        initSelectedTab(teamCollectionOfficerFormPanel);
    }

    @Override
    public void initSelectedTab(com.vaadin.ui.Component selectTab) {
        if(selectTab == teamCollectionOfficerFormPanel)
            teamCollectionOfficerFormPanel.assignValue(teamCollectionOfficerTablePanel.getItemSelectedId());
        else if (selectTab == teamCollectionOfficerTablePanel && getTabSheet().isNeedRefresh())
            teamCollectionOfficerTablePanel.refresh();

        getTabSheet().setSelectedTab(selectTab);

    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}
