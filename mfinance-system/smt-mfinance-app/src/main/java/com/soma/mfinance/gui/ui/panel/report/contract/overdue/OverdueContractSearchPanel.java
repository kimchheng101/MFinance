package com.soma.mfinance.gui.ui.panel.report.contract.overdue;

import com.soma.mfinance.core.address.model.Area;
import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.collection.model.EColResult;
import com.soma.mfinance.core.collection.model.EColTask;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.shared.system.FMProfile;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.mfinance.core.widget.SecUserComboBox;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.ersys.core.hr.model.address.District;
import com.soma.ersys.core.hr.model.address.Province;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.NumberField;
import com.vaadin.ui.*;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;

import java.util.Arrays;
import java.util.List;

/**
 * Search panel for Collection Supervisor
 *
 * @author kimsuor.seang
 * @author s.torn
 */
public class OverdueContractSearchPanel extends AbstractSearchPanel<Collection> implements FMEntityField {

    private ERefDataComboBox<EDealerType> cbxDealerType;
    private DealerComboBox cbxDealer;
    private SecUserComboBox cbxCreditOfficer;
    private SecUserComboBox cbxCollectionOfficer;
    private TextField txtContractReference;
    private EntityRefComboBox<Province> cbxProvince;
    private EntityRefComboBox<District> cbxDistrict;
    private EntityRefComboBox<EColResult> cbxCollectionStatus;
    private ERefDataComboBox<EColTask> cbxCollectionTask;
    private EntityRefComboBox<Area> cbxArea;
    //private EntityRefComboBox<EPaymentMethod> cbxPaymentMethod;
    private NumberField txtOverdueFrom;
    private NumberField txtOverdueTo;

    /**
     * @param tablePanel
     */
    public OverdueContractSearchPanel(OverdueContractTablePanel tablePanel) {
        super(I18N.message("search"), tablePanel);
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#createForm()
     */
    @Override
    protected Component createForm() {
        cbxDealer = new DealerComboBox(ENTITY_SRV.list(getDealerRestriction()));
        cbxDealer.setSelectedEntity(null);
        cbxDealer.setWidth("220px");

        List<EDealerType> dealerTypes = EDealerType.values();
        cbxDealerType = new ERefDataComboBox<EDealerType>(dealerTypes);
        cbxDealerType.setImmediate(true);
        cbxDealerType.setWidth("220px");
        cbxDealerType.addValueChangeListener(event -> {
            BaseRestrictions<Dealer> restrictions = getDealerRestriction();
            if (cbxDealerType.getSelectedEntity() != null) {
                restrictions.addCriterion(Restrictions.eq("dealerType", cbxDealerType.getSelectedEntity()));
            }
            cbxDealer.setDealers(ENTITY_SRV.list(restrictions));
            cbxDealer.setSelectedEntity(null);
        });

        cbxCreditOfficer = new SecUserComboBox(DataReference.getInstance().getUsers(FMProfile.CO, EStatusRecord.ACTIV));
        cbxCollectionOfficer = new SecUserComboBox(DataReference.getInstance().getUsers(FMProfile.CC, EStatusRecord.ACTIV));
        txtContractReference = new TextField();
        txtContractReference.setWidth("150");
        cbxCollectionStatus = new EntityRefComboBox<EColResult>();
        cbxCollectionStatus.setRestrictions(new BaseRestrictions<EColResult>(EColResult.class));
        cbxCollectionStatus.renderer();
        cbxCollectionTask = new ERefDataComboBox<EColTask>(EColTask.class);
        cbxArea = new EntityRefComboBox<Area>(I18N.message("area"));
        cbxArea.setRestrictions(new BaseRestrictions<>(Area.class));
        cbxArea.renderer();

        /*cbxPaymentMethod = new EntityRefComboBox<EPaymentMethod>();
        cbxPaymentMethod.setRestrictions(new BaseRestrictions<>(EPaymentMethod.class));
        cbxPaymentMethod.renderer();
        cbxPaymentMethod.setImmediate(true);*/

        cbxProvince = new EntityRefComboBox<Province>();
        cbxProvince.setRestrictions(new BaseRestrictions<Province>(Province.class));
        cbxProvince.renderer();
        cbxProvince.setImmediate(true);

        cbxDistrict = new EntityRefComboBox<District>();
        cbxDistrict.setRestrictions(new BaseRestrictions<District>(District.class));
        cbxDistrict.renderer();
        cbxDistrict.setImmediate(true);

        txtOverdueFrom = new NumberField();
        txtOverdueFrom.setWidth("150");
        txtOverdueFrom.addStyleName("numerical");
        txtOverdueTo = new NumberField();
        txtOverdueTo.setWidth("150");

        GridLayout gridLayout = new GridLayout(10, 3);
        gridLayout.setSpacing(true);
        gridLayout.setMargin(true);
        int col = 0;
        int row = 0;
        gridLayout.addComponent(new Label(I18N.message("dealer.type")), col++, row);
        gridLayout.addComponent(cbxDealerType, col++, row);
        gridLayout.addComponent(new Label(I18N.message("credit.officer")), col++, row);
        gridLayout.addComponent(cbxCreditOfficer, col++, row);
        gridLayout.addComponent(new Label(I18N.message("contract.reference")), col++, row);
        gridLayout.addComponent(txtContractReference, col++, row);
        gridLayout.addComponent(new Label(I18N.message("collection.task")), col++, row);
        gridLayout.addComponent(cbxCollectionTask, col++, row);

        col = 0;
        row++;
        gridLayout.addComponent(new Label(I18N.message("dealer")), col++, row);
        gridLayout.addComponent(cbxDealer, col++, row);
        gridLayout.addComponent(new Label(I18N.message("collection.officer")), col++, row);
        gridLayout.addComponent(cbxCollectionOfficer, col++, row);
        gridLayout.addComponent(new Label(I18N.message("collection.status")), col++, row);
        gridLayout.addComponent(cbxCollectionStatus, col++, row);
        //gridLayout.addComponent(new Label(I18N.message("payment.method")), col++, row);
        //gridLayout.addComponent(cbxPaymentMethod, col++, row);
        gridLayout.addComponent(new Label(I18N.message("province")), col++, row);
        gridLayout.addComponent(cbxProvince, col++, row);

        col = 0;
        row++;
        gridLayout.addComponent(new Label(I18N.message("district")), col++, row);
        gridLayout.addComponent(cbxDistrict, col++, row);
        gridLayout.addComponent(new Label(I18N.message("overdue.from")), col++, row);
        gridLayout.addComponent(txtOverdueFrom, col++, row);
        gridLayout.addComponent(new Label(I18N.message("overdue.to")), col++, row);
        gridLayout.addComponent(txtOverdueTo, col++, row);

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);
        horizontalLayout.addComponent(gridLayout);
        return horizontalLayout;
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#getRestrictions()
     */
    @Override
    public BaseRestrictions<Collection> getRestrictions() {

        BaseRestrictions<Collection> collectionBaseRestrictions = new BaseRestrictions<>(Collection.class);
        if(ProfileUtil.isCollectionSupervisor()){
            collectionBaseRestrictions.addAssociation("contract", "con", JoinType.INNER_JOIN);
            List listContractStatus = Arrays.asList(new Object[]{
                    ContractWkfStatus.FIN
            });
            collectionBaseRestrictions.addCriterion(Restrictions.in("con.wkfStatus", listContractStatus));
        }

        if (cbxCreditOfficer.getSelectedEntity() != null) {
            collectionBaseRestrictions.addCriterion(Restrictions.eq("creditOfficer", cbxCreditOfficer.getSelectedEntity()));
        }

        if (StringUtils.isNotEmpty(txtContractReference.getValue())) {
            collectionBaseRestrictions.addCriterion(Restrictions.ilike("con.reference", txtContractReference.getValue(), MatchMode.ANYWHERE));
        }

        //collectionBaseRestrictions.addAssociation("lastCollectionAssist", "assis", JoinType.INNER_JOIN);
        if (cbxCollectionOfficer.getSelectedEntity() != null) {
            collectionBaseRestrictions.addCriterion(Restrictions.eq("collectionOfficer", cbxCollectionOfficer.getSelectedEntity()));
        }

        if (cbxProvince.getSelectedEntity() != null) {
            collectionBaseRestrictions.addCriterion(Restrictions.eq("province", cbxProvince.getSelectedEntity()));
        }

        if (cbxDealer.getSelectedEntity() != null) {
            collectionBaseRestrictions.addCriterion(Restrictions.eq("dealer", cbxDealer.getSelectedEntity().getNameEn()));
        }

        if (cbxCollectionStatus.getSelectedEntity() != null) {
            collectionBaseRestrictions.addCriterion(Restrictions.eq("colResult", cbxCollectionStatus.getSelectedEntity()));
        }

        /*if (cbxPaymentMethod.getSelectedEntity() != null) {
            collectionBaseRestrictions.addCriterion(Restrictions.eq("lastPaidPaymentMethod", cbxPaymentMethod.getSelectedEntity()));
        }*/

        if (cbxDistrict.getSelectedEntity() != null) {
            collectionBaseRestrictions.addCriterion(Restrictions.eq("district", cbxDistrict.getSelectedEntity()));
        }

        // waiting another developer code
        if (cbxCollectionTask.getSelectedEntity() != null) {
            collectionBaseRestrictions.addCriterion(Restrictions.ge("collectionTask", cbxCollectionTask.getSelectedEntity()));
        }

        /*if (cbxArea.getSelectedEntity() != null) {
            collectionBaseRestrictions.addCriterion(Restrictions.eq("area", cbxArea.getSelectedEntity()));
        }*/

        if (StringUtils.isNotEmpty(txtOverdueFrom.getValue())) {
            collectionBaseRestrictions.addCriterion(Restrictions.ge("nbOverdueInDays", Integer.valueOf(txtOverdueFrom.getValue())));
        }

        if (StringUtils.isNotEmpty(txtOverdueTo.getValue())) {
            collectionBaseRestrictions.addCriterion(Restrictions.le("nbOverdueInDays", Integer.valueOf(txtOverdueTo.getValue())));
        }
        return collectionBaseRestrictions;
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#reset()
     */
    @Override
    protected void reset() {
        cbxDealerType.setSelectedEntity(null);
        cbxDealer.setSelectedEntity(null);
        cbxCreditOfficer.setSelectedEntity(null);
        cbxCollectionOfficer.setSelectedEntity(null);
        txtContractReference.setValue("");
        cbxArea.setSelectedEntity(null);
        cbxCollectionStatus.setSelectedEntity(null);
        cbxCollectionTask.setSelectedEntity(null);
        //cbxPaymentMethod.setSelectedEntity(null);
        cbxProvince.setSelectedEntity(null);
        cbxDistrict.setSelectedEntity(null);
        txtOverdueFrom.setValue("");
        txtOverdueTo.setValue("");
    }

    /**
     * Get all Dealer except DealerType = OTH
     *
     * @return List of Dealers
     */
    private BaseRestrictions<Dealer> getDealerRestriction() {
        BaseRestrictions<Dealer> restrictions = new BaseRestrictions<Dealer>(Dealer.class);
        restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
        return restrictions;
    }
}
