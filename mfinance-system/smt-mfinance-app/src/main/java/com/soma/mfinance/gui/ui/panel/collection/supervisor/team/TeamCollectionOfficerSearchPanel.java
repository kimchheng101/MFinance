package com.soma.mfinance.gui.ui.panel.collection.supervisor.team;

import com.soma.mfinance.core.address.model.Area;
import com.soma.mfinance.core.collection.model.ColAssignment;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.statusrecord.StatusRecordField;
import com.soma.frmk.vaadin.util.i18n.I18N;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.model.entity.EStatusRecord;

import java.util.List;

/**
 * Created by v.sai on 5/10/2017.
 */
public class TeamCollectionOfficerSearchPanel extends AbstractSearchPanel<ColAssignment> implements FMEntityField{

    private TextField txtLogin;
    private TextField txtName;
    private StatusRecordField statusRecordField;
    private EntityRefComboBox<Area> cbxArea;

    public TeamCollectionOfficerSearchPanel(TeamCollectionOfficerTablePanel teamCollectionOfficerTablePanel) {
        super("Search", teamCollectionOfficerTablePanel);
    }

    @Override
    protected void reset() {
        txtLogin.setValue("");
        txtName.setValue("");
        cbxArea.setValue(-1);
        statusRecordField.setActiveValue(true);
        statusRecordField.getInactiveValue(false);
    }

    @Override
    protected Component createForm() {

        final GridLayout gridLayout = new GridLayout(4,1);
        txtLogin = ComponentFactory.getTextField("login", false, 60, 200);
        txtName = ComponentFactory.getTextField("name", false, 60, 200);
        statusRecordField = new StatusRecordField();
        List<Area> areaList= ENTITY_SRV.list(new BaseRestrictions<Area>(Area.class));
        cbxArea = new EntityRefComboBox<Area>(I18N.message("areas"));
        cbxArea.setRestrictions(new BaseRestrictions<Area>(Area.class));
        cbxArea.renderer();

        gridLayout.setSpacing(true);
        gridLayout.addComponent(new FormLayout(txtLogin), 0, 0);
        gridLayout.addComponent(new FormLayout(txtName), 1, 0);
        gridLayout.addComponent(new FormLayout(cbxArea),2,0);
        gridLayout.addComponent(new FormLayout(statusRecordField), 3, 0);
        return gridLayout;
    }

    @Override
    public BaseRestrictions<ColAssignment> getRestrictions() {

        BaseRestrictions<ColAssignment> restrictions = new BaseRestrictions<>(ColAssignment.class);
        restrictions.addAssociation("assignedBy", "ass", JoinType.LEFT_OUTER_JOIN);
        if (txtLogin.getValue() != null) {
            restrictions.addCriterion(Restrictions.like("ass.login", txtLogin.getValue(), MatchMode.ANYWHERE));
        }
        restrictions.addAssociation("assignee", "asse", JoinType.LEFT_OUTER_JOIN);
        if (txtName.getValue() != null) {
            restrictions.addCriterion(Restrictions.like("asse.desc", txtName.getValue(), MatchMode.ANYWHERE));
        }
        if (cbxArea.getSelectedEntity() != null) {
            restrictions.addAssociation("areas","area",JoinType.INNER_JOIN);
            restrictions.addCriterion(Restrictions.eq("area.id", cbxArea.getSelectedEntity().getId()));
        }
        if(!this.statusRecordField.getActiveValue().booleanValue() && !this.statusRecordField.getInactiveValue().booleanValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
            restrictions.getStatusRecordList().add(EStatusRecord.INACT);
        }
        if (this.statusRecordField.getActiveValue().booleanValue() && this.statusRecordField.getInactiveValue().booleanValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.INACT);
            restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
        }
        if(!this.statusRecordField.getActiveValue().booleanValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.INACT);
        }

        if(!this.statusRecordField.getInactiveValue().booleanValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
        }

        return restrictions;
    }
}
