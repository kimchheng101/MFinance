package com.soma.mfinance.gui.ui.panel.collection.supervisor.team;

import com.soma.mfinance.core.address.model.Area;
import com.soma.mfinance.core.collection.model.ColAssignment;
import com.soma.mfinance.core.collection.model.EColResult;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.RowRenderer;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.soma.frmk.vaadin.util.i18n.I18N;
import com.vaadin.data.Item;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by sr.soth on 5/24/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TeamCollectionOfficerTablePanel extends AbstractTablePanel<ColAssignment> implements FMEntityField{

    @PostConstruct
    public void PostConstruct(){
        super.init(I18N.message("team.collection.officer"));
        setCaption(I18N.message("team.collection.officer"));
        setSizeFull();
        setMargin(true);
        addDefaultNavigation();
    }

    @Override
    protected ColAssignment getEntity() {
        final Long id = getItemSelectedId();
        if(id != null) {
            return ENTITY_SRV.getById(ColAssignment.class, id);
        }
        return null;
    }

    @Override
    protected TeamCollectionOfficerSearchPanel createSearchPanel() {
        return new TeamCollectionOfficerSearchPanel(this);
    }

    @Override
    protected PagedDataProvider<ColAssignment> createPagedDataProvider() {
        BaseRestrictions<ColAssignment> restrictions = new BaseRestrictions<>(ColAssignment.class);

        PagedDefinition<ColAssignment> pagedDefinition = new PagedDefinition<>(restrictions);

        pagedDefinition.setRowRenderer(new TeamRowRenderer());
        pagedDefinition.addColumnDefinition(ID, I18N.message("id").toUpperCase(), Long.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("assigneeLogin", I18N.message("login").toUpperCase(), String.class, Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("assignee", I18N.message("name").toUpperCase(), String.class, Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("overduePeriod", I18N.message("overdueperiod").toUpperCase(),String.class,Align.LEFT,150);
        pagedDefinition.addColumnDefinition("task", I18N.message("task").toUpperCase(), String.class, Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("statuses", I18N.message("collection.status").toUpperCase(), String.class, Align.LEFT, 300);
        pagedDefinition.addColumnDefinition("areas", I18N.message("areas").toUpperCase(), String.class, Align.LEFT, 300);

        EntityPagedDataProvider<ColAssignment> pagedDataProvider = new EntityPagedDataProvider<>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    private class TeamRowRenderer implements RowRenderer {

        @Override
        public void renderer(Item item, Entity entity) {
            ColAssignment colAssignment = (ColAssignment)entity;

            if(colAssignment != null) {
                item.getItemProperty(ID).setValue(colAssignment.getId());
                item.getItemProperty("assigneeLogin").setValue(colAssignment.getAssignee().getLogin());
                item.getItemProperty("assignee").setValue(colAssignment.getAssignee().getDesc());
                item.getItemProperty("overduePeriod").setValue(colAssignment.getOverduePeriod().getOverdueFrom()+" - "+colAssignment.getOverduePeriod().getOverdueTo());
                item.getItemProperty("task").setValue(colAssignment.getTask() != null ? colAssignment.getTask().getDescEn() : "");

               // display statuses
               List<EColResult> statuses = colAssignment.getStatuses();
               if(statuses != null && !statuses.isEmpty()) {
                   StringBuffer strStatus = new StringBuffer();
                   int i = 0;
                   for(EColResult colResult : statuses) {
                       i++;
                       strStatus.append(colResult.getDescEn());
                       if(i < statuses.size())
                           strStatus.append(", ");
                   }
                   item.getItemProperty("statuses").setValue(strStatus.toString());
               }

               // display groups
                List<Area> areas = colAssignment.getAreas();
                if(areas != null && !areas.isEmpty()) {
                    StringBuffer strArea = new StringBuffer();
                    int i = 0;
                    for(Area area : areas) {
                        i++;
                        strArea.append(area.getDescEn());
                        if(i < areas.size())
                            strArea.append(", ");
                    }
                    item.getItemProperty("areas").setValue(strArea.toString());
                }
            }
        }
    }
}
