package com.soma.mfinance.gui.ui.panel.payment.blocked;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.payment.model.MPayment;
import com.soma.mfinance.gui.ui.panel.payment.blocked.filters.BlockedPaymentFilterPanel;
import com.vaadin.ui.VerticalLayout;

/**
 * 
 * @author kimsuor.seang
 */
public class BlockedPaymentDetailsPanel extends VerticalLayout implements MPayment {

	/** */
	private static final long serialVersionUID = -9031155014596338762L;
	
	private BlockedPaymentFilterPanel filterPanel;
	private BlockedPaymentDetailTablePanel tablePanel;
	private BlockedPaymentDetailInfosPanel infosPanel;
	
	/**
	 * 
	 * @param wkfStatus
	 */
	public BlockedPaymentDetailsPanel(EWkfStatus wkfStatus) {
		infosPanel = new BlockedPaymentDetailInfosPanel(wkfStatus);
		tablePanel = new BlockedPaymentDetailTablePanel(infosPanel, wkfStatus);
		filterPanel = new BlockedPaymentFilterPanel(tablePanel, wkfStatus);
		
		setSpacing(true);
		setMargin(true);
		addComponent(filterPanel);
		addComponent(tablePanel);
		
		/*if (!PaymentFileWkfStatus.UNIDENTIFIED.equals(wkfStatus) && !PaymentFileWkfStatus.ERROR.equals(wkfStatus)) {
			addComponent(infosPanel);
		}	*/
	}
	
	/**
	 * 
	 */
	public void refresh() {
		tablePanel.refreshAfterSaveOrAllocation();
	}
		
}
