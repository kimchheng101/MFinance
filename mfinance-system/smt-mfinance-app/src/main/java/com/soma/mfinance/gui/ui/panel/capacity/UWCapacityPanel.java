package com.soma.mfinance.gui.ui.panel.capacity;

import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.system.FMProfile;
import com.soma.mfinance.gui.ui.panel.report.numpos.PagedTable;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.soma.mfinance.core.helper.FinServicesHelper.QUO_SRV;

/**
 * AUTHOR : kimsuor.seang
 * EMAIL  : chhaichivon1995@gmail.com
 * DATE   : 9/11/2017
 * TIME   : 11:08 AM
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class UWCapacityPanel extends AbstractTabPanel implements View {

    private static final long serialVersionUID = -9074769327393746050L;
    private PagedTable<Quotation> quotationPagedTable;

    private Date startDate;
    private Date endDate;


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        setDBSourceContainer();
    }

    @Override
    protected com.vaadin.ui.Component createForm() {
        TabSheet tabSheet = new TabSheet();
        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setSpacing(true);
        contentLayout.setMargin(true);

        quotationPagedTable = new PagedTable<>(createColumnDefinitions());

        contentLayout.addComponent(quotationPagedTable);
        contentLayout.addComponent(quotationPagedTable.createControls());
        tabSheet.addTab(contentLayout, "Underwriter Capacity");

        return tabSheet;
    }

    private List<ColumnDefinition> createColumnDefinitions() {
        List<ColumnDefinition> columnDefinitions = new ArrayList<>();

        columnDefinitions.add(new ColumnDefinition("uw_name", "UW Officer", String.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition("num_app", "# Application", Integer.class, Table.Align.LEFT, 150));

        return columnDefinitions;
    }

    @SuppressWarnings("unchecked")
    private void setDBSourceContainer() {
        Container indexedContainer = quotationPagedTable.getContainerDataSource();
        indexedContainer.removeAllItems();

        int id = 1;
        List<SecUser> secUsers  = QUO_SRV.getSecUserByProfile(FMProfile.UW);
        if(!secUsers.isEmpty()){
            for (SecUser secUser :  secUsers){
                Integer totalCapacity = 0;
                List<Quotation> quotations = QUO_SRV.getCapacityUWandUS(secUser,FMProfile.UW,startDate,endDate);
                if(quotations != null){
                    for (Quotation quotation : quotations){
                        if(quotation.getId() != null){
                            totalCapacity = totalCapacity + 1;
                        }
                    }
                }
                Item item = indexedContainer.addItem(id);
                item.getItemProperty("uw_name").setValue(secUser.getDesc());
                item.getItemProperty("num_app").setValue(totalCapacity);
                id += 1;
            }
        }

    }

    public void refreshTable() {
        setDBSourceContainer();
        quotationPagedTable.refreshContainerDataSource();
    }

    public void setFilterSearch(Date dtStart, Date dtEnd) {
        startDate = dtStart;
        endDate = dtEnd;
        setDBSourceContainer();
    }
}