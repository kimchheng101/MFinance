package com.soma.mfinance.gui.ui.panel.report.uwrejectreport;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ru.xpoft.vaadin.VaadinView;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;

/**
 * @modified p.ly
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(UWRejectReportPanel.NAME)
public class UWRejectReportPanel  extends AbstractTabsheetPanel implements View {
	/** */
	private static final long serialVersionUID = -1929551934421408575L;
	public static final String NAME = "uw.reject.report";
	
	@Autowired
	private UWRejectReportTablePanel uwRejectReportTablePanel;
	
	/**
	 */
	@PostConstruct
	public void PostConstruct() {
		super.init();
		uwRejectReportTablePanel.setMainPanel(this);
		getTabSheet().setTablePanel(uwRejectReportTablePanel);
	}

	/**
	 * @see com.vaadin.navigator.View#enter(com.vaadin.navigator.ViewChangeListener.ViewChangeEvent)
	 */
	@Override
	public void enter(ViewChangeEvent event) {
	}

	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel#onAddEventClick()
	 */
	@Override
	public void onAddEventClick() {
	}

	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel#onEditEventClick()
	 */
	@Override
	public void onEditEventClick() {
	}

	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel#initSelectedTab(com.vaadin.ui.Component)
	 */
	@Override
	public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
		getTabSheet().setSelectedTab(selectedTab);
	}

}
