package com.soma.mfinance.gui.ui.panel.capacity;

import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.component.DateRangeField;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * AUTHOR : kimsuor.seang
 * EMAIL  : chhaichivon1995@gmail.com
 * DATE   : 9/11/2017
 * TIME   : 11:06 AM
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(CapacityPanel.NAME)
public class CapacityPanel extends Panel implements View {

    private static final long serialVersionUID = -7293219035042371292L;
    public static final String NAME = "capacity.panel";

    @Autowired
    private UWCapacityPanel uwCapacityPanel;
    @Autowired
    private USCapacityPanel usCapacityPanel;

    private AutoDateField dfStartDate;
    private AutoDateField dfEndDate;
    private GridLayout grid;
    private Panel panelSearchCapacity;
    private DateRangeField dateRangeField;
    private Button btnSearch;

    @PostConstruct
    public void PostConstruct() {
        VerticalLayout rootPanel = new VerticalLayout();
        panelSearchCapacity = new Panel();
        final GridLayout gridLayout = new GridLayout(10, 4);
        gridLayout.setSpacing(true);
        gridLayout.setMargin(true);

        dfStartDate = new AutoDateField();
        dfStartDate.setValue(DateUtils.today());
        dfEndDate =  new AutoDateField();
        dfEndDate.setValue(DateUtils.today());

        dateRangeField = new DateRangeField(dfStartDate, dfEndDate);

        // FIRST LOAD
        search();

        Button btnSearch = new Button(I18N.message("search"));
        btnSearch.setClickShortcut(ShortcutAction.KeyCode.ENTER, null);
        btnSearch.setIcon(new ThemeResource("../smt-default/icons/16/search.png"));
        btnSearch.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                search();
            }
        });

        grid = new GridLayout(2, 1);
        grid.addComponent(uwCapacityPanel,0,0);
        grid.addComponent(usCapacityPanel,1,0);

        int iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("date.start")), iCol++, 0);
        gridLayout.addComponent(dfStartDate, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("date.end")), iCol++, 0);
        gridLayout.addComponent(dfEndDate, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("")), iCol++, 0);
        gridLayout.addComponent(dateRangeField, iCol++, 0);

        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("")), iCol++, 1);
        gridLayout.addComponent(btnSearch, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);

        panelSearchCapacity.setContent(gridLayout);
        rootPanel.addComponent(panelSearchCapacity);
        rootPanel.addComponent(grid);
        setContent(rootPanel);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        uwCapacityPanel.enter(event);
        usCapacityPanel.enter(event);

    }
    private void search(){
        usCapacityPanel.setFilterSearch( dfStartDate.getValue(), dfEndDate.getValue());
        uwCapacityPanel.setFilterSearch( dfStartDate.getValue(), dfEndDate.getValue());
    }
}