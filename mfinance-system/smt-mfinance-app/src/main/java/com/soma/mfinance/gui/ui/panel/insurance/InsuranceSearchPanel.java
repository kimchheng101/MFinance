package com.soma.mfinance.gui.ui.panel.insurance;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.common.security.model.SecUserDetail;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.financial.model.FinProduct;
import com.soma.mfinance.core.insurance.model.InsuranceStatus;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.registrations.model.ProfileContractStatus;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.ersys.core.hr.model.address.Province;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.Association;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.tools.DateUtils;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.List;

import static com.soma.mfinance.core.helper.FinServicesHelper.*;

public class InsuranceSearchPanel extends AbstractSearchPanel<Quotation> implements QuotationEntityField {

    private static final long serialVersionUID = 5652917190201361324L;

    private TextField txtReference;
    private TextField txtFirstNameEn;
    private TextField txtLastNameEn;
    private TextField txtChassisNumber;
    private TextField txtEngineNumber;
    private AutoDateField dfStartDate;
    private AutoDateField dfEndDate;
    private ERefDataComboBox<EDealerType> cbxDealerType;
    private EntityRefComboBox<Province> cbxProvince;
    private EntityComboBox<ProfileContractStatus> cbxContractStatus;
    private EntityRefComboBox<InsuranceStatus> cbxInsuranceStatus;
    private EntityRefComboBox<InsuranceStatus> cbxInsuranceStatus2;
    private EntityRefComboBox<InsuranceStatus> cbxInsuranceStatus3;
    private EntityRefComboBox<InsuranceStatus> cbxInsuranceStatus4;
    private DealerComboBox cbxDealer;
    private SecUserDetail usrDetail;
    private ValueChangeListener valueChangeListener;
    private EntityRefComboBox<FinProduct> cbxFinancialProduct;

    public InsuranceSearchPanel(InsuranceTablePanel insuranceTablePanel) {
        super(I18N.message("insurance.search"), insuranceTablePanel);
    }

    @Override
    protected Component createForm() {
        txtReference = ComponentFactory.getTextField(I18N.message("reference"), false, 60, 220);
        txtFirstNameEn = ComponentFactory.getTextField(I18N.message("firstname.en"), false, 60, 220);
        txtLastNameEn = ComponentFactory.getTextField(I18N.message("lastname.en"), false, 60, 220);
        txtChassisNumber = ComponentFactory.getTextField(I18N.message("chassis.number"), false, 50, 220);
        txtEngineNumber = ComponentFactory.getTextField(I18N.message("engine.number"), false, 50, 220);
        dfStartDate = ComponentFactory.getAutoDateField(I18N.message("startdate"), false);
        dfEndDate = ComponentFactory.getAutoDateField(I18N.message("enddate"), false);

        SecUser secUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        usrDetail = ENTITY_SRV.getByField(SecUserDetail.class, "secUser.id", secUser.getId());

        cbxDealerType = new ERefDataComboBox<>(I18N.message("dealer.type"), EDealerType.values());
        cbxDealerType.setImmediate(true);
        cbxDealerType.setWidth("220px");
        valueChangeListener = new ValueChangeListener() {
            private static final long serialVersionUID = 5719856916200703515L;

            @Override
            public void valueChange(ValueChangeEvent event) {
                cbxDealer.setDealers(ENTITY_SRV.list(DEA_SRV.getRestrictionsDealerByDealerType(cbxDealerType.getSelectedEntity())));
            }
        };
        cbxDealerType.addValueChangeListener(valueChangeListener);

        cbxContractStatus = new EntityComboBox<>(ProfileContractStatus.class, "wkfStatus.descEn");
        cbxContractStatus.setCaption(I18N.message("contract.status"));
        cbxContractStatus.setEntities(CONT_SRV.getProfileContractStatus());
        cbxContractStatus.setImmediate(true);
        cbxContractStatus.setWidth("220px");

        cbxInsuranceStatus = new EntityRefComboBox<>(I18N.message("insurance.status"));
        cbxInsuranceStatus.setRestrictions(new BaseRestrictions<>(InsuranceStatus.class));
        cbxInsuranceStatus.renderer();
        cbxInsuranceStatus.setImmediate(true);
        cbxInsuranceStatus.setWidth("220px");

        cbxInsuranceStatus2 = new EntityRefComboBox<>(I18N.message("insurance.status"));
        cbxInsuranceStatus2.setRestrictions(new BaseRestrictions<>(InsuranceStatus.class));
        cbxInsuranceStatus2.renderer();
        cbxInsuranceStatus2.setImmediate(true);
        cbxInsuranceStatus2.setWidth("220px");

        cbxInsuranceStatus3 = new EntityRefComboBox<>(I18N.message("insurance.status"));
        cbxInsuranceStatus3.setRestrictions(new BaseRestrictions<>(InsuranceStatus.class));
        cbxInsuranceStatus3.renderer();
        cbxInsuranceStatus3.setImmediate(true);
        cbxInsuranceStatus3.setWidth("220px");

        cbxInsuranceStatus4 = new EntityRefComboBox<>(I18N.message("insurance.status"));
        cbxInsuranceStatus4.setRestrictions(new BaseRestrictions<>(InsuranceStatus.class));
        cbxInsuranceStatus4.renderer();
        cbxInsuranceStatus4.setImmediate(true);
        cbxInsuranceStatus4.setWidth("220px");

        BaseRestrictions<Dealer> restrictions = new BaseRestrictions<>(Dealer.class);
        restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
        cbxDealer = new DealerComboBox(I18N.message("dealer"), ENTITY_SRV.list(restrictions));

        cbxDealer.setEnabled(true);
        cbxDealerType.setEnabled(true);
        cbxDealer.setImmediate(true);
        cbxDealer.setWidth("220px");
        if (ProfileUtil.isPOS() && usrDetail != null && usrDetail.getDealer() != null) {
            cbxDealer.setSelectedEntity(usrDetail.getDealer());
            cbxDealerType.removeValueChangeListener(valueChangeListener);
            cbxDealerType.setSelectedEntity(cbxDealer.getSelectedEntity() != null ? cbxDealer.getSelectedEntity().getDealerType() : null);
            cbxDealerType.addValueChangeListener(valueChangeListener);
        }

        cbxProvince = new EntityRefComboBox<>(I18N.message("province"));
        cbxProvince.setRestrictions(new BaseRestrictions<>(Province.class));
        cbxProvince.renderer();
        cbxProvince.setImmediate(true);
        cbxProvince.setWidth("220px");

        cbxFinancialProduct = new EntityRefComboBox<>(I18N.message("financial.product"));
        cbxFinancialProduct.setWidth("220px");
        cbxFinancialProduct.setRestrictions(FIN_PROD_SRV.getFinProductRestrictions());
        cbxFinancialProduct.setImmediate(true);
        cbxFinancialProduct.renderer();
        cbxFinancialProduct.setSelectedEntity(null);

        final GridLayout gridLayout = new GridLayout(10, 4);

        FormLayout formLayoutLeft = new FormLayout();
        formLayoutLeft.addComponent(txtLastNameEn);
        formLayoutLeft.addComponent(cbxProvince);
        formLayoutLeft.addComponent(cbxInsuranceStatus);
        formLayoutLeft.addComponent(dfStartDate);

        FormLayout formLayoutMiddleLeft = new FormLayout();
        formLayoutMiddleLeft.addComponent(txtFirstNameEn);
        formLayoutMiddleLeft.addComponent(cbxDealerType);
        formLayoutMiddleLeft.addComponent(cbxInsuranceStatus2);
        formLayoutMiddleLeft.addComponent(dfEndDate);

        FormLayout formLayoutMiddle = new FormLayout();
        formLayoutMiddle.addComponent(txtReference);
        formLayoutMiddle.addComponent(cbxDealer);
        formLayoutMiddle.addComponent(cbxInsuranceStatus3);

        FormLayout formLayoutMiddleRight = new FormLayout();
        formLayoutMiddleRight.addComponent(txtChassisNumber);
        formLayoutMiddleRight.addComponent(cbxContractStatus);
        formLayoutMiddleRight.addComponent(cbxInsuranceStatus4);

        FormLayout formLayoutRight = new FormLayout();
        formLayoutRight.addComponent(txtEngineNumber);
        formLayoutRight.addComponent(cbxFinancialProduct);

        int iCol = 0;
        gridLayout.addComponent(formLayoutLeft, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(20, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(formLayoutMiddleLeft, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(20, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(formLayoutMiddle, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(20, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(formLayoutMiddleRight, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(20, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(formLayoutRight, iCol++, 0);
        return gridLayout;
    }

    @Override
    public BaseRestrictions<Quotation> getRestrictions() {
        SecUser secUser = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Conjunction and = Restrictions.and();
        Disjunction or = Restrictions.or();

        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);

        List<EWkfStatus> excludedStatus = new ArrayList<EWkfStatus>();
        excludedStatus.add(ContractWkfStatus.THE);
        excludedStatus.add(ContractWkfStatus.ACC);
        excludedStatus.add(ContractWkfStatus.REP);
        excludedStatus.add(QuotationWkfStatus.WCA);

        restrictions.addCriterion(Restrictions.not(Restrictions.in(WKF_STATUS, excludedStatus)));

        restrictions.addAssociation("asset", "qouasset", JoinType.INNER_JOIN);

        if (StringUtils.isNotEmpty(txtReference.getValue())) {
            restrictions.addCriterion(Restrictions.like(REFERENCE, txtReference.getValue(), MatchMode.ANYWHERE));
        }

        if (cbxContractStatus.getSelectedEntity() != null) {
            restrictions.addAssociation("contract", "cotra", JoinType.LEFT_OUTER_JOIN);
            if (cbxContractStatus.getSelectedEntity().getWkfStatus().equals(QuotationWkfStatus.ACT)) {
                restrictions.addCriterion(Restrictions.eq("cotra." + WKF_STATUS, ContractWkfStatus.FIN));
            } else if (cbxContractStatus.getSelectedEntity().getWkfStatus().equals(QuotationWkfStatus.APV)
                    || cbxContractStatus.getSelectedEntity().getWkfStatus().equals(QuotationWkfStatus.PPO)
                    || cbxContractStatus.getSelectedEntity().getWkfStatus().equals(QuotationWkfStatus.WIV)
                    || cbxContractStatus.getSelectedEntity().getWkfStatus().equals(QuotationWkfStatus.WCA)
                    ) {

                restrictions.addCriterion(Restrictions.eq(WKF_STATUS, cbxContractStatus.getSelectedEntity().getWkfStatus()));
            } else {
                restrictions.addCriterion(Restrictions.eq("cotra." + WKF_STATUS, cbxContractStatus.getSelectedEntity().getWkfStatus()));
            }
        } else if (!cbxContractStatus.getValueMap().values().isEmpty()) {
            restrictions.addCriterion(Restrictions.in(WKF_STATUS, CONT_SRV.getDefaultContractStatusByCurrentUser()));
        } else {
            restrictions.addCriterion(Restrictions.isNull(WKF_STATUS));
        }
        restrictions.addCriterion(or);

        if (cbxInsuranceStatus.getSelectedEntity() != null
                || cbxInsuranceStatus2.getSelectedEntity() != null
                || cbxInsuranceStatus3.getSelectedEntity() != null
                || cbxInsuranceStatus4.getSelectedEntity() != null) {
            Disjunction orJunction = Restrictions.or();

            if (cbxInsuranceStatus.getSelectedEntity() != null) {
                orJunction.add(Restrictions.eq("insuranceStatus.id", cbxInsuranceStatus.getSelectedEntity().getId()));
            }
            if (cbxInsuranceStatus2.getSelectedEntity() != null) {
                orJunction.add(Restrictions.eq("insuranceStatus.id", cbxInsuranceStatus2.getSelectedEntity().getId()));
            }
            if (cbxInsuranceStatus3.getSelectedEntity() != null) {
                orJunction.add(Restrictions.eq("insuranceStatus.id", cbxInsuranceStatus3.getSelectedEntity().getId()));
            }
            if (cbxInsuranceStatus4.getSelectedEntity() != null) {
                orJunction.add(Restrictions.eq("insuranceStatus.id", cbxInsuranceStatus4.getSelectedEntity().getId()));
            }
            restrictions.addCriterion(orJunction);
        }

        if (cbxProvince.getSelectedEntity() != null || StringUtils.isNotEmpty(txtLastNameEn.getValue())
                || StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
            restrictions.addAssociation("quotationApplicants", "quoapp", JoinType.INNER_JOIN);
            restrictions.addAssociation("quoapp.applicant", "app", JoinType.INNER_JOIN);
            restrictions.addAssociation("app.individual", "ind", JoinType.INNER_JOIN);
            restrictions.addCriterion("quoapp.applicantType", EApplicantType.C);
        }

        if (cbxProvince.getSelectedEntity() != null) {
            restrictions.addAssociation("ind.individualAddresses", "appaddr", JoinType.INNER_JOIN);
            restrictions.addAssociation("appaddr.address", "addr", JoinType.INNER_JOIN);
            restrictions.addCriterion("addr.province.id", cbxProvince.getSelectedEntity().getId());
        }


        if (StringUtils.isNotEmpty(txtLastNameEn.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("ind." + LAST_NAME_EN, txtLastNameEn.getValue(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("ind." + FIRST_NAME_EN, txtFirstNameEn.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtChassisNumber.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("qouasset.chassisNumber", txtChassisNumber.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtEngineNumber.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("qouasset.engineNumber", txtEngineNumber.getValue(), MatchMode.ANYWHERE));
        }

        if (cbxDealer.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, cbxDealer.getSelectedEntity().getId()));
        }

        boolean joinContract = false;
        for (Association assoc : restrictions.getAssociations()) {
            if (assoc.getAssociationPath().equalsIgnoreCase("contract")) {
                joinContract = true;
                break;
            }
        }
        if (!joinContract) {
            restrictions.addAssociation("contract", "cotra", JoinType.LEFT_OUTER_JOIN);
        }
        if (dfStartDate.getValue() != null) {
            restrictions.addCriterion(Restrictions.ge("cotra.startDate", DateUtils.getDateAtBeginningOfDay(dfStartDate.getValue())));
        }
        if (dfEndDate.getValue() != null) {
            restrictions.addCriterion(Restrictions.le("cotra.startDate", DateUtils.getDateAtEndOfDay(dfEndDate.getValue())));
        }

		/*if(dfStartDate.getValue() != null || dfEndDate.getValue() != null ){
            restrictions.addOrder(Order.desc("insuranceEndYear"));
		}*/


        restrictions.addAssociation("dealer", "dea", JoinType.INNER_JOIN);
        if (cbxDealerType.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq("dea.dealerType", cbxDealerType.getSelectedEntity()));
        }
        //restrictions.addOrder(Order.desc(CHANGE_STATUS_DATE));

        if (cbxFinancialProduct.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq(FINANCIAL_PRODUCT, cbxFinancialProduct.getSelectedEntity()));
        }
        return restrictions;
    }

    @Override
    protected void reset() {
        cbxProvince.setSelectedEntity(null);
        cbxInsuranceStatus.setSelectedEntity(null);
        cbxInsuranceStatus2.setSelectedEntity(null);
        cbxInsuranceStatus3.setSelectedEntity(null);
        cbxInsuranceStatus4.setSelectedEntity(null);
        cbxContractStatus.setSelectedEntity(null);
        txtReference.setValue("");
        txtFirstNameEn.setValue("");
        txtLastNameEn.setValue("");
        txtChassisNumber.setValue("");
        txtEngineNumber.setValue("");
        dfStartDate.setValue(null);
        dfEndDate.setValue(null);
        cbxFinancialProduct.setSelectedEntity(null);
    }

    public List<String> getMessageErrorBeforeSearching() {
        List<String> errMsg = new ArrayList<String>();
        if (ProfileUtil.isDocumentController()) {
            if (cbxInsuranceStatus.getSelectedEntity() == null
                    && cbxInsuranceStatus2.getSelectedEntity() == null
                    && cbxInsuranceStatus3.getSelectedEntity() == null
                    && cbxInsuranceStatus4.getSelectedEntity() == null) {
                String messageFields = I18N.message("insurance.status") + " , " + I18N.message("insurance.company");
                errMsg.add(I18N.message("please.enter.one.there.searching.criteria",
                        new String[]{messageFields}));
            }
        }
        return errMsg;
    }

}
