package com.soma.mfinance.gui.ui.panel.accounting.installment;

import com.soma.mfinance.core.accounting.InstallmentVO;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowType;
import com.soma.mfinance.core.entityfield.InstallmentEntityField;
import com.soma.mfinance.core.financial.model.EServiceType;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.shared.contract.PenaltyVO;
import com.soma.frmk.security.context.SecApplicationContextHolder;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.ui.Button;
import com.vaadin.ui.Table.Align;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.amount.AmountUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author vi.sok
 *
 */
public class InstallmentTable implements InstallmentEntityField, FinServicesHelper {
	private List<ColumnDefinition> columnDefinitions;
	private SimplePagedTable<Contract> pagedTable;
	private InstallmentDetailPanel installmentDetailPanel;
	private Window window;
	private Button btnSearch;
	public InstallmentTable(Button btnSearch){
		this.btnSearch = btnSearch;
	}
	public List<ColumnDefinition> getHeader(){
		columnDefinitions = new ArrayList<ColumnDefinition>();
		columnDefinitions.add(new ColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 140, false));
		columnDefinitions.add(new ColumnDefinition(CONTRACT, I18N.message("contract"), String.class, Align.LEFT, 140));
		columnDefinitions.add(new ColumnDefinition(CUSTOMER, I18N.message("customer"), String.class, Align.LEFT, 140));
		columnDefinitions.add(new ColumnDefinition(DEALER + "." + NAME_EN, I18N.message("dealer"), String.class, Align.LEFT, 140));
		columnDefinitions.add(new ColumnDefinition("motor.model", I18N.message("motor.model"), String.class, Align.LEFT, 100));
		columnDefinitions.add(new ColumnDefinition(DUE_DATE, I18N.message("due.date"), Date.class, Align.LEFT, 110));
		columnDefinitions.add(new ColumnDefinition(NUM_INSTALLMENT, I18N.message("No"), Integer.class, Align.CENTER, 60));
		columnDefinitions.add(new ColumnDefinition(INSTALLMENT_AMOUNT, I18N.message("installment.amount"), String.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(PRINCIPAL_AMOUNT, I18N.message("principal.amount"), String.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(INTEREST_AMOUNT, I18N.message("interest.amount"), String.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(INSURANCE_FEE, I18N.message("insurance.fee"), String.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(SERVICING_FEE, I18N.message("servicing.fee"), String.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(TRANSFER_FEE, I18N.message("transfer.fee"), String.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(TOTAL_FEE_VAT, I18N.message("total.fee.vat"), String.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(OTHER_AMOUNT, I18N.message("other.amount"), String.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(NUM_PENALTY_DAY, I18N.message("no.penalty.days"), Integer.class, Align.LEFT, 80));
		columnDefinitions.add(new ColumnDefinition(PENALTY_AMOUNT, I18N.message("penalty.amount"), String.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(TOTAL_PAYMENT, I18N.message("total.payment"), String.class, Align.RIGHT, 70));
		return columnDefinitions;
	}
	/**
	 * @param contracts, startDate ,endDate
	 */
	public void getData(List<Contract> contracts, Date startDate, Date endDate){

		Indexed indexedContainer = pagedTable.getContainerDataSource();
		indexedContainer.removeAllItems();
		Date startDateInstallment = DateUtils.getDateAtBeginningOfDay(startDate);
		Date endDateInstallment = DateUtils.getDateAtEndOfDay(endDate);
		eventTable();
		int index = 0;
		boolean isBreak = false;
		if (contracts != null && !contracts.isEmpty()) {
			for (Contract contract : contracts) {
				int term = contract.getTerm();
				int firstInstallmentNotPaid = 1;
				Applicant applicant = contract.getApplicant();
				if (contract.getLastPaidNumInstallment() != null) {
					firstInstallmentNotPaid = contract.getLastPaidNumInstallment() + 1;
				}
				for (int i = firstInstallmentNotPaid; i <= term; i++) {
					List<InstallmentVO> installmentByNumInstallments = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(contract, i);
					Date installamentDate = installmentByNumInstallments.get(0).getInstallmentDate();
					if((startDate == null || installamentDate.after(startDateInstallment)) && (endDate == null || installamentDate.before(endDateInstallment))){
						double tiPrincipal = 0d;
						double tiInterestRate = 0d;
						double tiInsurance = 0d;
						double tiServiceFee = 0d;
						double tiTransFee = 0d;
						double otherAmount = 0d;
						double totalInstallment, installmentAmount, grandTotalInstallment;

						double totalFeeVat = 0d;

						InstallmentVO installmentVOFirstIndex = installmentByNumInstallments.get(0);
						for (InstallmentVO installmentVO : installmentByNumInstallments) {
							if (installmentVO.getCashflowType().equals(ECashflowType.CAP)) {
								tiPrincipal = installmentVO.getTiamount();
								totalFeeVat+= installmentVO.getVatAmount();
							} else if(installmentVO.getCashflowType().equals(ECashflowType.IAP)) {
								tiInterestRate = installmentVO.getTiamount();
							} else if(installmentVO.getService() != null && EServiceType.INSFEE.getCode().equals(installmentVO.getService().getCode())){
								tiInsurance = installmentVO.getTiamount();
								totalFeeVat+= installmentVO.getVatAmount();
							} else if(installmentVO.getService() != null && EServiceType.SRVFEE.getCode().equals(installmentVO.getService().getCode())){
								tiServiceFee = installmentVO.getTiamount();
								totalFeeVat+= installmentVO.getVatAmount();
							} else if(installmentVO.getService() != null && EServiceType.SRVFEEEXT.getCode().equals(installmentVO.getService().getCode())){
								tiServiceFee = installmentVO.getTiamount();
								totalFeeVat+= installmentVO.getVatAmount();
							} else if(installmentVO.getService() != null && EServiceType.TRANSFEE.getCode().equals(installmentVO.getService().getCode())) {
								tiTransFee = installmentVO.getTiamount() ;
								totalFeeVat+= installmentVO.getVatAmount();
							}else {
								otherAmount += installmentVO.getTiamount();
								totalFeeVat+= installmentVO.getVatAmount();
							}
						}
						PenaltyVO penaltyVO = CONT_SRV.calculatePenalty(contract, installmentVOFirstIndex.getInstallmentDate(), DateUtils.todayH00M00S00(), contract.getTiInstallmentAmount());
						double penaltyAmount = 0d;
						if (penaltyVO.getPenaltyAmount() != null) {
							penaltyAmount += penaltyVO.getPenaltyAmount().getTiAmount() + penaltyVO.getPenaltyAmount().getVatAmount();
						}
						totalInstallment = tiPrincipal + tiInterestRate + tiInsurance + tiServiceFee + tiTransFee +otherAmount + totalFeeVat;
						installmentAmount = tiPrincipal + tiInterestRate;
						grandTotalInstallment = totalInstallment + penaltyAmount;
						final Item item = indexedContainer.addItem(index);
						item.getItemProperty(ID).setValue(contract.getId());
						item.getItemProperty(CONTRACT).setValue(contract.getReference());
						item.getItemProperty(CUSTOMER).setValue(applicant.getLastNameEn() +" "+applicant.getFirstNameEn());
						item.getItemProperty(DEALER + "." + NAME_EN).setValue(contract.getDealer().getNameEn());
						item.getItemProperty("motor.model").setValue(contract.getAsset().getModel().getDesc());
						item.getItemProperty(DUE_DATE).setValue(installmentVOFirstIndex.getInstallmentDate());
						item.getItemProperty(NUM_INSTALLMENT).setValue(installmentVOFirstIndex.getNumInstallment());
						item.getItemProperty(INSTALLMENT_AMOUNT).setValue(AmountUtils.format(installmentAmount));
						item.getItemProperty(PRINCIPAL_AMOUNT).setValue(AmountUtils.format(tiPrincipal));
						item.getItemProperty(INTEREST_AMOUNT).setValue(AmountUtils.format(tiInterestRate));
						item.getItemProperty(INSURANCE_FEE).setValue(AmountUtils.format(tiInsurance));
						item.getItemProperty(SERVICING_FEE).setValue(AmountUtils.format(tiServiceFee));
						item.getItemProperty(TRANSFER_FEE).setValue(AmountUtils.format(tiTransFee));
						item.getItemProperty(TOTAL_FEE_VAT).setValue(AmountUtils.format(totalFeeVat));
						item.getItemProperty(OTHER_AMOUNT).setValue(AmountUtils.format(otherAmount));
						item.getItemProperty(NUM_PENALTY_DAY).setValue(penaltyVO.getNumPenaltyDays());
						item.getItemProperty(PENALTY_AMOUNT).setValue(AmountUtils.format(penaltyAmount));
						item.getItemProperty(TOTAL_PAYMENT).setValue(AmountUtils.format(grandTotalInstallment));
						index++;
						isBreak = true;
					} else if(isBreak){
						break;
					}
				}
			} 
		}
		
		pagedTable.refreshContainerDataSource();
	
	}
	/**
	 * 
	 */
	private void eventTable() {
		pagedTable.addItemClickListener(new ItemClickListener() {
			private static final long serialVersionUID = -6676228064499031341L;

			@Override
			public void itemClick(ItemClickEvent event) {
				boolean isDoubleClick = event.isDoubleClick() || SecApplicationContextHolder.getContext().clientDeviceIsMobileOrTablet();
				if (isDoubleClick) {
					Item item = event.getItem();
					Long contractId = (Long) item.getItemProperty(ID).getValue();
					Integer numInstallment = (Integer) item.getItemProperty(NUM_INSTALLMENT).getValue();
					installmentDetailPanel = new InstallmentDetailPanel(btnSearch);
					installmentDetailPanel.assignValues(contractId, numInstallment);
					getPopup(installmentDetailPanel);
				}
			}
		});
	}
	/**
	 * 
	 */
	private void getPopup(InstallmentDetailPanel installmentDetailPanel) {
		if (window != null) {
			window.close();
		}
		window = new Window();
		window.setImmediate(true);
		window.setModal(true);
		window.setContent(installmentDetailPanel);
		window.setCaption(I18N.message("installment.detail"));
		window.setWidth(900, Unit.PIXELS);
		window.setHeight(300, Unit.PIXELS);
		window.center();
		UI.getCurrent().addWindow(window);
	}
	/**
	 * @return the pagedTable
	 */
	public SimplePagedTable<Contract> getPagedTable() {
		return pagedTable;
	}
	/**
	 * @param pagedTable the pagedTable to set
	 */
	public void setPagedTable(SimplePagedTable<Contract> pagedTable) {
		this.pagedTable = pagedTable;
	}
}
