package com.soma.mfinance.gui.ui.panel.contracts;

import java.util.List;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.financial.model.FinProduct;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.ersys.core.hr.model.address.Province;
import com.soma.ersys.core.hr.model.eref.ETypeAddress;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.service.EntityService;
import org.seuksa.frmk.tools.spring.SpringUtils;

import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

/**
 * Contract search
 * @author kimsuor.seang
 *
 */
public class ContractsSearchPanel extends AbstractSearchPanel<Contract> implements FMEntityField {

	private static final long serialVersionUID = -686703278974497895L;
	private EntityService entityService = SpringUtils.getBean(EntityService.class);
	
	private TextField txtReference;
	private ERefDataComboBox<EWkfStatus> cbxContractStatus;
	private TextField txtFamilyNameEn;
	private TextField txtFirstNameEn;
	private ERefDataComboBox<EDealerType> cbxDealerType;

	private DealerComboBox cbxDealer;
	private EntityRefComboBox<Province> cbxProvince;
	private EntityRefComboBox<FinProduct> cbxFinancialProduct;
	public ContractsSearchPanel(AbstractTablePanel<Contract> contractTablePanel) {
		super(I18N.message("search"), contractTablePanel);
	}
	
	@Override
	protected void reset() {
		txtReference.setValue("");
		txtFamilyNameEn.setValue("");
		txtFirstNameEn.setValue("");
		cbxContractStatus.setSelectedEntity(null);
		cbxDealer.setSelectedEntity(null);
		cbxDealerType.setSelectedEntity(null);
		cbxProvince.setSelectedEntity(null);
		cbxFinancialProduct.setSelectedEntity(null);
	}


	@Override
	protected Component createForm() {		
		cbxFinancialProduct = new EntityRefComboBox<FinProduct>();
		BaseRestrictions<FinProduct> restrictionsFinancial = new BaseRestrictions<>(FinProduct.class);
		restrictionsFinancial.getStatusRecordList().add(EStatusRecord.ACTIV);

		cbxFinancialProduct.setRestrictions(restrictionsFinancial);		
		cbxFinancialProduct.setImmediate(true);
		cbxFinancialProduct.renderer();
		cbxFinancialProduct.setSelectedEntity(null);
		final GridLayout gridLayout = new GridLayout(11, 2);
		gridLayout.setSpacing(true);
		txtReference = ComponentFactory.getTextField(false, 60, 200);
		// remove some statuses as follow
		/*List<ContractStatus> listContractStatus = ContractStatus.list();
		listContractStatus.remove(ContractStatus.COM);
		listContractStatus.remove(ContractStatus.FRA);
		cbxContractStatus = new EnumComboBox<ContractStatus>(listContractStatus);
		cbxContractStatus.setSelectedEntity(ContractStatus.FIN);*/

		cbxContractStatus = new ERefDataComboBox<>(ContractWkfStatus.listContractStatus());
		cbxContractStatus.setWidth(150, Unit.PIXELS);
		
		txtFamilyNameEn = ComponentFactory.getTextField(false, 60, 200);	
		txtFirstNameEn = ComponentFactory.getTextField(false, 60, 200);
		cbxDealer = new DealerComboBox(null, DataReference.getInstance().getDealers(), I18N.message("all"));
		cbxDealer.setSelectedEntity(null);
		cbxDealer.setWidth("220px");

		cbxDealerType = new ERefDataComboBox<EDealerType>(EDealerType.class);
		cbxDealerType.setImmediate(true);
		cbxDealerType.addValueChangeListener(new ValueChangeListener() {
			/** */
			private static final long serialVersionUID = 4743882085175723521L;
			@Override
			public void valueChange(ValueChangeEvent event) {
				BaseRestrictions<Dealer> restrictions = new BaseRestrictions<Dealer>(Dealer.class);
				restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
				if (cbxDealerType.getSelectedEntity() != null) {
					restrictions.addCriterion(Restrictions.eq("dealerType", cbxDealerType.getSelectedEntity()));
				}
				cbxDealer.setDealers(entityService.list(restrictions));
				cbxDealer.setSelectedEntity(null);
			}
		});
        cbxProvince = new EntityRefComboBox<Province>();
		cbxProvince.setRestrictions(new BaseRestrictions<Province>(Province.class));
		cbxProvince.renderer();
        
        int iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("reference")), iCol++, 0);
        gridLayout.addComponent(txtReference, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("contract.status")), iCol++, 0);
        gridLayout.addComponent(cbxContractStatus, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("dealer.type")), iCol++, 0);
        gridLayout.addComponent(cbxDealerType, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("dealer")), iCol++, 0);
        gridLayout.addComponent(cbxDealer, iCol++, 0);
        
        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("lastname.en")), iCol++, 1);
        gridLayout.addComponent(txtFamilyNameEn, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("firstname.en")), iCol++, 1);
        gridLayout.addComponent(txtFirstNameEn, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("province")), iCol++, 1);
        gridLayout.addComponent(cbxProvince, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("financial.product")), iCol++, 1);
        gridLayout.addComponent(cbxFinancialProduct, iCol++, 1);
        
		return gridLayout;
	}
	
	@Override
	public BaseRestrictions<Contract> getRestrictions() {		
		BaseRestrictions<Contract> restrictions = new BaseRestrictions<Contract>(Contract.class);
		
		//restrictions.addCriterion(Restrictions.ne(CONTRACT_STATUS, ContractStatus.COM));
		
		if (StringUtils.isNotEmpty(txtReference.getValue())) {
			restrictions.addCriterion(Restrictions.like(REFERENCE, txtReference.getValue(), MatchMode.ANYWHERE));
		}
		if (cbxFinancialProduct.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("financialProduct", cbxFinancialProduct.getSelectedEntity()));
		}
		if (cbxContractStatus.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("wkfStatus", cbxContractStatus.getSelectedEntity()));
		}
		if (cbxDealerType.getSelectedEntity() != null) {
			restrictions.addAssociation("dealer", "quodeal", JoinType.INNER_JOIN);
			restrictions.addCriterion(Restrictions.eq("quodeal.dealerType", cbxDealerType.getSelectedEntity()));
		}
		
		if (cbxDealer.getSelectedEntity() != null) { 
			restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, cbxDealer.getSelectedEntity().getId()));
		}

		if (cbxProvince.getSelectedEntity() != null || StringUtils.isNotEmpty(txtFamilyNameEn.getValue())
				|| StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
			restrictions.addAssociation("quotations", "quot", JoinType.INNER_JOIN);
			restrictions.addAssociation("quot.quotationApplicants", "quoapp", JoinType.INNER_JOIN);
			restrictions.addAssociation("quoapp.applicant", "app", JoinType.INNER_JOIN);
			restrictions.addCriterion("quoapp.applicantType", EApplicantType.C);
			restrictions.addAssociation("app.individual","indi",JoinType.INNER_JOIN);
		}

		if (cbxProvince.getSelectedEntity() != null) {
			restrictions.addAssociation("indi.individualAddresses","iadd",JoinType.INNER_JOIN);
			restrictions.addAssociation("iadd.address","addr",JoinType.INNER_JOIN);
			restrictions.addAssociation("addr.province","pro",JoinType.INNER_JOIN);
			restrictions.addCriterion("pro.id", cbxProvince.getSelectedEntity().getId());
		}

		if (StringUtils.isNotEmpty(txtFamilyNameEn.getValue())) {
			restrictions.addCriterion(Restrictions.ilike("indi." + LAST_NAME_EN, txtFamilyNameEn.getValue(), MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
			restrictions.addCriterion(Restrictions.ilike("indi." + FIRST_NAME_EN, txtFirstNameEn.getValue(), MatchMode.ANYWHERE));
		}
		restrictions.addOrder(Order.desc(START_DATE));
		return restrictions;
	}

}
