package com.soma.mfinance.gui.ui.panel.report.lessee.info;

import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.shared.accounting.LeasesReport;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import org.seuksa.frmk.i18n.I18N;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * @author p.ing
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(LesseeInformationPanel.NAME)
public class LesseeInformationPanel extends AbstractTabPanel implements View, FinServicesHelper {

    private static final long serialVersionUID = 6227740006388204118L;

    protected Logger logger = LoggerFactory.getLogger(getClass());
    public static final String NAME = "lessee.information";
    private TabSheet tabSheet;
    private SimplePagedTable<Contract> pagedTable;
    private List<ColumnDefinition> columnDefinitions;
    private LesseeInformationSearchPanel lesseeInformationSearchPanel;
    private LesseeInformationTable lesseeInformationTable;
    private List<Contract> leasesReports;
    private AutoDateField dfEndDate;

    @PostConstruct
    public void PostConstruct() {
    }

    @Override
    public void enter(ViewChangeEvent event) {
        loadData();
    }

    @Override
    protected com.vaadin.ui.Component createForm() {
        lesseeInformationSearchPanel = new LesseeInformationSearchPanel();
        tabSheet = new TabSheet();
        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setSpacing(true);
        contentLayout.setMargin(true);

        VerticalLayout gridLayoutPanel = new VerticalLayout();
        VerticalLayout searchLayout = new VerticalLayout();
        HorizontalLayout buttonsLayout = new HorizontalLayout();
        Button btnSearch = new Button(I18N.message("search"));
        btnSearch.setClickShortcut(KeyCode.ENTER, null); // null it means we don't modify key of shortcut Enter(default = 13)
        btnSearch.setIcon(new ThemeResource("../smt-default/icons/16/search.png"));
        btnSearch.addClickListener(new ClickListener() {
            private static final long serialVersionUID = -3403059921454308342L;

            @Override
            public void buttonClick(ClickEvent event) {
                loadData();
            }
        });

        Button btnReset = new Button(I18N.message("reset"));
        btnReset.setIcon(new ThemeResource("../smt-default/icons/16/reset.png"));
        btnReset.addClickListener(new ClickListener() {
            private static final long serialVersionUID = -7165734546798826698L;

            @Override
            public void buttonClick(ClickEvent event) {
                reset();
            }
        });

        buttonsLayout.setSpacing(true);
        buttonsLayout.setStyleName("panel-search-center");
        buttonsLayout.addComponent(btnSearch);
        buttonsLayout.addComponent(btnReset);
        gridLayoutPanel.addComponent(lesseeInformationSearchPanel.getSearchForm());
        searchLayout.setMargin(true);
        searchLayout.setSpacing(true);
        searchLayout.addComponent(gridLayoutPanel);
        searchLayout.addComponent(buttonsLayout);

        Panel searchPanel = new Panel();
        searchPanel.setCaption(I18N.message("search"));
        searchPanel.setContent(searchLayout);
        lesseeInformationTable = new LesseeInformationTable();
        this.columnDefinitions = lesseeInformationTable.getHeader();
        pagedTable = new SimplePagedTable<Contract>(this.columnDefinitions);
        lesseeInformationTable.setPagedTable(pagedTable);
        contentLayout.addComponent(searchPanel);
        contentLayout.addComponent(pagedTable);
        contentLayout.addComponent(pagedTable.createControls());
        tabSheet.addTab(contentLayout, I18N.message("lessee.information"));
        return tabSheet;
    }

    private void loadData() {
        leasesReports = lesseeInformationSearchPanel.getLeaseInformation();
        dfEndDate = lesseeInformationSearchPanel.getDfEndDate();
        lesseeInformationTable.getData(leasesReports);
    }

}
