package com.soma.mfinance.gui.ui.panel.report.summary.report;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.seuksa.frmk.tools.DateUtils;
import com.soma.mfinance.core.asset.model.EFrequencyType;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.entityfield.InstallmentEntityField;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.ui.Button;
import com.vaadin.ui.Table.Align;

/**
 * 
 * @author p.ly
 *
 */
public class SummaryReportTable implements InstallmentEntityField, FinServicesHelper {
	private List<ColumnDefinition> columnDefinitions;
	private SimplePagedTable<Contract> pagedTable;
	private Button btnSearch;
	public SummaryReportTable(Button btnSearch){
		this.btnSearch = btnSearch;
	}
	
	public List<ColumnDefinition> getHeader(EFrequencyType type){
		columnDefinitions = new ArrayList<ColumnDefinition>();
		String header ="";
		if(type!=null){
			header =type.getDescEn();
		}
		columnDefinitions.add(new ColumnDefinition("type", header, String.class, Align.LEFT, 140));
		columnDefinitions.add(new ColumnDefinition("dealer","Dealer", String.class, Align.LEFT, 140));
		columnDefinitions.add(new ColumnDefinition("number.contract","Number Contract", Integer.class, Align.LEFT, 140));
		columnDefinitions.add(new ColumnDefinition("lease.amount", "Lease Amount", Double.class, Align.CENTER, 140));
		return columnDefinitions;
	}
	/**
	 * 
	 * @param Contract
	 */
	public void getData(List<Contract> contracts ,Date startDate ,Date endDate ,EFrequencyType type){
		Indexed indexedContainer = pagedTable.getContainerDataSource();
		indexedContainer.removeAllItems();
		int index = 0;
		if (contracts != null && !contracts.isEmpty()) {
			    if(type.getDescEn().equalsIgnoreCase("Daily")){
			    	int i = Math.abs(Integer.parseInt(DateUtils.getDiffInDays(startDate, endDate).toString()));
				    for(int j = 0;j < i ; j++){
				    	Date currentDay = DateUtils.addDaysDate(startDate, j);
				    	double leaseAmount = 0d;
						int numberContract = 0;
						String dealer = "All";
						for (Contract contract : contracts) {
							Date date1 = contract.getStartDate();
							dealer = contract.getDealer().toString();
							if(DateUtils.isSameDay(DateUtils.getDateAtBeginningOfDay(currentDay),DateUtils.getDateAtBeginningOfDay(date1))){
								leaseAmount += contract.getTiFinancedAmount();
								numberContract++;
							}
						}
						final Item item = indexedContainer.addItem(index);
						item.getItemProperty("type").setValue(DateUtils.formatDate(currentDay, "yyyy/MM/dd").toString());
						item.getItemProperty("dealer").setValue(dealer);
						item.getItemProperty("number.contract").setValue(numberContract);
						item.getItemProperty("lease.amount").setValue(leaseAmount);
						index++;
				    }
				    
			    }else if(type.getDescEn().equalsIgnoreCase("Weekly")){
			    	int i = Integer.parseInt(DateUtils.getDiffInDays(endDate,startDate).toString());
			    	int j = (int) Math.ceil((float)i/7);
			    	Date currentDay = DateUtils.getDateAtBeginningOfDay(startDate);
			    	for(int k=1;k<=j;k++){
			    		Date endWeek = DateUtils.getDateAtBeginningOfDay(DateUtils.getDateAtEndOfWeek(currentDay));
			    		double leaseAmount = 0d;
						int numberContract = 0;
						String dealer = "All";
			    		for (Contract contract : contracts) {
			    			Date date1 = DateUtils.getDateAtBeginningOfDay(contract.getStartDate());
			    			dealer = contract.getDealer().toString();
			    			if(DateUtils.isSameDay(date1, currentDay) || date1.after(currentDay) && date1.before(DateUtils.addDaysDate(endWeek, 1)) || DateUtils.isSameDay(date1, DateUtils.addDaysDate(endWeek, 1))){
			    				leaseAmount += contract.getTiFinancedAmount();
								numberContract++;
			    			}
			    		}
			    		Integer week = (Integer)k;
			    		final Item item = indexedContainer.addItem(index);
			    		item.getItemProperty("type").setValue(week != null ? week.toString(): "0");
						item.getItemProperty("dealer").setValue(dealer);
						item.getItemProperty("number.contract").setValue(numberContract);
						item.getItemProperty("lease.amount").setValue(leaseAmount);
						currentDay = DateUtils.addDaysDate(endWeek, 2);
						index++;		    		
			    	}

			    }else if(type.getDescEn().equalsIgnoreCase("Monthly")){
			    	int i = DateUtils.getNumberMonthOfTwoDates(endDate,startDate);
			    	Date currentDayofMonth = DateUtils.getDateAtBeginningOfDay(startDate);
			    	for(int k=1;k<=i;k++){
			    		Date endMonth = DateUtils.getDateAtBeginningOfDay(DateUtils.getDateAtEndOfMonth(currentDayofMonth));
			    		double leaseAmount = 0d;
						int numberContract = 0;
						String dealer = "All";
						for (Contract contract : contracts) {
			    			Date date1 = DateUtils.getDateAtBeginningOfDay(contract.getStartDate());
			    			dealer = contract.getDealer().toString();
			    			if(DateUtils.isSameDay(date1, currentDayofMonth) || date1.after(currentDayofMonth) && date1.before(endMonth) || DateUtils.isSameDay(date1, endMonth)){
			    				leaseAmount += contract.getTiFinancedAmount();
								numberContract++;
			    			}
			    		}
						Integer month = (Integer)k;
			    		final Item item = indexedContainer.addItem(index);
			    		item.getItemProperty("type").setValue(month != null ? month.toString(): "0");
						item.getItemProperty("dealer").setValue(dealer);
						item.getItemProperty("number.contract").setValue(numberContract);
						item.getItemProperty("lease.amount").setValue(leaseAmount);
						currentDayofMonth = DateUtils.addDaysDate(endMonth,1);
						index++;
			    	}
			    	
			    }else if(type.getDescEn().equalsIgnoreCase("Yearly")){
			    	int i = DateUtils.getNumberYearOfTwoDates(endDate,startDate);
			    	Date currentDayofYear = DateUtils.getDateAtBeginningOfDay(startDate);
			    	for(int k=1;k<=i;k++){
			    		Date endYear = DateUtils.getDateAtBeginningOfDay(DateUtils.getDateAtEndOfYear(currentDayofYear));
			    		double leaseAmount = 0d;
						int numberContract = 0;
						String dealer = "All";
						for (Contract contract : contracts) {
			    			Date date1 = DateUtils.getDateAtBeginningOfDay(contract.getStartDate());
			    			dealer = contract.getDealer().toString();
			    			if(DateUtils.isSameDay(date1, currentDayofYear) || date1.after(currentDayofYear) && date1.before(endYear) || DateUtils.isSameDay(date1, endYear)){
			    				leaseAmount += contract.getTiFinancedAmount();
								numberContract++;
			    			}
			    		}
						Integer year = (Integer)k;
			    		final Item item = indexedContainer.addItem(index);
			    		item.getItemProperty("type").setValue(year != null ? year.toString(): "0");
						item.getItemProperty("dealer").setValue(dealer);
						item.getItemProperty("number.contract").setValue(numberContract);
						item.getItemProperty("lease.amount").setValue(leaseAmount);
						currentDayofYear = DateUtils.addDaysDate(endYear, 1);
						index++;
			    	}
			    }
		}
	   pagedTable.refreshContainerDataSource();
	}
	private int lastPaidNum(boolean b, int i) {
		// TODO Auto-generated method stub
		return 0;
	}
	/**
	 * 
	 * @param intValue
	 * @return
	 */
	private int getInteger(Integer intValue){
		if(intValue == null){
			return 0;
		} else {
			return intValue;
		}
	}
	
	/**
	 * @return the pagedTable
	 */
	public SimplePagedTable<Contract> getPagedTable() {
		return pagedTable;
	}
	/**
	 * @param pagedTable the pagedTable to set
	 */
	public void setPagedTable(SimplePagedTable<Contract> pagedTable) {
		this.pagedTable = pagedTable;
	}
}
