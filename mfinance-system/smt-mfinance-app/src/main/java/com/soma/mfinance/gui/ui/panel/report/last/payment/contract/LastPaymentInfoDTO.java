package com.soma.mfinance.gui.ui.panel.report.last.payment.contract;

import java.util.Date;

/**
 * @author by kimsuor.seang  on 11/22/2017.
 */
public class LastPaymentInfoDTO {
    private Long id;
    private String reference;
    private String lastnameEn;
    private String firstNameEn;
    private String lastnameKh;
    private String firstNameKh;
    private String assetDescEn;
    private Date firstPaymentDate;
    private int term;
    private String dealer;
    private String quotationStatus;
    private String provice;
    private String district;
    private String commune;
    private String village;
    private String phone1;
    private String phone2;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getLastnameEn() {
        return lastnameEn;
    }

    public void setLastnameEn(String lastnameEn) {
        this.lastnameEn = lastnameEn;
    }

    public String getFirstNameEn() {
        return firstNameEn;
    }

    public void setFirstNameEn(String firstNameEn) {
        this.firstNameEn = firstNameEn;
    }

    public String getLastnameKh() {
        return lastnameKh;
    }

    public void setLastnameKh(String lastnameKh) {
        this.lastnameKh = lastnameKh;
    }

    public String getFirstNameKh() {
        return firstNameKh;
    }

    public void setFirstNameKh(String firstNameKh) {
        this.firstNameKh = firstNameKh;
    }

    public String getAssetDescEn() {
        return assetDescEn;
    }

    public void setAssetDescEn(String assetDescEn) {
        this.assetDescEn = assetDescEn;
    }

    public Date getFirstPaymentDate() {
        return firstPaymentDate;
    }

    public void setFirstPaymentDate(Date firstPaymentDate) {
        this.firstPaymentDate = firstPaymentDate;
    }

    public int getTerm() {
        return term;
    }

    public void setTerm(int term) {
        this.term = term;
    }

    public String getDealer() {
        return dealer;
    }

    public void setDealer(String dealer) {
        this.dealer = dealer;
    }

    public String getQuotationStatus() {
        return quotationStatus;
    }

    public void setQuotationStatus(String quotationStatus) {
        this.quotationStatus = quotationStatus;
    }

    public String getProvice() {
        return provice;
    }

    public void setProvice(String provice) {
        this.provice = provice;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }
}