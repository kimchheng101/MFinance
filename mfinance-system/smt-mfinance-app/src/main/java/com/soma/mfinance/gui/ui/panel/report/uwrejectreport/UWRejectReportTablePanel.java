package com.soma.mfinance.gui.ui.panel.report.uwrejectreport;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.applicant.model.Employment;
import com.soma.mfinance.core.quotation.model.Comment;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationSupportDecision;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.RowRenderer;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.data.Item;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.Entity;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.amount.AmountUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

/**
 * @author kimsuor.seang
 * @modified p.ly
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class UWRejectReportTablePanel extends AbstractTablePanel<Quotation> implements FMEntityField {

    private static final long serialVersionUID = 5204101495689053450L;
    private static final String QUALITY_INCENTIVE = "uw.reject.report";
    private Quotation quotation;
    private double monthlyInstallment = 0;

    /**
     */
    @PostConstruct
    public void PostConstruct() {

        setCaption(I18N.message(QUALITY_INCENTIVE));
        setSizeFull();
        setMargin(true);
        setSpacing(true);

        super.init("");
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTablePanel#createPagedDataProvider()
     */
    @Override
    protected PagedDataProvider<Quotation> createPagedDataProvider() {
        PagedDefinition<Quotation> pagedDefinition = new PagedDefinition<Quotation>(searchPanel.getRestrictions());

        pagedDefinition.setRowRenderer(new QualityIncentiveRowRenderer());
        pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 50);
        pagedDefinition.addColumnDefinition(FINANCIAL_PRODUCT , I18N.message("financial.product"), String.class, Align.LEFT, 110);
        pagedDefinition.addColumnDefinition(CUSTOMER, I18N.message("customers"), String.class, Align.LEFT, 120);
        pagedDefinition.addColumnDefinition(DEALER, I18N.message("dealer"), String.class, Align.LEFT, 230);
        pagedDefinition.addColumnDefinition("apply.date", I18N.message("apply.date"), Date.class, Align.LEFT, 110);
        pagedDefinition.addColumnDefinition("reject.date", I18N.message("reject.date"), Date.class, Align.LEFT, 110);
        pagedDefinition.addColumnDefinition("reject.resons", I18N.message("reject.resons"), String.class, Align.LEFT, 230);
        pagedDefinition.addColumnDefinition("field.check", I18N.message("field.check"), String.class, Align.LEFT, 110);
        pagedDefinition.addColumnDefinition("lease.term", I18N.message("lease.term"), Integer.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("lease.amount.percentage", I18N.message("lease.amount.percentage"), Double.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("repayment.cap.by.pos", I18N.message("repayment.cap.by.pos"), String.class, Align.LEFT, 150);
        pagedDefinition.addColumnDefinition("repayment.cap.by.uw", I18N.message("repayment.cap.by.uw"), String.class, Align.LEFT, 180);
        pagedDefinition.addColumnDefinition("uw", I18N.message("uw"), String.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("us", I18N.message("us"), String.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("employment.status", I18N.message("employment.status"), String.class, Align.LEFT, 120);
        pagedDefinition.addColumnDefinition("applicant.position", I18N.message("applicant.position"), String.class, Align.LEFT, 120);
        pagedDefinition.addColumnDefinition("applicant.occupation", "Applicant Occupation", String.class, Align.LEFT, 120);
        pagedDefinition.addColumnDefinition("employment.industry", I18N.message("employment.industry"), String.class, Align.LEFT, 120);
        pagedDefinition.addColumnDefinition("gender", I18N.message("gender"), String.class, Align.LEFT, 70);
        pagedDefinition.addColumnDefinition("housing", I18N.message("housing"), String.class, Align.LEFT, 70);
        pagedDefinition.addColumnDefinition("net.income", I18N.message("net.income"), String.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("liability", I18N.message("liability"), String.class, Align.LEFT, 60);
        pagedDefinition.addColumnDefinition("glf.installment", I18N.message("installment"), String.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("us.comment", I18N.message("us.comment"), String.class, Align.LEFT, 300);

        EntityPagedDataProvider<Quotation> pagedDataProvider = new EntityPagedDataProvider<Quotation>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    /**
     * @author kimsuor.seang
     */
    private class QualityIncentiveRowRenderer implements RowRenderer {
        @SuppressWarnings("unchecked")
        @Override
        public void renderer(Item item, Entity entity) {
            quotation = (Quotation) entity;
            List<QuotationSupportDecision> quotationSupportDecisions = quotation.getQuotationSupportDecisions(QuotationWkfStatus.REJ);
            item.getItemProperty(ID).setValue(quotation.getId());
            item.getItemProperty(FINANCIAL_PRODUCT).setValue(quotation.getFinancialProduct().getDescEn());
            if (quotation.getApplicant() != null) {
                item.getItemProperty(CUSTOMER).setValue(quotation.getApplicant().getIndividual().getLastNameEn() + " " + quotation.getApplicant().getIndividual().getFirstNameEn());
            }
            if (quotation.getDealer() != null) {
                item.getItemProperty(DEALER).setValue(quotation.getDealer().getNameEn());
            }
            if (quotation.getFirstSubmissionDate() != null) {
                item.getItemProperty("apply.date").setValue(quotation.getFirstSubmissionDate());
            }
            if (quotation.getRejectDate() != null)
                item.getItemProperty("reject.date").setValue(quotation.getRejectDate());

            if (quotationSupportDecisions != null) {
                for (QuotationSupportDecision quotationSupportDecision : quotationSupportDecisions) {
                    if (quotationSupportDecision.getSupportDecision() != null) {
                        item.getItemProperty("reject.resons").setValue(quotationSupportDecision.getSupportDecision().getDescEn());
                    }
                }
            }
            if (quotation.isFieldCheckPerformed() != null)
                item.getItemProperty("field.check").setValue(quotation.isFieldCheckPerformed() ? I18N.message("yes") : I18N.message("no"));

            item.getItemProperty("repayment.cap.by.pos").setValue(AmountUtils.format(getRationByCOInterview()));
            if(getRationByUW() != null)
                item.getItemProperty("repayment.cap.by.uw").setValue(AmountUtils.format(getRationByUW()));

            if (quotation.getTerm() != null)
                item.getItemProperty("lease.term").setValue(quotation.getTerm());

            item.getItemProperty("lease.amount.percentage").setValue(quotation.getLeaseAmountPercentage() != null ? quotation.getLeaseAmountPercentage(): 0.0);

            if (quotation.getUnderwriter() != null) {
                item.getItemProperty("uw").setValue(quotation.getUnderwriter().getDesc());
            }
            if (quotation.getUnderwriterSupervisor() != null) {
                item.getItemProperty("us").setValue(quotation.getUnderwriterSupervisor().getDesc());
            }
            if (quotation.getQuotationApplicant(EApplicantType.C) != null) {
                Applicant applicant = quotation.getQuotationApplicant(EApplicantType.C).getApplicant();
                if (applicant != null) {
                    Employment employment = applicant.getIndividual().getCurrentEmployment();
                    if (employment != null) {
                        item.getItemProperty("employment.status").setValue(employment.getEmploymentStatus() != null ? employment.getEmploymentStatus().getDescEn() : "");
                        item.getItemProperty("applicant.position").setValue(employment.getEmploymentPosition() != null ? employment.getEmploymentPosition().getDescEn() : "");
                        item.getItemProperty("applicant.occupation").setValue(employment.getEmploymentOccupation() != null ? employment.getEmploymentOccupation().getDescEn() : "");
                        item.getItemProperty("employment.industry").setValue(employment.getEmploymentIndustry() != null ? employment.getEmploymentIndustry().getDescEn() : "");
                    }
                    item.getItemProperty("gender").setValue(applicant.getIndividual().getGender() != null ? applicant.getIndividual().getGender().getDescEn() : "");
                    if (applicant.getIndividual() != null && applicant.getIndividual().getIndividualAddresses() != null && !applicant.getIndividual().getIndividualAddresses().isEmpty() && applicant.getIndividual().getIndividualAddresses().get(0) != null && applicant.getIndividual().getIndividualAddresses().get(0).getHousing() != null) {
                        item.getItemProperty("housing").setValue(applicant.getIndividual().getIndividualAddresses().get(0).getHousing().getDescEn());
                    }
                }
            }
            item.getItemProperty("net.income").setValue(quotation.getUwNetIncomeEstimation() != null ? AmountUtils.format(quotation.getUwNetIncomeEstimation()) : "0.0");
            item.getItemProperty("liability").setValue(quotation.getUwLiabilityEstimation() != null ? AmountUtils.format(quotation.getUwLiabilityEstimation()) : "");
            if (quotation.getComments() != null && !quotation.getComments().isEmpty()) {
                for (Comment comment : quotation.getComments()) {
                    if (comment.isOnlyForUS())
                        item.getItemProperty("us.comment").setValue(comment.getDesc());
                }
            }
            monthlyInstallment = quotation.getTotalInstallmentAmount() != null ? quotation.getTotalInstallmentAmount() : 0.0d;
            item.getItemProperty("glf.installment").setValue(AmountUtils.format(monthlyInstallment));
        }
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTablePanel#getEntity()
     */
    @Override
    protected Quotation getEntity() {
        return null;
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTablePanel#createSearchPanel()
     */
    @Override
    protected AbstractSearchPanel<Quotation> createSearchPanel() {
        return new UWRejectReportSearchPanel(this);
    }

    private Double getRationByCOInterview(){
        Applicant customer = quotation.getApplicant();
        double totalNetIncome = 0d;
        double totalRevenus = 0d;
        double totalAllowance = 0d;
        double totalBusinessExpenses = 0d;
        double totalDebtInstallment = MyNumberUtils.getDouble(customer.getIndividual().getTotalDebtInstallment());
        List<Employment> employments = customer.getIndividual().getEmployments();
        for (Employment employment : employments) {
            totalRevenus += MyNumberUtils.getDouble(employment.getRevenue());
            totalAllowance += MyNumberUtils.getDouble(employment.getAllowance());
            totalBusinessExpenses += MyNumberUtils.getDouble(employment
                    .getBusinessExpense());
        }
        totalNetIncome = totalRevenus + totalAllowance - totalBusinessExpenses;

        double totalExpenses = MyNumberUtils.getDouble(customer.getIndividual().getMonthlyPersonalExpenses())
                + MyNumberUtils.getDouble(customer.getIndividual().getMonthlyFamilyExpenses())	+ totalDebtInstallment;

        double disposableIncome = totalNetIncome - totalExpenses;
        monthlyInstallment = quotation.getTotalInstallmentAmount() != null ? quotation.getTotalInstallmentAmount() : 0.0d;
        double customerRatio = disposableIncome / monthlyInstallment;

        return customerRatio;
    }
    private Double getRationByUW(){
        double uwNetIncome = MyNumberUtils.getDouble(quotation.getUwRevenuEstimation()) + MyNumberUtils.getDouble(quotation.getUwAllowanceEstimation()) - MyNumberUtils.getDouble(quotation.getUwBusinessExpensesEstimation());
        double uwDisposableIncome = uwNetIncome - MyNumberUtils.getDouble(quotation.getUwPersonalExpensesEstimation()) - MyNumberUtils.getDouble(quotation.getUwFamilyExpensesEstimation()) - MyNumberUtils.getDouble(quotation.getUwLiabilityEstimation());
        double underwriterRatio = uwDisposableIncome / monthlyInstallment;
        return underwriterRatio;
    }

}
