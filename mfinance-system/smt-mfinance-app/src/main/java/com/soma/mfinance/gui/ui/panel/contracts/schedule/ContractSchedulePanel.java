package com.soma.mfinance.gui.ui.panel.contracts.schedule;

import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.schedule.ContractActivateSchedule;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.ersys.core.hr.model.address.Province;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.Reindeer;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.vaadin.dialogs.ConfirmDialog;
import ru.xpoft.vaadin.VaadinView;

import java.util.*;

import static com.soma.mfinance.core.helper.FinServicesHelper.CSMS_SRV;
import static com.soma.mfinance.core.helper.FinServicesHelper.QUO_SRV;

/**
 * Created by kimsuor.seang
 * USER   c.chhai
 * DATE on 6/8/2017.
 * TIME at 9:50 AM
 * EMIAL   c.chhai@gl-f.com
 */

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(ContractSchedulePanel.NAME)
public class ContractSchedulePanel extends AbstractTabPanel implements View, QuotationEntityField {

	public static final String NAME = "contract.schedule";
	private AutoDateField dfStartDate;
	private AutoDateField dfEndDate;
	private TabSheet tabSheet;
	private Set<Long> selectedItemIds;
	private List<Quotation> quotationList;
	private List<ColumnDefinition> columnDefinitions;
	private SimplePagedTable<Quotation> pagedTable;
	private boolean selectAll = false;
	private EntityRefComboBox<EDealerType> cbxDealerType;
	private EntityRefComboBox<Province> cbxProvince;
	private CheckBox cbExecuted;
	private Boolean booleanCheck = true;

	private ContractActivateSchedule  contractActivateSchedule;

	public ContractSchedulePanel() {

		super();
		setSizeFull();
		setMargin(false);

		refresh();

	}

	@Override
	protected com.vaadin.ui.Component createForm() {

			tabSheet = new TabSheet();
			VerticalLayout contentLayout = new VerticalLayout();
			contentLayout.setSpacing(true);
			contentLayout.setMargin(true);
			contentLayout.setStyleName("panel-search");

			selectedItemIds = new HashSet<Long>();
			quotationList = new ArrayList<>();
			this.columnDefinitions = createColumnDefinitions();
			pagedTable = new SimplePagedTable<Quotation>(this.columnDefinitions);
			pagedTable.setCaption(I18N.message("contract"));
			pagedTable.setColumnIcon("selectall", new ThemeResource("../smt-default/icons/16/tick.png"));
			pagedTable.addHeaderClickListener(new com.vaadin.ui.Table.HeaderClickListener() {
				private static final long serialVersionUID = 5868260573740919228L;
				@Override
				public void headerClick(Table.HeaderClickEvent event) {
					if (event.getPropertyId() == "selectall") {
						selectAll = !selectAll;
						search();
					}
				}
			});

			contentLayout.addComponent(createNavigationPanel());
			contentLayout.addComponent(createSearchPanel());
			contentLayout.addComponent(pagedTable);
			contentLayout.addComponent(pagedTable.createControls());
			tabSheet.addTab(contentLayout, I18N.message("contract.schedule"));

		return tabSheet;
	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent event) {

	}

	protected List<ColumnDefinition> createColumnDefinitions() {

			columnDefinitions = new ArrayList<ColumnDefinition>();
			columnDefinitions.add(new ColumnDefinition("selectall", I18N.message("select"), CheckBox.class, Table.Align.LEFT, 70));
			columnDefinitions.add(new ColumnDefinition(ID, I18N.message("id"), Long.class, Table.Align.LEFT, 70, true));
			columnDefinitions.add(new ColumnDefinition(REFERENCE, I18N.message("reference"), String.class, Table.Align.LEFT, 150));
			columnDefinitions.add(new ColumnDefinition("applicant." + LAST_NAME_EN, I18N.message("lastname.en"), String.class, Table.Align.LEFT, 120));
			columnDefinitions.add(new ColumnDefinition("applicant." + FIRST_NAME_EN, I18N.message("firstname.en"), String.class, Table.Align.LEFT, 120));
			columnDefinitions.add(new ColumnDefinition("asset." + DESC_EN, I18N.message("asset"), String.class, Table.Align.LEFT, 150));
			columnDefinitions.add(new ColumnDefinition("financialProduct." + DESC_EN, I18N.message("financial.product"), String.class, Table.Align.LEFT, 150));
			columnDefinitions.add(new ColumnDefinition("status."+ STATUS, I18N.message("status"), String.class, Table.Align.LEFT, 130));
			columnDefinitions.add(new ColumnDefinition("dealer." + NAME_EN, I18N.message("dealer"), String.class, Table.Align.LEFT, 150));
			columnDefinitions.add(new ColumnDefinition("executed." + EXECUTED,I18N.message("executed"),Boolean.class, Table.Align.LEFT, 140));
			columnDefinitions.add(new ColumnDefinition("performUser." + PERFORM_USER,I18N.message("perform.user"),String.class, Table.Align.LEFT, 140));
			columnDefinitions.add(new ColumnDefinition("performDate." + PERFORM_DATE,I18N.message("perform.date"),Date.class, Table.Align.LEFT, 140));
			columnDefinitions.add(new ColumnDefinition("executedDate." + EXECUTED_DATE,I18N.message("executed.date"),Date.class, Table.Align.LEFT, 140));

		return columnDefinitions;
	}

	private NavigationPanel createNavigationPanel() {

		NavigationPanel navigationPanel = new NavigationPanel();
		/**
		 * FORM ALERT MESSAGE ACTIVATION
		 */
		FormLayout formLayout = new FormLayout();
		formLayout.setMargin(true);
		formLayout.setSpacing(true);
		final DateField dfContractStartDate = ComponentFactory.getAutoDateField(I18N.message("contract.start.date"), true);
		dfContractStartDate.setValue(DateUtils.today());
		dfContractStartDate.setEnabled(false);
		final DateField dfFirstPaymentDate = ComponentFactory.getAutoDateField(I18N.message("first.payment.date"), true);
		dfFirstPaymentDate.setValue(DateUtils.addMonthsDate(DateUtils.today(), 1));

		dfContractStartDate.setWidth(95, Unit.PIXELS);
		dfFirstPaymentDate.setWidth(95, Unit.PIXELS);

		formLayout.addComponent(dfContractStartDate);
		formLayout.addComponent(dfFirstPaymentDate);


		Button btnActivate = new NativeButton(I18N.message("activate"));
		btnActivate.setIcon(new ThemeResource("../smt-default/icons/16/tick.png"));
		/**
		 * ACTION BUTTON ACTIVATE
		 */
		btnActivate.addClickListener(new Button.ClickListener() {
				private static final long serialVersionUID = 582828827319591959L;
				@Override
				public void buttonClick(Button.ClickEvent event) {
					if (selectedItemIds.isEmpty()) {
						MessageBox mb = new MessageBox(UI.getCurrent(), "400px", "160px", I18N.message("information"), MessageBox.Icon.ERROR, I18N.message("Please select quotation to activated"), Alignment.MIDDLE_RIGHT, new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, I18N.message("ok")));
						mb.show();
					} else {
						for(Quotation quotation : CSMS_SRV.quotationListSelected(selectedItemIds)){
							//Quotation activate already
							if(CSMS_SRV.isExecutedContractActivateSchedule(quotation).equals(true)){
								MessageBox mb = new MessageBox(UI.getCurrent(), "400px", "160px", I18N.message("information"), MessageBox.Icon.ERROR, I18N.message("Sorry ! This quotation is activated already"), Alignment.MIDDLE_RIGHT, new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, I18N.message("ok")));
								mb.show();
							}else{
								ConfirmDialog confirmDialog = ConfirmDialog.show(UI.getCurrent(), I18N.message("do.you.want.to.activated.this.quotation"),
										new ConfirmDialog.Listener() {
											private static final long serialVersionUID = 934662992018203665L;
											public void onClose(ConfirmDialog dialog) {
												if (dialog.isConfirmed()) {
													//Activate Quotation
													//QUO_SRV.activateQuotation(quotation.getId());
													//Update Item Contract Schedule Execute true
													CSMS_SRV.executedContractActivateSchedule(quotation);
													Panel msgPanel = new Panel();
													msgPanel.setStyleName(Reindeer.PANEL_LIGHT);
													msgPanel.setSizeFull();
													msgPanel.setContent(new Label("Activated", ContentMode.HTML));
													MessageBox mb = new MessageBox(UI.getCurrent(), "500px", "200px", I18N.message("information"), MessageBox.Icon.INFO, msgPanel, Alignment.MIDDLE_RIGHT, new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, I18N.message("ok")));
													mb.show();
													refresh();
												}
											}
										});
								confirmDialog.setWidth("400px");
								confirmDialog.setHeight("150px");
								confirmDialog.getOkButton().setDisableOnClick(true);
							}
						}
					}
				}
			});
			navigationPanel.addButton(btnActivate);

		return navigationPanel;
	}

	/**
	 * REFRESH
	 */
	public void refresh() {
		selectAll = false;
		search();
	}

	/**
	 * SEARCH
	 */
	private void search() {
		selectedItemIds.clear();
		List<ContractActivateSchedule> contractActivateSchedulesList = ENTITY_SRV.list(getContractActivateScheduleBaseRestrictions());
 		setIndexedContainer(contractActivateSchedulesList);
	}

	/**
	 * FILTER QUOTATION IN SCHEDULE
	 * @return
	 */
	public BaseRestrictions<ContractActivateSchedule>  getContractActivateScheduleBaseRestrictions(){

		BaseRestrictions<ContractActivateSchedule> contractActivateScheduleBaseRestrictions = new BaseRestrictions<>(ContractActivateSchedule.class);
			contractActivateScheduleBaseRestrictions.addAssociation("quotation", "quot",JoinType.LEFT_OUTER_JOIN);

			if (cbxDealerType.getSelectedEntity() != null) {
				contractActivateScheduleBaseRestrictions.addAssociation("quot.dealer", "quodeal", JoinType.INNER_JOIN);
				contractActivateScheduleBaseRestrictions.addCriterion(Restrictions.eq("quodeal.dealerType", cbxDealerType.getSelectedEntity()));
			}

			if (cbxProvince.getSelectedEntity() != null) {
				contractActivateScheduleBaseRestrictions.addAssociation("quot.quotationApplicants","quoapp",JoinType.INNER_JOIN);
				contractActivateScheduleBaseRestrictions.addAssociation("quoapp.applicant", "app", JoinType.INNER_JOIN);
				contractActivateScheduleBaseRestrictions.addAssociation("app.individual","indi",JoinType.INNER_JOIN);
				contractActivateScheduleBaseRestrictions.addAssociation("indi.individualAddresses","iadd",JoinType.INNER_JOIN);
				contractActivateScheduleBaseRestrictions.addAssociation("iadd.address","addr",JoinType.INNER_JOIN);
				contractActivateScheduleBaseRestrictions.addAssociation("addr.province","pro",JoinType.INNER_JOIN);
				contractActivateScheduleBaseRestrictions.addCriterion("pro.id", cbxProvince.getSelectedEntity().getId());
			}

			if (dfStartDate.getValue() != null ||  dfEndDate.getValue() != null) {
				contractActivateScheduleBaseRestrictions.addCriterion(Restrictions.between("performDate", DateUtils.getDateAtBeginningOfDay(dfStartDate.getValue()), DateUtils.getDateAtEndOfDay(dfEndDate.getValue())));
			}
			if(cbExecuted.getValue() == true){
				contractActivateScheduleBaseRestrictions.addCriterion(Restrictions.eq("executed",true));
			}else{
				contractActivateScheduleBaseRestrictions.addCriterion(Restrictions.eq("executed", false));
			}

		return  contractActivateScheduleBaseRestrictions;
	}


	/**
	 * TABLE VIEW QUOTATION IN TABLE SCHEDULE
	 * @param contractActivateScheduleList
	 */
	private void setIndexedContainer(List<ContractActivateSchedule> contractActivateScheduleList) {
		Container.Indexed indexedContainer = pagedTable.getContainerDataSource();
		indexedContainer.removeAllItems();
		int index = 0;
		for (ContractActivateSchedule contractActivateSchedule : contractActivateScheduleList) {

				Item item = indexedContainer.addItem(contractActivateSchedule.getId());
			Quotation quotation = contractActivateSchedule.getQuotation();
				item.getItemProperty("selectall").setValue(getRenderSelected(quotation.getId(), index++));
				item.getItemProperty(ID).setValue(quotation.getId());
				item.getItemProperty(REFERENCE).setValue(quotation.getReference());

			Contract contract = contractActivateSchedule.getQuotation().getContract();
				item.getItemProperty("applicant." + LAST_NAME_EN).setValue(quotation.getMainApplicant() != null ? quotation.getMainApplicant().getLastNameEn() : "");
				item.getItemProperty("applicant." + FIRST_NAME_EN).setValue(quotation.getMainApplicant() != null ? quotation.getMainApplicant().getFirstNameEn() : "");
				item.getItemProperty("asset." + DESC_EN).setValue(quotation.getAsset() !=null ? quotation.getAsset().getDescEn() : "");
				item.getItemProperty("financialProduct." + DESC_EN).setValue(quotation.getFinancialProduct() != null ? quotation.getFinancialProduct().getDescEn() : "");
				item.getItemProperty("status." + STATUS).setValue(quotation.getWkfStatus().getName());
				item.getItemProperty("dealer." + NAME_EN).setValue(quotation.getDealer() != null ? quotation.getDealer().getNameEn() : "");
				item.getItemProperty("executed." + EXECUTED).setValue(contractActivateSchedule.getExecuted());
				item.getItemProperty("performUser." + PERFORM_USER).setValue(contractActivateSchedule.getPerformUser().getUsername());
				item.getItemProperty("performDate." + PERFORM_DATE).setValue(contractActivateSchedule.getPerformDate());
				item.getItemProperty("exexutedDate."+ EXECUTED_DATE).setValue(contractActivateSchedule.getExecutedDate());

		}
		pagedTable.refreshContainerDataSource();
	}

	/**
	 * CHECK BOX SELECTED QUOTATION TO ACTIVATE
	 * @param quotationId
	 * @param index
	 * @return
	 */
	private CheckBox getRenderSelected(final Long quotationId, final int index) {

			final CheckBox checkBox = new CheckBox();
			boolean check = false;
			if (selectAll) {
				if (!selectedItemIds.contains(quotationId)) {
					check = true;
					selectedItemIds.add(quotationId);
				}
			} else if (selectedItemIds.contains(quotationId)) {
				check = true;
			}

			checkBox.setValue(check);
			checkBox.setData(quotationId);
			checkBox.addValueChangeListener(new Property.ValueChangeListener() {
				private static final long serialVersionUID = 5571858003324968541L;
				@Override
				public void valueChange(Property.ValueChangeEvent event) {
					Long id = (Long) checkBox.getData();
					if (selectAll) {
						if (!checkBox.getValue()) {
							selectedItemIds.add(id);
						} else {
							selectedItemIds.remove(id);
						}
					} else {
						// clear to test only one quotation id
						selectedItemIds.clear();
						if (checkBox.getValue()) {
							selectedItemIds.add(id);
						} else {
							selectedItemIds.remove(id);
						}
					}
				}
			});

		return checkBox;
	}

	/**
	 * FORM SEARCH
	 * @return
	 */
	private Panel createSearchPanel () {

			VerticalLayout searchLayout = new VerticalLayout();
			HorizontalLayout buttonsLayout = new HorizontalLayout();
			buttonsLayout.setStyleName("panel-search-center");
			Button btnSearch = new Button(I18N.message("search"));
			btnSearch.setClickShortcut(ShortcutAction.KeyCode.ENTER, null); // null it means we don't modify key of shortcut Enter(default = 13)
			btnSearch.setIcon(new ThemeResource("../smt-default/icons/16/search.png"));

			btnSearch.addClickListener(new Button.ClickListener() {
				private static final long serialVersionUID = 582828827319591959L;
				@Override
				public void buttonClick(Button.ClickEvent event) {
					refresh();
				}
			});

			Button btnReset = new Button(I18N.message("reset"));
			btnReset.setIcon(new ThemeResource("../smt-default/icons/16/reset.png"));
			btnReset.addClickListener(new Button.ClickListener() {
				private static final long serialVersionUID = -7598670497938819706L;
				@Override
				public void buttonClick(Button.ClickEvent event) {
					reset();
				}
			});
			buttonsLayout.setSpacing(true);
			buttonsLayout.addComponent(btnSearch);
			buttonsLayout.addComponent(btnReset);

			final GridLayout gridLayout = new GridLayout(8, 4);
			gridLayout.setSpacing(true);

			cbxDealerType = new EntityRefComboBox<>(EDealerType.values());
			cbxDealerType.setWidth(200.0F, Unit.PIXELS);

			cbxProvince = new EntityRefComboBox<Province>();
			cbxProvince.setRestrictions(new BaseRestrictions<Province>(Province.class));
			cbxProvince.renderer();

			dfStartDate = ComponentFactory.getAutoDateField("",false);
			dfStartDate.setValue(DateUtils.today());
			dfEndDate = ComponentFactory.getAutoDateField("",false);
			dfEndDate.setValue(DateUtils.today());


			cbExecuted = new CheckBox(I18N.message("executed"));
			cbExecuted.setValue(false);

			GridLayout statusGridLayout = new GridLayout(1, 1);
			statusGridLayout.setWidth(100, Unit.PERCENTAGE);
			statusGridLayout.addComponent(cbExecuted);

			int iCol = 0;
			gridLayout.addComponent(new Label(I18N.message("dealer.type")), iCol++, 0);
			gridLayout.addComponent(cbxDealerType, iCol++, 0);
			gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
			gridLayout.addComponent(new Label(I18N.message("province")), iCol++, 0);
			gridLayout.addComponent(cbxProvince, iCol++, 0);

			iCol = 0;
			gridLayout.addComponent(new Label(I18N.message("start.execute.date")), iCol++, 1);
			gridLayout.addComponent(dfStartDate, iCol++, 1);
			gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
			gridLayout.addComponent(new Label(I18N.message("end.execute.date")), iCol++, 1);
			gridLayout.addComponent(dfEndDate, iCol++, 1);
			gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
			gridLayout.addComponent(statusGridLayout, iCol++, 1);

			searchLayout.setMargin(true);
			searchLayout.setSpacing(true);
			searchLayout.addComponent(gridLayout);
			searchLayout.addComponent(buttonsLayout);

			Panel searchPanel = new Panel();
			searchPanel.setCaption(I18N.message("search"));
			searchPanel.setContent(searchLayout);

		return searchPanel;
	}

	/**
	 * RESET FORM
	 */
	protected void reset() {
		dfStartDate.setValue(null);
		dfEndDate.setValue(null);
		cbxDealerType.setSelectedEntity(null);
		cbxProvince.setSelectedEntity(null);
		cbExecuted.setValue(false);
	}

}
