package com.soma.mfinance.gui.ui.panel.report.qualityincentive;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(QualityIncentivePanel.NAME)
public class QualityIncentivePanel extends AbstractTabsheetPanel implements View {

    private static final long serialVersionUID = -1929551934421408575L;
    public static final String NAME = "quality.incentive.reports";

    @Autowired
    private QualityIncentiveTablePanel qualityIncentiveTablePanel;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        qualityIncentiveTablePanel.setMainPanel(this);
        getTabSheet().setTablePanel(qualityIncentiveTablePanel);
    }

    @Override
    public void enter(ViewChangeEvent event) {
    }

    @Override
    public void onAddEventClick() {
    }

    @Override
    public void onEditEventClick() {
    }

    @Override
    public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
        getTabSheet().setSelectedTab(selectedTab);
    }

}
