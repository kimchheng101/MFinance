package com.soma.mfinance.gui.ui.panel.report.contract.overdueReport;

import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.shared.system.FMProfile;
import com.soma.mfinance.core.widget.SecUserComboBox;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.service.EntityService;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 
 * @author sok.vina
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class OverdueReportFormPanel extends AbstractFormPanel {

	private static final long serialVersionUID = -428044630697485756L;
	
	private EntityService entityService = SpringUtils.getBean(EntityService.class);

	private SecUserComboBox cbxCreditOfficer;
	private SecUserComboBox cbxCollectionOfficer;
	private Contract contract; 
    
	/** */
    @PostConstruct
	public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
		navigationPanel.addSaveClickListener(this);
	}

    /**
     * @see AbstractFormPanel#getEntity()
     */
    @Override
	protected Contract getEntity() {
		/*contract.setCollectionCreditOfficer(cbxCreditOfficer.getSelectedEntity());
		contract.setCollectionOfficer(cbxCollectionOfficer.getSelectedEntity());*/
		return contract;
	}

    /**
     * @see AbstractFormPanel#createForm()
     */
	@Override
	protected com.vaadin.ui.Component createForm() {

		cbxCreditOfficer = new SecUserComboBox(DataReference.getInstance().getUsers(FMProfile.CO));
		cbxCollectionOfficer = new SecUserComboBox(DataReference.getInstance().getUsers(FMProfile.CC, EStatusRecord.ACTIV));

		VerticalLayout contentLayout = new VerticalLayout();
		contentLayout.setMargin(true);

		final GridLayout gridLayoutCollectionProcess = new GridLayout(5, 3);
		gridLayoutCollectionProcess.setSpacing(true);

        int iCol = 0;
        gridLayoutCollectionProcess.addComponent(new Label(I18N.message("credit.officer")), iCol++, 0);
        gridLayoutCollectionProcess.addComponent(cbxCreditOfficer, iCol++, 0);

        iCol = 0;
        gridLayoutCollectionProcess.addComponent(new Label(I18N.message("collection.officer")), iCol++, 1);
        gridLayoutCollectionProcess.addComponent(cbxCollectionOfficer, iCol++, 1);
		contentLayout.addComponent(gridLayoutCollectionProcess);

		return contentLayout;
	}

	/**
	 *
	 * @param contract
	 */
	public void assignValues(Contract contract) {
		reset();
		this.contract = contract;
		if (contract != null) {
			// TODO PYI
//			if (contract_old.getCollectionCreditOfficer() != null) {
//				cbxCreditOfficer.setSelectedEntity(contract_old.getCollectionCreditOfficer());
//			}
//			if (contract_old.getOfficer() != null) {
//				cbxCollectionOfficer.setSelectedEntity(contract_old.getOfficer());
//			}
		}
	}

	/**
	 * @see AbstractFormPanel#saveEntity()
	 */
	@Override
	public void saveEntity() {
		entityService.saveOrUpdate(getEntity());
	}

	/**
	 * Reset
	 * @see AbstractFormPanel#reset()
	 */
	@Override
	public void reset() {
		super.reset();
		cbxCreditOfficer.setSelectedEntity(null);
		cbxCollectionOfficer.setSelectedEntity(null);
	}
}
