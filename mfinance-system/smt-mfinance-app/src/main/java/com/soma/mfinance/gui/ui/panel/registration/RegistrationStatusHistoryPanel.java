package com.soma.mfinance.gui.ui.panel.registration;

import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.registrations.model.QuotationRegistrationStatusHistory;
import com.soma.mfinance.core.registrations.model.RegistrationStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.soma.frmk.vaadin.ui.widget.toolbar.event.AddClickListener;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;

import java.util.*;

/**
 * 
 * @author Riya.Pov
 */
public class RegistrationStatusHistoryPanel extends AbstractTabPanel implements AddClickListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6745196285248801680L;
	
	private EntityRefComboBox<RegistrationStatus> cbxRegistrationStatus;
	private SimplePagedTable<QuotationRegistrationStatusHistory> tableHistory;
	private NavigationPanel navigationPanel;
	private AutoDateField dfHistory;
	
	private Quotation quotation;
	
	
	/**
	 * @see AddClickListener#addButtonClick(ClickEvent)
	 */
	@Override
	public void addButtonClick(ClickEvent arg0) {
		final Window window = new Window();
		window.setModal(true);
		window.setClosable(false);
		window.setResizable(false);
		window.setWidth(550, Unit.PIXELS);
		window.setHeight(300, Unit.PIXELS);
		window.setCaption(I18N.message("registration.history"));
		
		cbxRegistrationStatus = new EntityRefComboBox<RegistrationStatus>(I18N.message("registration.status"));
		cbxRegistrationStatus.setRestrictions(new BaseRestrictions<RegistrationStatus>(RegistrationStatus.class));
		cbxRegistrationStatus.setWidth(250,Unit.PIXELS);
		cbxRegistrationStatus.renderer();
		cbxRegistrationStatus.setImmediate(true);
		
		dfHistory = new AutoDateField(I18N.message("set.enddate"));
		dfHistory.setWidth(250,Unit.PIXELS);
		
		Button btnSave = new NativeButton(I18N.message("save"));
		btnSave.setIcon(new ThemeResource("../smt-default/icons/16/save.png"));
		btnSave.addClickListener(new ClickListener() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -2674572347165741839L;

			@Override
			public void buttonClick(ClickEvent event) {
				if (quotation == null) {
					return;
				}
				QuotationRegistrationStatusHistory history = new QuotationRegistrationStatusHistory();
				history.setQuotation(quotation);
				
				if(cbxRegistrationStatus.getSelectedEntity() != null){
					history.setRegistrationStatus(cbxRegistrationStatus.getSelectedEntity());
					quotation.setRegistrationStatus(cbxRegistrationStatus.getSelectedEntity());
				}
				if(dfHistory.getValue() != null){
					history.setRegistrationHistoryDate(dfHistory.getValue());
					quotation.setRegistrationDeadline(dfHistory.getValue());
				}
				
				ENTITY_SRV.saveOrUpdate(history);
				quotation.getQuotationRegistrationStatusHistory().add(history);
				ENTITY_SRV.saveOrUpdate(quotation);
				setIndexedContainer();
				window.close();
				
			}
		
		});
		Button btnCancel = new NativeButton(I18N.message("cancel"));
		btnCancel.setIcon(new ThemeResource("../smt-default/icons/16/delete.png"));
		btnCancel.addClickListener(new ClickListener() {
			/** */
			private static final long serialVersionUID = 8902578830364522457L;
			@Override
			public void buttonClick(ClickEvent event) {
				window.close();
			}
		});
		NavigationPanel navigationPanel = new NavigationPanel();
		navigationPanel.addButton(btnSave);
		navigationPanel.addButton(btnCancel);
		
		FormLayout formLayout = new FormLayout();
		formLayout.setMargin(true);
		formLayout.addComponent(cbxRegistrationStatus);
		formLayout.addComponent(dfHistory);
		
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setSpacing(true);
		
		verticalLayout.addComponent(navigationPanel);
		verticalLayout.addComponent(formLayout);
		
		window.setContent(verticalLayout);
		UI.getCurrent().addWindow(window);
	}
	

	/**
	 * @see AbstractTabPanel#createForm()
	 */
	@Override
	protected Component createForm() {
		navigationPanel = new NavigationPanel();
		navigationPanel.addAddClickListener(this);
		
		tableHistory = new SimplePagedTable<QuotationRegistrationStatusHistory>(createColumnDefinitions());
		
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setSpacing(true);
		verticalLayout.addComponent(navigationPanel);
		verticalLayout.addComponent(tableHistory);
		verticalLayout.addComponent(tableHistory.createControls());
		
		return verticalLayout;
	}
	
	/**
	 * ColumnDefinitions
	 * @return
	 */
	private List<ColumnDefinition> createColumnDefinitions() {
		List<ColumnDefinition> columnDefinitions = new ArrayList<ColumnDefinition>();
		
		columnDefinitions.add(new ColumnDefinition("date", I18N.message("date"), Date.class, Align.LEFT, 80));
		columnDefinitions.add(new ColumnDefinition("registration.status", I18N.message("registration.status"), String.class, Align.LEFT, 200));
		columnDefinitions.add(new ColumnDefinition("updateUser", I18N.message("update.user"), String.class, Align.LEFT, 200));
		columnDefinitions.add(new ColumnDefinition("registrationHistoryDate", I18N.message("enddate"), Date.class, Align.LEFT, 80));
		
		return columnDefinitions;
	}
	
	/** */
	@SuppressWarnings("unchecked")
	private void setIndexedContainer() {
		Container.Indexed indexedContainer = tableHistory.getContainerDataSource();
		indexedContainer.removeAllItems();
		if (quotation.getQuotationRegistrationStatusHistory() == null || quotation.getQuotationRegistrationStatusHistory().isEmpty()) {
			return;
		}
		
		Collections.sort(quotation.getQuotationRegistrationStatusHistory(),	new Comparator<QuotationRegistrationStatusHistory>() {

			private static final int REVERSE = -1;

			@Override
			public int compare(QuotationRegistrationStatusHistory o1, QuotationRegistrationStatusHistory o2) {
				if (o1 == null || o2 == null) {
					return 0;
				}
				return o1.getCreateDate().compareTo(o2.getCreateDate()) * REVERSE;
			}
			
		});
		
		for (QuotationRegistrationStatusHistory history : quotation.getQuotationRegistrationStatusHistory()) {
			Item item = indexedContainer.addItem(history.getId());
			item.getItemProperty("date").setValue(history.getCreateDate());
			item.getItemProperty("registration.status").setValue(history.getRegistrationStatus() != null ? history.getRegistrationStatus().getDescEn() : "");
			item.getItemProperty("updateUser").setValue(history.getUpdateUser());
			item.getItemProperty("registrationHistoryDate").setValue(history.getRegistrationHistoryDate());
		}
		tableHistory.refreshContainerDataSource();
	}
	
	/**
	 * @param quotation
	 */
	public void assignValues (Quotation quotation) {
		this.quotation = quotation;
		setIndexedContainer();
	}


}
