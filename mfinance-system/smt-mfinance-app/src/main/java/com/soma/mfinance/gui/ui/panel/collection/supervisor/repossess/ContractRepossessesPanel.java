package com.soma.mfinance.gui.ui.panel.collection.supervisor.repossess;

import com.soma.mfinance.core.collection.model.Collection;
import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import org.apache.commons.lang.StringUtils;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * Contract repossesses panel
 * @author kimsuor.seang
 * @author Modified: sr.soth
 * @date 18-Nov-17
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(ContractRepossessesPanel.NAME)
public class ContractRepossessesPanel extends AbstractTabsheetPanel implements View {

	/** */
	private static final long serialVersionUID = -5671616382948056002L;
	
	public static final String NAME = "contract.repossesses";
	
	@Autowired
	private ContractRepossessTablePanel contractRepossessTablePanel;
	@Autowired
	private ContractRepossessFormPanel contractRepossessFormPanel;

	@PostConstruct
	public void PostConstruct() {
		super.init();
		contractRepossessTablePanel.setMainPanel(this);
		contractRepossessFormPanel.setCaption(I18N.message("contract.repossess"));
		getTabSheet().setTablePanel(contractRepossessTablePanel);
	}
	
	/**
	 * @see View#enter(ViewChangeEvent)
	 */
	@Override
	public void enter(ViewChangeEvent event) {
		String cotraId = event.getParameters();
		if (StringUtils.isNotEmpty(cotraId)) {
			getTabSheet().addFormPanel(contractRepossessFormPanel);
			contractRepossessFormPanel.assignValues(new Long(cotraId));
			getTabSheet().setForceSelected(true);
			getTabSheet().setSelectedTab(contractRepossessFormPanel);
		}
	}

	/**
	 * @see AbstractTabsheetPanel#onAddEventClick()
	 */
	@Override
	public void onAddEventClick() {
		contractRepossessFormPanel.reset();
		getTabSheet().addFormPanel(contractRepossessFormPanel);
		getTabSheet().setSelectedTab(contractRepossessFormPanel);
	}

	/**
	 * @see AbstractTabsheetPanel#onEditEventClick()
	 */
	@Override
	public void onEditEventClick() {
		getTabSheet().addFormPanel(contractRepossessFormPanel);
		initSelectedTab(contractRepossessFormPanel);
	}
	
	/**
	 * @see AbstractTabsheetPanel#initSelectedTab(com.vaadin.ui.Component)
	 */
	@Override
	public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
		if (selectedTab == contractRepossessFormPanel) {
			//contractRepossessFormPanel.assignValues(contractRepossessTablePanel.getItemSelectedId());
			Long contraId=ENTITY_SRV.getById(Collection.class,contractRepossessTablePanel.getItemSelectedId()).getContract().getId();
			contractRepossessFormPanel.assignValues(contraId);
		} else {
			contractRepossessTablePanel.refresh();
		}
		getTabSheet().setSelectedTab(selectedTab);
	}
}
