package com.soma.mfinance.gui.job.dailyreport;

import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.glf.report.service.DailyReportService;

/**
 * @author kimsuor.seang
 */
public class DailyReportJob extends QuartzJobBean {
		
	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
	
		DailyReportService dailyReportService = SpringUtils.getBean(DailyReportService.class);
		
		List<Dealer> dealers = DataReference.getInstance().getDealers();
		
		for (Dealer dealer : dealers) {
			dailyReportService.calculateDailyReportByDealer(dealer, DateUtils.today(), true);
		}
			
	}
}
