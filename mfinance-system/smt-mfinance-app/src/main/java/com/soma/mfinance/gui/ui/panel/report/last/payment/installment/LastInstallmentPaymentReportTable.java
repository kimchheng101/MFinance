package com.soma.mfinance.gui.ui.panel.report.last.payment.installment;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.amount.Amount;
import org.seuksa.frmk.tools.amount.AmountUtils;

import com.soma.mfinance.core.accounting.InstallmentVO;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.asset.model.ENumMonthBeforeMaturity;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowType;
import com.soma.mfinance.core.entityfield.InstallmentEntityField;
import com.soma.mfinance.core.financial.model.EServiceType;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.payment.model.Payment;
import com.soma.mfinance.core.shared.contract.PenaltyVO;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.frmk.security.context.SecApplicationContextHolder;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox.ButtonType;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Table.Align;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;

/**
 * 
 * @author p.ly
 *
 */

public class LastInstallmentPaymentReportTable implements InstallmentEntityField, FinServicesHelper {
	private List<ColumnDefinition> columnDefinitions;
	private SimplePagedTable<Payment> pagedTable;
	private Button btnSearch;
	public LastInstallmentPaymentReportTable(Button btnSearch){
		this.btnSearch = btnSearch;
	}
	public List<ColumnDefinition> getHeader(){
		columnDefinitions = new ArrayList<ColumnDefinition>();
		columnDefinitions.add(new ColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 140, false));
		columnDefinitions.add(new ColumnDefinition(CONTRACT, I18N.message("contract"), String.class, Align.LEFT, 140));
		columnDefinitions.add(new ColumnDefinition(LAST_NAME_EN, I18N.message("lastname.en"), String.class, Align.LEFT, 100));
		columnDefinitions.add(new ColumnDefinition(FIRST_NAME_EN , I18N.message("firstname.en"), String.class, Align.LEFT, 100));
		columnDefinitions.add(new ColumnDefinition(DEALER + "." + NAME_EN, I18N.message("dealer"), String.class, Align.LEFT, 140));
		columnDefinitions.add(new ColumnDefinition("motor.model", I18N.message("motor.model"), String.class, Align.LEFT, 100));
		columnDefinitions.add(new ColumnDefinition(DUE_DATE, I18N.message("due.date"), Date.class, Align.LEFT, 110));
		columnDefinitions.add(new ColumnDefinition(NUM_INSTALLMENT, I18N.message("No"), Integer.class, Align.CENTER, 60));
		columnDefinitions.add(new ColumnDefinition(PAYMENT_DATE, I18N.message("payment.date"), Date.class, Align.CENTER, 70));
		columnDefinitions.add(new ColumnDefinition(INSTALLMENT_AMOUNT, I18N.message("installment.amount"), Amount.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(PRINCIPAL_AMOUNT, I18N.message("principal.amount"), Amount.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(INTEREST_AMOUNT, I18N.message("interest.amount"), Amount.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(INSURANCE_FEE, I18N.message("insurance.fee"), Amount.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(SERVICING_FEE, I18N.message("servicing.fee"), Amount.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(COMMISSION, I18N.message("commission"), Amount.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(OTHER_AMOUNT, I18N.message("other.amount"), Amount.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(NUM_PENALTY_DAY, I18N.message("no.penalty.days"), Integer.class, Align.LEFT, 40));
		columnDefinitions.add(new ColumnDefinition(PENALTY_AMOUNT, I18N.message("penalty.amount"), Amount.class, Align.RIGHT, 70));
		columnDefinitions.add(new ColumnDefinition(TOTAL_PAYMENT, I18N.message("total.payment"), Amount.class, Align.RIGHT, 70));
		return columnDefinitions;
	}
	/**
	 * 
	 * @param installmentVOs
	 */
	public void getData(List<Contract> contracts , Date startDate , Date endDate ,int numMonthBeforeMaturity){
		Indexed indexedContainer = pagedTable.getContainerDataSource();
		indexedContainer.removeAllItems();
		int index = 0;
		if (contracts != null && !contracts.isEmpty()) {
			for (Contract contract : contracts) {
				Applicant applicant = contract.getApplicant(); 
					if( contract.getLastPaidNumInstallment()!=null &&(contract.getTerm() -  contract.getLastPaidNumInstallment()) == numMonthBeforeMaturity){
						for(int i = contract.getLastPaidNumInstallment(); i <=  contract.getTerm(); i++){
							double installmentAmount = 0d;
							double tiPrincipal = 0d;
							double tiInterestRate = 0d;
							double tiInsurance = 0d;
							double tiServiceFee = 0d;
							double otherAmount = 0d;
							double totalInstallment =0d;
							double penaltyAmount = 0d;
							List<InstallmentVO> installmentVOs = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(contract, i);
							InstallmentVO installment = installmentVOs.get(0);
							if(DateUtils.getDateAtBeginningOfDay(installment.getInstallmentDate()).after(DateUtils.getDateAtBeginningOfDay(startDate)) 
									&& DateUtils.getDateAtBeginningOfDay(installment.getInstallmentDate()).before(DateUtils.getDateAtEndOfDay(endDate)) ){
								for (InstallmentVO installmentVO : installmentVOs) {
									if(!ECashflowType.PEN.getCode().equals(installmentVO.getCashflowType().getCode())){
										installmentAmount += installmentVO.getTiamount() + installmentVO.getVatAmount();
									}
									if (ECashflowType.CAP.getCode().equals(installmentVO.getCashflowType().getCode())) {
										tiPrincipal = installmentVO.getTiamount() + installmentVO.getVatAmount();
									} else if(ECashflowType.IAP.getCode().equals(installmentVO.getCashflowType().getCode())) {
										tiInterestRate = installmentVO.getTiamount() + installmentVO.getVatAmount();
									} else if(installmentVO.getService() != null && EServiceType.INSFEE.getCode().equals(installmentVO.getService().getCode())){
										tiInsurance = installmentVO.getTiamount() + installmentVO.getVatAmount();
									} else if(installmentVO.getService() != null && EServiceType.SRVFEE.getCode().equals(installmentVO.getService().getCode())){
										tiServiceFee = installmentVO.getTiamount() + installmentVO.getVatAmount();
									} else if(ECashflowType.PEN.getCode().equals(installmentVO.getCashflowType().getCode())){
										penaltyAmount = installmentVO.getTiamount() + installmentVO.getVatAmount();
									} else {
										otherAmount += installmentVO.getTiamount() + installmentVO.getVatAmount();
									}
									
								}
								PenaltyVO penaltyVO = CONT_SRV.calculatePenalty(contract, installment.getInstallmentDate(), DateUtils.todayH00M00S00(), contract.getTiInstallmentAmount());
								if (penaltyVO.getPenaltyAmount() != null) {
									penaltyAmount += penaltyVO.getPenaltyAmount().getTiAmount() + penaltyVO.getPenaltyAmount().getVatAmount();
								}
								final Item item = indexedContainer.addItem(index);
								item.getItemProperty(ID).setValue(contract.getId());
								item.getItemProperty(CONTRACT).setValue(contract.getReference());
								item.getItemProperty(LAST_NAME_EN).setValue(applicant.getLastNameEn());
								item.getItemProperty(FIRST_NAME_EN).setValue(applicant.getFirstNameEn());
								item.getItemProperty(DEALER + "." + NAME_EN).setValue(contract.getDealer().getNameEn());
								item.getItemProperty("motor.model").setValue(contract.getAsset().getModel().getDesc());
								item.getItemProperty(DUE_DATE).setValue(installment.getInstallmentDate());
								item.getItemProperty(NUM_INSTALLMENT).setValue(installment.getNumInstallment());
								item.getItemProperty(PAYMENT_DATE).setValue(contract.getLastPaidDateInstallment());
								item.getItemProperty(INSTALLMENT_AMOUNT).setValue(new Amount(installmentAmount, 0d, installmentAmount));
								item.getItemProperty(PRINCIPAL_AMOUNT).setValue(new Amount(tiPrincipal, 0d, tiPrincipal));
								item.getItemProperty(INTEREST_AMOUNT).setValue(new Amount(tiInterestRate, 0d, tiInterestRate));
								item.getItemProperty(INSURANCE_FEE).setValue(new Amount(tiInsurance, 0d, tiInsurance));
								item.getItemProperty(SERVICING_FEE).setValue(new Amount(tiServiceFee, 0d, tiServiceFee));
								item.getItemProperty(COMMISSION).setValue(new Amount(0d, 0d, 0d));
								item.getItemProperty(OTHER_AMOUNT).setValue(new Amount(otherAmount, 0d, otherAmount));
								item.getItemProperty(NUM_PENALTY_DAY).setValue(getInteger(penaltyVO.getNumPenaltyDays()));
								item.getItemProperty(PENALTY_AMOUNT).setValue(new Amount(penaltyAmount, 0d, penaltyAmount));
								item.getItemProperty(TOTAL_PAYMENT).setValue(new Amount(totalInstallment, 0d, totalInstallment));
								index++;
							}
							
						}
						
					}
				}
			}
		pagedTable.refreshContainerDataSource();
	}
	private int lastPaidNum(boolean b, int i) {
		// TODO Auto-generated method stub
		return 0;
	}
	/**
	 * 
	 * @param intValue
	 * @return
	 */
	private int getInteger(Integer intValue){
		if(intValue == null){
			return 0;
		} else {
			return intValue;
		}
	}
	
	/**
	 * @return the pagedTable
	 */
	public SimplePagedTable<Payment> getPagedTable() {
		return pagedTable;
	}
	/**
	 * @param pagedTable the pagedTable to set
	 */
	public void setPagedTable(SimplePagedTable<Payment> pagedTable) {
		this.pagedTable = pagedTable;
	}
}
