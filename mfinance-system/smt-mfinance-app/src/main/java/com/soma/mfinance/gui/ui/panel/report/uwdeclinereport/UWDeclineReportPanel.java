package com.soma.mfinance.gui.ui.panel.report.uwdeclinereport;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ru.xpoft.vaadin.VaadinView;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;

/**
 * @modified p.ly
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(UWDeclineReportPanel.NAME)
public class UWDeclineReportPanel  extends AbstractTabsheetPanel implements View {
	/** */
	private static final long serialVersionUID = -1929551934421408575L;
	public static final String NAME = "uw.decline.reports";
	
	@Autowired
	private UWDeclineReportTablePanel uwDeclineReportTablePanel;
	
	/**
	 */
	@PostConstruct
	public void PostConstruct() {
		super.init();
		uwDeclineReportTablePanel.setMainPanel(this);
		getTabSheet().setTablePanel(uwDeclineReportTablePanel);
	}

	/**
	 * @see com.vaadin.navigator.View#enter(com.vaadin.navigator.ViewChangeListener.ViewChangeEvent)
	 */
	@Override
	public void enter(ViewChangeEvent event) {
	}

	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel#onAddEventClick()
	 */
	@Override
	public void onAddEventClick() {
	}

	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel#onEditEventClick()
	 */
	@Override
	public void onEditEventClick() {
	}

	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel#initSelectedTab(com.vaadin.ui.Component)
	 */
	@Override
	public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
		getTabSheet().setSelectedTab(selectedTab);
	}

}
