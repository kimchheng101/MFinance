package com.soma.mfinance.gui.ui.panel.collection.supervisor.repossess;

import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.EntityColumnRenderer;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.Table.Align;
import org.apache.commons.lang.StringUtils;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.amount.Amount;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;

/**
 * Contract re possess table panel
 * @author Modified: sr.soth
 * @date 18-Nov-17
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ContractRepossessTablePanel extends AbstractTablePanel<Collection> implements FMEntityField {

	/** */
	private static final long serialVersionUID = -4923844187558010673L;

	@PostConstruct
	public void PostConstruct() {
		setCaption(I18N.message("contract.repossesses"));
		setSizeFull();
		setMargin(true);
		setSpacing(true);

		super.init(I18N.message("contract.repossesses"));

		NavigationPanel navigationPanel = addNavigationPanel();
		navigationPanel.addEditClickListener(this);
		navigationPanel.addRefreshClickListener(this);
	}

	/**
	 * Get Paged definition
	 * @return
	 */
	@Override
	protected PagedDataProvider<Collection> createPagedDataProvider() {
		PagedDefinition<Collection> pagedDefinition = new PagedDefinition<Collection>(searchPanel.getRestrictions());
		pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 70);
		pagedDefinition.addColumnDefinition("contract.contractAuctionData.requestRepossessedDate", I18N.message("date.repossessed"), Date.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("contract."+REFERENCE, I18N.message("contract.reference"), String.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("contract.financialProduct." + DESC_EN, I18N.message("financial.product"), String.class, Align.LEFT, 150);

		pagedDefinition.addColumnDefinition("contract.whoRepossess", I18N.message("who.repossess"), String.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("contract.support1", I18N.message("support1"), String.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("contract.support2", I18N.message("support2"), String.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("contract.whoBringToWareHouse", I18N.message("who.bring.to.ware.house"), String.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition(CUSTOMER, I18N.message("customer"), String.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition(Lease_MobilePhones, I18N.message("mobile.phones"), String.class, Align.LEFT, 150,new PhoneNumbers());
		pagedDefinition.addColumnDefinition("guarantorMobilePhone", I18N.message("guarantor.mobile.phones"), String.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition(DEALER , I18N.message("dealer"), String.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("contract.term", I18N.message("term"), Integer.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("contract.quotation.leaseAmountPercentage", I18N.message("lease.amount.percent"), Double.class, Align.LEFT, 100);

		pagedDefinition.addColumnDefinition("creditOfficer."+DESC, I18N.message("credit.officer"), String.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("collectionOfficer."+DESC, I18N.message("collection.officer"), String.class, Align.LEFT, 100);
		//pagedDefinition.addColumnDefinition("collectionTask."+DESC_EN, I18N.message("collection.task"), String.class, Align.LEFT, 100);
		//pagedDefinition.addColumnDefinition("area."+DESC_EN, I18N.message("areas"), String.class, Align.LEFT, 100, new AreaRenderer());

		pagedDefinition.addColumnDefinition("lastNumInstallmentPaid", I18N.message("last.paid.installment"), Integer.class, Align.LEFT, 50);
		pagedDefinition.addColumnDefinition("lastPaymentDate", I18N.message("last.paid.due.date"), Date.class, Align.LEFT, 50);

		pagedDefinition.addColumnDefinition("lastPaidPaymentMethod.descEn", I18N.message("last.paid.payment.method"), String.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("nbInstallmentsInOverdue", I18N.message("num.installment.in.overdue"), Integer.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("nbOverdueInDays", I18N.message("num.overdue.in.days"), Integer.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("tiPenaltyAmount", I18N.message("total.penalty.amount.in.overdue"), Amount.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition(TOTAL_AMOUNT_NOT_PAID, I18N.message("total.amount.not.paid"), Double.class, Align.LEFT, 100,new TotalAmountNotPaidPlusVAT());

		pagedDefinition.addColumnDefinition("contract."+START_DATE, I18N.message("start.date"), Date.class, Align.LEFT, 50);
		pagedDefinition.addColumnDefinition("contract."+WKF_STATUS+"." + DESC_EN, I18N.message("contract.status"), String.class, Align.LEFT, 70);
		pagedDefinition.addColumnDefinition(Remaining_Principle, I18N.message("remaining.principal"), Double.class, Align.LEFT, 70, new RemainingPrinciple());
		pagedDefinition.addColumnDefinition(Un_Earn_Interest, I18N.message("unearn.interest"), Double.class, Align.LEFT, 70, new UnEarnInterest());
		//Display Address of Applicant.
		pagedDefinition.addColumnDefinition("province.descEn", I18N.message("province").toUpperCase(), String.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("district.descEn", I18N.message("district").toUpperCase(), String.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("commune.descEn", I18N.message("commune").toUpperCase(), String.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("village.descEn", I18N.message("village").toUpperCase(), String.class, Align.LEFT, 100);
		EntityPagedDataProvider<Collection> pagedDataProvider = new EntityPagedDataProvider<>();
		pagedDataProvider.setPagedDefinition(pagedDefinition);
		return pagedDataProvider;
	}

	/**
	 * @see AbstractTablePanel#getEntity()
	 */
	@Override
	protected Collection getEntity() {
		final Long id = getItemSelectedId();
		if (id != null) {
			return ENTITY_SRV.getById(Collection.class, id);
		}
		return null;
	}

	/**
	 * @see AbstractTablePanel#createSearchPanel()
	 */
	@Override
	protected ContractRepossessSearchPanel createSearchPanel() {
		return new ContractRepossessSearchPanel(this);
	}

	private class RemainingPrinciple extends EntityColumnRenderer{
		@Override
		public Object getValue() {
			Collection collection=((Collection) getEntity());
			return collection.getTeBalanceCapital();
		}
	}

	private class TotalAmountNotPaidPlusVAT extends EntityColumnRenderer {
		@Override
		public Object getValue() {
			Collection collection=((Collection) getEntity());
			Double tiTotalAmountNotPaid=0d;
			Double vatTotalAmountNotPaid=0d;
			if(collection.getTiTotalAmountNotPaid()!=null){
				tiTotalAmountNotPaid= collection.getTiTotalAmountNotPaid();
			}
			if(collection.getVatTotalAmountNotPaid()!=null){
				vatTotalAmountNotPaid=collection.getVatTotalAmountNotPaid();
			}
			return tiTotalAmountNotPaid+vatTotalAmountNotPaid;
		}
	}

	private class PhoneNumbers extends EntityColumnRenderer {
		@Override
		public Object getValue() {
			Collection collection=((Collection) getEntity());
			String comma="";
			if(isValidate(collection.getLeaseMobilePhone1())&& isValidate(collection.getLeaseMobilePhone2())){
				comma=", ";
			}
			String leaseMobilePhone1=isValidate(collection.getLeaseMobilePhone1())?collection.getLeaseMobilePhone1():"";
			String leaseMobilePhone2=isValidate(collection.getLeaseMobilePhone2())?collection.getLeaseMobilePhone2():"";
			return leaseMobilePhone1+comma+leaseMobilePhone2;
		}

		private boolean isValidate(String phoneNumber){
			return phoneNumber!=null && StringUtils.isNotEmpty(phoneNumber);
		}
	}


	private class UnEarnInterest extends EntityColumnRenderer{
		@Override
		public Object getValue() {
			Collection collection=((Collection)getEntity());
			return collection.getTiUnEarnInterest();
		}
	}
}