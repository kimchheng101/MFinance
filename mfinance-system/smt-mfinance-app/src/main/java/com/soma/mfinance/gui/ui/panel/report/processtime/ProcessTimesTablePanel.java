package com.soma.mfinance.gui.ui.panel.report.processtime;

import com.soma.common.app.workflow.service.WkfHistoryItemRestriction;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationApplicant;
import com.soma.mfinance.core.quotation.model.QuotationWkfHistoryItem;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.mfinance.core.shared.util.DateFilterUtil;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.core.workflow.QuotationWkfStatus;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.EntityColumnRenderer;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.ui.Table;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.DecimalFormat;
import java.util.*;

/**
 * @author N/A
 * @Modified Panha MORN
 */

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ProcessTimesTablePanel extends AbstractTablePanel<Quotation> implements QuotationEntityField {

    private static final long serialVersionUID = -5108003734598681093L;
    private Quotation quotation;
    private QuotationReturn quotationReturn;
    private QuotationProcessingTimeByProfile quotationProcessingTimeByProfile;

    @PostConstruct
    public void PostConstruct() {
        setCaption(I18N.message("process.time.report"));
        setSizeFull();
        setMargin(true);
        setSpacing(true);
        super.init(null);

    }

    @Override
    protected PagedDataProvider<Quotation> createPagedDataProvider() {
        PagedDefinition<Quotation> pagedDefinition = new PagedDefinition<>(searchPanel.getRestrictions());
        pagedDefinition.addColumnDefinition("firstSubmissionDate", I18N.message("apply.date").toUpperCase(), Date.class, Table.Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("applicant", I18N.message("application").toUpperCase(), String.class, Table.Align.LEFT, 125, new ApplicantRenderer());
        pagedDefinition.addColumnDefinition("reference", I18N.message("lid").toUpperCase(), String.class, Table.Align.LEFT, 125);
        pagedDefinition.addColumnDefinition("dealer.nameEn", I18N.message("dealer").toUpperCase(), String.class, Table.Align.LEFT, 150);

        //People in Charge Renderer
        pagedDefinition.addColumnDefinition("creditOfficer.desc", I18N.message("co.name").toUpperCase(), String.class, Table.Align.LEFT, 200);
        pagedDefinition.addColumnDefinition("productionOfficer.desc", I18N.message("po.name").toUpperCase(), String.class, Table.Align.LEFT, 125);
        pagedDefinition.addColumnDefinition("underwriter.desc", I18N.message("uw.name").toUpperCase(), String.class, Table.Align.LEFT, 125);
        pagedDefinition.addColumnDefinition("underwriterSupervisor.desc", I18N.message("us.name").toUpperCase(), String.class, Table.Align.LEFT, 125);
        pagedDefinition.addColumnDefinition("manager.desc", I18N.message("mgt.name").toUpperCase(), String.class, Table.Align.LEFT, 125);

        //Process Time Report Amendment
        pagedDefinition.addColumnDefinition("FC.OPT", I18N.message("fc.opt").toUpperCase(), Float.class, Table.Align.LEFT, 200, new GFCProcessTimeRenderer());
        pagedDefinition.addColumnDefinition("POS", I18N.message("pos").toUpperCase(), Float.class, Table.Align.LEFT, 150, new POSTimeRenderer());
        pagedDefinition.addColumnDefinition("UW", I18N.message("uw").toUpperCase(), Float.class, Table.Align.LEFT, 150, new UWTimeRenderer());
        pagedDefinition.addColumnDefinition("US", I18N.message("us").toUpperCase(), Float.class, Table.Align.LEFT, 150, new USTimeRenderer());
        pagedDefinition.addColumnDefinition("FC.UW", I18N.message("fc.uw").toUpperCase(), Float.class, Table.Align.LEFT, 150, new FieldCheckUWTimeRenderer());
        pagedDefinition.addColumnDefinition("MGT", I18N.message("mgt").toUpperCase(), Float.class, Table.Align.LEFT, 150, new MGTTimeRenderer());
        /*pagedDefinition.addColumnDefinition("purchase.order", I18N.message("purchase.order").toUpperCase(), Float.class, Table.Align.LEFT, 150, new PurchaseOrderTimeRenderer());*/
        pagedDefinition.addColumnDefinition("insuranceIO", I18N.message("insurance.io").toUpperCase(), Float.class, Table.Align.LEFT, 150, new InsuranceIOTimeRenderer());
        pagedDefinition.addColumnDefinition("contractPO", I18N.message("contract.po").toUpperCase(), Float.class, Table.Align.LEFT, 150, new ContractPOTimeRenderer());
        pagedDefinition.addColumnDefinition("PreApplication", I18N.message("pre.application").toUpperCase(), Float.class, Table.Align.LEFT, 125, new PreApplicationTimeRenderer());
        pagedDefinition.addColumnDefinition("PreApproval", I18N.message("pre.approval").toUpperCase(), Float.class, Table.Align.LEFT, 125, new PreApprovalTimeRenderer());
        pagedDefinition.addColumnDefinition("PostApproval", I18N.message("post.approval").toUpperCase(), Float.class, Table.Align.LEFT, 200, new PostApprovalTimeRenderer());
        pagedDefinition.addColumnDefinition("Total", I18N.message("total").toUpperCase(), Float.class, Table.Align.LEFT, 150, new TotalTimeRenderer());

        pagedDefinition.addColumnDefinition("result", I18N.message("result").toUpperCase(), String.class, Table.Align.LEFT, 100, new ResultRenderer());
        pagedDefinition.addColumnDefinition("FC.OPT2", I18N.message("fc.opt").toUpperCase(), String.class, Table.Align.LEFT, 100, new GFCRenderer());
        pagedDefinition.addColumnDefinition("FC.UW2", I18N.message("fc.uw").toUpperCase(), String.class, Table.Align.LEFT, 100, new RFCRenderer());
        pagedDefinition.addColumnDefinition("AWC", I18N.message("awc").toUpperCase(), String.class, Table.Align.LEFT, 100, new ApprovedWithConditionRenderer());

        pagedDefinition.addColumnDefinition("POSReturn", I18N.message("pos.return").toUpperCase(), Integer.class, Table.Align.LEFT, 100, new POSReturnRenderer());
        pagedDefinition.addColumnDefinition("UWReturn", I18N.message("uw.return").toUpperCase(), Integer.class, Table.Align.LEFT, 100, new UWReturnRenderer());
        pagedDefinition.addColumnDefinition("SUWReturn", I18N.message("suw.return").toUpperCase(), Integer.class, Table.Align.LEFT, 100, new SUWReturnRenderer());

        pagedDefinition.addColumnDefinition(EMPLOYMENT_STATUS, I18N.message("employment.status").toUpperCase(), String.class, Table.Align.LEFT, 130, new EmploymentStatusRenderer());
        pagedDefinition.addColumnDefinition("housing", I18N.message("housing").toUpperCase(), String.class, Table.Align.LEFT, 100, new HousingRenderer());

        EntityPagedDataProvider<Quotation> pagedDataProvider = new EntityPagedDataProvider<>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    @Override
    protected Quotation getEntity() {
        return null;
    }

    @Override
    protected AbstractSearchPanel<Quotation> createSearchPanel() {
        return new ProcessTimesSearchPanel(this);
    }


    /**
     * ==========================================Start Column Renderer==========================================
     */

    private class POSTimeRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            return getTimeInHour(quotationProcessingTimeByProfile.getPOSBeforeApprovalTime());
        }

    }

    private class UWTimeRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            return getTimeInHour(quotationProcessingTimeByProfile.getUnderwriterProcessTime());
        }
    }

    private class USTimeRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            return getTimeInHour(quotationProcessingTimeByProfile.getUnderwriterSupProcessTime());
        }
    }

    private class FieldCheckUWTimeRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            return getTimeInHour(quotationProcessingTimeByProfile.getRFCTime());
        }
    }

    private class MGTTimeRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            return getTimeInHour(quotationProcessingTimeByProfile.getMGTProcessTime());
        }
    }

    private class PurchaseOrderTimeRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            return getTimeInHour(quotationProcessingTimeByProfile.getApprovedAndPPOProcessTime());
        }
    }

    private class InsuranceIOTimeRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            return getTimeInHour(quotationProcessingTimeByProfile.getWIVTime());
        }
    }

    private class ContractPOTimeRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            return getTimeInHour(quotationProcessingTimeByProfile.getWCATime());
        }
    }

    private class PreApplicationTimeRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            return getTimeInHour(quotationProcessingTimeByProfile.getPreApplicationProcessTime());
        }
    }

    private class PreApprovalTimeRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            return getTimeInHour(quotationProcessingTimeByProfile.getPreApprovalProcessTime());
        }
    }

    private class PostApprovalTimeRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            return getTimeInHour(quotationProcessingTimeByProfile.getPostApprovalProcessTime());
        }
    }

    private class TotalTimeRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            return getTimeInHour(quotationProcessingTimeByProfile.getTotalProcessTime());
        }
    }

    private class ApplicantRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            quotation = ((Quotation) getEntity());
            quotationProcessingTimeByProfile = new QuotationProcessingTimeByProfile(quotation);
            Applicant applicant = quotation.getMainApplicant();
            return applicant.getLastNameEn() + " " + applicant.getFirstNameEn();
        }
    }

    private class GFCProcessTimeRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            if (quotationProcessingTimeByProfile != null) {
                return getTimeInHour(quotationProcessingTimeByProfile.getGFCTime());
            }
            return null;
        }
    }

    private class ResultRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            /**
             * QUO =  // Quotation or Application
             PRO =  // Proposal,
             DEC =  // Declined
             PRA =  // proposal.approved
             SUB =  // Submitted
             RAD =  // Request additional info
             RFC =   // Request field check
             CAN = // Cancelled
             REJ =  // Rejected
             APU =  // Approved(UW)
             APS =  // Approved(US)
             AWU =  // Approved with condition(UW)
             AWS =  // Approved with condition(US)
             AWT =  // Approved with condition
             APV =  // Approved
             ACT =  // Activated
             REU =   // Rejected(UW)
             RCG =  // Request change guarantor
             LCG =  // Allowed change guarantor
             ACG =  // Approved change guarantor
             RVG =  // Request validate change guarantor
             PPO =  // Pending purchase order
             APP =  // Asset Appraisal
             AUP =  // Auction Approve
             WIV =  // Waiting back office validation
             WCA =  // Waiting Contract Activation
             ACS =  // Approved (CS)
             WIV2 =  // Waiting Insurance validation
             */
            if (quotation.getWkfStatus().equals(QuotationWkfStatus.QUO)) {
                return I18N.message(QuotationWkfStatus.QUO.getDesc());
            } else if (quotation.getWkfStatus().equals(QuotationWkfStatus.PRO)) {
                return I18N.message(QuotationWkfStatus.DEC.getDesc());
            } else if (quotation.getWkfStatus().equals(QuotationWkfStatus.DEC)) {
                return I18N.message(QuotationWkfStatus.DEC.getDescEn());
            } else if (quotation.getWkfStatus().equals(QuotationWkfStatus.PRA)) {
                return I18N.message(QuotationWkfStatus.PRA.getDescEn());
            } else if (quotation.getWkfStatus().equals(QuotationWkfStatus.SUB)) {
                return I18N.message(QuotationWkfStatus.SUB.getDescEn());
            } else if (quotation.getWkfStatus().equals(QuotationWkfStatus.RAD)) {
                return I18N.message(QuotationWkfStatus.RAD.getDescEn());
            } else if (quotation.getWkfStatus().equals(QuotationWkfStatus.RFC)) {
                return I18N.message(QuotationWkfStatus.RFC.getDescEn());
            } else if (quotation.getWkfStatus().equals(QuotationWkfStatus.CAN)) {
                return I18N.message(QuotationWkfStatus.CAN.getDescEn());
            } else if (quotation.getWkfStatus().equals(QuotationWkfStatus.REJ)) {
                return I18N.message(QuotationWkfStatus.REJ.getDescEn());
            } else if (quotation.getWkfStatus().equals(QuotationWkfStatus.APU)) {
                return I18N.message(QuotationWkfStatus.APU.getDescEn());
            } else if (quotation.getWkfStatus().equals(QuotationWkfStatus.APS)) {
                return I18N.message(QuotationWkfStatus.APS.getDescEn());
            } else if (quotation.getWkfStatus().equals(QuotationWkfStatus.AWU)) {
                return I18N.message(QuotationWkfStatus.AWU.getDescEn());
            } else if (quotation.getWkfStatus().equals(QuotationWkfStatus.AWS)) {
                return I18N.message(QuotationWkfStatus.AWS.getDescEn());
            } else if (quotation.getWkfStatus().equals(QuotationWkfStatus.AWT)) {
                return I18N.message(QuotationWkfStatus.AWT.getDescEn());
            } else if (quotation.getWkfStatus().equals(QuotationWkfStatus.ACT)) {
                return I18N.message("activated");
            } else if (quotation.getWkfStatus().equals(QuotationWkfStatus.REJ)) {
                return I18N.message("rejected");
            } else if (quotation.getWkfStatus().equals(QuotationWkfStatus.DEC)) {
                return I18N.message("declined");
            } else {
                return null;
            }

        }
    }

    private class RFCRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            return isRFC() ? I18N.message("yes") : I18N.message("no");
        }
    }

    private class ApprovedWithConditionRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            return isApprovedWithCondition() ? I18N.message("yes") : I18N.message("no");
        }
    }

    private class GFCRenderer extends EntityColumnRenderer {

        @Override
        public Object getValue() {
            return isGFC() ? I18N.message("yes") : I18N.message("no");
        }

    }

    private class AdvancePaymentRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            if (quotation.getAdvancePaymentPercentage() != null) {
                double advancePaymentPercentage = quotation.getAdvancePaymentPercentage();
                return advancePaymentPercentage + "%";
            }
            return null;
        }
    }

    private class POSReturnRenderer extends EntityColumnRenderer {
        int value = 0;

        @Override
        public Object getValue() {
            if (quotationReturn != null) {
                if (quotationReturn.getPOSReturn() != 0) {
                    value = quotationReturn.getPOSReturn();
                }
            }
            return value;
        }
    }

    private class UWReturnRenderer extends EntityColumnRenderer {
        int value = 0;

        @Override
        public Object getValue() {
            if (quotationReturn != null) {
                if (quotationReturn.getUWReturn() != 0) {
                    value = quotationReturn.getUWReturn();
                }
            }
            return value;
        }
    }

    private class SUWReturnRenderer extends EntityColumnRenderer {
        int value = 0;

        @Override
        public Object getValue() {
            if (quotationReturn != null) {
                if (quotationReturn.getSUWReturn() != 0) {
                    value = quotationReturn.getSUWReturn();
                }
            }
            return value;
        }
    }

    private class EmploymentStatusRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            Applicant application = quotation.getApplicant();
            String value = "";
            try {
                if (application.getIndividual() != null
                        && application.getIndividual().getCurrentEmployment() != null
                        && application.getIndividual().getCurrentEmployment().getEmploymentStatus() != null) {
                    if (!application.getIndividual().getCurrentEmployment().getEmploymentStatus().getDescEn().isEmpty() || application.getIndividual().getCurrentEmployment().getEmploymentStatus().getDescEn() != null) {
                        value = application.getIndividual().getCurrentEmployment().getEmploymentStatus().getDescEn();
                    }
                }
            } catch (Exception e) {
                logger.error("Null employment rendering ", e);
                return value;
            }
            return value;
        }
    }

    /**
     * Renderer the Hosing field
     */
    private class HousingRenderer extends EntityColumnRenderer {
        @Override
        public Object getValue() {
            Applicant application = quotation.getMainApplicant();
            String value = "";
            try {
                if (application.getIndividual() != null) {
                    if (application.getIndividual().getIndividualAddresses() != null && !application.getIndividual().getIndividualAddresses().isEmpty()) {
                        if (application.getIndividual().getIndividualAddresses().get(0) != null) {
                            return application.getIndividual().getIndividualAddresses().get(0).getHousing().getDescEn();
                        }
                    }
                }
            } catch (Exception e) {
                logger.error("Null housing rendering ", e);
                return value;
            }
            return value;
        }
    }

    /**
     * ==========================================End Column Renderer==========================================
     */


    /**
     * ==========================================Start Calculation==========================================
     */
    private List<QuotationWkfHistoryItem> getQuotationStatusHisotires2() {
        WkfHistoryItemRestriction restrictions = new WkfHistoryItemRestriction(QuotationWkfHistoryItem.class);
        restrictions.setEntityId(quotation.getId());
        List<QuotationWkfHistoryItem> wkfBaseHistoryItems = WKF_SRV.getHistories(restrictions);
		/*BaseRestrictions<QuotationWkfHistoryItem> restrictions = new BaseRestrictions<>(QuotationWkfHistoryItem.class);*/

        //restrictions.addAssociation("user", "SEC_USER", JoinType.INNER_JOIN);
        //restrictions.addAssociation("SEC_USER.defaultProfile", "DEFAULT_PROFILE", JoinType.INNER_JOIN);

		/*restrictions.addCriterion(Restrictions.eq("quotation.id", quotation.getId()));
		restrictions.addOrder(Order.asc("updateDate"));*/
        return wkfBaseHistoryItems;
    }

    /**
     * Check if a quotation has GFC
     *
     * @return
     */
    private boolean isGFC() {
        BaseRestrictions<QuotationApplicant> restrictions = new BaseRestrictions<>(QuotationApplicant.class);
        List<QuotationApplicant> lists = ENTITY_SRV.list(restrictions);
        for (QuotationApplicant list : lists) {
            if (list.getApplicantType() == EApplicantType.C) {
                if (list.getQuotation().getId() == quotation.getId() && list.getApplicant().isFieldCheckAfterInterview()) {
                    return true;
                }

            }
        }
        return false;
    }

    /**
     * Calculate the time between the two dates in accordance to the GLF working day time
     *
     * @param startDate
     * @param endDate
     * @return
     */
    private Long calculateTime(Date startDate, Date endDate) {
        if (startDate == null || endDate == null) {
            return 0l;
        }
        startDate = validateWorkingDate(startDate);
        endDate = validateWorkingDate(endDate);
        long diffDays = DateUtils.getDiffInDays(DateUtils.getDateAtBeginningOfDay(endDate), DateUtils.getDateAtBeginningOfDay(startDate));
        if (diffDays == 0) {
            return endDate.getTime() - startDate.getTime();
        } else {
            long time = 0;
            for (int i = 0; i <= diffDays; i++) {
                Date nextDay = DateUtils.addDaysDate(startDate, i);
                if (i == 0) {
                    time += DateFilterUtil.getWorkingUWEndDate(nextDay).getTime() - startDate.getTime();
                } else if (i == diffDays) {
                    time += endDate.getTime() - DateFilterUtil.getWorkingUWStartDate(nextDay).getTime();
                } else {
                    time += DateFilterUtil.getWorkingUWEndDate(nextDay).getTime() - DateFilterUtil.getWorkingUWStartDate(nextDay).getTime();
                }
            }
            return time;
        }
    }

    private Date validateWorkingDate(Date date) {
        Date startWorkingDate = DateFilterUtil.getWorkingUWStartDate(date);
        Date endWorkingDate = DateFilterUtil.getWorkingUWEndDate(date);
        if (date.before(startWorkingDate)) {
            return startWorkingDate;
        } else if (date.after(endWorkingDate)) {
            return endWorkingDate;
        }
        return date;
    }

    /**
     * Check if a quotation have RFC
     *
     * @return
     */
    private boolean isRFC() {
		/*BaseRestrictions<QuotationWkfHistoryItem> restrictions = new BaseRestrictions<>(QuotationWkfHistoryItem.class);
		restrictions.addCriterion(Restrictions.eq("quotation.id", quotation.getId()));
		List<QuotationWkfHistoryItem> list = ENTITY_SRV.list(restrictions);*/
        WkfHistoryItemRestriction restrictions = new WkfHistoryItemRestriction(QuotationWkfHistoryItem.class);
        restrictions.setEntityId(quotation.getId());
        List<QuotationWkfHistoryItem> wkfBaseHistoryItems = WKF_SRV.getHistories(restrictions);
        for (QuotationWkfHistoryItem status : wkfBaseHistoryItems) {
            if (status.getWkfCurrentStatus().equals(QuotationWkfStatus.RFC)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if a quotation is approved with condition
     *
     * @return
     */
    private boolean isApprovedWithCondition() {
        boolean result = false;
		/*BaseRestrictions<QuotationWkfHistoryItem> restrictions = new BaseRestrictions<>(QuotationWkfHistoryItem.class);
		restrictions.addCriterion(Restrictions.eq("quotation.id", quotation.getId()));
		List<QuotationWkfHistoryItem> list = ENTITY_SRV.list(restrictions);*/
        WkfHistoryItemRestriction restrictions = new WkfHistoryItemRestriction(QuotationWkfHistoryItem.class);
        restrictions.setEntityId(quotation.getId());
        List<QuotationWkfHistoryItem> wkfBaseHistoryItems = WKF_SRV.getHistories(restrictions);
        for (QuotationWkfHistoryItem status : wkfBaseHistoryItems) {
            if (status.getWkfCurrentStatus().equals(QuotationWkfStatus.AWU) || status.getWkfCurrentStatus().equals(QuotationWkfStatus.AWS)) {
                result = true;
            }
        }
        return result;
    }


    /**
     * Represent the Quotation Process Time
     */
    protected class QuotationProcessTime {

        private long POSTime;
        private long UWTime;
        private long USTime;
        private long MATime;
        private long RFCTime;

        public long getPOSTime() {
            return POSTime;
        }

        public void setPOSTime(long pOSTime) {
            POSTime = pOSTime;
        }

        public long getUWTime() {
            return UWTime;
        }

        public void setUWTime(long uWTime) {
            UWTime = uWTime;
        }

        public long getUSTime() {
            return USTime;
        }

        public void setUSTime(long uSTime) {
            USTime = uSTime;
        }

        public long getMATime() {
            return MATime;
        }

        public void setMATime(long mATime) {
            MATime = mATime;
        }

        public long getRFCTime() {
            return RFCTime;
        }

        public void setRFCTime(long requestFieldCheckTime) {
            this.RFCTime = requestFieldCheckTime;
        }

        public void incrementPOSTime(long value) {
            POSTime += value;
        }

        public void incrementUWTime(long value) {
            UWTime += value;
        }

        public void incrementUSTime(long value) {
            USTime += value;
        }

        public void incrementMATime(long value) {
            MATime += value;
        }

        public void incrementRFCime(long value) {
            RFCTime += value;
        }
    }

    /**
     * Represent the Quotation return
     */
    protected class QuotationReturn {

        int POSReturn;
        int UWReturn;
        int SUWReturn;

        public int getPOSReturn() {
            return POSReturn;
        }

        public void setPOSReturn(int pOSReturn) {
            POSReturn = pOSReturn;
        }

        public int getUWReturn() {
            return UWReturn;
        }

        public void setUWReturn(int uWReturn) {
            UWReturn = uWReturn;
        }

        public int getSUWReturn() {
            return SUWReturn;
        }

        public void setSUWReturn(int sUWReturn) {
            SUWReturn = sUWReturn;
        }

        public void incrementPOSReturn(int value) {
            POSReturn += value;
        }

        public void incrementUWReturn(int value) {
            UWReturn += value;
        }

        public void incrementSUWReturn(int value) {
            SUWReturn += value;
        }
    }

    class QuotationProcessingTimeByProfile {
        private Quotation quotation = new Quotation();
        private List<QuotationWkfHistoryItem> quotationStatusHistories = new ArrayList<>();

        public QuotationProcessingTimeByProfile(Quotation quotation) {
            this.quotation = quotation;
            this.quotationStatusHistories = getQuotationStatusHisotires2();
            quotationReturn = getQuotationReturn(quotationStatusHistories);
        }

        /**
         * return the total time during in quotation
         *
         * @param quotation
         * @param list
         * @return
         */
        private long getQuotationTime(Quotation quotation, List<QuotationWkfHistoryItem> list) {
            Date start = quotation.getCreateDate();
            Date end = DateUtils.today();
            long totalTime = 0;
            int size = list.size() - 1;
            if (list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    QuotationWkfHistoryItem history = list.get(i);
                    if ((history.getWkfPreviousStatus().equals(QuotationWkfStatus.PRO) ||
                            history.getWkfPreviousStatus().equals(QuotationWkfStatus.APU) ||
                            history.getWkfPreviousStatus().equals(QuotationWkfStatus.AWU) ||
                            history.getWkfPreviousStatus().equals(QuotationWkfStatus.APV)) && history.getWkfCurrentStatus().equals(QuotationWkfStatus.QUO)) {
                        start = history.getChangeDate();
                    }
                    if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.QUO)) {
                        end = history.getChangeDate();
                    } else {
                        if (i == size && ((history.getWkfPreviousStatus().equals(QuotationWkfStatus.PRO) ||
                                history.getWkfPreviousStatus().equals(QuotationWkfStatus.APU) ||
                                history.getWkfPreviousStatus().equals(QuotationWkfStatus.AWU) ||
                                history.getWkfPreviousStatus().equals(QuotationWkfStatus.APV))
                                && history.getWkfCurrentStatus().equals(QuotationWkfStatus.QUO))) {
                            end = DateUtils.today();
                        } else {
                            end = null;
                        }
                    }
                    totalTime += calculateTime(start, end);
                }
            } else {
                if (quotation.getWkfStatus().equals(QuotationWkfStatus.QUO) && quotation.getPreviousWkfStatus() == null) {
                    totalTime = calculateTime(start, end);
                }
            }

            return totalTime;
        }

        /**
         * return the total time during in Go to Field Check
         *
         * @param quotation
         * @param list
         * @return
         */
        private long getGoToFieldCheckTime(Quotation quotation, List<QuotationWkfHistoryItem> list) {

            Date start = quotation.getCreateDate();
            Date end = DateUtils.today();
            long totalTime = 0;
            int size = list.size() - 1;
            if (list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    QuotationWkfHistoryItem history = list.get(i);
                    if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.PRO)
                            && history.getWkfCurrentStatus().equals(QuotationWkfStatus.RFC)) {
                        start = history.getChangeDate();
                    }
                    if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.RFC)) {
                        end = history.getChangeDate();

                    } else {
                        if (i == size && (history.getWkfPreviousStatus().equals(QuotationWkfStatus.PRO)
                                && history.getWkfCurrentStatus().equals(QuotationWkfStatus.RFC))) {
                            end = DateUtils.today();
                        } else {
                            end = null;
                        }
                    }
                    totalTime += calculateTime(start, end);
                }
            } else {
                if (quotation.getWkfStatus().equals(QuotationWkfStatus.RFC) && quotation.getPreviousWkfStatus() == null) {
                    totalTime = calculateTime(start, end);
                }
            }

            return totalTime;

        }

        /**
         * return the total time during proposal
         *
         * @param list
         * @return
         */
        private long getProposalTime(List<QuotationWkfHistoryItem> list) {
            Date start = null;
            Date end = null;
            long totalTime = 0;
            int size = list.size() - 1;
            List<Date> proStartdate = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                QuotationWkfHistoryItem history = list.get(i);
                if ((history.getWkfPreviousStatus().equals(QuotationWkfStatus.QUO) ||
                        history.getWkfPreviousStatus().equals(QuotationWkfStatus.RFC) ||
                        history.getWkfPreviousStatus().equals(QuotationWkfStatus.RFC) ||
                        history.getWkfPreviousStatus().equals(QuotationWkfStatus.APU) ||
                        history.getWkfPreviousStatus().equals(QuotationWkfStatus.APS) ||
                        history.getWkfPreviousStatus().equals(QuotationWkfStatus.AWU) ||
                        history.getWkfPreviousStatus().equals(QuotationWkfStatus.AWS) ||
                        history.getWkfPreviousStatus().equals(QuotationWkfStatus.AWT)
                                && history.getWkfCurrentStatus().equals(QuotationWkfStatus.PRO))) {
                    start = history.getChangeDate();
                }
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.QUO)
                        && history.getWkfCurrentStatus().equals(QuotationWkfStatus.PRO)) {
                    proStartdate.add(list.get(i).getChangeDate());
                }

                if (proStartdate.size() > 1) {
                    start = proStartdate.get(0);
                }
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.PRO)) {
                    end = history.getChangeDate();
                } else {
                    if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.QUO) && history.getWkfCurrentStatus().equals(QuotationWkfStatus.PRO)
                            && proStartdate.size() > 1) {
                        end = null;
                    } else if (i == size && (history.getWkfPreviousStatus().equals(QuotationWkfStatus.QUO) ||
                            history.getWkfPreviousStatus().equals(QuotationWkfStatus.RFC) ||
                            history.getWkfPreviousStatus().equals(QuotationWkfStatus.RFC) ||
                            history.getWkfPreviousStatus().equals(QuotationWkfStatus.APU) ||
                            history.getWkfPreviousStatus().equals(QuotationWkfStatus.APS) ||
                            history.getWkfPreviousStatus().equals(QuotationWkfStatus.AWU) ||
                            history.getWkfPreviousStatus().equals(QuotationWkfStatus.AWS) ||
                            history.getWkfPreviousStatus().equals(QuotationWkfStatus.AWT)
                                    && history.getWkfCurrentStatus().equals(QuotationWkfStatus.PRO))) {

                        end = DateUtils.today();

                    } else {
                        end = null;
                    }
                }
                totalTime += calculateTime(start, end);
            }
            return totalTime;
        }

        /**
         * return the total time during  proposal (approved with condition)
         *
         * @param list
         * @return
         */
        private long getProApprovWitConTime(List<QuotationWkfHistoryItem> list) {
            Date start = null;
            Date end = null;
            long totalTime = 0;
            int size = list.size() - 1;
            for (int i = 0; i < list.size(); i++) {
                QuotationWkfHistoryItem history = list.get(i);
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.AWT)
                        && history.getWkfCurrentStatus().equals(QuotationWkfStatus.PRA)) {
                    start = history.getChangeDate();
                }
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.PRA)) {
                    end = history.getChangeDate();
                } else {
                    if (i == size && history.getWkfPreviousStatus().equals(QuotationWkfStatus.AWT)
                            && history.getWkfCurrentStatus().equals(QuotationWkfStatus.PRA)) {
                        end = DateUtils.today();
                    } else {
                        end = null;
                    }
                }
                totalTime += calculateTime(start, end);
            }
            return totalTime;
        }

        /**
         * return the total time during Approved Underwriter
         *
         * @param list
         * @return
         */
        private long getApprovedUWTime(List<QuotationWkfHistoryItem> list) {
            Date start = null;
            Date end = null;
            long totalTime = 0;
            int size = list.size() - 1;
            List<Date> endDates = new ArrayList<>();
            int k = 0;
            for (int i = 0; i < list.size(); i++) {
                QuotationWkfHistoryItem history = list.get(i);
                if ((history.getWkfPreviousStatus().equals(QuotationWkfStatus.PRO)
                        || history.getWkfPreviousStatus().equals(QuotationWkfStatus.APS)
                        || history.getWkfPreviousStatus().equals(QuotationWkfStatus.APU)
                        && (history.getWkfCurrentStatus().equals(QuotationWkfStatus.APU)
                        || history.getWkfCurrentStatus().equals(QuotationWkfStatus.APS)
                        || history.getWkfCurrentStatus().equals(QuotationWkfStatus.QUO)
                ))) {
                    start = history.getChangeDate();
                }
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.QUO)
                        && history.getWkfCurrentStatus().equals(QuotationWkfStatus.PRO)) {
                    endDates.add(history.getChangeDate());
                }
                if (history.getWkfCurrentStatus().equals(QuotationWkfStatus.APS)
                        || history.getWkfCurrentStatus().equals(QuotationWkfStatus.QUO)) {
                    end = history.getChangeDate();
                } else if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.QUO)
                        && history.getWkfCurrentStatus().equals(QuotationWkfStatus.PRO) && endDates != null && !endDates.isEmpty() && endDates.size() > 1) {
                    if (k < endDates.size() - 1) {
                        k += 1;
                        end = endDates.get(k);
                    }
                } else if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.APU)) {
                    end = history.getChangeDate();
                } else {
                    if (i == size && (history.getWkfPreviousStatus().equals(QuotationWkfStatus.PRO) ||
                            history.getWkfPreviousStatus().equals(QuotationWkfStatus.APS))
                            && history.getWkfCurrentStatus().equals(QuotationWkfStatus.APU)) {
                        end = DateUtils.today();
                    } else {
                        end = null;
                    }
                }
                totalTime += calculateTime(start, end);
            }

            return totalTime;
        }

        /**
         * return the total time during Approved underwriter Supervisor
         *
         * @param list
         * @return
         */
        private long getApprovedUSTime(List<QuotationWkfHistoryItem> list) {
            Date start = null;
            Date end = null;
            long totalTime = 0;
            int size = list.size() - 1;
            for (int i = 0; i < list.size(); i++) {
                QuotationWkfHistoryItem history = list.get(i);
                if ((history.getWkfPreviousStatus().equals(QuotationWkfStatus.APU) ||
                        history.getWkfPreviousStatus().equals(QuotationWkfStatus.AWU) ||
                        history.getWkfPreviousStatus().equals(QuotationWkfStatus.PRO))
                        && history.getWkfCurrentStatus().equals(QuotationWkfStatus.APS)) {
                    start = history.getChangeDate();
                }
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.APS)) {
                    end = history.getChangeDate();
                } else {
                    if (i == size && (history.getWkfPreviousStatus().equals(QuotationWkfStatus.APU) ||
                            history.getWkfPreviousStatus().equals(QuotationWkfStatus.AWU) ||
                            history.getWkfPreviousStatus().equals(QuotationWkfStatus.PRO)
                                    && history.getWkfCurrentStatus().equals(QuotationWkfStatus.APS))) {
                        end = DateUtils.today();
                    } else {
                        end = null;
                    }
                }
                totalTime += calculateTime(start, end);
            }
            return totalTime;
        }

        /**
         * return the total time of Approved with condition underwriter
         *
         * @param list
         * @return
         */
        private long getApprovWitConUWTime(List<QuotationWkfHistoryItem> list) {
            Date start = null;
            Date end = null;
            long totalTime = 0;
            int size = list.size() - 1;
            for (int i = 0; i < list.size(); i++) {
                QuotationWkfHistoryItem history = list.get(i);
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.PRO)
                        && history.getWkfCurrentStatus().equals(QuotationWkfStatus.AWU)) {
                    start = history.getChangeDate();
                }
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.AWU)) {
                    end = history.getChangeDate();
                } else {
                    if (i == size && history.getWkfPreviousStatus().equals(QuotationWkfStatus.PRO)
                            && history.getWkfCurrentStatus().equals(QuotationWkfStatus.AWU)) {
                        end = DateUtils.today();
                    } else {
                        end = null;
                    }
                }
                totalTime += calculateTime(start, end);
            }
            return totalTime;
        }

        /**
         * return the total time of Approved with condition underwriter supervisor
         *
         * @param list
         * @return
         */
        private long getApprovWitConUSTime(List<QuotationWkfHistoryItem> list) {
            Date start = null;
            Date end = null;
            long totalTime = 0;
            int size = list.size() - 1;
            for (int i = 0; i < list.size(); i++) {
                QuotationWkfHistoryItem history = list.get(i);
                if ((history.getWkfPreviousStatus().equals(QuotationWkfStatus.PRO)
                        || history.getWkfPreviousStatus().equals(QuotationWkfStatus.APU)
                        || history.getWkfPreviousStatus().equals(QuotationWkfStatus.AWU))
                        && history.getWkfCurrentStatus().equals(QuotationWkfStatus.AWS)) {
                    start = history.getChangeDate();
                }
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.AWS)) {
                    end = history.getChangeDate();
                } else {
                    if (i == size && (history.getWkfPreviousStatus().equals(QuotationWkfStatus.PRO)
                            || history.getWkfPreviousStatus().equals(QuotationWkfStatus.APU)
                            || history.getWkfPreviousStatus().equals(QuotationWkfStatus.AWU))
                            && history.getWkfCurrentStatus().equals(QuotationWkfStatus.AWS)) {
                        end = DateUtils.today();
                    } else {
                        end = null;
                    }
                }
                totalTime += calculateTime(start, end);
            }
            return totalTime;
        }

        /**
         * return the total time of rejected underwriter
         *
         * @param list
         * @return
         */
        private long getRejectedUWTime(List<QuotationWkfHistoryItem> list) {
            Date start = null;
            Date end = null;
            long totalTime = 0;
            int size = list.size() - 1;
            for (int i = 0; i < list.size(); i++) {
                QuotationWkfHistoryItem history = list.get(i);
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.PRO)
                        && history.getWkfCurrentStatus().equals(QuotationWkfStatus.REU)) {
                    start = history.getChangeDate();
                }
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.REU)) {
                    end = history.getChangeDate();
                } else {
                    if (i == size && history.getWkfPreviousStatus().equals(QuotationWkfStatus.PRO)
                            && history.getWkfCurrentStatus().equals(QuotationWkfStatus.REU)) {
                        end = DateUtils.today();
                    } else {
                        end = null;
                    }
                }
                totalTime += calculateTime(start, end);
            }
            return totalTime;
        }

        /**
         * return the total time of management approved
         *
         * @param list
         * @return
         */
        private long getApprovedTime(List<QuotationWkfHistoryItem> list) {
            Date start = null;
            Date end = null;
            long totalTime = 0;
            int size = list.size() - 1;
            for (int i = 0; i < list.size(); i++) {
                QuotationWkfHistoryItem history = list.get(i);
                if ((history.getWkfPreviousStatus().equals(QuotationWkfStatus.APS) ||
                        history.getWkfPreviousStatus().equals(QuotationWkfStatus.PRA) ||
                        history.getWkfPreviousStatus().equals(QuotationWkfStatus.AWS))
                        && history.getWkfCurrentStatus().equals(QuotationWkfStatus.APV)) {
                    start = history.getChangeDate();
                }
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.APV)) {
                    end = history.getChangeDate();
                } else {
                    if (i == size && (history.getWkfPreviousStatus().equals(QuotationWkfStatus.APS) ||
                            history.getWkfPreviousStatus().equals(QuotationWkfStatus.PRA) ||
                            history.getWkfPreviousStatus().equals(QuotationWkfStatus.AWS))
                            && history.getWkfCurrentStatus().equals(QuotationWkfStatus.APV)) {
                        end = DateUtils.today();
                    } else {
                        end = null;
                    }
                }
                totalTime += calculateTime(start, end);
            }
            return totalTime;
        }

        /**
         * return the total of approved with condition
         *
         * @param list
         * @return
         */
        private long getApprovedWithConditionTime(List<QuotationWkfHistoryItem> list) {
            Date start = null;
            Date end = null;
            long totalTime = 0;
            int size = list.size() - 1;
            for (int i = 0; i < list.size(); i++) {
                QuotationWkfHistoryItem history = list.get(i);
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.PRA)
                        && history.getWkfCurrentStatus().equals(QuotationWkfStatus.AWT)) {
                    start = history.getChangeDate();
                }
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.AWT)) {
                    end = history.getChangeDate();
                } else {
                    if (i == size && history.getWkfPreviousStatus().equals(QuotationWkfStatus.PRA)
                            && history.getWkfCurrentStatus().equals(QuotationWkfStatus.AWT)) {
                        end = DateUtils.today();
                    } else {
                        end = null;
                    }
                }
                totalTime += calculateTime(start, end);
            }
            return totalTime;
        }

        /**
         * Count the time every time request field check
         *
         * @param list
         * @return
         */
        private long getRFCProcessingTime(List<QuotationWkfHistoryItem> list) {

            long time = 0l;
            int size = list.size();
            Date start = null;
            Date end = null;

            for (int i = 0; i < size; i++) {
                QuotationWkfHistoryItem status = list.get(i);
                if ((status.getWkfPreviousStatus().equals(QuotationWkfStatus.PRO) ||
                        status.getWkfPreviousStatus().equals(QuotationWkfStatus.PRA) ||
                        status.getWkfPreviousStatus().equals(QuotationWkfStatus.APU) ||
                        status.getWkfPreviousStatus().equals(QuotationWkfStatus.AWU) ||
                        status.getWkfPreviousStatus().equals(QuotationWkfStatus.RFC))
                        && (status.getWkfCurrentStatus().equals(QuotationWkfStatus.RFC))) {
                    start = status.getChangeDate();
                }

                if (status.getWkfPreviousStatus().equals(QuotationWkfStatus.RFC)
                        && (status.getWkfCurrentStatus().equals(QuotationWkfStatus.PRO) || status.getWkfCurrentStatus().equals(QuotationWkfStatus.DEC))) {
                    end = status.getChangeDate();

                    if (start != null) {
                        time += calculateTime(start, end);
                        start = null;
                        end = null;
                    }
                }
            }

            if (start != null && end == null) {
                time += calculateTime(start, DateUtils.today());
            }

            return time;
        }

        /**
         * return the request validation change guarantor
         *
         * @param list
         * @return
         */
        private long getRequestValidationChangeGaurantorTime(List<QuotationWkfHistoryItem> list) {
            Date start = null;
            Date end = null;
            long totalTime = 0;
            int size = list.size() - 1;
            for (int i = 0; i < list.size(); i++) {
                QuotationWkfHistoryItem history = list.get(i);
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.LCG)
                        && history.getWkfCurrentStatus().equals(QuotationWkfStatus.AWT)) {
                    start = history.getChangeDate();
                }
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.AWT)) {
                    end = history.getChangeDate();
                } else {
                    if (i == size && history.getWkfPreviousStatus().equals(QuotationWkfStatus.LCG)
                            && history.getWkfCurrentStatus().equals(QuotationWkfStatus.AWT)) {
                        end = DateUtils.today();
                    } else {
                        end = null;
                    }
                }
                totalTime += calculateTime(start, end);
            }
            return totalTime;
        }

        /**
         * return the approved change gaurantor time
         *
         * @param list
         * @return
         */
        private long getApprovedChangeGaurantorTime(List<QuotationWkfHistoryItem> list) {
            Date start = null;
            Date end = null;
            long totalTime = 0;
            int size = list.size() - 1;
            for (int i = 0; i < list.size(); i++) {
                QuotationWkfHistoryItem history = list.get(i);
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.RVG)
                        && history.getWkfCurrentStatus().equals(QuotationWkfStatus.ACG)) {
                    start = history.getChangeDate();
                }
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.ACG)) {
                    end = history.getChangeDate();
                } else {
                    if (i == size && history.getWkfPreviousStatus().equals(QuotationWkfStatus.RVG)
                            && history.getWkfCurrentStatus().equals(QuotationWkfStatus.ACG)) {
                        end = DateUtils.today();
                    } else {
                        end = null;
                    }
                }
                totalTime += calculateTime(start, end);
            }
            return totalTime;
        }

        /**
         * return the total pending purchase order time
         *
         * @param list
         * @return
         */
        private long getPendingPurchaseOrderTime(List<QuotationWkfHistoryItem> list) {
            Date start = null;
            Date end = null;
            long totalTime = 0;
            int size = list.size() - 1;
            for (int i = 0; i < list.size(); i++) {
                QuotationWkfHistoryItem history = list.get(i);
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.APV)
                        && history.getWkfCurrentStatus().equals(QuotationWkfStatus.PPO)) {
                    start = history.getChangeDate();
                }
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.PPO)) {
                    end = history.getChangeDate();
                } else {
                    if (i == size && history.getWkfPreviousStatus().equals(QuotationWkfStatus.APV)
                            && history.getWkfCurrentStatus().equals(QuotationWkfStatus.PPO)) {
                        end = DateUtils.today();
                    } else {
                        end = null;
                    }
                }
                totalTime += calculateTime(start, end);
            }
            return totalTime;
        }

        /**
         * return the total time of approved change gaurantor
         *
         * @param list
         * @return
         */
        private long getAllowChangeGaurantorTime(List<QuotationWkfHistoryItem> list) {
            Date start = null;
            Date end = null;
            long totalTime = 0;
            int size = list.size() - 1;
            for (int i = 0; i < list.size(); i++) {
                QuotationWkfHistoryItem history = list.get(i);
                if ((history.getWkfPreviousStatus().equals(QuotationWkfStatus.RVG)
                        || history.getWkfPreviousStatus().equals(QuotationWkfStatus.RCG))
                        && history.getWkfCurrentStatus().equals(QuotationWkfStatus.LCG)) {
                    start = history.getChangeDate();
                }
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.LCG)) {
                    end = history.getChangeDate();
                } else {
                    if (i == size && (history.getWkfPreviousStatus().equals(QuotationWkfStatus.RVG)
                            || history.getWkfPreviousStatus().equals(QuotationWkfStatus.RCG))
                            && history.getWkfCurrentStatus().equals(QuotationWkfStatus.LCG)) {
                        end = DateUtils.today();
                    } else {
                        end = null;
                    }
                }
                totalTime += calculateTime(start, end);
            }
            return totalTime;
        }

        /**
         * return the total time of approved
         *
         * @param list
         * @return
         */
        private long getWaitingContractActivationTime(List<QuotationWkfHistoryItem> list) {
            Date start = null;
            Date end = null;
            long totalTime = 0;
            int size = list.size() - 1;
            for (int i = 0; i < list.size(); i++) {
                QuotationWkfHistoryItem history = list.get(i);
                if ((history.getWkfPreviousStatus().equals(QuotationWkfStatus.PPO)
                        || history.getWkfPreviousStatus().equals(QuotationWkfStatus.WIV))
                        && history.getWkfCurrentStatus().equals(QuotationWkfStatus.WCA)) {
                    start = history.getChangeDate();
                }
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.WCA)) {
                    end = history.getChangeDate();
                } else {
                    if (i == size && (history.getWkfPreviousStatus().equals(QuotationWkfStatus.PPO)
                            || history.getWkfPreviousStatus().equals(QuotationWkfStatus.WIV))
                            && history.getWkfCurrentStatus().equals(QuotationWkfStatus.WCA)) {
                        end = DateUtils.today();
                    } else {
                        end = null;
                    }
                }
                totalTime += calculateTime(start, end);
            }
            return totalTime;
        }

        /**
         * return the total time of waiting insurance validation
         *
         * @param list
         * @return
         */
        private long getWaitingInsuranceValidationTime(List<QuotationWkfHistoryItem> list) {
            Date start = null;
            Date end = null;
            long totalTime = 0;
            int size = list.size() - 1;
            for (int i = 0; i < list.size(); i++) {
                QuotationWkfHistoryItem history = list.get(i);
                if ((history.getWkfPreviousStatus().equals(QuotationWkfStatus.PPO)
                        || history.getWkfPreviousStatus().equals(QuotationWkfStatus.WCA))
                        && history.getWkfPreviousStatus().equals(QuotationWkfStatus.WIV)) {
                    start = history.getChangeDate();
                }
                if (history.getWkfPreviousStatus().equals(QuotationWkfStatus.WIV)) {
                    end = history.getChangeDate();
                } else {
                    if (i == size && (history.getWkfPreviousStatus().equals(QuotationWkfStatus.PPO)
                            || history.getWkfPreviousStatus().equals(QuotationWkfStatus.WCA))
                            && history.getWkfCurrentStatus().equals(QuotationWkfStatus.WIV)) {
                        end = DateUtils.today();
                    } else {
                        end = null;
                    }
                }
                totalTime += calculateTime(start, end);
            }
            return totalTime;
        }

        public long getQUOTime() {
            return getQuotationTime(quotation, quotationStatusHistories);
        }

        public long getPROTime() {
            return getProposalTime(quotationStatusHistories);
        }

        public long getAPUTime() {
            return getApprovedUWTime(quotationStatusHistories);
        }

        public long getAPSTime() {
            return getApprovedUSTime(quotationStatusHistories);
        }

        public long getAWUTime() {
            return getApprovWitConUWTime(quotationStatusHistories);
        }

        public long getAWSTime() {
            return getApprovWitConUSTime(quotationStatusHistories);
        }

        public long getPRATime() {
            return getProApprovWitConTime(quotationStatusHistories);
        }

        public long getRFCTime() {
            return getRFCProcessingTime(quotationStatusHistories);
        }

        public long getGFCTime() {
            return getGoToFieldCheckTime(quotation, quotationStatusHistories);
        }

        public long getREUTime() {
            return getRejectedUWTime(quotationStatusHistories);
        }

        public long getAPVTime() {
            return getApprovedTime(quotationStatusHistories);
        }

        public long getAWTTime() {
            return getApprovedWithConditionTime(quotationStatusHistories);
        }

        public long getRVGTime() {
            return getRequestValidationChangeGaurantorTime(quotationStatusHistories);
        }

        public long getACGTime() {
            return getApprovedChangeGaurantorTime(quotationStatusHistories);
        }

        public long getPPOTime() {
            return getPendingPurchaseOrderTime(quotationStatusHistories);
        }

        public long getLCGTime() {
            return getAllowChangeGaurantorTime(quotationStatusHistories);
        }

        public long getWCATime() {
            return getWaitingContractActivationTime(quotationStatusHistories);
        }

        public long getWIVTime() {
            return getWaitingInsuranceValidationTime(quotationStatusHistories);
        }

        // Process time to be shown
        public long getPOSBeforeApprovalTime() {
            return getQuotationTime(quotation, quotationStatusHistories) + getApprovedWithConditionTime(quotationStatusHistories);
        }

        public long getUnderwriterProcessTime() {
            return getProposalTime(quotationStatusHistories) +
                    getProApprovWitConTime(quotationStatusHistories);
			/*return getProcessTimeInMimiSecond(quotationStatusHistories, true, false);*/
        }

        public long getUnderwriterSupProcessTime() {
            return getRejectedUWTime(quotationStatusHistories) +
                    getApprovedUWTime(quotationStatusHistories) +
                    getApprovWitConUWTime(quotationStatusHistories);
			/*return getProcessTimeInMimiSecond(quotationStatusHistories, false, true);*/
        }

        public long getMGTProcessTime() {
            return getApprovedUSTime(quotationStatusHistories) + getApprovWitConUSTime(quotationStatusHistories);
        }

        public long getApprovedAndPPOProcessTime() {
            return getApprovedTime(quotationStatusHistories) + getPendingPurchaseOrderTime(quotationStatusHistories);
        }

        public long getPreApplicationProcessTime() {
            return getGFCTime() + getPOSBeforeApprovalTime();
        }

        public long getPreApprovalProcessTime() {
            return getGFCTime() + getPOSBeforeApprovalTime() + getUnderwriterProcessTime() + getUnderwriterSupProcessTime() +
                    getRFCProcessingTime(quotationStatusHistories) + getMGTProcessTime();
        }

        public long getPostApprovalProcessTime() {
            return getApprovedAndPPOProcessTime() + getWaitingContractActivationTime(quotationStatusHistories) +
                    getWaitingInsuranceValidationTime(quotationStatusHistories);
        }

        public long getTotalProcessTime() {
            return getPreApprovalProcessTime() + getPostApprovalProcessTime();
        }

        /**
         * @param list
         * @return
         */
        private QuotationReturn getQuotationReturn(List<QuotationWkfHistoryItem> list) {
            QuotationReturn quotationReturn = new QuotationReturn();
            if (list != null && !list.isEmpty()) {
                for (QuotationWkfHistoryItem status : list) {
                    /*if (status.getSecUserProfile().equals("Underwriter")) {
                        if (status.getWkfCurrentStatus().equals(QuotationWkfStatus.QUO) ||
                                status.getWkfCurrentStatus().equals(QuotationWkfStatus.RFC)) {
                            quotationReturn.incrementPOSReturn(1);
                        }
                    } else if (status.getSecUserProfile().equals("Underwriter Supervisor")) {
                        if (status.getWkfCurrentStatus().equals(QuotationWkfStatus.QUO)
                                || status.getWkfCurrentStatus().equals(QuotationWkfStatus.RFC)) {
                            quotationReturn.incrementPOSReturn(1);
                        } else if (status.getWkfCurrentStatus().equals(QuotationWkfStatus.PRO)) {
                            quotationReturn.incrementUWReturn(1);
                        }
                    } else if (status.getSecUserProfile().equals("Management")) {
                        if (status.getWkfCurrentStatus().equals(QuotationWkfStatus.PRO)) {
                            quotationReturn.incrementSUWReturn(1);
                            quotationReturn.incrementUWReturn(1);
                        }
                    }*/
                }
            }
            return quotationReturn;
        }
    }


    public float getTimeInHour(long val) {
        DecimalFormat fmt = new DecimalFormat("###.##");
        float time = val / (1000 * 3600f);
		/*float time = val/3600;*/
        try {
            time = Float.valueOf(fmt.format(time)).floatValue();
        } catch (Exception e) {

        }
        return time;
    }

    private long getProcessTimeInMimiSecond(List<QuotationWkfHistoryItem> quotationStatusHistories, boolean isUwApproved, boolean isUsApproved) {
        long result = 0;
        long totalProcessTime = 0;
        if (quotationStatusHistories != null && !quotationStatusHistories.isEmpty()) {
            long nbOfDate = 0;
            long processingTimeByOneDay = 0;
            long processingTimeEnd = 0;
            long processingTimeStart = 0;
            Date processingTimeDate = null;

            for (int i = 0; i < quotationStatusHistories.size(); i++) {
                if ((i + 1) == quotationStatusHistories.size()) {
                    processingTimeDate = quotationStatusHistories.get(i).getCreateDate();
                } else {
                    processingTimeDate = quotationStatusHistories.get(i + 1).getChangeDate();
                }
                //Find the number of day from start to end
                nbOfDate = DateUtils.getDiffInDays(DateUtils.getDateAtBeginningOfDay(quotationStatusHistories.get(i).getChangeDate()), DateUtils.getDateAtBeginningOfDay(processingTimeDate));
                if (nbOfDate > 0) {
                    for (int j = 0; j <= nbOfDate; j++) {
                        //Increase one day by loop
                        Date quotationStatusHistoryNextDate = DateUtils.addDaysDate(processingTimeDate, j);
                        if (j == 0) {
                            //Date before 17:30
                            if (quotationStatusHistoryNextDate.before(DateFilterUtil.getWorkingEndDate(quotationStatusHistoryNextDate))) {
                                processingTimeByOneDay = DateFilterUtil.getWorkingEndDate(quotationStatusHistoryNextDate).getTime() - quotationStatusHistoryNextDate.getTime();
                            }
                        } else if (j == nbOfDate) {
                            //Date after 8:30
                            if (quotationStatusHistories.get(i).getChangeDate().after(DateFilterUtil.getWorkingStartDate(quotationStatusHistoryNextDate))) {
                                processingTimeByOneDay = processingTimeByOneDay + (quotationStatusHistories.get(i).getChangeDate().getTime() - DateFilterUtil.getWorkingStartDate(quotationStatusHistoryNextDate).getTime());
                            }
                        } else {
                            processingTimeByOneDay = processingTimeByOneDay + (DateFilterUtil.getWorkingEndDate(quotationStatusHistoryNextDate).getTime() - DateFilterUtil.getWorkingStartDate(quotationStatusHistoryNextDate).getTime());
                        }
                    }
                } else {
                    if (processingTimeDate.after(DateFilterUtil.getWorkingStartDate(processingTimeDate))) {
                        processingTimeStart = processingTimeDate.getTime();
                    } else {
                        processingTimeStart = DateFilterUtil.getWorkingStartDate(processingTimeDate).getTime();
                    }

                    if (quotationStatusHistories.get(i).getChangeDate().before(DateFilterUtil.getWorkingEndDate(quotationStatusHistories.get(i).getChangeDate()))) {
                        processingTimeEnd = quotationStatusHistories.get(i).getChangeDate().getTime();
                    } else {
                        processingTimeEnd = DateFilterUtil.getWorkingEndDate(quotationStatusHistories.get(i).getChangeDate()).getTime();
                    }
                    int x = i + 1;
                    if (x < quotationStatusHistories.size()) {
                        totalProcessTime += getProcessTimeApproval(quotationStatusHistories.get(x), processingTimeStart, processingTimeEnd, isUwApproved, isUsApproved);
                    }
                }
                return totalProcessTime;
            }

            Map<Integer, QuotationWkfHistoryItem> maps = new HashMap<>();
            Integer i = 0;
            Date prevDate = null;
            for (QuotationWkfHistoryItem quotationWkfHistoryItem : quotationStatusHistories) {
                maps.put(i++, quotationWkfHistoryItem);
                QuotationWkfHistoryItem prevItem = maps.get(i - 2);
                if (prevItem != null) {
                    prevDate = prevItem.getChangeDate();
                }
                if (ProfileUtil.isUnderwriter(quotationWkfHistoryItem.getModifiedBy())) {
                    if (quotationWkfHistoryItem.getWkfCurrentStatus().equals(QuotationWkfStatus.PRO)) {

                    }
                } else if (ProfileUtil.isUnderwriterSupervisor(quotationWkfHistoryItem.getModifiedBy())) {
                    if (quotationWkfHistoryItem.getWkfCurrentStatus().equals(QuotationWkfStatus.APU) ||
                            quotationWkfHistoryItem.getWkfCurrentStatus().equals(QuotationWkfStatus.AWU) ||
                            quotationWkfHistoryItem.getWkfCurrentStatus().equals(QuotationWkfStatus.REU)) {
                        result = calculateDateTimeProcess(quotationWkfHistoryItem.getChangeDate(), prevDate);
                    }
                } else if (ProfileUtil.isAdmin(quotationWkfHistoryItem.getModifiedBy())) {
                    if (quotationWkfHistoryItem.getWkfCurrentStatus().equals(QuotationWkfStatus.APS) ||
                            quotationWkfHistoryItem.getWkfCurrentStatus().equals(QuotationWkfStatus.AWS)) {
                        result = calculateDateTimeProcess(quotationWkfHistoryItem.getChangeDate(), prevDate);
                    }

                }
            }
        }
        return result;
    }

    private long getProcessTimeApproval(QuotationWkfHistoryItem quotationWkfHistoryItem, long processingTimeStart, long processingTimeEnd, boolean isUnderwriterApproved, boolean isUsApproved) {
        long processingTime = 0;
        if (ProfileUtil.isUnderwriterSupervisor(quotationWkfHistoryItem.getModifiedBy()) && isUsApproved) {
            processingTime = calculateProcessTime(processingTimeStart, processingTimeEnd);
        } else if (ProfileUtil.isUnderwriter(quotationWkfHistoryItem.getModifiedBy()) && isUnderwriterApproved) {
            processingTime = calculateProcessTime(processingTimeStart, processingTimeEnd);
            System.out.println("UW Process Time:" + processingTime);
        }
        return processingTime;

    }

    private long calculateProcessTime(long processingTimeStart, long processingTimeEnd) {
        long processingTime = 0;
        processingTime = processingTimeEnd - processingTimeStart;
        if (processingTime < 0) {
            processingTime = processingTime * (-1);
        }
        return processingTime;
    }

    /**
     * TODO: class caculate time !
     */
    public class ProcessTimeColumnRender extends EntityColumnRenderer {
        private Date changeDate, prevDate;


        public ProcessTimeColumnRender(Date changeDate, Date prevDate) {
            this.changeDate = changeDate;
            this.prevDate = prevDate;
        }

        @Override
        public Object getValue() {
            long changeTime = changeDate.getTime();
            long prevTime = 0;
            if (prevDate != null) {
                prevTime = prevDate.getTime();

                long processTime = changeTime - prevTime;

                String format = String.format("%%0%dd", 2);
                processTime = processTime / 1000;
                String seconds = String.format(format, processTime % 60);
                String minutes = String.format(format, (processTime % 3600) / 60);
                String hours = String.format(format, processTime / 3600);
                String days = String.format(format, processTime / (3600 * 24));
                String time = days + "d " + hours + "h:" + minutes + "m:" + seconds + "s";
                return hours;
            } else {
                return "0";
            }
        }

    }

    public long calculateDateTimeProcess(Date changeDate, Date previousDate) {
        long result = 0;
        if (previousDate != null) {
            long mChangeDate = changeDate.getTime();
            long mPreviousDate = previousDate.getTime();
            result = (mChangeDate - mPreviousDate) / 3600;
        }
        return result;
    }
}