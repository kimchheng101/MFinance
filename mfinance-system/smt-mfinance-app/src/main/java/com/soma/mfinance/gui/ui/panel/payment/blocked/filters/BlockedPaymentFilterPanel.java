package com.soma.mfinance.gui.ui.panel.payment.blocked.filters;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.collection.model.MCollection;
import com.soma.mfinance.core.payment.service.PaymentFileItemRestriction;
import com.soma.mfinance.core.workflow.PaymentFileWkfStatus;
import com.soma.mfinance.gui.ui.panel.payment.blocked.BlockedPaymentDetailTablePanel;

/**
 * 
 * @author kimsuor.seang
 */
public class BlockedPaymentFilterPanel extends AbstractBlockedPaymentFilterPanel implements MCollection {
	
	/** */
	private static final long serialVersionUID = -1802662755996217893L;
	
	/**
	 * 
	 * @param tablePanel
	 * @param wkfStatus
	 */
	public BlockedPaymentFilterPanel(BlockedPaymentDetailTablePanel tablePanel, EWkfStatus wkfStatus) {
		super(tablePanel, wkfStatus);
	}

	/**
	 * @see com.soma.mfinance.gui.ui.panel.payment.blocked.filters.AbstractBlockedPaymentFilterPanel#getRestrictions()
	 */
	@Override
	public PaymentFileItemRestriction getRestrictions() {
		PaymentFileItemRestriction restrictions = new PaymentFileItemRestriction();
		if (storeControlFilter != null) {
			restrictions.setWkfStatuses(new EWkfStatus[] { storeControlFilter.getWkfStatus() });
			restrictions.setPaymentChannels(storeControlFilter.getChannels());
			if (!PaymentFileWkfStatus.UNIDENTIFIED.equals(storeControlFilter.getWkfStatus())) {
				restrictions.setUploadDateFrom(storeControlFilter.getFrom());
				restrictions.setUploadDateTo(storeControlFilter.getTo());
				restrictions.setAmountFrom(storeControlFilter.getAmountFrom());
				restrictions.setAmountTo(storeControlFilter.getAmountTo());
				restrictions.setContractReference(storeControlFilter.getContractId());
			}
		}
		return restrictions;
	}
}
