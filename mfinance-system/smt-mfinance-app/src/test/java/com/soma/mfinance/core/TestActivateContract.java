package com.soma.mfinance.core;

import com.soma.frmk.security.SecurityHelper;
import com.soma.frmk.testing.BaseSecurityTestCase;
import com.soma.mfinance.core.accounting.InstallmentVO;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.model.Quotation;
import org.seuksa.frmk.tools.DateUtils;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/22/2017
 * Name  : 10:29 AM
 * Email : k.seang@gl-f.com
 */
public class TestActivateContract extends BaseSecurityTestCase implements FinServicesHelper{
    private String sysAdmlogin = "admin";
    private String sysAdmPassword = "11";

    /**
     *
     */
    public TestActivateContract() {
    }

    @Override
    protected void setAuthentication() {
        login = "admin";
        password = "11";
    }

    public void testAAAA(){
       authenticate(getLogin(), getPassword());
       String logUser = APP_SESSION_MNG.isAuthenticated() ? APP_SESSION_MNG.getCurrentUser().getLogin() : SecurityHelper.getAnonymous();
       logger.info("Login: " + logUser);

        //authenticateSysAdmin(sysAdmlogin, sysAdmPassword);

       // String logSysUser = APP_SESSION_MNG.isAuthenticated() ? APP_SESSION_MNG.getCurrentUser().getLogin() : SecurityHelper.getAnonymous();
      //  logger.info("SysLogin authenticated: " + logSysUser);
        //Quotation quotation = ENTITY_SRV.getById(Quotation.class,12);

       // Contract contract = ACT_CON_MOTO_FOR_FLUS_SRV.activateContract(quotation);
        Quotation quotation = ENTITY_SRV.getById(Quotation.class,1l);

        //List<Schedule> scheduleList = CASHFLOW_SERVICE_MFP.getCashflows(quotation, DateUtils.today());
        HashMap<Integer, List<InstallmentVO>> map = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(quotation,DateUtils.today());
        map.forEach((key, value) -> {
            List<InstallmentVO> installmentVOs = value;
            Double installmentAmount = 0d;
            for (InstallmentVO vo : installmentVOs) {
                installmentAmount += vo.getTiamount()  + vo.getVatAmount();
            }
            System.out.println("Number installment : " + key + " Total installment : " + installmentAmount);
        });

        //QUO_SRV.activate(147l, DateUtils.todayDate(),DateUtils.todayDate());

        logger.info("************SUCCESS**********");
    }




}
