package com.soma.frmk.tools;

import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.collection.service.CollectionService;
import com.soma.mfinance.core.collection.service.ContractOtherDataService;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.third.einsurance.service.ContractMigrationService;
import com.soma.frmk.testing.BaseTestCase;
import org.seuksa.frmk.tools.spring.SpringUtils;

import java.util.List;

/**
 * Created by Kimsuor SEANG
 * Date  : 11/16/2017
 * Name  : 12:39 PM
 * Email : k.seang@gl-f.com
 */
public class TestOverdueContract extends BaseTestCase {
    //1_Mengtang@$11.com

    @Override
    protected void setAuthentication() {
        login = "admin";
        //password = "1_Mengtang@$11.com";
        password = "11";
    }

    public void testOverdueContract() {
        authenticate(getLogin(), getPassword());
        System.out.println("...............Overdue contract");
        ContractOtherDataService contractMigrationService =  SpringUtils.getBean(ContractOtherDataService.class);

     /*   Collection collection = ENTITY_SRV.getById(Collection.class,1l);
        System.out.println("Last History" + collection.getLastCollectionContractHistory().getId());*/
       // contractMigrationService.calculateOtherDataContract(contract);
       // contractMigrationService.calculateOtherDataContracts();
        CollectionService collectionService = SpringUtils.getBean(CollectionService.class);
        collectionService.assignOverdueContracts();

        //List<Collection> listCollection = collectionService.assignOverdueContracts();

        System.out.println(".............. End of overdue contract ..............");
    }
}
