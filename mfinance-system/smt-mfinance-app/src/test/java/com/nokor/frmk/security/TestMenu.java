package com.soma.frmk.security;

import com.soma.frmk.security.context.SecApplicationContextHolder;
import com.soma.frmk.security.model.SecApplication;
import com.soma.frmk.testing.BaseTestCase;
import com.soma.frmk.vaadin.util.VaadinServicesHelper;


/**
 * @author kimsuor.seang
 * @version $Revision$
 */
public class TestMenu extends BaseTestCase implements VaadinServicesHelper {



    /**
     * 
     */
    public TestMenu() {
    }
    
    @Override
    protected void setAuthentication() {
        login = "admin";
        password = "admin@EFIN";
    }
    
    /**
     * 
     */
    public void testBuildMenuBar() {
    	try {
    		
    		authenticate(login, password);
    		
//    		VaadinMenuHelper.buildMenuBar(privileges)
    		SecApplication secApp = SECURITY_SRV.getApplication("EFINANCE_RA");
    		SecApplicationContextHolder.getContext().setSecApplication(secApp);
    		VAADIN_SESSION_MNG.getCurrent().getMainMenuBar();

        	
    		logger.info("************SUCCESS**********");
        	
    	} catch (Exception e) {
    		logger.error(e.getMessage(), e);
    	}
    }
   
   

}
