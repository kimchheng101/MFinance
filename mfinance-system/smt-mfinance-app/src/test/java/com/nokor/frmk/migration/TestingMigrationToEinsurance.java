package com.soma.frmk.migration;

import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.third.einsurance.service.ContractMigrationService;
import com.soma.frmk.testing.BaseTestCase;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.springframework.util.Assert;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.soma.mfinance.core.shared.quotation.QuotationEntityField.*;

/**
 * Created by Kimsuor SEANG
 * Date  : 10/18/2017
 * Name  : 9:14 AM
 * Email : k.seang@gl-f.com
 */
public class TestingMigrationToEinsurance extends BaseTestCase {
    //1_Mengtang@$11.com

    @Override
    protected void setAuthentication() {
        login = "admin";
        password = "1_Mengtang@$11.com";
        //password = "1_Mengtang@$11.com";
    }
    private File file;
    private final static String EXTENSION_XLS = "xls";
    private final static String EXTENSION_XLSX = "xlsx";


    public void testMigrationToEinsurance() {
        System.out.println("Well come testing......");
        ContractMigrationService contractMigrationService = SpringUtils.getBean(ContractMigrationService.class);
        //contractMigrationService.migrationToEinsurance("GLF-KCN-02-70000343");

       /* contractMigrationService.migrationToEinsurance("GLF-SRP-03-70000006");
        contractMigrationService.migrationToEinsurance("GLF-PNP-02-70000010");
        contractMigrationService.migrationToEinsurance("GLF-KCN-03-70000003");
        contractMigrationService.migrationToEinsurance("GLF-SRP-03-77000014");
        contractMigrationService.migrationToEinsurance("GLF-SHV-01-77000054");*/
       int nbQuoation = 0;
       List<Quotation> listQuotation = getContracts();

       for (Quotation quotation: listQuotation) {
            if(StringUtils.isNotEmpty(quotation.getReference())) {
                contractMigrationService.migrationToEinsurance(quotation.getId());
                nbQuoation ++;
            }
        }
        System.out.println("Number quotation to migration : " + nbQuoation);
   /*   for(String ref : readQuotationRefFromFile()) {
      //    System.out.println("Reference number : " + ref);
          String glfRef = StringUtils.replace(ref,"CPMI","GLF");
          System.out.println("GLF reference : " + glfRef);
          Quotation quotation = ENTITY_SRV.getByField(Quotation.class,REFERENCE,glfRef.trim());
          contractMigrationService.migrationToEinsurance(quotation.getReference());
          nbQuoation ++;
        //  System.out.println("GLF Reference number : " + glfRef);
      }*/
//        System.out.println("Number quotation to migration : " + nbQuoation);
    }


    public List<Quotation> getContracts() {
        List<Quotation> result = new ArrayList<>();
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<Quotation>(Quotation.class);
        restrictions.addCriterion(Restrictions.isNull(QUOTATION_VA_ID));
        restrictions.addCriterion(Restrictions.ge(INSURANCE_START_DATE, DateUtils.getDateAtBeginningOfDay(DateUtils.string2DateDDMMYYNoSeparator("01012017"))));
        restrictions.addCriterion(Restrictions.le(INSURANCE_START_DATE, DateUtils.getDateAtEndOfDay(DateUtils.string2DateDDMMYYNoSeparator("08122017"))));
        restrictions.addAssociation("contract", "cont", JoinType.LEFT_OUTER_JOIN);

        restrictions.addCriterion(Restrictions.isNull(QUOTATION_VA_ID));
        result = ENTITY_SRV.list(restrictions);
        return result;
    }

    private List<String> readQuotationRefFromFile() {
        List<String> result = new ArrayList<>();
        file = new File("D:\\work\\Document\\insurance\\mig_files\\MFP-11-16.xlsx");

        Iterator<Row> rowIterator = getRowIterator(file);
        int totalColorImport = 0;
        if(rowIterator != null) {
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                if (i > 0) {
                    String refNum = convertToString(row.getCell(0));
                    result.add(refNum);
                }
                i++;
            }
        }
        System.out.println("Total Referenence number Import : " + result.size() );

        return result;
    }

    private Iterator<Row> getRowIterator(File file) {
        Iterator<Row> row = null;
        InputStream stream;
        try {

            String extension = FilenameUtils.getExtension(file.getName());
            stream = new FileInputStream(file);
            if(EXTENSION_XLS.equals(extension)) {
                HSSFWorkbook wb = new HSSFWorkbook(stream);
                HSSFSheet sheet = wb.getSheetAt(0);
                row = sheet.iterator();
            } else if(EXTENSION_XLSX.equals(extension)) {
                XSSFWorkbook wb = new XSSFWorkbook (stream);
                XSSFSheet sheet = wb.getSheetAt(0);
                row = sheet.iterator();
            }
        } catch (FileNotFoundException e) {
            logger.error("FileNotFoundException", e);
        } catch (IOException e) {
            logger.error("IOException", e);
        }

        return row;
    }


    private Object getCellValue(Cell cell) {
        if (cell != null) {
            switch (cell.getCellType()) {
                case Cell.CELL_TYPE_STRING:
                    return cell.getStringCellValue();
                case Cell.CELL_TYPE_NUMERIC:
                    if (HSSFDateUtil.isCellDateFormatted(cell)) {
                        return cell.getDateCellValue();
                    } else {
                        return NumberToTextConverter.toText(cell.getNumericCellValue());
                    }
                case Cell.CELL_TYPE_BOOLEAN:
                    return cell.getBooleanCellValue();
                case Cell.CELL_TYPE_BLANK:
                    return "";
                case Cell.CELL_TYPE_FORMULA:
                    return "";
            }
        }
        return null;
    }

    private  String convertToString(Cell cell) {
        if (cell != null) {
            return String.valueOf(getCellValue(cell));
        } else {
            return StringUtils.EMPTY;
        }
    }
}
