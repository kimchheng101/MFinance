package com.soma.frmk.tools;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.asset.model.Asset;
import com.soma.mfinance.core.auction.model.Auction;
import com.soma.mfinance.core.collection.model.Collection;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.payment.model.Payment;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationApplicant;
import com.soma.frmk.testing.BaseTestCase;


/**
 * @author kimsuor.seang
 * @version $Revision$
 */
public class TestMetaClass extends BaseTestCase {


    /**
     * 
     */
    public TestMetaClass() {
        
    }
    
    /**
     * 
     * @return
     */
    protected boolean initSpring() {
    	return false;
    }
   
    /**
     *  
     */
    public void testClass() {
    	generateMetaClass(Contract.class, "D:/");
    	generateMetaClass(Payment.class, "D:/");
    	generateMetaClass(Quotation.class, "D:/");
    	generateMetaClass(Collection.class, "D:/");
    	generateMetaClass(Auction.class, "D:/");
    	generateMetaClass(Dealer.class, "D:/");
    	generateMetaClass(Asset.class, "D:/");
    	generateMetaClass(Applicant.class, "D:/");
    	generateMetaClass(QuotationApplicant.class, "D:/");
    }
    

    
   
}
