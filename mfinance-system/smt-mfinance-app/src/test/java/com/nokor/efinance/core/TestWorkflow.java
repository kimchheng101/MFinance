package com.soma.mfinance.core;

import com.soma.common.app.workflow.model.EWkfFlow;
import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.common.app.workflow.model.WkfBaseHistoryItem;
import com.soma.common.app.workflow.service.WkfHistoryItemRestriction;
import com.soma.common.app.workflow.service.WorkflowService;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationWkfHistory;
import com.soma.mfinance.core.quotation.model.QuotationWkfHistoryItem;
import com.soma.ersys.core.hr.model.organization.EmployeeWkfHistory;
import com.soma.frmk.security.model.SecProfile;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.testing.BaseTestCase;
import org.joda.time.DateTimeUtils;
import org.junit.Test;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by horndaneth on 5/7/17.
 */
public class TestWorkflow extends BaseTestCase implements FinServicesHelper{



    @Test
    public void testNextWfStatus(){

       /* WorkflowService workflowService = SpringUtils.getBean(WorkflowService.class);
       // workflowService.getWkfStatuses(EWkfFlow.getByCode("Applicant"));

        List<EWkfStatus> applicant = workflowService.getNextWkfStatuses(EWkfFlow.getByCode("Applicant"), EWkfStatus.getById(1));
        Quotation quotation = QUO_SRV.getById(Quotation.class, 147L);
        //QuotationWkfHistoryItem wkfBaseHistoryItems = WKF_SRV.getPreviousHistoProperty( quotation.getPreviousWkfStatus(),175,"");

        //QuotationWkfHistoryItem wkfBaseHistoryItems = (QuotationWkfHistoryItem) quotation.getPreviousHistoStatus();
        QuotationWkfHistoryItem wkfBaseHistoryItems = (QuotationWkfHistoryItem) quotation.getPreviousHistoStatus();

        System.out.println("Hotory Id : " + wkfBaseHistoryItems.getId() + " change date: " + wkfBaseHistoryItems.getChangeDate());
        long processTime = wkfBaseHistoryItems.getChangeDate().getTime();


        long endTime = System.currentTimeMillis();
        long elapsedTime = endTime - processTime;
        String format = String.format("%%0%dd", 2);
        elapsedTime = elapsedTime / 1000;
        String seconds = String.format(format, elapsedTime % 60);
        String minutes = String.format(format, (elapsedTime % 3600) / 60);
        String hours = String.format(format, elapsedTime / 3600);
        String time = hours + ":" + minutes + ":" + seconds;
        System.out.println("* Process time  : " + time);
        System.out.println("******************************");


        INSTALLMENT_SERVICE_MFP.getInstallmentVOs(quotation, DateUtils.today());*/

/*
        WkfHistoryItemRestriction restrictions = new WkfHistoryItemRestriction(QuotationWkfHistoryItem.class);
        restrictions.setEntityId(96l);
        restrictions.setPropertyName("wkfStatus");
        List<QuotationWkfHistoryItem> wkfBaseHistoryItems = WKF_SRV.getHistories(restrictions);
        SECURITY_SRV.getListProfiles();
        for (QuotationWkfHistoryItem item :wkfBaseHistoryItems
             ) {
            System.out.println(item.getId() + " : " + item.getOldValue() + " : " + item.getNewValue() + " : " + item.getCreateUser());
            SecUser secUser =  SECURITY_SRV.loadUserByUsername(item.getCreateUser());
            System.out.println("Profile : " + secUser.getDefaultProfile().getDesc());


        }*/





       /* Calendar cal = Calendar.getInstance();
        // You cannot use Date class to extract individual Date fields
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);      // 0 to 11
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        int second = cal.get(Calendar.SECOND);

        System.out.printf("Now is %4d/%02d/%02d %02d:%02d:%02d\n",year, month+1, day, hour, minute, second);*/


/*

        Calendar calendar = new GregorianCalendar();
        int hour = DateUtils.getHour(DateUtils.today());
        int minute = DateUtils.getMinute(DateUtils.today());
        int second  = calendar.get(Calendar.SECOND);

        Calendar cal = Calendar.getInstance();
        if (cal.get(Calendar.AM_PM) == Calendar.PM) {
            if (hour >= 2) {
                System.out.print(" H :" + hour + " M :" + minute + " S :" + second + " PM");
            }
        }else{
            if (hour <= 7) {
                System.out.print(" H :" + hour + " M :" + minute + " S :" + second + " AM");
            }
        }
*/


        /*Date tomorrow = org.apache.commons.lang.time.DateUtils.addDays(DateUtils.todayDate(), 1);
        System.out.print("Today: " + DateUtils.todayDate());
        System.out.print("Tomorrow: " + tomorrow);*/


        int hour = DateUtils.getHour(DateUtils.today());
        Calendar cal = Calendar.getInstance();

        if (cal.get(Calendar.AM_PM) == Calendar.PM) {
            if (hour >= 1) {
                System.out.println("Time is :" + hour);
            }
        }
        else {

            System.out.println("Time is less than " + hour);
        }
    }
}
