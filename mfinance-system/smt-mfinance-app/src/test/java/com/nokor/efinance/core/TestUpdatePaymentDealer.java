package com.soma.mfinance.core;

import java.util.List;

import org.junit.Test;
import org.seuksa.frmk.dao.hql.BaseRestrictions;

import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.payment.model.Payment;
import com.soma.frmk.testing.BaseTestCase;

public class TestUpdatePaymentDealer extends BaseTestCase {
	
	@Test
	public void test() {
		try {
			BaseRestrictions<Payment> restrictions = new BaseRestrictions<Payment>(Payment.class);
			List<Payment> payments = ENTITY_SRV.list(restrictions);
			if (payments != null & !payments.isEmpty()) {
				for (Payment payment : payments) {
					if (payment.getDealer() == null) {
						Cashflow cashflowFirstIndex = payment.getCashflows().get(0);
						Contract contract = cashflowFirstIndex.getContract();
						payment.setDealer(contract.getDealer());
						ENTITY_SRV.saveOrUpdate(payment);
					}
				}
			}
		} catch (Exception e) {
			logger.error("", e);
		}
	}

}
