package com.soma.mfinance.testservice.finwiz;

import com.soma.frmk.testing.BaseTestCase;

/**
 * 
 * @author kimsuor.seang
 *
 */
public class TestUser extends BaseTestCase {

	/**
	 * 
	 */
	public TestUser() {
		requiredAuhentication = false;
	}
	
    
	/**
	 * 
	 */
	public void testLoginUser() {
		String login = "";
		String password = "";
		try {
			login = "admin";
			password = "admin";
			AUTHENTICAT_SRV.authenticate(login, password);
			logger.info("************authentication success [" + login + "]**********");
		} catch (Exception e) {
			logger.error("Can not authenticate [" + login + "]");
			logger.error(e.getMessage(), e);
		}
		
		try {
			login = "cm_staff";
			password = "password";
			AUTHENTICAT_SRV.authenticate(login, password);

			logger.info("************authentication success [" + login + "]**********");
		} catch (Exception e) {
			logger.error("Can not authenticate [" + login + "]");
			logger.error(e.getMessage(), e);
		}
	}
	

}
