package com.soma.mfinance.core;

import org.seuksa.frmk.tools.DateUtils;

import com.soma.mfinance.core.quotation.FakeQuotationService;
import com.soma.frmk.testing.BaseTestCase;


/**
 * 
 * @author kimsuor.seang
 *
 */
public class TestCreateCM extends BaseTestCase {

	/**
	 * 
	 */
	public TestCreateCM() {
	}
	
	public void testCreateQuotationCM() {
		try {
			FakeQuotationService cIService = getBean(FakeQuotationService.class);
			for (int i = 0; i < 10; i++) {
				cIService.simulateCreateQuotation(1l, DateUtils.todayH00M00S00(), DateUtils.todayH00M00S00());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
