package com.soma.mfinance.core;

import com.soma.common.app.workflow.service.WkfHistoConfigRestriction;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.DealerAddress;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.ersys.collab.project.model.ETaskType;
import com.soma.ersys.collab.project.model.Task;
import com.soma.ersys.collab.project.model.TaskClassification;
import com.soma.ersys.core.hr.model.organization.Employee;
import com.soma.frmk.testing.BaseTestCase;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.model.entity.EMainEntity;
import org.seuksa.frmk.tools.DateUtils;

import java.util.List;


/**
 * 
 * @author kimsuor.seang
 *
 */
public class TestDealerAddress extends BaseTestCase implements FinServicesHelper {

	/**
	 *
	 */
	public TestDealerAddress() {
	}

	/**
	 * @see BaseTestCase#isRequiredAuhentication()
	 */
	@Override
	protected boolean isRequiredAuhentication() {
		return false;
	}

	/**
	 * @see BaseTestCase#setAuthentication()
	 */
	@Override
	protected void setAuthentication() {
		login = "mengtang";
		password = "11";
	}
	
	
	
	/**
	 * 
	 */
	public void testAssignTask() {
		try {
			//authenticate();


			//TODO: get Province
			BaseRestrictions<DealerAddress> restrictions = new BaseRestrictions<>(DealerAddress.class);
			/*restrictions.addAssociation("dealer", "deal", JoinType.INNER_JOIN);
			restrictions.addCriterion(Restrictions.eq("deal.id" , 1l));*/
			//restrictions.addAssociation("address", "address", JoinType.INNER_JOIN);
			List<DealerAddress> dealerAdds = ENTITY_SRV.list(restrictions);

			for(DealerAddress dealerAddress : dealerAdds) {
				/*dealerAddress.getAddress().getProvince();
				dealerAddress.getDealer().getId();*/

				System.out.println("Dealer: " +dealerAddress.getDealer().getNameEn() +" => Province: " + dealerAddress.getAddress().getProvince().getDescEn());
				dealerAddress.getAddress().getProvince().getDescEn();

			}

			logger.info("************SUCCESS**********");
	    	
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	

}
