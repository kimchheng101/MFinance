package com.soma.mfinance.core;

import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.report.ReportParameter;
import com.soma.mfinance.glf.report.xls.GLFGuaranteeContractLegalKH;
import com.soma.mfinance.glf.report.xls.GuaranteeContractMfp;
import com.soma.mfinance.glf.report.xls.LeasingContractLegalMfp;
import com.soma.mfinance.glf.report.xls.LeasingContractMfp;
import com.soma.mfinance.glf.report.xls.PaymentScheduleMfp;
import com.soma.mfinance.tools.report.Report;
import com.soma.frmk.config.model.SettingConfig;
import com.soma.frmk.testing.BaseSecurityTestCase;

/**
 * Created by Kimsuor SEANG
 * Date  : 4/26/2017
 * Name  : 10:32 AM
 * Email : k.seang@gl-f.com
 */
public class TestReport extends BaseSecurityTestCase implements FinServicesHelper {
    private Class<? extends Report> reportClass;

    /**
     *
     */
    public TestReport() {
    }

    @Override
    protected void setAuthentication() {
        login = "admin";
        password = "11";
    }

    public void testPaymentSchedule() {
        authenticate(getLogin(), getPassword());
        //Applicant app = ENTITY_SRV.getById(Applicant.class, 25L);
        //Quotation quotation = QUO_SRV.getById(Quotation.class,93L);
        ReportParameter reportParameter = new ReportParameter();
        reportParameter.addParameter("quotaId", 7L);

        //reportParameter.addParameter("stamp", quotation.isStamp());
        Quotation quotation = QUO_SRV.getById(Quotation.class, 7L);
       reportClass = PaymentScheduleMfp.class;

        SettingConfig settingConfigSolar = null; //DataReference.getInstance().getSettingConfigByCode("solar-tmp-path");
        SettingConfig settingConfigLao = null;//DataReference.getInstance().getSettingConfigByCode("lao-tmp-path");
        try {
            String fileName = REPORT_SRV.extract(reportClass, reportParameter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String fileDir = "";

        logger.debug("cat" + quotation.getId());

/*
        Map<Integer,List<InstallmentVO>> mapInstallmentVOs = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(quotation, DateUtils.today());
        for (Integer nbInstallment : mapInstallmentVOs.keySet()) {
            System.out.println("Nb Payment : " + nbInstallment);
            for(InstallmentVO vo : mapInstallmentVOs.get(nbInstallment)) {
                if (vo.getCashflowType() == ECashflowType.CAP) {
                    System.out.println("tiPrincipal: " + vo.getTiamount());
                } else if(vo.getCashflowType() == ECashflowType.IAP) {
                    System.out.println("tiInterestRate: " + vo.getTiamount());
                } else if(vo.getService() != null && EServiceType.INSFEE.getCode().equals(vo.getService().getCode())){
                    System.out.println("Insurancre: " + vo.getTiamount());

                } else if(vo.getService() != null && EServiceType.SRVFEE.getCode().equals(vo.getService().getCode())){
                    System.out.println("Service Fee: " + vo.getTiamount());
                } else {
                    System.out.println("Other : " + vo.getTiamount());
                }
            }

        }*/
    }

    public void testGuaranteeContractMfp() {
        authenticate(getLogin(), getPassword());
        ReportParameter reportParameter = new ReportParameter();
        reportParameter.addParameter("quotaId", 94l);
        reportParameter.addParameter("stamp", false);
        //reportParameter.addParameter("stamp", quotation.isStamp());
        Quotation quotation = QUO_SRV.getById(Quotation.class, 93L);
        reportClass = GuaranteeContractMfp.class;
        SettingConfig settingConfigSolar = null; //DataReference.getInstance().getSettingConfigByCode("solar-tmp-path");
        SettingConfig settingConfigLao = null;//DataReference.getInstance().getSettingConfigByCode("lao-tmp-path");
        try {
            String fileName = REPORT_SRV.extract(reportClass, reportParameter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String fileDir = "";
        logger.debug("cat" + quotation.getId());
    }

    public void testGuaranteeContractLegalMfp() {
        authenticate(getLogin(), getPassword());
        ReportParameter reportParameter = new ReportParameter();
        reportParameter.addParameter("quotaId", 94l);
        reportParameter.addParameter("stamp", false);
        //reportParameter.addParameter("stamp", quotation.isStamp());
        Quotation quotation = QUO_SRV.getById(Quotation.class, 94L);
        reportClass = GLFGuaranteeContractLegalKH.class;
        SettingConfig settingConfigSolar = null; //DataReference.getInstance().getSettingConfigByCode("solar-tmp-path");
        SettingConfig settingConfigLao = null;//DataReference.getInstance().getSettingConfigByCode("lao-tmp-path");
        try {
            String fileName = REPORT_SRV.extract(reportClass, reportParameter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String fileDir = "";
        logger.debug("cat" + quotation.getId());
    }

    public void testLeasingContractMfp() {
        authenticate(getLogin(), getPassword());
        ReportParameter reportParameter = new ReportParameter();
        reportParameter.addParameter("quotaId", 147l);
        reportParameter.addParameter("stamp", false);
        //reportParameter.addParameter("stamp", quotation.isStamp());
        Quotation quotation = QUO_SRV.getById(Quotation.class, Long.parseLong(reportParameter.getParameters().get("quotaId").toString()));
        reportClass = LeasingContractMfp.class;

        SettingConfig settingConfigSolar = null; //DataReference.getInstance().getSettingConfigByCode("solar-tmp-path");
        SettingConfig settingConfigLao = null;//DataReference.getInstance().getSettingConfigByCode("lao-tmp-path");

        try {
            String fileName = REPORT_SRV.extract(reportClass, reportParameter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String fileDir = "";
        logger.debug("cat" + quotation.getId());
    }

  public void testLeasingContractLegalMfp() {
        authenticate(getLogin(), getPassword());
        ReportParameter reportParameter = new ReportParameter();
        reportParameter.addParameter("quotaId", 126l);
        reportParameter.addParameter("stamp", true);

        Quotation quotation = QUO_SRV.getById(Quotation.class, Long.parseLong(reportParameter.getParameters().get("quotaId").toString()));
        reportClass = LeasingContractLegalMfp.class;

        try {
            String fileName = REPORT_SRV.extract(reportClass, reportParameter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.debug("cat" + quotation.getId());
    }  

}
