/*
package soma.mfinance.core;

import com.soma.common.app.workflow.model.EntityWkf;
import com.soma.mfinance.core.accounting.InstallmentVO;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowType;
import com.soma.mfinance.core.contract.model.cashflow.ETreasuryType;
import com.soma.mfinance.core.contract.service.ContractService;
import com.soma.mfinance.core.contract.service.cashflow.impl.CashflowUtils;
import com.soma.mfinance.core.contract.service.penalty.impl.PenaltyMRRImpl;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.financial.model.FinService;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.payment.model.EPaymentMethod;
import com.soma.mfinance.core.payment.model.EPaymentType;
import com.soma.mfinance.core.payment.model.EPenaltyCalculMethod;
import com.soma.mfinance.core.payment.model.Payment;
import com.soma.mfinance.core.shared.contract.PenaltyVO;
import com.soma.mfinance.core.system.PublicHoliday;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.mfinance.core.workflow.PaymentWkfStatus;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.testing.BaseTestCase;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.amount.Amount;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;


*/
/**
 *
 * @author kimsuor.seang
 *
 *//*

public class TestTask extends BaseTestCase implements FinServicesHelper {

	*/
/**
	 *
	 *//*

	public TestTask() {
	}

	*/
/**
	 * @see BaseTestCase#isRequiredAuhentication()
	 *//*

	@Override
	protected boolean isRequiredAuhentication() {
		return false;
	}

	*/
/**
	 * @see BaseTestCase#setAuthentication()
	 *//*

	@Override
	protected void setAuthentication() {
		login = "admin";
		password = "admin@EFIN";
	}


	private boolean isDueDateOnNonOperationDay(Date installmentAndDelayDay) {
		//List<PublicHoliday> publicHolidays = DataReference.getInstance().getPublicHoliday();
		BaseRestrictions<PublicHoliday> restrictions = new BaseRestrictions<>(PublicHoliday.class);
		restrictions.addCriterion(Restrictions.eq("excludeOverdue", Boolean.TRUE));
		//restrictions.addCriterion(Restrictions.like("reference", "70000356", MatchMode.ANYWHERE));
		List<PublicHoliday> publicHolidays = ENTITY_SRV.list(restrictions);

		for (PublicHoliday publicHoliday : publicHolidays) {
			Date sDate_holi=DateUtils.getDateAtBeginningOfDay(publicHoliday.getDate());
			Date eDate_holi=DateUtils.getDateAtEndOfDay(publicHoliday.getEndDate());
			*/
/*try {
				*//*
*/
/*installmentAndDelayDay=DateUtils.getDateAtBeginningOfDay(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse("2017-10-25 00:00:00.0"));
				//sDate_holi=DateUtils.getDateAtBeginningOfDay(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse("2017-10-25 00:00:00.0"));
				sDate_holi=installmentAndDelayDay;*//*
*/
/*
			} catch (ParseException e) {
				e.printStackTrace();
			}*//*

			sDate_holi=installmentAndDelayDay;
			if (publicHoliday.isExcludeOverdue())
			{
				*/
/*if(installmentAndDelayDay.after(sDate_holi) && installmentAndDelayDay.before(eDate_holi))
				{
					return true;
				}*//*

				if(installmentAndDelayDay.compareTo(sDate_holi)>=0 && installmentAndDelayDay.compareTo(eDate_holi)<=0)
				{
					return true;
				}
			}

			//int a=DateUtils.getDiffInDays(publicHoliday.getDate(), publicHoliday.getEndDate()).intValue();
		}

		*/
/*
		Date min, max;   // assume these are set to something
		Date d;          // the date in question

		d.compareTo(min) >= 0 && d.compareTo(max) <= 0
		 *//*

		return false;
	}


	//public PenaltyVO calculatePenalty(Contract contract, Date installmentDate, Date paymentDate, double installmentAmountUsd) {
	public PenaltyVO calculatePenalty() {
		ContractService cIService = getBean(ContractService.class);
		*/
/*BaseRestrictions<Contract> restrictions = new BaseRestrictions<Contract>(Contract.class);
		restrictions.addCriterion(Restrictions.eq("quotationStatus", QuotationWkfStatus.ACT));
		restrictions.addOrder(Order.asc("contractStartDate"));
		List<Quotation> quotations = cIService.list(restrictions);*//*


		///
		BaseRestrictions<Contract> restrictions = new BaseRestrictions<>(Contract.class);
		restrictions.addCriterion(Restrictions.eq(EntityWkf.WKFSTATUS, ContractWkfStatus.FIN));
		restrictions.addCriterion(Restrictions.like("reference", "70000356", MatchMode.ANYWHERE));
		List<Contract> contracts = ENTITY_SRV.list(restrictions);
		Contract contract=contracts.get(0);
		Date installmentDate=null;
		List<InstallmentVO> installmentByNumInstallments = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(contract, 1);
		//
		Date paymentDate;
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		PenaltyVO penaltyVO = new PenaltyVO();

		double installmentAmountUsd=contract.getTiInstallmentAmount();
		if (contract.getPenaltyRule() != null) {
			//installmentDate = DateUtils.getDateAtBeginningOfDay("2017-09-15 00:00:00.0");
			try {
				installmentDate = DateUtils.getDateAtBeginningOfDay(formatter.parse("2017-10-25 00:00:00.0"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			paymentDate = DateUtils.getDateAtBeginningOfDay(DateUtils.todayH00M00S00());

			Date installmentAndDelayDay = installmentDate;
			Date maxPanaltyDate = DateUtils.addDaysDate(DateUtils.addMonthsDate(installmentDate, 1), -1);
			if (paymentDate.before(maxPanaltyDate)) {
				maxPanaltyDate = paymentDate;
			}

			int nbGracePeriod = 0;
			int nbExculdeHoliday = 0;
			for (int i = 0; i < contract.getPenaltyRule().getGracePeriod() + 1; i++) {
				if (isDueDateOnNonOperationDay(installmentAndDelayDay)) {
					i--;
					nbExculdeHoliday++;
				}
				nbGracePeriod++;
				installmentAndDelayDay = DateUtils.addDaysDate(installmentAndDelayDay, 1);
			}

			installmentAndDelayDay = DateUtils.getDateAtBeginningOfDay(DateUtils.addDaysDate(installmentDate, nbGracePeriod));

			if (maxPanaltyDate.compareTo(installmentAndDelayDay) >= 0) {
				double vatPenalty = 0d;
				Amount penaltyAmount = new Amount(0d, 0d, 0d);
				int nbPenaltyDays = DateUtils.getDiffInDays(maxPanaltyDate, installmentDate).intValue() + 1 - nbExculdeHoliday;
				nbPenaltyDays = nbPenaltyDays < 0 ? 0 : nbPenaltyDays;
				penaltyVO.setNumPenaltyDays(nbPenaltyDays);
				penaltyVO.setNumOverdueDays(DateUtils.getDiffInDays(paymentDate, installmentDate).intValue());
				if (contract.getPenaltyRule().getPenaltyCalculMethod().equals(EPenaltyCalculMethod.FPD)) {
					penaltyAmount.setTiAmount(penaltyVO.getNumPenaltyDays() * contract.getPenaltyRule().getTiPenaltyAmounPerDaytUsd());
				} else if (contract.getPenaltyRule().getPenaltyCalculMethod().equals(EPenaltyCalculMethod.PIS)) {
					penaltyAmount.setTiAmount(penaltyVO.getNumPenaltyDays() * (contract.getPenaltyRule().getPenaltyRate() / 100) * installmentAmountUsd);
				} else if (contract.getPenaltyRule().getPenaltyCalculMethod().equals(EPenaltyCalculMethod.MRR)) {
					penaltyAmount.setTiAmount(new PenaltyMRRImpl().getPenaltyAmont(installmentAmountUsd, paymentDate, MyNumberUtils.getDouble(contract.getPenaltyRule().getPenaltyRate())));
				}
				if (contract.getPenaltyRule().getVat() != null) {
					vatPenalty = penaltyAmount.getTiAmount()*(contract.getPenaltyRule().getVat()/100);
				}
				penaltyAmount.setVatAmount(vatPenalty);
				penaltyAmount.setTeAmount(penaltyAmount.getTiAmount());
				penaltyVO.setPenaltyAmount(penaltyAmount);
			}
		}
		return penaltyVO;
	}

	public void test_penalty(){
		calculatePenalty();
	}

	private void addCashflowCommission(*/
/*FinService finService, InstallmentVO installmentVO, EPaymentMethod ePaymentMethod*//*
){
		ContractService cIService = getBean(ContractService.class);
		BaseRestrictions<FinService> restrictions = new BaseRestrictions<>(FinService.class);
		restrictions.addCriterion(Restrictions.eq("id", 9));
		List<FinService> finServices = cIService.list(restrictions);

		BaseRestrictions<Contract> restrictions_con = new BaseRestrictions<>(Contract.class);
		restrictions_con.addCriterion(Restrictions.eq("con_va_reference", "GLF-PNP-01-70000356"));
		List<Contract> contracts = cIService.list(restrictions_con);
		Date dateTest=null;
		EPaymentMethod ePaymentMethod = cIService.getByCode(EPaymentMethod.class, "TRUE_MONEY");
		try {
			dateTest= DateUtils.getDateAtBeginningOfDay(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse("2017-10-25 00:00:00.0"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Contract contract=contracts.get(0);
		double commission = 0d;
		double vatOfCommission = 0d;
		FinService finService=finServices.get(0);
		commission = finServices.get(0).getTiPrice();
		if (finService.getVat() != null) {
			vatOfCommission = finService.getVat().getValue() * commission / 100;
		}

		*/
/*Cashflow cashflowCommission = CashflowUtils.createCashflow(installmentVO.getContract().getProductLine(),
				null, installmentVO.getContract(), 0d,
				ECashflowType.FEE, ETreasuryType.APP, null, installmentVO.getContract().getProductLine().getPaymentConditionCap(),
				commission, vatOfCommission, commission,
				installmentVO.getInstallmentDate(), installmentVO.getPeriodStartDate(), installmentVO.getPeriodEndDate(), installmentVO.getNumInstallment());
		cashflowCommission.setService(finService);
		cashflowCommission.setPaymentMethod(ePaymentMethod);
		cashflowCommission.setCreateUser(ePaymentMethod.getCode());
		cashflowCommission.setUpdateUser(ePaymentMethod.getCode());
		cashflows.add(cashflowCommission);*//*


		Cashflow cashflowCommission = CashflowUtils.createCashflow(contract.getProductLine(),
				null, contract, 0d,
				ECashflowType.FEE, ETreasuryType.APP, null, contract.getProductLine().getPaymentConditionCap(),
				commission, vatOfCommission, commission,
				dateTest, dateTest, dateTest, 2);
		cashflowCommission.setService(finService);
		cashflowCommission.setPaymentMethod(ePaymentMethod);
		cashflowCommission.setCreateUser("TRUE_MONEY");
		cashflowCommission.setUpdateUser("TRUE_MONEY");
		List<Cashflow> cashflows=null;
		cashflows.add(cashflowCommission);

		createPayment(cashflows,dateTest,4,contract.getDealer(),ePaymentMethod,3.0);

	}

	public Payment createPayment(List<Cashflow> cashflows, Date paymentDate, int penaltyDay, Dealer dealer, EPaymentMethod paymentMethod, double insFromTicket) {
		ContractService cIService = getBean(ContractService.class);
		SecUser receivedUser = cIService.getByDesc(SecUser.class, paymentMethod.toString());
		double totalPaid = 0d;

		for (Cashflow cashflow : cashflows) {
			totalPaid += cashflow.getTiInstallmentAmount() + cashflow.getVatInstallmentAmount();//has Both penalty and Real orignial Payment
			ENTITY_SRV.saveOrUpdate(cashflow);
		}

		*/
/*if (insFromTicket < totalPaid) {
			errorMessage = Message.PAID_AMOUNT_NOT_MATCH;
			return null;
		}*//*

		Cashflow cashflow = cashflows.get(0);
		Contract contract = cashflow.getContract();
		Payment payment = new Payment();
		payment.setApplicant(contract.getApplicant());
		payment.setContract(contract);
		payment.setPaymentDate(paymentDate);
		payment.setTePaidAmount(insFromTicket);
		payment.setTiPaidAmount(insFromTicket);
		payment.setPaid(true);
		payment.setWkfStatus(PaymentWkfStatus.PAI);
		payment.setReceivedUser(receivedUser);
		payment.setPaymentType(EPaymentType.IRC);
		payment.setNumPenaltyDays(penaltyDay);
		payment.setPaymentMethod(paymentMethod);
		payment.setDealer(dealer);
		payment.setCashflows(cashflows);
		contract.setLastPaidNumInstallment(cashflow.getNumInstallment());
		contract.setLastPaidDateInstallment(cashflow.getInstallmentDate());
		//ENTITY_SRV.saveOrUpdate(contract);
		//ENTITY_SRV.saveOrUpdate(payment);
		for (Cashflow cashflowpaid : cashflows) {
			cashflowpaid.setPayment(payment);
			cashflowpaid.setPaid(true);
			ENTITY_SRV.saveOrUpdate(cashflowpaid);
		}
		return payment;
	}
	public void testAddCashflowCommission(){
		calculatePenalty();
	}

	public void testPaymentCombobox(){
		numberOfDaysInMonth(1,2017);
	}

	public static int numberOfDaysInMonth(int month, int year) {
		Calendar monthStart = new GregorianCalendar(year, month-1, 1);
		return monthStart.getActualMaximum(Calendar.DAY_OF_MONTH);
	}
}
*/
