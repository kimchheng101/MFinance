package com.soma.mfinance.ws;

/**
 * Created by cheasocheat on 4/8/17.
 */
public interface WebServiceConstant {
    String RSLT_MSG = "RSLT_MSG";
    String RSLT_CD = "RSLT_CD";
    String RSLT_DATA = "RSLT_DATA";

    //Quotation
    String REFERENCE_ID = "REFERENCE_ID";
    String ASSET = "ASSET";
    String APPLICANTS = "APPLICANTS";
    String DOCUMENTS = "DOCUMENTS";
}
