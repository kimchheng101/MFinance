package com.soma.mfinance.ws.resource.app.ECountry;

import com.soma.common.app.eref.ECountry;
import com.soma.common.messaging.ws.resource.cfg.refdata.AbstractRefDataSrvRsc;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.ws.resource.model.response.country.CountryDTO;

/**
 * Created by kanchanproseth on 6/3/17.
 */
public class CountryResSrvRsc extends AbstractRefDataSrvRsc implements FinServicesHelper {

    /**
     * list District
     * Convert to District data transfer
     * @param country
     * @return
     */
    protected CountryDTO toListCountriesDTO(ECountry country) {
        CountryDTO listCountryDTO = new CountryDTO();
        listCountryDTO.setId(country.getId());
        listCountryDTO.setDesc(country.getDesc());
        listCountryDTO.setDesc_en(country.getDescEn());
        listCountryDTO.setCode(country.getCode());
		listCountryDTO.setUpdate_date("");
		listCountryDTO.setCbc_code("");
        return listCountryDTO;

    }
}
