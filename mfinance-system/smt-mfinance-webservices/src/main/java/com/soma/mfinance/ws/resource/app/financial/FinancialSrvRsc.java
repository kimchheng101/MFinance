package com.soma.mfinance.ws.resource.app.financial;
import com.soma.mfinance.core.custom.erefdata.ERefDataHelper;
import com.soma.mfinance.core.financial.model.Term;
import com.soma.mfinance.ws.resource.model.response.financial.FinancialDTO;
import com.soma.mfinance.ws.resource.model.response.financial.FrequencyDTO;
import com.soma.finance.services.shared.system.EFrequency;
import com.soma.frmk.messaging.ws.EResponseStatus;
import com.soma.frmk.messaging.ws.ResponseHelper;
import com.soma.frmk.messaging.ws.WsReponseException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by v.hum on 6/5/2017.
 */
@Path("/financial")
public class FinancialSrvRsc extends FinancialResSrvRsc {
    @GET
    @Path("/list_term")
    @Produces(MediaType.APPLICATION_JSON + SEMI_COLON + CHARSET_UTF8)
    public Response listTerm() {
        Map<String, Object> response = new HashMap<>();
        try {

            response.put("RSLT_MSG", "Transaction Successful");
            response.put("RSLT_CD", 200);

            List<Term> terms = ENTITY_SRV.list(Term.class);
            List<FinancialDTO> listFinancails = new ArrayList<>();
            for (Term term : terms) {
                listFinancails.add(toFinancialDTO(term));
            }
            response.put("RSLT_DATA", listFinancails);

        } catch (Exception e) {
            String errMsg = "Transaction failed: [" + e.getMessage() + "]";
            LOG.error(errMsg, e);
            response.put("RSLT_MSG", "Transaction Unsuccessful");
            response.put("RSLT_CD", 500);
            response.put("RSLT_DATA", errMsg);
        }
        return ResponseHelper.ok(response);
    }

    @GET
    @Path("/list_frequency")
    @Produces(MediaType.APPLICATION_JSON + SEMI_COLON + CHARSET_UTF8)
    public Response listFrequency() {
        try {
            //Map<Long, EFrequency> response = (Map<Long, EFrequency>) ERefDataHelper.refData.get(EFrequency.class.getName());
            List<EFrequency> listFreq = new ArrayList<>(EFrequency.values());
            List<FrequencyDTO> listFreqs = new ArrayList<>();
            for (EFrequency freq : listFreq) {
                listFreqs.add(toFrequencyDTO(freq));
            }
            return ResponseHelper.ok(listFreqs);

        } catch (Exception e) {
            String errMsg = "Error while searching Individuals [" + e.getMessage() + "]";
            LOG.error(errMsg, e);
            throw new WsReponseException(EResponseStatus.GET_LIST_KO, errMsg);
        }
    }
}
