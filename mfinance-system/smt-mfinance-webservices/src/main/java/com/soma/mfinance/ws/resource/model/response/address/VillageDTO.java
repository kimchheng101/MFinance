package com.soma.mfinance.ws.resource.model.response.address;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.soma.mfinance.ws.resource.model.response.StandardRes;

/**
 * Created by v.hum on 6/5/2017.
 */
public class VillageDTO extends StandardRes {
    public Long getCommune_id() {
        return commune_id;
    }

    public void setCommune_id(Long commune_id) {
        this.commune_id = commune_id;
    }

    @JsonProperty("COMMUNE_ID")
    private Long commune_id;
}
