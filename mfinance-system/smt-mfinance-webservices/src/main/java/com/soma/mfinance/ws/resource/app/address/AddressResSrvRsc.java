package com.soma.mfinance.ws.resource.app.address;

import com.soma.common.messaging.ws.resource.cfg.refdata.AbstractRefDataSrvRsc;
import com.soma.mfinance.core.Enum.EHousing;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.ws.resource.model.response.address.*;

import com.soma.ersys.core.hr.model.address.Commune;
import com.soma.ersys.core.hr.model.address.District;
import com.soma.ersys.core.hr.model.address.Province;
import com.soma.ersys.core.hr.model.address.Village;

/**
 * Created by kanchanproseth on 6/3/17.
 */
public class AddressResSrvRsc extends AbstractRefDataSrvRsc implements FinServicesHelper {
    /**
     * list District
     * Convert to District data transfer
     * @param district
     * @return
     */
    protected DistrictDTO toListDistrictDTO(District district) {
        DistrictDTO listDistrictDTO = new DistrictDTO();
        listDistrictDTO.setId(district.getId());
        listDistrictDTO.setDesc(district.getDesc());
        listDistrictDTO.setDesc_en(district.getDescEn());
        listDistrictDTO.setUpdate_date(district.getUpdateDate().toString());
        listDistrictDTO.setProvince_id(district.getProvince().getId());
        return listDistrictDTO;
    }

    /**
     * list District
     * Convert to District data transfer
     * @param commune
     * @return
     */
    protected CommuneDTO toListCommuneDTO(Commune commune) {
        CommuneDTO listCommuneDTO = new CommuneDTO();
        listCommuneDTO.setId(commune.getId());
        listCommuneDTO.setDesc(commune.getDesc());
        listCommuneDTO.setDesc_en(commune.getDescEn());
        listCommuneDTO.setUpdate_date(commune.getUpdateDate().toString());
        listCommuneDTO.setDistrict_id(commune.getDistrict().getId());
        return listCommuneDTO;
    }


    /**
     * list District
     * Convert to District data transfer
     * @param province
     * @return
     */
    protected ProvinceDTO toProvinceDTO(Province province) {
        ProvinceDTO provinceDTO =  new ProvinceDTO();
        provinceDTO.setId(province.getId());
        provinceDTO.setDesc(province.getDesc());
        provinceDTO.setDesc_en(province.getDescEn());
        provinceDTO.setUpdate_date(province.getUpdateDate().toString());
        return provinceDTO;
    }

    protected VillageDTO toVillageDTO(Village village) {
        VillageDTO villageDTO =  new VillageDTO();
        villageDTO.setId(village.getId());
        villageDTO.setDesc(village.getDesc());
        villageDTO.setDesc_en(village.getDescEn());
        villageDTO.setUpdate_date(village.getUpdateDate().toString());
        villageDTO.setCommune_id(village.getCommune().getId());
        return villageDTO;
    }

    protected PropertyDTO toPropertyDTO(EHousing property) {
        PropertyDTO propertyDTO =  new PropertyDTO();
        propertyDTO.setId(property.getId());
        propertyDTO.setDesc(property.getDesc());
        propertyDTO.setDesc_en(property.getDescEn());
        propertyDTO.setUpdate_date("");
        propertyDTO.setCode(property.getCode());
        return propertyDTO;
    }


}
