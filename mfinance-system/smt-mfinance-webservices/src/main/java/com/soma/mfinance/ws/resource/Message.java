package com.soma.mfinance.ws.resource;
import com.soma.mfinance.core.shared.system.MessageType;
/**
 * Created by p.leap on 14-Jun-17.
 */
public enum Message {
    NO_DATA("No data fetch from database"),
    INTERNAL_SERVER_ERROR("Internal server error"),
    REQUEST_MESSAGE_MANDATORY("RequestMessage is mandatory"),
    SERVICE_HEADER_MANDATORY("ServiceHeader is mandatory"),
    THIRDPARTY_MANDATORY("Third Party is mandatory"),
    THIRDPARTY_NOT_FOUND("Unknown Third Party"),
    PAYMENT_METHOD_NOT_FOUND("Unknown Payment Method"),
    USERNAME_MANDATORY("Username is mandatory"),
    PASSWORD_MANDATORY("Password is mandatory"),
    USERNAME_NOT_FOUND("Unknown username"),
    TOKEN_ID_NOT_FOUND("Token id is mandatory"),
    TOKEN_ID_INVALID("Token id invalid"),
    OTHER_ERROR("Other error"),
    CONTRACT_NOT_FOUND("Reference number is mandatory"),
    CONTRACT_LENGTH_NOT_CORRECT("Reference number should be 8 digits"),
    UNKNOWN_CONTRACT("Unknown reference number"),
    PAID_AMOUNT_NOT_MATCH("Paid Not Match With Amount"),
    SUCCESSED("Transaction Successful"),
    USER_RECEIVER_NOT_FOUND("Don't have this user in system. Please request admin to add: "),
    NO_MORE_INSTALLMENT("All installments are paid already.");

    /** Message type */
    private MessageType type;

    /** Message text */
    private String text;

    /**
     * @param text
     */
    private Message(String text) {
        this.text = text;
        this.type = MessageType.ERROR;
    }

    /**
     * @return MessageType
     */
    public MessageType getType() {
        return type;
    }

    /**
     * @return text as string
     */
    public String getText() {
        return text;
    }

    /**
     * @param text
     * @return Message
     */
    public static Message fromText(String text) {
        if (text != null) {
            for (Message message : values()) {
                if (message.getText().equals(text)) {
                    return message;
                }
            }
        }
        return null;
    }
}
