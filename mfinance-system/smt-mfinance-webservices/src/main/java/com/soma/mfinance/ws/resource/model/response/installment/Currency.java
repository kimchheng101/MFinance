package com.soma.mfinance.ws.resource.model.response.installment;

/**
 * Created by p.leap on 13-Jun-17.
 */
public enum Currency {
    USD("USD"),
    RIEL("RIEL");

    private String text;

    private Currency(String text){
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static Currency fromText(String text) {
        if (text != null) {
            for (Currency currency : values()) {
                if (currency.getText().equals(text)) {
                    return currency;
                }
            }
        }
        return null;
    }
}
