package com.soma.mfinance.ws.resource.model.response.country;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.soma.mfinance.ws.resource.model.response.StandardRes;

/**
 * Created by kanchanproseth on 6/3/17.
 */
public class CountryDTO extends StandardRes {
    @JsonProperty("CBC_CODE")
    private String cbc_code;

    @JsonProperty("CODE")
    private String code;

    public String getCbc_code() {
        return cbc_code;
    }

    public void setCbc_code(String cbc_code) {
        this.cbc_code = cbc_code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
