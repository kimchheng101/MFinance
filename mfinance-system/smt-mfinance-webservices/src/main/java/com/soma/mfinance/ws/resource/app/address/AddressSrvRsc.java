package com.soma.mfinance.ws.resource.app.address;

import com.soma.mfinance.core.Enum.EHousing;
import com.soma.mfinance.core.custom.erefdata.ERefDataHelper;
import com.soma.mfinance.ws.resource.model.response.address.*;
import com.soma.ersys.core.hr.model.address.Commune;
import com.soma.ersys.core.hr.model.address.District;
import com.soma.ersys.core.hr.model.address.Province;

import com.soma.ersys.core.hr.model.address.Village;
import com.soma.frmk.messaging.ws.EResponseStatus;
import com.soma.frmk.messaging.ws.ResponseHelper;
import com.soma.frmk.messaging.ws.WsReponseException;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kanchanproseth on 6/2/17.
 */

@Path("/address")
public class AddressSrvRsc extends AddressResSrvRsc {

    @POST
    @Path("/list_province")
    @Produces(MediaType.APPLICATION_JSON + SEMI_COLON + CHARSET_UTF8)
    public Response getListProvinces(){
        Map<String, Object> response = new HashMap<>();
        try {

            response.put("RSLT_MSG", "Transaction Successful");
            response.put("RSLT_CD", 200);

            List<Province> provinces = ENTITY_SRV.list(Province.class);
            List<ProvinceDTO> provinceResponses = new ArrayList<>();
            for (Province province : provinces) {
                provinceResponses.add(toProvinceDTO(province));
            }
            response.put("RSLT_DATA", provinceResponses);

        } catch (Exception e) {
            String errMsg = "Transaction failed: [" + e.getMessage() + "]";
            LOG.error(errMsg, e);

            response.put("RSLT_MSG", "Transaction Unsuccessful");
            response.put("RSLT_CD", 500);
            response.put("RSLT_DATA", errMsg);

//            throw new WsReponseException(EResponseStatus.GET_LIST_KO, errMsg);
        }
        return ResponseHelper.ok(response);
    }

    @POST
    @Path("/list_district")
    @Produces(MediaType.APPLICATION_JSON + SEMI_COLON + CHARSET_UTF8)
    public Response listDistrict() {
        try {

            List<District> districts = ENTITY_SRV.list(District.class);
            List<DistrictDTO> listDistrictDTOS = new ArrayList<>();
            for (District district : districts) {
                listDistrictDTOS.add(toListDistrictDTO(district));
            }
            return ResponseHelper.ok(listDistrictDTOS);
        } catch (Exception e) {
            String errMsg = "Error while searching Individuals [" + e.getMessage() + "]";
            LOG.error(errMsg, e);
            throw new WsReponseException(EResponseStatus.GET_LIST_KO, errMsg);
        }
    }

    @POST
    @Path("/list_commune")
    @Produces(MediaType.APPLICATION_JSON + SEMI_COLON + CHARSET_UTF8)
    public Response listCommune() {
        try {

            List<Commune> communes = ENTITY_SRV.list(Commune.class);
            List<CommuneDTO> listCommuneDTOS = new ArrayList<>();
            for (Commune commune : communes) {
                listCommuneDTOS.add(toListCommuneDTO(commune));
            }
            return ResponseHelper.ok(listCommuneDTOS);
        } catch (Exception e) {
            String errMsg = "Error while searching Individuals [" + e.getMessage() + "]";
            LOG.error(errMsg, e);
            throw new WsReponseException(EResponseStatus.GET_LIST_KO, errMsg);
        }
    }

    @GET
    @Path("/list_village")
    @Produces(MediaType.APPLICATION_JSON + SEMI_COLON + CHARSET_UTF8)
    public Response listVillage() {
        Map<String, Object> response = new HashMap<>();
        try {

            response.put("RSLT_MSG", "Transaction Successful");
            response.put("RSLT_CD", 200);

            List<Village> villages = ENTITY_SRV.list(Village.class);
            List<VillageDTO> listVillages = new ArrayList<>();
            for (Village village : villages) {
                listVillages.add(toVillageDTO(village));
            }
            response.put("RSLT_DATA", listVillages);

        } catch (Exception e) {
            String errMsg = "Transaction failed: [" + e.getMessage() + "]";
            LOG.error(errMsg, e);
            response.put("RSLT_MSG", "Transaction Unsuccessful");
            response.put("RSLT_CD", 500);
            response.put("RSLT_DATA", errMsg);
        }
        return ResponseHelper.ok(response);
    }

    @GET
    @Path("/list_property")
    @Produces(MediaType.APPLICATION_JSON + SEMI_COLON + CHARSET_UTF8)
    public Response listProperty() {
        try {
            Map<Long, EHousing> response = (Map<Long, EHousing>) ERefDataHelper.refData.get(EHousing.class.getName());
            List<EHousing> listproperties = new ArrayList<>(response.values());
            List<PropertyDTO> listProp = new ArrayList<>();
            for (EHousing prop : listproperties) {
                listProp.add(toPropertyDTO(prop));
            }
            return ResponseHelper.ok(listProp);

        } catch (Exception e) {
            String errMsg = "Error while searching Individuals [" + e.getMessage() + "]";
            LOG.error(errMsg, e);
            throw new WsReponseException(EResponseStatus.GET_LIST_KO, errMsg);
        }
    }
}
