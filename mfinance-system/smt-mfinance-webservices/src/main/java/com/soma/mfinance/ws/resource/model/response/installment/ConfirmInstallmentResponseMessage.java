package com.soma.mfinance.ws.resource.model.response.installment;

import java.io.Serializable;

/**
 * Created by p.leap on 15-Jun-17.
 */
public class ConfirmInstallmentResponseMessage implements Serializable {

    private String reference_number;
    private double amount;
    private String session_id;
    private String response_msg;
    private int response_code;
    private Currency currency;

    public String getReference_number() {
        return reference_number;
    }
    public void setReference_number(String reference_number) {
        this.reference_number = reference_number;
    }

    public Currency getCurrency() {
        return currency;
    }
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public double getAmount() {
        return amount;
    }
    public void setAmount(double amount) {
        this.amount = amount;
    }
    public String getSession_id() {
        return session_id;
    }
    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }
    public String getResponse_msg() {
        return response_msg;
    }
    public void setResponse_msg(String response_msg) {
        this.response_msg = response_msg;
    }
    public int getResponse_code() {
        return response_code;
    }
    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

}
