package com.soma.mfinance.ws.resource.app.installment;

import com.soma.mfinance.core.accounting.InstallmentVO;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.ws.resource.model.response.installment.InstallmentDTO;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by p.leap on 09-Jun-17.
 */
@Api(value = "/installmentPay",description = "InstallmentPay")
@Path("/installmentPay")
public class InstallmentPay implements FinServicesHelper{

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(
            value="Installment",
            notes="Installment"
    )
    @ApiResponses(value = {
            @ApiResponse(code=200, message = "Transaction Success"),
            @ApiResponse(code=404, message = "Contract not found"),
            @ApiResponse(code = 500, message = "Internal Error")
    })

    public String installmentPay(InstallmentDTO installmentDTO){
        String response="";
        try{
            Contract contract=INSTALLMENT_SERVICE_MFP.getByField(Contract.class,"reference",installmentDTO.getRefID());
            if(contract!=null){
                List<InstallmentVO> installmentVOs= INSTALLMENT_SERVICE_MFP.getInstallmentVOs(contract,1);

                for(InstallmentVO installmentVO:installmentVOs){
                    installmentVO.getInstallmentDate();
                }
                if(installmentVOs!=null || !installmentVOs.isEmpty()){
                    response="Transaction Success";
                }else{
                    response="Incorrect installment number";
                }
            }else{
                response="Contract not found";
            }
        }catch (Exception e){
            e.printStackTrace();
            response=e.getMessage();
        }
    return response;
    }
}
