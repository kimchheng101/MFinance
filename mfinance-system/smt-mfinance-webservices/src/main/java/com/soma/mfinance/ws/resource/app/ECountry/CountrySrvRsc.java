package com.soma.mfinance.ws.resource.app.ECountry;

import com.soma.common.app.eref.ECountry;
import com.soma.mfinance.core.custom.erefdata.ERefDataHelper;
import com.soma.mfinance.ws.resource.model.response.country.CountryDTO;
import com.soma.frmk.messaging.ws.EResponseStatus;
import com.soma.frmk.messaging.ws.ResponseHelper;
import com.soma.frmk.messaging.ws.WsReponseException;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by kanchanproseth on 6/2/17.
 */
@Path("/country")
public class CountrySrvRsc extends CountryResSrvRsc {

    /**
     * LIST
     * @return
     */
    @POST
    @Path("/list_country")
    @Produces(MediaType.APPLICATION_JSON + SEMI_COLON + CHARSET_UTF8)
    public Response listCountry() {
        try {
        Map<Long, ECountry> mapCountries = (Map<Long, ECountry>) ERefDataHelper.refData.get(ECountry.class.getName());
        List<ECountry> listCountries = new ArrayList<>(mapCountries.values());
            List<CountryDTO> listCountryDTOS = new ArrayList<>();
            for (ECountry country : listCountries) {
                listCountryDTOS.add(toListCountriesDTO(country));
            }
            return ResponseHelper.ok(listCountryDTOS);

        } catch (Exception e) {
            String errMsg = "Error while searching Individuals [" + e.getMessage() + "]";
            LOG.error(errMsg, e);
            throw new WsReponseException(EResponseStatus.GET_LIST_KO, errMsg);
        }
    }

}
