package com.soma.mfinance.ws.resource.model.response.financial;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.soma.mfinance.ws.resource.model.response.StandardRes;

/**
 * Created by v.hum on 6/5/2017.
 */
public class FinancialDTO extends StandardRes {
    @JsonProperty("CODE")
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
