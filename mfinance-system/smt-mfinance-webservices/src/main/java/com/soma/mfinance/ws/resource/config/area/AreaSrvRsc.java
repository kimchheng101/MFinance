package com.soma.mfinance.ws.resource.config.area;

import com.soma.mfinance.core.address.model.Area;
import com.soma.mfinance.core.collection.service.AreaRestriction;
import com.soma.mfinance.share.area.AreaDTO;
import com.soma.mfinance.ws.FinResourceSrvRsc;
import com.soma.frmk.messaging.ws.EResponseStatus;
import com.soma.frmk.messaging.ws.ResponseHelper;
import com.soma.frmk.messaging.ws.WsReponseException;
import org.seuksa.frmk.i18n.I18N;

import javax.persistence.EntityNotFoundException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author kimsuor.seang
 */
@Path("/configs/areas")
public class AreaSrvRsc extends FinResourceSrvRsc {
	
	/**
	 * List all areas
	 * @param subDistrictId
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON + SEMI_COLON + CHARSET_UTF8)
	public Response list(@QueryParam("subdistrictid") Long subDistrictId) {		
		try {				
			AreaRestriction restrictions = new AreaRestriction();
			if (subDistrictId != null) {
				restrictions.setCommuneId(subDistrictId);
			}
			List<Area> areas = ENTITY_SRV.list(restrictions);
			List<AreaDTO> areaDTOs = new ArrayList<>();
			for (Area area : areas) {
				areaDTOs.add(toAreaDTO(area));
			}
			return ResponseHelper.ok(areaDTOs);
		} catch (EntityNotFoundException e) {
			String errMsg = e.getMessage();
			LOG.error(errMsg, e);
			throw new WsReponseException(EResponseStatus.NOT_FOUND, errMsg);
		} catch (Exception e) {
			String errMsg = I18N.messageUnexpectedException(e.getMessage());
			LOG.error(errMsg, e);
			throw new WsReponseException(EResponseStatus.GET_LIST_KO, errMsg);
		} 
	}
	
	/**
	 * List Area by id
	 * @param id
	 * @return
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON + SEMI_COLON + CHARSET_UTF8)
	public Response get(@PathParam("id") Long id) {
		try {
			LOG.debug("Area - id. [" + id + "]");
		
			Area area = ENTITY_SRV.getById(Area.class, id);
			
			if (area == null) {
				String errMsg = "Area [" + id + "]";
				throw new EntityNotFoundException(I18N.messageObjectNotFound(errMsg));
			}
			
			AreaDTO areaDTO = toAreaDTO(area);
			
			return ResponseHelper.ok(areaDTO);
		} catch (EntityNotFoundException e) {
			String errMsg = e.getMessage();
			LOG.error(errMsg, e);
			throw new WsReponseException(EResponseStatus.NOT_FOUND, errMsg);
		} catch (Exception e) {
			String errMsg = I18N.messageUnexpectedException(e.getMessage());
			LOG.error(errMsg, e);
			throw new WsReponseException(EResponseStatus.GET_UNIQUE_KO, errMsg);
		}
	}
	
	/**
	 * 
	 * @param area
	 * @return
	 */
	private AreaDTO toAreaDTO(Area area) {
		AreaDTO areaDTO = new AreaDTO();
		areaDTO.setId(area.getId());
		areaDTO.setCode(area.getCode());
		areaDTO.setShordCode(area.getShordCode());
		areaDTO.setPostalCode(area.getPostalCode());
		areaDTO.setStreet(area.getStreet());
		areaDTO.setProvince(area.getProvince() != null ? getProvinceUriDTO(area.getProvince().getId()) : null);
		areaDTO.setDistrict(area.getDistricts().get(0) != null ? getDistrictUriDTO(area.getDistricts().get(0).getId()) : null);
		areaDTO.setCommune(area.getCommunes().get(0) != null ? getSubDistrictUriDTO(area.getCommunes().get(0).getId()) : null);
		return areaDTO;
	}
}
