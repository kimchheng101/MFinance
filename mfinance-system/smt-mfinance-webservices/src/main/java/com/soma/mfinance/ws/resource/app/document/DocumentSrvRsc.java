package com.soma.mfinance.ws.resource.app.document;

import com.soma.mfinance.core.document.model.Document;
import com.soma.mfinance.core.document.model.DocumentGroup;
import com.soma.mfinance.ws.resource.model.response.document.DocumentDTO;
import com.soma.mfinance.ws.resource.model.response.document.GroupDocumentDTO;
import com.soma.frmk.messaging.ws.EResponseStatus;
import com.soma.frmk.messaging.ws.ResponseHelper;
import com.soma.frmk.messaging.ws.WsReponseException;
//import org.glassfish.jersey.media.multipart.*;
//
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by kanchanproseth on 6/3/17.
 */
@Path("/document")
public class DocumentSrvRsc extends DocumentResSrvRsc {

    @POST
    @Path("/list_document")
    @Produces(MediaType.APPLICATION_JSON + SEMI_COLON + CHARSET_UTF8)
    public Response listDocument() {
        try {

            List<Document> documents = ENTITY_SRV.list(Document.class);
            List<DocumentDTO> listDocumentDTOS = new ArrayList<>();
            for (Document document : documents) {
                    if ((document.getCode().equals("N")&& document.getDocumentGroup().getCode().equals("CE"))||(document.getCode().equals("PAT")&&document.getDocumentGroup().getCode().equals("CE"))) {
                        listDocumentDTOS.add(toListDocumentDTO(document));
                    }
            }
            return ResponseHelper.ok(listDocumentDTOS);
        } catch (Exception e) {
            String errMsg = "Error while searching Individuals [" + e.getMessage() + "]";
            LOG.error(errMsg, e);
            throw new WsReponseException(EResponseStatus.GET_LIST_KO, errMsg);
        }
    }

    @POST
    @Path("/list_group")
    @Produces(MediaType.APPLICATION_JSON + SEMI_COLON + CHARSET_UTF8)
    public Response listGroup() {
        try {

            List<DocumentGroup> documentGroups = ENTITY_SRV.list(DocumentGroup.class);
            List<GroupDocumentDTO> listGroupDocumentDTOS = new ArrayList<>();
            for (DocumentGroup documentGroup : documentGroups) {
                if (documentGroup.getCode().equals("CE")) {
                    listGroupDocumentDTOS.add(toListGroupDocumentDTO(documentGroup));
                }
            }
            return ResponseHelper.ok(listGroupDocumentDTOS);
        } catch (Exception e) {
            String errMsg = "Error while searching Individuals [" + e.getMessage() + "]";
            LOG.error(errMsg, e);
            throw new WsReponseException(EResponseStatus.GET_LIST_KO, errMsg);
        }
    }





//
//    @POST
//    @Path("/pdf")
//    @Consumes({MediaType.MULTIPART_FORM_DATA})
//    public Response uploadPdfFile(	@FormDataParam("file") InputStream fileInputStream,
//                                      @FormDataParam("file") FormDataContentDisposition fileMetaData) throws Exception
//    {
//        String UPLOAD_PATH = "/tmp/";
//        try
//        {
//            int read = 0;
//            byte[] bytes = new byte[1024];
//
//            OutputStream out = new FileOutputStream(new File(UPLOAD_PATH + fileMetaData.getFileName()));
//            while ((read = fileInputStream.read(bytes)) != -1)
//            {
//                out.write(bytes, 0, read);
//            }
//            out.flush();
//            out.close();
//        } catch (IOException e)
//        {
//            throw new WebApplicationException("Error while uploading file. Please try again !!");
//        }
//        return Response.ok("Data uploaded successfully !!").build();
//    }



}
