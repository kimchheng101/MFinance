package com.soma.mfinance.ws.resource.app.installment;

import com.soma.common.app.workflow.model.EntityWkf;
import com.soma.mfinance.core.accounting.InstallmentVO;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowType;
import com.soma.mfinance.core.contract.model.cashflow.ETreasuryType;
import com.soma.mfinance.core.contract.service.cashflow.impl.CashflowUtils;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.entityfield.InstallmentEntityField;
import com.soma.mfinance.core.financial.model.EServiceType;
import com.soma.mfinance.core.financial.model.FinService;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.payment.model.EPaymentMethod;
import com.soma.mfinance.core.payment.model.Payment;
import com.soma.mfinance.core.shared.contract.PenaltyVO;
import com.soma.mfinance.core.shared.mplus.accounting.InstallmentServiceMfp;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.mfinance.third.conf.ModuleConfig;
import com.soma.mfinance.third.intergration.model.EThirdParty;
import com.soma.mfinance.third.intergration.model.Ticket;
import com.soma.mfinance.ws.resource.Message;
import com.soma.mfinance.ws.resource.StatusMessage;
import com.soma.mfinance.ws.resource.model.request.ConfirmInstallmentRequestMessage;
import com.soma.mfinance.ws.resource.model.request.GetInstallmentRequestMessage;
import com.soma.mfinance.ws.resource.model.response.installment.ConfirmInstallmentResponseMessage;
import com.soma.mfinance.ws.resource.model.response.installment.Currency;
import com.soma.mfinance.ws.resource.model.response.installment.GetInstallmentResponseMessage;
import com.soma.frmk.messaging.ws.rest.BaseResource;
import com.soma.frmk.security.model.SecUser;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.QueryException;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyMathUtils;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by p.leap on 09-Jun-17.
 */
@Api(value = "/installment", description = "Installment")
@Path("/installment")
public class InstallmentSrvRsc extends BaseResource implements FinServicesHelper, InstallmentEntityField {

    protected static final Log log = LogFactory.getLog(InstallmentServiceMfp.class);
    List<Cashflow> cashflows;
    private int penaltyDays;
    private Message errorMessage = null;//Used by ConfirmInstallmentResponseMessage only

    @POST
    @Path("/getinstinfo")
    @Produces(MediaType.APPLICATION_JSON + SEMI_COLON + CHARSET_UTF8)
    @Consumes(MediaType.APPLICATION_JSON + SEMI_COLON + CHARSET_UTF8)
    @ApiOperation(
            value = "Installment",
            notes = "Installment"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Internal server error"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 503, message = "Service unavailable"),
            @ApiResponse(code = 410, message = "Gone")
    })

    public GetInstallmentResponseMessage getInstallmentResponseMessage(GetInstallmentRequestMessage infoRequestMessage) {
        //protected static final Log log = LogFactory.getLog(InstallmentServiceImpl.class);
        Message errorMessage = null;
        //Validate data that get from param infoRequestMessage before query data from database
        if (infoRequestMessage != null) {
            errorMessage = controlServiceHeader(infoRequestMessage.getEThirdParty(), infoRequestMessage.getUsername(), infoRequestMessage.getPassword());
            if (errorMessage == null) {
                if (StringUtils.isEmpty(infoRequestMessage.getReference_number())) {
                    errorMessage = Message.CONTRACT_NOT_FOUND;
                } else if (infoRequestMessage.getReference_number().length() != 8) {
                    errorMessage = Message.CONTRACT_LENGTH_NOT_CORRECT;
                } else if (isReferenceNumberRejected(infoRequestMessage.getReference_number())) {
                    //log.error("reference number is included in the reject list");
                    errorMessage = Message.OTHER_ERROR;
                }
            }

        } else {
            errorMessage = Message.REQUEST_MESSAGE_MANDATORY;
        }

        GetInstallmentResponseMessage getInstallmentResponseMessage = new GetInstallmentResponseMessage();
        //If there is any error message from above it will return and won't do other job
        if (errorMessage != null) {
            getInstallmentResponseMessage.setResponse_msg(errorMessage.getText());
            getInstallmentResponseMessage.setResponse_code(StatusMessage.BAD_REQUEST.getStatusCode());
            return getInstallmentResponseMessage;
        } else {

            BaseRestrictions<Contract> restrictions = new BaseRestrictions<>(Contract.class);
            restrictions.addCriterion(Restrictions.eq(EntityWkf.WKFSTATUS, ContractWkfStatus.FIN));
            restrictions.addCriterion(Restrictions.like(REFERENCE, infoRequestMessage.getReference_number(), MatchMode.ANYWHERE));
            List<Contract> contracts = ENTITY_SRV.list(restrictions);
            if (contracts != null && !contracts.isEmpty()) {
                Contract contract = contracts.get(0);
                int currentInstallmentNum = 1;
                if (contract.getLastPaidNumInstallment() != null) {
                    currentInstallmentNum = contract.getLastPaidNumInstallment() + 1;
                }
                List<InstallmentVO> installmentByNumInstallments = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(contract, currentInstallmentNum);
                //Use installmentByNumInstallments.get(0): installmentByNumInstallments has only one installmentByNumInstallment.
                // Becasue we used INSTALLMENT_SERVICE_MFP.getInstallmentVOs(contract_old, currentInstallmentNum) so it will get by one installmentNumber that we put.
                EPaymentMethod ePaymentMethod=new EPaymentMethod();
                try {

                    InstallmentVO installmentVOFirstIndex = installmentByNumInstallments.get(0);
                    double totalInstallment = 0d;
                    double totalPayment = 0d;
                    double commisionIncludeVat = 0d;
                    double commission = 0d;
                    double vatOfCommission = 0d;
                    ePaymentMethod = PAYMENT_SERVICE_MFP.getByCode(EPaymentMethod.class, infoRequestMessage.getEThirdParty().getCode());
                    FinService finService = ePaymentMethod.getService();

                    if (finService != null) {
                        commission = finService.getTiPrice();
                        if (finService.getVat() != null) {
                            vatOfCommission = finService.getVat().getValue() * commission / 100;
                        }
                        commisionIncludeVat = commission + vatOfCommission;
                    }

                    for (InstallmentVO installmentVO : installmentByNumInstallments) {
                        totalInstallment += installmentVO.getTiamount() + installmentVO.getVatAmount();
                    }

                    PenaltyVO penaltyVO = CONT_SRV.calculatePenalty(contract, installmentVOFirstIndex.getInstallmentDate(), DateUtils.todayH00M00S00(), contract.getTiInstallmentAmount());
                    double penaltyAmount = 0d;
                    if (penaltyVO.getPenaltyAmount() != null) {
                        penaltyAmount += penaltyVO.getPenaltyAmount().getTiAmount() + penaltyVO.getPenaltyAmount().getVatAmount();
                    }
                    totalPayment = totalInstallment + penaltyAmount;

                    getInstallmentResponseMessage.setCustomer_name(contract.getApplicant().getLastNameEn() + " " + contract.getApplicant().getFirstNameEn());
                    getInstallmentResponseMessage.setCurrency(Currency.USD);
                    getInstallmentResponseMessage.setAmount(MyMathUtils.roundAmountTo(totalPayment + commisionIncludeVat));

                    Ticket installmentTicket = new Ticket();
                    installmentTicket.setUuid(UUID.randomUUID().toString());
                    installmentTicket.setContract(contract);
                    installmentTicket.setReference(infoRequestMessage.getReference_number());
                    installmentTicket.setInstallmentDate(installmentVOFirstIndex.getInstallmentDate());
                    installmentTicket.setThirdParty(infoRequestMessage.getEThirdParty());
                    installmentTicket.setPaidAmount(getInstallmentResponseMessage.getAmount());
                    installmentTicket.setUsername(infoRequestMessage.getUsername());
                    installmentTicket.setStatus("INPRO");//INPRO = INPROGRESS, It means it haven't paid yet and just get the information to show.
                    CONT_SRV.saveOrUpdate(installmentTicket);

                    getInstallmentResponseMessage.setSession_id(installmentTicket.getUuid());
                    getInstallmentResponseMessage.setReference_number(infoRequestMessage.getReference_number());
                    getInstallmentResponseMessage.setResponse_msg(Message.SUCCESSED.getText());
                    getInstallmentResponseMessage.setResponse_code(StatusMessage.OK.getStatusCode());
                    log.info("Exit getInstallmentInfo");

                } catch (NullPointerException e) {
                    if(ePaymentMethod==null){
                        errorMessage = Message.PAYMENT_METHOD_NOT_FOUND;
                        getInstallmentResponseMessage.setResponse_msg(errorMessage.getText());
                    }
                    getInstallmentResponseMessage.setResponse_code(StatusMessage.BAD_REQUEST.getStatusCode());
                }
            }
            return getInstallmentResponseMessage;
        }
    }

    /**
     * @param thirdParty
     * @param username
     * @param password
     * @return
     */
    private Message controlServiceHeader(EThirdParty thirdParty, String username, String password) {
        Message errorMessage = null;
        if (thirdParty == null) {
            errorMessage = Message.THIRDPARTY_NOT_FOUND;
        } else if (org.apache.commons.lang.StringUtils.isEmpty(username)) {
            errorMessage = Message.USERNAME_MANDATORY;
        } else if (org.apache.commons.lang.StringUtils.isEmpty(password)) {
            errorMessage = Message.PASSWORD_MANDATORY;
        } else {
            Configuration config = ModuleConfig.getInstance().getConfiguration();
            if (thirdParty.equals(EThirdParty.WING)) {
                if (!username.equals(config.getString("thirdparty.wing.username"))
                        || !password.equals(config.getString("thirdparty.wing.password"))) {
                    errorMessage = Message.USERNAME_NOT_FOUND;
                }
            } else if (thirdParty.equals(EThirdParty.PAYGO)) {
                if (!username.equals(config.getString("thirdparty.paygo.username"))
                        || !password.equals(config.getString("thirdparty.paygo.password"))) {
                    errorMessage = Message.USERNAME_NOT_FOUND;
                }
            } else if (thirdParty.equals(EThirdParty.TRUE_MONEY)) {
                if (!username.equals(config.getString("thirdparty.truemoney.username"))
                        || !password.equals(config.getString("thirdparty.truemoney.password"))) {
                    errorMessage = Message.USERNAME_NOT_FOUND;
                }
            } else if (thirdParty.equals(EThirdParty.EMONEY)) {
                if (!username.equals(config.getString("thirdparty.emoney.username"))
                        || !password.equals(config.getString("thirdparty.emoney.password"))) {
                    errorMessage = Message.USERNAME_NOT_FOUND;
                }
            } else {
                errorMessage = Message.OTHER_ERROR;
            }
        }
        return errorMessage;
    }

    private boolean isReferenceNumberRejected(String referenceNumber) {
        boolean isReferenceRejected = false;
        Configuration config = ModuleConfig.getInstance().getConfiguration();

        String rejectContracts = config.getString("thirdparty.wing.rejectcontracts");
        if (org.apache.commons.lang.StringUtils.isEmpty(rejectContracts)) {
            return false;
        }
        isReferenceRejected = rejectContracts.indexOf(referenceNumber) >= 0;

        return isReferenceRejected;
    }

    @POST
    @Path("/recinst")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Returns responsebody object",
            notes = "Returns response body object dealer.",
            httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 500, message = "Internal server error"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 503, message = "Service unavailable"),
            @ApiResponse(code = 410, message = "Gone")
    })
    public ConfirmInstallmentResponseMessage getConfirmInstallment(ConfirmInstallmentRequestMessage confirmInstallmentRequestMessage) {

        log.info("Enter confirmReceivePayment");

        if (confirmInstallmentRequestMessage != null) {
            errorMessage = controlServiceHeader(confirmInstallmentRequestMessage.getEThirdParty(), confirmInstallmentRequestMessage.getUsername(), confirmInstallmentRequestMessage.getPassword());
            if (errorMessage == null) {
                if (org.apache.commons.lang.StringUtils.isEmpty(confirmInstallmentRequestMessage.getReference_number())) {
                    errorMessage = Message.CONTRACT_NOT_FOUND;
                } else if (confirmInstallmentRequestMessage.getReference_number().length() != 8) {
                    errorMessage = Message.CONTRACT_LENGTH_NOT_CORRECT;
                } else if (org.apache.commons.lang.StringUtils.isEmpty(confirmInstallmentRequestMessage.getSession_id())) {
                    errorMessage = Message.TOKEN_ID_NOT_FOUND;
                }
            }
        } else {
            errorMessage = Message.REQUEST_MESSAGE_MANDATORY;
        }
        BaseRestrictions<Ticket> restrictions = new BaseRestrictions<>(Ticket.class);
        restrictions.addCriterion(Restrictions.eq("uuid", confirmInstallmentRequestMessage.getSession_id()));
        restrictions.addCriterion(Restrictions.eq("reference", confirmInstallmentRequestMessage.getReference_number()));
        restrictions.addCriterion(Restrictions.eq("status", "INPRO"));
        List<Ticket> installmentTickets = CONT_SRV.list(restrictions);

        if (installmentTickets == null || installmentTickets.size() != 1) {
            errorMessage = Message.TOKEN_ID_INVALID;
        } else if (confirmInstallmentRequestMessage.getAmount() < installmentTickets.get(0).getPaidAmount()) {
            errorMessage = Message.PAID_AMOUNT_NOT_MATCH;
        }
        log.info("reference - [" + confirmInstallmentRequestMessage.getReference_number() + "] uuid - [" + confirmInstallmentRequestMessage.getSession_id() + "]");
        ConfirmInstallmentResponseMessage confirmInstallmentResponseMessage = new ConfirmInstallmentResponseMessage();

        if (errorMessage != null) {
            log.error(errorMessage);
            confirmInstallmentResponseMessage.setResponse_code(StatusMessage.BAD_REQUEST.getStatusCode());
            confirmInstallmentResponseMessage.setResponse_msg(errorMessage.getText());
            log.error(errorMessage);
            return confirmInstallmentResponseMessage;
        } else {
            Ticket installmentTicket = installmentTickets.get(0);
            Contract contract = installmentTicket.getContract();
            if (contract != null) {
                int currentInstallmentNum = 1;
                if (contract.getLastPaidNumInstallment() != null) {
                    currentInstallmentNum = contract.getLastPaidNumInstallment() + 1;
                }
                List<InstallmentVO> installmentVOs = INSTALLMENT_SERVICE_MFP.getInstallmentVOs(contract, currentInstallmentNum);

                cashflows = setInstallmentDetail(installmentVOs, confirmInstallmentRequestMessage.getEThirdParty());
                receiveInstallment(contract, currentInstallmentNum, penaltyDays, confirmInstallmentRequestMessage.getEThirdParty(), installmentTickets.get(0).getPaidAmount(),installmentVOs.get(0),confirmInstallmentRequestMessage);
                if (errorMessage != null) {
                    confirmInstallmentResponseMessage.setResponse_code(StatusMessage.BAD_REQUEST.getStatusCode());
                    confirmInstallmentResponseMessage.setResponse_msg(errorMessage.getText());
                    return confirmInstallmentResponseMessage;
                }
                installmentTicket.setStatus("PROCE");
                log.debug("==== start update or save intallment ticket ");
                PAYMENT_SERVICE_MFP.saveOrUpdate(installmentTicket);
                log.debug("==== finised update or save intallment ticket === ");

                confirmInstallmentResponseMessage.setResponse_code(StatusMessage.OK.getStatusCode());
                confirmInstallmentResponseMessage.setResponse_msg(Message.SUCCESSED.getText());
                confirmInstallmentResponseMessage.setReference_number(confirmInstallmentRequestMessage.getReference_number());
                confirmInstallmentResponseMessage.setCurrency(confirmInstallmentRequestMessage.getCurrency());
                confirmInstallmentResponseMessage.setSession_id(confirmInstallmentRequestMessage.getSession_id());
                confirmInstallmentResponseMessage.setAmount(confirmInstallmentRequestMessage.getAmount());
            }
        }
        log.info("Exit confirmReceivePayment");
        return confirmInstallmentResponseMessage;
    }

    /**
     * @param installmentVOs
     * @return
     */
    private List<Cashflow> setInstallmentDetail(List<InstallmentVO> installmentVOs, EThirdParty eThirdParty) {
        List<Cashflow> cashflows = new ArrayList<Cashflow>();
        for (InstallmentVO installmentVO : installmentVOs) {
            if(installmentVO.getTiamount()==0d){
                continue;
            }
            Contract contract = installmentVO.getContract();
            Cashflow cashflow = CashflowUtils.createCashflow(contract.getProductLine(),
                    null, contract, 0d,
                    installmentVO.getCashflowType(), ETreasuryType.APP, null, contract.getProductLine().getPaymentConditionCap(),
                    installmentVO.getTiamount(), installmentVO.getVatAmount(), installmentVO.getTiamount(),
                    installmentVO.getInstallmentDate(), installmentVO.getPeriodStartDate(), installmentVO.getPeriodEndDate(), installmentVO.getNumInstallment());
            cashflow.setService(installmentVO.getService());
            cashflow.setPaymentMethod(PAYMENT_SERVICE_MFP.getByCode(EPaymentMethod.class, eThirdParty.getCode()));
            cashflow.setCreateUser("migration");
            cashflow.setUpdateUser("migration");
            cashflows.add(cashflow);
        }

        Contract contract = installmentVOs.get(0).getContract();
        PenaltyVO penaltyVO = CONT_SRV.calculatePenalty(contract, installmentVOs.get(0).getInstallmentDate(), DateUtils.todayH00M00S00(), contract.getTiInstallmentAmount());

        if (penaltyVO != null && penaltyVO.getNumPenaltyDays() != null && penaltyVO.getNumPenaltyDays() > 0) {
            penaltyDays = penaltyVO.getNumPenaltyDays();
            InstallmentVO installmentVO = installmentVOs.get(0);

            Cashflow cashflowPenalty = CashflowUtils.createCashflow(contract.getProductLine(),
                    null, contract, 0d,
                    ECashflowType.PEN, ETreasuryType.APP, null, contract.getProductLine().getPaymentConditionCap(),
                    penaltyVO.getPenaltyAmount().getTiAmount(), penaltyVO.getPenaltyAmount().getVatAmount(), penaltyVO.getPenaltyAmount().getTiAmount(),
                    installmentVO.getInstallmentDate(), installmentVO.getPeriodStartDate(), installmentVO.getPeriodEndDate(), installmentVO.getNumInstallment());
            cashflowPenalty.setPaymentMethod(PAYMENT_SERVICE_MFP.getByCode(EPaymentMethod.class, eThirdParty.getCode()));
            cashflowPenalty.setCreateUser("migration");
            cashflowPenalty.setUpdateUser("migration");
            cashflows.add(cashflowPenalty);
        }
        return cashflows;
    }

    /**
     * @param contract
     * @param numInstallment
     * @param penaltyDay
     * @param eThirdParty
     */
    private void receiveInstallment(Contract contract, int numInstallment, int penaltyDay, EThirdParty eThirdParty, double insFromTicket,InstallmentVO installmentVO,ConfirmInstallmentRequestMessage confirmInstallmentRequestMessage) {
        if ((((contract.getLastPaidNumInstallment() == null || contract.getLastPaidNumInstallment() == 0) && numInstallment == 1)
                || (contract.getLastPaidNumInstallment() != null && (numInstallment - 1) == contract.getLastPaidNumInstallment()))) {

            EPaymentMethod ePaymentMethod = PAYMENT_SERVICE_MFP.getByCode(EPaymentMethod.class, eThirdParty.getCode());
            if (ePaymentMethod == null) {
                errorMessage = Message.USER_RECEIVER_NOT_FOUND;
                log.info(errorMessage + " " + eThirdParty.getCode());
            }

            try {
                FinService finService= ePaymentMethod.getService();
                if (finService != null && EServiceType.COMM.getCode().equals(finService.getServiceType().getCode())) {
                    addCashflowCommission(finService,installmentVO,ePaymentMethod);
                }
                createPayment(cashflows, DateUtils.today(), penaltyDay, contract.getDealer(), ePaymentMethod, insFromTicket, confirmInstallmentRequestMessage);
            } catch (NullPointerException e) {
                e.printStackTrace();
                errorMessage = Message.OTHER_ERROR;
                log.info(errorMessage);
            }
        }
    }

    /***
     *
     * @param cashflows
     * @param paymentDate
     * @param penaltyDay
     * @param dealer
     * @param paymentMethod
     * @return
     */

    public Payment createPayment(List<Cashflow> cashflows, Date paymentDate, int penaltyDay, Dealer dealer, EPaymentMethod paymentMethod, double insFromTicket, ConfirmInstallmentRequestMessage confirmInstallmentRequestMessage) {
        log.debug("==== start to get user receive by code [" + paymentMethod.toString() + "] === ");

        try{
            PAYMENT_SRV.getByDesc(SecUser.class, paymentMethod.toString());
        }catch (QueryException e){
            errorMessage = Message.USERNAME_NOT_FOUND;
        }

        double totalPaid = 0d;
        for (Cashflow cashflow : cashflows) {
            totalPaid += cashflow.getTiInstallmentAmount() + cashflow.getVatInstallmentAmount();//has Both penalty and Real orignial Payment
            cashflow.setCreateUser("migration");
            cashflow.setUpdateUser("migration");
            ENTITY_SRV.saveOrUpdate(cashflow);
        }

        if (insFromTicket != MyMathUtils.roundAmountTo(totalPaid)
                || MyMathUtils.roundAmountTo(confirmInstallmentRequestMessage.getAmount())!=MyMathUtils.roundAmountTo(insFromTicket)) {
            errorMessage = Message.PAID_AMOUNT_NOT_MATCH;
            return null;
        }
        Payment payment = new Payment();
        PAYMENT_SERVICE_MFP.createPayment(cashflows, paymentDate, penaltyDay, dealer, paymentMethod);
        return payment;
    }

    /***
     * @author p.leap
     * @param finService
     */
    private void addCashflowCommission(FinService finService, InstallmentVO installmentVO,EPaymentMethod ePaymentMethod){
        double commission = 0d;
        double vatOfCommission = 0d;
        commission = finService.getTiPrice();
        if (finService.getVat() != null) {
            vatOfCommission = finService.getVat().getValue() * commission / 100;
        }

        Cashflow cashflowCommission = CashflowUtils.createCashflow(installmentVO.getContract().getProductLine(),
                null, installmentVO.getContract(), 0d,
                ECashflowType.FEE, ETreasuryType.APP, null, installmentVO.getContract().getProductLine().getPaymentConditionCap(),
                commission, vatOfCommission, commission,
                installmentVO.getInstallmentDate(), installmentVO.getPeriodStartDate(), installmentVO.getPeriodEndDate(), installmentVO.getNumInstallment());
        cashflowCommission.setService(finService);
        cashflowCommission.setPaymentMethod(ePaymentMethod);
        cashflowCommission.setCreateUser("migration");
        cashflowCommission.setUpdateUser("migration");
        cashflows.add(cashflowCommission);
    }
}