package com.soma.mfinance.ws.resource.model.response.installment;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by p.leap on 09-Jun-17.
 */
public class InstallmentDTO {

    @JsonProperty("REF_ID")
    private String refID;
    @JsonProperty("INSTALLMENT_NUMBER")
    private int instalNum;

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public int getInstalNum() {
        return instalNum;
    }

    public void setInstalNum(int instalNum) {
        this.instalNum = instalNum;
    }
}
