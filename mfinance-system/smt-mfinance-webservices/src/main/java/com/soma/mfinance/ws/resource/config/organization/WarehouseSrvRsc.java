package com.soma.mfinance.ws.resource.config.organization;

import javax.ws.rs.Path;

import com.soma.ersys.core.hr.model.eref.ESubTypeOrganization;
import com.soma.ersys.core.hr.model.organization.OrganizationSubTypes;
import com.soma.ersys.messaging.ws.resource.BaseLocationSrvRsc;

/**
 * 
 * @author kimsuor.seang
 *
 */
@Path("/configs/collections/{orgPath:warehouses}")
public class WarehouseSrvRsc extends BaseLocationSrvRsc {
	private static final ESubTypeOrganization SUB_TYPE_ORGANIZATION = OrganizationSubTypes.COLLECTION_LOCATION;
	
	@Override
	public ESubTypeOrganization getSubTypeOrganization() {
		return SUB_TYPE_ORGANIZATION;
	}

}
