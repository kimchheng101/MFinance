package com.soma.mfinance.ws.resource.config.organization;

import javax.ws.rs.Path;

import com.soma.ersys.core.hr.model.eref.ETypeOrganization;
import com.soma.ersys.core.hr.model.organization.OrganizationTypes;
import com.soma.ersys.messaging.ws.resource.BaseBranchSrvRsc;

/**
 * 
 * @author kimsuor.seang
 *
 */
@Path("/configs/collections/{orgPath:warehouses}")
public class AreasSrvRsc extends BaseBranchSrvRsc {
private static final ETypeOrganization TYPE_ORGANIZATION = OrganizationTypes.LOCATION;
	
	@Override
	public ETypeOrganization getTypeOrganization() {
		return TYPE_ORGANIZATION;
	}
}
