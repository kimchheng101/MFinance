package com.soma.mfinance.ws.resource.model.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.soma.mfinance.third.intergration.model.EThirdParty;
import com.soma.mfinance.ws.resource.model.response.installment.Currency;

import java.io.Serializable;

/**
 * Created by p.leap on 15-Jun-17.
 */
public class ConfirmInstallmentRequestMessage implements Serializable {

    @JsonProperty("reference_number")
    private String reference_number;
    @JsonProperty("currency")
    private Currency currency;
    @JsonProperty("amount")
    private double amount;
    @JsonProperty("session_id")
    private String session_id;
    @JsonProperty("transaction_id")
    private String transaction_id;
   /* @JsonProperty("REQUEST_MEDIUM_ID")
    private String request_medium_id;
    @JsonProperty("REQUEST_MEDIUM")
    private String request_medium;*/
    @JsonProperty("third_party")
    private String third_party;
    @JsonProperty("username")
    private String username;
    @JsonProperty("password")
    private String password;

    public String getReference_number() {
        return reference_number;
    }
    public void setReference_number(String reference_number) {
        this.reference_number = reference_number;
    }
    public Currency getCurrency() {
        return currency;
    }
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
    public double getAmount() {
        return amount;
    }
    public void setAmount(double amount) {
        this.amount = amount;
    }
    public String getSession_id() {
        return session_id;
    }
    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }
    public String getTransaction_id() {
        return transaction_id;
    }
    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }
   /* public String getRequest_medium_id() {
        return request_medium_id;
    }
    public void setRequest_medium_id(String request_medium_id) {
        this.request_medium_id = request_medium_id;
    }
    public String getRequest_medium() {
        return request_medium;
    }
    public void setRequest_medium(String request_medium) {
        this.request_medium = request_medium;
    }*/
    public String getThird_party() {
        return third_party;
    }
    public void setThird_party(String third_party) {
        this.third_party = third_party;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    @JsonIgnore
    public EThirdParty getEThirdParty() {
        return EThirdParty.getByCode(third_party.toUpperCase());
    }



}
