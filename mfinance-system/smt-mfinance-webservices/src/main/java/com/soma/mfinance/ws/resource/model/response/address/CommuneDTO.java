package com.soma.mfinance.ws.resource.model.response.address;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.soma.mfinance.ws.resource.model.response.StandardRes;

/**
 * Created by kanchanproseth on 6/3/17.
 */
public class CommuneDTO extends StandardRes {
    @JsonProperty("DISTRICT_ID")
    private Long district_id;

    public Long getDistrict_id() {
        return district_id;
    }

    public void setDistrict_id(Long district_id) {
        this.district_id = district_id;
    }
}
