package com.soma.mfinance.ws.resource.app.quotation;

import com.soma.common.app.eref.ECountry;
import com.soma.mfinance.client.jersey.ClientQuotation;
import com.soma.mfinance.core.Enum.ECertifyCurrentAddress;
import com.soma.mfinance.core.Enum.ECertifyIncome;
import com.soma.mfinance.core.Enum.ECorrespondence;
import com.soma.mfinance.core.Enum.EHousing;
import com.soma.mfinance.core.applicant.model.*;
import com.soma.mfinance.core.asset.model.*;
import com.soma.mfinance.core.custom.erefdata.ERefDataHelper;
import com.soma.mfinance.core.custom.loader.PropertyLoader;
import com.soma.mfinance.core.document.model.Document;
import com.soma.mfinance.core.financial.model.FinProduct;
import com.soma.mfinance.core.quotation.QuotationService;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationApplicant;
import com.soma.mfinance.core.quotation.model.QuotationDocument;
import com.soma.mfinance.share.quotation.QuotationDTO;
import com.soma.mfinance.ws.FinResourceSrvRsc;
import com.soma.mfinance.ws.WebServiceConstant;
import com.soma.mfinance.ws.resource.MFPBaseWsPath;
import com.soma.mfinance.ws.resource.MFPWsParam;
import com.soma.mfinance.ws.resource.MFPWsPath;
import com.soma.ersys.core.hr.model.address.*;
import com.soma.ersys.core.hr.model.eref.*;
import com.soma.finance.services.shared.system.EFrequency;
import com.soma.frmk.helper.SeuksaServicesHelper;
import com.soma.frmk.messaging.ws.EResponseStatus;
import com.soma.frmk.messaging.ws.ResponseHelper;
import com.soma.frmk.messaging.ws.WsReponseException;
import com.soma.frmk.security.model.SecUser;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.RefDataId;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.EntityNotFoundException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by cheasocheat on 4/3/17.
 */

@Api(value = MFPWsPath.QUOTATION, description = "Endpoint for quotation")
@Path(MFPWsPath.QUOTATION)
public class QuotationSrvRsc extends FinResourceSrvRsc implements WebServiceConstant, SeuksaServicesHelper {

    private QuotationService quotationService = SpringUtils.getBean(QuotationService.class);
    private PropertyLoader propertyLoader = SpringUtils.getBean(PropertyLoader.class);
    protected final static Logger logger = LoggerFactory.getLogger(QuotationSrvRsc.class);

    @GET
    @Path("/test")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Lists all employees",
            notes = "Lists all employees"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of employees"),
            @ApiResponse(code = 404, message = "Employee records not found"),
            @ApiResponse(code = 500, message = "Internal servererror")
    })
    public String test() {
        //return Response.status(Status.NO_CONTENT).entity("hello").build(); //this will throw 200
        return "Hello worlds";
    }

    @GET
    @Path("/list")
    @ApiOperation(
            value = "Lists all quotations",
            notes = "Lists all quotations"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of quotations"),
            @ApiResponse(code = 404, message = "quotations records not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Produces(MediaType.APPLICATION_JSON + SEMI_COLON + CHARSET_UTF8)
    public Response getQuotationList() {
        List<QuotationDTO> quotationDTO = toQuotationDTO(ENTITY_SRV.list(Quotation.class));
        return ResponseHelper.ok(quotationDTO);
    }

    @GET
    @Path(MFPBaseWsPath.PATH_ID)
    @Produces(MediaType.APPLICATION_JSON + SEMI_COLON + CHARSET_UTF8)
    public Response getQuotationById(@PathParam(MFPWsParam.ID) Long id) {
        try {
            Quotation quotation = ENTITY_SRV.getById(Quotation.class, id);
            if (quotation == null) {
                String errMsg = "==> Quotation ID = " + id;
                throw new EntityNotFoundException(I18N.messageObjectNotFound(errMsg));
            }
            QuotationDTO quotationDTO = toQuotationDTO(quotation);

            return ResponseHelper.ok(quotationDTO);
        } catch (EntityNotFoundException e) {
            String errMsg = e.getMessage();
            LOG.error(errMsg, e);
            throw new WsReponseException(EResponseStatus.NOT_FOUND, errMsg);
        } catch (Exception e) {
            String errMsg = I18N.messageUnexpectedException(e.getMessage());
            LOG.error(errMsg, e);
            throw new WsReponseException(EResponseStatus.GET_UNIQUE_KO, errMsg);
        }
    }

    @GET
    @Path(MFPBaseWsPath.LESSEE + MFPBaseWsPath.REFERENCE)
    @Produces(MediaType.APPLICATION_JSON + SEMI_COLON + CHARSET_UTF8)
    public Response getQuotationByReference(@Context HttpHeaders httpHeaders, @PathParam(MFPWsParam.REFERENCE) String reference) {

        try {
            Quotation quotation = null;

            //Check if already exist in MFP
            if (this.checkExistingQuotation(reference)) {
                return ResponseHelper.error(EResponseStatus.ALREADY_EXISTS, "Quotation already existed in MotoForPlus", "");
            }

            Response response = null;
            if (propertyLoader != null) {
                String ef_ws_url = propertyLoader.getEnvironment().getProperty("ws.mfinance.webservice");
                if (ef_ws_url != null && !ef_ws_url.isEmpty()) {
                    response = ClientQuotation.getEFContractByReference(ef_ws_url, reference);
                }
            }

            //Add to Entity Quotation
            if (response != null) {
                if (response.getStatus() == Response.Status.OK.getStatusCode()) {
                    String jsonOutput = response.readEntity(String.class);
                    quotation = new Quotation();
                    JSONObject jsonObject = new JSONObject(jsonOutput);
                    if (jsonObject != null) {
                        JSONArray jsonArray = jsonObject.getJSONArray(RSLT_DATA);
                        if (jsonArray != null && jsonArray.length() != 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

                                //Reference
                                String ref = object.getString(REFERENCE_ID);
                                quotation.setReference(ref);

                                //Asset
                                JSONObject jsAsset = object.getJSONObject(ASSET);
                                if (jsAsset != null) {
                                    Asset asset = new Asset();
                                    int year = jsAsset.getInt("YEAR");
                                    String color = jsAsset.getString("COLOR");
                                    String engine = jsAsset.getString("ENGINE");
                                    String assGender = jsAsset.getString("ASS_GENDER");
                                    asset.setTiAssetPrice(jsAsset.getDouble("TI_ASSET_PRICE"));
                                    asset.setTeAssetPrice(jsAsset.getDouble("TE_ASSET_PRICE"));
                                    asset.setVatAssetPrice(jsAsset.getDouble("VAT_ASSET_PRICE"));
                                    asset.setPlateNumber(jsAsset.getString("PLATE_NUMBER"));
                                    asset.setChassisNumber(jsAsset.getString("CHASSIS_NUMBER"));
                                    asset.setEngineNumber(jsAsset.getString("ENGINE_NUMBER"));
                                    if (!jsAsset.getString("REGIST_DATE").isEmpty())
                                        asset.setRegistrationDate(formatter.parse(jsAsset.getString("REGIST_DATE")));

                                    //Set to year
                                    asset.setYear(year);

                                    //Set Asset Year
                                    if (this.getEAssetYear(year) != null) {
                                        asset.setAssetYear(this.getEAssetYear(year));
                                    }

                                    //Color
                                    if (color != null) {
                                        String eColorCode = this.getEColorCode(color);
                                        if (eColorCode != null) {
                                            asset.setColor(this.getRefDataObject(EColor.class, eColorCode));
                                        }
                                    }

                                    //Engine
                                    if (engine != null) {
                                        String eEngineCode = this.getEEngineCode(engine);
                                        if (eEngineCode != null) {
                                            asset.setEngine(this.getRefDataObject(EEngine.class, eEngineCode));
                                        }
                                    }

                                    //gender
                                    if (assGender != null) {
                                        if (("F".compareToIgnoreCase(assGender.trim())) == 0) {
                                            asset.setAssetGender(EAssetGender.F);
                                        } else if (("M".compareToIgnoreCase(assGender.trim())) == 0) {
                                            asset.setAssetGender(EAssetGender.M);
                                        }
                                    }


                                    //AssetModel
                                    JSONObject jsonAssetModel = jsAsset.getJSONObject("ASS_MODEL");
                                    if (jsonAssetModel != null) {
                                    /*
                                    *   This case ass_code of MPF == EFINANCE
                                    *   else config it.
                                    * */
                                        String ass_code = jsonAssetModel.getString("CODE");
                                        AssetModel assetModel = this.getAssetModel(ass_code);
                                        if (assetModel != null) {
                                            asset.setModel(assetModel);
                                        }
                                    }

                                    quotation.setAsset(asset);
                                }


                                //Applicant
                                JSONArray lstApplicants = object.getJSONArray(APPLICANTS);
                                Applicant applicant = null;
                                for (int j = 0; j < lstApplicants.length(); j++) {
                                    JSONObject jsonApplicant = lstApplicants.getJSONObject(i);
                                    if (jsonApplicant != null) {

                                        //Individual
                                        Individual individual = new Individual();

                                        String civility = jsonApplicant.getString("CIVILITY");
                                        if (civility.equals(ECivility.MR.getCode())) {
                                            individual.setCivility(ECivility.MR);
                                        } else if (civility.equals(ECivility.MRS.getCode())) {
                                            individual.setCivility(ECivility.MRS);
                                        } else if (civility.equals(ECivility.MS.getCode())) {
                                            individual.setCivility(ECivility.MS);
                                        }
                                        individual.setFirstName(jsonApplicant.getString("FIRST_NAME"));
                                        individual.setFirstNameEn(jsonApplicant.getString("FIRST_NAME_EN"));
                                        individual.setLastName(jsonApplicant.getString("LAST_NAME"));
                                        individual.setLastNameEn(jsonApplicant.getString("LAST_NAME_EN"));
                                        String gender = jsonApplicant.getString("GENDER");
                                        if (gender.equals(EGender.F.getCode())) {
                                            individual.setGender(EGender.F);
                                        } else if (gender.equals(EGender.M.getCode())) {
                                            individual.setGender(EGender.M);
                                        } else if (gender.equals(EGender.U)) {
                                            individual.setGender(EGender.U);
                                        }

                                        individual.setChiefVillagePhoneNumber(jsonApplicant.getString("CHIEF_PHONE"));
                                        individual.setChiefVillageName(jsonApplicant.getString("CHIEF_NAME"));
                                        individual.setDeputyChiefVillagePhoneNumber(jsonApplicant.getString("DEPUTY_CHIEF_PHONE"));
                                        individual.setDeputyChiefVillageName(jsonApplicant.getString("DEPUTY_CHIEF_NAME"));
                                        //City of birth
                                        String cityOfBirth = jsonApplicant.getString("CITY_OF_BIRTH");
                                        if (cityOfBirth != null && cityOfBirth != "") {
                                            if (("Cambodia".compareToIgnoreCase(cityOfBirth.trim())) == 0) {
                                                individual.setBirthCountry(ECountry.KHM);
                                            }
                                        }

                                        individual.setMobilePerso(jsonApplicant.getString("MOBILE_PHONE"));
                                        individual.setMobilePerso2(jsonApplicant.getString("MOBILE_PHONE_2"));
                                        individual.setBirthDate(formatter.parse(jsonApplicant.getString("BIRTH_DATE")));
                                        individual.setTotalFamilyMember(jsonApplicant.getInt("TOTAL_FAMILY_MEMBER"));

                                        //Individual Spouse
                                        List<IndividualSpouse> individualSpouses = new ArrayList<>();
                                        IndividualSpouse individualSpouse = new IndividualSpouse();
                                        individualSpouse.setFirstName("");
                                        individualSpouse.setLastName("");
                                        individualSpouse.setNickName("");
                                        individualSpouse.setMiddleName("");
                                        individualSpouse.setMobilePhone(jsonApplicant.getString("SPOUSE_MOBILE"));
                                        //individualSpouse.setIndividual(individual);

                                        individualSpouses.add(individualSpouse);
                                        individual.setIndividualSpouses(individualSpouses);

                                        //Place of birth
                                        JSONObject jsonBirthPlace = jsonApplicant.getJSONObject("PLACE_OF_BIRTH");
                                        Province provOfBirth = new Province();
                                        provOfBirth.setId(jsonBirthPlace.getLong("ID"));
                                        provOfBirth.setCode(jsonBirthPlace.getString("CODE"));
                                        provOfBirth.setDesc(jsonBirthPlace.getString("DESC"));
                                        provOfBirth.setDescEn(jsonBirthPlace.getString("DESC_EN"));
                                        provOfBirth.setUpdateDate(formatter.parse(jsonBirthPlace.getString("UPDATE_DATE")));
                                        //province.setUpdateDate();
                                        individual.setPlaceOfBirth(provOfBirth);

                                        //Nationality
                                        Long nationality = jsonApplicant.getLong("NATIONALITY");
                                        if (nationality == 1l) {
                                            individual.setNationality(ENationality.KHMER);
                                        }

                                        //Marital Status
                                        String maritalStatus = jsonApplicant.getString("MARITAL_STATUS");
                                        if (this.getMaritialStatus(maritalStatus) != null) {
                                            individual.setMaritalStatus(this.getMaritialStatus(maritalStatus));
                                        }

                                        //Address
                                        IndividualAddress indAddr = new IndividualAddress();
                                        JSONObject jsonAddr = jsonApplicant.getJSONObject("ADDRESS");

                                        if (jsonAddr != null) {
                                            indAddr.setTimeAtAddressInYear(jsonAddr.getInt("TIME_YEAR"));
                                            indAddr.setTimeAtAddressInMonth(jsonAddr.getInt("TIME_MONTH"));
                                            indAddr.setZipCode(jsonAddr.getString("ZIP_CODE"));

                                            Address address = new Address();
                                            address.setHouseNo(jsonAddr.getString("HOUSE_NO"));
                                            address.setStreet(jsonAddr.getString("STREET"));

                                            String country = jsonAddr.getString("COUNTRY");
                                            if (("Cambodia".compareToIgnoreCase(country.trim())) == 0) {
                                                address.setCountry(ECountry.KHM);
                                            } else if (("Laos".compareToIgnoreCase(country.trim())) == 0) {
                                                address.setCountry(ECountry.LAO);
                                            }  //==>> Add more TH + VN


                                            //Housing
                                            JSONObject housing = jsonAddr.getJSONObject("HOUSING");
                                            if (housing != null) {
                                                String eHousingCode = this.getEHousingCode(housing.getString("CODE"));
                                                if (eHousingCode != null) {
                                                    indAddr.setHousing(this.getRefDataObject(EHousing.class, eHousingCode));
                                                }
                                            }

                                            //Province
                                            JSONObject jsonProv = jsonAddr.getJSONObject("PROVIN");
                                            Province province = new Province();
                                            province.setId(jsonProv.getLong("ID"));
                                            province.setCode(jsonProv.getString("CODE"));
                                            province.setDesc(jsonProv.getString("DESC"));
                                            province.setDescEn(jsonProv.getString("DESC_EN"));
                                            province.setUpdateDate(formatter.parse(jsonProv.getString("UPDATE_DATE")));

                                            //District
                                            JSONObject jsonDistrict = jsonAddr.getJSONObject("DISTRICT");
                                            District district = new District();
                                            district.setId(jsonDistrict.getLong("ID"));
                                            district.setCode(jsonDistrict.getString("CODE"));
                                            district.setDesc(jsonDistrict.getString("DESC"));
                                            district.setDescEn(jsonDistrict.getString("DESC_EN"));
                                            district.setUpdateDate(formatter.parse(jsonDistrict.getString("UPDATE_DATE")));
                                            district.setProvince(province);

                                            //Commune
                                            JSONObject jsonCommune = jsonAddr.getJSONObject("COMMUNE");
                                            Commune commune = new Commune();
                                            commune.setId(jsonCommune.getLong("ID"));
                                            commune.setCode(jsonCommune.getString("CODE"));
                                            commune.setDesc(jsonCommune.getString("DESC"));
                                            commune.setDescEn(jsonCommune.getString("DESC_EN"));
                                            commune.setUpdateDate(formatter.parse(jsonCommune.getString("UPDATE_DATE")));
                                            commune.setDistrict(district);

                                            //Village
                                            JSONObject jsonVillage = jsonAddr.getJSONObject("VILLAGE");
                                            Village villeage = new Village();
                                            villeage.setId(jsonVillage.getLong("ID"));
                                            villeage.setCode(jsonVillage.getString("CODE"));
                                            villeage.setDesc(jsonVillage.getString("DESC"));
                                            villeage.setDescEn(jsonVillage.getString("DESC_EN"));
                                            villeage.setUpdateDate(formatter.parse(jsonVillage.getString("UPDATE_DATE")));
                                            villeage.setCommune(commune);

                                            //add to address
                                            address.setProvince(province);
                                            address.setDistrict(district);
                                            address.setCommune(commune);
                                            address.setVillage(villeage);

                                            //Add to individual addr
                                            indAddr.setAddress(address);

                                            List<IndividualAddress> lstIndividualAddress = new ArrayList<>();
                                            lstIndividualAddress.add(indAddr);

                                            //set individual address to individual
                                            individual.setIndividualAddresses(lstIndividualAddress);

                                            //else wont work
                                            individual.setECertifyCurrentAddress(ECertifyCurrentAddress.TEST);
                                            individual.setECertifyIncome(ECertifyIncome.TEST);
                                            individual.setECorrespondence(ECorrespondence.CURRENT_ADDRESS);


                                            applicant = new Applicant();
                                            applicant.setIndividual(individual);
                                            applicant.setApplicantCategory(EApplicantCategory.INDIVIDUAL);
                                        }
                                    }
                                }

                                quotation.setApplicant(applicant);
                                List<QuotationApplicant> quotationApplicants = new ArrayList<>();

                                QuotationApplicant quotationApplicant = new QuotationApplicant();
                                quotationApplicant.setApplicant(applicant);
                                quotationApplicant.setApplicantType(EApplicantType.C);
                                quotationApplicant.setQuotation(quotation);
                                quotationApplicants.add(quotationApplicant);

                                quotation.setQuotationApplicants(quotationApplicants);

                                //Documents
                                JSONArray jsLstDocuments = object.getJSONArray(DOCUMENTS);
                                List<QuotationDocument> lstQuotaDocuments = new ArrayList<>();
                                QuotationDocument quotationDocument = null;
                                if (jsLstDocuments != null && jsLstDocuments.length() > 0) {
                                    for (int j = 0; j < jsLstDocuments.length(); j++) {
                                        JSONObject jsDocument = jsLstDocuments.getJSONObject(j);
                                        if (jsDocument != null) {
                                            Document document = this.getDocumentByCode(jsDocument.getLong("ID"), jsDocument.getLong("GROUP_ID"));
                                            if (document != null) {
                                                quotationDocument = new QuotationDocument();
                                                quotationDocument.setPath(jsDocument.getString("DOC_PATH"));
                                                quotationDocument.setReference(jsDocument.getString("DOC_REFERENCE"));
                                                if (!jsDocument.getString("DOC_ISS_DATE").equals("null")) {
                                                    quotationDocument.setIssueDate(formatter.parse(jsDocument.getString("DOC_ISS_DATE")));
                                                }
                                                if (!jsDocument.getString("DOC_EXP_DATE").equals("null")) {
                                                    quotationDocument.setExpireDate(formatter.parse(jsDocument.getString("DOC_EXP_DATE")));
                                                }

                                                quotationDocument.setOriginal(jsDocument.getBoolean("DOC_ORIGIN"));
                                                quotationDocument.setCovConfirmationCo(jsDocument.getBoolean("COV_CONF_CO"));
                                                quotationDocument.setCovConfirmationUw(jsDocument.getBoolean("COV_CONF_UW"));
                                                quotationDocument.setEmployerConfirmationCo(jsDocument.getBoolean("EMP_CONF_CO"));
                                                quotationDocument.setEmployerConfirmationUw(jsDocument.getBoolean("EMP_CONF_UW"));
                                                quotationDocument.setDocument(document);
                                                lstQuotaDocuments.add(quotationDocument);
                                            }
                                        }
                                    }
                                    quotation.setQuotationDocuments(lstQuotaDocuments);
                                }

                                quotation.setFinancialProduct(getFinProduct());
                                quotation.setTiAppraisalEstimateAmount(0.0);
                                quotation.setTeAppraisalEstimateAmount(0.0);
                                quotation.setLeaseAmountPercentage(100.0);
                                quotation.setInterestRate(0.0);
                                quotation.setTerm(24);
                                quotation.setFrequency(EFrequency.M);


                                //Set default User
                            /*quotation.setCreateUser(this.getSecUser(7l).getDesc());
                            quotation.setUpdateUser(this.getSecUser(7l).getDesc());
                            quotation.setSecUser(this.getSecUser(7l));
                            quotation.setCreditOfficer(this.getSecUser(7l));*/

                                SecUser secUser = SECURITY_SRV.loadUserByUsername(httpHeaders.getHeaderString("user-name"));
                                SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(secUser, null, secUser.getAuthorities()));
                                quotation.setCreateUser(secUser.getDesc());
                                quotation.setUpdateUser(secUser.getDesc());
                                quotation.setCreditOfficer(secUser);
                                quotation.setSecUser(secUser);
                                //quotation.setWkfStatus(QuotationWkfStatus.QUO);
                                quotation.setVatValue(0);
                                quotation.setValid(true);
                            }

                            //Save or Update Quotation
                            //quotationService.saveOrUpdateQuotation(quotation);

                        } else {
                            // Response no result
                            return ResponseHelper.error(EResponseStatus.NOT_FOUND, "There is no quotation with this reference!", "");
                        }
                    }
                    //Response data back
                }
                //QuotationDTO dto = new QuotationDTO();
                if (quotation != null) {
                    File file = new File("/tmp/javadev");
                    if (!file.exists())
                        file.mkdirs();
                    file = new File("/tmp/javadev/" + reference + ".json");
                    FileOutputStream stream = new FileOutputStream(file);
                    ObjectOutputStream outputStream = new ObjectOutputStream(stream);
                    outputStream.writeObject(quotation);
                    outputStream.flush();
                    outputStream.close();
                    stream.close();
                    return Response.ok(file.getPath(), MediaType.TEXT_PLAIN).status(200).build();
                    //return ResponseHelper.error(EResponseStatus.OK, "success", "");
                } else
                    return ResponseHelper.error(EResponseStatus.BAD_REQUEST, "Qutation object is null", "");
            } else {
                //QuotationDTO dto = new QuotationDTO();
                return ResponseHelper.error(EResponseStatus.OK, "Please check mfinance webservice url", "");
            }
        } catch (EntityNotFoundException e) {
            String errMsg = e.getMessage();
            LOG.error(errMsg, e);
            throw new WsReponseException(EResponseStatus.NOT_FOUND, errMsg);
        } catch (Exception e) {
            String errMsg = I18N.messageUnexpectedException(e.getMessage());
            LOG.error(errMsg, e);
            throw new WsReponseException(EResponseStatus.GET_UNIQUE_KO, errMsg);
        }
    }

    /***/
    private Document getDocumentByCode(Long id, Long gid) {
        if (id == null || gid == null) return null;
        BaseRestrictions<Document> restrictions = new BaseRestrictions<>(Document.class);
        restrictions.addCriterion(Restrictions.eq("id", id));
        restrictions.addCriterion(Restrictions.eq("documentGroup.id", gid));
        List<Document> lstDocuments = ENTITY_SRV.list(restrictions);
        return lstDocuments.get(0);
    }

    /**
     * Check Quotation Already existing in MFP
     */
    private boolean checkExistingQuotation(String reference) {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<>(Quotation.class);
        restrictions.addCriterion(Restrictions.eq("reference", reference));
        List<Quotation> lstQuotations = ENTITY_SRV.list(restrictions);
        return lstQuotations.isEmpty() ? false : true;
    }

    /**
     * get Asset Model
     */
    private AssetModel getAssetModel(String code) {
        BaseRestrictions<AssetModel> restrictions = new BaseRestrictions<>(AssetModel.class);
        restrictions.addCriterion(Restrictions.eq("assetCode", code));
        List<AssetModel> lstAssetModel = ENTITY_SRV.list(restrictions);
        if (!lstAssetModel.isEmpty() || lstAssetModel != null) {
            return lstAssetModel.get(0);
        }
        return null;
    }

    private FinProduct getFinProduct() {
        BaseRestrictions<FinProduct> restrictions = new BaseRestrictions<>(FinProduct.class);
        restrictions.addCriterion(Restrictions.eq("id", 3l));
        List<FinProduct> lstFinProduct = ENTITY_SRV.list(restrictions);
        if (!lstFinProduct.isEmpty() || lstFinProduct != null) {
            return lstFinProduct.get(0);
        }
        return null;
    }

    /**
     * get SecUser
     */
    private SecUser getSecUser(Long id) {
        BaseRestrictions<SecUser> restrictions = new BaseRestrictions<>(SecUser.class);
        restrictions.addCriterion(Restrictions.eq("id", id));
        List<SecUser> lstSecUsers = ENTITY_SRV.list(restrictions);
        for (SecUser user : lstSecUsers) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    /**
     * get RefDataObject
     */
    private <T extends RefDataId> T getRefDataObject(Class<T> klass, String code) {
        if (klass != null && code != null && !code.isEmpty()) {
            Map<Long, T> tmp = (Map<Long, T>) ERefDataHelper.refData.get(klass.getName());
            for (Map.Entry<Long, T> entry : tmp.entrySet()) {
                if (entry.getValue().getCode().equals(code))
                    return entry.getValue();
            }
        }
        return null;
    }


    /**
     * get Marital Status
     */
    public EMaritalStatus getMaritialStatus(String maritalStatus) {
        if (("Married".compareToIgnoreCase(maritalStatus.trim())) == 0) {
            return EMaritalStatus.MARRIED;
        } else if (("single".compareToIgnoreCase(maritalStatus.trim())) == 0) {
            return EMaritalStatus.SINGLE;
        } else if (("widow.widower".compareToIgnoreCase(maritalStatus.trim())) == 0) {
            return EMaritalStatus.WIDOW;
        } else if (("divorced".compareToIgnoreCase(maritalStatus.trim())) == 0) {
            return EMaritalStatus.DIVORCED;
        } else if (("separated".compareToIgnoreCase(maritalStatus.trim())) == 0) {
            return EMaritalStatus.SEPARATED;
        } else if (("defacto".compareToIgnoreCase(maritalStatus.trim())) == 0) {
            return EMaritalStatus.DEFACTO;
        } else if (("unknown".compareToIgnoreCase(maritalStatus.trim())) == 0) {
            return EMaritalStatus.UNKNOWN;
        }
        return null;
    }

    /**
     * get EEngine Code
     */
    public String getEEngineCode(String engine) {
        String eEngineCode = null;
        if (("110".compareToIgnoreCase(engine.trim())) == 0) {
            eEngineCode = "001";
        } else if (("125".compareToIgnoreCase(engine.trim())) == 0) {
            eEngineCode = "003";
        } else if (("100".compareToIgnoreCase(engine.trim())) == 0) {
            eEngineCode = "082";
        } else if (("150".compareToIgnoreCase(engine.trim())) == 0) {
            eEngineCode = "033";
        }
        return eEngineCode;
    }

    /**
     * get EHousing Code
     */
    public String getEHousingCode(String code) {
        String eHousingCode = null;
        if (code.equals("01")) { //owner //config in ra
            //set owner object to individual
            eHousingCode = "owner";
        } else if (code.equals("02")) { //parent
            eHousingCode = "parent";
        } else if (code.equals("03")) { //rental
            eHousingCode = "rental";
        } else if (code.equals("04")) { //relative
            eHousingCode = "relative";
        } else if (code.equals("05")) { //other
            eHousingCode = "other";
        }
        return eHousingCode;
    }

    /**
     * get EColor Code
     */

    public String getEColorCode(String color) {
        String eColorCode = null;
        if (("Black".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "054";
        } else if (("White".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "001";
        } else if (("Red".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "095";
        } else if (("Yellow".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "183";
        } else if (("Blue".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "130";
        } else if (("Brown".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "149";
        } else if (("Pink".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "047";
        } else if (("white and red".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "016";
        } else if (("Black and Red".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "072";
        } else if (("Blue and White".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "132";
        } else if (("Dark Blue Mica".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "120";
        } else if (("Pink and White".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "048";
        } else if (("Red-Black".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "103";
        } else if (("Blue-White".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "132";
        } else if (("White-Black".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "009";
        } else if (("Black-Red".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "072";
        } else if (("Pink and White".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "048";
        } else if (("Black and Blue".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "080";
        } else if (("Grey".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "116";
        } else if (("GOLD".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "114";
        } else if (("Black Orange".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "089";
        } else if (("White Orange".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "030";
        } else if (("Green-Black".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "035";
        } else if (("Green-white".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "034";
        } else if (("Blue orange".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "147";
        } else if (("Red-White".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "097";
        } else if (("Blue and Black".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "138";
        } else if (("Green".compareToIgnoreCase(color.trim())) == 0) {
            eColorCode = "033";
        }
        return eColorCode;
    }


    /**
     * get EAssetYear
     */
    private EAssetYear getEAssetYear(int year) {
        switch (year) {
            case 2011:
                return EAssetYear.Y2011;
            case 2012:
                return EAssetYear.Y2012;
            case 2013:
                return EAssetYear.Y2013;
            case 2014:
                return EAssetYear.Y2014;
            case 2015:
                return EAssetYear.Y2015;
            case 2016:
                return EAssetYear.Y2016;
            case 2017:
                return EAssetYear.Y2017;
            default:
                return null;
        }
    }

}