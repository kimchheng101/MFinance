package com.soma.mfinance.ws.resource.model.response.address;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.soma.mfinance.ws.resource.model.response.StandardRes;

/**
 * Created by kanchanproseth on 6/3/17.
 */
public class DistrictDTO extends StandardRes{

    @JsonProperty("PROVINCE_ID")
    private Long province_id;

    public Long getProvince_id() {
        return province_id;
    }

    public void setProvince_id(Long province_id) {
        this.province_id = province_id;
    }
}
