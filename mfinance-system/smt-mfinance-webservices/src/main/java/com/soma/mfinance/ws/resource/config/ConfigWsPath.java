package com.soma.mfinance.ws.resource.config;

/**
 * @author kimsuor.seang
 */
public interface ConfigWsPath {
	
	final static String MINIMUM_INTERESTS = "/minimuminterests";
	final static String ASSET_CATEGORY_ID = "assetCategoryId";
	final static String TERM_ID = "termId";
	final static String BRAND_ID = "brandId";
	final static String MODEL_ID = "modelId";
	final static String DEALER_ID = "dealerId";
	final static String CONTRACT_NO = "contractNo";
	final static String TYPE = "type";
	final static String DATE = "date";

}
