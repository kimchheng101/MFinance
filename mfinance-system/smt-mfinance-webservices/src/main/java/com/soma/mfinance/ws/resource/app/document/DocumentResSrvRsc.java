package com.soma.mfinance.ws.resource.app.document;

import com.soma.common.messaging.ws.resource.cfg.refdata.AbstractRefDataSrvRsc;
import com.soma.mfinance.core.document.model.Document;
import com.soma.mfinance.core.document.model.DocumentGroup;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.ws.resource.model.response.document.DocumentDTO;
import com.soma.mfinance.ws.resource.model.response.document.GroupDocumentDTO;

/**
 * Created by kanchanproseth on 6/3/17.
 */
public class DocumentResSrvRsc extends AbstractRefDataSrvRsc  implements FinServicesHelper {

    /**
     * list Document
     * Convert to Document data transfer
     * @param document
     * @return
     */
    protected DocumentDTO toListDocumentDTO(Document document) {
        DocumentDTO listDocumentDTO = new DocumentDTO();
        listDocumentDTO.setId(document.getId());
        listDocumentDTO.setDesc(document.getDesc());
        listDocumentDTO.setDesc_en(document.getDescEn());
        listDocumentDTO.setUpdate_date(document.getUpdateDate().toString());
        listDocumentDTO.setCode(document.getCode());
        listDocumentDTO.setGroup_id(document.getDocumentGroup().getId());
        return listDocumentDTO;
    }

    /**
     * list Group Document
     * Convert to groupDocument data transfer
     * @param documentGroup
     * @return
     */
    protected GroupDocumentDTO toListGroupDocumentDTO(DocumentGroup documentGroup) {
        GroupDocumentDTO listGroupDocumentDTO = new GroupDocumentDTO();

        listGroupDocumentDTO.setId(documentGroup.getId());
        listGroupDocumentDTO.setDesc(documentGroup.getDesc());
        listGroupDocumentDTO.setDesc_en(documentGroup.getDesc());
        listGroupDocumentDTO.setUpdate_date(documentGroup.getUpdateDate().toString());
        listGroupDocumentDTO.setCode(documentGroup.getCode());
        return listGroupDocumentDTO;
    }


}
