package com.soma.mfinance.ws.resource.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by kanchanproseth on 6/3/17.
 */
public abstract class StandardRes implements Serializable{
    @JsonProperty("ID")
    private long id;

    @JsonProperty("DESC")
    private String desc;

    @JsonProperty("DESC_EN")
    private String desc_en;

    @JsonProperty("UPDATE_DATE")
    private String update_date;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDesc_en() {
        return desc_en;
    }

    public void setDesc_en(String desc_en) {
        this.desc_en = desc_en;
    }

    public String getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(String update_date) {
        this.update_date = update_date;
    }
}
