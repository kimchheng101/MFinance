package com.soma.mfinance.ws.resource.model.response.address;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.soma.mfinance.ws.resource.model.response.StandardRes;

/**
 * Created by kanchanproseth on 6/3/17.
 */
public class PropertyDTO extends StandardRes {

    @JsonProperty("CODE")
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


}
