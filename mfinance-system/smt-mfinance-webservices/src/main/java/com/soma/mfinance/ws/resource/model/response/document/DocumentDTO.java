package com.soma.mfinance.ws.resource.model.response.document;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.soma.mfinance.ws.resource.model.response.StandardRes;

/**
 * Created by kanchanproseth on 6/3/17.
 */
public class DocumentDTO extends StandardRes {

    @JsonProperty("CODE")
    private String code;

    @JsonProperty("GROUP_ID")
    private Long group_id;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getGroup_id() {
        return group_id;
    }

    public void setGroup_id(Long group_id) {
        this.group_id = group_id;
    }
}
