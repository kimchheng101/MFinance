package com.soma.mfinance.ws.resource.model.response.installment;


import java.io.Serializable;

/**
 * Created by p.leap on 13-Jun-17.
 */
public class GetInstallmentResponseMessage implements Serializable {
    private String response_msg;

    private int response_code;

    private String reference_number;
    private String session_id;
    private String customer_name;
    private double amount;
    private Currency currency;


    public String getReference_number() {
        return reference_number;
    }
    public void setReference_number(String reference_number) {
        this.reference_number = reference_number;
    }
    public String getSession_id() {
        return session_id;
    }
    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }
    public String getCustomer_name() {
        return customer_name;
    }
    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public Currency getCurrency() {
        return currency;
    }
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public double getAmount() {
        return amount;
    }
    public void setAmount(double amount) {
        this.amount = amount;
    }


    public String getResponse_msg() {
        return response_msg;
    }
    public void setResponse_msg(String response_msg) {
        this.response_msg = response_msg;
    }
    public int getResponse_code() {
        return response_code;
    }
    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }
}
