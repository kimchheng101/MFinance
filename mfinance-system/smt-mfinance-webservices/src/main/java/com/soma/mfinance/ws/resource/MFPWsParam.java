package com.soma.mfinance.ws.resource;

/**
 * Created by cheasocheat on 4/26/17.
 */
public interface MFPWsParam {
    String ID = "id";
    String REFERENCE = "reference";
}
