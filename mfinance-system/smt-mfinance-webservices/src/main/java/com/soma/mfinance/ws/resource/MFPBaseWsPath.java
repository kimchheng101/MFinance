package com.soma.mfinance.ws.resource;

import com.soma.common.messaging.ws.resource.BaseWsPath;

/**
 * Created by cheasocheat on 4/26/17.
 */
public interface MFPBaseWsPath extends BaseWsPath{
    String REFERENCE = "/{reference}";
    String LESSEE = "/lessee";
}
