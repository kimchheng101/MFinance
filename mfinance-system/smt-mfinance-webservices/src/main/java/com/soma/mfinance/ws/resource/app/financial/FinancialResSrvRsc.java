package com.soma.mfinance.ws.resource.app.financial;

import com.soma.common.messaging.ws.resource.cfg.refdata.AbstractRefDataSrvRsc;
import com.soma.mfinance.core.financial.model.Term;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.ws.resource.model.response.financial.FinancialDTO;
import com.soma.mfinance.ws.resource.model.response.financial.FrequencyDTO;
import com.soma.finance.services.shared.system.EFrequency;

/**
 * Created by v.hum on 6/5/2017.
 */
public class FinancialResSrvRsc extends AbstractRefDataSrvRsc implements FinServicesHelper {
    protected FinancialDTO toFinancialDTO(Term term) {
        FinancialDTO financialDTO =  new FinancialDTO();
        financialDTO.setId(term.getId());
        financialDTO.setDesc(term.getDesc());
        financialDTO.setDesc_en(term.getDescEn());
        financialDTO.setUpdate_date(term.getUpdateDate().toString());
        financialDTO.setCode(term.getCode());
        return financialDTO;
    }

    protected FrequencyDTO toFrequencyDTO(EFrequency freq) {
        FrequencyDTO frequencyDTO =  new FrequencyDTO();
        frequencyDTO.setId(freq.getId());
        frequencyDTO.setDesc(freq.getDesc());
        frequencyDTO.setDesc_en(freq.getDescEn());
        frequencyDTO.setUpdate_date("");
        frequencyDTO.setCode(freq.getCode());
        return frequencyDTO;
    }
}
