package com.soma.ersys.messaging.ws.resource;

import javax.ws.rs.Path;

import com.soma.ersys.core.hr.model.eref.ETypeOrganization;
import com.soma.ersys.core.hr.model.organization.OrganizationTypes;

/**
 * 
 * @author kimsuor.seang
 *
 */
@Path("/configs/{orgPath:organizations}")
public class BranchSrvRsc extends BaseBranchSrvRsc {
	private static final ETypeOrganization TYPE_ORGANIZATION = OrganizationTypes.MAIN;
	
	@Override
	public ETypeOrganization getTypeOrganization() {
		return TYPE_ORGANIZATION;
	}

}
