package com.soma.mfinance.ra.ui.panel.asset.assetmodelhp;

import com.soma.mfinance.core.asset.model.AssetModel;
import com.soma.mfinance.core.asset.model.AssetModelHp;
import com.soma.mfinance.core.asset.model.EEngine;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.FormLayout;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 
 * @author vi.sok
 * @since 18/03/2017
 *
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AssetModelHPFormPanel extends AbstractFormPanel {

	private static final long serialVersionUID = 5920777662405156757L;

	private AssetModelHp assetModelHp;
	private EntityRefComboBox<AssetModel> cbxAssetModel;
	private EntityRefComboBox<EEngine> cbxHp;
    
    @PostConstruct
	public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
		navigationPanel.addSaveClickListener(this);
	}
	
	@Override
	protected AssetModelHp getEntity() {
		assetModelHp.setAssetModel(cbxAssetModel.getSelectedEntity());
		assetModelHp.setHp(cbxHp.getSelectedEntity());
		return assetModelHp;
	}

	@Override
	protected com.vaadin.ui.Component createForm() {
		final FormLayout formPanel = new FormLayout();
		cbxAssetModel = new EntityRefComboBox<AssetModel>(I18N.message("asset.model"));
		cbxAssetModel.setRestrictions(new BaseRestrictions<AssetModel>(AssetModel.class));
		cbxAssetModel.setRequired(true);
		cbxAssetModel.renderer();
		cbxAssetModel.setSelectedEntity(null);
		cbxAssetModel.setImmediate(true);

		cbxHp = new EntityRefComboBox<EEngine>(I18N.message("hp"), EEngine.values());
		cbxHp.setRequired(true);
		cbxHp.setSelectedEntity(null);
        
        formPanel.addComponent(cbxAssetModel);
        formPanel.addComponent(cbxHp);
		return formPanel;
	}

	public void assignValues(Long id) {
		super.reset();
		if (id != null) {
			assetModelHp = ENTITY_SRV.getById(AssetModelHp.class, id);
			cbxAssetModel.setSelectedEntity(assetModelHp.getAssetModel());
			cbxHp.setSelectedEntity(assetModelHp.getHp());
		}
	}
	
	/**
	 * Reset
	 */
	@Override
	public void reset() {
		super.reset();
		assetModelHp = new AssetModelHp();
		cbxAssetModel.setSelectedEntity(null);
		cbxHp.setSelectedEntity(null);
		markAsDirty();
	}
	
	/**
	 * @return
	 */
	@Override
	protected boolean validate() {
		checkMandatorySelectField(cbxAssetModel, "asset.model");
		checkMandatorySelectField(cbxHp, "hp");
		return errors.isEmpty();
	}
}
