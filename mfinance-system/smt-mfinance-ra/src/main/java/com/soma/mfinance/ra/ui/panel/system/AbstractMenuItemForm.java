package com.soma.mfinance.ra.ui.panel.system;

import com.soma.common.app.menu.model.MenuEntity;
import com.soma.common.app.menu.model.MenuItemEntity;
import com.soma.mfinance.core.applicant.service.ValidateNumbers;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.*;
import org.hibernate.criterion.Order;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.Entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ki.kao on 2/20/2017.
 */
public abstract class AbstractMenuItemForm extends AbstractFormPanel {

    protected TextField txtId;
    protected TextField txtCode;
    protected TextField txtAction;
    protected TextField txtDesc;
    protected TextField txtSortIndex;
    protected CheckBox cbMainMenu;
    protected CheckBox cbSubMenu;
    protected CheckBox cbRead;
    protected CheckBox cbWrite;
    protected CheckBox cbExecute;
    protected EntityComboBox<MenuItemEntity> parentMenuComboBox;
    protected EntityComboBox<MenuEntity> appMenuComboBox;
    protected MenuItemEntity menuItemEntity = null;

    protected Component initializeComponents() {
        ValidateNumbers curentNumber = new ValidateNumbers();
        ArrayList arrayListNum = new ArrayList();

        txtId = ComponentFactory.getTextField("id", true, 60, 200);
        txtCode = ComponentFactory.getTextField("code", true, 60, 200);
        txtAction = ComponentFactory.getTextField("action", true, 60, 200);
        txtDesc = ComponentFactory.getTextField("desc", true, 60, 200);
        txtSortIndex = ComponentFactory.getTextField("index", true, 60, 200);
        arrayListNum.add(txtSortIndex);
        cbMainMenu = new CheckBox(I18N.message("is.main.menu"));
        cbMainMenu.setValue(false);
        cbSubMenu = new CheckBox(I18N.message("is.sub.menu"));
        cbSubMenu.setValue(true);
        appMenuComboBox = new EntityComboBox<MenuEntity>(MenuEntity.class, I18N.message("application.menu"), "code", "");
        appMenuComboBox.renderer();
        appMenuComboBox.setImmediate(true);
        appMenuComboBox.setRequired(true);
        parentMenuComboBox = new EntityComboBox<MenuItemEntity>(MenuItemEntity.class, I18N.message("parent.menu.code"), "code", "");

        cbRead = new CheckBox(I18N.message("read.code"));
        cbRead.setValue(true);
        cbRead.setRequired(true);
        cbWrite = new CheckBox(I18N.message("write.code"));
        cbExecute = new CheckBox(I18N.message("execute.code"));

        curentNumber.validateNumber(arrayListNum);
        return addComponents();
    }

    private Component addComponents() {
        FormLayout container = new FormLayout();
        GridLayout gridLayout = new GridLayout(2, 1);
        gridLayout.setSpacing(true);
        gridLayout.addComponent(cbMainMenu);
        gridLayout.addComponent(cbSubMenu);
        container.addComponent(txtCode);
        container.addComponent(txtAction);
        container.addComponent(txtDesc);
        container.addComponent(gridLayout);
        container.addComponent(appMenuComboBox);
        container.addComponent(parentMenuComboBox);
        container.addComponent(txtSortIndex);

        appMenuComboBox.addValueChangeListener(event -> {
            MenuEntity menuEntity = appMenuComboBox.getSelectedEntity();
            if (menuEntity != null)
                parentMenuComboBox.renderer(filterMenuItemByApplication(menuEntity.getId()));
            if (appMenuComboBox.getSelectedEntity() == null)
                parentMenuComboBox.removeAllItems();
        });

        cbMainMenu.addValueChangeListener(event -> {
            cbSubMenu.setValue(!cbMainMenu.getValue());
            if (cbMainMenu.getValue()) {
                parentMenuComboBox.setEnabled(false);
                parentMenuComboBox.setSelectedEntity(null);
            } else
                parentMenuComboBox.setEnabled(true);
        });

        cbSubMenu.addValueChangeListener(event -> {
            cbMainMenu.setValue(!cbSubMenu.getValue());
        });
        return container;
    }

    private List<MenuItemEntity> filterMenuItemByApplication(Long appId) {
        if (appId != null && appId > 0) {
            BaseRestrictions<MenuItemEntity> restrictions = new BaseRestrictions<>(MenuItemEntity.class);
            restrictions.addCriterion("menu.id", appId);
            restrictions.addOrder(Order.asc("id"));
            return ENTITY_SRV.list(restrictions);
        }
        return new ArrayList<MenuItemEntity>();
    }

    protected void clearErrorPanel() {
        super.reset();
    }

    @Override
    public void reset() {
        super.reset();
        menuItemEntity = null;
        txtId.setValue("");
        txtCode.setValue("");
        txtAction.setValue("");
        txtDesc.setValue("");
        txtSortIndex.setValue("");
        cbWrite.setValue(false);
        cbExecute.setValue(false);
        cbSubMenu.setValue(true);
        appMenuComboBox.setSelectedEntity(null);
        parentMenuComboBox.setSelectedEntity(null);
        markAsDirty();
    }

    @Override
    protected boolean validate() {
        checkMandatoryField(txtCode, "code");
        checkMandatoryField(txtAction, "action");
        checkMandatoryField(txtDesc, "desc");
        checkMandatoryField(txtSortIndex,"index");
        checkMandatorySelectField(appMenuComboBox, "application.menu");
        if (cbSubMenu.getValue()) {
            checkMandatorySelectField(parentMenuComboBox, "parent.menu.code");
            parentMenuComboBox.setRequired(true);
        } else
            parentMenuComboBox.setRequired(false);
        return errors.isEmpty();
    }

    @Override
    protected Entity getEntity() {
        if (menuItemEntity == null)
            menuItemEntity = new MenuItemEntity();
        if (txtId.getValue() != null && !txtId.getValue().equals(""))
            menuItemEntity.setId(new Long(txtId.getValue()));
        menuItemEntity.setCode(txtCode.getValue());
        menuItemEntity.setAction(txtAction.getValue());
        menuItemEntity.setDesc(txtDesc.getValue());
        menuItemEntity.setSortIndex(Integer.valueOf(txtSortIndex.getValue()));
        if (cbSubMenu.getValue() && parentMenuComboBox.getSelectedEntity() != null && parentMenuComboBox.getSelectedEntity().getId() != null)
            menuItemEntity.setParent(parentMenuComboBox.getSelectedEntity());
        else
            menuItemEntity.setParent(null);
        if (appMenuComboBox.getSelectedEntity() != null && appMenuComboBox.getSelectedEntity().getId() != null)
            menuItemEntity.setMenu(appMenuComboBox.getSelectedEntity());
        return menuItemEntity;
    }

}
