package com.soma.mfinance.ra.ui.panel.asset.assetmodelhp;

import com.soma.mfinance.core.asset.model.AssetModel;
import com.soma.mfinance.core.asset.model.AssetModelHp;
import com.soma.mfinance.core.asset.model.EEngine;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author vi.sok
 * @since 18/03/2017
 *
 */
public class AssetModelHPSearchPanel extends AbstractSearchPanel<AssetModelHp> implements FMEntityField {

	private static final long serialVersionUID = 9136565522330994370L;
	
	private EntityRefComboBox<AssetModel> cbxAssetModel;
	private EntityRefComboBox<EEngine> cbxHp;
	
	public AssetModelHPSearchPanel(AssetModelHPTablePanel assetModelHPTablePanel) {
		super(I18N.message("search"), assetModelHPTablePanel);
	}
	
	@Override
	protected void reset() {
		cbxAssetModel.setSelectedEntity(null);
		cbxHp.setSelectedEntity(null);
	}


	@Override
	protected Component createForm() {
		final GridLayout gridLayout = new GridLayout(4, 1);
		cbxAssetModel = new EntityRefComboBox<AssetModel>(I18N.message("asset.model"));
		cbxAssetModel.setRestrictions(new BaseRestrictions<AssetModel>(AssetModel.class));
		cbxAssetModel.renderer();
		cbxAssetModel.setSelectedEntity(null);
		cbxAssetModel.setImmediate(true);

		cbxHp = new EntityRefComboBox<EEngine>(I18N.message("hp"));
        gridLayout.setSpacing(true);
        gridLayout.addComponent(new FormLayout(cbxAssetModel), 1, 0);
        gridLayout.addComponent(new FormLayout(cbxHp), 2, 0);
        
		return gridLayout;
	}
	
	@Override
	public BaseRestrictions<AssetModelHp> getRestrictions() {
		BaseRestrictions<AssetModelHp> restrictions = new BaseRestrictions<AssetModelHp>(AssetModelHp.class);
		List<Criterion> criterions = new ArrayList<Criterion>();
		if (cbxAssetModel.getSelectedEntity() != null) { 
			criterions.add(Restrictions.eq("assetModel.id", cbxAssetModel.getSelectedEntity().getId()));
		}	
		if (cbxHp.getSelectedEntity() != null) { 
			criterions.add(Restrictions.eq("hp.id", cbxHp.getSelectedEntity().getId()));
		}	
		restrictions.setCriterions(criterions);
		restrictions.addOrder(Order.desc(ID));
		return restrictions;
	}

}
