package com.soma.mfinance.ra.ui.panel.dealer.mapping;

import com.soma.mfinance.core.dealer.model.DealerMapAddress;
import com.soma.mfinance.core.shared.dealer.DealerEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author by kimsuor.seang  on 11/11/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DealerMappingTablePanel extends AbstractTablePanel<DealerMapAddress> implements DealerEntityField {
    private static final long serialVersionUID = -2983055467054135680L;

    @PostConstruct
    public void PostConstruct() {
        setCaption(I18N.message("dealers.mapping"));
        setSizeFull();
        setMargin(true);
        setSpacing(true);

        super.init(I18N.message("dealers.mapping"));

        addDefaultNavigation();
    }

    @Override
    protected PagedDataProvider<DealerMapAddress> createPagedDataProvider() {
        PagedDefinition<DealerMapAddress> pagedDefinition = new PagedDefinition<>(searchPanel.getRestrictions());
        pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition(DEALER + "." + NAME_EN, I18N.message("dealer"), String.class, Align.CENTER, 300);
        pagedDefinition.addColumnDefinition(PROVINCE + "." + DESC_EN, I18N.message("province"), String.class, Align.CENTER, 200);
        pagedDefinition.addColumnDefinition("district" + "." + DESC_EN, I18N.message("district"), String.class, Align.CENTER, 200);
        pagedDefinition.addColumnDefinition("commune" + "." + DESC_EN, I18N.message("commue"), String.class, Align.CENTER, 200);
        //pagedDefinition.addColumnDefinition(VILLAGE+"."+DESC_EN, I18N.message("village"), String.class, Align.CENTER, 200);
        EntityPagedDataProvider<DealerMapAddress> pagedDataProvider = new EntityPagedDataProvider<>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    @Override
    protected DealerMapAddress getEntity() {
        final Long id = getItemSelectedId();
        if (id != null) {
            return ENTITY_SRV.getById(DealerMapAddress.class, id);
        }
        return null;
    }

    @Override
    protected void deleteEntity(Entity entity) {
        ENTITY_SRV.changeStatusRecord((DealerMapAddress) entity, EStatusRecord.RECYC);
    }

    @Override
    protected DealerMappingSearchPanel createSearchPanel() {
        return new DealerMappingSearchPanel(this);
    }
}
