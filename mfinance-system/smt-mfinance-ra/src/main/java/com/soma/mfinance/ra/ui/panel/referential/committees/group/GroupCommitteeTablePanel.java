package com.soma.mfinance.ra.ui.panel.referential.committees.group;

import com.soma.mfinance.core.referential.committee.GroupCommittee;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author by kimsuor.seang  on 9/27/2017.
 */

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class GroupCommitteeTablePanel extends AbstractTablePanel<GroupCommittee> implements FMEntityField  {

	@PostConstruct
	public void PostConstruct() {
		setCaption(I18N.message("group.committee.panel"));
		setSizeFull();
		setMargin(true);
		setSpacing(true);

		super.init(I18N.message("group.committee.panel"));

		NavigationPanel navigationPanel = addNavigationPanel();
		navigationPanel.addAddClickListener(this);
		navigationPanel.addEditClickListener(this);
		navigationPanel.addDeleteClickListener(this);
		navigationPanel.addRefreshClickListener(this);
	}

	@Override
	protected PagedDataProvider<GroupCommittee> createPagedDataProvider() {
		PagedDefinition<GroupCommittee> pagedDefinition = new PagedDefinition<>(searchPanel.getRestrictions());
		pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("secUser." + DESC, I18N.message("user.name").toUpperCase(), String.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("secUser." + LOG_IN, I18N.message("login"), String.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("productLine." + DESC, I18N.message("product.line"), String.class, Align.LEFT, 100);

		EntityPagedDataProvider<GroupCommittee> pagedDataProvider = new EntityPagedDataProvider<>();
		pagedDataProvider.setPagedDefinition(pagedDefinition);
		return pagedDataProvider;
	}

	@Override
	protected GroupCommittee getEntity() {
		final Long id = getItemSelectedId();
		if (id != null) {
			return ENTITY_SRV.getById(GroupCommittee.class, id);
		}
		return null;
	}


	@Override
	protected GroupCommitteeSearchPanel createSearchPanel() {
		return new GroupCommitteeSearchPanel(this);
	}

}
