package com.soma.mfinance.ra.ui.panel.referential.whosejob;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * @author by kimsuor.seang  on 10/14/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(WhoseJobPanel.NAME)
public class WhoseJobPanel extends AbstractTabsheetPanel implements View {

	private static final long serialVersionUID = 2762740409276658002L;

	public static final String NAME = "whose.job";
	
	@Autowired
	private WhoseJobTablePanel whoseJobTablePanel;
	@Autowired
	private WhoseJobFormPanel whoseJobFormPanel;
	
	@PostConstruct
	public void PostConstruct() {
		super.init();
		whoseJobTablePanel.setMainPanel(this);
		whoseJobFormPanel.setCaption(I18N.message("applicant.type"));
		getTabSheet().setTablePanel(whoseJobTablePanel);
	}

	@Override
	public void enter(ViewChangeEvent event) {
	}
	
	
	@Override
	public void onAddEventClick() {
		whoseJobFormPanel.reset();
		getTabSheet().addFormPanel(whoseJobFormPanel);
		getTabSheet().setSelectedTab(whoseJobFormPanel);
	}

	@Override
	public void onEditEventClick() {
		getTabSheet().addFormPanel(whoseJobFormPanel);
		initSelectedTab(whoseJobFormPanel);
	}
	
	@Override
	public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
		if (selectedTab == whoseJobFormPanel) {
			whoseJobFormPanel.assignValues(whoseJobTablePanel.getItemSelectedId());
		} else if (selectedTab == whoseJobTablePanel && getTabSheet().isNeedRefresh()) {
			whoseJobTablePanel.refresh();
		}
		getTabSheet().setSelectedTab(selectedTab);
	}
}
