package com.soma.mfinance.ra.ui.panel.referential.kubota4plus.assethistoryitem;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * @author by kimsuor.seang  on 10/9/2017.
 */

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(AssetHistoryItemPanel.NAME)
public class AssetHistoryItemPanel extends AbstractTabsheetPanel implements View  {

    public static final String NAME = "asset.history.item";

    @Autowired
    private AssetHistoryItemTablePanel assetHistoryItemTablePanel;
    @Autowired
    private AssetHistoryItemFormPanel assetHistoryItemFormPanel;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        assetHistoryItemTablePanel.setMainPanel(this);
        assetHistoryItemFormPanel.setCaption(I18N.message("asset.history.item"));
        getTabSheet().setTablePanel(assetHistoryItemTablePanel);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }


    @Override
    public void onAddEventClick() {
        assetHistoryItemFormPanel.reset();
        getTabSheet().addFormPanel(assetHistoryItemFormPanel);
        getTabSheet().setSelectedTab(assetHistoryItemFormPanel);
    }

    @Override
    public void onEditEventClick() {
        assetHistoryItemFormPanel.reset();
        getTabSheet().addFormPanel(assetHistoryItemFormPanel);
        initSelectedTab(assetHistoryItemFormPanel);
    }

    @Override
    public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
        if (selectedTab == assetHistoryItemFormPanel) {
            assetHistoryItemFormPanel.assignValues(assetHistoryItemTablePanel.getItemSelectedId());
        } else if (selectedTab == assetHistoryItemTablePanel && getTabSheet().isNeedRefresh()) {
            assetHistoryItemTablePanel.refresh();
        }
        getTabSheet().setSelectedTab(selectedTab);
    }
}
