package com.soma.mfinance.ra.ui.panel.system;

import com.soma.common.app.menu.model.MenuItemEntity;
import com.soma.common.app.tools.helper.AppServicesHelper;
import com.soma.mfinance.core.shared.system.CustomProfilePrivilege;
import com.soma.frmk.security.model.*;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Created by ki.kao on 2/15/2017.
 */
@Service
class MenuItemServiceUtil implements AppServicesHelper {

    public void saveOrUpdateMenuItem(MenuItemEntity entity, String previousMenu) throws Exception {
        if (entity != null) {
            SecControl secControl = null;
            SecControl group = ENTITY_SRV.getByCode(SecControl.class, entity.getMenu().getCode());
            secControl = getSecControl(entity, group);
            ENTITY_SRV.saveOrUpdate(secControl);
            if (secControl != null && secControl.getId() != null) {
                entity.setPopupWindow(false);
                entity.setControl(secControl);
                ENTITY_SRV.saveOrUpdate(entity);
                if (previousMenu != null && !previousMenu.equals(entity.getMenu().getCode()))
                    updateSubMenu(entity, secControl);
                saveControlPrivilege(secControl);
            }
        }
    }

    public void deleteSubMenu(MenuItemEntity entity, SecControl secControl) {
        if (entity.getParent() == null && secControl.getParent() == null) {
            BaseRestrictions<MenuItemEntity> restrictions = new BaseRestrictions<>(MenuItemEntity.class);
            restrictions.addCriterion(Restrictions.eq("parent.id", entity.getId()));
            for (MenuItemEntity obj : ENTITY_SRV.list(restrictions)) {
                BaseRestrictions<SecControlProfilePrivilege> restrictions1 = new BaseRestrictions<>(SecControlProfilePrivilege.class);
                restrictions1.addCriterion(Restrictions.eq("control.id", obj.getControl().getId()));
                for (SecControlProfilePrivilege sec1 : ENTITY_SRV.list(restrictions1))
                    ENTITY_SRV.delete(sec1);
                BaseRestrictions<SecControlPrivilege> restrictions2 = new BaseRestrictions<>(SecControlPrivilege.class);
                restrictions2.addCriterion(Restrictions.eq("control.id", obj.getControl().getId()));
                for (SecControlPrivilege sec2 : ENTITY_SRV.list(restrictions2))
                    ENTITY_SRV.delete(sec2);
                ENTITY_SRV.delete(obj);
                ENTITY_SRV.delete(obj.getControl());
            }

        }
    }

    public void clearSession() {
        if (ENTITY_SRV.getSessionFactory().getCurrentSession() != null)
            ENTITY_SRV.getSessionFactory().getCurrentSession().clear();
    }

    private SecControl getSecControl(MenuItemEntity entity, SecControl group) {
        SecControl secControl = null;
        if (entity.getId() != null) {
            secControl = ENTITY_SRV.getById(SecControl.class, entity.getControl().getId());
        } else
            secControl = new SecControl();
        secControl.setCode(entity.getCode());
        secControl.setDesc(entity.getDesc());
        secControl.setType(ESecControlType.MENU);
        secControl.setApplication(entity.getMenu().getApplication());
        secControl.setGroup(group);
        if (entity.getParent() != null) {
            secControl.setParent(entity.getParent().getControl());
        } else
            secControl.setParent(null);
        return secControl;
    }

    private void updateSubMenu(MenuItemEntity entity, SecControl secControl) {
        if (entity.getParent() == null && secControl.getParent() == null) {
            BaseRestrictions<MenuItemEntity> restrictions = new BaseRestrictions<>(MenuItemEntity.class);
            restrictions.addCriterion(Restrictions.eq("parent.id", entity.getId()));
            List<MenuItemEntity> menuItemEntityList = ENTITY_SRV.list(restrictions);
            for (MenuItemEntity obj : menuItemEntityList) {
                obj.setParent(entity);
                ENTITY_SRV.saveOrUpdate(obj);
                obj.getControl().setParent(secControl);
                ENTITY_SRV.saveOrUpdate(obj.getControl());
            }

        }
    }

    private void saveControlPrivilege(SecControl secControl) {
        List<ESecPrivilege> list = new ArrayList<>(Arrays.asList(new ESecPrivilege[]{ESecPrivilege.READ, ESecPrivilege.WRITE, ESecPrivilege.EXECUTE}));
        for (ESecPrivilege privilege : list) {
            SecControlPrivilege secControlPrivilege = new SecControlPrivilege();
            secControlPrivilege.setPrivilege(privilege);
            secControlPrivilege.setControl(secControl);
            ENTITY_SRV.saveOrUpdate(secControlPrivilege);
            SecControlProfilePrivilege secControlProfilePrivilege=new SecControlProfilePrivilege();
            secControlProfilePrivilege.setProfile(SecProfile.ADMIN);
            secControlProfilePrivilege.setPrivilege(privilege);
            secControlProfilePrivilege.setControl(secControl);
            ENTITY_SRV.saveOrUpdate(secControlProfilePrivilege );
        }
    }

}
