package com.soma.mfinance.ra.ui.panel.system;

import com.soma.common.app.menu.model.MenuEntity;
import com.soma.common.app.menu.model.MenuItemEntity;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.security.model.SecApplication;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.*;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;

/**
 * Created by ki.kao on 2/13/2017.
 */
public class MenuItemSearchPanel extends AbstractSearchPanel<MenuItemEntity> implements FMEntityField {

    private TextField txtId;
    private TextField txtCode;
    private TextField txtAction;
    private EntityComboBox<MenuEntity> menuCombox;

    public MenuItemSearchPanel(String caption, AbstractTablePanel<MenuItemEntity> tablePanel) {
        super(caption, tablePanel);
    }

    @Override
    protected void reset() {
        txtId.setValue("");
        txtCode.setValue("");
        txtAction.setValue("");
        menuCombox.setSelectedEntity(null);
    }

    @Override
    protected Component createForm() {
        final GridLayout gridLayout = new GridLayout(4, 1);

        txtId = ComponentFactory.getTextField("id", false, 60, 200);
        txtCode = ComponentFactory.getTextField("code", false, 60, 200);
        txtAction = ComponentFactory.getTextField("action", false, 60, 200);

        menuCombox = new EntityComboBox<MenuEntity>(MenuEntity.class, I18N.message("application.menu"), "code", "");
        menuCombox.renderer();

        gridLayout.setSpacing(true);
        gridLayout.addComponent(new FormLayout(txtId), 0, 0);
        gridLayout.addComponent(new FormLayout(txtCode), 1, 0);
        gridLayout.addComponent(new FormLayout(txtAction), 2, 0);
        gridLayout.addComponent(new FormLayout(menuCombox), 3, 0);

        return gridLayout;
    }

    @Override
    public BaseRestrictions<MenuItemEntity> getRestrictions() {
        BaseRestrictions<MenuItemEntity> restrictions = new BaseRestrictions<>(MenuItemEntity.class);

        if (StringUtils.isNotEmpty(txtId.getValue())) {
            restrictions.addCriterion(Restrictions.eq(ID, new Long(txtId.getValue())));
        }
        if (StringUtils.isNotEmpty(txtCode.getValue())) {
            restrictions.addCriterion(Restrictions.ilike(CODE, txtCode.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtAction.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("action", txtAction.getValue(), MatchMode.ANYWHERE));
        }
        if (menuCombox.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq("menu.id", menuCombox.getSelectedEntity().getId()));
        }

        return restrictions;
    }
}
