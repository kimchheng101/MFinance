package com.soma.mfinance.ra.ui.panel.referential.committees.committee;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * Created by Dang Dim
 * Date     : 19-Oct-17, 11:03 AM
 * Email    : d.dim@gl-f.com
 */

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(CommitteePanel.NAME)
public class CommitteePanel extends AbstractTabsheetPanel implements View {

    public static final String NAME = "committees";

    @Autowired
    CommitteeFormPanel committeeFormPanel;

    @Autowired
    CommitteeTablePanel committeeTablePanel;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        committeeTablePanel.setMainPanel(this);
        committeeFormPanel.setCaption(I18N.message("credit.committee"));
        getTabSheet().setTablePanel(committeeTablePanel);
    }

    @Override
    public void onAddEventClick() {
        getTabSheet().addFormPanel(committeeFormPanel);
        getTabSheet().setSelectedTab(committeeFormPanel);
        committeeFormPanel.reset();
    }

    @Override
    public void onEditEventClick() {
        getTabSheet().addFormPanel(committeeFormPanel);
        initSelectedTab(committeeFormPanel);
    }

    @Override
    public void initSelectedTab(com.vaadin.ui.Component selectedTab) {

        if (selectedTab == committeeFormPanel){
            committeeFormPanel.assignValue(committeeTablePanel.getItemSelectedId());
        }else if(selectedTab == committeeTablePanel && getTabSheet().isNeedRefresh()){
            committeeTablePanel.refresh();
        }

        getTabSheet().setSelectedTab(selectedTab);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}
