package com.soma.mfinance.ra.ui.panel.referential.businesstypes;

import com.soma.mfinance.core.shared.system.BusinessTypes;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author by kimsuor.seang  on 10/18/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BusinessTypesFormPanel extends AbstractFormPanel {
	
	private static final long serialVersionUID = 6284433757461762740L;

	private BusinessTypes businessTypes;
	private TextField txtDesc;
    private TextField txtDescEn;
    
    @PostConstruct
	public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
		navigationPanel.addSaveClickListener(this);
	}
	
	@Override
	protected Entity getEntity() {
		businessTypes.setDesc(txtDesc.getValue());
		businessTypes.setDescEn(txtDescEn.getValue());
		return businessTypes;
	}
	

	@Override
	protected com.vaadin.ui.Component createForm() {
		final FormLayout formPanel = new FormLayout();
		txtDescEn = ComponentFactory.getTextField("desc.en", true, 60, 200);
		txtDesc = ComponentFactory.getTextField35("desc", false, 60, 200);
        
        formPanel.addComponent(txtDescEn);
        formPanel.addComponent(txtDesc);
		return formPanel;
	}
	
	public void assignValues(Long busTypId) {
		super.reset();
		if (busTypId != null) {
			businessTypes = ENTITY_SRV.getById(BusinessTypes.class, busTypId);
			txtDescEn.setValue(businessTypes.getDescEn());
			txtDesc.setValue(businessTypes.getDesc());
		}
	}

	@Override
	public void reset() {
		super.reset();
		businessTypes = new BusinessTypes();
		txtDescEn.setValue("");
		txtDesc.setValue("");
		markAsDirty();
	}

	@Override
	protected boolean validate() {
		checkMandatoryField(txtDescEn, "desc.en");
		return errors.isEmpty();
	}
}
