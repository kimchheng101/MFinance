
package com.soma.mfinance.ra.ui.panel.finproduct.service.migrationvo.impl;

import com.soma.frmk.helper.SeuksaServicesHelper;
import com.soma.mfinance.core.shared.conf.AppConfig;
import com.soma.mfinance.core.shared.contract.ContractEntityField;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.mfinance.ra.ui.panel.finproduct.service.migrationvo.ImportContractServiceClient;
import com.soma.mfinance.third.einsurance.service.ContractMigrationService;
import org.seuksa.frmk.dao.BaseEntityDao;
import org.seuksa.frmk.service.impl.BaseEntityServiceImpl;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Create by Rith RON.
 * User r.ron.
 * Date on 2017-05-22.
 * Time at 11:04 AM.
 * Email r.ron@gl-f.com.
 */

@Service("ImportContractServiceClient")
@Transactional
public class ImportContractServiceClientImpl extends BaseEntityServiceImpl implements ImportContractServiceClient, ContractEntityField, QuotationEntityField, SeuksaServicesHelper {
    private static final int OK = 0;
    private static final int KO = 1;
    private static final String CPMI_START_DATE = "01-07-2015";


    @Override
    public String migrateQuotation(Set<Long> selectedItemIds) {
        System.out.println("============================================");
        System.out.println("========    Migrate Quotation      =========");
        System.out.println("============================================");
        String msg;
        Set<String> responseMessages = new HashSet<>();
        ContractMigrationService contractMigrationService = SpringUtils.getBean(ContractMigrationService.class);
        responseMessages = contractMigrationService.migrationToEinsurance(selectedItemIds);
        writeMessageToFile(responseMessages);
        msg = generateMessages(responseMessages);
        return msg;
    }


    private String generateMessages(Set<String> responseMessages) {
        System.out.println(responseMessages);
        StringBuffer msg = new StringBuffer();
        msg.append("<table><tbody>");
        responseMessages.forEach(responseMessage -> {
            msg.append("<tr><td>");
            msg.append(responseMessage);
            msg.append("</td></tr>");
        });
        msg.append("</tbody></table>");
        return msg.toString();
    }

    private String[] emailSendTo;

    @Override
    public void excecuteJob() {

    }

    @Override
    public List<String> getLogFileNames() {
        List<String> fileNames = new ArrayList<String>();
        List<File> files = new ArrayList<File>();
        String documentDir = AppConfig.getInstance().getConfiguration().getString("document.path");
        File dir = new File(documentDir + "/" + CONTRACT_MIGRATION_DIR);

        if (dir.isDirectory()) {
            File[] aFiles = dir.listFiles(new FilenameFilter() {
                /**
                 * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
                 */
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".txt");
                }
            });
            Arrays.sort(aFiles);
            files = Arrays.asList(aFiles);
        }
        Collections.reverse(files);
        for (File file : files) {
            fileNames.add(file.getName());
        }
        return fileNames;
    }

    @Override
    public int getNbMonthOfTwoDates(Date date1, Date date2) {
        return 0;
    }

    @Override
    public void generateInsuranceStartDate() {

    }

    @Override
    public int getRenewalTerm(Date insuranceStartDate, Date contractStartDate, Integer term) {
        return 0;
    }

    @Override
    public BaseEntityDao getDao() {
        return null;
    }

/*
    public AssetVO getAssetVO(Quotation quotation) {
        AssetVO assetVO = new AssetVO();
        Asset asset = quotation.getAsset();

        AssetModel assetModel = getById(AssetModel.class, asset.getModel());
        if (assetModel != null) {
            assetVO.setAssetModelCode(assetModel.getCode());
        }
        if (asset.getColor() != null) {
            // In E-Finance, Color has no code property. Then use id instead of Code
            assetVO.setAssetColorCode(asset.getColor().getId().toString());
        }
        if (asset.getEngine() != null) {
            // In E-Finance, Engine has no code property. Then use id instead of Code
            assetVO.setAssetEngineCode(asset.getEngine().getId().toString());
        }
        assetVO.setTiCashPriceUsd(asset.getTiAssetPrice());
        assetVO.setTeCashPriceUsd(asset.getTeAssetPrice());
        assetVO.setVatCashPriceUsd(asset.getVatAssetPrice());
        assetVO.setYear(asset.getYear());
        if (asset.getAssetGender() != null) {
            assetVO.setAssetGender(asset.getAssetGender().getName());
        }

        if (asset.getRegistrationDate() != null) {
            assetVO.setRegistrationDate(asset.getRegistrationDate());
        } else {
            assetVO.setRegistrationDate(asset.getRegistrationDate());
        }
        assetVO.setPlateNumber(asset.getPlateNumber());
        assetVO.setChassisNumber(asset.getChassisNumber());
        assetVO.setEngineNumber(asset.getEngineNumber());

        assetVO.setCode(asset.getCode());
        assetVO.setDesc(asset.getDesc());
        assetVO.setDescEn(asset.getDescEn());
        return assetVO;
    }*/


    private void writeMessageToFile(Set<String> responseMessages) {
        try {
            DateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");
            String fileName = df.format(DateUtils.today()) + ".txt";
            String documentDir = AppConfig.getInstance().getConfiguration().getString("document.path") + "/" + CONTRACT_MIGRATION_DIR;

            File tmpDir = new File(documentDir);
            if (!tmpDir.exists()) {
                tmpDir.mkdirs();
            }
            File file = new File(documentDir + "/" + fileName);
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write("------------------------------Error Log---------------------------------");
            bufferedWriter.newLine();
            responseMessages.forEach(responseMessage -> {
                try {
                    bufferedWriter.write(responseMessage.toString());
                    bufferedWriter.newLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            bufferedWriter.write("----------------------------------End-----------------------------------");
            bufferedWriter.close();
        } catch (IOException e) {
            logger.error("Error write contract migration log file", e);
        }
    }

}