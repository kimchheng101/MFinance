package com.soma.mfinance.ra.ui.panel.system.tab;

import com.soma.common.app.tools.UserSessionManager;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.mfinance.core.tab.model.TabItemEntity;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.statusrecord.StatusRecordField;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by k.seang on 3/3/2017.
 */
public class TabItemSearchPanel extends AbstractSearchPanel<TabItemEntity> implements FMEntityField {


    private StatusRecordField statusRecordField;
    private TextField txtCode;
    private TextField txtDesc;

    public TabItemSearchPanel(TabItemTablePanel tabItemTablePanel) {
        super(I18N.message("search.criteria"), tabItemTablePanel);
    }
    @Override
    protected void reset() {
        this.txtCode.setValue(StringUtils.EMPTY);
        this.txtDesc.setValue(StringUtils.EMPTY);
        this.statusRecordField.clearValues();
    }

    @Override
    protected Component createForm() {
        GridLayout gridLayout = new GridLayout(3, 1);
        gridLayout.setWidth(100.0F, Unit.PERCENTAGE);
        this.txtCode = ComponentFactory.getTextField("code", false, 60, 200.0F);
        this.txtDesc = ComponentFactory.getTextField("desc.en", false, 60, 200.0F);
        this.statusRecordField = new StatusRecordField();
        gridLayout.setSpacing(true);
        gridLayout.addComponent(new FormLayout(new Component[]{this.txtCode}));
        gridLayout.addComponent(new FormLayout(new Component[]{this.txtDesc}));
        gridLayout.addComponent(new FormLayout(new Component[]{this.statusRecordField}));
        return gridLayout;
    }

    @Override
    public BaseRestrictions<TabItemEntity> getRestrictions() {
        BaseRestrictions restrictions = new BaseRestrictions(TabItemEntity.class);
        SecUser user = UserSessionManager.getCurrentUser();
        List<Criterion> criterions = new ArrayList<Criterion>();
        if(!user.isSysAdmin()) {
            if (StringUtils.isNotEmpty(txtCode.getValue())) {
                criterions.add(Restrictions.ilike(CODE, txtCode.getValue(), MatchMode.ANYWHERE));
            }
            if (StringUtils.isNotEmpty(txtDesc.getValue())) {
                criterions.add(Restrictions.ilike(DESC, txtDesc.getValue(), MatchMode.ANYWHERE));
            }

            if(!this.statusRecordField.getActiveValue().booleanValue() && !this.statusRecordField.getInactiveValue().booleanValue()) {
                restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
                restrictions.getStatusRecordList().add(EStatusRecord.INACT);
            }

            if(this.statusRecordField.getActiveValue().booleanValue()) {
                restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
            }

            if(this.statusRecordField.getInactiveValue().booleanValue()) {
                restrictions.getStatusRecordList().add(EStatusRecord.INACT);
            }

            restrictions.setCriterions(criterions);
            restrictions.addOrder(Order.asc(DESC));
        }

        return restrictions;
    }
}
