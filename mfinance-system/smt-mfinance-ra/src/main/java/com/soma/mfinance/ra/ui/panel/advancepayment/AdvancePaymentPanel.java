package com.soma.mfinance.ra.ui.panel.advancepayment;

import com.soma.mfinance.ra.ui.panel.interestrate.InterestRateFormPanel;
import com.soma.mfinance.ra.ui.panel.interestrate.InterestRatePanel;
import com.soma.mfinance.ra.ui.panel.interestrate.InterestRateTablePanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * Created by b.chea on 2/22/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(AdvancePaymentPanel.NAME)
public class AdvancePaymentPanel extends AbstractTabsheetPanel implements View {

    public static final String NAME = "advance.payment";

    @Autowired
    private AdvancePaymentTablePanel advancePaymentTablePanel;
    @Autowired
    private AdvancePaymentFormPanel advancePaymentFormPanel;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        advancePaymentTablePanel.setMainPanel(this);
        advancePaymentFormPanel.setCaption(I18N.message("asset.engine.status"));
        getTabSheet().setTablePanel(advancePaymentTablePanel);
    }

    /**
     * @see com.vaadin.navigator.View#enter(com.vaadin.navigator.ViewChangeListener.ViewChangeEvent)
     */
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel#onAddEventClick()
     */
    @Override
    public void onAddEventClick() {
        advancePaymentFormPanel.reset();
        getTabSheet().addFormPanel(advancePaymentFormPanel);
        getTabSheet().setSelectedTab(advancePaymentFormPanel);
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel#onEditEventClick()
     */
    @Override
    public void onEditEventClick() {
        getTabSheet().addFormPanel(advancePaymentFormPanel);
        initSelectedTab(advancePaymentFormPanel);
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel#initSelectedTab(com.vaadin.ui.Component)
     */
    @Override
    public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
        if (selectedTab == advancePaymentFormPanel) {
            advancePaymentFormPanel.assignValues(advancePaymentTablePanel.getItemSelectedId());
        } else if (selectedTab == advancePaymentTablePanel && getTabSheet().isNeedRefresh()) {
            advancePaymentTablePanel.refresh();
        }
        getTabSheet().setSelectedTab(selectedTab);
    }
}
