package com.soma.mfinance.ra.ui.panel.contactevidence;

import com.soma.mfinance.core.model.system.ContactVerification;
import com.soma.mfinance.core.shared.asset.AssetEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ContactVerificationTablePanel extends AbstractTablePanel<ContactVerification> implements AssetEntityField {
	
	private static final long serialVersionUID = -7185135733682265837L;
	
	@PostConstruct
	public void PostConstruct() {
		setCaption(I18N.message("contact.verifications"));
		setSizeFull();
		setMargin(true);
		setSpacing(true);
				
		super.init(I18N.message("contact.verifications"));
		
		addDefaultNavigation();
	}	
	/*return*/
	@Override
	protected PagedDataProvider<ContactVerification> createPagedDataProvider() {
		PagedDefinition<ContactVerification> pagedDefinition = new PagedDefinition<ContactVerification>(searchPanel.getRestrictions());
		pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 80);
		pagedDefinition.addColumnDefinition(DESC_EN, I18N.message("desc.en"), String.class, Align.LEFT, 200);
		pagedDefinition.addColumnDefinition(DESC, I18N.message("desc"), String.class, Align.LEFT, 200);
		
		EntityPagedDataProvider<ContactVerification> pagedDataProvider = new EntityPagedDataProvider<ContactVerification>();
		pagedDataProvider.setPagedDefinition(pagedDefinition);
		return pagedDataProvider;
	}

		
	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractTablePanel#getEntity()
	 */
	@Override
	protected ContactVerification getEntity() {
		final Long id = getItemSelectedId();
		if (id != null) {
		    return EMPL_SRV.getById(ContactVerification.class, id);
		}
		return null;
	}
	
	@Override
	protected ContactVerificationSearchPanel createSearchPanel() {
		return new ContactVerificationSearchPanel(this);		
	}
}
