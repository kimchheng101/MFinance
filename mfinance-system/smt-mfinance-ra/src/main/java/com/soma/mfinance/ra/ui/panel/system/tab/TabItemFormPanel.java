package com.soma.mfinance.ra.ui.panel.system.tab;

import com.soma.mfinance.core.tab.model.TabItemEntity;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.soma.frmk.vaadin.util.VaadinServicesHelper;
import com.vaadin.ui.*;
import org.apache.commons.lang.StringUtils;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;

/**
 * Created by k.seang on 3/3/2017.
 */
@org.springframework.stereotype.Component
@Scope("prototype")
public class TabItemFormPanel extends AbstractFormPanel implements VaadinServicesHelper {

    private TabItemEntity tabItemEntity;
    protected TextField txtId;
    private CheckBox cbActive;
    private TextField txtCode;
    private TextField txtDesc;
    private ProfileGroupLayout profileGroupLayout;
    protected EntityComboBox<TabItemEntity> cbxParents;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        setCaption(I18N.message("tab.item.create"));
        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addSaveClickListener(this);
    }

    public TabItemFormPanel() {

    }

    @Override
    protected Entity getEntity() {
        this.tabItemEntity.setCode(txtCode.getValue());
        this.tabItemEntity.setDesc(txtDesc.getValue());
        this.tabItemEntity.setStatusRecord(((Boolean)this.cbActive.getValue()).booleanValue()?EStatusRecord.ACTIV:EStatusRecord.INACT);
        this.tabItemEntity.setParent(cbxParents.getSelectedEntity());
        tabItemEntity.setTabProfiles(profileGroupLayout.getSelectedProfile());
        return this.tabItemEntity;
    }

    @Override
    protected Component createForm() {
        this.txtId = ComponentFactory.getTextField("id", false, 60, 200.0F);
        this.txtCode = ComponentFactory.getTextField("code", true, 60, 200.0F);
        this.txtDesc = ComponentFactory.getTextField35("desc", true, 60, 200.0F);
        this.cbActive = new CheckBox(I18N.message("active"));
        this.cbActive.setValue(Boolean.valueOf(true));
        txtId.setEnabled(false);

        cbxParents = new EntityComboBox<TabItemEntity>(TabItemEntity.class, I18N.message("parent"), "code", "");
        cbxParents.renderer();
        cbxParents.setImmediate(true);

        FormLayout formPanel = new FormLayout();
        formPanel.addComponent(this.txtId);
        formPanel.addComponent(this.txtCode);
        formPanel.addComponent(this.txtDesc);
        formPanel.addComponent(cbxParents);
        formPanel.addComponent(this.cbActive);

        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.setSpacing(true);
        mainLayout.setMargin(true);
        mainLayout.addComponent(formPanel);
        this.profileGroupLayout = new ProfileGroupLayout();
        mainLayout.addComponent(this.profileGroupLayout);
        // Page Panel list all page that can have the tab
        // Profile Panel all profile that display this page
        Panel mainPanel = ComponentFactory.getPanel();
        mainPanel.setContent(mainLayout);

        return mainPanel;
    }

    /**
     * @param id
     */
    public void assignValues(Long id) {
        reset();
        if (id != null) {
            this.tabItemEntity = (TabItemEntity) ENTITY_SRV.getById(TabItemEntity.class, id);
            txtId.setValue(String.valueOf(tabItemEntity.getId()));
            this.txtCode.setValue(this.tabItemEntity.getCode());
            this.txtDesc.setValue(this.tabItemEntity.getDesc());
            this.cbActive.setValue(Boolean.valueOf(this.tabItemEntity.getStatusRecord() == EStatusRecord.ACTIV));
            cbxParents.setSelectedEntity(tabItemEntity.getParent());
            profileGroupLayout.setSelectedProfile(tabItemEntity.getTabProfiles());
            txtId.setVisible(true);
        } else {
            txtId.setVisible(false);
        }

    }

    @Override
    public void reset() {
        super.reset();
        tabItemEntity = new TabItemEntity();
        txtCode.setValue(StringUtils.EMPTY);
        txtDesc.setValue(StringUtils.EMPTY);
        cbxParents.setSelectedEntity(null);
        txtId.setValue(StringUtils.EMPTY);
        profileGroupLayout.clearValues();
        markAsDirty();
    }

    @Override
    protected boolean validate() {
        checkMandatoryField(txtCode,"code");
        checkMandatoryField(txtDesc,"desc");
        return errors.isEmpty();
    }
}
