package com.soma.mfinance.ra.ui.panel.referential.employment.occupation;

import com.soma.common.app.tools.helper.AppServicesHelper;
import com.soma.mfinance.core.application.model.EmploymentOccupation;
import com.soma.ersys.core.hr.model.eref.EEmploymentIndustry;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.*;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;

/**
 * Created by Pisethraingsey SOUN
 * Date     : 23/06/2017, 8:57 AM
 * Email    : p.suon@gl-f.com
 */
@org.springframework.stereotype.Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class EmploymentOccupationFormPanel extends AbstractFormPanel implements AppServicesHelper {
    private static final long serialVersionUID = -1811794282665846640L;

    private EmploymentOccupation employmentOccupation;
    private TextField txtCode;
    private TextField txtDesc;
    private TextField txtDescEn;
    private ERefDataComboBox<EEmploymentIndustry> cbxIndustry;
    private CheckBox cbActive;

    public EmploymentOccupationFormPanel() {

    }

    @PostConstruct
    public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = this.addNavigationPanel();
        navigationPanel.addSaveClickListener(this);
    }

    @Override
    protected Entity getEntity() {
        this.employmentOccupation.setCode(this.txtCode.getValue());
        this.employmentOccupation.setDesc(this.txtDesc.getValue());
        this.employmentOccupation.setDescEn(this.txtDescEn.getValue());
        this.employmentOccupation.setEmploymentIndustry(this.cbxIndustry.getSelectedEntity());
        this.employmentOccupation.setStatusRecord((this.cbActive.getValue()).booleanValue() ? EStatusRecord.ACTIV : EStatusRecord.INACT);
        return employmentOccupation;
    }

    @Override
    protected Component createForm() {
        FormLayout formPanel = new FormLayout();
        this.txtCode = ComponentFactory.getTextField("code", true, 50, 150.0F);
        this.txtDescEn = ComponentFactory.getTextField("desc.en", true, 200, 350.0F);
        this.txtDesc = ComponentFactory.getTextField35("desc", true, 200, 350.0F);
        this.cbxIndustry = new ERefDataComboBox(I18N.message("industry"), EEmploymentIndustry.values());
        this.cbxIndustry.setRequired(true);
        this.cbxIndustry.setWidth(200.0F, Unit.PIXELS);
        this.cbActive = new CheckBox(I18N.message("active"));
        this.cbActive.setValue(Boolean.valueOf(true));

        formPanel.addComponent(this.txtCode);
        formPanel.addComponent(this.txtDesc);
        formPanel.addComponent(this.txtDescEn);
        formPanel.addComponent(this.cbxIndustry);
        formPanel.addComponent(this.cbActive);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        verticalLayout.setSpacing(true);
        verticalLayout.addComponent(formPanel);
        Panel mainPanel = ComponentFactory.getPanel();
        mainPanel.setContent(verticalLayout);
        return mainPanel;
    }

    public void assignValues(Long itemSelectedId) {
        if (itemSelectedId != null) {
            this.employmentOccupation = ENTITY_SRV.getById(EmploymentOccupation.class, itemSelectedId);
            this.txtCode.setValue(this.employmentOccupation.getCode());
            this.txtDescEn.setValue(this.employmentOccupation.getDescEn());
            this.txtDesc.setValue(this.employmentOccupation.getDesc());
            this.cbxIndustry.setSelectedEntity(this.employmentOccupation.getEmploymentIndustry());
            this.cbActive.setValue(Boolean.valueOf(this.employmentOccupation.getStatusRecord() == EStatusRecord.ACTIV));
        }
    }

    public void reset() {
        super.reset();
        this.employmentOccupation = new EmploymentOccupation();
        this.txtCode.setValue("");
        this.txtDescEn.setValue("");
        this.txtDesc.setValue("");
        this.cbxIndustry.setSelectedEntity(null);
        this.cbActive.setValue(Boolean.valueOf(true));
        this.markAsDirty();
    }

    protected boolean validate() {
        this.checkMandatoryField(this.txtCode, "code");
        this.checkMandatoryField(this.txtDesc, "desc");
        this.checkMandatoryField(this.txtDescEn, "desc.en");
        this.checkMandatorySelectField(this.cbxIndustry, "industry");
        return this.errors.isEmpty();
    }

}
