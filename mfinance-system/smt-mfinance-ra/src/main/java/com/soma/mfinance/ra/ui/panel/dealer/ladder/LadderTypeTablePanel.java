package com.soma.mfinance.ra.ui.panel.dealer.ladder;

import javax.annotation.PostConstruct;

import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.soma.mfinance.core.dealer.model.LadderType;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.ui.Table.Align;

/**
 * 
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LadderTypeTablePanel extends AbstractTablePanel<LadderType> implements FMEntityField {
	
	/**
	 */
	private static final long serialVersionUID = -4371220788070058393L;

	@PostConstruct
	public void PostConstruct() {
		setCaption(I18N.message("ladder.types"));
		setSizeFull();
		setMargin(true);
		setSpacing(true);
				
		super.init(I18N.message("ladder.types"));
		
		addDefaultNavigation();
	}	
	
	/**
	 * Get item selected id
	 * @return
	 */
	public Long getItemSelectedId() {
		if (selectedItem != null) {
			return (Long) selectedItem.getItemProperty(ID).getValue();
		}
		return null;
	}	
	
	/**
	 * Get Paged definition
	 * @return
	 */
	@Override
	protected PagedDataProvider<LadderType> createPagedDataProvider() {
		PagedDefinition<LadderType> pagedDefinition = new PagedDefinition<>(searchPanel.getRestrictions());
		pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition(DESC_EN, I18N.message("desc"), String.class, Align.LEFT, 200);
		
		EntityPagedDataProvider<LadderType> pagedDataProvider = new EntityPagedDataProvider<>();
		pagedDataProvider.setPagedDefinition(pagedDefinition);
		return pagedDataProvider;
	}

		
	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractTablePanel#getEntity()
	 */
	@Override
	protected LadderType getEntity() {
		final Long id = getItemSelectedId();
		if (id != null) {
		    return ENTITY_SRV.getById(LadderType.class, id);
		}
		return null;
	}
	
	@Override
	protected LadderTypeSearchPanel createSearchPanel() {
		return new LadderTypeSearchPanel(this);		
	}
}
