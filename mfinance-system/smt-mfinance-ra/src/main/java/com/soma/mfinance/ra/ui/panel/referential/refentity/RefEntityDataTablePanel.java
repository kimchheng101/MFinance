package com.soma.mfinance.ra.ui.panel.referential.refentity;

import com.soma.ersys.vaadin.ui.referential.reftable.refdata.IRefTableDataForm;
import com.soma.ersys.vaadin.ui.referential.reftable.refdata.RefTableDataForm;
import com.soma.ersys.vaadin.ui.referential.reftable.refdata.RefTableDataPopupPanel;
import com.soma.ersys.vaadin.ui.referential.reftable.refdata.RefTableDataTablePanel;
import com.soma.frmk.config.model.RefTable;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.vaadin.data.Item;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.UI;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.eref.BaseERefData;
import org.seuksa.frmk.model.eref.BaseERefEntity;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by ki.kao on 6/12/2017.
 */
@Component
@Scope("prototype")
public class RefEntityDataTablePanel extends RefTableDataTablePanel {

    public RefEntityDataTablePanel() {
        super();
    }

    private Long getRefDataId(Item item) {
        return this.refTableIds != null && this.refTableIds.containsKey(item) ? (Long) this.refTableIds.get(item) : null;
    }

    @Override
    public void addButtonClick(Button.ClickEvent event) {
        try {
            IRefTableDataForm e = this.refTableDataForm;
            e.addFieldValueControls();
            RefTableDataPopupPanel refTableDataPopupPanel = new RefTableDataPopupPanel(this, I18N.message("ref.entry"), e);
            refTableDataPopupPanel.reset();
            refTableDataPopupPanel.setRefTableId(this.refTable.getId());
        } catch (IllegalStateException var4) {
            this.errors.clear();
            this.errors.add(var4.getMessage());
            this.displayErrorsPanel();
        }

    }

    @Override
    public void editButtonClick(Button.ClickEvent event) {
        if (this.selectedItem == null) {
            MessageBox e = new MessageBox(UI.getCurrent(), "280px", "150px", I18N.message("information"), MessageBox.Icon.WARN, I18N.message("msg.info.edit.item.not.selected"), Alignment.MIDDLE_RIGHT, new MessageBox.ButtonConfig[]{new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, I18N.message("ok"))});
            e.show();
        } else if (!this.refTable.getReadOnly().booleanValue()) {
            try {
                Long e1 = this.getRefDataId(this.selectedItem);
                IRefTableDataForm form = this.refTableDataForm;
                RefTableDataPopupPanel refTableDataPopupPanel = new RefTableDataPopupPanel(this, I18N.message("ref.data"), form) {
                    @Override
                    public void assignValues(Long entityId) {
                        if (entityId != null && refTable != null) {
                            System.out.println(refTable.getName());
                            BaseERefEntity entity = BaseERefData.getById(refTable.getName(), entityId);
                            //((RefTableDataForm) refTableDataForm).assignValues(entity, refTable);
                        }
                    }
                };
                refTableDataPopupPanel.setRefTableId(this.refTable.getId());
                refTableDataPopupPanel.assignValues(e1);
            } catch (IllegalStateException var5) {
                this.errors.clear();
                this.errors.add(var5.getMessage());
                this.displayErrorsPanel();
            }
        }

    }

    @Override
    public void assignValuesToControls(Long entityId, IRefTableDataForm refTableDataForm) {
        this.refTableDataForm = refTableDataForm;
        selectedItem = null;
        if (entityId != null) {
            reset();
            refTable = ENTITY_SRV.getById(RefTable.class, entityId);
            this.columnDefinitions = createColumnDefinitions();
            try {
                Method method = getClass().getSuperclass().getDeclaredMethod("assignValues");
                method.setAccessible(true);
                method.invoke(this);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

            if (refTable.getReadOnly()) {
                navigationPanel.getAddClickButton().setVisible(false);
                navigationPanel.getEditClickButton().setVisible(false);
                navigationPanel.getDeleteClickButton().setVisible(false);
            } else {
                navigationPanel.getAddClickButton().setVisible(true);
                navigationPanel.getEditClickButton().setVisible(true);
                navigationPanel.getDeleteClickButton().setVisible(true);
            }
        }
    }
}
