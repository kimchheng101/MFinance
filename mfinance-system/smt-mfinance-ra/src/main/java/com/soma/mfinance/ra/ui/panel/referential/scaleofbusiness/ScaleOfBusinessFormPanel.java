package com.soma.mfinance.ra.ui.panel.referential.scaleofbusiness;


import com.soma.mfinance.core.address.model.ScaleOfBusiness;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author by kimsuor.seang  on 10/13/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ScaleOfBusinessFormPanel extends AbstractFormPanel {

	private static final long serialVersionUID = 376274000596870581L;

	private ScaleOfBusiness scaleOfBusiness;
	private TextField txtDesc;
    private TextField txtDescEn;
    
    @PostConstruct
	public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
		navigationPanel.addSaveClickListener(this);
	}
	
	@Override
	protected Entity getEntity() {
		scaleOfBusiness.setDesc(txtDesc.getValue());
		scaleOfBusiness.setDescEn(txtDescEn.getValue());
		return scaleOfBusiness;
	}

	@Override
	protected com.vaadin.ui.Component createForm() {
		final FormLayout formPanel = new FormLayout();
		txtDescEn = ComponentFactory.getTextField("desc.en", true, 60, 200);
		txtDesc = ComponentFactory.getTextField35("desc", false, 60, 200);
        
        formPanel.addComponent(txtDescEn);
        formPanel.addComponent(txtDesc);
		return formPanel;
	}

	public void assignValues(Long scaBusId) {
		super.reset();
		if (scaBusId != null) {
			scaleOfBusiness = ENTITY_SRV.getById(ScaleOfBusiness.class, scaBusId);
			txtDescEn.setValue(scaleOfBusiness.getDescEn());
			txtDesc.setValue(scaleOfBusiness.getDesc());
		}
	}

	@Override
	public void reset() {
		super.reset();
		scaleOfBusiness = new ScaleOfBusiness();
		txtDescEn.setValue("");
		txtDesc.setValue("");
		markAsDirty();
	}
	
	@Override
	protected boolean validate() {
		checkMandatoryField(txtDescEn, "desc.en");
		return errors.isEmpty();
	}
}
