package com.soma.mfinance.ra.ui.panel.Lease.amount;

import com.soma.mfinance.core.financial.model.LeaseAmountPercent;
import com.soma.mfinance.core.interestrate.InterestRate;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.model.entity.Entity;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
/**
 * Created by b.chea on 4/10/2017.
 */

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LeaseAmountFormPanel extends AbstractFormPanel{

    private LeaseAmountPercent leaseAmountPercent;
    private CheckBox cbActive;
    private TextField txtDescEn;
    private TextField txtValue;

    /** */
    @PostConstruct
    public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addSaveClickListener(this);
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractFormPanel#createForm()
     */
    @Override
    protected com.vaadin.ui.Component createForm() {

        txtValue = ComponentFactory.getTextField("value", true, 3, 200);
        txtDescEn = ComponentFactory.getTextField("desc.en", true, 100, 200);
        cbActive = new CheckBox(I18N.message("active"));
        cbActive.setValue(true);

        FormLayout formLayout = new FormLayout();
        formLayout.addComponent(txtValue);
        formLayout.addComponent(txtDescEn);
        formLayout.addComponent(cbActive);
        return formLayout;
    }

    /**
     * Assign value to form
     * @param
     */
    public void assignValues(Long leaseAmountPercentId) {
        reset();
        if (leaseAmountPercentId != null) {
            leaseAmountPercent = ENTITY_SRV.getById(LeaseAmountPercent.class, leaseAmountPercentId);
            txtValue.setValue(leaseAmountPercent.getValue() != null ? String.valueOf(leaseAmountPercent.getValue()) : "");
            txtDescEn.setValue(leaseAmountPercent.getDescEn());
            cbActive.setValue(leaseAmountPercent.getStatusRecord().equals(EStatusRecord.ACTIV));
        }
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractFormPanel#reset()
     */
    @Override
    public void reset() {
        super.reset();
        leaseAmountPercent = new LeaseAmountPercent();
        txtValue.setValue("");
        txtDescEn.setValue("");
        cbActive.setValue(true);
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractFormPanel#validate()
     */
    @Override
    protected boolean validate() {
        checkDoubleField(txtValue, "value");
        checkMandatoryField(txtValue, "value");
        checkMandatoryField(txtDescEn, "desc.en");
        return errors.isEmpty();
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractFormPanel#getEntity()
     */
    @Override
    protected Entity getEntity() {
        leaseAmountPercent.setValue(MyNumberUtils.getDouble(txtValue.getValue(), 0));
        leaseAmountPercent.setDesc(txtDescEn.getValue());
        leaseAmountPercent.setDescEn(txtDescEn.getValue());
        leaseAmountPercent.setStatusRecord(cbActive.getValue() ? EStatusRecord.ACTIV : EStatusRecord.INACT);
        return leaseAmountPercent;
    }
}
