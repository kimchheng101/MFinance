package com.soma.mfinance.ra.ui.panel.referential.committees.committee;

import com.soma.mfinance.core.referential.committee.Committee;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.ui.Table;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by Dang Dim
 * Date     : 19-Oct-17, 11:05 AM
 * Email    : d.dim@gl-f.com
 */

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CommitteeTablePanel extends AbstractTablePanel<Committee> implements FMEntityField {

    @PostConstruct
    public void PostConstruct() {
        setCaption(I18N.message("Committee"));
        setSizeFull();
        setMargin(true);
        setSpacing(true);

        super.init(I18N.message("Committee"));
        addDefaultNavigation();

    }

    @Override
    protected Committee getEntity() {
        final Long id = getItemSelectedId();
        if (id != null) {
            return ENTITY_SRV.getById(Committee.class, id);
        }

        return null;
    }

    @Override
    protected PagedDataProvider<Committee> createPagedDataProvider() {

        PagedDefinition<Committee> pagedDefinition = new PagedDefinition<Committee>(searchPanel.getRestrictions());
        pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Table.Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("code", I18N.message("code"), String.class, Table.Align.LEFT, 200);
        pagedDefinition.addColumnDefinition("desc", I18N.message("committee.name"), String.class, Table.Align.LEFT, 200);
        pagedDefinition.addColumnDefinition("descEn", I18N.message("description"), String.class, Table.Align.LEFT, 200);

        EntityPagedDataProvider<Committee> pagedDataProvider = new EntityPagedDataProvider<Committee>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    @Override
    protected CommitteeSearchPanel createSearchPanel() {
        return new CommitteeSearchPanel(this);
    }
}
