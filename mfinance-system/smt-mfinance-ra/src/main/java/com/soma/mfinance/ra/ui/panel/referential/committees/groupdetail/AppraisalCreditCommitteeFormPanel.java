package com.soma.mfinance.ra.ui.panel.referential.committees.groupdetail;

import com.soma.mfinance.core.asset.model.AssetModel;
import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.custom.component.CustomEntityRefComboBox;
import com.soma.mfinance.core.referential.creditcommitteeappraisal.model.AppraisalCreditCommittee;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.widget.SecUserComboBox;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.data.Property;
import com.vaadin.ui.FormLayout;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * @author by kimsuor.seang  on 9/27/2017.
 */

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AppraisalCreditCommitteeFormPanel extends AbstractFormPanel {

    private AppraisalCreditCommittee appraisalCreditCommittee;
    private SecUserComboBox cbxSecUser;
    private List<SecUser> secUsers;
    private EntityRefComboBox<AssetRange> cbxAssetRange;
    protected EntityRefComboBox<AssetModel> cbxAssetModel;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addSaveClickListener(this);
    }

    @Override
    protected Entity getEntity() {
        appraisalCreditCommittee.setSecUser(cbxSecUser.getSelectedEntity());
        appraisalCreditCommittee.setAssetRange(cbxAssetRange.getSelectedEntity());
        appraisalCreditCommittee.setAssetModel(cbxAssetModel.getSelectedEntity());
        return appraisalCreditCommittee;
    }

    @Override
    protected com.vaadin.ui.Component createForm() {
        final FormLayout formPanel = new FormLayout();
        Long ManagementProfileId = 4L;
        secUsers = DataReference.getInstance().getUsers(ManagementProfileId);
        cbxSecUser = new SecUserComboBox(I18N.message("user"), secUsers) {
            public void setValues(List<SecUser> users) {
                clear();
                if (users != null && !users.isEmpty()) {
                    for (SecUser user : users) {
                        addItem(user.getId().toString());
                        setItemCaption(user.getId().toString(), user.getLogin());
                        valueMap.put(user.getId().toString(), user);
                    }
                }
            }
        };
        cbxSecUser.setImmediate(true);
        cbxSecUser.setRequired(true);


        cbxAssetRange = new CustomEntityRefComboBox<>();
        cbxAssetRange.setCaption(I18N.message("asset.make2"));
        cbxAssetRange.setRestrictions(new BaseRestrictions<>(AssetRange.class));
        cbxAssetRange.renderer();
        cbxAssetRange.setSelectedEntity(null);
        cbxAssetRange.setImmediate(true);
            cbxAssetRange.addValueChangeListener(new Property.ValueChangeListener() {
                @Override
                public void valueChange(Property.ValueChangeEvent event) {
                    cbxSecUser.setValues(getListUserManagement(cbxAssetRange.getSelectedEntity()));
                }
            });

        cbxAssetModel = new CustomEntityRefComboBox<>();
        cbxAssetModel.setCaption(I18N.message("asset.range2"));
        cbxAssetModel.setRestrictions(new BaseRestrictions<>(AssetModel.class));
        cbxAssetModel.renderer();
        cbxAssetModel.setSelectedEntity(null);
        cbxAssetModel.setImmediate(true);

        formPanel.addComponent(cbxAssetRange);
        formPanel.addComponent(cbxAssetModel);
        formPanel.addComponent(cbxSecUser);
        return formPanel;
    }

	private List<SecUser> getListUserManagement(AssetRange assetRange){
		List<SecUser> newUserList = new ArrayList<>();
		BaseRestrictions<AppraisalCreditCommittee> restrictions = new BaseRestrictions<>(AppraisalCreditCommittee.class);
		if(assetRange != null){
			restrictions.addCriterion(Restrictions.eq("assetRange", assetRange));
		}
		List<AppraisalCreditCommittee> appraisalCommittees = ENTITY_SRV.list(restrictions);
		List<SecUser> usersIncommittee = new ArrayList<>();
		for(AppraisalCreditCommittee appraisalCommittee : appraisalCommittees){
			usersIncommittee.add(appraisalCommittee.getSecUser());
		}
		for(SecUser secUser : secUsers){
			if(!usersIncommittee.contains(secUser)){
				newUserList.add(secUser);
			}
		}
		return newUserList;
	}

    public void assignValues(Long id) {
        reset();
		if (id != null) {
			cbxSecUser.clear();
			List<SecUser> users = getListUserManagement(cbxAssetRange.getSelectedEntity());
			appraisalCreditCommittee = ENTITY_SRV.getById(AppraisalCreditCommittee.class, id);
			if(appraisalCreditCommittee.getAssetRange() != null)
				cbxAssetRange.setSelectedEntity(appraisalCreditCommittee.getAssetRange());
			cbxAssetModel.setSelectedEntity(appraisalCreditCommittee.getAssetModel());
			if(!users.contains(appraisalCreditCommittee.getSecUser())){
				users.add(appraisalCreditCommittee.getSecUser());
			}
			cbxSecUser.setValues(users);
			cbxSecUser.setSelectedEntity(appraisalCreditCommittee.getSecUser());
		}

    }

    @Override
    public void reset() {
        super.reset();
        appraisalCreditCommittee = new AppraisalCreditCommittee();
        cbxSecUser.clear();
        cbxSecUser.setValues(secUsers);
        cbxAssetModel.setSelectedEntity(null);
        cbxAssetRange.setSelectedEntity(null);
        markAsDirty();
    }

    @Override
    protected boolean validate() {
        checkMandatorySelectField(cbxSecUser, "user");
        checkMandatorySelectField(cbxAssetRange, "asset.range2");
        checkMandatorySelectField(cbxAssetModel, "asset.make2");
        return errors.isEmpty();
    }

}
