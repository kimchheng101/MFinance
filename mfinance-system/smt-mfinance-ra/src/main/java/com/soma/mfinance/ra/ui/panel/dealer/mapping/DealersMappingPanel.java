package com.soma.mfinance.ra.ui.panel.dealer.mapping;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * @author by kimsuor.seang  on 11/11/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(DealersMappingPanel.NAME)
public class DealersMappingPanel extends AbstractTabsheetPanel implements View {

    private static final long serialVersionUID = -2149707177396177780L;

    public static final String NAME = "dealers.mapping";

    @Autowired
    private DealerMappingTablePanel dealerTablePanel;
    @Autowired
    private DealerMappingFormPanel dealerFormPanel;



    @PostConstruct
    public void PostConstruct() {
        super.init();
        dealerTablePanel.setMainPanel(this);
        dealerFormPanel.setCaption(I18N.message("dealer"));
        getTabSheet().setTablePanel(dealerTablePanel);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }

    @Override
    public void onAddEventClick() {
        dealerFormPanel.reset();
        getTabSheet().addFormPanel(dealerFormPanel);
        getTabSheet().setSelectedTab(dealerFormPanel);
    }

    @Override
    public void onEditEventClick() {
        getTabSheet().addFormPanel(dealerFormPanel);
        initSelectedTab(dealerFormPanel);
    }

    @Override
    public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
        if (selectedTab == dealerFormPanel) {
            dealerFormPanel.assignValues(dealerTablePanel.getItemSelectedId());
        } else if (selectedTab == dealerTablePanel && getTabSheet().isNeedRefresh()) {
            dealerTablePanel.refresh();
        }
        getTabSheet().setSelectedTab(selectedTab);
    }
}
