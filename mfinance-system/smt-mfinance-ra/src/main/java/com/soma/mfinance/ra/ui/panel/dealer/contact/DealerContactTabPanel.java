package com.soma.mfinance.ra.ui.panel.dealer.contact;

import com.soma.mfinance.core.dealer.model.Dealer;
import com.vaadin.ui.VerticalLayout;
/**
 * 
 * @author kimsuor.seang
 *
 */
public class DealerContactTabPanel extends VerticalLayout {
	
	private static final long serialVersionUID = 6635987943587402568L;
	
	private DealerContactInfoPanel dealerContactInfoPanel;

	public DealerContactTabPanel() {
		setMargin(true);
		setSpacing(true);
		
        dealerContactInfoPanel = new DealerContactInfoPanel();
		
		addComponent(dealerContactInfoPanel);
	}
	
	/**
	 * AssignValue
	 */
	public void assignValue(Dealer dealer) {
		if (dealer != null) {
			dealerContactInfoPanel.assignValues(dealer);
		}
	}
	/**
	 * Reset
	 */
	public void reset() {
		dealerContactInfoPanel.reset();
	}

}
