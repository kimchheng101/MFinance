package com.soma.mfinance.ra.ui.panel.referential.employment.position;

import com.soma.mfinance.core.application.model.EmploymentOccupation;
import com.soma.mfinance.core.application.model.EmploymentPosition;
import com.soma.ersys.core.hr.model.eref.EEmploymentIndustry;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.soma.frmk.vaadin.util.VaadinServicesHelper;
import com.vaadin.data.Property;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.EntityFactory;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Create by Rith RON.
 * User r.ron.
 * Date on 2017-06-21.
 * Time at 2:13 PM.
 * Email r.ron@gl-f.com.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PositionFormPanel extends AbstractFormPanel  implements VaadinServicesHelper {
    private EmploymentPosition employmentPosition;

    private CheckBox cbActive;
    private TextField txtCode;
    private TextField txtDesc;
    private TextField txtDescEn;
    private EntityRefComboBox<EmploymentOccupation> cbxEmploymentOccupation;
    private ERefDataComboBox<EEmploymentIndustry> cbxEmploymentIndustry;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addSaveClickListener(this);
    }
    @Override
    protected Entity getEntity() {
        employmentPosition.setCode(txtCode.getValue());
        employmentPosition.setDesc(txtDesc.getValue());
        employmentPosition.setDescEn(txtDescEn.getValue());
        employmentPosition.setStatusRecord(cbActive.getValue() ? EStatusRecord.ACTIV: EStatusRecord.INACT);
        employmentPosition.setEmploymentOccupation(cbxEmploymentOccupation.getSelectedEntity());
        employmentPosition.setEmploymentIndustry(cbxEmploymentIndustry.getSelectedEntity());
        return employmentPosition;
    }

    @Override
    protected com.vaadin.ui.Component createForm() {
        final FormLayout formPanel = new FormLayout();
        txtCode = ComponentFactory.getTextField("code", true, 60, 200);
        txtDescEn = ComponentFactory.getTextField("desc.en", true, 60, 200);
        txtDesc = ComponentFactory.getTextField35("desc", false, 60, 200);
        cbActive = new CheckBox(I18N.message("active"));
        cbActive.setValue(true);
        cbxEmploymentOccupation = new EntityRefComboBox<EmploymentOccupation>(I18N.message("employment.occupation"), true);
        cbxEmploymentOccupation.setRestrictions(new BaseRestrictions<>(EmploymentOccupation.class));
        cbxEmploymentOccupation.renderer();
        cbxEmploymentOccupation.setImmediate(true);
        cbxEmploymentIndustry = new ERefDataComboBox<EEmploymentIndustry>(I18N.message("employment.industry"), EEmploymentIndustry.class);
        cbxEmploymentIndustry.addValueChangeListener(new Property.ValueChangeListener() {

            private static final long serialVersionUID = 6740681769253190853L;

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if (cbxEmploymentIndustry.getSelectedEntity() != null) {
                    BaseRestrictions<EmploymentOccupation> restrictions = cbxEmploymentOccupation.getRestrictions();
                    List<Criterion> criterions = new ArrayList<Criterion>();
                    criterions.add(Restrictions.eq("employmentIndustry", cbxEmploymentIndustry.getSelectedEntity()));
                    criterions.add(Restrictions.eq("statusRecord", EStatusRecord.ACTIV));
                    restrictions.setCriterions(criterions);
                    cbxEmploymentOccupation.renderer();
                } else {
                    cbxEmploymentOccupation.clear();
                }
            }
        });
        formPanel.addComponent(txtCode);
        formPanel.addComponent(txtDescEn);
        formPanel.addComponent(txtDesc);
        formPanel.addComponent(cbxEmploymentIndustry);
        formPanel.addComponent(cbxEmploymentOccupation);
        formPanel.addComponent(cbActive);
        return formPanel;
    }
    /**
     * @param id
     */
    public void assignValues(Long id) {
        super.reset();
        if (id != null) {
            employmentPosition = ENTITY_SRV.getById(EmploymentPosition.class, id);
            txtCode.setValue(employmentPosition.getCode());
            txtDescEn.setValue(employmentPosition.getDescEn());
            txtDesc.setValue(employmentPosition.getDesc());
            cbActive.setValue(employmentPosition.getStatusRecord() == EStatusRecord.ACTIV);
            cbxEmploymentOccupation.setSelectedEntity(employmentPosition.getEmploymentOccupation());
            cbxEmploymentIndustry.setSelectedEntity(employmentPosition.getEmploymentIndustry());
        }
    }
    public void reset() {
        super.reset();
        employmentPosition = (EmploymentPosition) EntityFactory.createInstance(EmploymentPosition.class);
        txtCode.setValue("");
        txtDescEn.setValue("");
        cbActive.setValue(Boolean.valueOf(true));
        cbActive.setValue(Boolean.valueOf(true));
        markAsDirty();
    }
}
