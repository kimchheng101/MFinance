package com.soma.mfinance.ra.ui.panel.advancepayment;

import com.soma.mfinance.core.payment.model.AdvancePayment;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.*;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;

/**
 * Created by b.chea on 2/22/2017.
 */
public class AdvancePaymentSearchPanel extends AbstractSearchPanel<AdvancePayment> implements FMEntityField {

    private CheckBox cbActive;
    private CheckBox cbInactive;
    private TextField txtDescEn;

    /**
     * @param advancePaymentTablePanel
     */
    public AdvancePaymentSearchPanel(AbstractTablePanel advancePaymentTablePanel) {
        super(I18N.message("search"), advancePaymentTablePanel);
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#reset()
     */
    @Override
    protected void reset() {
        txtDescEn.setValue("");
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#createForm()
     */
    @Override
    protected Component createForm() {
        final GridLayout gridLayout = new GridLayout(3, 1);

        txtDescEn = ComponentFactory.getTextField("desc.en", false, 60, 200);

        cbActive = new CheckBox(I18N.message("active"));
        cbActive.setValue(true);

        cbInactive = new CheckBox(I18N.message("inactive"));
        cbInactive.setValue(false);

        int iCol = 0;
        gridLayout.setSpacing(true);
        gridLayout.addComponent(new FormLayout(txtDescEn), iCol++, 0);
        gridLayout.addComponent(new FormLayout(cbActive), iCol++, 0);
        gridLayout.addComponent(new FormLayout(cbInactive), iCol++, 0);

        return gridLayout;
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#getRestrictions()
     */
    @Override
    public BaseRestrictions<AdvancePayment> getRestrictions() {

        BaseRestrictions<AdvancePayment> restrictions = new BaseRestrictions<>(AdvancePayment.class);

        if (StringUtils.isNotEmpty(txtDescEn.getValue())) {
            restrictions.addCriterion(Restrictions.ilike(DESC_EN, txtDescEn.getValue(), MatchMode.ANYWHERE));
        }
        if (!cbActive.getValue() && !cbInactive.getValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
            restrictions.getStatusRecordList().add(EStatusRecord.INACT);
        }
        if (cbActive.getValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
        }
        if (cbInactive.getValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.INACT);
        }

        return restrictions;
    }
}
