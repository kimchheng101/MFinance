package com.soma.mfinance.ra.ui.panel.referential.committees.group;

import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.referential.committee.Committee;
import com.soma.mfinance.core.referential.committee.GroupCommittee;
import com.soma.mfinance.core.referential.committee.GroupCommitteeDetail;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.mfinance.core.shared.component.ComponentLayoutFactory;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.SelectedItem;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.dialogs.ConfirmDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dang Dim
 * Date     : 31-Oct-17, 5:37 PM
 * Email    : d.dim@gl-f.com
 */

public class GroupCommitteeTableDetailPanel extends AbstractTabPanel implements FMEntityField, FinServicesHelper, ItemClickEvent.ItemClickListener, SelectedItem, Button.ClickListener {

    private GroupCommitteeFormPanel delegate;
    private List<ColumnDefinition> columnDefinitions;
    private SimplePagedTable<GroupCommitteeDetail> pagedTable;
    private GroupCommitteeDetail groupCommitteeDetail;

    private Button btnAdd;
    private Button btnDelete;
    private Item selectedItem;

    private EntityComboBox<GroupCommittee> cbxGroupCommittee;
    private EntityRefComboBox<Committee> cbxCommittee;
    private EntityRefComboBox<AssetRange> cbxAssetRange;

    public GroupCommitteeTableDetailPanel(GroupCommitteeFormPanel delegate) {
        super();
        this.delegate = delegate;
        setSizeFull();
        setMargin(true);
        setSpacing(true);
        this.setCaption("Group Committee Detail");
    }


    @Override
    protected com.vaadin.ui.Component createForm() {
        this.columnDefinitions = createColumnDefinitions();
        pagedTable = new SimplePagedTable<>(this.columnDefinitions);
        pagedTable.addItemClickListener(this);

        btnAdd = new NativeButton(I18N.message("add"));
        btnAdd.setIcon(FontAwesome.PLUS);
        btnAdd.addClickListener(this);
        btnDelete = new NativeButton(I18N.message("delete"));
        btnDelete.setIcon(FontAwesome.TRASH_O);
        btnDelete.addClickListener(this);

        NavigationPanel navigationPanel = new NavigationPanel();
        navigationPanel.addButton(btnAdd);
        navigationPanel.addButton(btnDelete);

        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setSpacing(true);
        contentLayout.addComponent(navigationPanel);
        contentLayout.addComponent(pagedTable);
        contentLayout.addComponent(pagedTable.createControls());

        cbxGroupCommittee = new EntityComboBox<GroupCommittee>(GroupCommittee.class, "desc");
        cbxGroupCommittee.renderer();
        cbxGroupCommittee.setSelectedEntity(null);
        cbxGroupCommittee.setImmediate(true);

        BaseRestrictions<Committee> committee = new BaseRestrictions<>(Committee.class);
        cbxCommittee = new EntityRefComboBox();
        cbxCommittee.setRestrictions(committee);
        cbxCommittee.renderer();
        cbxCommittee.setSelectedEntity(null);
        cbxCommittee.setImmediate(true);

        BaseRestrictions<AssetRange> ass = new BaseRestrictions<>(AssetRange.class);
        cbxAssetRange = new EntityRefComboBox<>();
        cbxAssetRange.setRestrictions(ass);
        cbxAssetRange.renderer();
        cbxAssetRange.setSelectedEntity(null);
        cbxAssetRange.setImmediate(true);

        return contentLayout;
    }

    @Override
    public Item getSelectedItem() {
        return this.selectedItem;
    }

    @Override
    public void itemClick(ItemClickEvent event) {
        this.selectedItem = event.getItem();
        if (event.isDoubleClick()) {
            popUpGroupCommitteeDetailFormPanel();
            assignValues(getItemSelectedId());
        }
    }

    @Override
    public void buttonClick(Button.ClickEvent event) {
        if (event.getButton() == btnAdd) {
            popUpGroupCommitteeDetailFormPanel();
        } else if (event.getButton() == btnDelete) {
            if (this.selectedItem == null) {
                ComponentLayoutFactory.displayErrorMsg("msg.info.delete.item.not.selected");
            } else {
                ConfirmDialog confirmDialog = ConfirmDialog.show(UI.getCurrent(), I18N.message("msg.ask.delete"), new ConfirmDialog.Listener() {
                    private static final long serialVersionUID = 4605195684545338632L;

                    public void onClose(ConfirmDialog dialog) {
                        if (dialog.isConfirmed()) {
                            try {
                                ENTITY_SRV.delete(ENTITY_SRV.getById(GroupCommitteeDetail.class, getItemSelectedId()));
                                dialog.close();
                            } catch (Exception e) {
                                logger.error(e.getMessage());
                                ComponentLayoutFactory.displayErrorMsg("msg.warning.delete.selected.item.is.used");
                            }
                        }
                    }
                });
                confirmDialog.setWidth("400px");
                confirmDialog.setHeight("150px");
            }
        }

    }

    @SuppressWarnings("unchecked")
    private IndexedContainer getIndexedContainer(List<GroupCommitteeDetail> committeeDetails) {
        IndexedContainer indexedContainer = new IndexedContainer();

        for (ColumnDefinition column : this.columnDefinitions) {
            indexedContainer.addContainerProperty(column.getPropertyId(), column.getPropertyType(), null);
        }

        if (committeeDetails != null && !committeeDetails.isEmpty()) {
            for (GroupCommitteeDetail groupCommitteeDetail : committeeDetails) {
                Item item = indexedContainer.addItem(groupCommitteeDetail.getId());
                item.getItemProperty(ID).setValue(groupCommitteeDetail.getId());
                item.getItemProperty("groupCommittee").setValue(groupCommitteeDetail.getGroupCommittee().getDesc());
                item.getItemProperty("assetRange").setValue(groupCommitteeDetail.getAssetRange().getDesc());
                item.getItemProperty("committee.type").setValue(groupCommitteeDetail.getCommittee().getDesc());
            }
        }

        return indexedContainer;
    }

    protected List<ColumnDefinition> createColumnDefinitions() {
        List<ColumnDefinition> columnDefinitions = new ArrayList<ColumnDefinition>();
        columnDefinitions.add(new ColumnDefinition(ID, I18N.message("id"), Long.class, Table.Align.LEFT, 70));
        columnDefinitions.add(new ColumnDefinition("groupCommittee", I18N.message("groupCommittee"), String.class, Table.Align.LEFT, 200));
        columnDefinitions.add(new ColumnDefinition("assetRange", I18N.message("asset.range2"), String.class, Table.Align.LEFT, 200));
        columnDefinitions.add(new ColumnDefinition("committee.type", I18N.message("committee.type"), String.class, Table.Align.LEFT, 200));
        return columnDefinitions;
    }

    public boolean isValid() {
        checkMandatorySelectField(cbxGroupCommittee, "Group.committee");
        checkMandatorySelectField(cbxAssetRange, "asset range");
        checkMandatorySelectField(cbxCommittee, "committee.type");
        return errors.isEmpty();
    }

    public void assignValue(GroupCommittee groupCommittee) {
        this.selectedItem = null;
        if (groupCommittee != null) {
            BaseRestrictions<GroupCommitteeDetail> groupComDetail = new BaseRestrictions<>(GroupCommitteeDetail.class);
            groupComDetail.addCriterion(Restrictions.eq("groupCommittee.id", groupCommittee.getId()));
            List<GroupCommitteeDetail> listDetail = ENTITY_SRV.list(groupComDetail);
            pagedTable.setContainerDataSource(getIndexedContainer(listDetail));
        }
    }

    public void assignValues(Long id) {
        if (id != null) {
            groupCommitteeDetail= ENTITY_SRV.getById(GroupCommitteeDetail.class, id);
            cbxGroupCommittee.setSelectedEntity(groupCommitteeDetail.getGroupCommittee());
            cbxCommittee.setSelectedEntity(groupCommitteeDetail.getCommittee());
            cbxAssetRange.setSelectedEntity(groupCommitteeDetail.getAssetRange());
        }
    }

    private Long getItemSelectedId() {
        if (this.selectedItem != null) {
            return ((Long) this.selectedItem.getItemProperty(ID).getValue());
        }
        return null;
    }

    public void popUpGroupCommitteeDetailFormPanel() {
        final Window winGroupComDetail = new Window();
        winGroupComDetail.setModal(true);
        winGroupComDetail.setClosable(false);
        winGroupComDetail.setResizable(false);
        winGroupComDetail.setWidth(400, Unit.PIXELS);
        winGroupComDetail.setHeight(250, Unit.PIXELS);
        winGroupComDetail.setCaption(I18N.message("please.select.dealer"));
        groupCommitteeDetail = new GroupCommitteeDetail();

        cbxAssetRange.setSelectedEntity(null);
        cbxCommittee.setSelectedEntity(null);
        cbxGroupCommittee.setSelectedEntity(null);

        Button btnCancel = new NativeButton(I18N.message("cancel"), new Button.ClickListener() {
            private static final long serialVersionUID = 3975121141565713259L;

            public void buttonClick(Button.ClickEvent event) {
                winGroupComDetail.close();
            }
        });
        btnCancel.setIcon(new ThemeResource("../smt-default/icons/16/delete.png"));

        Button btnSave = new NativeButton(I18N.message("save"), new Button.ClickListener() {
            private static final long serialVersionUID = 8088485001713740490L;

            public void buttonClick(Button.ClickEvent event) {
                if (isValid()) {
                    groupCommitteeDetail.setGroupCommittee(cbxGroupCommittee.getSelectedEntity());
                    groupCommitteeDetail.setAssetRange(cbxAssetRange.getSelectedEntity());
                    groupCommitteeDetail.setCommittee(cbxCommittee.getSelectedEntity());
                    ENTITY_SRV.saveOrUpdate(groupCommitteeDetail);
                    GroupCommittee groupCommittee = ENTITY_SRV.getById(GroupCommittee.class, cbxGroupCommittee.getSelectedEntity().getId());
                    assignValue(groupCommittee);
                    winGroupComDetail.close();
                }

            }
        });
        btnSave.setIcon(new ThemeResource("../smt-default/icons/16/save.png"));

        NavigationPanel navigationPanel = new NavigationPanel();
        navigationPanel.addButton(btnSave);
        navigationPanel.addButton(btnCancel);

        final GridLayout gridLayout = new GridLayout(2, 3);
        gridLayout.setMargin(true);
        gridLayout.setSpacing(true);

        gridLayout.addComponent(new Label(I18N.message("group.committee")), 0, 0);
        gridLayout.addComponent(cbxGroupCommittee, 1, 0);

        gridLayout.addComponent(new Label(I18N.message("asset.range2")), 0, 1);
        gridLayout.addComponent(cbxAssetRange, 1, 1);

        gridLayout.addComponent(new Label(I18N.message("committee.type")), 0, 2);
        gridLayout.addComponent(cbxCommittee, 1, 2);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addComponent(navigationPanel);
        verticalLayout.addComponent(gridLayout);
        winGroupComDetail.setContent(verticalLayout);
        UI.getCurrent().addWindow(winGroupComDetail);
    }
}
