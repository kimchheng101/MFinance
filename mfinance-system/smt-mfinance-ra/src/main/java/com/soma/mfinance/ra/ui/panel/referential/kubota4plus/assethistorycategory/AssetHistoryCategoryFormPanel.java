package com.soma.mfinance.ra.ui.panel.referential.kubota4plus.assethistorycategory;

import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.asset.model.appraisal.AssetHistoryCategory;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;


/**
 * @author by kimsuor.seang  on 10/9/2017.
 */

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AssetHistoryCategoryFormPanel extends AbstractFormPanel {

    private AssetHistoryCategory assetHistoryCategory;

    private CheckBox cbActive;
    private TextField txtDesc;
    private TextField txtDescEn;
    private TextField txtSortIndex;
    private EntityRefComboBox<AssetRange> cbxAssetAppraisalType;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addSaveClickListener(this);
    }

    @Override
    protected Entity getEntity() {
        assetHistoryCategory.setAssetAppraisalType(cbxAssetAppraisalType.getSelectedEntity());
        assetHistoryCategory.setDesc(txtDesc.getValue());
        assetHistoryCategory.setDescEn(txtDescEn.getValue());
        if(txtSortIndex.getValue() != null){
            this.assetHistoryCategory.setSortIndex(Integer.parseInt(txtSortIndex.getValue()));
        }
        assetHistoryCategory.setStatusRecord(cbActive.getValue() ? EStatusRecord.ACTIV : EStatusRecord.INACT);
        return assetHistoryCategory;
    }

    @Override
    protected com.vaadin.ui.Component createForm() {
        final FormLayout formPanel = new FormLayout();
        cbxAssetAppraisalType = new EntityRefComboBox<>();
        cbxAssetAppraisalType.setRestrictions(new BaseRestrictions<>(AssetRange.class));
        cbxAssetAppraisalType.renderer();
        cbxAssetAppraisalType.setCaption(I18N.message("asset.appraisal.type"));
        txtDescEn = ComponentFactory.getTextField("desc.en", true, 60, 200);
        txtDesc = ComponentFactory.getTextField35("desc", false, 60, 200);
        txtSortIndex = ComponentFactory.getTextField("sort.index", true, 60, 200);
        cbActive = new CheckBox(I18N.message("active"));
        cbActive.setValue(true);

        formPanel.addComponent(cbxAssetAppraisalType);
        formPanel.addComponent(txtDescEn);
        formPanel.addComponent(txtDesc);
        formPanel.addComponent(txtSortIndex);
        formPanel.addComponent(cbActive);

        getAssetAppraisalCategory(1L);
        return formPanel;
    }

    public void assignValues(Long assetHistoryCategoryId) {
        super.reset();
        if (assetHistoryCategoryId != null) {
            assetHistoryCategory = ENTITY_SRV.getById(AssetHistoryCategory.class, assetHistoryCategoryId);
            cbxAssetAppraisalType.setSelectedEntity(assetHistoryCategory.getAssetAppraisalType());
            txtDescEn.setValue(assetHistoryCategory.getDescEn());
            txtDesc.setValue(assetHistoryCategory.getDesc());
            if(assetHistoryCategory.getSortIndex() != null){
                txtSortIndex.setValue(assetHistoryCategory.getSortIndex().toString());
            }
            cbActive.setValue(assetHistoryCategory.getStatusRecord() == EStatusRecord.ACTIV);
        }
    }

    @Override
    public void reset() {
        super.reset();
        assetHistoryCategory = new AssetHistoryCategory();
        cbxAssetAppraisalType.setSelectedEntity(null);
        txtDescEn.setValue("");
        txtDesc.setValue("");
        txtSortIndex.setValue("");
        cbActive.setValue(true);
        markAsDirty();
    }

    @Override
    protected boolean validate() {
        checkMandatoryField(txtDescEn, "desc.en");
        return errors.isEmpty();
    }

    public List<AssetHistoryCategory> getAssetAppraisalCategory(Long id){
        BaseRestrictions<AssetHistoryCategory> restrictions = new BaseRestrictions<>(AssetHistoryCategory.class);
        restrictions.addCriterion(Restrictions.eq("assetAppraisalType.id" , id));
        List<AssetHistoryCategory> lsHistoryCategory = ENTITY_SRV.list(restrictions);
        return lsHistoryCategory;
    }
}
