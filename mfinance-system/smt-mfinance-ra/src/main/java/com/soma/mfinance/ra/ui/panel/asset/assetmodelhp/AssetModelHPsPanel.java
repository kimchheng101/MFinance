package com.soma.mfinance.ra.ui.panel.asset.assetmodelhp;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * 
 * @author vi.sok
 * @since 18/03/2017
 *
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(AssetModelHPsPanel.NAME)
public class AssetModelHPsPanel extends AbstractTabsheetPanel implements View {

	private static final long serialVersionUID = -3937984387542289828L;

	public static final String NAME = "asset.model.hp";
	
	@Autowired
	private AssetModelHPTablePanel assetModelHPTablePanel;
	@Autowired
	private AssetModelHPFormPanel assetModelHPFormPanel;
	
	@PostConstruct
	public void PostConstruct() {
		super.init();
		assetModelHPTablePanel.setMainPanel(this);
		assetModelHPFormPanel.setCaption(I18N.message("asset.model.hp"));
		getTabSheet().setTablePanel(assetModelHPTablePanel);
	}

	@Override
	public void enter(ViewChangeEvent event) {
	}
	
	
	@Override
	public void onAddEventClick() {
		assetModelHPFormPanel.reset();
		getTabSheet().addFormPanel(assetModelHPFormPanel);
		getTabSheet().setSelectedTab(assetModelHPFormPanel);
	}

	@Override
	public void onEditEventClick() {
		getTabSheet().addFormPanel(assetModelHPFormPanel);
		initSelectedTab(assetModelHPFormPanel);
	}
	
	@Override
	public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
		if (selectedTab == assetModelHPFormPanel) {
			assetModelHPFormPanel.assignValues(assetModelHPTablePanel.getItemSelectedId());
		} else if (selectedTab == assetModelHPTablePanel && getTabSheet().isNeedRefresh()) {
			assetModelHPTablePanel.refresh();
		}
		getTabSheet().setSelectedTab(selectedTab);
	}
}
