package com.soma.mfinance.ra.ui.panel.referential.scaleofbusiness;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * @author by kimsuor.seang  on 10/13/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(ScaleOfBusinessPanel.NAME)
public class ScaleOfBusinessPanel extends AbstractTabsheetPanel implements View {

    private static final long serialVersionUID = 2762740409276658002L;

    public static final String NAME = "scale.business";

    @Autowired
    private ScaleOfBusinessFormPanel scaleOfBusinessFormPanel;
    @Autowired
    //private ScaleOfBusinessTablePanel scaleOfBusinessTablePanel;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        //scaleOfBusinessTablePanel.setMainPanel(this);
        scaleOfBusinessFormPanel.setCaption(I18N.message("scale.business"));
        //getTabSheet().setTablePanel(scaleOfBusinessTablePanel);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }


    @Override
    public void onAddEventClick() {
        scaleOfBusinessFormPanel.reset();
        getTabSheet().addFormPanel(scaleOfBusinessFormPanel);
        getTabSheet().setSelectedTab(scaleOfBusinessFormPanel);
    }

    @Override
    public void onEditEventClick() {
        getTabSheet().addFormPanel(scaleOfBusinessFormPanel);
        initSelectedTab(scaleOfBusinessFormPanel);
    }

    @Override
    public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
       /* if (selectedTab == scaleOfBusinessFormPanel) {
            scaleOfBusinessFormPanel.assignValues(scaleOfBusinessTablePanel.getItemSelectedId());
        } else if (selectedTab == scaleOfBusinessTablePanel && getTabSheet().isNeedRefresh()) {
            scaleOfBusinessTablePanel.refresh();
        }*/
        getTabSheet().setSelectedTab(selectedTab);
    }
}
