package com.soma.mfinance.ra.ui.panel.referential.whosejob;


import com.soma.mfinance.core.shared.asset.AssetEntityField;
import com.soma.mfinance.core.shared.system.WhoseJob;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author by kimsuor.seang  on 10/14/2017.
 */

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WhoseJobTablePanel extends AbstractTablePanel<WhoseJob> implements AssetEntityField {

	private static final long serialVersionUID = -5762740515902293311L;

	@PostConstruct
	public void PostConstruct() {
		setCaption(I18N.message("whose.job"));
		setSizeFull();
		setMargin(true);
		setSpacing(true);
				
		super.init(I18N.message("whose.job"));
		
		addDefaultNavigation();
	}	

	@Override
	protected PagedDataProvider<WhoseJob> createPagedDataProvider() {
		PagedDefinition<WhoseJob> pagedDefinition = new PagedDefinition<WhoseJob>(searchPanel.getRestrictions());
		pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition(DESC_EN, I18N.message("desc.en"), String.class, Align.LEFT, 200);
		pagedDefinition.addColumnDefinition(DESC, I18N.message("desc"), String.class, Align.LEFT, 200);
		
		EntityPagedDataProvider<WhoseJob> pagedDataProvider = new EntityPagedDataProvider<WhoseJob>();
		pagedDataProvider.setPagedDefinition(pagedDefinition);
		return pagedDataProvider;
	}

	@Override
	protected WhoseJob getEntity() {
		final Long id = getItemSelectedId();
		if (id != null) {
		    return ENTITY_SRV.getById(WhoseJob.class, id);
		}
		return null;
	}
	
	@Override
	protected WhoseJobSearchPanel createSearchPanel() {
		return new WhoseJobSearchPanel(this);		
	}
}
