package com.soma.mfinance.ra.ui.panel.interestrate;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * Created by b.chea on 2/22/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(InterestRatePanel.NAME)
public class InterestRatePanel extends AbstractTabsheetPanel implements View {

    public static final String NAME = "interest.rates";

    @Autowired
    private InterestRateTablePanel interestRateTablePanel;
    @Autowired
    private InterestRateFormPanel interestRateFormPanel;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        interestRateTablePanel.setMainPanel(this);
        interestRateFormPanel.setCaption(I18N.message("interest.rate"));
        getTabSheet().setTablePanel(interestRateTablePanel);
    }

    /**
     * @see com.vaadin.navigator.View#enter(com.vaadin.navigator.ViewChangeListener.ViewChangeEvent)
     */
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel#onAddEventClick()
     */
    @Override
    public void onAddEventClick() {
        interestRateFormPanel.reset();
        getTabSheet().addFormPanel(interestRateFormPanel);
        getTabSheet().setSelectedTab(interestRateFormPanel);
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel#onEditEventClick()
     */
    @Override
    public void onEditEventClick() {
        getTabSheet().addFormPanel(interestRateFormPanel);
        initSelectedTab(interestRateFormPanel);
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel#initSelectedTab(com.vaadin.ui.Component)
     */
    @Override
    public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
        if (selectedTab == interestRateFormPanel) {
            interestRateFormPanel.assignValues(interestRateTablePanel.getItemSelectedId());
        } else if (selectedTab == interestRateTablePanel && getTabSheet().isNeedRefresh()) {
            interestRateTablePanel.refresh();
        }
        getTabSheet().setSelectedTab(selectedTab);
    }

}
