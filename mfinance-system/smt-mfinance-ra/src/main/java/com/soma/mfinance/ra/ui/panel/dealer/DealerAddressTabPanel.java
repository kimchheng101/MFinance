package com.soma.mfinance.ra.ui.panel.dealer;

import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.frmk.vaadin.ui.panel.AbstractControlPanel;

/**
 * 
 * @author kimsuor.seang
 *
 */
public class DealerAddressTabPanel extends AbstractControlPanel implements FinServicesHelper {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7780196605397106950L;
	
	private DealerAddressesPanel dealerContactAddressPanel;
	
	public DealerAddressTabPanel() {
		setMargin(true);
        dealerContactAddressPanel = new DealerAddressesPanel();
        addComponent(dealerContactAddressPanel);
	}

	/**
	 * Assign Value
	 * @param dealer
	 */
	public void assignValue(Dealer dealer) {
		if (dealer != null) {
			dealerContactAddressPanel.setDealer(dealer);
			dealerContactAddressPanel.assignValue();
		}
	}

}
