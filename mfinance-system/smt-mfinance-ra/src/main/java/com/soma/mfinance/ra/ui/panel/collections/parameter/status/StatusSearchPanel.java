package com.soma.mfinance.ra.ui.panel.collections.parameter.status;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;

import com.soma.mfinance.core.collection.model.EColResult;
import com.soma.mfinance.core.collection.model.EColType;
import com.soma.mfinance.core.shared.collection.CollectionEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;

/**
 * Status search panel in collection
 * @author kimsuor.seang
 */
public class StatusSearchPanel extends AbstractSearchPanel<EColResult> implements CollectionEntityField {

	/** */
	private static final long serialVersionUID = -5083994045612412451L;

    private ERefDataComboBox<EColType> cbxCollectionType;
    
    /**
     * 
     * @param colStatusTablePanel
     */
	public StatusSearchPanel(StatusTablePanel colStatusTablePanel) {
		super(I18N.message("search"), colStatusTablePanel);
	}
	
	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#reset()
	 */
	@Override
	protected void reset() {
		cbxCollectionType.setSelectedEntity(null);
	}

	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#createForm()
	 */
	@Override
	protected Component createForm() {	
		cbxCollectionType = new ERefDataComboBox<>(I18N.message("collection.type"), EColType.class);
		cbxCollectionType.setWidth(200, Unit.PIXELS);
		HorizontalLayout mainLayout = new HorizontalLayout();
        mainLayout.setSpacing(true);
        mainLayout.addComponent(new FormLayout(cbxCollectionType));
		return mainLayout;
	}
		
	
	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel#getRestrictions()
	 */
	@Override
	public BaseRestrictions<EColResult> getRestrictions() {		
		BaseRestrictions<EColResult> restrictions = new BaseRestrictions<>(EColResult.class);	
		if (cbxCollectionType.getSelectedEntity() != null) {
//			restrictions.addAssociation(COLLECTION_TYPE, "colTyp", JoinType.INNER_JOIN);
			restrictions.addCriterion(Restrictions.eq(COLLECTION_TYPE, cbxCollectionType.getSelectedEntity().getId()));
		}
		restrictions.addOrder(Order.desc(ID));
		return restrictions;
}

}
