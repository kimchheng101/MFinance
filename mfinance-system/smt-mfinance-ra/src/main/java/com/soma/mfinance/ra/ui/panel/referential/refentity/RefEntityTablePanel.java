package com.soma.mfinance.ra.ui.panel.referential.refentity;

import com.soma.common.app.tools.helper.AppConfigFileHelper;
import com.soma.ersys.vaadin.ui.referential.reftable.list.BaseRefTableHolderPanel;
import com.soma.ersys.vaadin.ui.referential.reftable.list.RefTableTablePanel;
import com.soma.frmk.config.model.RefTable;
import com.soma.frmk.security.context.SecApplicationContextHolder;
import com.soma.frmk.vaadin.ui.widget.table.EntityColumnRenderer;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedTable;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.data.Container;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.NativeButton;
import org.apache.commons.lang.StringUtils;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


/**
 * Created by ki.kao on 6/10/2017.
 */
@Component
@Scope("prototype")
public class RefEntityTablePanel extends RefTableTablePanel {

    @PostConstruct
    public void PostConstruct() {
        this.setCaption(I18N.message("ref.entity"));
        this.setSizeFull();
        this.setMargin(true);
        this.setSpacing(true);
        super.init(this.getCaption());
        NavigationPanel navigationPanel = this.addNavigationPanel();
        navigationPanel.addEditClickListener(this);
        NativeButton btnViewData = new NativeButton(I18N.message(I18N.message("view.data")), new Button.ClickListener() {
            private static final long serialVersionUID = 8037669867834969655L;

            public void buttonClick(Button.ClickEvent event) {
                RefEntityTablePanel.this.mainPanel.getTabSheet().setAdd(false);
                ((RefEntityHolderPanel) RefEntityTablePanel.this.mainPanel).onViewSubTab();
            }
        });
        if (AppConfigFileHelper.isFontAwesomeIcon()) {
            btnViewData.setIcon(FontAwesome.TABLE);
        } else {
            btnViewData.setIcon(new ThemeResource("../smt-default/icons/16/view.png"));
        }

        navigationPanel.addButton(btnViewData);
        navigationPanel.addRefreshClickListener(this);
        this.pagedTable.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            private static final long serialVersionUID = -6816111694043069470L;

            public void itemClick(ItemClickEvent event) {
                RefEntityTablePanel.this.selectedItem = event.getItem();
                boolean isDoubleClick = event.isDoubleClick() || SecApplicationContextHolder.getContext().clientDeviceIsMobileOrTablet();
                if (isDoubleClick) {
                    RefEntityTablePanel.this.mainPanel.getTabSheet().setAdd(false);
                    ((RefEntityHolderPanel) RefEntityTablePanel.this.mainPanel).onViewSubTab();
                }

            }
        });
    }


    @Override
    protected RefEntitySearchPanel createSearchPanel() {
        return new RefEntitySearchPanel(this);
    }

}
