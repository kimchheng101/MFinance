package com.soma.mfinance.ra.ui.panel.referential.businesstypes;


import com.soma.mfinance.core.shared.asset.AssetEntityField;
import com.soma.mfinance.core.shared.system.BusinessTypes;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author by kimsuor.seang  on 10/18/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BusinessTypesTablePanel extends AbstractTablePanel<BusinessTypes> implements AssetEntityField {

	private static final long serialVersionUID = -5762740146067891225L;

	@PostConstruct
	public void PostConstruct() {
		setCaption(I18N.message("business.types"));
		setSizeFull();
		setMargin(true);
		setSpacing(true);
				
		super.init(I18N.message("business.types"));
		
		addDefaultNavigation();
	}	

	@Override
	protected PagedDataProvider<BusinessTypes> createPagedDataProvider() {
		PagedDefinition<BusinessTypes> pagedDefinition = new PagedDefinition<BusinessTypes>(searchPanel.getRestrictions());
		pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition(CODE, I18N.message("code"), String.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition(DESC_EN, I18N.message("desc.en"), String.class, Align.LEFT, 200);
		pagedDefinition.addColumnDefinition(DESC, I18N.message("desc"), String.class, Align.LEFT, 200);
		
		EntityPagedDataProvider<BusinessTypes> pagedDataProvider = new EntityPagedDataProvider<BusinessTypes>();
		pagedDataProvider.setPagedDefinition(pagedDefinition);
		return pagedDataProvider;
	}

	@Override
	protected BusinessTypes getEntity() {
		final Long id = getItemSelectedId();
		if (id != null) {
		    return ENTITY_SRV.getById(BusinessTypes.class, id);
		}
		return null;
	}
	
	@Override
	protected BusinessTypesSearchPanel createSearchPanel() {
		return new BusinessTypesSearchPanel(this);		
	}
}
