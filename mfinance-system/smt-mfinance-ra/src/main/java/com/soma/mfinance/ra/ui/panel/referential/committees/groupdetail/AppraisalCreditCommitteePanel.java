package com.soma.mfinance.ra.ui.panel.referential.committees.groupdetail;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * @author by kimsuor.seang  on 9/27/2017.
 */

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(AppraisalCreditCommitteePanel.NAME)
public class AppraisalCreditCommitteePanel extends AbstractTabsheetPanel implements View {

	public static final String NAME = "appraisal.credit.committee.panel";

	@Autowired
	private AppraisalCreditCommitteeFormPanel creditCommitteeFormPanel;
	@Autowired
	private AppraisalCreditCommitteeTablePanel creditCommitteeTablePanel;

	@PostConstruct
	public void PostConstruct() {
		super.init();
		creditCommitteeTablePanel.setMainPanel(this);
		creditCommitteeFormPanel.setCaption(I18N.message("appraisal.credit.committee"));
		getTabSheet().setTablePanel(creditCommitteeTablePanel);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAddEventClick() {
		creditCommitteeFormPanel.reset();
		getTabSheet().addFormPanel(creditCommitteeFormPanel);
		getTabSheet().setSelectedTab(creditCommitteeFormPanel);
	}

	@Override
	public void onEditEventClick() {
		getTabSheet().addFormPanel(creditCommitteeFormPanel);
		initSelectedTab(creditCommitteeFormPanel);
	}

	@Override
	public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
		if (selectedTab == creditCommitteeFormPanel) {
			creditCommitteeFormPanel.assignValues(creditCommitteeTablePanel.getItemSelectedId());
		} else if (selectedTab == creditCommitteeTablePanel && getTabSheet().isNeedRefresh()) {
			creditCommitteeTablePanel.refresh();
		}
		getTabSheet().setSelectedTab(selectedTab);
	}

}
