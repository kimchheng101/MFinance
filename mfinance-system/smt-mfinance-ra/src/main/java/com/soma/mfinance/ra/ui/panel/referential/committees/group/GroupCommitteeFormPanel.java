package com.soma.mfinance.ra.ui.panel.referential.committees.group;

import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.custom.component.CustomEntityRefComboBox;
import com.soma.mfinance.core.financial.model.ProductLine;
import com.soma.mfinance.core.referential.committee.GroupCommittee;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.widget.SecUserComboBox;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.sun.org.apache.regexp.internal.RE;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.UI;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * @author by kimsuor.seang  on 9/27/2017.
 */

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class GroupCommitteeFormPanel extends AbstractFormPanel {

    private GroupCommittee groupCommittee;
    private SecUserComboBox cbxSecUser;
    private List<SecUser> secUsers;
    private EntityRefComboBox<ProductLine> cbxProductLine;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addSaveClickListener(this);

    }

    @Override
    protected Entity getEntity() {
		groupCommittee.setSecUser(cbxSecUser.getSelectedEntity());
		groupCommittee.setProductLine(cbxProductLine.getSelectedEntity());
		return groupCommittee;
    }

    @Override
    protected com.vaadin.ui.Component createForm() {

        final FormLayout formPanel = new FormLayout();
        Long ManagementProfileId = 4L;

        secUsers = DataReference.getInstance().getUsers(ManagementProfileId);
        cbxSecUser = new SecUserComboBox(I18N.message("user"), secUsers) {
            public void setValues(List<SecUser> users) {
                clear();
                if (users != null && !users.isEmpty()) {
                    for (SecUser user : users) {
                        addItem(user.getId().toString());
                        setItemCaption(user.getId().toString(), user.getLogin());
                        valueMap.put(user.getId().toString(), user);
                    }
                }
            }
        };
        cbxSecUser.setImmediate(true);
        cbxSecUser.setRequired(true);

        cbxProductLine = new CustomEntityRefComboBox<>();
        cbxProductLine.setCaption(I18N.message("product.line"));
        cbxProductLine.setRestrictions(new BaseRestrictions<>(ProductLine.class));
        cbxProductLine.renderer();
        cbxProductLine.setSelectedEntity(null);
        cbxProductLine.setImmediate(true);
        cbxProductLine.setRequired(true);

        formPanel.addComponent(cbxProductLine);
        formPanel.addComponent(cbxSecUser);
        return formPanel;
    }

	private List<SecUser> getListUserManagement(AssetRange assetRange){
		List<SecUser> newUserList = new ArrayList<>();
		BaseRestrictions<GroupCommittee> restrictions = new BaseRestrictions<>(GroupCommittee.class);

		if(assetRange != null){
			restrictions.addCriterion(Restrictions.eq("assetRange", assetRange));
		}
		List<GroupCommittee> committees = ENTITY_SRV.list(restrictions);
		List<SecUser> usersIncommittee = new ArrayList<>();
		for(GroupCommittee committee : committees){
			usersIncommittee.add(committee.getSecUser());
		}
		for(SecUser secUser : secUsers){
			if(!usersIncommittee.contains(secUser)){
				newUserList.add(secUser);
			}
		}
		return newUserList;
	}

    public void assignValues( Long id) {
        reset();
		if (id != null) {
			cbxSecUser.clear();
			List<SecUser> users = getListUserManagement(null);
			groupCommittee = ENTITY_SRV.getById(GroupCommittee.class, id);
			cbxProductLine.setSelectedEntity(groupCommittee.getProductLine());

			if(!users.contains(groupCommittee.getSecUser())){
				users.add(groupCommittee.getSecUser());
			}
			cbxSecUser.setValues(users);
			cbxSecUser.setSelectedEntity(groupCommittee.getSecUser());
		}
    }

    @Override
    public void reset() {
        super.reset();
		groupCommittee = new GroupCommittee();
		cbxSecUser.clear();
		cbxSecUser.setValues(secUsers);
		cbxProductLine.setSelectedEntity(null);
        markAsDirty();
    }

    @Override
    protected boolean validate() {

		if (isExistCommittee()){
            errors.add(com.soma.frmk.vaadin.util.i18n.I18N.message("Committee already exist !"));
        }
        checkMandatorySelectField(cbxSecUser, "user");
        checkMandatorySelectField(cbxProductLine, "product.line");
        return errors.isEmpty();
    }

    public boolean isExistCommittee(){
        BaseRestrictions<GroupCommittee> restrictions = new BaseRestrictions<>(GroupCommittee.class);
        List<GroupCommittee> list = ENTITY_SRV.list(restrictions);
        if (list != null || !list.isEmpty()){
            for (GroupCommittee groupCommittee : list){
                if (groupCommittee.getSecUser().getDesc().equals(cbxSecUser.getSelectedEntity().getDesc())){
                   return true;
                }
            }
        }
        return false;
    }

}
