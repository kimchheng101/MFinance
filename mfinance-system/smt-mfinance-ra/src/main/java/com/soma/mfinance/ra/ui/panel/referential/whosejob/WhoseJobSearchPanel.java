package com.soma.mfinance.ra.ui.panel.referential.whosejob;


import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.mfinance.core.shared.system.WhoseJob;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by kimsuor.seang  on 10/14/2017.
 */
public class WhoseJobSearchPanel extends AbstractSearchPanel<WhoseJob> implements FMEntityField {
	
	private static final long serialVersionUID = -1762740473450616843L;
	private TextField txtDescEn;
	private TextField txtDesc;
	
	public WhoseJobSearchPanel(WhoseJobTablePanel whoseJobTablePanel) {
		super(I18N.message("Search"), whoseJobTablePanel);
	}
	
	@Override
	protected void reset() {
		txtDescEn.setValue("");
		txtDesc.setValue("");
	}


	@Override
	protected Component createForm() {
		final GridLayout gridLayout = new GridLayout(4, 1);
		txtDescEn = ComponentFactory.getTextField("desc.en", false, 60, 200);
		txtDesc = ComponentFactory.getTextField("desc", false, 60, 200);
        gridLayout.setSpacing(true);
        gridLayout.addComponent(new FormLayout(txtDescEn), 1, 0);
        gridLayout.addComponent(new FormLayout(txtDesc), 2, 0);
        
		return gridLayout;
	}
	
	@Override
	public BaseRestrictions<WhoseJob> getRestrictions() {
		BaseRestrictions<WhoseJob> restrictions = new BaseRestrictions<WhoseJob>(WhoseJob.class);
		List<Criterion> criterions = new ArrayList<Criterion>();
		if (StringUtils.isNotEmpty(txtDescEn.getValue())) { 
			criterions.add(Restrictions.like(DESC_EN, txtDescEn.getValue(), MatchMode.ANYWHERE));
		}
		restrictions.setCriterions(criterions);
		restrictions.addOrder(Order.asc(DESC_EN));
		return restrictions;
	}

}
