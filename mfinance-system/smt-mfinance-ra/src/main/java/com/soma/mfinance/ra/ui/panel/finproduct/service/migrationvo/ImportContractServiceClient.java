

package com.soma.mfinance.ra.ui.panel.finproduct.service.migrationvo;

import org.seuksa.frmk.service.BaseEntityService;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface ImportContractServiceClient extends BaseEntityService {

    String CONTRACT_MIGRATION_DIR = "contract_migration";

    /**
     *
     * @param selectedItemIds
     * @return
     */
    String migrateQuotation(Set<Long> selectedItemIds);

    /**
     *
     * @return
     */
    void excecuteJob();

    /**
     *
     * @return
     */
    List<String> getLogFileNames();

    /**
     *
     * @param date1
     * @param date2
     * @return
     */
    int getNbMonthOfTwoDates(Date date1, Date date2);

    /**
     * Generate Insurance Start Date
     */
    void generateInsuranceStartDate();

    /**
     *
     * @param insuranceStartDate
     * @param contractStartDate
     * @param term
     * @return
     */
    int getRenewalTerm(Date insuranceStartDate, Date contractStartDate, Integer term);

}
