package com.soma.mfinance.ra.ui.panel.referential.whosejob;


import com.soma.mfinance.core.shared.system.WhoseJob;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author by kimsuor.seang  on 10/14/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WhoseJobFormPanel extends AbstractFormPanel {

	private static final long serialVersionUID = 376274000596870581L;

	private WhoseJob whoseJob;
	private TextField txtDesc;
    private TextField txtDescEn;
    
    @PostConstruct
	public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
		navigationPanel.addSaveClickListener(this);
	}
	
	@Override
	protected Entity getEntity() {
		whoseJob.setDesc(txtDesc.getValue());
		whoseJob.setDescEn(txtDescEn.getValue());
		return whoseJob;
	}

	@Override
	protected com.vaadin.ui.Component createForm() {
		final FormLayout formPanel = new FormLayout();
		txtDescEn = ComponentFactory.getTextField("desc.en", true, 60, 200);
		txtDesc = ComponentFactory.getTextField35("desc", false, 60, 200);
        
        formPanel.addComponent(txtDescEn);
        formPanel.addComponent(txtDesc);
		return formPanel;
	}
	

	public void assignValues(Long appTypeId) {
		super.reset();
		if (appTypeId != null) {
			whoseJob = ENTITY_SRV.getById(WhoseJob.class, appTypeId);
			txtDescEn.setValue(whoseJob.getDescEn());
			txtDesc.setValue(whoseJob.getDesc());
		}
	}

	@Override
	public void reset() {
		super.reset();
		whoseJob = new WhoseJob();
		txtDescEn.setValue("");
		txtDesc.setValue("");
		markAsDirty();
	}
	

	@Override
	protected boolean validate() {
		checkMandatoryField(txtDescEn, "desc.en");
		return errors.isEmpty();
	}
}
