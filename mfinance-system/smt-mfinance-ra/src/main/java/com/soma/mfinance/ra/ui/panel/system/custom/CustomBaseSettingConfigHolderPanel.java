package com.soma.mfinance.ra.ui.panel.system.custom;

import com.soma.ersys.vaadin.ui.referential.setting.detail.SettingConfigFormPanel;
import com.soma.ersys.vaadin.ui.referential.setting.list.BaseSettingConfigHolderPanel;
import com.soma.frmk.config.model.SettingConfig;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import java.lang.reflect.Field;

/**
 * @author by kimsuor.seang  on 11/7/2017.
 */
@Component
@Scope("prototype")
@VaadinView("settings.list.custom")
public class CustomBaseSettingConfigHolderPanel extends BaseSettingConfigHolderPanel {


    public CustomBaseSettingConfigHolderPanel() {
        super();
    }


    @Override
    public void onAddEventClick() {
        super.onAddEventClick();
        try {
            Field field = getClass().getSuperclass().getDeclaredField("settingFormPanel");
            if (field != null) {
                field.setAccessible(true);
                SettingConfigFormPanel settingConfigFormPanel = (SettingConfigFormPanel) field.get(this);
                settingConfigFormPanel.reset();
                field = settingConfigFormPanel.getClass().getDeclaredField("settingConfig");
                field.setAccessible(true);
                SettingConfig settingConfig = (SettingConfig) field.get(settingConfigFormPanel);
                settingConfig.setReadOnly(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
