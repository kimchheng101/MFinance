package com.soma.mfinance.ra.ui.panel.system;

import com.soma.common.app.menu.model.MenuItemEntity;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.security.model.SecControl;
import com.soma.frmk.security.model.SecControlPrivilege;
import com.soma.frmk.security.model.SecControlProfilePrivilege;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.vaadin.dialogs.ConfirmDialog;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by ki.kao on 2/13/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MenuItemTablePanel extends AbstractTablePanel<MenuItemEntity> implements FMEntityField {

    @Autowired
    private MenuItemServiceUtil menuItemServiceUtil;

    @PostConstruct
    public void PostConstruct() {
        setCaption(I18N.message("menus"));
        setSizeFull();
        setMargin(true);
        setSpacing(true);
        super.init(I18N.message("menus"));
        addDefaultNavigation();
    }

    public Long getItemSelectedId() {
        if (selectedItem != null) {
            return (Long) selectedItem.getItemProperty(ID).getValue();
        }
        return null;
    }

    @Override
    protected MenuItemEntity getEntity() {
        final Long id = getItemSelectedId();
        if (id != null) {
            return ENTITY_SRV.getById(MenuItemEntity.class, id);
        }
        return null;
    }

    @Override
    protected PagedDataProvider<MenuItemEntity> createPagedDataProvider() {
        PagedDefinition<MenuItemEntity> pagedDefinition = new PagedDefinition<>(searchPanel.getRestrictions());
        pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Table.Align.LEFT, 100);
        pagedDefinition.addColumnDefinition(CODE, I18N.message("code"), String.class, Table.Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("action", I18N.message("menu.action"), String.class, Table.Align.LEFT, 200);
        pagedDefinition.addColumnDefinition(DESC, I18N.message("desc"), String.class, Table.Align.LEFT, 200);
        pagedDefinition.addColumnDefinition("menu.id", "Application Menu Id", Long.class, Table.Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("parent.id", "Parent Menu Item Id", Long.class, Table.Align.LEFT, 200);
        EntityPagedDataProvider<MenuItemEntity> pagedDataProvider = new EntityPagedDataProvider<>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    @Override
    protected AbstractSearchPanel<MenuItemEntity> createSearchPanel() {
        return new MenuItemSearchPanel(I18N.message("menus.search"), this);
    }

    @Override
    public void deleteButtonClick(Button.ClickEvent event) {
        final MenuItemEntity selectedEntity = this.getEntity();
        if (selectedEntity == null) {
            MessageBox mb = new MessageBox(UI.getCurrent(), "280px", "150px", com.soma.frmk.vaadin.util.i18n.I18N.message("information"), MessageBox.Icon.WARN, com.soma.frmk.vaadin.util.i18n.I18N.message("msg.info.delete.item.not.selected"), Alignment.MIDDLE_RIGHT, new MessageBox.ButtonConfig[]{new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, com.soma.frmk.vaadin.util.i18n.I18N.message("ok"))});
            mb.show();
        } else {
            ConfirmDialog.show(UI.getCurrent(), com.soma.frmk.vaadin.util.i18n.I18N.message("msg.ask.delete", new String[]{String.valueOf(selectedEntity.getId())}), new ConfirmDialog.Listener() {
                private static final long serialVersionUID = -8825432459387761406L;

                public void onClose(ConfirmDialog dialog) {
                    if (dialog.isConfirmed()) {
                        try {
                            BaseRestrictions<SecControlProfilePrivilege> restrictions1 = new BaseRestrictions<>(SecControlProfilePrivilege.class);
                            restrictions1.addCriterion(Restrictions.eq("control.id", selectedEntity.getControl().getId()));
                            for (SecControlProfilePrivilege obj : ENTITY_SRV.list(restrictions1))
                                MenuItemTablePanel.this.deleteEntity(obj);
                            BaseRestrictions<SecControlPrivilege> restrictions2 = new BaseRestrictions<SecControlPrivilege>(SecControlPrivilege.class);
                            restrictions2.addCriterion(Restrictions.eq("control.id", selectedEntity.getControl().getId()));
                            for (SecControlPrivilege obj : ENTITY_SRV.list(restrictions2))
                                MenuItemTablePanel.this.deleteEntity(obj);
                            menuItemServiceUtil.deleteSubMenu(selectedEntity, selectedEntity.getControl());
                            MenuItemTablePanel.this.deleteEntity(selectedEntity);
                            menuItemServiceUtil.clearSession();
                            MenuItemTablePanel.this.deleteEntity(selectedEntity.getControl());
                            MenuItemTablePanel.this.selectedItem = null;
                            MenuItemTablePanel.this.refresh();
                        } catch (Exception var4) {
                            MenuItemTablePanel.this.logger.error(var4.getMessage(), var4);
                            MessageBox mb;
                            if (var4 instanceof DataIntegrityViolationException) {
                                mb = new MessageBox(UI.getCurrent(), com.soma.frmk.vaadin.util.i18n.I18N.message("warning"), MessageBox.Icon.WARN, com.soma.frmk.vaadin.util.i18n.I18N.message("msg.warning.delete.selected.item.is.used"), new MessageBox.ButtonConfig[]{new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, com.soma.frmk.vaadin.util.i18n.I18N.message("ok"))});
                                mb.setWidth("300px");
                                mb.setHeight("160px");
                                mb.show();
                            } else {
                                mb = new MessageBox(UI.getCurrent(), com.soma.frmk.vaadin.util.i18n.I18N.message("error"), MessageBox.Icon.ERROR, com.soma.frmk.vaadin.util.i18n.I18N.message("msg.error.technical"), new MessageBox.ButtonConfig[]{new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, com.soma.frmk.vaadin.util.i18n.I18N.message("ok"))});
                                mb.setWidth("300px");
                                mb.setHeight("160px");
                                mb.show();
                            }
                        }
                    }

                }
            });
        }

    }

}
