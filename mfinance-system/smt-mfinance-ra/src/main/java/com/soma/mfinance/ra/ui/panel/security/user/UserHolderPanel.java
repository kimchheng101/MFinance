package com.soma.mfinance.ra.ui.panel.security.user;

/**
 * UserHolderPanel
 *
 * @author phirun.kong
 */
/*@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(UserHolderPanel.NAME)
public class UserHolderPanel extends SecUserHolderPanel {

    private static final long serialVersionUID = -7830714722263393940L;

    public static final String NAME = "users";

    private final CustomSecUserFormPanel userFormPanel=new CustomSecUserFormPanel();

    public UserHolderPanel() {
        super();
        //FormLayout formLayout= (FormLayout) userFormPanel.getComponent(0);
    }

    @PostConstruct
    public void PostConstruct() {
        super.init();
        this.userTablePanel.setMainPanel(this);
        this.userFormPanel.setCaption(I18N.message("user"));
        this.userFormPanel.setMainPanel(this);
        this.getTabSheet().setTablePanel(this.userTablePanel);
//		userBackupTablePanel.setCaption(I18N.message("user.backup"));
    }

    @Override
    public void onEditEventClick() {
//		userBackupTablePanel.assignValuesToControls(userTablePanel.getItemSelectedId());
        getTabSheet().addFormPanel(userFormPanel);
        initSelectedTab(userFormPanel);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        super.enter(event);
    }

    @Override
    public void onAddEventClick() {
        this.userFormPanel.reset();
        this.getTabSheet().addFormPanel(this.userFormPanel);
        this.getTabSheet().setSelectedTab(this.userFormPanel);
    }

    @Override
    public void addSubTab(Long selectedId) {
        super.addSubTab(selectedId);
    }

    @Override
    public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
        if (selectedTab == this.userFormPanel) {
            this.userFormPanel.assignValues(this.userTablePanel.getItemSelectedId());
        } else if (selectedTab == this.userTablePanel && this.getTabSheet().isNeedRefresh()) {
            this.userTablePanel.refresh();
        }

        this.getTabSheet().setSelectedTab(selectedTab);
    }
}*/
