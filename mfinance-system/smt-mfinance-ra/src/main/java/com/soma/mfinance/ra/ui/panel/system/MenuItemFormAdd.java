package com.soma.mfinance.ra.ui.panel.system;

import com.soma.common.app.menu.model.MenuItemEntity;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;

/**
 * Created by ki.kao on 2/13/2017.
 */
@org.springframework.stereotype.Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MenuItemFormAdd extends AbstractMenuItemForm {

    @Autowired
    private MenuItemServiceUtil menuItemServiceUtil;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addSaveClickListener(this);
    }

    @Override
    protected Component createForm() {
        return initializeComponents();
    }

    @Override
    public void saveButtonClick(Button.ClickEvent event) {
        try {
            super.clearErrorPanel();
            if (super.validate()) {
                MenuItemEntity entity = (MenuItemEntity) super.getEntity();
                menuItemServiceUtil.saveOrUpdateMenuItem(entity, null);
                super.reset();
                this.displaySuccess();
            } else {
                this.displayErrors();
                processAfterError();
            }
        } catch (Exception e) {
            e.printStackTrace();
            ENTITY_SRV.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally {
            menuItemServiceUtil.clearSession();
        }
    }
}
