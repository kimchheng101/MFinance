package com.soma.mfinance.ra.ui.panel.contactevidence;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * @author kimsuor.seang
 *
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(ContactVerificationsPanel.NAME)
public class ContactVerificationsPanel extends AbstractTabsheetPanel implements View {

	private static final long serialVersionUID = -8044899957098000880L;

	public static final String NAME = "contact.verification";
	
	@Autowired
	private ContactVerificationTablePanel contactVerificationTablePanel;
	@Autowired
	private ContactVerificationFormPanel contactVerificationFormPanel;
	
	@PostConstruct
	public void PostConstruct() {
		super.init();
		contactVerificationTablePanel.setMainPanel(this);
		contactVerificationFormPanel.setCaption(I18N.message("contact.verification"));
		getTabSheet().setTablePanel(contactVerificationTablePanel);
	}

	@Override
	public void enter(ViewChangeEvent event) {
	}
	
	
	@Override
	public void onAddEventClick() {
		contactVerificationFormPanel.reset();
		getTabSheet().addFormPanel(contactVerificationFormPanel);
		getTabSheet().setSelectedTab(contactVerificationFormPanel);
	}

	@Override
	public void onEditEventClick() {
		getTabSheet().addFormPanel(contactVerificationFormPanel);
		initSelectedTab(contactVerificationFormPanel);
	}
	
	@Override
	public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
		if (selectedTab == contactVerificationFormPanel) {
			contactVerificationFormPanel.assignValues(contactVerificationTablePanel.getItemSelectedId());
		} else if (selectedTab == contactVerificationTablePanel && getTabSheet().isNeedRefresh()) {
			contactVerificationTablePanel.refresh();
		}
		getTabSheet().setSelectedTab(selectedTab);
	}
}
