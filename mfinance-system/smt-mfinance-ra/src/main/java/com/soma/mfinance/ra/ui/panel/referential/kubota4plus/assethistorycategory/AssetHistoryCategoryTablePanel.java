package com.soma.mfinance.ra.ui.panel.referential.kubota4plus.assethistorycategory;

import com.soma.mfinance.core.asset.model.appraisal.AssetHistoryCategory;
import com.soma.mfinance.core.shared.asset.AssetEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.ui.Table;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author by kimsuor.seang  on 10/9/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AssetHistoryCategoryTablePanel extends AbstractTablePanel<AssetHistoryCategory> implements AssetEntityField {
    @PostConstruct
    public void PostConstruct() {
        setCaption(I18N.message("asset.history.category"));
        setSizeFull();
        setMargin(true);
        setSpacing(true);

        super.init(I18N.message("asset.history.category"));

        addDefaultNavigation();
    }

    @Override
    protected PagedDataProvider<AssetHistoryCategory> createPagedDataProvider() {
        PagedDefinition<AssetHistoryCategory> pagedDefinition = new PagedDefinition<>(searchPanel.getRestrictions());
        pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Table.Align.LEFT, 100);
        pagedDefinition.addColumnDefinition(DESC_EN, I18N.message("desc.en"), String.class, Table.Align.LEFT, 200);
        pagedDefinition.addColumnDefinition(DESC, I18N.message("desc"), String.class, Table.Align.LEFT, 200);

        EntityPagedDataProvider<AssetHistoryCategory> pagedDataProvider = new EntityPagedDataProvider<>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    @Override
    protected AssetHistoryCategory getEntity() {
        final Long id = getItemSelectedId();
        if (id != null) {
            return ENTITY_SRV.getById(AssetHistoryCategory.class, id);
        }
        return null;
    }

    @Override
    protected AssetHistoryCategorySearchPanel createSearchPanel() {
        return new AssetHistoryCategorySearchPanel(this);
    }

}
