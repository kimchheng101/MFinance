package com.soma.mfinance.ra.ui.panel.referential.employment.occupation;

import com.soma.mfinance.core.application.model.EmploymentOccupation;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by Pisethraingsey SOUN
 * Date     : 23/06/2017, 8:57 AM
 * Email    : p.suon@gl-f.com
 */
@Component
@Scope("prototype")
public class EmploymentOccupationTablePanel extends AbstractTablePanel<EmploymentOccupation> implements FMEntityField {
    private static final long serialVersionUID = 1565249715684903010L;

    public EmploymentOccupationTablePanel() {
    }

    @PostConstruct
    public void PostConstruct() {
        super.init(I18N.message("occupation"));
        this.setCaption(I18N.message("occupation"));
        this.addDefaultNavigation();
    }

    @Override
    protected EmploymentOccupation getEntity() {
        Long id = this.getItemSelectedId();
        return id != null ? ENTITY_SRV.getById(EmploymentOccupation.class, id) : null;
    }

    @Override
    protected PagedDataProvider<EmploymentOccupation> createPagedDataProvider() {
        PagedDefinition<EmploymentOccupation> pagedDefinition = new PagedDefinition(this.searchPanel.getRestrictions());
        pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Table.Align.LEFT, 70);
        pagedDefinition.addColumnDefinition(CODE, I18N.message("code"), String.class, Table.Align.LEFT, 100);
        pagedDefinition.addColumnDefinition(DESC, I18N.message("desc"), String.class, Table.Align.LEFT, 250);
        pagedDefinition.addColumnDefinition(DESC_EN, I18N.message("desc.en"), String.class, Table.Align.LEFT, 250);
        pagedDefinition.addColumnDefinition(EMPLOYMENT_INDUSTRY+ "." + DESC_EN, I18N.message("employment.industry"), String.class, Table.Align.LEFT, 200);
        EntityPagedDataProvider<EmploymentOccupation> pagedDataProvider = new EntityPagedDataProvider();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    @Override
    protected AbstractSearchPanel<EmploymentOccupation> createSearchPanel() {
        return new EmploymentOccupationSearchPanel(this);
    }

    @Override
    protected Panel createBeforeTablePanel() {
        VerticalLayout mainVerLayout = new VerticalLayout();
        Panel mainPanel = new Panel();
        mainPanel.setStyleName(Reindeer.PANEL_LIGHT);
        mainPanel.setContent(mainVerLayout);
        return mainPanel;
    }


}
