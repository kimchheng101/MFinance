package com.soma.mfinance.ra.ui.panel.system.tab;

import com.soma.frmk.security.model.SecProfile;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.util.VaadinServicesHelper;
import com.vaadin.data.Property;
import com.vaadin.event.LayoutEvents;
import com.vaadin.ui.*;
import org.seuksa.frmk.i18n.I18N;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by k.seang on 3/3/2017.
 */
public class ProfileGroupLayout extends VerticalLayout implements VaadinServicesHelper {
    private static final long serialVersionUID = -7491146056264783660L;

    private VerticalLayout profileLayout;
    private List<CheckBox> listCbProfiles;
    private Map<Long,Object[]> mapProfiles;
    private Map<Long,Property.ValueChangeListener> mapProfilesValueChange;
    private List<SecProfile> selectedProfile ;



    public ProfileGroupLayout() {
        this.initRows();
    }

    protected void initRows() {
        VerticalLayout rowLayouts = new VerticalLayout();
        mapProfiles = new HashMap<Long, Object[]>();
        listCbProfiles = new ArrayList<CheckBox>();
        List<SecProfile> secProfiles = SCHEDULER_SRV.list(SecProfile.class);
        profileLayout = new VerticalLayout();
        profileLayout.setEnabled(true);

        Label lblComponentHeader = ComponentFactory.getHtmlLabel("<b>" + I18N.message("profile") + "</b>");
        lblComponentHeader.setWidth(305.0F, Unit.PIXELS);
        HorizontalLayout colHeader = new HorizontalLayout(new Component[]{lblComponentHeader});
        colHeader.setStyleName("control-group-header");

        final CheckBox groupDisplay = new CheckBox("<b> Display </b>");
        groupDisplay.setCaptionAsHtml(true);
        groupDisplay.setWidth(75.0F, Unit.PIXELS);
        groupDisplay.setEnabled(true);
        colHeader.addComponent(groupDisplay);
        groupDisplay.addValueChangeListener(new Property.ValueChangeListener() {
            private static final long serialVersionUID = 2751625802238914288L;
            public void valueChange(Property.ValueChangeEvent event) {
                mapProfiles.forEach((k,v)->{
                    Object[] value = v;
                    CheckBox checkBox = (CheckBox) value[0];
                    checkBox.setValue(groupDisplay.getValue());
                    //System.out.println("Item : " + k + " Count : " + v);
                    /*if("E".equals(k)){
                        System.out.println("Hello E");
                    }*/
                });
               // for (CheckBox checkBox : listCbProfiles) {
                 //   checkBox.setValue(groupDisplay.getValue());
               // }
            }
        });

        profileLayout.addComponent(colHeader);

        for(SecProfile secProfile :secProfiles) {

            Label labelProfileDesc = ComponentFactory.getHtmlLabel(secProfile.getDesc() );
            labelProfileDesc.setWidth(305.0F, Unit.PIXELS);
            VerticalLayout verticalLayout = new VerticalLayout();
            verticalLayout.addComponent(labelProfileDesc);
            HorizontalLayout colRow = new HorizontalLayout(new Component[]{verticalLayout});
            colRow.setStyleName("control-group-row");
            final CheckBox cbProfile = new CheckBox();
            cbProfile.setValue(Boolean.valueOf(false));
            cbProfile.setWidth(75.0F, Unit.PIXELS);
            groupDisplay.setEnabled(true);

            VerticalLayout cellLayout = new VerticalLayout();
            cellLayout.addLayoutClickListener(new LayoutEvents.LayoutClickListener() {
                private static final long serialVersionUID = 3114904998419435624L;
                public void layoutClick(LayoutEvents.LayoutClickEvent event) {
                    //cbProfile.setValue(Boolean.valueOf(!((Boolean)cbProfile.getValue()).booleanValue()));
                    //groupDisplay.setValue(!cbProfile.getValue());
                    //curCheckBoxGroupVO.setValue(secProfile.getId(), ((Boolean)cbProfile.getValue()).booleanValue(), true);
                   // System.out.println("Combobox " + );
                    //cbProfile.setValue(Boolean.valueOf(!((Boolean)cbProfile.getValue()).booleanValue()));

                    //Object[] values;
                    //values =  new Object[]{cbProfile, secProfile};
                    //mapProfiles.put(secProfile.getId(),values);

                }
            });


            cellLayout.addComponent(cbProfile);
            colRow.addComponent(cellLayout);
            //listCbProfiles.add(cbProfile);

            Object[] values;
            values =  new Object[]{cbProfile, secProfile};
            mapProfiles.put(secProfile.getId(),values);

            profileLayout.addComponent(colRow);
        }
        rowLayouts.addComponent(profileLayout);
        this.addComponent(new Panel(I18N.message("profile"), rowLayouts));
    }
    public void clearValues() {
        if(this.mapProfiles != null && !this.mapProfiles.isEmpty()) {
            mapProfiles.forEach((k,v)->{
                Object[] value = v;
                CheckBox checkBox = (CheckBox) value[0];
                checkBox.setValue(false);

            });
        }

    }

    public boolean validate() {
        boolean isValid = false;
      /*  if(this.appLayouts != null && !this.appLayouts.isEmpty()) {
            Iterator i$ = this.appLayouts.values().iterator();

            while(i$.hasNext()) {
                SecApplicationLayout appGroup = (SecApplicationLayout)i$.next();
                if(appGroup.isChecked()) {
                    isValid = true;
                    break;
                }
            }
        }*/

        return isValid;
    }

    public String getRequireMsg() {
        return I18N.message("field.required.1", I18N.message("application"));
    }

    public List<SecProfile> getSelectedProfile() {
        if(selectedProfile == null) {
            selectedProfile = new ArrayList<>();
        }
        if(this.mapProfiles != null && !this.mapProfiles.isEmpty()) {
            mapProfiles.forEach((k,v)->{
                Object[] value = v;
                CheckBox checkBox = (CheckBox) value[0];
                if(Boolean.valueOf(checkBox.getValue().booleanValue())) {
                    selectedProfile.add((SecProfile) value[1]);
                }

            });
        }
        return selectedProfile;
    }

    public void setSelectedProfile(List<SecProfile> selectedProfile) {
        for (SecProfile secProfile : selectedProfile) {
            if (mapProfiles.containsKey(secProfile.getId())) {
                Object[] value = mapProfiles.get(secProfile.getId());
                CheckBox checkBox = (CheckBox) value[0];
                checkBox.setValue(true);

            }
        }
        this.selectedProfile = selectedProfile;
    }

}
