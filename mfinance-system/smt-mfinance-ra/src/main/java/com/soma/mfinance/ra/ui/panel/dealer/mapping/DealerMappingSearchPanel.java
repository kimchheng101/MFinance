package com.soma.mfinance.ra.ui.panel.dealer.mapping;


import com.soma.mfinance.core.dealer.model.DealerMapAddress;
import com.soma.mfinance.core.shared.dealer.DealerEntityField;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.ersys.core.hr.model.address.Commune;
import com.soma.ersys.core.hr.model.address.District;
import com.soma.ersys.core.hr.model.address.Province;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by kimsuor.seang  on 11/11/2017.
 */
public class DealerMappingSearchPanel extends AbstractSearchPanel<DealerMapAddress> implements DealerEntityField {

	private static final long serialVersionUID = 5489374367808132695L;

	private DealerComboBox cbxDealer;
	private EntityRefComboBox<Province> cbxProvince;
	private EntityRefComboBox<District> cbxDistrict;
	private EntityRefComboBox<Commune> cbxCommune;
	
	public DealerMappingSearchPanel(DealerMappingTablePanel dealerTablePanel) {
		super(I18N.message("dealer.search"), dealerTablePanel);
	}
	
	@Override
	protected void reset() {
		cbxCommune.setSelectedEntity(null);
		cbxDistrict.setSelectedEntity(null);
		cbxProvince.setSelectedEntity(null);
		cbxDealer.setSelectedEntity(null);
	}


	@Override
	protected Component createForm() {
		
		final HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSpacing(true);
		horizontalLayout.setMargin(true);
		
		
		Label lblProvince = new Label(I18N.message("province"));
        cbxProvince = new EntityRefComboBox<>();
		cbxProvince.setRestrictions(new BaseRestrictions<>(Province.class));
		cbxProvince.renderer();
		cbxProvince.setImmediate(true);
		
		Label lblDistrict = new Label(I18N.message("distrct"));
		cbxDistrict = new EntityRefComboBox<>();
		cbxDistrict.setRestrictions(new BaseRestrictions<>(District.class));
		cbxDistrict.renderer();
		cbxDistrict.setImmediate(true);
		
		Label lblCommune = new Label(I18N.message("commue"));
	    cbxCommune = new EntityRefComboBox<>();
	    cbxCommune.setRestrictions(new BaseRestrictions<>(Commune.class));
	    cbxCommune.renderer();
	    cbxCommune.setImmediate(true);
	    
	    cbxProvince.addValueChangeListener(new ValueChangeListener() {

			private static final long serialVersionUID = -78822726610125810L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cbxProvince.getSelectedEntity() != null) {
					BaseRestrictions<District> restrictions = cbxDistrict.getRestrictions();
					List<Criterion> criteria = new ArrayList<>();
					criteria.add(Restrictions.eq("province.id", cbxProvince.getSelectedEntity().getId()));
					restrictions.setCriterions(criteria);
					cbxDistrict.renderer();
				} else {
					cbxDistrict.clear();
				}
				cbxCommune.clear();
			}
		});

	    cbxDistrict.addValueChangeListener(new ValueChangeListener() {

			private static final long serialVersionUID = 1761539763125185708L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cbxDistrict.getSelectedEntity() != null) {
					BaseRestrictions<Commune> restrictions = cbxCommune.getRestrictions();
					List<Criterion> criteria = new ArrayList<>();
					criteria.add(Restrictions.eq("district.id", cbxDistrict.getSelectedEntity().getId()));
					restrictions.setCriterions(criteria);
					cbxCommune.renderer();
				} else {
					cbxCommune.clear();
				}
			}
		});
	    
	    
	    Label lblDealer = new Label(I18N.message("dealer"));
	    cbxDealer=new DealerComboBox(DataReference.getInstance().getDealers());
	    cbxDealer.setImmediate(true);
        
	    horizontalLayout.addComponent(lblDealer);
	    horizontalLayout.addComponent(cbxDealer);
	    horizontalLayout.addComponent(lblProvince);
	    horizontalLayout.addComponent(cbxProvince);
	    horizontalLayout.addComponent(lblDistrict);
	    horizontalLayout.addComponent(cbxDistrict);
	    horizontalLayout.addComponent(lblCommune);
	    horizontalLayout.addComponent(cbxCommune);
	  
                
		return horizontalLayout;
	}
	
	@Override
	public BaseRestrictions<DealerMapAddress> getRestrictions() {
		BaseRestrictions<DealerMapAddress> restrictions = new BaseRestrictions<>(DealerMapAddress.class);
		if (cbxDealer.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, cbxDealer.getSelectedEntity().getId()));
		}
		if (cbxProvince.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("province.id", cbxProvince.getSelectedEntity().getId()));
		}
		if (cbxCommune.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("commune.id", cbxCommune.getSelectedEntity().getId()));
		}
		if (cbxDistrict.getSelectedEntity() != null) {
			restrictions.addCriterion(Restrictions.eq("district.id", cbxDistrict.getSelectedEntity().getId()));
		}
		restrictions.addOrder(Order.asc(UPDATE_DATE));
		return restrictions;
	}

}
