package com.soma.mfinance.ra.ui.panel.interestrate;
import com.soma.mfinance.core.financial.model.Term;
import com.soma.mfinance.core.interestrate.InterestRate;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.ui.Table;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by b.chea on 2/22/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class InterestRateTablePanel extends AbstractTablePanel<InterestRate> implements FMEntityField {

    @PostConstruct
    public void PostConstruct() {
        super.init(I18N.message("interest.rates"));
        setCaption(I18N.message("interest.rates"));
        setSizeFull();
        setMargin(true);
        setSpacing(true);
        addDefaultNavigation();
    }

    /**
     * Get Paged definition
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTablePanel#createPagedDataProvider()
     */
    @Override
    protected PagedDataProvider<InterestRate> createPagedDataProvider() {
        PagedDefinition<InterestRate> pagedDefinition = new PagedDefinition<>(searchPanel.getRestrictions());
        pagedDefinition.addColumnDefinition(InterestRate.ID, I18N.message("id"), Long.class, Table.Align.LEFT, 70);
        pagedDefinition.addColumnDefinition(InterestRate.VALUE, I18N.message("value"), Double.class, Table.Align.LEFT, 100);
        pagedDefinition.addColumnDefinition(InterestRate.DESCEN, I18N.message("desc.en"), String.class, Table.Align.LEFT, 200);

        EntityPagedDataProvider<InterestRate> pagedDataProvider = new EntityPagedDataProvider<>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTablePanel#getEntity()
     */
    @Override
    protected InterestRate getEntity() {
        final Long id = getItemSelectedId();
        if (id != null) {
            return ENTITY_SRV.getById(InterestRate.class, id);
        }
        return null;
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTablePanel#createSearchPanel()
     */
    @Override
    protected InterestRateSearchPanel createSearchPanel() {
        return new InterestRateSearchPanel(this);
    }

}
