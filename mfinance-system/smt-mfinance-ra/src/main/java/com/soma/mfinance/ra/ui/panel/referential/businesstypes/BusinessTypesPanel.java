package com.soma.mfinance.ra.ui.panel.referential.businesstypes;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * @author by kimsuor.seang  on 10/18/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(BusinessTypesPanel.NAME)
public class BusinessTypesPanel extends AbstractTabsheetPanel implements View {

	private static final long serialVersionUID = 7762740624925762740L;

	public static final String NAME = "business.types";
	
	@Autowired
	private BusinessTypesFormPanel businessTypesFormPanel;
	
	@Autowired
	private BusinessTypesTablePanel businessTypesTablePanel;
	
	
	@PostConstruct
	public void PostConstruct() {
		super.init();
		businessTypesTablePanel.setMainPanel(this);
		businessTypesFormPanel.setCaption(I18N.message("business.type"));
		getTabSheet().setTablePanel(businessTypesTablePanel);
	}

	@Override
	public void enter(ViewChangeEvent event) {
	}
	
	
	@Override
	public void onAddEventClick() {
		businessTypesFormPanel.reset();
		getTabSheet().addFormPanel(businessTypesFormPanel);
		getTabSheet().setSelectedTab(businessTypesFormPanel);
	}

	@Override
	public void onEditEventClick() {
		getTabSheet().addFormPanel(businessTypesFormPanel);
		initSelectedTab(businessTypesFormPanel);
	}
	
	@Override
	public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
		if (selectedTab == businessTypesFormPanel) {
			businessTypesFormPanel.assignValues(businessTypesTablePanel.getItemSelectedId());
		} else if (selectedTab == businessTypesTablePanel && getTabSheet().isNeedRefresh()) {
			businessTypesTablePanel.refresh();
		}
		getTabSheet().setSelectedTab(selectedTab);
	}
}