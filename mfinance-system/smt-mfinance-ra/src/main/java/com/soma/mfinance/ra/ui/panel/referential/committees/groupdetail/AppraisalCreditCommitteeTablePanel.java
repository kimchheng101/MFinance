package com.soma.mfinance.ra.ui.panel.referential.committees.groupdetail;


import com.soma.mfinance.core.referential.creditcommitteeappraisal.model.AppraisalCreditCommittee;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author by kimsuor.seang  on 9/27/2017.
 */

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AppraisalCreditCommitteeTablePanel extends AbstractTablePanel<AppraisalCreditCommittee> implements FMEntityField {

    @PostConstruct
    public void PostConstruct() {
        setCaption(I18N.message("appraisal.credit.committee"));
        setSizeFull();
        setMargin(true);
        setSpacing(true);

        super.init(I18N.message("appraisal.credit.committee"));

        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addAddClickListener(this);
        navigationPanel.addEditClickListener(this);
        navigationPanel.addDeleteClickListener(this);
        navigationPanel.addRefreshClickListener(this);
    }

    /**
     * @return
     */
    @Override
    protected PagedDataProvider<AppraisalCreditCommittee> createPagedDataProvider() {
        PagedDefinition<AppraisalCreditCommittee> pagedDefinition = new PagedDefinition<>(searchPanel.getRestrictions());
        pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("secUser." + DESC, I18N.message("user.name").toUpperCase(), String.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("secUser." + LOG_IN, I18N.message("login").toUpperCase(), String.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("assetModel." + DESC, I18N.message("asset.make2").toUpperCase(), String.class, Align.LEFT, 100);
        pagedDefinition.addColumnDefinition("assetRange." + DESC, I18N.message("asset.range2").toUpperCase(), String.class, Align.LEFT, 100);
        EntityPagedDataProvider<AppraisalCreditCommittee> pagedDataProvider = new EntityPagedDataProvider<>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    @Override
    protected AppraisalCreditCommittee getEntity() {
        final Long id = getItemSelectedId();
        if (id != null) {
            return ENTITY_SRV.getById(AppraisalCreditCommittee.class, id);
        }
        return null;
    }

    @Override
    protected AppraisalCreditCommitteeSearchPanel createSearchPanel() {
        return new AppraisalCreditCommitteeSearchPanel(this);
    }
}
