package com.soma.mfinance.ra.ui.panel.system;

import com.soma.common.app.menu.model.MenuItemEntity;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;

/**
 * Created by ki.kao on 2/20/2017.
 */
@org.springframework.stereotype.Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MenuItemFormUpdate extends AbstractMenuItemForm {

    @Autowired
    private MenuItemServiceUtil menuItemServiceUtil;
    private String previousMenu = null;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addSaveClickListener(this);
    }

    @Override
    protected Component createForm() {
        FormLayout container = (FormLayout) super.initializeComponents();
        container.addComponent(txtId, 0);
        txtId.setEnabled(false);
        return container;
    }

    @Override
    public void saveButtonClick(Button.ClickEvent event) {
        try {
            super.clearErrorPanel();
            if (super.validate()) {
                MenuItemEntity entity = (MenuItemEntity) this.getEntity();
                menuItemServiceUtil.saveOrUpdateMenuItem(entity, previousMenu);
                super.reset();
                this.displaySuccess();
            } else {
                this.displayErrors();
                this.processAfterError();
            }
        } catch (Exception e) {
            e.printStackTrace();
            ENTITY_SRV.getSessionFactory().getCurrentSession().getTransaction().rollback();
        } finally {
            menuItemServiceUtil.clearSession();
        }
    }
    
    public void assignValues(MenuItemEntity entity) {
        super.reset();
        if (entity != null && entity.getId() != null) {
            menuItemEntity = entity;
            txtId.setValue(entity.getId().toString());
            txtCode.setValue(entity.getCode());
            txtAction.setValue(entity.getAction());
            txtDesc.setValue(entity.getDesc());
            txtSortIndex.setValue(String.valueOf(entity.getSortIndex()));
            appMenuComboBox.setSelectedEntity(entity.getMenu());
            if (entity.getMenu() != null)
                previousMenu = new String(entity.getMenu().getCode());
            if (entity.getParent() == null)
                cbMainMenu.setValue(true);
            else {
                parentMenuComboBox.setSelectedEntity(entity.getParent());
                cbSubMenu.setValue(true);
            }
        }
    }
}
