package com.soma.mfinance.ra.ui.panel.security.user;

import com.soma.mfinance.core.common.security.model.SecUserDetail;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.shared.util.ProfileUtil;
import com.soma.mfinance.ra.ui.panel.system.tab.ProfileGroupLayout;
import com.soma.ersys.vaadin.ui.security.secuser.detail.SecUserFormPanel;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.vaadin.ui.widget.combo.EntityComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import java.util.List;

/**
 * Created by ki.kao on 3/17/2017.
 */
@org.springframework.stereotype.Component("CustomSecUserFormPanel")
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CustomSecUserFormPanel extends SecUserFormPanel {

    private EntityComboBox<Dealer> dealerEntityComboBox;
    private CustomSecUserHolderPanel mainPanel;
    private SecUserDetail secUserDetail;
    private PasswordField txtPassword;
    private SecUser secUser;

    public CustomSecUserFormPanel() {
        super();
    }

    @Override
    public void PostConstruct() {
        super.PostConstruct();
    }

    @Override
    protected Component createForm() {
        Component component = super.createForm();
        dealerEntityComboBox = new EntityComboBox<Dealer>(Dealer.class, I18N.message("dealer"), "nameEn", "");
        dealerEntityComboBox.renderer(new BaseRestrictions<Dealer>(Dealer.class));
        dealerEntityComboBox.setRequired(true);
        this.formDetailPanel.addComponent(dealerEntityComboBox, 5);
        txtPassword = (PasswordField) formDetailPanel.getComponent(2);
        return component;
    }

    @Override
    public void assignValues(Long id) {
        if (id != null) {
            super.assignValues(id);
            BaseRestrictions<SecUserDetail> restrictions = new BaseRestrictions<>(SecUserDetail.class);
            restrictions.addCriterion(Restrictions.eq("secUser.id", id));
            List<SecUserDetail> list = ENTITY_SRV.list(restrictions);
            if (list != null && !list.isEmpty()) {
                this.secUserDetail = list.get(0);
                dealerEntityComboBox.setSelectedEntity(secUserDetail.getDealer());

            }
        }

    }

    @Override
    protected SecUser getEntity() {
        return super.getEntity();
    }

    @Override
    public void saveEntity() {
        if (!this.validate()) return;
        secUser = this.getEntity();
        if (secUser != null && secUser.getId() != null) {
            SECURITY_SRV.update(secUser, (String) this.txtPassword.getValue());
        } else {
            secUser = SECURITY_SRV.createUser(secUser, (String) this.txtPassword.getValue());
            this.mainPanel.addSubTab(secUser.getId());
        }
        if (ProfileUtil.isCreditOfficer(secUser) || ProfileUtil.isPOS(secUser)) {
            if (secUserDetail == null)
                this.secUserDetail = new SecUserDetail();
            secUserDetail.setSecUser(secUser);
            secUserDetail.setDealer(dealerEntityComboBox.getSelectedEntity() != null ? dealerEntityComboBox.getSelectedEntity() : null);
            ENTITY_SRV.saveOrUpdate(secUserDetail);
        }
    }

    @Override
    public void reset() {
        secUserDetail = null;
        super.reset();
        dealerEntityComboBox.setSelectedEntity(null);
    }

    @Override
    protected boolean validate() {
        secUser = this.getEntity();
        TextField txtLogin = (TextField) formDetailPanel.getComponent(1);
        PasswordField txtConfirm = (PasswordField) formDetailPanel.getComponent(3);
        if (txtLogin != null) {
            if (txtLogin.getValue().isEmpty()) {
                txtLogin.setValue(" ");
                this.errors.add(I18N.message("login is required"));
            } else {
                String tmp = txtLogin.getValue();
                txtLogin.setValue(tmp.replace(" ", ""));
            }
        }
        if (secUser != null && secUser.getId() != null) {
        }else {
            if (txtConfirm != null && txtConfirm.isEmpty()) {
                this.errors.add(I18N.message("confirm.password"));
            }
        }


        if (this.secUser.getDefaultProfile() != null) {
            if ((ProfileUtil.isCreditOfficer(secUser) || ProfileUtil.isPOS(secUser)) && dealerEntityComboBox.getSelectedEntity() == null) {
                this.errors.add(I18N.message("dealer.is.required"));
            }
        }

        return super.validate();
    }

    public void setMainPanel(CustomSecUserHolderPanel mainPanel) {
        this.mainPanel = mainPanel;
    }


}
