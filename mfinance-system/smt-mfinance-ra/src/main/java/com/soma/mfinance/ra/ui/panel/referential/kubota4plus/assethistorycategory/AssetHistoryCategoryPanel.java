package com.soma.mfinance.ra.ui.panel.referential.kubota4plus.assethistorycategory;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * @author by kimsuor.seang  on 10/9/2017.
 */

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(AssetHistoryCategoryPanel.NAME)
public class AssetHistoryCategoryPanel extends AbstractTabsheetPanel implements View {

    public static final String NAME = "asset.history.category";

    @Autowired
    private AssetHistoryCategoryTablePanel assetHistoryCategoryTablePanel;
    @Autowired
    private AssetHistoryCategoryFormPanel assetHistoryCategoryFormPanel;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        assetHistoryCategoryTablePanel.setMainPanel(this);
        assetHistoryCategoryFormPanel.setCaption(I18N.message("asset.history.category"));
        getTabSheet().setTablePanel(assetHistoryCategoryTablePanel);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }


    @Override
    public void onAddEventClick() {
        assetHistoryCategoryFormPanel.reset();
        getTabSheet().addFormPanel(assetHistoryCategoryFormPanel);
        getTabSheet().setSelectedTab(assetHistoryCategoryFormPanel);
    }

    @Override
    public void onEditEventClick() {
        assetHistoryCategoryFormPanel.reset();
        getTabSheet().addFormPanel(assetHistoryCategoryFormPanel);
        initSelectedTab(assetHistoryCategoryFormPanel);
    }

    @Override
    public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
        if (selectedTab == assetHistoryCategoryFormPanel) {
            assetHistoryCategoryFormPanel.assignValues(assetHistoryCategoryTablePanel.getItemSelectedId());
        } else if (selectedTab == assetHistoryCategoryTablePanel && getTabSheet().isNeedRefresh()) {
            assetHistoryCategoryTablePanel.refresh();
        }
        getTabSheet().setSelectedTab(selectedTab);
    }
}
