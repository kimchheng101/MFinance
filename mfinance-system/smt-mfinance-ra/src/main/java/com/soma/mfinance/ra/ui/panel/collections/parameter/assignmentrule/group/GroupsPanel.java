package com.soma.mfinance.ra.ui.panel.collections.parameter.assignmentrule.group;

import javax.annotation.PostConstruct;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import ru.xpoft.vaadin.VaadinView;

/**
 * Group tab panel in collection
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(GroupsPanel.NAME)
public class GroupsPanel extends AbstractTabsheetPanel implements View{

	/** */
	public static final String NAME = "collection.group";
	private static final long serialVersionUID = 7514446945089546868L;
	
	@Autowired
	private GroupTablePanel groupTablePanel;
	@Autowired
	private GroupFormPanel groupFormPanel;
	
	@PostConstruct
	public void PostConstruct() {
		super.init();
		setMargin(true);
		groupTablePanel.setMainPanel(this);
		groupFormPanel.setCaption(I18N.message("collection.group"));
		getTabSheet().setTablePanel(groupTablePanel);
	}
	
	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel#onAddEventClick()
	 */
	@Override
	public void onAddEventClick() {
		groupFormPanel.reset();
		getTabSheet().addFormPanel(groupFormPanel);
		getTabSheet().setSelectedTab(groupFormPanel);
	}

	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel#onEditEventClick()
	 */
	@Override
	public void onEditEventClick() {
		getTabSheet().addFormPanel(groupFormPanel);
		initSelectedTab(groupFormPanel);
	}
	
	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel#initSelectedTab(com.vaadin.ui.Component)
	 */
	@Override
	public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
		if (selectedTab == groupFormPanel) {
			groupFormPanel.assignValues(groupTablePanel.getItemSelectedId());
		} else if (selectedTab == groupTablePanel && getTabSheet().isNeedRefresh()) {
			groupTablePanel.refresh();
		}
		getTabSheet().setSelectedTab(selectedTab);
	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent event) {

	}
}
