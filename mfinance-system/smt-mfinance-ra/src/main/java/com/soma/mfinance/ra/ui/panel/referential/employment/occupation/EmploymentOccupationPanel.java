package com.soma.mfinance.ra.ui.panel.referential.employment.occupation;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * Created by Pisethraingsey SOUN
 * Date     : 22/06/2017, 3:06 PM
 * Email    : p.suon@gl-f.com
 */
@Component
@VaadinView(value = EmploymentOccupationPanel.NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class EmploymentOccupationPanel extends AbstractTabsheetPanel implements View {

    private static final long serialVersionUID = -3364578594797133531L;
    public static final String NAME = "occupation.list";

    @Autowired
    private EmploymentOccupationTablePanel occupationTablePanel;
    @Autowired
    private EmploymentOccupationFormPanel occupationFormPanel;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        this.occupationTablePanel.setMainPanel(this);
        this.occupationFormPanel.setCaption(I18N.message("occupation"));
        this.getTabSheet().setTablePanel(this.occupationTablePanel);

    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }

    @Override
    public void onAddEventClick() {
        this.occupationFormPanel.reset();
        this.getTabSheet().addFormPanel(this.occupationFormPanel);
        this.getTabSheet().setSelectedTab(this.occupationFormPanel);
    }

    @Override
    public void onEditEventClick() {
        this.getTabSheet().addFormPanel(this.occupationFormPanel);
        this.initSelectedTab(this.occupationFormPanel);
    }

    @Override
    public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
        if (selectedTab == this.occupationFormPanel) {
            this.occupationFormPanel.assignValues(this.occupationTablePanel.getItemSelectedId());
        } else if (selectedTab == this.occupationTablePanel && this.getTabSheet().isNeedRefresh()) {
            this.occupationTablePanel.refresh();
        }
        this.getTabSheet().setSelectedTab(selectedTab);
    }
}
