package com.soma.mfinance.ra.ui.panel.referential.employment.position;

import com.soma.mfinance.core.application.model.EmploymentPosition;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.ui.Table;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Create by Rith RON.
 * User r.ron.
 * Date on 2017-06-21.
 * Time at 2:12 PM.
 * Email r.ron@gl-f.com.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PositionTablePanel extends AbstractTablePanel<EmploymentPosition> implements FMEntityField {

    @PostConstruct
    public void PostConstruct(){
        setCaption(I18N.message("position.table"));
        setSizeFull();
        setMargin(true);
        setSpacing(true);

        super.init(I18N.message("position.search"));

        addDefaultNavigation();
    }

    /**
     * Get item selected id
     * @return
     */
    public Long getItemSelectedId() {
        if (selectedItem != null) {
            return (Long) selectedItem.getItemProperty(ID).getValue();
        }
        return null;
    }

    @Override
    protected EmploymentPosition getEntity() {
        final Long id = getItemSelectedId();
        if (id != null) {
            return ENTITY_SRV.getById(EmploymentPosition.class, id);
        }
        return null;
    }

    @Override
    protected PagedDataProvider<EmploymentPosition> createPagedDataProvider() {
        PagedDefinition<EmploymentPosition> pagedDefinition = new PagedDefinition<>(searchPanel.getRestrictions());
        pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Table.Align.LEFT, 100);
        pagedDefinition.addColumnDefinition(CODE,I18N.message("code"), String.class, Table.Align.LEFT, 100);
        pagedDefinition.addColumnDefinition(DESC, I18N.message("desc"), String.class, Table.Align.LEFT, 200);
        pagedDefinition.addColumnDefinition(DESC_EN, I18N.message("desc.en"), String.class, Table.Align.LEFT, 200);
        pagedDefinition.addColumnDefinition("employmentOccupation"+ "." + DESC_EN, I18N.message("employment.occupation"), String.class, Table.Align.LEFT, 200);
        pagedDefinition.addColumnDefinition(EMPLOYMENT_INDUSTRY+ "." + DESC_EN, I18N.message("employment.industry"), String.class, Table.Align.LEFT, 200);

        EntityPagedDataProvider<EmploymentPosition> pagedDataProvider = new EntityPagedDataProvider<>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    @Override
    protected PositionSearchPanel createSearchPanel(){
        return new PositionSearchPanel(this);
    }
}
