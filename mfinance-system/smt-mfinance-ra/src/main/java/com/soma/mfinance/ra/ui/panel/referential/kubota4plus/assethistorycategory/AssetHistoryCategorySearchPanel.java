package com.soma.mfinance.ra.ui.panel.referential.kubota4plus.assethistorycategory;

import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.asset.model.appraisal.AssetHistoryCategory;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.*;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by kimsuor.seang  on 10/9/2017.
 */
public class AssetHistoryCategorySearchPanel extends AbstractSearchPanel<AssetHistoryCategory> implements FMEntityField {

    private CheckBox cbActive;
    private CheckBox cbInactive;
    private TextField txtDescEn;
    private EntityRefComboBox<AssetRange> cbxAssetAppraisalType;

    public AssetHistoryCategorySearchPanel(AssetHistoryCategoryTablePanel assetHistoryCategoryTablePanel) {
        super(I18N.message("asset.history.category"), assetHistoryCategoryTablePanel);
    }

    @Override
    protected void reset() {
        txtDescEn.setValue("");
    }


    @Override
    protected Component createForm() {
        final GridLayout gridLayout = new GridLayout(5, 1);
        cbxAssetAppraisalType = new EntityRefComboBox<>();
        cbxAssetAppraisalType.setRestrictions(new BaseRestrictions<>(AssetRange.class));
        cbxAssetAppraisalType.renderer();
        cbxAssetAppraisalType.setCaption(I18N.message("asset.appraisal.type"));
        txtDescEn = ComponentFactory.getTextField("desc.en", false, 60, 200);
        cbActive = new CheckBox(I18N.message("active"));
        cbActive.setValue(true);
        cbInactive = new CheckBox(I18N.message("inactive"));
        cbInactive.setValue(false);
        gridLayout.setSpacing(true);
        gridLayout.addComponent(new FormLayout(txtDescEn), 1, 0);
        gridLayout.addComponent(new FormLayout(cbxAssetAppraisalType) , 2 , 0);
        gridLayout.addComponent(new FormLayout(cbActive), 3, 0);
        gridLayout.addComponent(new FormLayout(cbInactive), 4, 0);

        return gridLayout;
    }

    @Override
    public BaseRestrictions<AssetHistoryCategory> getRestrictions() {
        BaseRestrictions<AssetHistoryCategory> restrictions = new BaseRestrictions<>(AssetHistoryCategory.class);
        List<Criterion> criterions = new ArrayList<>();
        if (StringUtils.isNotEmpty(txtDescEn.getValue())) {
            criterions.add(Restrictions.like(DESC_EN, txtDescEn.getValue(), MatchMode.ANYWHERE));
        }
        if(cbxAssetAppraisalType.getSelectedEntity() != null ){
            criterions.add(Restrictions.eq("assetAppraisalType.id", cbxAssetAppraisalType.getSelectedEntity().getId()));
        }
        if (!cbActive.getValue() && !cbInactive.getValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
            restrictions.getStatusRecordList().add(EStatusRecord.INACT);
        }
        if (cbActive.getValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
        }
        if (cbInactive.getValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.INACT);
        }
        restrictions.setCriterions(criterions);
        restrictions.addOrder(Order.asc(DESC_EN));
        return restrictions;
    }
}
