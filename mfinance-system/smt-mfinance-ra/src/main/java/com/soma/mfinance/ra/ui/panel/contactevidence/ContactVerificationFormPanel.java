package com.soma.mfinance.ra.ui.panel.contactevidence;

import com.soma.mfinance.core.model.system.ContactVerification;
import com.soma.frmk.security.model.SecProfile;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author kimsuor.seang
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ContactVerificationFormPanel extends AbstractFormPanel {

	private static final long serialVersionUID = -5278994729455226840L;
	
	private ContactVerification contactVerification;
	private TextField txtDesc;
    private TextField txtDescEn;
    private EntityRefComboBox<SecProfile> cbxProfile;
    
    @PostConstruct
	public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
		navigationPanel.addSaveClickListener(this);
	}
	
	@Override
	protected Entity getEntity() {
		contactVerification.setDesc(txtDesc.getValue());
		contactVerification.setDescEn(txtDescEn.getValue());
		contactVerification.setProfile(cbxProfile.getSelectedEntity());
		contactVerification.setStatusRecord(EStatusRecord.ACTIV);
		return contactVerification;
	}

	@Override
	protected com.vaadin.ui.Component createForm() {
		final FormLayout formPanel = new FormLayout();
		txtDescEn = ComponentFactory.getTextField("desc.en", true, 60, 200);
		txtDesc = ComponentFactory.getTextField35("desc", false, 60, 200);
		
		cbxProfile = new EntityRefComboBox<SecProfile>(I18N.message("profile"));
        cbxProfile.setRestrictions(new BaseRestrictions<SecProfile>(SecProfile.class));
        cbxProfile.setRequired(true);
        cbxProfile.renderer();
		
        formPanel.addComponent(txtDescEn);
        formPanel.addComponent(txtDesc);
        formPanel.addComponent(cbxProfile);
		return formPanel;
	}

	public void assignValues(Long id) {
		super.reset();
		if (id != null) {
			contactVerification = ENTITY_SRV.getById(ContactVerification.class, id);
			txtDescEn.setValue(contactVerification.getDescEn());
			txtDesc.setValue(contactVerification.getDesc());
			cbxProfile.setSelectedEntity(contactVerification.getProfile());
		}
	}
	
	/**
	 * Reset
	 */
	@Override
	public void reset() {
		super.reset();
		contactVerification = new ContactVerification();
		txtDescEn.setValue("");
		txtDesc.setValue("");
		cbxProfile.setSelectedEntity(null);
		markAsDirty();
	}
	
	/**
	 * @return
	 */
	@Override
	protected boolean validate() {	
		checkMandatoryField(txtDescEn, "desc.en");
		checkMandatorySelectField(cbxProfile, "profile");
		return errors.isEmpty();
	}
}
