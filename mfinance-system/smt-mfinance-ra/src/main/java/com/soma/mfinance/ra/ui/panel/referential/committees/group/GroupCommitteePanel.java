package com.soma.mfinance.ra.ui.panel.referential.committees.group;

import com.soma.mfinance.core.referential.committee.GroupCommittee;
import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * @author by kimsuor.seang  on 9/27/2017.
 */

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(GroupCommitteePanel.NAME)
public class GroupCommitteePanel extends AbstractTabsheetPanel implements View {

	public static final String NAME = "group.committee.panel";

	@Autowired
	private GroupCommitteeFormPanel groupCommitteeFormPanel;

	private GroupCommitteeTableDetailPanel groupCommitteeTableDetailPanel;

	@Autowired
	private GroupCommitteeTablePanel groupCommitteeTablePanel;

	@PostConstruct
	public void PostConstruct() {
		super.init();
		groupCommitteeTablePanel.setMainPanel(this);
		groupCommitteeFormPanel.setCaption(I18N.message("group.committee.panel"));

		groupCommitteeTableDetailPanel = new GroupCommitteeTableDetailPanel(groupCommitteeFormPanel);
		getTabSheet().setTablePanel(groupCommitteeTablePanel);

	}

	@Override
	public void enter(ViewChangeEvent event) {

	}

	@Override
	public void onAddEventClick() {
		groupCommitteeFormPanel.reset();
		getTabSheet().addFormPanel(groupCommitteeFormPanel);
		getTabSheet().setSelectedTab(groupCommitteeFormPanel);
	}

	@Override
	public void onEditEventClick() {
		getTabSheet().addFormPanel(groupCommitteeFormPanel);
		getTabSheet().addFormPanel(groupCommitteeTableDetailPanel);
		initSelectedTab(groupCommitteeFormPanel);
	}

	@Override
	public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
		if (selectedTab == groupCommitteeFormPanel || selectedTab == groupCommitteeTableDetailPanel) {
			groupCommitteeFormPanel.assignValues(groupCommitteeTablePanel.getItemSelectedId());
			groupCommitteeTableDetailPanel.assignValue(ENTITY_SRV.getById(GroupCommittee.class, groupCommitteeTablePanel.getItemSelectedId()));
		} else if (selectedTab == groupCommitteeTablePanel && getTabSheet().isNeedRefresh()) {
			groupCommitteeTablePanel.refresh();
		}
		getTabSheet().setSelectedTab(selectedTab);
	}
}
