package com.soma.mfinance.ra.ui.panel.referential.refentity;

import com.soma.ersys.vaadin.ui.referential.reftable.list.RefTableSearchPanel;
import com.soma.ersys.vaadin.ui.referential.reftable.list.RefTableTablePanel;
import com.soma.frmk.config.model.ERefType;
import com.soma.frmk.config.model.RefTable;
import com.soma.frmk.config.service.RefTableRestriction;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by ki.kao on 6/10/2017.
 */
public class RefEntitySearchPanel extends RefTableSearchPanel {

    public RefEntitySearchPanel(RefTableTablePanel districtTablePanel) {
        super(districtTablePanel);
    }

    @Override
    public BaseRestrictions<RefTable> getRestrictions() {
        RefTableRestriction restrictions = new RefTableRestriction(RefTable.class);
        ArrayList criterions = new ArrayList();
       /* if (StringUtils.isNotEmpty((String) this.txtSearchCode.getValue())) {
            String selectedReadonlyItems = (String) RefTableTablePanel.packageName.get(((String) this.txtSearchCode.getValue()).toString());
            criterions.add(Restrictions.eq("code", selectedReadonlyItems));
        }

        if (StringUtils.isNotEmpty((String) this.txtSearchDes.getValue())) {
            criterions.add(Restrictions.like("descEn", this.txtSearchDes.getValue(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotEmpty((String) this.txtSearchShortName.getValue())) {
            criterions.add(Restrictions.eq("shortName", ((String) this.txtSearchShortName.getValue()).toString()));
        }*/

        Collection selectedReadonlyItems1 = (Collection) this.cbReadOnly.getValue();
        if (selectedReadonlyItems1.size() != 2) {
            restrictions.setIsReadonly(Boolean.valueOf(selectedReadonlyItems1.contains(Boolean.TRUE)));
        }

        Collection selectedVisibleItems = (Collection) this.cbVisible.getValue();
        if (selectedVisibleItems.size() != 2) {
            restrictions.setIsVisible(Boolean.valueOf(selectedVisibleItems.contains(Boolean.TRUE)));
        }

        criterions.add(Restrictions.eq("type", ERefType.ENTITY_REF));
        restrictions.setCriterions(criterions);
        restrictions.addOrder(Order.asc("id"));
        return restrictions;
    }
}
