package com.soma.mfinance.ra.ui.panel.contactevidence;

import com.soma.mfinance.core.model.system.ContactVerification;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kimsuor.seang
 *
 */
public class ContactVerificationSearchPanel extends AbstractSearchPanel<ContactVerification> implements FMEntityField {
	
	private static final long serialVersionUID = 3404964847618664068L;
	private TextField txtDescEn;
	
	public ContactVerificationSearchPanel(ContactVerificationTablePanel contactVerificationTablePanel) {
		super(I18N.message("contact.verification.search"), contactVerificationTablePanel);
	}
	
	@Override
	protected void reset() {
		txtDescEn.setValue("");
	}
	@Override
	protected Component createForm() {
		final GridLayout gridLayout = new GridLayout(3, 1);
		txtDescEn = ComponentFactory.getTextField("desc.en", false, 60, 200);
        gridLayout.setSpacing(true);
        gridLayout.addComponent(new FormLayout(txtDescEn), 1, 0);
        
		return gridLayout;
	}
	
	@Override
	public BaseRestrictions<ContactVerification> getRestrictions() {
		BaseRestrictions<ContactVerification> restrictions = new BaseRestrictions<ContactVerification>(ContactVerification.class);
		List<Criterion> criterions = new ArrayList<Criterion>();
		if (StringUtils.isNotEmpty(txtDescEn.getValue())) { 
			criterions.add(Restrictions.like(DESC_EN, txtDescEn.getValue(), MatchMode.ANYWHERE));
		}		
		restrictions.setCriterions(criterions);
		restrictions.addOrder(Order.asc(DESC_EN));
		return restrictions;
	}

}
