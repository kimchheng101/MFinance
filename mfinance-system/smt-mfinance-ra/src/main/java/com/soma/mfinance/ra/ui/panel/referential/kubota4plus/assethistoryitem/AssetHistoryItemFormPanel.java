package com.soma.mfinance.ra.ui.panel.referential.kubota4plus.assethistoryitem;


import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.asset.model.appraisal.AssetHistoryCategory;
import com.soma.mfinance.core.asset.model.appraisal.AssetHistoryItem;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.data.Property;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * @author by kimsuor.seang  on 10/9/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AssetHistoryItemFormPanel extends AbstractFormPanel {

    private AssetHistoryItem assetHistoryItem;

    private CheckBox cbActive;
    private TextField txtDesc;
    private TextField txtDescEn;
    private TextField txtPercent;
    private TextField txtSortIndex;
    private EntityRefComboBox<AssetRange> cbxAssetAppraisalType;
    private EntityRefComboBox<AssetHistoryCategory> cbxAssetHistoryCategory;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addSaveClickListener(this);
    }

    @Override
    protected Entity getEntity() {
        assetHistoryItem.setAssetHistoryCategory(cbxAssetHistoryCategory.getSelectedEntity());
        assetHistoryItem.setDesc(txtDesc.getValue());
        assetHistoryItem.setDescEn(txtDescEn.getValue());
        assetHistoryItem.setPercent(Integer.parseInt(txtPercent.getValue()));
        if(txtSortIndex.getValue() != null){
            assetHistoryItem.setSortIndex(Integer.parseInt(txtSortIndex.getValue()));
        }
        assetHistoryItem.setStatusRecord(cbActive.getValue() ? EStatusRecord.ACTIV : EStatusRecord.INACT);
        return assetHistoryItem;
    }

    @Override
    protected com.vaadin.ui.Component createForm() {
        final FormLayout formPanel = new FormLayout();
        cbxAssetAppraisalType = new EntityRefComboBox<>();
//        cbxAssetAppraisalType.setRestrictions(new BaseRestrictions<>(AssetAppraisalType.class));
        cbxAssetAppraisalType.setRestrictions(new BaseRestrictions<>(AssetRange.class));
        cbxAssetAppraisalType.renderer();
        cbxAssetAppraisalType.setCaption(I18N.message("asset.appraisal.type"));

        cbxAssetHistoryCategory = new EntityRefComboBox<>();
        cbxAssetHistoryCategory.setRestrictions(new BaseRestrictions<>(AssetHistoryCategory.class));
        cbxAssetHistoryCategory.renderer();
        cbxAssetHistoryCategory.setCaption(I18N.message("asset.history.category"));

        cbxAssetAppraisalType.addValueChangeListener(new Property.ValueChangeListener() {

            public void valueChange(Property.ValueChangeEvent event) {
                if (cbxAssetAppraisalType.getSelectedEntity() != null) {
                    BaseRestrictions<AssetHistoryCategory> restrictions = cbxAssetHistoryCategory.getRestrictions();
                    List<Criterion> criterions = new ArrayList<>();
                    criterions.add(Restrictions.eq("assetAppraisalType.id", cbxAssetAppraisalType.getSelectedEntity().getId()));
                    restrictions.setCriterions(criterions);
                    cbxAssetHistoryCategory.renderer();
                } else {
                    cbxAssetHistoryCategory.clear();
                    cbxAssetHistoryCategory.setRestrictions(new BaseRestrictions<>(AssetHistoryCategory.class));
                    cbxAssetHistoryCategory.renderer();
                }
            }
        });

        txtDescEn = ComponentFactory.getTextField("desc.en", true, 60, 200);
        txtDesc = ComponentFactory.getTextField35("desc", false, 60, 200);
        txtSortIndex = ComponentFactory.getTextField("sort.index", true, 60, 200);
        txtPercent = ComponentFactory.getTextField("percent", true, 60, 200);
        cbActive = new CheckBox(I18N.message("active"));
        cbActive.setValue(true);

        formPanel.addComponent(cbxAssetAppraisalType);
        formPanel.addComponent(cbxAssetHistoryCategory);
        formPanel.addComponent(txtDescEn);
        formPanel.addComponent(txtDesc);
        formPanel.addComponent(txtPercent);
        formPanel.addComponent(txtSortIndex);
        formPanel.addComponent(cbActive);
        return formPanel;
    }

    public void assignValues(Long assetHistoryItemId) {
        super.reset();
        if (assetHistoryItemId != null) {
            assetHistoryItem = ENTITY_SRV.getById(AssetHistoryItem.class, assetHistoryItemId);
            txtDescEn.setValue(assetHistoryItem.getDescEn());
            txtDesc.setValue(assetHistoryItem.getDesc());
            if(assetHistoryItem.getPercent() != null)
                txtPercent.setValue(assetHistoryItem.getPercent().toString());
            cbxAssetAppraisalType.setSelectedEntity(assetHistoryItem.getAssetHistoryCategory().getAssetAppraisalType());
            cbxAssetHistoryCategory.setSelectedEntity(assetHistoryItem.getAssetHistoryCategory());
            if(assetHistoryItem.getSortIndex() != null){
                txtSortIndex.setValue(assetHistoryItem.getSortIndex().toString());
            }
            cbActive.setValue(assetHistoryItem.getStatusRecord() == EStatusRecord.ACTIV);
        }
    }

    @Override
    public void reset() {
        super.reset();
        assetHistoryItem = new AssetHistoryItem();
        txtDescEn.setValue("");
        txtDesc.setValue("");
        txtPercent.setValue("");
        cbActive.setValue(true);
        txtSortIndex.setValue("");
        cbxAssetHistoryCategory.setSelectedEntity(null);
        cbxAssetAppraisalType.setSelectedEntity(null);
        markAsDirty();
    }

    @Override
    protected boolean validate() {
        checkMandatoryField(txtDescEn, "desc.en");
        checkMandatoryField(txtPercent, "percent");
        return errors.isEmpty();
    }
}
