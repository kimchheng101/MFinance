package com.soma.mfinance.ra.ui.panel.Lease.amount;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;
/**
 * Created by b.chea on 4/10/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(LeaseAmountPanel.NAME)
public class LeaseAmountPanel extends AbstractTabsheetPanel implements View{

    public static final String NAME = "lease.amount.percent";

    @Autowired
    private LeaseAmountTablePanel leaseAmountTablePanel;
    @Autowired
    private LeaseAmountFormPanel leaseAmountFormPanel;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        leaseAmountTablePanel.setMainPanel(this);
        leaseAmountFormPanel.setCaption(I18N.message("lease.amount.percent"));
        getTabSheet().setTablePanel(leaseAmountTablePanel);
    }

    /**
     * @see com.vaadin.navigator.View#enter(com.vaadin.navigator.ViewChangeListener.ViewChangeEvent)
     */
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel#onAddEventClick()
     */
    @Override
    public void onAddEventClick() {
        leaseAmountFormPanel.reset();
        getTabSheet().addFormPanel(leaseAmountFormPanel);
        getTabSheet().setSelectedTab(leaseAmountFormPanel);
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel#onEditEventClick()
     */
    @Override
    public void onEditEventClick() {
        getTabSheet().addFormPanel(leaseAmountFormPanel);
        initSelectedTab(leaseAmountFormPanel);
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel#initSelectedTab(com.vaadin.ui.Component)
     */
    @Override
    public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
        if (selectedTab == leaseAmountFormPanel) {
            leaseAmountFormPanel.assignValues(leaseAmountTablePanel.getItemSelectedId());
        } else if (selectedTab == leaseAmountTablePanel && getTabSheet().isNeedRefresh()) {
            leaseAmountTablePanel.refresh();
        }
        getTabSheet().setSelectedTab(selectedTab);
    }

}
