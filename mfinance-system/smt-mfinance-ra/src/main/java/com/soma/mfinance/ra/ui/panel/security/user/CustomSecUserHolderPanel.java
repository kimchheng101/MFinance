package com.soma.mfinance.ra.ui.panel.security.user;

import com.soma.ersys.vaadin.ui.security.secuser.list.SecUserTablePanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * Created by ki.kao on 3/17/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(CustomSecUserHolderPanel.NAME)
public class CustomSecUserHolderPanel extends AbstractTabsheetPanel implements View {

    public static final String NAME = "users";

    @Autowired
    protected SecUserTablePanel userTablePanel;

    @Autowired
    @Qualifier("CustomSecUserFormPanel")
    protected CustomSecUserFormPanel secUserFormPanel;

    public CustomSecUserHolderPanel() {
        super();
    }

    @PostConstruct
    public void PostConstruct() {
        super.init();
        this.userTablePanel.setMainPanel(this);
        this.secUserFormPanel.setCaption(I18N.message("user"));
        this.secUserFormPanel.setMainPanel(this);
        this.getTabSheet().setTablePanel(this.userTablePanel);
    }

    @Override
    public void onEditEventClick() {
        getTabSheet().addFormPanel(secUserFormPanel);
        initSelectedTab(secUserFormPanel);
    }


    @Override
    public void onAddEventClick() {
        this.secUserFormPanel.reset();
        this.getTabSheet().addFormPanel(this.secUserFormPanel);
        this.getTabSheet().setSelectedTab(this.secUserFormPanel);
    }

    @Override
    public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
        if (selectedTab == this.secUserFormPanel) {
            this.secUserFormPanel.assignValues(this.userTablePanel.getItemSelectedId());
        } else if (selectedTab == this.userTablePanel && this.getTabSheet().isNeedRefresh()) {
            this.userTablePanel.refresh();
        }

        this.getTabSheet().setSelectedTab(selectedTab);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }

    public void addSubTab(Long selectedId) {
    }
}
