package com.soma.mfinance.ra.ui.panel.dealer.mapping;

import com.soma.mfinance.core.dealer.model.DealerMapAddress;
import com.soma.mfinance.core.shared.referencial.DataReference;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.ersys.core.hr.model.address.Commune;
import com.soma.ersys.core.hr.model.address.District;
import com.soma.ersys.core.hr.model.address.Province;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * @author by kimsuor.seang  on 11/11/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DealerMappingFormPanel extends AbstractFormPanel {

	private static final long serialVersionUID = -428044630697485756L;

	private DealerMapAddress dealerMapAddress;
	private EntityRefComboBox<Province> cbxProvince;
	private EntityRefComboBox<District> cbxDistrict;
	private EntityRefComboBox<Commune> cbxCommune;
	//private EntityRefComboBox<Village> cbxVillage;
	private DealerComboBox cbxDealer;

    @PostConstruct
	public void PostConstruct() {
        super.init();
        setCaption(I18N.message("delear.create"));
        NavigationPanel navigationPanel = addNavigationPanel();
		navigationPanel.addSaveClickListener(this);
	}

	@Override
	protected DealerMapAddress getEntity() {
		dealerMapAddress.setProvince(cbxProvince.getSelectedEntity());
		dealerMapAddress.setCommune(cbxCommune.getSelectedEntity());
		dealerMapAddress.setDistrict(cbxDistrict.getSelectedEntity());
		//dealerMapAddress.setVillage(cbxVillage.getSelectedEntity());
		dealerMapAddress.setDealer(cbxDealer.getSelectedEntity());
		return dealerMapAddress;
	}

	@Override
	protected com.vaadin.ui.Component createForm() {
				
		cbxProvince = new EntityRefComboBox<>(I18N.message("province"));
		cbxProvince.setRestrictions(new BaseRestrictions<>(Province.class));
		cbxProvince.setImmediate(true);
		cbxProvince.renderer();
		cbxProvince.setRequired(true);

		cbxDistrict = new EntityRefComboBox<>(I18N.message("district"));
		cbxDistrict.setRestrictions(new BaseRestrictions<>(District.class));
		cbxDistrict.setImmediate(true);
		cbxDistrict.renderer();
		cbxDistrict.setRequired(true);

	    cbxCommune = new EntityRefComboBox<>(I18N.message("commune"));
	    cbxCommune.setRestrictions(new BaseRestrictions<>(Commune.class));
	    cbxCommune.setImmediate(true);
	    cbxCommune.renderer();
	    cbxCommune.setRequired(true);
	  
	   /* cbxVillage = new EntityRefComboBox<Village>(I18N.message("village"));
	    cbxVillage.setRestrictions(new BaseRestrictions<Village>(Village.class));
	    cbxVillage.setImmediate(true);
	    cbxVillage.setRequired(true);
	    cbxVillage.renderer();*/
	    
	    cbxDealer=new DealerComboBox(I18N.message("dealer"), DataReference.getInstance().getDealers());
	    cbxDealer.setImmediate(true);
	    cbxDealer.setRequired(true);

	    cbxProvince.addValueChangeListener(new ValueChangeListener() {

			private static final long serialVersionUID = -78822726610125810L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cbxProvince.getSelectedEntity() != null) {
					BaseRestrictions<District> restrictions = cbxDistrict.getRestrictions();
					List<Criterion> criteria = new ArrayList<>();
					criteria.add(Restrictions.eq("province.id", cbxProvince.getSelectedEntity().getId()));
					restrictions.setCriterions(criteria);
					cbxDistrict.renderer();
				} else {
					cbxDistrict.clear();
				}
				cbxCommune.clear();
				//cbxVillage.clear();
			}
		});
			    
	    
	    
	    cbxDistrict.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 1761539763125185708L;
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cbxDistrict.getSelectedEntity() != null) {
					BaseRestrictions<Commune> restrictions = cbxCommune.getRestrictions();
					List<Criterion> criteria = new ArrayList<>();
					criteria.add(Restrictions.eq("district.id", cbxDistrict.getSelectedEntity().getId()));
					restrictions.setCriterions(criteria);
					cbxCommune.renderer();
				} else {
					cbxCommune.clear();
				}
				//cbxVillage.clear();
			}
		});
	    
	    
	    /*cbxCommune.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = -4700289434066242228L;
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cbxCommune.getSelectedEntity() != null) {
					BaseRestrictions<Village> restrictions = cbxVillage.getRestrictions();
					List<Criterion> criterions = new ArrayList<Criterion>();
					criterions.add(Restrictions.eq("commune.id", cbxCommune.getSelectedEntity().getId()));
					restrictions.setCriterions(criterions);
					//cbxVillage.renderer();
				} else {
					//cbxVillage.clear();
				}
			}
		});*/
	    
        final Panel detailPanel = new Panel(I18N.message("detail"));
        detailPanel.setSizeFull();
        final FormLayout formDetailPanel = new FormLayout();
        formDetailPanel.setMargin(true);
        formDetailPanel.setSizeFull();
        formDetailPanel.addComponent(cbxDealer);
        formDetailPanel.addComponent(cbxProvince);
        formDetailPanel.addComponent(cbxDistrict);
        formDetailPanel.addComponent(cbxCommune);
        //formDetailPanel.addComponent(cbxVillage);
        detailPanel.setContent(formDetailPanel);

        final VerticalLayout leftVerticalLayout = new VerticalLayout();
        leftVerticalLayout.setSizeFull();
        leftVerticalLayout.setSpacing(true);
        leftVerticalLayout.setMargin(true);
        leftVerticalLayout.addComponent(detailPanel);

		return leftVerticalLayout;
	}

	public void assignValues(Long delerId) {
		super.reset();
		if (delerId != null) {
			dealerMapAddress=ENTITY_SRV.getById(DealerMapAddress.class, delerId);
			cbxProvince.setSelectedEntity(dealerMapAddress.getProvince());
			cbxDistrict.setSelectedEntity(dealerMapAddress.getDistrict());
			cbxCommune.setSelectedEntity(dealerMapAddress.getCommune());
			//cbxVillage.setSelectedEntity(dealerMapAddress.getVillage());
			cbxDealer.setSelectedEntity(dealerMapAddress.getDealer());
		} else {
			reset();
		}
	}

	@Override
	public void reset() {
		super.reset();
		dealerMapAddress = new DealerMapAddress();
		cbxCommune.setSelectedEntity(null);
		cbxDistrict.setSelectedEntity(null);
		cbxProvince.setSelectedEntity(null);
		cbxDealer.setSelectedEntity(null);
		//cbxVillage.setSelectedEntity(null);
		markAsDirty();
	}

	@Override
	protected boolean validate() {
		boolean isValid = true;
		if (cbxCommune.getSelectedEntity()==null) {
			errors.add(I18N.message("field.required.1", I18N.message("commune")));
			isValid = false;
		}
		if (cbxDistrict.getSelectedEntity()==null) {
			errors.add(I18N.message("field.required.1", I18N.message("district")));
			isValid = false;
		}
		/*if (cbxVillage.getSelectedEntity()==null) {
			errors.add(I18N.message("field.required.1", I18N.message("village")));
			isValid = false;
		}*/
		if (cbxDealer.getSelectedEntity()==null) {
			errors.add(I18N.message("field.required.1", I18N.message("dealer")));
			isValid = false;
		}
		if (cbxProvince.getSelectedEntity()==null) {
			errors.add(I18N.message("field.required.1", I18N.message("province")));
			isValid = false;
		}
		return isValid;
	}
}
