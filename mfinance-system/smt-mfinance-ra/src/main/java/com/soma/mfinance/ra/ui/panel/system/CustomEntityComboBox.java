/*
package com.soma.mfinance.ra.ui.panel.system;

import com.soma.frmk.vaadin.ui.widget.combo.EntityComboBox;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EntityRefA;

import java.util.Iterator;
import java.util.List;

*/
/**
 * Created by ki.kao on 2/15/2017.
 *//*

class CustomEntityComboBox<T extends EntityRefA> extends EntityComboBox {

    private String representEmpty;

    public CustomEntityComboBox(Class entityClass, String caption) {
        super(entityClass, caption, null, null);
    }

    public CustomEntityComboBox(Class entityClass, String caption, String defaultSelected) {
        super(entityClass, caption, null, defaultSelected);
        this.representEmpty = defaultSelected;
    }

    @Override
    public void renderer(List entities) {
        this.setValue((Object) null);
        this.getValueMap().clear();
        this.removeAllItems();
        if (entities != null && !entities.isEmpty()) {
            if (this.representEmpty != null && !this.representEmpty.isEmpty()) {
                this.addItem("[empty]");
                this.setItemCaption("[empty]", this.representEmpty);
                this.setNullSelectionAllowed(false);
            }
            EntityRefA entity;
            for (Iterator var2 = entities.iterator(); var2.hasNext(); this.getValueMap().put(entity.getId().toString(), entity)) {
                entity = (EntityRefA) var2.next();
                this.addItem(entity.getId().toString());
                try {
                    this.setItemCaption(entity.getId().toString(), I18N.message(entity.getCode()));
                } catch (Exception var6) {
                    throw new IllegalStateException("Error on Field [" + entity.getCode() + "]", var6);
                }
            }
        }
    }
}
*/
