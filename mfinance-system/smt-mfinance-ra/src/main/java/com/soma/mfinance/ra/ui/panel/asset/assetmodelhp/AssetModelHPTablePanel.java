package com.soma.mfinance.ra.ui.panel.asset.assetmodelhp;

import com.soma.mfinance.core.asset.model.AssetModelHp;
import com.soma.mfinance.core.shared.asset.AssetEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.ui.Table.Align;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 
 * @author vi.sok
 * @since 18/03/2017
 *
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AssetModelHPTablePanel extends AbstractTablePanel<AssetModelHp> implements AssetEntityField {

	private static final long serialVersionUID = 7649753186581142898L;

	@PostConstruct
	public void PostConstruct() {
		setCaption(I18N.message("asset.model.hps"));
		setSizeFull();
		setMargin(true);
		setSpacing(true);
				
		super.init(I18N.message("asset.model.hps"));
		
		addDefaultNavigation();
	}	
		
	/**
	 * Get Paged definition
	 * @return
	 */
	@Override
	protected PagedDataProvider<AssetModelHp> createPagedDataProvider() {
		PagedDefinition<AssetModelHp> pagedDefinition = new PagedDefinition<AssetModelHp>(searchPanel.getRestrictions());
		pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Align.LEFT, 100);
		pagedDefinition.addColumnDefinition("assetModel."+ DESC_EN, I18N.message("asset.model"), String.class, Align.LEFT, 200);
		pagedDefinition.addColumnDefinition("hp."+ DESC_EN, I18N.message("hp"), String.class, Align.LEFT, 200);
		
		EntityPagedDataProvider<AssetModelHp> pagedDataProvider = new EntityPagedDataProvider<AssetModelHp>();
		pagedDataProvider.setPagedDefinition(pagedDefinition);
		return pagedDataProvider;
	}

		
	/**
	 * @see com.soma.frmk.vaadin.ui.panel.AbstractTablePanel#getEntity()
	 */
	@Override
	protected AssetModelHp getEntity() {
		final Long id = getItemSelectedId();
		if (id != null) {
		    return ENTITY_SRV.getById(AssetModelHp.class, id);
		}
		return null;
	}
	
	@Override
	protected AssetModelHPSearchPanel createSearchPanel() {
		return new AssetModelHPSearchPanel(this);		
	}
}
