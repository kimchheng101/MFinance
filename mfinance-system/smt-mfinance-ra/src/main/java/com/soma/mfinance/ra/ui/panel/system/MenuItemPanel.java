package com.soma.mfinance.ra.ui.panel.system;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Component;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * Created by ki.kao on 2/13/2017.
 */
@org.springframework.stereotype.Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(MenuItemPanel.NAME)
public class MenuItemPanel extends AbstractTabsheetPanel implements View {

    public static final String NAME = "add.menu.action";

    @Autowired
    private MenuItemTablePanel menuItemTablePanel;
    @Autowired
    private MenuItemFormAdd menuItemFormAdd;
    @Autowired
    private MenuItemFormUpdate menuItemFormUpdate;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        menuItemTablePanel.setMainPanel(this);
        menuItemFormAdd.setCaption(I18N.message("menu.add"));
        menuItemFormUpdate.setCaption(I18N.message("menu.edit"));
        getTabSheet().setTablePanel(menuItemTablePanel);
    }

    @Override
    public void onAddEventClick() {
        menuItemFormAdd.reset();
        getTabSheet().addFormPanel(menuItemFormAdd);
        getTabSheet().setSelectedTab(menuItemFormAdd);
    }

    @Override
    public void onEditEventClick() {
        getTabSheet().addFormPanel(menuItemFormUpdate);
        initSelectedTab(menuItemFormUpdate);
    }

    @Override
    public void initSelectedTab(Component component) {
        if (component == menuItemFormUpdate) {
            menuItemFormUpdate.assignValues(menuItemTablePanel.getEntity());
        } else if (component == menuItemTablePanel && getTabSheet().isNeedRefresh()) {
            menuItemTablePanel.refresh();
        }
        getTabSheet().setSelectedTab(component);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }

}
