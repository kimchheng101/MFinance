package com.soma.mfinance.ra.ui.panel.contract.migration;

import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.shared.conf.AppConfig;
import com.soma.mfinance.ra.ui.panel.finproduct.service.migrationvo.ImportContractServiceClient;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.soma.frmk.vaadin.util.i18n.I18N;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.StreamResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimpleTable;
import com.vaadin.server.StreamResource.StreamSource;
import org.seuksa.frmk.tools.spring.SpringUtils;

import java.io.FileInputStream;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ContractMigrationViewLogPopupPanel extends Window {

	/** */
    private static final long serialVersionUID = -1007946577337553819L;
	
	private SimpleTable<Contract> pagedTable;
	private List<ColumnDefinition> columnDefinitions;

	private ImportContractServiceClient importContract = SpringUtils.getBean(ImportContractServiceClient.class);
	
	public ContractMigrationViewLogPopupPanel() {
		setModal(true);
		setCaption(I18N.message("view.log"));
		
		this.columnDefinitions = createColumnDefinitions();
		pagedTable = new SimpleTable<Contract>(this.columnDefinitions);
		VerticalLayout tableLayout = new VerticalLayout();
		tableLayout.setSpacing(true);
		tableLayout.addComponent(pagedTable);
		
		VerticalLayout contentLayout = new VerticalLayout();
		contentLayout.setSpacing(true);
		contentLayout.addComponent(createNavigationPanel());
		contentLayout.addComponent(tableLayout);
		setIndexedContainer();
		
		setContent(contentLayout);
		setWidth(650, Unit.PIXELS);
		setHeight(450, Unit.PIXELS);
	}
	
	/**
	 * Create navigation panel
	 * @return
	 */
	private NavigationPanel createNavigationPanel() {
		NavigationPanel navigationPanel = new NavigationPanel();
		Button btnCancel = new NativeButton(I18N.message("close"), new Button.ClickListener() {
			/** */
			private static final long serialVersionUID = 5320362578430658080L;
			public void buttonClick(Button.ClickEvent event) {
            	close();
            }
		});
		btnCancel.setIcon(new ThemeResource("../smt-default/icons/16/delete.png"));
		
		navigationPanel.addButton(btnCancel);
		
		return navigationPanel;
	}
	
	/**
	 * Create Column Definitions
	 * @return
	 */
	private List<ColumnDefinition> createColumnDefinitions() {
		List<ColumnDefinition> columnDefinitions = new ArrayList<>();
		
		columnDefinitions.add(new ColumnDefinition("file.name", I18N.message("file.name"), String.class, Table.Align.LEFT, 400));
		columnDefinitions.add(new ColumnDefinition("download", I18N.message("download"), Button.class, Table.Align.CENTER, 150));
		
		return columnDefinitions;
	}
	
	/**
	 * Set indexed container
	 */
	@SuppressWarnings("unchecked")
	private void setIndexedContainer() {
		List<String> fileNames = importContract.getLogFileNames();
		Container container = pagedTable.getContainerDataSource();
		container.removeAllItems();
		int index = 0;
		for (final String fileName : fileNames) {
			Item item = container.addItem(index++);
			Button btnDownload = ComponentFactory.getButton("download");
			btnDownload.setIcon(FontAwesome.DOWNLOAD);
			StreamSource source = new StreamSource() {
	        	private static final long serialVersionUID = 3558447485541502851L;
				@Override
				public InputStream getStream() {
	                InputStream is = null;
					try {
						String documentDir = AppConfig.getInstance().getConfiguration().getString("document.path");
						//is = new FileInputStream(documentDir + "/" + ImportContractServiceClient.CONTRACT_MIGRATION_DIR + "/" + fileName);
						is = new FileInputStream(documentDir + "/" + ImportContractServiceClient.CONTRACT_MIGRATION_DIR + "/" + fileName);
					} catch (FileNotFoundException e) {
						is = null;
					}
	                return is;
				}
			};
			FileDownloader downloader = new FileDownloader(new StreamResource(source, fileName));
			downloader.extend(btnDownload);
			item.getItemProperty("file.name").setValue(fileName);
			item.getItemProperty("download").setValue(btnDownload);
		}					
	}

}
