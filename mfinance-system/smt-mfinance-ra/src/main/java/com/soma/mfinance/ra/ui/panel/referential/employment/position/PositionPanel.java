package com.soma.mfinance.ra.ui.panel.referential.employment.position;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * Create by Rith RON.
 * User r.ron.
 * Date on 2017-06-21.
 * Time at 2:11 PM.
 * Email r.ron@gl-f.com.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(PositionPanel.NAME)
public class PositionPanel extends AbstractTabsheetPanel implements View {
    public static final String NAME = "positions.list";

    @Autowired
    private PositionTablePanel employmentPositionTablePanel;
    @Autowired
    private PositionFormPanel employmentPositionFormPanel;

    @PostConstruct
    public void PostConstruct(){
        super.init();
        employmentPositionTablePanel.setMainPanel(this);
        employmentPositionFormPanel.setCaption(I18N.message("groups"));
        getTabSheet().setTablePanel(employmentPositionTablePanel);
    }

    @Override
    public void onAddEventClick() {
        employmentPositionFormPanel.reset();
        getTabSheet().addFormPanel(employmentPositionFormPanel);
        getTabSheet().setSelectedTab(employmentPositionFormPanel);
    }

    @Override
    public void onEditEventClick() {
        getTabSheet().addFormPanel(employmentPositionFormPanel);
        initSelectedTab(employmentPositionFormPanel);
    }

    @Override
    public void initSelectedTab(com.vaadin.ui.Component component) {
        if (component == employmentPositionFormPanel) {
            employmentPositionFormPanel.assignValues(employmentPositionTablePanel.getItemSelectedId());
        } else if (component == employmentPositionTablePanel && getTabSheet().isNeedRefresh()) {
            employmentPositionTablePanel.refresh();
        }
        getTabSheet().setSelectedTab(component);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}
