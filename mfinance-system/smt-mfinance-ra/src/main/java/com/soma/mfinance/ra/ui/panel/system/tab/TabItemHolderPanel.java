package com.soma.mfinance.ra.ui.panel.system.tab;

import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * Created by k.seang on 3/3/2017.
 */
@Component
@Scope("prototype")
@VaadinView(TabItemHolderPanel.NAME)
public class TabItemHolderPanel extends AbstractTabsheetPanel implements View{

    public static final String NAME = "tabitem.list";

    @Autowired
    private TabItemTablePanel tabItemTablePanel;
    @Autowired
    private TabItemFormPanel tabItemFormPanel;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        this.tabItemTablePanel.setMainPanel(this);
        this.tabItemTablePanel.setCaption(I18N.message("tab.item"));
        this.getTabSheet().setTablePanel(this.tabItemTablePanel);
    }
    @Override
    public void onAddEventClick() {
        this.tabItemFormPanel.reset();
        this.getTabSheet().addFormPanel(this.tabItemFormPanel);
        initSelectedTab(this.tabItemFormPanel);
    }

    @Override
    public void onEditEventClick() {
        this.getTabSheet().addFormPanel(this.tabItemFormPanel);
        this.initSelectedTab(this.tabItemFormPanel);
    }

    @Override
    public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
        if(selectedTab == this.tabItemFormPanel) {
            this.tabItemFormPanel.assignValues(this.tabItemTablePanel.getItemSelectedId());
        } else if(selectedTab == this.tabItemTablePanel && this.getTabSheet().isNeedRefresh()) {
            this.tabItemTablePanel.refresh();
        }
        this.getTabSheet().setSelectedTab(selectedTab);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }
}
