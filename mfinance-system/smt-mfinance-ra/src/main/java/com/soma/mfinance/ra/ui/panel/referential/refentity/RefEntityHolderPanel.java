package com.soma.mfinance.ra.ui.panel.referential.refentity;

import com.soma.ersys.vaadin.ui.referential.reftable.detail.RefTableFormPanel;
import com.soma.ersys.vaadin.ui.referential.reftable.list.RefTableDataFormRender;
import com.soma.ersys.vaadin.ui.referential.reftable.refdata.IRefTableDataForm;
import com.soma.ersys.vaadin.ui.referential.reftable.refdata.IRefTableDataFormRender;
import com.soma.ersys.vaadin.ui.referential.reftable.refdata.RefTableDataForm;
import com.soma.frmk.config.model.RefTable;
import com.soma.frmk.vaadin.ui.panel.AbstractTabsheetPanel;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.UI;
import org.seuksa.frmk.i18n.I18N;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.xpoft.vaadin.VaadinView;

import javax.annotation.PostConstruct;

/**
 * @author kim chheng
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(RefEntityHolderPanel.NAME)
public class RefEntityHolderPanel extends AbstractTabsheetPanel implements View {

    public static final String NAME = "refentity";
    private static final long serialVersionUID = 8254046231316566295L;
    private final Logger logger = LoggerFactory.getLogger(RefEntityHolderPanel.class);

    private IRefTableDataFormRender formRender;
    @Autowired
    private RefEntityTablePanel refEntityTablePanel;
    @Autowired
    private RefTableFormPanel refTableFormPanel;
    @Autowired
    private RefEntityDataTablePanel refEntityDataTablePanel;

    public RefEntityHolderPanel() {
    }

    @PostConstruct
    public void PostConstruct() {
        super.init();
        this.refEntityTablePanel.setMainPanel(this);
        this.refTableFormPanel.setCaption(I18N.message("detail"));
        this.refEntityDataTablePanel.setCaption(I18N.message("ref.table.entity"));
        this.getTabSheet().setTablePanel(this.refEntityTablePanel);
    }

    public void setFormRender(RefTableDataFormRender formRender) {
        this.formRender = formRender;
    }

    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }

    public void initSelectedTab(com.vaadin.ui.Component selectedTab) {
        if (selectedTab == this.refTableFormPanel) {
            this.refTableFormPanel.assignValues(this.refEntityTablePanel.getItemSelectedId());
        } else if (selectedTab == this.refEntityTablePanel && this.getTabSheet().isNeedRefresh()) {
            this.refEntityTablePanel.refresh();
        }

        this.getTabSheet().setSelectedTab(selectedTab);
    }

    public void onAddEventClick() {
    }

    private void addSubTab(Long selectedId, Class<?> clazz) {
        Object form = null;
        if (this.formRender != null) {
            form = this.formRender.renderForm(clazz);
        }

        if (form == null) {
            RefTable refTable = REFDATA_SRV.getTable(clazz.getCanonicalName());
            form = new RefTableDataForm(refTable);
        }

        this.refEntityDataTablePanel.assignValuesToControls(selectedId, (IRefTableDataForm) form);
        this.getTabSheet().addFormPanel(this.refEntityDataTablePanel);
    }

    public void onEditEventClick() {
        this.getTabSheet().addFormPanel(this.refTableFormPanel);

        try {
            this.addSubTab(this.refEntityTablePanel.getItemSelectedId(), this.refEntityTablePanel.getERefDataClass());
            this.initSelectedTab(this.refTableFormPanel);
        } catch (ClassNotFoundException var2) {
            this.logger.error(var2.getMessage(), var2);
        }

    }

    public void onViewSubTab() {
        if (this.refEntityTablePanel.getSelectedItem() == null) {
            MessageBox e = new MessageBox(UI.getCurrent(), "280px", "150px", I18N.message("information"), MessageBox.Icon.WARN, I18N.message("msg.info.view.item.not.selected"), Alignment.MIDDLE_RIGHT, new MessageBox.ButtonConfig[]{new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, I18N.message("ok"))});
            e.show();
        } else {
            this.getTabSheet().addFormPanel(this.refTableFormPanel);

            try {
                this.addSubTab(this.refEntityTablePanel.getItemSelectedId(), this.refEntityTablePanel.getERefDataClass());
                this.initSelectedTab(this.refEntityDataTablePanel);
            } catch (ClassNotFoundException var2) {
                this.logger.error(var2.getMessage(), var2);
            }
        }

    }

}