package com.soma.mfinance.ra.ui.panel.referential.committees.group;

import com.soma.mfinance.core.asset.model.AssetRange;
import com.soma.mfinance.core.custom.component.CustomEntityRefComboBox;
import com.soma.mfinance.core.referential.committee.GroupCommittee;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;

/**
 * @author by kimsuor.seang  on 9/27/2017.
 */
public class GroupCommitteeSearchPanel extends AbstractSearchPanel<GroupCommittee> implements FMEntityField {

    private TextField txtName;
    private TextField txtUserLogin;
    private EntityRefComboBox<AssetRange> cbxAssetRange;

    public GroupCommitteeSearchPanel(GroupCommitteeTablePanel groupCommitteeTablePanel) {
        super(I18N.message("search"), groupCommitteeTablePanel);
    }

    @Override
    protected void reset() {
        txtName.setValue("");
        txtUserLogin.setValue("");

    }

    @Override
    protected Component createForm() {
        final HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);

        txtName = ComponentFactory.getTextField("name.en", false, 100, 100);
        txtUserLogin = ComponentFactory.getTextField("login", false, 100, 100);

        cbxAssetRange = new CustomEntityRefComboBox<>();
        cbxAssetRange.setCaption(I18N.message("asset.make2"));
        cbxAssetRange.setRestrictions(new BaseRestrictions<>(AssetRange.class));
        cbxAssetRange.renderer();
        cbxAssetRange.setSelectedEntity(null);
        cbxAssetRange.setImmediate(true);

        horizontalLayout.addComponent(new FormLayout(txtName));
        horizontalLayout.addComponent(new FormLayout(txtUserLogin));
        horizontalLayout.addComponent(new FormLayout(cbxAssetRange));

        return horizontalLayout;
    }

    @Override
    public BaseRestrictions<GroupCommittee> getRestrictions() {

        BaseRestrictions<GroupCommittee> restrictions = new BaseRestrictions<>(GroupCommittee.class);
        restrictions.addAssociation("secUser", "user", JoinType.INNER_JOIN);
        if (StringUtils.isNotEmpty(txtName.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("user.desc", txtName.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtUserLogin.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("user.login", txtUserLogin.getValue(), MatchMode.ANYWHERE));
        }
        if (cbxAssetRange.getSelectedEntity() != null) {
            restrictions.addCriterion(Restrictions.eq("assetRange.id", cbxAssetRange.getSelectedEntity().getId()));
        }

        return restrictions;
    }

}
