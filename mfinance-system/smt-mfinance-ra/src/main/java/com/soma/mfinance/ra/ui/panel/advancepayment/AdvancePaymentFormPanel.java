package com.soma.mfinance.ra.ui.panel.advancepayment;

import com.soma.mfinance.core.payment.model.AdvancePayment;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by b.chea on 2/22/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AdvancePaymentFormPanel extends AbstractFormPanel {

    private AdvancePayment advancePayment;

    private CheckBox cbActive;
    private TextField txtDesc;
    private TextField txtDescEn;
    private TextField txtPercent;

    /** */
    @PostConstruct
    public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addSaveClickListener(this);
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractFormPanel#getEntity()
     */
    @Override
    protected Entity getEntity() {
        advancePayment.setDesc(txtDesc.getValue());
        advancePayment.setDescEn(txtDescEn.getValue());
        advancePayment.setAdvancePaymentPercent(txtPercent.getValue());
        advancePayment.setStatusRecord(cbActive.getValue() ? EStatusRecord.ACTIV : EStatusRecord.INACT);
        return advancePayment;
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractFormPanel#createForm()
     */
    @Override
    protected com.vaadin.ui.Component createForm() {
        final FormLayout formPanel = new FormLayout();
        txtDescEn = ComponentFactory.getTextField("desc.en", true, 60, 200);
        txtDesc = ComponentFactory.getTextField35("desc", false, 60, 200);
        txtPercent = ComponentFactory.getTextField35("advance.percent", false, 60, 200);
        cbActive = new CheckBox(I18N.message("active"));
        cbActive.setValue(true);
        formPanel.addComponent(txtDescEn);
        formPanel.addComponent(txtDesc);
        formPanel.addComponent(txtPercent);
        formPanel.addComponent(cbActive);
        return formPanel;
    }

    /**
     * @param advancePaymentId
     */
    public void assignValues(Long advancePaymentId) {
        super.reset();
        if (advancePaymentId != null) {
            advancePayment = ENTITY_SRV.getById(AdvancePayment.class, advancePaymentId);
            txtDescEn.setValue(advancePayment.getDescEn());
            txtDesc.setValue(advancePayment.getDesc());
            txtPercent.setValue(advancePayment.getAdvancePaymentPercent().toString());
            cbActive.setValue(advancePayment.getStatusRecord().equals(EStatusRecord.ACTIV));
        }
    }



    @Override
    public void saveButtonClick(Button.ClickEvent event) {
        try {
            if (super.validate()) {
                ENTITY_SRV.saveOrUpdate(this.getEntity());
                reset();
                this.displaySuccess();
            } else {
                this.displayErrors();
                this.processAfterError();
            }
        } catch (Exception e) {
            e.printStackTrace();
            ENTITY_SRV.getSessionFactory().getCurrentSession().getTransaction().rollback();
        }
    }

    /**
     * Reset
     * @see com.soma.frmk.vaadin.ui.panel.AbstractFormPanel#reset()
     */
    @Override
    public void reset() {
        super.reset();
        advancePayment = new AdvancePayment();
        txtDescEn.setValue("");
        txtDesc.setValue("");
        txtPercent.setValue("");
        cbActive.setValue(true);
        markAsDirty();
    }

    /**
     * Validate control
     * @see com.soma.frmk.vaadin.ui.panel.AbstractFormPanel#validate()
     */
    @Override
    protected boolean validate() {
        checkMandatoryField(txtDescEn, "desc.en");
        return errors.isEmpty();

    }
}
