package com.soma.mfinance.ra.ui.panel.referential.committees.committee;

import com.soma.mfinance.core.referential.committee.Committee;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;

/**
 * Created by Dang Dim
 * Date     : 19-Oct-17, 11:15 AM
 * Email    : d.dim@gl-f.com
 */
public class CommitteeSearchPanel extends AbstractSearchPanel<Committee> implements FMEntityField {

    private TextField txtCommittee;
    private TextField txtCode;
    private HorizontalLayout horizontalLayout;
    private FormLayout formLayout;

    public CommitteeSearchPanel(CommitteeTablePanel committeeTablePanel) {
        super(I18N.message("search"), committeeTablePanel);
    }

    @Override
    protected void reset() {
        txtCommittee.setValue("");
        txtCode.setValue("");
    }

    @Override
    protected Component createForm() {
        horizontalLayout = new HorizontalLayout();
        formLayout = new FormLayout();

        txtCommittee = ComponentFactory.getTextField("committee.name", false, 100, 200);
        txtCode = ComponentFactory.getTextField("code", false, 100, 200);

        formLayout.addComponent(txtCode);
        formLayout.addComponent(txtCommittee);

        horizontalLayout.addComponent(formLayout);
        horizontalLayout.setSpacing(true);
        return horizontalLayout;
    }

    @Override
    public BaseRestrictions<Committee> getRestrictions() {
        BaseRestrictions<Committee> restrictions = new BaseRestrictions<>(Committee.class);

        if (StringUtils.isNotEmpty(txtCommittee.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("committeeName", txtCommittee.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtCode.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("code", txtCode.getValue(), MatchMode.ANYWHERE));
        }

        return restrictions;
    }
}
