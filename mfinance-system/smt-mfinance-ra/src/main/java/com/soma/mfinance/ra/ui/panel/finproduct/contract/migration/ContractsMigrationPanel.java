package com.soma.mfinance.ra.ui.panel.finproduct.contract.migration;

import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.dealer.model.Dealer;
import com.soma.mfinance.core.dealer.model.EDealerType;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.mfinance.core.widget.DealerComboBox;
import com.soma.mfinance.ra.ui.panel.contract.migration.ContractMigrationViewLogPopupPanel;
import com.soma.mfinance.ra.ui.panel.finproduct.service.migrationvo.ImportContractServiceClient;
import com.soma.ersys.core.hr.model.address.Province;
import com.soma.frmk.vaadin.ui.panel.AbstractTabPanel;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.AutoDateField;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.dialog.MessageBox;
import com.soma.frmk.vaadin.ui.widget.table.ColumnDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.SimplePagedTable;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.Reindeer;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.vaadin.dialogs.ConfirmDialog;
import ru.xpoft.vaadin.VaadinView;

import java.util.*;

/**
 * Create by Rith RON.
 * User r.ron.
 * Date on 2017-05-08.
 * Time at 8:16 AM.
 * Email r.ron@gl-f.com
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@VaadinView(ContractsMigrationPanel.NAME)
public class ContractsMigrationPanel extends AbstractTabPanel implements View, QuotationEntityField {
    public static final String NAME = "migration";
    private TextField txtReference;
    private TextField txtFamilyNameEn;
    private TextField txtFirstNameEn;
    private AutoDateField dfStartDate;
    private AutoDateField dfEndDate;
    private TextField txtContractMonth;

    private CheckBox cbMigrated;
    private CheckBox cbNotMigrated;

    private TabSheet tabSheet;
    private Set<Long> selectedItemIds;
    private List<ColumnDefinition> columnDefinitions;
    private SimplePagedTable<Quotation> pagedTable;
    private boolean selectAll = false;
    private EntityRefComboBox<EDealerType> cbxDealerType;
    private ComboBox cbxDealer;
    private EntityRefComboBox<Province> cbxProvince;

    private ImportContractServiceClient importContract = SpringUtils.getBean(ImportContractServiceClient.class);

    public ContractsMigrationPanel() {
        super();
        setSizeFull();
        setMargin(false);
    }

    @Override
    protected com.vaadin.ui.Component createForm() {
        tabSheet = new TabSheet();

        VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.setSpacing(true);
        contentLayout.setMargin(true);
        contentLayout.setStyleName("panel-search");

        selectedItemIds = new HashSet<Long>();
        this.columnDefinitions = createColumnDefinitions();
        pagedTable = new SimplePagedTable<Quotation>(this.columnDefinitions);
        pagedTable.setCaption(I18N.message("contract"));
        pagedTable.setColumnIcon("selectall", new ThemeResource("../smt-default/icons/16/tick.png"));
        pagedTable.addHeaderClickListener(new com.vaadin.ui.Table.HeaderClickListener() {
            private static final long serialVersionUID = 5868260573740919228L;

            @Override
            public void headerClick(Table.HeaderClickEvent event) {
                if (event.getPropertyId() == "selectall") {
                    selectAll = !selectAll;
                    search();
                }
            }
        });

        contentLayout.addComponent(createNavigationPanel());
        contentLayout.addComponent(createSearchPanel());
        contentLayout.addComponent(pagedTable);
        contentLayout.addComponent(pagedTable.createControls());

        tabSheet.addTab(contentLayout, I18N.message("contract_old.migration"));
        return tabSheet;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }

    protected List<ColumnDefinition> createColumnDefinitions() {
        columnDefinitions = new ArrayList<ColumnDefinition>();
        columnDefinitions.add(new ColumnDefinition("selectall", I18N.message("select"), CheckBox.class, Table.Align.LEFT, 70));
        columnDefinitions.add(new ColumnDefinition(ID, I18N.message("id"), Long.class, Table.Align.LEFT, 70, true));
        columnDefinitions.add(new ColumnDefinition("migrated", I18N.message("migrated"), CheckBox.class, Table.Align.LEFT, 70));
        columnDefinitions.add(new ColumnDefinition(START_DATE, I18N.message("start.date"), Date.class, Table.Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition(REFERENCE, I18N.message("reference"), String.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition(END_DATE, I18N.message("start.date"), Date.class, Table.Align.LEFT, 100));
        columnDefinitions.add(new ColumnDefinition("term", I18N.message("renewal.term"), Integer.class, Table.Align.LEFT, 120));
        columnDefinitions.add(new ColumnDefinition("applicant." + LAST_NAME_EN, I18N.message("lastname.en"), String.class, Table.Align.LEFT, 120));
        columnDefinitions.add(new ColumnDefinition("applicant." + FIRST_NAME_EN, I18N.message("firstname.en"), String.class, Table.Align.LEFT, 120));
        columnDefinitions.add(new ColumnDefinition("asset." + DESC_EN, I18N.message("asset"), String.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition("financialProduct." + DESC_EN, I18N.message("financial.product"), String.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition("dealer." + NAME_EN, I18N.message("dealer"), String.class, Table.Align.LEFT, 150));
        columnDefinitions.add(new ColumnDefinition("insuranceCompany." + DESC_EN, I18N.message("insurance.company"), String.class, Table.Align.LEFT, 140));

        return columnDefinitions;
    }

    private NavigationPanel createNavigationPanel() {
        NavigationPanel navigationPanel = new NavigationPanel();

        Button btnSave = new NativeButton(I18N.message("save"));
        btnSave.setIcon(new ThemeResource("../smt-default/icons/16/save.png"));
        btnSave.addClickListener(new Button.ClickListener() {
            private static final long serialVersionUID = 582828827319591959L;

            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (selectedItemIds.isEmpty()) {
                    MessageBox mb = new MessageBox(UI.getCurrent(), "400px",
                            "160px", I18N.message("information"),
                            MessageBox.Icon.ERROR, I18N
                            .message("msg.info.contract_old.not.selected"),
                            Alignment.MIDDLE_RIGHT,
                            new MessageBox.ButtonConfig(MessageBox.ButtonType.OK, I18N
                                    .message("ok")));
                    mb.show();
                } else {
                    ConfirmDialog confirmDialog = ConfirmDialog.show(UI.getCurrent(), I18N.message("confirm.export.to.einsurance"),
                            new ConfirmDialog.Listener() {
                                private static final long serialVersionUID = 934662992018203665L;

                                public void onClose(ConfirmDialog dialog) {
                                    if (dialog.isConfirmed()) {
                                    /*
                                     * Process Migration
				                	 */
                                        String message = importContract.migrateQuotation(selectedItemIds);

                                        Panel msgPanel = new Panel();
                                        msgPanel.setStyleName(Reindeer.PANEL_LIGHT);
                                        msgPanel.setSizeFull();
                                        msgPanel.setContent(new Label(message.toString(), ContentMode.HTML));
                                        MessageBox mb = new MessageBox(UI.getCurrent(), "500px", "200px",
                                                I18N.message("information"), MessageBox.Icon.INFO, msgPanel,
                                                Alignment.MIDDLE_RIGHT, new MessageBox.ButtonConfig(
                                                MessageBox.ButtonType.OK, I18N.message("ok")));
                                        mb.show();
                                        refresh();
                                    }
                                }
                            });
                    confirmDialog.setWidth("400px");
                    confirmDialog.setHeight("150px");
                    confirmDialog.getOkButton().setDisableOnClick(true);
                }
            }
        });

        Button btnViewLog = new NativeButton(I18N.message("view.log"));
        btnViewLog.setIcon(FontAwesome.FILE);
        btnViewLog.addClickListener(new Button.ClickListener() {
            /** */
            private static final long serialVersionUID = -7598670497938819706L;

            /**
             * @see com.vaadin.ui.Button.ClickListener#buttonClick(com.vaadin.ui.Button.ClickEvent)
             */
            @Override
            public void buttonClick(Button.ClickEvent event) {
                UI.getCurrent().addWindow(new ContractMigrationViewLogPopupPanel());
            }
        });

        /*Button btnInsuranceStartDate = new NativeButton(I18N.message("generate.insurance.start.date"));
        btnInsuranceStartDate.setIcon(FontAwesome.EDIT);
        btnInsuranceStartDate.addClickListener(new Button.ClickListener() {
            *//** *//*
            private static final long serialVersionUID = 7491517337432835315L;
            *//**
         * @see com.vaadin.ui.Button.ClickListener#buttonClick(com.vaadin.ui.Button.ClickEvent)
         *//*
            @Override
            public void buttonClick(Button.ClickEvent event) {
                ConfirmDialog dialog = ConfirmDialog.show(UI.getCurrent(), I18N.message("msg.ask.generate.insurance.start.date"),
                        new ConfirmDialog.Listener() {
                            *//** *//*
                            private static final long serialVersionUID = 8536932422876182194L;

                            @Override
                            public void onClose(ConfirmDialog dialog) {
                                if (dialog.isConfirmed()) {
                                    importContract.generateInsuranceStartDate();
                                }
                            }
                        });
                dialog.setWidth("400px");
                dialog.setHeight("150px");
                dialog.getOkButton().setDisableOnClick(true);
            }
        });*/

        navigationPanel.addButton(btnSave);
        navigationPanel.addButton(btnViewLog);
//        navigationPanel.addButton(btnInsuranceStartDate);

        return navigationPanel;
    }

    public void refresh() {
        selectAll = false;
        search();
    }

    private void search() {
        selectedItemIds.clear();
        List<Quotation> quotations = ENTITY_SRV.list(getRestrictions());
        setIndexedContainer(quotations);
    }

    @SuppressWarnings("Duplicates")
    public BaseRestrictions<Quotation> getRestrictions() {
        BaseRestrictions<Quotation> restrictions = new BaseRestrictions<Quotation>(Quotation.class);

        restrictions.addCriterion(Restrictions.isNotNull("contract"));

        if (StringUtils.isNotEmpty(txtReference.getValue())) {
            restrictions.addCriterion(Restrictions.like(REFERENCE, txtReference.getValue(), MatchMode.ANYWHERE));
        }
        if (cbxDealerType.getSelectedEntity() != null) {
            restrictions.addAssociation("dealer", "quodeal", JoinType.INNER_JOIN);
            restrictions.addCriterion(Restrictions.eq("quodeal.dealerType", cbxDealerType.getSelectedEntity()));
        }
        if (cbxDealer.getId() != null) {
            restrictions.addCriterion(Restrictions.eq(DEALER + "." + ID, cbxDealer.getId()));
        }
        if (cbxProvince.getSelectedEntity() != null || StringUtils.isNotEmpty(txtFamilyNameEn.getValue())
                || StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
            restrictions.addAssociation("quotationApplicants", "quoapp", JoinType.INNER_JOIN);
            restrictions.addAssociation("quoapp.applicant", "app", JoinType.INNER_JOIN);
            restrictions.addCriterion("quoapp.applicantType", EApplicantType.C);
            restrictions.addAssociation("app.individual", "indi", JoinType.INNER_JOIN);
        }

        if (cbxProvince.getSelectedEntity() != null) {
            restrictions.addAssociation("indi.individualAddresses", "iadd", JoinType.INNER_JOIN);
            restrictions.addAssociation("iadd.address", "addr", JoinType.INNER_JOIN);
            restrictions.addAssociation("addr.province", "pro", JoinType.INNER_JOIN);
            restrictions.addCriterion("pro.id", cbxProvince.getSelectedEntity().getId());
        }

        if (StringUtils.isNotEmpty(txtFamilyNameEn.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("indi." + LAST_NAME_EN, txtFamilyNameEn.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtFirstNameEn.getValue())) {
            restrictions.addCriterion(Restrictions.ilike("indi." + FIRST_NAME_EN, txtFirstNameEn.getValue(), MatchMode.ANYWHERE));
        }

        if (dfStartDate.getValue() != null) {
            restrictions.addCriterion(Restrictions.ge("quotationDate", DateUtils.getDateAtBeginningOfDay(dfStartDate.getValue())));
        }

        if (dfEndDate.getValue() != null) {
            restrictions.addCriterion(Restrictions.le("quotationDate", DateUtils.getDateAtBeginningOfDay(dfEndDate.getValue())));
        }

        if (!cbMigrated.getValue() || !cbNotMigrated.getValue()) {
            if (cbMigrated.getValue()) {
                restrictions.addCriterion(Restrictions.isNotNull(QUOTATION_VA_ID));
                restrictions.addCriterion(Restrictions.isNotNull(CHECKER_ID));
            }
            if (cbNotMigrated.getValue()) {
                restrictions.addCriterion(Restrictions.isNull(QUOTATION_VA_ID));
            }
        }

        return restrictions;
    }

    private void setIndexedContainer(List<Quotation> quotations) {
        Container.Indexed indexedContainer = pagedTable.getContainerDataSource();
        indexedContainer.removeAllItems();
        int index = 0;
        for (Quotation quotation : quotations) {
            Item item = indexedContainer.addItem(quotation.getId());
            item.getItemProperty("selectall").setValue(getRenderSelected(quotation.getId(), index++));
            item.getItemProperty(ID).setValue(quotation.getId());
            item.getItemProperty(REFERENCE).setValue(quotation.getReference());

            Contract contract = quotation.getContract();
            Date contractStartDate = null;
            String insuranceCompany = "";
            int renewalTerm = 0;
            if (contract != null) {
                contractStartDate = contract.getStartDate();
                insuranceCompany = contract.getInsuranceCompany() != null ? contract.getInsuranceCompany().getDescEn() : "";
                Date insuranceStartDate = quotation.getStartCreationDate();
                renewalTerm = importContract.getRenewalTerm(insuranceStartDate, contractStartDate, 1);
            } else {
                renewalTerm = 1;
            }

            item.getItemProperty(START_DATE).setValue(contractStartDate);
            item.getItemProperty(END_DATE).setValue(contractStartDate);
            item.getItemProperty("migrated").setValue(getRenderMigrated(quotation.getCheckerID(), index));
            item.getItemProperty("term").setValue(renewalTerm);
            item.getItemProperty("applicant." + LAST_NAME_EN).setValue(quotation.getMainApplicant() != null ? quotation.getMainApplicant().getLastNameEn() : "");
            item.getItemProperty("applicant." + FIRST_NAME_EN).setValue(quotation.getMainApplicant() != null ? quotation.getMainApplicant().getFirstNameEn() : "");
            item.getItemProperty("asset." + DESC_EN).setValue(quotation.getAsset() != null ? quotation.getAsset().getDescEn() : "");
            item.getItemProperty("financialProduct." + DESC_EN).setValue(quotation.getFinancialProduct() != null ? quotation.getFinancialProduct().getDescEn() : "");
            item.getItemProperty("dealer." + NAME_EN).setValue(quotation.getDealer() != null ? quotation.getDealer().getNameEn() : "");
            item.getItemProperty("insuranceCompany." + DESC_EN).setValue(insuranceCompany);
        }
        pagedTable.refreshContainerDataSource();
    }

    private CheckBox getRenderSelected(final Long quotationId, final int index) {
        final CheckBox checkBox = new CheckBox();
        boolean check = false;
        if (selectAll) {
            if (!selectedItemIds.contains(quotationId)) {
                check = true;
                selectedItemIds.add(quotationId);
            }
        } else if (selectedItemIds.contains(quotationId)) {
            check = true;
        }
        checkBox.setValue(check);
        checkBox.setData(quotationId);
        checkBox.addValueChangeListener(new Property.ValueChangeListener() {
            private static final long serialVersionUID = 5571858003324968541L;

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                Long id = (Long) checkBox.getData();
                if (selectAll) {
                    if (!checkBox.getValue()) {
                        selectedItemIds.add(id);
                    } else {
                        selectedItemIds.remove(id);
                    }
                } else {
                    // clear to test only one quotation id
//                    selectedItemIds.clear();
                    if (checkBox.getValue()) {
                        selectedItemIds.add(id);
                    } else {
                        selectedItemIds.remove(id);
                    }
                }
            }
        });
        return checkBox;
    }

    private Panel createSearchPanel() {
        VerticalLayout searchLayout = new VerticalLayout();
        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setStyleName("panel-search-center");
        Button btnSearch = new Button(I18N.message("search"));
        btnSearch.setClickShortcut(ShortcutAction.KeyCode.ENTER, null); // null it means we don't modify key of shortcut Enter(default = 13)
        btnSearch.setIcon(new ThemeResource("../smt-default/icons/16/search.png"));
        btnSearch.addClickListener(new Button.ClickListener() {
            private static final long serialVersionUID = 582828827319591959L;

            @Override
            public void buttonClick(Button.ClickEvent event) {
                refresh();
            }
        });

        Button btnReset = new Button(I18N.message("reset"));
        btnReset.setIcon(new ThemeResource("../smt-default/icons/16/reset.png"));
        btnReset.addClickListener(new Button.ClickListener() {
            private static final long serialVersionUID = -7598670497938819706L;

            @Override
            public void buttonClick(Button.ClickEvent event) {
                reset();
            }
        });
        buttonsLayout.setSpacing(true);
        buttonsLayout.addComponent(btnSearch);
        buttonsLayout.addComponent(btnReset);

        final GridLayout gridLayout = new GridLayout(8, 4);
        gridLayout.setSpacing(true);
        txtReference = ComponentFactory.getTextField(false, 60, 200);
        txtFamilyNameEn = ComponentFactory.getTextField(false, 60, 200);
        txtFirstNameEn = ComponentFactory.getTextField(false, 60, 200);

        BaseRestrictions<Dealer> baseRDealer = new BaseRestrictions<>(Dealer.class);
        List<Dealer> dealerList = ENTITY_SRV.list(baseRDealer);
        cbxDealer = new DealerComboBox(dealerList);

        cbxDealerType = new EntityRefComboBox<>(EDealerType.values());
        cbxDealerType.setWidth(200.0F, Unit.PIXELS);

        cbxProvince = new EntityRefComboBox<Province>();
        cbxProvince.setRestrictions(new BaseRestrictions<Province>(Province.class));
        cbxProvince.renderer();

        dfStartDate = ComponentFactory.getAutoDateField("", false);
        dfStartDate.setValue(DateUtils.today());
        dfEndDate = ComponentFactory.getAutoDateField("", false);
        dfEndDate.setValue(DateUtils.today());

        txtContractMonth = ComponentFactory.getTextField(false, 60, 200);

        cbMigrated = new CheckBox(I18N.message("migrated"));
        cbMigrated.setValue(true);

        cbNotMigrated = new CheckBox(I18N.message("not.migrated"));
        cbNotMigrated.setValue(true);

        GridLayout statusGridLayout = new GridLayout(2, 1);
        statusGridLayout.setWidth(100, Unit.PERCENTAGE);
        statusGridLayout.addComponent(cbMigrated);
        statusGridLayout.addComponent(cbNotMigrated);

        int iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("reference")), iCol++, 0);
        gridLayout.addComponent(txtReference, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("dealer.type")), iCol++, 0);
        gridLayout.addComponent(cbxDealerType, iCol++, 0);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 0);
        gridLayout.addComponent(new Label(I18N.message("dealer")), iCol++, 0);
        gridLayout.addComponent(cbxDealer, iCol++, 0);

        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("lastname.en")), iCol++, 1);
        gridLayout.addComponent(txtFamilyNameEn, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("firstname.en")), iCol++, 1);
        gridLayout.addComponent(txtFirstNameEn, iCol++, 1);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 1);
        gridLayout.addComponent(new Label(I18N.message("province")), iCol++, 1);
        gridLayout.addComponent(cbxProvince, iCol++, 1);

        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("start.date")), iCol++, 2);
        gridLayout.addComponent(dfStartDate, iCol++, 2);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 2);
        gridLayout.addComponent(new Label(I18N.message("end.date")), iCol++, 2);
        gridLayout.addComponent(dfEndDate, iCol++, 2);

        iCol = 0;
        gridLayout.addComponent(new Label(I18N.message("contract_old.month")), iCol++, 3);
        gridLayout.addComponent(txtContractMonth, iCol++, 3);
        gridLayout.addComponent(ComponentFactory.getSpaceLayout(10, Unit.PIXELS), iCol++, 3);
        gridLayout.addComponent(new Label(), iCol++, 3);
        gridLayout.addComponent(statusGridLayout, iCol++, 3);

        searchLayout.setMargin(true);
        searchLayout.setSpacing(true);
        searchLayout.addComponent(gridLayout);
        searchLayout.addComponent(buttonsLayout);

        Panel searchPanel = new Panel();
        searchPanel.setCaption(I18N.message("search"));
        searchPanel.setContent(searchLayout);

        return searchPanel;
    }

    private CheckBox getRenderMigrated(final String migratedReference, final int index) {
        final CheckBox checkBox = new CheckBox();
        boolean check = migratedReference != null ? true : false;
        checkBox.setValue(check);
        checkBox.setEnabled(false);
        return checkBox;
    }
}
