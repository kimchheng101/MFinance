package com.soma.mfinance.ra.ui.panel.advancepayment;

import com.soma.mfinance.core.payment.model.AdvancePayment;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.vaadin.ui.Table;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by b.chea on 2/22/2017.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AdvancePaymentTablePanel extends AbstractTablePanel<AdvancePayment> implements FMEntityField {

    @PostConstruct
    public void PostConstruct() {
        setCaption(I18N.message("asset.engines.status"));
        setSizeFull();
        setMargin(true);
        setSpacing(true);

        super.init(I18N.message("asset.engines.status"));

        addDefaultNavigation();
    }

    /**
     * Get Paged definition
     *
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTablePanel#createPagedDataProvider()
     */
    @Override
    protected PagedDataProvider<AdvancePayment> createPagedDataProvider() {
        PagedDefinition<AdvancePayment> pagedDefinition = new PagedDefinition<AdvancePayment>(searchPanel.getRestrictions());
        pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Table.Align.LEFT, 100);
        pagedDefinition.addColumnDefinition(DESC_EN, I18N.message("desc.en"), String.class, Table.Align.LEFT, 200);
        pagedDefinition.addColumnDefinition(DESC, I18N.message("desc"), String.class, Table.Align.LEFT, 200);
        pagedDefinition.addColumnDefinition(ADVANCE_PAYMENT_PERCENT, I18N.message("advance.percent"), String.class, Table.Align.LEFT, 200);
        EntityPagedDataProvider<AdvancePayment> pagedDataProvider = new EntityPagedDataProvider<AdvancePayment>();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTablePanel#getEntity()
     */
    @Override
    protected AdvancePayment getEntity() {
        final Long id = getItemSelectedId();
        if (id != null) {
            return ENTITY_SRV.getById(AdvancePayment.class, id);
        }
        return null;
    }

    /**
     * @see com.soma.frmk.vaadin.ui.panel.AbstractTablePanel#createSearchPanel()
     */
    @Override
    protected AdvancePaymentSearchPanel createSearchPanel() {
        return new AdvancePaymentSearchPanel(this);
    }

}
