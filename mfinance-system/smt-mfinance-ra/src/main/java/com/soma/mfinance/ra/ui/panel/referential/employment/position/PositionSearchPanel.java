package com.soma.mfinance.ra.ui.panel.referential.employment.position;

import com.soma.mfinance.core.application.model.EmploymentOccupation;
import com.soma.mfinance.core.application.model.EmploymentPosition;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.ersys.core.hr.model.eref.EEmploymentIndustry;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EntityRefComboBox;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.data.Property;
import com.vaadin.ui.*;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Create by Rith RON.
 * User r.ron.
 * Date on 2017-06-21.
 * Time at 2:12 PM.
 * Email r.ron@gl-f.com.
 */
public class PositionSearchPanel extends AbstractSearchPanel<EmploymentPosition> implements FMEntityField {
    private CheckBox cbActive;
    private CheckBox cbInactive;
    private TextField txtCode;
    private TextField txtDescEn;
    private EntityRefComboBox<EmploymentOccupation> cbxEmploymentOccupation;
    private ERefDataComboBox<EEmploymentIndustry> cbxEmploymentIndustry;
    public PositionSearchPanel(String caption, AbstractTablePanel<EmploymentPosition> tablePanel) {
        super(caption, tablePanel);
    }

    public PositionSearchPanel(String caption, AbstractTablePanel<EmploymentPosition> tablePanel, Class<EmploymentPosition> searchEntityClass) {
        super(caption, tablePanel, searchEntityClass);
    }

    public PositionSearchPanel(PositionTablePanel components) {
        super(I18N.message("search"),components);
    }

    @Override
    protected void reset() {
        txtCode.setValue("");
        txtDescEn.setValue("");
    }

    @Override
    protected Component createForm() {
        final GridLayout gridLayout = new GridLayout(6, 2);
        txtCode = ComponentFactory.getTextField("code", false, 60, 200);
        txtDescEn = ComponentFactory.getTextField("desc.en", false, 60, 200);
        cbActive = new CheckBox(I18N.message("active"));
        cbActive.setValue(true);
        cbInactive = new CheckBox(I18N.message("inactive"));
        cbInactive.setValue(false);
        cbxEmploymentIndustry = new ERefDataComboBox<EEmploymentIndustry>(I18N.message("employment.industry"), EEmploymentIndustry.class);

        cbxEmploymentIndustry.addValueChangeListener(new Property.ValueChangeListener() {

            private static final long serialVersionUID = 6740681769253190853L;

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if (cbxEmploymentIndustry.getSelectedEntity() != null) {
                    BaseRestrictions<EmploymentOccupation> restrictions = cbxEmploymentOccupation.getRestrictions();
                    List<Criterion> criterions = new ArrayList<Criterion>();
                    criterions.add(Restrictions.eq("employmentIndustry", cbxEmploymentIndustry.getSelectedEntity()));
                    criterions.add(Restrictions.eq("statusRecord", EStatusRecord.ACTIV));
                    restrictions.setCriterions(criterions);
                    cbxEmploymentOccupation.renderer();
                } else {
                    cbxEmploymentOccupation.clear();
                }
            }
        });

        cbxEmploymentOccupation = new EntityRefComboBox<>(I18N.message("occupation"));
        cbxEmploymentOccupation.setRestrictions(new BaseRestrictions<>(EmploymentOccupation.class));
        cbxEmploymentOccupation.renderer();
        cbxEmploymentOccupation.setImmediate(true);
        cbxEmploymentOccupation.setWidth("200px");

        gridLayout.setSpacing(true);
        gridLayout.addComponent(new FormLayout(txtCode), 0, 0);
        gridLayout.addComponent(new FormLayout(txtDescEn), 1, 0);
        gridLayout.addComponent(new FormLayout(cbxEmploymentIndustry), 2, 0);
        gridLayout.addComponent(new FormLayout(cbxEmploymentOccupation), 3, 0);
        gridLayout.addComponent(new FormLayout(cbActive), 0, 1);
        gridLayout.addComponent(new FormLayout(cbInactive), 1, 1);

        return gridLayout;
    }

    @Override
    public BaseRestrictions<EmploymentPosition> getRestrictions() {
        BaseRestrictions<EmploymentPosition> restrictions = new BaseRestrictions<EmploymentPosition>(EmploymentPosition.class);
        List<Criterion> criterions = new ArrayList<Criterion>();
        if (StringUtils.isNotEmpty(txtCode.getValue())) {
            criterions.add(Restrictions.like(CODE, txtCode.getValue(), MatchMode.ANYWHERE));
        }
        if (StringUtils.isNotEmpty(txtDescEn.getValue())) {
            criterions.add(Restrictions.like(DESC_EN, txtDescEn.getValue(), MatchMode.ANYWHERE));
        }

        if (cbxEmploymentIndustry.getSelectedEntity() != null) {
            criterions.add(Restrictions.eq("employmentIndustry", cbxEmploymentIndustry.getSelectedEntity()));
        }

        if (cbxEmploymentOccupation.getSelectedEntity() != null) {
            restrictions.addAssociation("employmentOccupation", "occup", JoinType.INNER_JOIN);
            restrictions.addCriterion("occup.id", cbxEmploymentOccupation.getSelectedEntity().getId());
        }
        if (!cbActive.getValue() && !cbInactive.getValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
            restrictions.getStatusRecordList().add(EStatusRecord.INACT);
        }
        if (cbActive.getValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.ACTIV);
        }
        if (cbInactive.getValue()) {
            restrictions.getStatusRecordList().add(EStatusRecord.INACT);
        }
        restrictions.setCriterions(criterions);
        restrictions.addOrder(Order.asc(DESC_EN));
        return restrictions;
    }
}
