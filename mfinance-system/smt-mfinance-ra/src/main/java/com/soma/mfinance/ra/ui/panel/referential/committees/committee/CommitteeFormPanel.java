package com.soma.mfinance.ra.ui.panel.referential.committees.committee;

import com.soma.mfinance.core.referential.committee.Committee;
import com.soma.frmk.vaadin.ui.panel.AbstractFormPanel;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.soma.frmk.vaadin.ui.widget.toolbar.NavigationPanel;
import com.vaadin.ui.*;
import org.seuksa.frmk.model.entity.Entity;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;

/**
 * Created by Dang Dim
 * Date     : 19-Oct-17, 11:04 AM
 * Email    : d.dim@gl-f.com
 */

@org.springframework.stereotype.Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CommitteeFormPanel extends AbstractFormPanel {

    private TextField txtCode;
    private TextField txtCommitteeName;
    private TextArea txtDescription;

    public Committee committee;
    public HorizontalLayout horizontalLayout;
    public FormLayout formLayout;

    @PostConstruct
    public void PostConstruct() {
        super.init();
        NavigationPanel navigationPanel = addNavigationPanel();
        navigationPanel.addSaveClickListener(this);
    }

    @Override
    protected Component createForm() {

        formLayout = new FormLayout();

        txtCode = ComponentFactory.getTextField("Code", true, 100, 200);
        txtCommitteeName = ComponentFactory.getTextField("committee.name", true, 100, 200);
        txtDescription = ComponentFactory.getTextArea("desc", true, 200, 50);

        formLayout.addComponent(txtCode);
        formLayout.addComponent(txtCommitteeName);
        formLayout.addComponent(txtDescription);

        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(formLayout);
        horizontalLayout.setSpacing(true);

        return horizontalLayout;
    }

    @Override
    protected Entity getEntity() {
        committee.setCode(txtCode.getValue());
        committee.setDesc(txtCommitteeName.getValue());
        committee.setDescEn(txtDescription.getValue());

        return committee;
    }


    @Override
    public void reset() {
        super.reset();
        committee = new Committee();
        txtCommitteeName.setValue("");
        txtDescription.setValue("");
    }

    public void assignValue(Long id) {
        if (id != null) {
            txtCode.setValue(committee.getCode());
            txtCommitteeName.setValue(committee.getDesc());
            txtDescription.setValue(committee.getDescEn());
        }

    }

    @Override
    protected boolean validate() {
        checkMandatoryField(txtCode, "code");
        checkMandatoryField(txtCommitteeName, "Committee Name");
        checkMandatoryField(txtDescription, "Description");
        return errors.isEmpty();
    }
}
