package com.soma.mfinance.ra.ui.panel.system.tab;

import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.mfinance.core.tab.model.TabItemEntity;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.panel.AbstractTablePanel;
import com.soma.frmk.vaadin.ui.widget.table.PagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.table.PagedDefinition;
import com.soma.frmk.vaadin.ui.widget.table.impl.EntityPagedDataProvider;
import com.soma.frmk.vaadin.ui.widget.toolbar.event.DeleteClickListener;
import com.soma.frmk.vaadin.ui.widget.toolbar.event.SearchClickListener;
import com.soma.frmk.vaadin.util.VaadinServicesHelper;
import com.vaadin.ui.Table;
import org.seuksa.frmk.i18n.I18N;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by k.seang on 3/3/2017.
 */
@Component
@Scope("prototype")
public class TabItemTablePanel extends AbstractTablePanel<TabItemEntity> implements DeleteClickListener, SearchClickListener, VaadinServicesHelper,FMEntityField {

    @PostConstruct
    public void PostConstruct() {
        this.setCaption(I18N.message("tab.items"));
        this.setSizeFull();
        this.setMargin(true);
        this.setSpacing(true);
        super.init(I18N.message("tab.items"));
        this.addDefaultNavigation();
    }


    @Override
    protected TabItemEntity getEntity() {
        if(this.selectedItem != null) {
            Long id = this.getItemSelectedId();
            if(id != null) {
                return (TabItemEntity)ENTITY_SRV.getById(TabItemEntity.class, id);
            }
        }
        return null;
    }

    @Override
    protected PagedDataProvider<TabItemEntity> createPagedDataProvider() {
        PagedDefinition pagedDefinition = new PagedDefinition(this.searchPanel.getRestrictions());
        pagedDefinition.addColumnDefinition(ID, I18N.message("id"), Long.class, Table.Align.LEFT, 70);
        pagedDefinition.addColumnDefinition(CODE, I18N.message("code"), String.class, Table.Align.LEFT, 150);
        pagedDefinition.addColumnDefinition(DESC, I18N.message("desc"), String.class, Table.Align.LEFT, 250);
        EntityPagedDataProvider pagedDataProvider = new EntityPagedDataProvider();
        pagedDataProvider.setPagedDefinition(pagedDefinition);
        return pagedDataProvider;

    }

    @Override
    protected AbstractSearchPanel<TabItemEntity> createSearchPanel() {
        return new TabItemSearchPanel(this);
    }
}
