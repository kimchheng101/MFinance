package com.soma.mfinance.ra.ui.panel.referential.employment.occupation;

import com.soma.mfinance.core.application.model.EmploymentOccupation;
import com.soma.mfinance.core.shared.FMEntityField;
import com.soma.ersys.core.hr.model.eref.EEmploymentIndustry;
import com.soma.frmk.vaadin.ui.panel.AbstractSearchPanel;
import com.soma.frmk.vaadin.ui.widget.combo.ERefDataComboBox;
import com.soma.frmk.vaadin.ui.widget.combo.EStatusRecordOptionGroup;
import com.soma.frmk.vaadin.ui.widget.component.ComponentFactory;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.seuksa.frmk.dao.hql.BaseRestrictions;
import org.seuksa.frmk.i18n.I18N;
import org.seuksa.frmk.model.entity.EStatusRecord;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Pisethraingsey SOUN
 * Date     : 23/06/2017, 9:42 AM
 * Email    : p.suon@gl-f.com
 */
public class EmploymentOccupationSearchPanel extends AbstractSearchPanel<EmploymentOccupation> implements FMEntityField {

    private static final long serialVersionUID = 2412148308425964134L;
    private TextField txtCode;
    private TextField txtDesc;
    private ERefDataComboBox<EEmploymentIndustry> cbxIndustry;
    private EStatusRecordOptionGroup cbStatuses;

    public EmploymentOccupationSearchPanel(EmploymentOccupationTablePanel tablePanel) {
        super(I18N.message("search"), tablePanel);
    }

    @Override
    protected void reset() {
        this.txtCode.setValue("");
        this.txtDesc.setValue("");
        this.cbStatuses.setDefaultValue();
    }

    @Override
    protected Component createForm() {
        GridLayout gridLayout = new GridLayout(4, 1);
        this.txtCode = ComponentFactory.getTextField("code", true, 60, 180);
        this.txtDesc = ComponentFactory.getTextField("desc", false, 60, 180);
        this.cbStatuses = new EStatusRecordOptionGroup();
        this.cbxIndustry = new ERefDataComboBox(I18N.message("employment.industry"), EEmploymentIndustry.values());
        this.cbxIndustry.setRequired(true);
        this.cbxIndustry.setWidth(180.0F, Unit.PIXELS);

        gridLayout.setSpacing(true);
        gridLayout.addComponent(new FormLayout(new Component[]{this.txtCode}));
        gridLayout.addComponent(new FormLayout(new Component[]{this.txtDesc}));
        gridLayout.addComponent(new FormLayout(new Component[]{this.cbxIndustry}));
        gridLayout.addComponent(new FormLayout(new Component[]{this.cbStatuses}));

        return gridLayout;
    }

    @Override
    public BaseRestrictions<EmploymentOccupation> getRestrictions() {
        BaseRestrictions<EmploymentOccupation> restrictions = new BaseRestrictions<>(EmploymentOccupation.class);
        List<Criterion> criterions = new ArrayList<Criterion>();

        if (StringUtils.isNotEmpty(txtCode.getValue())) {
            criterions.add(Restrictions.eq(CODE, txtCode.getValue()));
        }
        if (StringUtils.isNotEmpty(txtDesc.getValue())) {
            criterions.add(Restrictions.eq(DESC, txtDesc.getValue()));
        }
        if (cbxIndustry.getSelectedEntity() != null) {
            criterions.add(Restrictions.eq(EMPLOYMENT_INDUSTRY, cbxIndustry.getSelectedEntity()));
        }

        Collection<EStatusRecord> colSta = (Collection) this.cbStatuses.getValue();
        restrictions.setStatusRecordList(new ArrayList(colSta));
        restrictions.setCriterions(criterions);
        restrictions.addOrder(Order.desc(DESC));
        return restrictions;
    }
}
