package com.soma.mfinance.accounting;

import com.soma.frmk.security.SecurityEntityFactory;
import com.soma.frmk.security.SecurityHelper;
import com.soma.frmk.security.model.SecApplication;
import com.soma.frmk.security.model.SecControl;
import com.soma.frmk.security.model.SecProfile;
import com.soma.frmk.security.model.SecUser;
import com.soma.frmk.security.service.AuthenticationService;
import com.soma.frmk.security.service.SecurityService;
import com.soma.frmk.testing.BaseSecurityTestCase;
import org.springframework.util.Assert;

import java.util.List;

/**
 * @author prasnar
 */
public class TestUserMigration extends BaseSecurityTestCase {

   // private static final String EFINANCE_APP = FinSecurity.EFINANCE_APP;
    private static final String EFINANCE_RA = "EFINANCE_RA";
   // private static final String EFINANCE_TM = FinSecurity.EFINANCE_TM;

    /**
     *
     */
    public TestUserMigration() {
        requiredSecApplicationContext = false;
    }

    /**
     * @return
     */
    protected boolean isRequiredAuhentication() {
        return false;
    }

    @Override
    protected void setAuthentication() {
        login = "mengtang";
        password = "11";
    }


    /**
     *
     */
    public void testCreateUsers() {

        String errMsg = null;
        /*try {

            List<TmpUser> lstTmpUserVOs = ENTITY_SRV.list(TmpUser.class);
            SecUser tmp = SECURITY_SRV.loadUserByUsername("mengtang");
            Authentication auth = new UsernamePasswordAuthenticationToken(tmp, tmp.getPassword(), tmp.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(auth);
            for (TmpUser userVo : lstTmpUserVOs) {
                SecProfile pro = ENTITY_SRV.getByCode(SecProfile.class, userVo.getProfileCode());
                if (pro == null) {
                    errMsg = "Can not find profile [" + userVo.getProfileCode() + "]";
                    throw new IllegalStateException(errMsg);
                }
                SecUser secUser = SecurityEntityFactory.createInstance(SecUser.class, "mengtang");
                secUser.setLogin(userVo.getLogin());
                secUser.setDesc(userVo.getDesc());
                secUser.setDefaultProfile(pro);

                secUser = SECURITY_SRV.createUser(secUser, userVo.getPwd());
                ENTITY_SRV.saveOrUpdate(secUser);
                if (secUser.getId() == null) {
                    userVo.setDealer(userVo.getId() + "----No .... dealer.");
                    ENTITY_SRV.saveOrUpdate(secUser);
                    continue;
                }
                if (pro == null) {
                    userVo.setDealer("can not found form mpf dealer");
                    ENTITY_SRV.saveOrUpdate(userVo);
                    continue;
                }
                if (!StringUtils.isEmpty(userVo.getDealer())) {
                    // find delere
                    SecUserDetail detail = new SecUserDetail();
                    BaseRestrictions<Dealer> restrictions = new BaseRestrictions<>(Dealer.class);
                    restrictions.addCriterion("nameEn", userVo.getDealer());
                    List<Dealer> list = ENTITY_SRV.list(restrictions);
                    detail.setDealer(list.get(0));
                    detail.setSecUser(secUser);

                    ENTITY_SRV.saveOrUpdate(detail);
                }

            }

        } catch (Exception e) {
            errMsg = e.getMessage();
            logger.error(errMsg, e);
            throw new IllegalStateException(errMsg);
        }*/
    }

    /**
     * Build the default security
     */
    public void xtestBuildDefaultSecurity() {
        /*try {
            FinSecurity.execBuildDefaultSecurity();

            logger.info("************SUCCESS**********");

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }*/
    }

    public void xxxxtestCreateUser() {
        try {
            String username = "admin";
            String password = "admin";
            SecUser secUser = SECURITY_SRV.loadUserByUsername(username);
//			SecUser secUser = authSrv.authenticate(username, password);
            Assert.isNull(secUser, "[" + username + "] already existed.");
            secUser = SecurityEntityFactory.createInstance(SecUser.class, "adminCreator");
            secUser.setLogin(username);
            secUser.setDesc(username);

            SECURITY_SRV.createUser(secUser, password);

            Assert.notNull(secUser, "The creation of [" + username + "] has failed.");

            logger.info("Digested password [" + secUser.getPassword() + "]");
            logger.info("Salt password [" + secUser.getPasswordSalt() + "]");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     *
     */
    public void xxtestChangePasswordUser() {
        try {
            String username = "admin";
            String oldPassword = "admin";
            String newPassword = "admin";
            SecurityService SECURITY_SRV = getBean(SecurityService.class);
            SecUser secUser = SECURITY_SRV.loadUserByUsername(username);
            Assert.notNull(secUser, "[" + username + "] does not exist.");

            SECURITY_SRV.changePasswordSalt(secUser);
            SECURITY_SRV.changePassword(secUser, oldPassword, newPassword);

            Assert.notNull(secUser, "The creation of [" + username + "] has failed.");

            logger.info("New digested password [" + secUser.getPassword() + "]");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     *
     */
    public void testChangePasswordUserWithoutOldPassword() {
        try {
            String username = "admin";
            String newPassword = "admin";
            SecurityHelper.changeUserPassword(username, newPassword);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * SecUser authentication
     */
    public void xxtestAuthentication() {
        try {
            String username = "admin";
            String password = "admin";
            AuthenticationService authSrv = getBean(AuthenticationService.class);
            //SecUser secUser = authSrv.loadUserByUsername(username);
            SecUser secUser = authSrv.authenticate(username, password);

            Assert.notNull(secUser, "[" + username + "] can not be authenticated.");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }


    /**
     *
     */
    public void xxtestProfileControls() {
        try {
            SecApplication app = ENTITY_SRV.getByCode(SecApplication.class, EFINANCE_RA);
            List<SecControl> controls = SECURITY_SRV.listControlsByAppIdAndProfile(app.getId(), SecProfile.ADMIN.getId());
            for (SecControl ctl : controls) {
                logger.info("- [" + ctl.getId() + "] " + ctl.getCode());
            }

            logger.info("*********SUCESS************");
            logger.info("************SUCCESS**********");

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }


}
