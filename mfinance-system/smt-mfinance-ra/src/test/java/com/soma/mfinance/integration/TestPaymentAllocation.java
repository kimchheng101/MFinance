package com.soma.mfinance.integration;

import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.payment.model.PaymentFile;
import com.soma.mfinance.core.shared.cashflow.CashflowEntityField;
import com.soma.frmk.testing.BaseTestCase;

/**
 * Test Payment Allocation
 * @author kimsuor.seang
 */
public class TestPaymentAllocation extends BaseTestCase implements FinServicesHelper, CashflowEntityField {
	
	public TestPaymentAllocation() {
	}
	
	/**
	 * @see com.soma.frmk.testing.BaseTestCase#isRequiredAuhentication()
	 */
	@Override
	protected boolean isRequiredAuhentication() {
		return false;
	}
	
	/**
	 * @see com.soma.frmk.testing.BaseTestCase#setAuthentication()
	 */
	@Override
	protected void setAuthentication() {
		login = "admin";
		password = "admin@EFIN";
	}
	
	/**
	 */
	public void testPaymentAllocation() {
		PaymentFile paymentFile = PAYMENT_ALLOCATION_SRV.getById(PaymentFile.class, 48l);
		PAYMENT_ALLOCATION_SRV.allocatePayments(paymentFile);
	}

}
