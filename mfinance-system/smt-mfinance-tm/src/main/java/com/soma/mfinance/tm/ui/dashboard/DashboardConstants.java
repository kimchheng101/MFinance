package com.soma.mfinance.tm.ui.dashboard;

/**
 * 
 * @author kimsuor.seang
 *
 */
public interface DashboardConstants {
	public static final String EVT_DASHBOARD = "evt.dashboard";
}
