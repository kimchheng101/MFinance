package com.soma.mfinance.batch;

import com.soma.common.app.tools.helper.AppConfigFileHelper;
import com.soma.common.app.tools.helper.AppSettingConfigHelper;
import com.soma.common.app.web.spring.StartupApplicationContextListener;
import com.soma.mfinance.core.system.DataReferenceService;
import com.soma.frmk.config.AppConfigFile;
import com.soma.frmk.config.service.RefDataService;
import com.soma.frmk.config.service.SettingService;
import com.soma.frmk.helper.FrmkServicesHelper;
import com.soma.frmk.security.SecurityHelper;
import com.soma.frmk.security.context.SecApplicationContext;
import com.soma.frmk.security.context.SecApplicationContextHolder;
import com.soma.frmk.security.model.SecApplication;
import com.soma.frmk.security.service.SecurityService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.seuksa.frmk.service.BaseEntityService;
import org.seuksa.frmk.service.EntityService;
import org.seuksa.frmk.tools.spring.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate5.SessionFactoryUtils;
import org.springframework.orm.hibernate5.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.util.StringUtils;

import java.util.Locale;

/**
 * Created by Kimsuor SEANG
 * Date  : 11/28/2017
 * Name  : 8:57 AM
 * Email : k.seang@gl-f.com
 */
public class BatchSession extends StartupApplicationContextListener implements FrmkServicesHelper {
    protected static Logger logger = LoggerFactory.getLogger(BatchSession.class.getSimpleName());

    private final String DEFAULT_APP_CONFIGURATION_FILE = "app-config.properties";
    private final String DEFAULT_I18N_FILE = "MessagesI18n";
    private final String DEFAULT_MAIN_APP_CONTEXT_FILE = "application-main-context.xml";

    private String appCfgFile = DEFAULT_APP_CONFIGURATION_FILE;
    private String i18nFile = DEFAULT_I18N_FILE;
    private String mainAppContextFile = DEFAULT_MAIN_APP_CONTEXT_FILE;

    private EntityService entitySrv;
    private SettingService settingSrv;
    private SecurityService securitySrv;
    private RefDataService refDataSrv;
    private DataReferenceService dataReferenceSrv;

    /**
     * @param beanClass
     * @return
     */
    protected static <T> T getBean(Class<T> beanClass) {
        return SpringUtils.getBean(beanClass);
    }

    /**
     * @throws Exception
     */
    public void init() throws Exception {
        try {
            super.contextInitialized(DEFAULT_MAIN_APP_CONTEXT_FILE);
            DataReferenceService dataReferenceService = SpringUtils.getBean(DataReferenceService.class);

            logger.info("---------------------- Startup : >> Initialized data reference ------------------------");

            dataReferenceService.initialized();
           /* initSecApplicationContext();

            final ResourceBundle rb = ResourceBundle.getBundle("i18n.messages", Locale.ENGLISH);
            final ValuePair[] messages = new ValuePair[rb.keySet().size()];
            int i = 0;
            for (final String key : rb.keySet()) {
                final ValuePair message = new ValuePair();
                message.setCode(key);
                try {
                    message.setValue(new String(rb.getString(key).getBytes("ISO-8859-1"), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    logger.error("UnsupportedEncodingException", e);
                }
                messages[i++] = message;
            }
            I18N.initBundle(Locale.ENGLISH, messages);

            openSession();

            logger.info("=======>>>>> open batch session success");

            entitySrv = getBean("entityService");
            securitySrv = getBean("securityService");
            settingSrv = getBean("settingService");
            refDataSrv = getBean("refDataService");
            dataReferenceSrv=getBean("dataReferenceService");*/


        } catch (Exception e) {
            String errMsg = "*******Fatal error occured initializing application. Exiting.!!!*******";
            System.err.println(errMsg);
            logger.error(errMsg, e);
            SpringUtils.shutdownHsql();
            System.exit(-1);
        }
    }

    /**
     * @see StartupApplicationContextListener
     */
    private void initSecApplicationContext() {
        try {
            logger.info("===========>>>>>>> Start init application context");
            logger.info("===========>>>>>>> " + mainAppContextFile);
            SpringUtils.initAppContext(mainAppContextFile);
            String appCode = null;

            if (StringUtils.isEmpty(appCfgFile)) {
                logger.info("The default config file is ["
                        + AppConfigFile.DEFAULT_APP_CONFIGURATION_FILE + "]");
                appCfgFile = AppConfigFile.DEFAULT_APP_CONFIGURATION_FILE;
            }

            AppConfigFile.initFile(appCfgFile);
            appCode = AppConfigFileHelper.getApplicationCode();
            logger.info("Config file [" + appCfgFile + "] ["
                    + AppConfigFileHelper.APP_CODE + "]: "
                    + (appCode != null ? appCode : "<N/A>"));

            if (StringUtils.isEmpty(appCode)) {
                throw new IllegalStateException("The application code ["
                        + AppConfigFileHelper.APP_CODE
                        + "] must be not defined in ["
                        + AppConfigFile.getConfigFile() + "]");
            }

            SecApplicationContext secAppContext = SecApplicationContextHolder
                    .getContext();
            secAppContext.setApplicationContext(SpringUtils.getAppContext());

            SecApplication secApp = SecurityHelper.getSecApplication();
            logger.info("SecApplication: ["
                    + (secApp != null ? secApp : "<null>"));
            secAppContext.setSecApplication(secApp);

            Locale locale = AppSettingConfigHelper.getLocale();
            secAppContext.setLocale(locale); // read from init file/db

            logger.info("Context initialized.");

        } catch (final Exception e) {
            logger.error("****Context NOT initialized.*****", e);
            throw new IllegalStateException(e);
        }
    }

    /**
     * @throws Exception
     */
    private void openSession() throws Exception {
        logger.info("OPEN BATCH SESSION - Simulate OpenSessionInViewFilter");
        SessionFactory sessionFactory = SpringUtils.getBean(SessionFactory.class);
        logger.info("======>>>>> getSessionFactory sucess");
        Session session = sessionFactory.openSession();
        TransactionSynchronizationManager.bindResource(sessionFactory,
                new SessionHolder(session));
    }

    /**
     *
     */
    public void shutDown() {
        logger.info("CLOSE BATCH SESSION");
        SessionFactory sessionFactory = SpringUtils.getSessionFactory();
        SessionHolder sessionHolder = (SessionHolder) TransactionSynchronizationManager
                .unbindResource(sessionFactory);
        SessionFactoryUtils.closeSession(sessionHolder.getSession());
    }

    /**
     * @return the appContext
     */
    public ApplicationContext getAppContext() {
        return SpringUtils.getAppContext();
    }

    /**
     * @param serviceClass
     * @return
     */
    public <T extends BaseEntityService> T getService(
            Class<T> serviceClass) {
        return SpringUtils.getService(serviceClass);
    }

    /**
     * @param beanName
     * @return
     */
    public <T> T getBean(String beanName) {
        return SpringUtils.getBean(beanName);
    }

    /**
     * @return the appConfigFile
     */
    public String getAppConfigFile() {
        return appCfgFile;
    }

    /**
     * @param appConfigFile the appConfigFile to set
     */
    public void setAppConfigFile(String appConfigFile) {
        this.appCfgFile = appConfigFile;
    }

    /**
     * @return the i18nFile
     */
    public String getI18nFile() {
        return i18nFile;
    }

    /**
     * @param i18nFile the i18nFile to set
     */
    public void setI18nFile(String i18nFile) {
        this.i18nFile = i18nFile;
    }

    /**
     * @return the mainAppContextFile
     */
    public String getMainAppContextFile() {
        return mainAppContextFile;
    }

    /**
     * @param mainAppContextFile the mainAppContextFile to set
     */
    public void setMainAppContextFile(String mainAppContextFile) {
        this.mainAppContextFile = mainAppContextFile;
    }

    /**
     * @return
     */
    public EntityService getEntitySrv() {
        return entitySrv;
    }

    /**
     * @param entitySrv
     */
    public void setEntitySrv(EntityService entitySrv) {
        this.entitySrv = entitySrv;
    }

    /**
     * @return
     */
    public SettingService getSettingSrv() {
        return settingSrv;
    }

    /**
     * @param settingSrv
     */
    public void setSettingSrv(SettingService settingSrv) {
        this.settingSrv = settingSrv;
    }

    /**
     * @return
     */
    public SecurityService getSecuritySrv() {
        return securitySrv;
    }

    /**
     * @param securitySrv
     */
    public void setSecuritySrv(SecurityService securitySrv) {
        this.securitySrv = securitySrv;
    }

    /**
     * @return
     */
    public RefDataService getRefTableSrv() {
        return refDataSrv;
    }

    /**
     * @param refTableSrv
     */
    public void setRefTableSrv(RefDataService refTableSrv) {
        this.refDataSrv = refTableSrv;
    }

    /**
     * @return
     */
    public DataReferenceService getDataReferenceSrv() {
        return dataReferenceSrv;
    }

    /**
     * @param dataReferenceSrv
     */
    public void setDataReferenceSrv(DataReferenceService dataReferenceSrv) {
        this.dataReferenceSrv = dataReferenceSrv;
    }
}
