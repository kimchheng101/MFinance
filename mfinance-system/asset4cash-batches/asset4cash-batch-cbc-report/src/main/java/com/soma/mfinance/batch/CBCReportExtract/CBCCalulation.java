package com.soma.mfinance.batch.CBCReportExtract;

import com.soma.common.app.workflow.model.EWkfStatus;
import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.ContractWkfHistoryItem;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowCode;
import com.soma.mfinance.core.contract.model.cashflow.ECashflowType;
import com.soma.mfinance.core.payment.model.Payment;
import com.soma.mfinance.core.quotation.model.QuotationApplicant;
import org.seuksa.frmk.model.meta.NativeColumn;
import org.seuksa.frmk.model.meta.NativeRow;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.amount.Amount;
import org.seuksa.frmk.tools.exception.NativeQueryException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.soma.frmk.helper.SeuksaServicesHelper.ENTITY_SRV;

public class CBCCalulation {


    public static List<Contract>getContractCBC4Upload(Long contract_id){
        List<Contract>contracts=new ArrayList<>();
        String query="SELECT con_id FROM td_contract " +
                "WHERE  wkf_sta_id <> (SELECT wkf_sta_id FROM ts_wkf_status WHERE ref_code='WTD') " +
                "AND con_id NOT IN (SELECT his_entity_id FROM td_history_process hisPro " +
                "INNER JOIN td_contract_wkf_history_item hisItm on hisItm.wkf_his_ite_id=hisPro.wkf_his_ite_id) ";
               if(contract_id!=null){
                   query= query+ "AND con_id ="+contract_id;
               }
               //query=query+" AND con_id IN ('10','80','64','24')";

        try {
            List<NativeRow> contractRows = ENTITY_SRV.executeSQLNativeQuery(query);
            for (NativeRow row : contractRows) {
                List<NativeColumn> columns = row.getColumns();
                Contract contract=ENTITY_SRV.getById(Contract.class,(Long) columns.get(0).getValue());
                contracts.add(contract);
            }
        } catch (NativeQueryException e) {
            CBCReportExtraction.logger.error("Method getContractCBC4Upload error: ",e);
        }
        return contracts;
    }
    /**
     * @param cotraId
     * @return
     */
    static List<Cashflow> getCashflowsNoCancel(Long cotraId) {
		/*
		 * BaseRestrictions<Cashflow> restrictions = new
		 * BaseRestrictions<>(Cashflow.class);
		 * restrictions.addCriterion(Restrictions.eq(CANCEL, Boolean.FALSE));
		 * restrictions.addCriterion(Restrictions.eq(CONTRACT + "." + ID,
		 * cotraId)); restrictions.addOrder(Order.asc(NUM_INSTALLMENT));
		 */
        List<Cashflow> cashflows = new ArrayList<>();
        String query = "SELECT " +
                "c.cfw_id, c.cfw_typ_id ," +
                "c.cfw_dt_installment," +
                "c.cfw_dt_period_start," +
                "c.cfw_dt_period_end," +
                "c.cfw_bl_cancel," +
                "c.cfw_bl_paid," +
                "c.cfw_bl_unpaid," +
                "c.cfw_am_te_installment," +
                "c.cfw_am_vat_installment," +
                "c.cfw_am_ti_installment," +
                "c.cfw_nu_num_installment," +
                "c.cfw_cod_id," +
                "c.pay_id," +
                "c.cfw_dt_installment," +
                "p.pay_am_ti_paid," +
                "p.pay_dt_payment " +
                "FROM td_cashflow c " +
                "left join td_payment p on p.pay_id = c.pay_id " +
                "WHERE c.con_id =" + cotraId +
                " AND (c.cfw_bl_cancel is false or cfw_cod_id is not null) " +
                "AND cfw_typ_id in (SELECT refData.ref_dat_ide FROM ts_ref_data refData " +
                "INNER JOIN (SELECT ref_tab_id,ref_tab_desc_en FROM ts_ref_table WHERE ref_tab_code ILIKE '%ECashflowType%') AS status ON status.ref_tab_id=refData.ref_tab_id " +
                "WHERE ref_dat_code IN ('CAP', 'IAP', 'PEN')) " +
                "ORDER BY c.cfw_nu_num_installment asc;";

        try {
            List<NativeRow> cashflowRows = ENTITY_SRV.executeSQLNativeQuery(query);
            for (NativeRow row : cashflowRows) {
                List<NativeColumn> columns = row.getColumns();
                int i = 0;
                Cashflow cashflow = new Cashflow();
                cashflow.setId((Long) columns.get(i++).getValue());
                cashflow.setCashflowType(ECashflowType.getById((Long) columns.get(i++).getValue()));
                cashflow.setInstallmentDate((Date) columns.get(i++).getValue());
                cashflow.setPeriodStartDate((Date) columns.get(i++).getValue());
                cashflow.setPeriodEndDate((Date) columns.get(i++).getValue());
                cashflow.setCancel((Boolean) columns.get(i++).getValue());
                cashflow.setPaid((Boolean) columns.get(i++).getValue());
                cashflow.setUnpaid((Boolean) columns.get(i++).getValue());
                cashflow.setTeInstallmentAmount((Double) columns.get(i++).getValue());
                cashflow.setVatInstallmentAmount((Double) columns.get(i++).getValue());
                cashflow.setTiInstallmentAmount((Double) columns.get(i++).getValue());
                cashflow.setNumInstallment((Integer) columns.get(i++).getValue());
                cashflow.setCashflowCode((Long) columns.get(i++).getValue() > 0 ? (ECashflowCode) columns.get(i++).getValue() : null);
                Long paymnId = (Long) columns.get(i++).getValue();
                cashflow.setPayment(ENTITY_SRV.getById(Payment.class, paymnId));
                cashflows.add(cashflow);
            }
        } catch (NativeQueryException e) {
            CBCReportExtraction.logger.error("Method getCashflowsNoCancel error: ",e);
        }

        return cashflows;
    }

    /**
     * @param contractWkfHistoryItems
     * @param contractWkfStatus
     * @return
     */
    static ContractWkfHistoryItem getContractWkfHistoryItemByContractStatus(List<ContractWkfHistoryItem> contractWkfHistoryItems, EWkfStatus contractWkfStatus,Long contract_id) {
        for (ContractWkfHistoryItem contractWkfHistoryItem : contractWkfHistoryItems) {
            if (contractWkfHistoryItem != null && contractWkfHistoryItem.getNewValue().equals(contractWkfStatus.getCode())) {
                return contractWkfHistoryItem;
            }
        }
        CBCReportExtraction.logger.error("<<<<<<<<<<<<<<contractWkfHistoryItems Null<<<<<<<<<<<<<< "+contract_id);
        return null;
    }

    /**
     * @param cashflows
     * @return
     */
    static Payment getLastPayment(List<Cashflow> cashflows) {
        Payment payment = null;
        for (Cashflow cashflow : cashflows) {
            if (cashflow.getCashflowType() == ECashflowType.IAP && !cashflow.isCancel() && cashflow.isPaid()
                    && (cashflow.getPayment() != null) && !cashflow.isUnpaid()) {
                if (payment == null || payment.getPaymentDate().compareTo(cashflow.getPayment().getPaymentDate()) < 0) {
                    payment = cashflow.getPayment();
                }
            }
        }
        return payment;
    }

    /**
     * @param cashflows
     * @return
     */
    static Date getNextPaymentDate(List<Cashflow> cashflows) {
        Cashflow cashflowValue = null;
        for (Cashflow cashflow : cashflows) {
            if (cashflow != null && cashflow.getCashflowType() == ECashflowType.CAP && !cashflow.isCancel()
                    && !cashflow.isUnpaid() && !cashflow.isPaid()
                    && DateUtils.getDateAtBeginningOfDay(cashflow.getInstallmentDate())
                    .compareTo(DateUtils.todayH00M00S00()) > 0) {
                if (cashflowValue == null || DateUtils.getDateAtBeginningOfDay(cashflowValue.getInstallmentDate())
                        .compareTo(DateUtils.getDateAtBeginningOfDay(cashflow.getInstallmentDate())) > 0) {
                    cashflowValue = cashflow;
                }
            }
        }
        return (cashflowValue != null ? DateUtils.getDateAtBeginningOfDay(cashflowValue.getInstallmentDate()) : null);
    }

    /**
     * @param cashflows
     * @return
     */
    static Date getPaymentStatusDate(List<Cashflow> cashflows) {
        Cashflow cashflowValue = null;
        for (Cashflow cashflow : cashflows) {
            if (cashflow != null && cashflow.getCashflowType() == ECashflowType.IAP && !cashflow.isCancel()
                    && !cashflow.isUnpaid() && !cashflow.isPaid()) {
                if (cashflowValue == null || DateUtils.getDateAtBeginningOfDay(cashflowValue.getInstallmentDate())
                        .after(DateUtils.getDateAtBeginningOfDay(cashflow.getInstallmentDate())))
						/*.compareTo(DateUtils.getDateAtBeginningOfDay(cashflow.getInstallmentDate())) > 0)*/ {
                    cashflowValue = cashflow;
                }
            }
        }
        return (cashflowValue != null ? DateUtils.getDateAtBeginningOfDay(cashflowValue.getInstallmentDate()) : null);
    }

    /**
     * Get principal balance
     *
     * @param calculDate
     * @param cashflows
     * @return
     */
    static Amount getPrincipalBalance(Date calculDate, List<Cashflow> cashflows) {
        Amount principalBalance = new Amount(0d, 0d, 0d);
        for (Cashflow cashflow : cashflows) {
            if (cashflow.getCashflowType() == ECashflowType.CAP && !cashflow.isCancel()
                    && (!cashflow.isPaid() || (cashflow.getPayment() != null
                    && cashflow.getPayment().getPaymentDate().compareTo(calculDate) > 0))
                    && !cashflow.isUnpaid()) {
                principalBalance.plusTiAmount(cashflow.getTiInstallmentAmount());
                principalBalance.plusTeAmount(cashflow.getTeInstallmentAmount());
                principalBalance.plusVatAmount(cashflow.getVatInstallmentAmount());
            }
        }
        return principalBalance;
    }

    /**
     * @param calDate
     * @param cashflows
     * @return
     */
    static Amount getTotalAmountInOverdueUsd(Date calDate, List<Cashflow> cashflows) {
        Amount totalAmountInOverdueUsd = new Amount(0d, 0d, 0d);
        for (Cashflow cashflow : cashflows) {
            if (cashflow != null && !cashflow.isCancel() && !cashflow.isUnpaid() && !cashflow.isPaid()) {
                long nbOverdueInDays = DateUtils.getDiffInDaysPlusOneDay(calDate,
                        DateUtils.getDateAtBeginningOfDay(cashflow.getInstallmentDate()));
                if (nbOverdueInDays > 0) {
                    totalAmountInOverdueUsd.plus(new Amount(cashflow.getTeInstallmentAmount(),
                            cashflow.getVatInstallmentAmount(), cashflow.getTiInstallmentAmount()));
                }
            }
        }
        return totalAmountInOverdueUsd;
    }

    /***
     *
     * @param quotationApplicants
     * @param eApplicantType
     * @return
     */
    static Applicant getApplicant(List<QuotationApplicant> quotationApplicants, EApplicantType eApplicantType) {
        if (quotationApplicants != null && !quotationApplicants.isEmpty()) {
            for (QuotationApplicant quotationApplicant : quotationApplicants) {
                if (quotationApplicant.getApplicantType().equals(eApplicantType)) {
                    return quotationApplicant.getApplicant();
                }
            }
        }
        return null;
    }
}
