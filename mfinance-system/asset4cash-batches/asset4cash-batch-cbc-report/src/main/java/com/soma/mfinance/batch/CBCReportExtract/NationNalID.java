package com.soma.mfinance.batch.CBCReportExtract;

import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationDocument;

import java.util.Date;

public class NationNalID {
    String IDNumber;
    Date epdat_nat;
    QuotationDocument quotationDocument;
    String applicantTypeCode;
    Quotation quotation;

    NationNalID(String IDNumber, Date epdat_nat) {

        this.IDNumber = IDNumber;
        this.epdat_nat = epdat_nat;
    }
}
