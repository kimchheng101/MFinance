package com.soma.mfinance.batch.CBCReportExtract;

import com.soma.mfinance.core.document.model.Document;

import java.util.Date;

public class CBCDocument {
    private String documCode;
    private String documReference;
    private Date expiredDate;
    private Document document;

    String getDocumCode() {
        return documCode;
    }

    void setDocumCode(String documCode) {
        this.documCode = documCode;
    }

    String getDocumReference() {
        return documReference;
    }

    void setDocumReference(String documReference) {
        this.documReference = documReference;
    }

    Date getExpiredDate() {
        return expiredDate;
    }

    void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }
}
