package com.soma.mfinance.batch.CBCReportExtract;


import com.soma.mfinance.core.helper.FinServicesHelper;
import com.soma.mfinance.core.shared.quotation.QuotationEntityField;
import com.soma.mfinance.core.shared.report.ReportParameter;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.seuksa.frmk.tools.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*import com.soma.mfinance.core.helper.FinServicesHelper;*/

/**
 * CBC report extraction
 *
 * @author sok.vina
 */
public class CBCReportExtraction extends StepExecutionListenerSupport implements  QuotationEntityField, FinServicesHelper,Tasklet {
    static String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
    static List<String> AREA_CODE_FIX = Arrays.asList("023", "024", "025", "026", "032", "033", "034", "035", "036", "042",
            "043", "044", "052", "053", "054", "055", "062", "063", "064", "065", "072", "073", "074", "075");
    static List<String> errRecs = new ArrayList<>();
    static Logger logger = LoggerFactory.getLogger(CBCReportExtraction.class.getSimpleName());
    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        generate(null);
        return RepeatStatus.FINISHED;
    }

    /**
     * @param reportParameter
     * @return
     * @throws Exception
     */
    public String generate(ReportParameter reportParameter) throws Exception {

        // Workbook wb = new HSSFWorkbook();
        String fileName;
        String basePath;
        FileOutputStream out;
        try (SXSSFWorkbook wb = new SXSSFWorkbook(1000)) {
            Sheet sheet = wb.createSheet();

            CellStyle style = wb.createCellStyle();
		/*
		 * for (int i = 1; i < 162; i++) { sheet.setColumnWidth(i, 6000); }
		 */

            for (int i = 161; i >= 1; i--) {
                sheet.setColumnWidth(i, 6000);
            }

            sheet.setZoom(7, 10);
            final PrintSetup printSetup = sheet.getPrintSetup();
            printSetup.setPaperSize(HSSFPrintSetup.A4_PAPERSIZE);
            printSetup.setScale((short) 75);

            // Setup the Page margins - Left, Right, Top and Bottom
            sheet.setMargin(Sheet.LeftMargin, 0.25);
            sheet.setMargin(Sheet.RightMargin, 0.25);
            sheet.setMargin(Sheet.TopMargin, 0.25);
            sheet.setMargin(Sheet.BottomMargin, 0.25);
            sheet.setMargin(Sheet.HeaderMargin, 0.25);
            sheet.setMargin(Sheet.FooterMargin, 0.25);

            style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
            style.setFillPattern(HSSFCellStyle.BIG_SPOTS);
            style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

            int iRow = 0;
            iRow = new CBCDataTable().dataTable(sheet, iRow, style);

            Row tmpRowEnd = sheet.createRow(iRow++ + 1);
            tmpRowEnd.setRowStyle(style);

            iRow = iRow + 2;

            //fileName = "output/CBC_Report_Extraction_" + DateUtils.getDateLabel(DateUtils.today(), "yyyyMMddHHmmss") + ".xls";
            //fileName = "D:/CBC_Report_Extraction_" + DateUtils.getDateLabel(DateUtils.today(), "yyyyMMddHHmmss") + ".xls";
            //basePath="D:/TEST_EXTRACT_CBC/";
            basePath="output/CBC_Report_Extraction_";
            fileName = basePath+"CBC_Report_Extraction_" + DateUtils.getDateLabel(DateUtils.today(), "yyyyMMddHHmmss") + ".xls";
            out = new FileOutputStream(fileName);
            wb.write(out);
        }
        out.close();
        generateEorrorRecord(errRecs,basePath);
        return fileName;
    }

    /**
     * For get information and show in txt file
     */
    static void generateEorrorRecord(List<String> errRecList, String basePath) {
        //String fileName = "output/CBC_Error_Record_ " + ".txt";
        String fileName = basePath+"CBC_Error_Record_ " + ".txt";
        File errorRecord = new File(fileName);
        try {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(errorRecord))) {
                for (String err : errRecList) {
                    writer.write(err);
                    writer.newLine();
                }
                writer.close();
            }

        } catch (IOException e) {

            System.err.println(e.getMessage());
        }
    }
}
