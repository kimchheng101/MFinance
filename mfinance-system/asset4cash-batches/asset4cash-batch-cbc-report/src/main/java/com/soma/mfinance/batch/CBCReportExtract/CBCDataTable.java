package com.soma.mfinance.batch.CBCReportExtract;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.applicant.model.Employment;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.contract.model.cashflow.Cashflow;
import com.soma.mfinance.core.payment.model.Payment;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationApplicant;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.mfinance.core.workflow.model.history.EProcessType;
import com.soma.mfinance.core.workflow.model.history.HistoryProcess;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.seuksa.frmk.model.entity.EStatusRecord;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyNumberUtils;
import org.seuksa.frmk.tools.amount.Amount;

import java.util.Date;
import java.util.List;

import static com.soma.mfinance.batch.CBCReportExtract.CBCCalulation.*;
import static com.soma.mfinance.batch.CBCReportExtract.CBCReportExtraction.errRecs;
import static com.soma.frmk.helper.SeuksaServicesHelper.ENTITY_SRV;

public class CBCDataTable extends CreateCell{

    /**
     * @param sheet
     * @param iRow
     * @param style
     * @return
     * @throws Exception
     */
     int dataTable(final Sheet sheet, int iRow, final CellStyle style) throws Exception {
        /* Create total data header */
        int iCol = 0;
        Row tmpRow = sheet.createRow(iRow++);
        createCell(tmpRow, iCol++, "CustomerID");
        createCell(tmpRow, iCol++, "IDCode");
        createCell(tmpRow, iCol++, "IDNumber");
        createCell(tmpRow, iCol++, "IDExpiryDate");
        createCell(tmpRow, iCol++, "IDCode2");
        createCell(tmpRow, iCol++, "IDNumber2");
        createCell(tmpRow, iCol++, "IDExpiryDate2");
        createCell(tmpRow, iCol++, "IDCode3");
        createCell(tmpRow, iCol++, "IDNumber3");
        createCell(tmpRow, iCol++, "IDExpiryDate3");
        createCell(tmpRow, iCol++, "DateOfBirth");
        createCell(tmpRow, iCol++, "FamilyNameEng");
        createCell(tmpRow, iCol++, "FirstNameEng");
        createCell(tmpRow, iCol++, "SecondNameEng");
        createCell(tmpRow, iCol++, "ThirdNameEng");
        createCell(tmpRow, iCol++, "UnformattedNameEng");
        createCell(tmpRow, iCol++, "MotherNameEng");
        createCell(tmpRow, iCol++, "FamilyNameKhm");
        createCell(tmpRow, iCol++, "FirstNameKhm");
        createCell(tmpRow, iCol++, "SecondNameKhm");
        createCell(tmpRow, iCol++, "ThirdNameKhm");
        createCell(tmpRow, iCol++, "UnformattedNameKh");
        createCell(tmpRow, iCol++, "MotherNameKhm");
        createCell(tmpRow, iCol++, "GenderCode");
        createCell(tmpRow, iCol++, "MaritalStatusCode");
        createCell(tmpRow, iCol++, "NationalityCode");
        createCell(tmpRow, iCol++, "TaxpayerRegistrationNumber");
        createCell(tmpRow, iCol++, "ApplicantTypeCode");
        createCell(tmpRow, iCol++, "AddressTypeCode");
        createCell(tmpRow, iCol++, "Province");
        createCell(tmpRow, iCol++, "District");
        createCell(tmpRow, iCol++, "Commune");
        createCell(tmpRow, iCol++, "Village");
        createCell(tmpRow, iCol++, "AddressField1Eng");
        createCell(tmpRow, iCol++, "AddressField2Eng");
        createCell(tmpRow, iCol++, "AddressField1Khm");
        createCell(tmpRow, iCol++, "AddressField2Khm");
        createCell(tmpRow, iCol++, "CityEng");
        createCell(tmpRow, iCol++, "CityKhm");
        createCell(tmpRow, iCol++, "CountryCode");
        createCell(tmpRow, iCol++, "PostalCode");
        createCell(tmpRow, iCol++, "AddressTypeCode2");
        createCell(tmpRow, iCol++, "Province2");
        createCell(tmpRow, iCol++, "District2");
        createCell(tmpRow, iCol++, "Commune2");
        createCell(tmpRow, iCol++, "Village2");
        createCell(tmpRow, iCol++, "AddressField1Eng2");
        createCell(tmpRow, iCol++, "AddressField2Eng2");
        createCell(tmpRow, iCol++, "AddressField1Khm2");
        createCell(tmpRow, iCol++, "AddressField2Khm2");
        createCell(tmpRow, iCol++, "CityEng2");
        createCell(tmpRow, iCol++, "CityKhm2");
        createCell(tmpRow, iCol++, "CountryCode2");
        createCell(tmpRow, iCol++, "PostalCode2");
        createCell(tmpRow, iCol++, "AddressTypeCode3");
        createCell(tmpRow, iCol++, "Province3");
        createCell(tmpRow, iCol++, "District3");
        createCell(tmpRow, iCol++, "Commune3");
        createCell(tmpRow, iCol++, "Village3");
        createCell(tmpRow, iCol++, "AddressField1Eng3");
        createCell(tmpRow, iCol++, "AddressField2Eng3");
        createCell(tmpRow, iCol++, "AddressField1Khm3");
        createCell(tmpRow, iCol++, "AddressField2Khm3");
        createCell(tmpRow, iCol++, "CityEng3");
        createCell(tmpRow, iCol++, "CityKhm3");
        createCell(tmpRow, iCol++, "CountryCode3");
        createCell(tmpRow, iCol++, "PostalCode3");
        createCell(tmpRow, iCol++, "EmailAddress");
        createCell(tmpRow, iCol++, "ContactNumberTypeCode");
        createCell(tmpRow, iCol++, "ContactNumberCountryCode");
        createCell(tmpRow, iCol++, "ContactNumberArea");
        createCell(tmpRow, iCol++, "ContactNumberNumber");
        createCell(tmpRow, iCol++, "ContactNumberExtension");
        createCell(tmpRow, iCol++, "ContactNumberTypeCode2");
        createCell(tmpRow, iCol++, "ContactNumberCountryCode2");
        createCell(tmpRow, iCol++, "ContactNumberArea2");
        createCell(tmpRow, iCol++, "ContactNumberNumber2");
        createCell(tmpRow, iCol++, "ContactNumberExtension2");
        createCell(tmpRow, iCol++, "ContactNumberTypeCode3");
        createCell(tmpRow, iCol++, "ContactNumberCountryCode3");
        createCell(tmpRow, iCol++, "ContactNumberArea3");
        createCell(tmpRow, iCol++, "ContactNumberNumber3");
        createCell(tmpRow, iCol++, "ContactNumberExtension3");
        createCell(tmpRow, iCol++, "EmployerTypeCode");
        createCell(tmpRow, iCol++, "SelfEmployed");
        createCell(tmpRow, iCol++, "EmployerNameEng");
        createCell(tmpRow, iCol++, "EmployerNameKhm");
        createCell(tmpRow, iCol++, "EconomicSector");
        createCell(tmpRow, iCol++, "BusinessType");
        createCell(tmpRow, iCol++, "EmployerAddressEng");
        createCell(tmpRow, iCol++, "EmployerAddressKhm");
        createCell(tmpRow, iCol++, "EmployerProvince");
        createCell(tmpRow, iCol++, "EmployerDistrict");
        createCell(tmpRow, iCol++, "EmployerCommune");
        createCell(tmpRow, iCol++, "EmployerVillage");
        createCell(tmpRow, iCol++, "EmployerAddressCityEng");
        createCell(tmpRow, iCol++, "EmployerAddressCityKhm");
        createCell(tmpRow, iCol++, "EmployerAddressCountryCode");
        createCell(tmpRow, iCol++, "EmployerAddressPostalCode");
        createCell(tmpRow, iCol++, "OccupationEng");
        createCell(tmpRow, iCol++, "OccupationKhm");
        createCell(tmpRow, iCol++, "DateOfEmployment");
        createCell(tmpRow, iCol++, "LengthOfService");
        createCell(tmpRow, iCol++, "ContractExpiryDate");
        createCell(tmpRow, iCol++, "IncomeCurrency");
        createCell(tmpRow, iCol++, "MonthlyBasicIncome");
        createCell(tmpRow, iCol++, "MonthlyTotalIncome");
        createCell(tmpRow, iCol++, "EmployerTypeCode2");
        createCell(tmpRow, iCol++, "SelfEmployed2");
        createCell(tmpRow, iCol++, "EmployerNameEng2");
        createCell(tmpRow, iCol++, "EmployerNameKhm2");
        createCell(tmpRow, iCol++, "EconomicSector2");
        createCell(tmpRow, iCol++, "BusinessType2");
        createCell(tmpRow, iCol++, "EmployerAddressEng2");
        createCell(tmpRow, iCol++, "EmployerAddressKhm2");
        createCell(tmpRow, iCol++, "EmployerProvince2");
        createCell(tmpRow, iCol++, "EmployerDistrict2");
        createCell(tmpRow, iCol++, "EmployerCommune2");
        createCell(tmpRow, iCol++, "EmployerVillage2");
        createCell(tmpRow, iCol++, "EmployerAddressCityEng2");
        createCell(tmpRow, iCol++, "EmployerAddressCityKhm2");
        createCell(tmpRow, iCol++, "EmployerAddressCountryCode2");
        createCell(tmpRow, iCol++, "EmployerAddressPostalCode2");
        createCell(tmpRow, iCol++, "OccupationEng2");
        createCell(tmpRow, iCol++, "OccupationKhm2");
        createCell(tmpRow, iCol++, "DateOfEmployment2");
        createCell(tmpRow, iCol++, "LengthOfService2");
        createCell(tmpRow, iCol++, "ContractExpiryDate2");
        createCell(tmpRow, iCol++, "IncomeCurrency2");
        createCell(tmpRow, iCol++, "MonthlyBasicIncome2");
        createCell(tmpRow, iCol++, "MonthlyTotalIncome2");
        createCell(tmpRow, iCol++, "EmployerTypeCode3");
        createCell(tmpRow, iCol++, "SelfEmployed3");
        createCell(tmpRow, iCol++, "EmployerNameEng3");
        createCell(tmpRow, iCol++, "EmployerNameKhm3");
        createCell(tmpRow, iCol++, "EconomicSector3");
        createCell(tmpRow, iCol++, "BusinessType3");
        createCell(tmpRow, iCol++, "EmployerAddressEng3");
        createCell(tmpRow, iCol++, "EmployerAddressKhm3");
        createCell(tmpRow, iCol++, "EmployerProvince3");
        createCell(tmpRow, iCol++, "EmployerDistrict3");
        createCell(tmpRow, iCol++, "EmployerCommune3");
        createCell(tmpRow, iCol++, "EmployerVillage3");
        createCell(tmpRow, iCol++, "EmployerAddressCityEng3");
        createCell(tmpRow, iCol++, "EmployerAddressCityKhm3");
        createCell(tmpRow, iCol++, "EmployerAddressCountryCode3");
        createCell(tmpRow, iCol++, "EmployerAddressPostalCode3");
        createCell(tmpRow, iCol++, "OccupationEng3");
        createCell(tmpRow, iCol++, "OccupationKhm3");
        createCell(tmpRow, iCol++, "DateOfEmployment3");
        createCell(tmpRow, iCol++, "LengthOfService3");
        createCell(tmpRow, iCol++, "ContractExpiryDate3");
        createCell(tmpRow, iCol++, "IncomeCurrency3");
        createCell(tmpRow, iCol++, "MonthlyBasicIncome3");
        createCell(tmpRow, iCol++, "MonthlyTotalIncome3");
        createCell(tmpRow, iCol++, "CreditorID");
        createCell(tmpRow, iCol++, "AccountTypeCode");
        createCell(tmpRow, iCol++, "GroupAccountReference");
        createCell(tmpRow, iCol++, "AccountNumber");
        createCell(tmpRow, iCol++, "DateIssued");
        createCell(tmpRow, iCol++, "ProductTypeCode");
        createCell(tmpRow, iCol++, "Currency");
        createCell(tmpRow, iCol++, "LoanAmount");
        createCell(tmpRow, iCol++, "ProductExpiryDate");
        createCell(tmpRow, iCol++, "ProductStatusCode");
        createCell(tmpRow, iCol++, "RestructuredLoan");
        createCell(tmpRow, iCol++, "InstalmentAmount");
        createCell(tmpRow, iCol++, "PaymentFrequencyCode");
        createCell(tmpRow, iCol++, "Tenure");
        createCell(tmpRow, iCol++, "LastPaymentDate");
        createCell(tmpRow, iCol++, "LastAmountPaid");
        createCell(tmpRow, iCol++, "SecurityTypeCode");
        createCell(tmpRow, iCol++, "OutstandingBalance");
        createCell(tmpRow, iCol++, "PastDue");
        createCell(tmpRow, iCol++, "NextPaymentDate");
        createCell(tmpRow, iCol++, "PaymentStatusCode");
        createCell(tmpRow, iCol++, "AsOfDate");
        createCell(tmpRow, iCol++, "WriteOffStatusCode");
        createCell(tmpRow, iCol++, "WriteOffStatusDate");
        createCell(tmpRow, iCol++, "OriginalAmountAsAtLoadDate");
        createCell(tmpRow, iCol++, "OutstandingBalanceAsAtLoadDate");

        /*BaseRestrictions<Contract> restrictions = new BaseRestrictions<>(Contract.class);
        restrictions.addCriterion(Restrictions.ne(CONTRACT_STATUS, ContractWkfStatus.WTD));
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(HistoryProcess.class, "hisProc");
        detachedCriteria.createAlias("hisProc.contractWkfHistoryItem", "conHisItm", JoinType.INNER_JOIN);
        detachedCriteria.add(Restrictions.eq("processType", EProcessType.CB));
        detachedCriteria.setProjection(Projections.projectionList().add(Projections.property("conHisItm.entityId")));
        restrictions.addCriterion(Property.forName("id").notIn(detachedCriteria));
        restrictions.addCriterion(Restrictions.in(REFERENCE,new String[]{"GLF-PNP-12-70000001"}));*/
        //restrictions.addCriterion(Restrictions.eq(CONTRACT_STATUS, ContractWkfStatus.EAR));
        //restrictions.setMaxResults(4);
        /*List<Contract> contracts = ENTITY_SRV.list(restrictions);*/
         List<Contract> contracts = getContractCBC4Upload(null);
        int nbRows = contracts.size();
        errRecs.add("Contract size: " + contracts.size());
        System.out.println("Contract size: " + contracts.size());
		/*E-Finance
		SELECT * FROM td_contract
    		WHERE cotra_id NOT IN (SELECT cotra_id FROM td_history_process hisProc INNER JOIN td_contract_wkf_history_item conHisItm ON hisProc.wkf_his_ite_id=conHisItm.wkf_his_ite_id WHERE hisProc.prtyp_code=1 --is 'CB')
    		AND wkf_sta_id !=27 --is WTD;
		 */
        int customerId = 0;
        for (int i = 0 ; i < nbRows ; i++) {
            customerId++;
            Contract contract = contracts.get(i);
            Quotation quotation = contract.getQuotation();

            System.out.println("iter = " + i + "/" + nbRows + " (" + quotation.getReference() + ")");

            List<QuotationApplicant> quotationApplicants = quotation.getQuotationApplicants();
            Applicant applicant = getApplicant(quotationApplicants, EApplicantType.C);
            Applicant guarantor;
            if (quotation.getFinancialProduct() != null && quotation.getFinancialProduct().getCode().equals("EXT")) {
                guarantor = null;
            } else {
                guarantor = getApplicant(quotationApplicants, EApplicantType.G);
            }

            List<Employment> employments = null;
            if (applicant != null && applicant.getIndividual() != null) {
                employments = applicant.getIndividual().getEmployments();
            }
            // Date dateIssued = quotation.getActivationDate();
            Date dateIssued = contract.getStartDate();

            double applicantRevenus = 0d;
            double applicantAllowance = 0d;
            double applicantBusinessExpenses = 0d;

            List<Cashflow> cashflows = getCashflowsNoCancel(contract.getId());
            Amount outstandingBalance = new Amount(0d, 0d, 0d);

            double lastAmountPaid = 0d;
            Date lastPaymentDate = null;
            Date nextPaymentDate = null;
            Amount pastDueAmount = new Amount(0d, 0d, 0d);
            // Date productExpire =
            // DateUtils.addMonthsDate(quotation.getFirstPaymentDate(),
            // quotation.getTerm());
            Date productExpire = null;
            String productStatusCode = "N";
            String paymentStatusCode = "0";

            Date today = DateUtils.todayH00M00S00();
            Date paymentStatusDate = getPaymentStatusDate(cashflows);
            long numberOfDays = 0;

            long loanTerm = DateUtils.getDiffInDays(contract.getEndDate(), contract.getStartDate());
            double loanYearTerm = (loanTerm * 100.0) / (365 * 100.0);

            if (quotation != null && quotation.getContract() != null) {
                contract = quotation.getContract();
                productExpire = contract.getEndDate();
            }
            if (paymentStatusDate != null) {
                numberOfDays = DateUtils.getDiffInDays(today, DateUtils.getDateAtBeginningOfDay(paymentStatusDate));
            }
			/*if (DateUtils.getDateAtBeginningOfDay(contract.getFirstInstallmentDate()).compareTo(today) > 0) {*/
            if (DateUtils.getDateAtBeginningOfDay(contract.getFirstDueDate()).after(today)) {
                // paymentStatusCode = "N";
                productStatusCode = "N";
                paymentStatusCode = "Q";
            }
			/*if (DateUtils.getDateAtBeginningOfDay(contract.getFirstInstallmentDate()).compareTo(today) > 0) {
				// paymentStatusCode = "N";
				paymentStatusCode = "Q";
			}*/
            else if (contract.getWkfStatus().equals(ContractWkfStatus.EAR)
                    || contract.getWkfStatus().equals(ContractWkfStatus.CLO)
                    //|| contract.getWkfStatus() == ContractWkfStatus.TRA
                    || contract.getWkfStatus().equals(ContractWkfStatus.REP)
                    || contract.getWkfStatus().equals(ContractWkfStatus.THE)
                    || contract.getWkfStatus().equals(ContractWkfStatus.ACC)
                    || contract.getWkfStatus().equals(ContractWkfStatus.FRA)) {
                productStatusCode = "C";
                paymentStatusCode = "C";
                nextPaymentDate = null;
                HistoryProcess historyProcess = new HistoryProcess();
                historyProcess.setProcessType(EProcessType.CB);
                historyProcess.setContractWkfHistoryItem(getContractWkfHistoryItemByContractStatus(contract.getHistories(), contract.getWkfStatus(),contract.getId()));
                historyProcess.setProcessDate(DateUtils.today());
                historyProcess.setStatusRecord(EStatusRecord.ACTIV);
                ENTITY_SRV.saveOrUpdate(historyProcess);

            } else if (contract.getWkfStatus().equals(ContractWkfStatus.WRI)) {
                productStatusCode = "W";
                paymentStatusCode = "W";
                nextPaymentDate = null;
                HistoryProcess historyProcess = new HistoryProcess();
                historyProcess.setProcessType(EProcessType.CB);
                historyProcess.setContractWkfHistoryItem(getContractWkfHistoryItemByContractStatus(contract.getHistories(), contract.getWkfStatus(),contract.getId()));
                historyProcess.setProcessDate(DateUtils.today());
                historyProcess.setStatusRecord(EStatusRecord.ACTIV);
                ENTITY_SRV.saveOrUpdate(historyProcess);

            } else if (contract.getWkfStatus().equals(ContractWkfStatus.LOS)) {
                productStatusCode = "L";
                paymentStatusCode = "L";
                nextPaymentDate = null;
                HistoryProcess historyProcess = new HistoryProcess();
                historyProcess.setProcessType(EProcessType.CB);
                historyProcess.setContractWkfHistoryItem(getContractWkfHistoryItemByContractStatus(contract.getHistories(), contract.getWkfStatus(),contract.getId()));
                historyProcess.setProcessDate(DateUtils.today());
                historyProcess.setStatusRecord(EStatusRecord.ACTIV);
                ENTITY_SRV.saveOrUpdate(historyProcess);

            } else if (loanYearTerm <= 1) {

                if (numberOfDays <= 0) {
                    productStatusCode = "N";
                    paymentStatusCode = "0";
                } else if (numberOfDays <= 29) {
                    productStatusCode = "N";
                    paymentStatusCode = "1";
                } else if (numberOfDays <= 59) {
                    productStatusCode = "U";
                    paymentStatusCode = "2";
                } else if (numberOfDays <= 89) {
                    productStatusCode = "D";
                    paymentStatusCode = "3";
                } else if (numberOfDays <= 119) {
                    productStatusCode = "L";
                    paymentStatusCode = "4";
                } else if (numberOfDays <= 149) {
                    productStatusCode = "L";
                    paymentStatusCode = "5";
                } else if (numberOfDays <= 179) {
                    productStatusCode = "L";
                    paymentStatusCode = "6";
                } else if (numberOfDays <= 209) {
                    productStatusCode = "L";
                    paymentStatusCode = "7";
                } else if (numberOfDays <= 239) {
                    productStatusCode = "L";
                    paymentStatusCode = "8";
                } else if (numberOfDays <= 269) {
                    productStatusCode = "L";
                    paymentStatusCode = "9";
                } else if (numberOfDays <= 299) {
                    productStatusCode = "L";
                    paymentStatusCode = "T";
                } else if (numberOfDays <= 329) {
                    productStatusCode = "L";
                    paymentStatusCode = "E";
                } else if (numberOfDays <= 359) {
                    productStatusCode = "L";
                    paymentStatusCode = "Y";
                } else if (numberOfDays >= 360L) {
                    productStatusCode = "L";
                    paymentStatusCode = "L";
                }
            } else if (loanYearTerm > 1) {

                if (numberOfDays <= 0) {
                    productStatusCode = "N";
                    paymentStatusCode = "0";
                } else if (numberOfDays <= 29) {
                    productStatusCode = "N";
                    paymentStatusCode = "1";
                } else if (numberOfDays <= 59) {
                    productStatusCode = "U";
                    paymentStatusCode = "2";
                } else if (numberOfDays <= 89) {
                    productStatusCode = "U";
                    paymentStatusCode = "3";
                } else if (numberOfDays <= 119) {
                    productStatusCode = "U";
                    paymentStatusCode = "4";
                } else if (numberOfDays <= 149) {
                    productStatusCode = "U";
                    paymentStatusCode = "5";
                } else if (numberOfDays <= 179) {
                    productStatusCode = "U";
                    paymentStatusCode = "6";
                } else if (numberOfDays <= 209) {
                    productStatusCode = "D";
                    paymentStatusCode = "7";
                } else if (numberOfDays <= 239) {
                    productStatusCode = "D";
                    paymentStatusCode = "8";
                } else if (numberOfDays <= 269) {
                    productStatusCode = "D";
                    paymentStatusCode = "9";
                } else if (numberOfDays <= 299) {
                    productStatusCode = "D";
                    paymentStatusCode = "T";
                } else if (numberOfDays <= 329) {
                    productStatusCode = "D";
                    paymentStatusCode = "E";
                } else if (numberOfDays <= 359) {
                    productStatusCode = "D";
                    paymentStatusCode = "Y";
                    nextPaymentDate = null;
                    HistoryProcess historyProcess = new HistoryProcess();
                    historyProcess.setProcessType(EProcessType.CB);
                    historyProcess.setContractWkfHistoryItem(getContractWkfHistoryItemByContractStatus(contract.getHistories(), contract.getWkfStatus(),contract.getId()));
                    historyProcess.setProcessDate(DateUtils.today());
                    historyProcess.setStatusRecord(EStatusRecord.ACTIV);
                    ENTITY_SRV.saveOrUpdate(historyProcess);

                } else if (numberOfDays >= 360) {
                    productStatusCode = "L";
                    paymentStatusCode = "L";
                }
            }
            outstandingBalance = getPrincipalBalance(DateUtils.getDateAtEndOfDay(DateUtils.todayDate()), cashflows);
            pastDueAmount = getTotalAmountInOverdueUsd(DateUtils.todayH00M00S00(), cashflows);
            if ("N".equals(paymentStatusCode) || "R".equals(paymentStatusCode) || "Q".equals(paymentStatusCode)
                    || "D".equals(paymentStatusCode) || "V".equals(paymentStatusCode)) {
                lastAmountPaid = 0d;
                lastPaymentDate = null;
            } else {
                Payment lastPayment = getLastPayment(cashflows);
                if (lastPayment != null) {
                    lastAmountPaid = lastPayment.getTiPaidAmount() != null ? lastPayment.getTiPaidAmount() : 0d;
                    lastPaymentDate = lastPayment.getPaymentDate();
                } else {
                    /*lastPaymentDate = DateUtils.today(); //ASK
                    lastAmountPaid = MyMathUtils.roundAmountTo(contract.getAsset().getTiAssetPrice() * contract.getAdvancePaymentPercentage() / 100);*/ //ASK
                    lastPaymentDate = DateUtils.today();
                    lastAmountPaid = 0d;
                }
            }

            // if ("N".equals(paymentStatusCode) ||
            // "R".equals(paymentStatusCode) || "C".equals(paymentStatusCode)) {
            if ("R".equals(paymentStatusCode) || "C".equals(paymentStatusCode) || "0".equals(paymentStatusCode)
                    || "Q".equals(paymentStatusCode)) {
                // outstandingBalance = new Amount(0d, 0d, 0d);
                pastDueAmount = new Amount(0d, 0d, 0d);

                // Field: 176, PaymentStatus C => Field: 174, PastDue must be 0

            } else {
				/*
				 * outstandingBalance =
				 * getPrincipalBalance(DateUtils.getDateAtEndOfDay(DateUtils.
				 * todayDate()), cashflows); pastDueAmount =
				 * getTotalAmountInOverdueUsd(DateUtils.todayH00M00S00(),
				 * cashflows);
				 */
                if (!"L".equals(paymentStatusCode)) {
                    nextPaymentDate = getNextPaymentDate(cashflows);
                    if (nextPaymentDate == null) {
                        nextPaymentDate = DateUtils.addDaysDate(DateUtils.todayH00M00S00(), 1);
                    }
                }
            }

            if ("C".equals(paymentStatusCode)) {
                // 1- Field: 176, PaymentStatus C => Field: 175, Next Payment
                // Date must be blank
                nextPaymentDate = null;
                // 2- - Field: 176, PaymentStatus C => Field: 173, Outstanding
                // must be 0
                outstandingBalance = new Amount(0d, 0d, 0d);
            }
            if ("Q".equals(paymentStatusCode)) {
                // productExpire = null;
                lastPaymentDate = null;
            }
            if (!"L".equals(paymentStatusCode) && !"C".equals(paymentStatusCode)) {
                nextPaymentDate = getNextPaymentDate(cashflows);
                if (nextPaymentDate == null) {
                    nextPaymentDate = DateUtils.addDaysDate(DateUtils.todayH00M00S00(), 1);
                }
            }

            if (employments != null && !employments.isEmpty()) {
                for (Employment employmentAmount : employments) {
                    applicantRevenus += MyNumberUtils.getDouble(employmentAmount.getRevenue());
                    applicantAllowance += MyNumberUtils.getDouble(employmentAmount.getAllowance());
                    applicantBusinessExpenses += MyNumberUtils.getDouble(employmentAmount.getBusinessExpense());
                }
            }
            double monthlyFimilyExpense = applicant != null && applicant.getIndividual().getMonthlyFamilyExpenses() != null
                    ? MyNumberUtils.getDouble(applicant.getIndividual().getMonthlyFamilyExpenses()) : 0d;
            double monthlyPersonalExpense = applicant != null && applicant.getIndividual().getMonthlyPersonalExpenses() != null
                    ? MyNumberUtils.getDouble(applicant.getIndividual().getMonthlyPersonalExpenses()) : 0d;
            Double monthlyBasicIncom = applicantRevenus + applicantAllowance - applicantBusinessExpenses - monthlyFimilyExpense - monthlyPersonalExpense;

            if (applicant != null && (applicant.getFirstNameEn() == null || applicant.getLastNameEn() == null || applicant.getIndividual().getBirthDate() == null)) {
                errRecs.add(quotation.getReference() + "++++++++Applicant Type: Applicant " + " Lose information++++++");
                continue;
            }

                iCol = 0;
                if (contract.getReference() != null) {
                    iRow = new CBCGenerateRow().generateRow(sheet, applicant, quotation, "P", monthlyBasicIncom,
                            productExpire, dateIssued, lastPaymentDate, lastAmountPaid, outstandingBalance.getTiAmount(),
                            pastDueAmount.getTiAmount(), nextPaymentDate, productStatusCode, paymentStatusCode, iRow,
                            customerId);
                }

                if (guarantor != null) {
                    customerId = customerId + 1;

                    if (guarantor != null) {

                        if (guarantor.getFirstName() == null || guarantor.getLastName() == null
                                || guarantor.getIndividual().getBirthDate() == null) {
                            errRecs.add(quotation.getReference() + "++++++++Applicant Type: Guotation"
                                    + "Lose quotation information++++++");
                            continue;
                        }
                    }
                    if (contract.getReference() != null) {
                        iRow = new CBCGenerateRow().generateRow(sheet, guarantor, quotation, "G", monthlyBasicIncom,
                                productExpire, dateIssued, lastPaymentDate, lastAmountPaid,
                                outstandingBalance.getTiAmount(), pastDueAmount.getTiAmount(), nextPaymentDate,
                                productStatusCode, paymentStatusCode, iRow, customerId);
                    }
                }
        }
        return iRow;
    }
}
