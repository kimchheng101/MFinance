package com.soma.mfinance.batch.CBCReportExtract;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.util.Date;

import static com.soma.mfinance.batch.CBCReportExtract.CBCReportExtraction.DEFAULT_DATE_FORMAT;

public class CreateCell {

    /**
     * @param row
     * @param iCol
     * @param value
     * @return
     */
    Cell createCell(final Row row, final int iCol, final String value) {
        final Cell cell = row.createCell(iCol);
        cell.setCellValue((value == null ? "" : value));
        return cell;
    }

    /**
     * @param row
     * @param iCol
     * @param value
     * @return
     */
    Cell createNumericCell(final Row row, final int iCol, final Object value) {
        final Cell cell = row.createCell(iCol);
        if (value == null) {
            cell.setCellValue("");
        } else if (value instanceof Integer) {
            cell.setCellValue(Integer.valueOf(value.toString()));
        } else if (value instanceof Long) {
            cell.setCellValue(Long.valueOf(value.toString()));
        } else if (value instanceof String) {
            cell.setCellValue(value.toString());
        }
        if (value instanceof Double) {
            cell.setCellValue((Double) value);
            cell.setCellType(Cell.CELL_TYPE_NUMERIC);
        } else {
        }
        return cell;
    }

    /**
     * @param date
     * @return
     */
    public String getDateLabel(final Date date) {
        return getDateLabel(date, DEFAULT_DATE_FORMAT);
    }

    /**
     * @param date
     * @param formatPattern
     * @return
     */
    public String getDateLabel(final Date date, final String formatPattern) {
        if (date != null && formatPattern != null) {
            return DateFormatUtils.format(date, formatPattern);
        }
        return null;
    }
}
