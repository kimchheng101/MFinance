package com.soma.mfinance.batch.CBCReportExtract;

import com.soma.mfinance.core.applicant.model.Applicant;
import com.soma.mfinance.core.applicant.model.Employment;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.third.creditbureau.cbc.model.CBCAddressCode;
import com.soma.mfinance.third.creditbureau.cbc.model.CBCWrongAddressCode;
import com.soma.ersys.core.hr.model.address.Address;
import com.soma.ersys.core.hr.model.eref.ECivility;
import com.soma.ersys.core.hr.model.eref.EGender;
import com.soma.ersys.core.hr.model.eref.EMaritalStatus;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.seuksa.frmk.tools.DateUtils;
import org.seuksa.frmk.tools.MyMathUtils;
import org.seuksa.frmk.tools.MyNumberUtils;

import java.util.Date;

import static com.soma.mfinance.batch.CBCReportExtract.CBCReportExtraction.AREA_CODE_FIX;
import static com.soma.mfinance.batch.CBCReportExtract.CBCReportExtraction.errRecs;
import static com.soma.mfinance.batch.CBCReportExtract.CBCValidation.*;

public class CBCGenerateRow extends CreateCell{

    /**
     * @param sheet
     * @param applicant
     * @param quotation
     * @param applicantTypeCode
     * @param monthlyBasicIncom
     * @param productExpire
     * @param dateIssued
     * @param productStatusCode
     * @param lastPaymentDate
     * @param lastAmountPaid
     * @param outstandingBalance
     * @param nextPaymentDate
     * @param paymentStatusCode
     * @param iRow
     * @param customerId
     * @return
     */
    int generateRow(final Sheet sheet, Applicant applicant, Quotation quotation, String applicantTypeCode, Double monthlyBasicIncom, Date productExpire,
                            Date dateIssued, Date lastPaymentDate, double lastAmountPaid, double outstandingBalance,
                            double pastDueAmount, Date nextPaymentDate, String productStatusCode, String paymentStatusCode, int iRow,
                            int customerId) {

        Employment employment = applicant.getIndividual().getCurrentEmployment();
        Address address = applicant.getIndividual().getMainAddress();

        String provinceCode = "";
        String districtCode = "";
        String communeCode = "";
        String villageCode = "";
        if (address != null) {
            CBCAddressCode cbcAddressCode = CBCWrongAddressCode.invalidCBCAddress(address);
            provinceCode = address.getProvince() != null ? address.getProvince().getCode() : "";
            districtCode = address.getDistrict() != null ? address.getDistrict().getCode() : "";
            communeCode = address.getCommune() != null ? address.getCommune().getCode() : "";
            villageCode = address.getVillage() != null ? address.getVillage().getCode() : "";

            if (address.getProvince() != null && address.getDistrict() != null && address.getCommune() != null
                    && address.getVillage() == null) {
                villageCode = address.getCommune() + "00";
            }

            if (cbcAddressCode != null) {
                provinceCode = cbcAddressCode.getProvinceCode();
                districtCode = cbcAddressCode.getDistrictCode();
                communeCode = cbcAddressCode.getCommuneCode();
            }

        } else {
            errRecs.add(quotation.getReference() + " - Applicant Type " + applicantTypeCode + "  Lose Address");
            return iRow;
        }

        if (!StringUtils.isNotEmpty(villageCode) || villageCode == null) {
            errRecs.add(quotation.getReference() + " - Applicant Type " + applicantTypeCode + "  Lose village Address");
            return iRow;
        }
        String accountNumber = quotation.getReference();
        String houseNo = "";
        String str = "";
        if (address != null) {
            if (address.getHouseNo() != null) {
                houseNo = address.getHouseNo();
            }
            if (address.getStreet() != null) {
                str = address.getStreet();
            }
        }
        int iCol = 0;
        Row tmpRow = sheet.createRow(iRow++);
        createCell(tmpRow, iCol++, (Integer.toString(iRow-1) ));
        CBCDocument cbcDocument=new DocumentValidate().getCustomerCBCDocument(quotation,applicantTypeCode);

        if(cbcDocument!=null){
            createCell(tmpRow, iCol++, cbcDocument.getDocumCode());
            createCell(tmpRow, iCol++, cbcDocument.getDocumReference());
            if (cbcDocument.getDocument()!=null && !cbcDocument.getDocument().isExpireDateRequired()) {
                createCell(tmpRow, iCol++, "");
            } else {
                createCell(tmpRow, iCol++, cbcDocument.getExpiredDate() != null ? DateUtils.formatDate(cbcDocument.getExpiredDate(), "ddMMyyyy") : "");
            }
        }else {
            createCell(tmpRow, iCol++, "");
            createCell(tmpRow, iCol++, "");
            createCell(tmpRow, iCol++, "");
            errRecs.add("Customer ID:  " + customerId + "Applicant Type: " + applicantTypeCode + "--LID Number:" + quotation.getReference() + "--No Document");
        }

        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");

        if (applicant != null && applicant.getIndividual().getBirthDate() != null || applicant.getIndividual().getBirthDate().toString() != "") {
            createCell(tmpRow, iCol++, DateUtils.formatDate(applicant.getIndividual().getBirthDate(), "ddMMyyyy"));
        } else {
            errRecs.add("Applicant Type: " + applicantTypeCode + "--LID Number:" + quotation.getReference() + "--No date of birth");
            errRecs.add("Customer ID:  " + customerId + "Applicant Type: " + applicantTypeCode + "--LID Number:" + quotation.getReference() + "--No date of birth");
            sheet.removeRow(tmpRow);
            return iRow - 1;
        }
        if (applicant != null) {
            if (applicant.getLastNameEn().equalsIgnoreCase("x") || applicant.getFirstNameEn().equalsIgnoreCase("x")
                    || applicant.getLastNameEn().equals("1") || applicant.getFirstNameEn().equals("1")) {
                errRecs.add("Customer ID:  " + customerId + quotation.getReference() + applicantTypeCode + " Name contains X or 1    " + sheet.getLastRowNum());
                sheet.removeRow(tmpRow);
                errRecs.add("After Edit" + sheet.getLastRowNum() + " iRow : " + iRow);
                return iRow - 1;
            }
        }
        createCell(tmpRow, iCol++, applicant != null && applicant.getLastNameEn() != null ? removeNonEngCharacters(applicant.getLastNameEn()) : "");
        createCell(tmpRow, iCol++, applicant != null && applicant.getFirstNameEn() != null ? removeNonEngCharacters(applicant.getFirstNameEn()) : "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, applicant.getLastName());
        createCell(tmpRow, iCol++, applicant.getFirstName());
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");

        if (applicant != null) {
            if (applicant.getIndividual().getGender() == null || applicant.getIndividual().getGender().getCode().equals("")
                    || applicant.getIndividual().getGender().getCode().equals( EGender.U.getCode())) {
                if (applicant.getIndividual().getCivility().getCode() .equals( ECivility.MR.getCode())) {
                    applicant.getIndividual().setGender(EGender.M);
                    applicant.getIndividual().setGender(EGender.M);
                } else if (applicant.getIndividual().getCivility().getCode() .equals(ECivility.MS.getCode())  || applicant.getIndividual().getCivility().getCode() .equals(ECivility.MRS.getCode())) {
                    applicant.getIndividual().setGender(EGender.F);
                }
            }
        }
        createCell(tmpRow, iCol++, applicant.getIndividual().getGender().getCode());
        if (applicant.getIndividual().getMaritalStatus() == null || applicant.getIndividual().getMaritalStatus().getCode().equals("")
                || applicant.getIndividual().getMaritalStatus().getCode().equals(EMaritalStatus.UNKNOWN.getCode())) {
            applicant.getIndividual().setMaritalStatus(EMaritalStatus.SINGLE);
        }
        createCell(tmpRow, iCol++, applicant.getIndividual().getMaritalStatus().getCode());
        createCell(tmpRow, iCol++, applicant.getIndividual().getNationality() != null ? applicant.getIndividual().getNationality().getCode() : "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, applicantTypeCode);
        createCell(tmpRow, iCol++, "RESID");

        createCell(tmpRow, iCol++, provinceCode);// Province
        createCell(tmpRow, iCol++, districtCode);// District
        createCell(tmpRow, iCol++, communeCode);// Commune
        createCell(tmpRow, iCol++, villageCode);// Village
		/*
		 * createCell(tmpRow, iCol++, address != null && address.getProvince()
		 * != null ? address.getProvince().getCode() : "");//Province
		 * createCell(tmpRow, iCol++, address != null && address.getDistrict()
		 * != null ? address.getDistrict().getCode() : "");//District
		 * createCell(tmpRow, iCol++, address != null && address.getCommune() !=
		 * null ? address.getCommune().getCode() : "");//Commune
		 * createCell(tmpRow, iCol++, address != null && address.getVillage() !=
		 * null ? address.getVillage().getCode() : "");//Village
		 */ /*
			 * createCell(tmpRow, iCol++, "#" + removeNonEngCharacters(houseNo)
			 * + ", " + removeNonEngCharacters(str) + ", "+
			 * removeNonEngCharacters(address.getVillage().getDescEn()) + ", " +
			 * removeNonEngCharacters(address.getCommune().getDescEn()) + ", " +
			 * removeNonEngCharacters(address.getDistrict().getDescEn()) + ", "+
			 * removeNonEngCharacters(address.getProvince().getDescEn()));
			 */
        createCell(tmpRow, iCol++,
                "#" + (removeNonEngCharacters(houseNo) != null ? removeNonEngCharacters(houseNo) : "") + ", "
                        + (removeNonEngCharacters(str) != null ? removeNonEngCharacters(str) : "") + ", "
                        + (address.getVillage() != null && address.getVillage().getDescEn() != null
                        ? removeNonEngCharacters(address.getVillage().getDescEn()) : "")
                        + ", "
                        + (address.getCommune() != null && address.getCommune().getDescEn() != null
                        ? removeNonEngCharacters(address.getCommune().getDescEn()) : "")
                        + ", "
                        + (address.getDistrict() != null && address.getDistrict().getDescEn() != null
                        ? removeNonEngCharacters(address.getDistrict().getDescEn()) : "") + ", "
                        + (address.getProvince() != null && address.getProvince().getDescEn() != null
                        ? removeNonEngCharacters(address.getProvince().getDescEn()) : ""));

        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "KHM");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");

        String phoneNumber = formatPhoneNumber(removeNonEngCharacters(applicant.getIndividual().getMobilePerso()));
        String phoneNumberType = "";
        String phoneNumberCountryCode = "";
        String phoneNumberAreaCode = "";

        if (StringUtils.isNotEmpty(phoneNumber)) {
            phoneNumberCountryCode = "855";
            String areaCode = phoneNumber.substring(0, 3);
            if (AREA_CODE_FIX.contains(areaCode)) {
                phoneNumber = phoneNumber.substring(3);
                if (phoneNumber.length() == 6) {
                    phoneNumberType = "O";
                    phoneNumberAreaCode = areaCode;
                } else {
                    phoneNumberCountryCode = "";
                    areaCode = "";
                    phoneNumber = "";
                }
            } else {
                if (phoneNumber.indexOf("1") == 3 || phoneNumber.length() < 9 || phoneNumber.length() > 10) {
                    phoneNumber = "";
                    phoneNumberType = "";
                    phoneNumberCountryCode = "";
                    errRecs.add(quotation.getReference() + "-applicant type: " + applicantTypeCode + "- phone number contain 1 at index 3");
                } else {
                    phoneNumberType = "M";
                }

            }
        }

        createCell(tmpRow, iCol++, phoneNumberType);
        createCell(tmpRow, iCol++, phoneNumberCountryCode);
        createCell(tmpRow, iCol++, phoneNumberAreaCode);
        createCell(tmpRow, iCol++, phoneNumber);
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        // createCell(tmpRow, iCol++,
        // formatOccupationEng(removeNonEngCharacters(employment.getPosition())));

        createCell(tmpRow, iCol++, formatOccupationEng(employment != null && employment.getPosition() != null
                ? removeNonEngCharacters(employment.getPosition()).trim() : ""));
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "USD");

        /**
         * To validate monthlyBasicIncom :not a minus number
         */
        if (monthlyBasicIncom < 0) {
            monthlyBasicIncom = monthlyBasicIncom * (-1);
        }
        createNumericCell(tmpRow, iCol++, MyMathUtils.roundAmountTo(monthlyBasicIncom));
        createNumericCell(tmpRow, iCol++, MyMathUtils.roundAmountTo(monthlyBasicIncom));
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, "330");
        createCell(tmpRow, iCol++, "S");
        createCell(tmpRow, iCol++, "");
        createCell(tmpRow, iCol++, accountNumber);

        createCell(tmpRow, iCol++, (dateIssued != null ? DateUtils.formatDate(dateIssued, "ddMMyyyy") : ""));
        createCell(tmpRow, iCol++, "MTL");
        createCell(tmpRow, iCol++, "USD");

        createNumericCell(tmpRow, iCol++, MyMathUtils.roundAmountTo(MyNumberUtils.getDouble(quotation.getTiFinanceAmount())));
        if (productExpire != null) {
            if (DateUtils.today().after(productExpire)) {
                productExpire = DateUtils.today();
                createCell(tmpRow, iCol++, DateUtils.formatDate(productExpire, "ddMMyyyy"));
            } else if (DateUtils.today().before(productExpire)) {
                createCell(tmpRow, iCol++, DateUtils.formatDate(productExpire, "ddMMyyyy"));
            }
        } else {
            createCell(tmpRow, iCol++, "");
        }

        // createCell(tmpRow, iCol++, productExpire != null ?
        // DateUtils.formatDate(productExpire,"ddMMyyyy") : "");
        createCell(tmpRow, iCol++, productStatusCode);
        createCell(tmpRow, iCol++, "N");
        createNumericCell(tmpRow, iCol++, MyMathUtils.roundAmountTo(quotation.getTiInstallmentAmount()));
        createCell(tmpRow, iCol++, "M");
        createNumericCell(tmpRow, iCol++, quotation.getTerm());

        /**
         * To validate lastPaymentDate: LastPaymentDate must greater than
         * AsOfDate
         */

        if (lastPaymentDate != null) {

            if (DateUtils.today().compareTo(lastPaymentDate) < 0) {
                lastPaymentDate = DateUtils.today();
            }
        } else {

        }

        createCell(tmpRow, iCol++, lastPaymentDate != null ? (DateUtils.formatDate(lastPaymentDate, "ddMMyyyy")) : "");

        createNumericCell(tmpRow, iCol++, MyMathUtils.roundAmountTo(lastAmountPaid));
        createCell(tmpRow, iCol++, "MV");

        /**
         * To validate outstandingBalance: outstandingBalance must be equal or
         * less than Loan Amount
         */
        if (outstandingBalance > quotation.getTiFinanceAmount()) {
            outstandingBalance = quotation.getTiFinanceAmount();
        }

        createNumericCell(tmpRow, iCol++, MyMathUtils.roundAmountTo(outstandingBalance));
        createNumericCell(tmpRow, iCol++, MyMathUtils.roundAmountTo(pastDueAmount));

        /**
         * To validate NextPaymentDate: NextPaymentDate must be greater than
         * AsOfDate
         */
        if (nextPaymentDate != null) {
            // System.out.println("Orignal NextPayment Date:
            // "+DateUtils.formatDate(nextPaymentDate, "ddMMyyyy"));
            if (DateUtils.today().after(nextPaymentDate)) {
                nextPaymentDate = DateUtils.today();
                createCell(tmpRow, iCol++, DateUtils.formatDate(nextPaymentDate, "ddMMyyyy"));
            } else if (DateUtils.today().before(nextPaymentDate)) {
                createCell(tmpRow, iCol++, DateUtils.formatDate(nextPaymentDate, "ddMMyyyy"));
            }
        } else {
            createCell(tmpRow, iCol++, "");
        }
        createCell(tmpRow, iCol++, paymentStatusCode);
        createCell(tmpRow, iCol++, DateUtils.formatDate(DateUtils.today(), "ddMMyyyy"));

        String writeOffStatus = "";
        String writeOffStatusDate = "";
        String writeOffOriginalAmount = "";
        String writeOffOustandingBalance = "";
        if ("W".equals(paymentStatusCode)) {
            writeOffStatus = "OS";
            writeOffStatusDate = DateUtils.formatDate(DateUtils.today(), "ddMMyyyy");
            writeOffOriginalAmount = String.valueOf(MyMathUtils.roundAmountTo(outstandingBalance));
            writeOffOustandingBalance = String.valueOf(MyMathUtils.roundAmountTo(outstandingBalance));
        }

        createCell(tmpRow, iCol++, writeOffStatus);
        createCell(tmpRow, iCol++, writeOffStatusDate);
        createCell(tmpRow, iCol++, writeOffOriginalAmount);
        createCell(tmpRow, iCol++, writeOffOustandingBalance);
        return iRow;
    }
}
