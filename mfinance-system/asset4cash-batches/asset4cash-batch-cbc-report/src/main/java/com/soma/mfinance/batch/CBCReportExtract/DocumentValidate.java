package com.soma.mfinance.batch.CBCReportExtract;

import com.google.common.collect.Lists;
import com.soma.mfinance.core.applicant.model.EApplicantType;
import com.soma.mfinance.core.document.model.Document;
import com.soma.mfinance.core.quotation.model.Quotation;
import com.soma.mfinance.core.quotation.model.QuotationDocument;
import org.apache.commons.lang.StringUtils;
import org.seuksa.frmk.model.eref.BaseERefData;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DocumentValidate {
//    /**
//     * @param documents
//     * @param nationalNumber
//     * @param EApplicantType
//     */
//    static QuotationDocument checkNationalID(List<QuotationDocument> documents, int nationalNumber, String EApplicantType, QuotationDocument quotationDocument) {
//
//        // nationalNumber is used for recognize ID of reference (in database
//        // tu_document --> column id)
//        // QuotationDocument quotationDocument=null;
//        if (documents != null && !documents.isEmpty()) {
//
//            String tempIDNumber = null;
//            String temOtherDoc = null;
//            Collections.sort(documents, new DocumentValidate.DocumentComparatorBySortIndex());
//            QuotationDocument quotationDocumentOther = null;
//
//            for (QuotationDocument document : documents) {
//                if (document.getDocument().getId() == nationalNumber && document.getDocument().isSubmitCreditBureau()) {
//                    try {
//                        quotationDocument = document;
//                        tempIDNumber = CBCValidation.deleteSpecailCharater(document.getReference());
//                        if (tempIDNumber == null) {
//                            tempIDNumber = "";
//                        }
//                    } catch (NullPointerException ex) {
//
//                    }
//                } else if (document.getDocument().getId() != nationalNumber
//                        && document.getDocument().isSubmitCreditBureau() && document.getReference() != null) {
//
//                    if ((temOtherDoc == null || temOtherDoc.isEmpty() || temOtherDoc == ""
//                            || temOtherDoc.equalsIgnoreCase("N/A") || temOtherDoc.equalsIgnoreCase("NA")
//                            || quotationDocumentOther == null)) {
//                        try {
//                            quotationDocumentOther = document;
//                            temOtherDoc = document.getReference();
//                        } catch (NullPointerException e) {
//
//                        }
//
//                    }
//                }
//
//            }
//            try {
//                if (tempIDNumber == "" || tempIDNumber.isEmpty() || tempIDNumber == null
//                        || tempIDNumber.equalsIgnoreCase("N/A") || tempIDNumber.equalsIgnoreCase("NA")) {
//
//                    try {
//                        quotationDocument = quotationDocumentOther;
//                    } catch (NullPointerException e) {
//                    }
//                }
//            } catch (NullPointerException e) {
//            }
//        }
//
//        return quotationDocument;
//    }

    /**
     * @author kimsuor.seang
     */
    static class DocumentComparatorBySortIndex implements Comparator<Object> {
        @Override
        public int compare(Object o1, Object o2) {
            QuotationDocument c1 = (QuotationDocument) o1;
            QuotationDocument c2 = (QuotationDocument) o2;
            if (c1 == null || c1.getDocument().getSortIndex() == null) {
                if (c2 == null || c2.getDocument().getSortIndex() == null) {
                    return 0;
                }
                return 1;
            }
            if (c2 == null || c2.getDocument().getSortIndex() == null) {
                return -1;
            }
            return c1.getDocument().getSortIndex().compareTo(c2.getDocument().getSortIndex());
        }
    }

//    /**
//     * @param quotationDocument
//     * @param applicantTypeCode
//     * @param IDNumber
//     * @param epdat_nat
//     * @param quotation
//     * @return
//     * @author p.leap
//     */
//
//    static NationNalID chkifCaseNoIDNumber(QuotationDocument quotationDocument, String applicantTypeCode, String IDNumber, Date epdat_nat, Quotation quotation) {
//
//        if (quotationDocument != null && quotationDocument.getReference() != null) {
//            String TempIDNumber = null;
//            int natNum = 0;
//            int restbokNum = 0;
//            int fambokNum = 0;
//            if ((quotationDocument.getDocument() != null && quotationDocument.getDocument().getCode().equals("N") && applicantTypeCode.equals("P"))
//                    || (quotationDocument.getDocument() != null && quotationDocument.getDocument().getCode().equals("N") && applicantTypeCode.equals("G"))) {
//                TempIDNumber = CBCValidation.deleteSpecailCharater(quotationDocument.getReference()).trim();
//                int getDot = TempIDNumber.indexOf(".");
//                if (getDot >= 0) {
//                    IDNumber = TempIDNumber.replace(".", "");
//                    TempIDNumber = IDNumber;
//                }
//
//            } else {
//                TempIDNumber = deletSpecailCharater(quotationDocument.getReference().trim());
//            }
//            if (TempIDNumber == null) {
//                TempIDNumber = "N/A";
//            }
//
//            int getSpect = TempIDNumber.indexOf(" ");
//
//            epdat_nat = quotationDocument.getExpireDate();
//
//            if (getSpect >= 0) {
//                IDNumber = TempIDNumber.replace(" ", "");
//            } else {
//                IDNumber = TempIDNumber;
//            }
//
//            if (applicantTypeCode == "P") {
//                natNum = 1;
//                restbokNum = 5;
//                fambokNum = 3;
//
//            } else {
//                natNum = 10;
//                restbokNum = 16;
//                fambokNum = 12;
//            }
//
//            if (((IDNumber.length() < 9 || IDNumber.length() > 9)
//                    && (quotationDocument.getDocument().getId() == 1 || quotationDocument.getDocument().getId() == 10))
//                    ||
//                    !(quotationDocument.getDocument().getId() == 1 || quotationDocument.getDocument().getId() == 10)
//                    ) {
//
//                IDNumber = getOtherRefDoc(quotation, applicantTypeCode, natNum, restbokNum, fambokNum).IDNumber;
//                epdat_nat = getOtherRefDoc(quotation, applicantTypeCode, natNum, restbokNum, fambokNum).epdat_nat;
//            }
//
//        }
//        return new NationNalID(IDNumber, epdat_nat);
//    }

    /**Delete Special Charater beside National ID Number
     * @param temRefereneNumber
     * @return
     */
    public static String deletSpecailCharater(String temRefereneNumber) {
        String refereneNumber = null;
        Pattern regex = Pattern.compile("[;''!@#+$%^&*_-]++");
        Matcher matcher = regex.matcher(temRefereneNumber);

        if (matcher.find()) {
            refereneNumber = temRefereneNumber.replaceAll("[;''!@#+$%^&*_-]+", "");
        } else {
            refereneNumber = temRefereneNumber;
        }
        return refereneNumber;
    }

//    /**
//     * This method is used for getting other reference (Priority is family book
//     * or resident book)
//     *
//     * @param quotation
//     * @param applicantTypeCode
//     * @param natNum
//     * @param restbokNum
//     * @param fambokNum
//     * @return
//     * @author p.leap
//     */
//
//    static NationNalID getOtherRefDoc(Quotation quotation, String applicantTypeCode, int natNum, int restbokNum, int fambokNum) {
//		/*
//		 * natNum, restbokNum, fambokNum: to recognize the ID number of document
//		 * reference in table tu_document
//		 */
//        String other = "";
//        Date epdat_ot = null;
//        String restbok = "";
//        Date epdat_rb = null;
//        String fambok = "";
//        Date epdat_fb = null;
//        String IDNumber = "";
//        Date epdat_nat = null;
//
//        for (QuotationDocument doc : quotation.getQuotationDocuments()) {
//            if (applicantTypeCode.equalsIgnoreCase("P") && doc.getDocument().getApplicantType() != EApplicantType.C) {
//                continue;
//            }
//            if (applicantTypeCode.equalsIgnoreCase("G") && doc.getDocument().getApplicantType() != EApplicantType.G) {
//                continue;
//            }
//            if (doc.getDocument().isSubmitCreditBureau()) {
//
//                if (doc.getDocument().getId() != natNum && doc.getDocument().getId() != restbokNum
//                        && doc.getDocument().getId() != fambokNum && (other == "" || other.isEmpty() || other == null
//                        || other.equalsIgnoreCase("N/A") || other.equalsIgnoreCase("NA"))) {
//
//                    other = doc.getReference().trim();
//                    epdat_ot = doc.getExpireDate();
//                } else if (doc.getDocument().getId() == restbokNum) {
//
//                    restbok = doc.getReference().trim();
//                    epdat_rb = doc.getExpireDate();
//
//                } else if (doc.getDocument().getId() == fambokNum) {
//                    fambok = doc.getReference().trim();
//                    epdat_fb = doc.getExpireDate();
//
//                } else if (restbok != "" && fambok != "" && (other != null && other != "" && !other.isEmpty()
//                        && !other.equalsIgnoreCase("N/A") && !other.equalsIgnoreCase("NA"))) {
//                    break;
//
//                }
//
//            }
//
//        }
//        if (fambok != null && fambok != "" && !fambok.equalsIgnoreCase("N/A") && !fambok.equalsIgnoreCase("NA")) {
//            IDNumber = deletSpecailCharater(fambok);
//            epdat_nat = epdat_fb;
//        } else if (restbok != null && restbok != "" && !restbok.equalsIgnoreCase("N/A") && !restbok.equalsIgnoreCase("NA")) {
//            IDNumber = deletSpecailCharater(restbok);
//            epdat_nat = epdat_rb;
//        } else {
//
//            IDNumber = deletSpecailCharater(other);
//            epdat_nat = epdat_ot;
//        }
//        int getSpect = IDNumber.indexOf(" ");
//
//        if (getSpect >= 0) {
//            String TempIDNumber = IDNumber.replace(" ", "");
//            IDNumber = TempIDNumber;
//        }
//
//        return new NationNalID(IDNumber, epdat_nat);
//
//    }

    /**
     * @param value
     * @return
     */

    /**
     * @param quotation
     * @param applicantCode
     * @return
     */
    public static List<CBCDocument> getCustomerDocuments(Quotation quotation, String applicantCode) {
        List<CBCDocument> cbcDocuments = new ArrayList<CBCDocument>();
        EApplicantType applicantType = EApplicantType.C;
        if ("G".equals(applicantCode)) {
            applicantType = EApplicantType.G;
        }
        if (quotation != null) {
            List<QuotationDocument> quotationDocuments = quotation.getQuotationDocuments();
            if (quotationDocuments != null && !quotationDocuments.isEmpty()) {
                for (QuotationDocument quotationDocument : quotationDocuments) {
                    Document document = quotationDocument.getDocument();
                    CBCDocument cbcDocument = new CBCDocument();
                    if (document != null && document.isSubmitCreditBureau()&& (document.getApplicantType().equals(applicantType))) {
                        if (StringUtils.isNotEmpty(quotationDocument.getReference())) {
                            cbcDocument.setDocument(quotationDocument.getDocument());
                            cbcDocument.setDocumCode(quotationDocument.getDocument().getCode());
                            cbcDocument.setDocumReference(quotationDocument.getReference());
                            cbcDocument.setExpiredDate(quotationDocument.getExpireDate());
                            cbcDocuments.add(cbcDocument);
                        }
                    }
                }
            }
        }
        //Order By doc_cbc_prio ==> indext of cbcDocmuments' items depend on doc_cbc_prio
        cbcDocuments.sort((cbcDoc1, cbcDoc2) -> cbcDoc1.getDocument().getCbcUploadPriority().compareTo(cbcDoc2.getDocument().getCbcUploadPriority()));
        return cbcDocuments;
    }

    /**
     * @param
     * @return
     */
    static boolean isCBCDocumentCode(String cbcDocCode) {
        boolean isDocCBCCode = false;
        List<String> cbcAllowDocCodes = new ArrayList<String>();
        cbcAllowDocCodes.add("N");
        cbcAllowDocCodes.add("F");
        cbcAllowDocCodes.add("P");
        cbcAllowDocCodes.add("D");
        cbcAllowDocCodes.add("D");
        cbcAllowDocCodes.add("B");
        cbcAllowDocCodes.add("V");
        cbcAllowDocCodes.add("T");
        cbcAllowDocCodes.add("N");
        if (cbcAllowDocCodes != null && !cbcAllowDocCodes.isEmpty() && cbcAllowDocCodes.contains(cbcDocCode)) {
            isDocCBCCode = true;
        }
        return isDocCBCCode;
    }


    /**
     * @param isExpireDatte
     * @return
     */
    static boolean isExpireDateRequired(boolean isExpireDatte) {
        return isExpireDatte;
    }

    /**
     * @param idNum
     * @return
     */
    static String formatIdentity(String idNum) {
        String IDNumber = "";
        if (StringUtils.isNotEmpty(idNum)) {
            String TempIDNumber = idNum;
            int getSpect = TempIDNumber.indexOf(" ");
            int getDot = TempIDNumber.indexOf(".");
            IDNumber = TempIDNumber;
            if (getSpect >= 0) {
                IDNumber = TempIDNumber.replace(" ", "");
            }
            if (getDot >= 0) {
                IDNumber = IDNumber.replace(".", "");
            }
        }
        return IDNumber;
    }

    private String deleteSpace(String referenceNum){
        int getSpect = referenceNum.indexOf(' ');
        if (getSpect >= 0) {
            referenceNum = referenceNum.replace(" ", "");
        }

        return referenceNum;
    }

    private String deleteDot(String referenceNum){
        int getDot = referenceNum.indexOf('.');
        if (getDot >= 0) {
            referenceNum = referenceNum.replace(".", "");
        }

        return referenceNum;
    }

    private boolean isMeetNationalIDRequired(int length){
        final int REQUIRED_LENGTH=9;
        if (length==REQUIRED_LENGTH ){
            return true;
        }
        return false;
    }

    private boolean isStringNotNullAndEmpty(String referenceNumber) {
        if (referenceNumber != null && StringUtils.isNotEmpty(referenceNumber)) {
            return true;
        }

        return false;
    }

    private String validateNationID(String nationIdNum){
        nationIdNum=CBCValidation.deleteSpecailCharater(nationIdNum).trim();
        nationIdNum=deleteSpace(nationIdNum);
        nationIdNum=deleteDot(nationIdNum);

        if(isMeetNationalIDRequired(nationIdNum.length())){
            return nationIdNum;
        }
        return null;
    }

    private boolean isNotAllowWords(String referenceBesideNationalNum){
        ArrayList<String> notAllowWords = Lists.newArrayList("N/A","NA","N\\A");
        if(notAllowWords.contains(referenceBesideNationalNum.toUpperCase())){
            return true;
        }
        return false;
    }
    /***
     * Validate other reference Number when national id number don't meet the requirement
     * @return
     */
    private String validateReferenceNumber(String referenceBesideNationalNum){
        referenceBesideNationalNum = deletSpecailCharater(referenceBesideNationalNum.trim());
        referenceBesideNationalNum=deleteSpace(referenceBesideNationalNum);
        if(!isNotAllowWords(referenceBesideNationalNum)){
            return referenceBesideNationalNum;
        }

        return null;
    }

    public CBCDocument getCustomerCBCDocument(Quotation quotation, String applicantTypeCode){
        List<CBCDocument>cbcDocuments=getCustomerDocuments(quotation,applicantTypeCode);
        String tempIDNumber = null;
        for(CBCDocument cbcDocument:cbcDocuments){
            if(cbcDocument!=null && isStringNotNullAndEmpty(cbcDocument.getDocumReference())){
               if(cbcDocument.getDocumCode().equals(EPriorityDoc.NATION_ID.getCode())){
                  tempIDNumber=validateNationID(cbcDocument.getDocumReference());
                  if(tempIDNumber!=null){
                      cbcDocument.setDocumReference(tempIDNumber);
                      return cbcDocument;
                  }

               }else{
                  tempIDNumber=validateReferenceNumber(cbcDocument.getDocumReference());
                   if(tempIDNumber!=null){
                       cbcDocument.setDocumReference(tempIDNumber);
                       return cbcDocument;
                   }
               }

            }

        }
        return null;
    }
}

class EPriorityDoc extends BaseERefData {
    public final static EPriorityDoc NATION_ID=new EPriorityDoc("N",1);
    public final static EPriorityDoc FAMILY_BOOK=new EPriorityDoc("F",2);
    public final static EPriorityDoc RESIDENT_BOOK=new EPriorityDoc("R",3);
    public final static EPriorityDoc PASSPORT=new EPriorityDoc("P",4);
    public final static EPriorityDoc DRIVING_LICENCE=new EPriorityDoc("D",5);
    public final static EPriorityDoc BIRTH_CERTIFICATE=new EPriorityDoc("B",6);
    public final static EPriorityDoc GOVERNMENT=new EPriorityDoc("G",7);

    public EPriorityDoc() {
    }

    public EPriorityDoc(String code, long id) {
        super(code, id);
    }
}
