package com.soma.mfinance.batch.CBCReportExtract;

import org.apache.commons.lang.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CBCValidation {

    /**
     * To validate Number in wrong format ( it contains (01), (1), 8 digits, 10
     * digits, 4 digits)
     */
    static String deleteSpecailCharater(String tempIdNum) {

        String nationalId = null;

        Pattern parences = Pattern.compile("(.+?)");
        Matcher matcherParences = parences.matcher(tempIdNum);

        if (matcherParences.find()) {
            nationalId = tempIdNum.replaceAll("\\(.*?\\) ?", "");
        }

        Pattern regex = Pattern.compile("[&%$#@!~^//\\(\\)]++");
        Matcher matcher = regex.matcher(tempIdNum);

        if (matcher.find() && nationalId!=null) {
            nationalId = nationalId.replaceAll("[&%$#@!~//^\\(\\)]+", "");
        } else {
            nationalId = tempIdNum;
        }

        return nationalId;
    }

    /**
     * @param occupation
     * @return
     */
    static String formatOccupationEng(String occupation) {
        if (occupation != null) {
            occupation = occupation.replaceAll("[/&]", " ");
        }
        return occupation;
    }

    /**
     * @param phoneNumber
     * @return
     */
    static String formatPhoneNumber(String phoneNumber) {
        String phoneNumberFormatted = "";
        if (phoneNumber != null) {
            phoneNumber = phoneNumber.replaceAll(" ", "");
            if (phoneNumber.length() > 8) {
                phoneNumberFormatted = phoneNumber;
                if (!phoneNumberFormatted.startsWith("0")) {
                    phoneNumberFormatted = "0" + phoneNumberFormatted;
                }
                if (phoneNumberFormatted.length() > 10) {
                    phoneNumberFormatted = "";
                } else if (phoneNumberFormatted.startsWith("012") && phoneNumberFormatted.length() >= 10) {
                    phoneNumberFormatted = "";
                }

            }
        }
        return phoneNumberFormatted;
    }


    /**
     * @param value
     * @return
     */
    static String removeNonEngCharacters(String value) {
        if (StringUtils.isNotEmpty(value)) {
            return value.replaceAll("[^\\p{ASCII}]", "");
        }
        return "";
    }
}

