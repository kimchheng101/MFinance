package com.soma.mfinance.batch;

import com.soma.mfinance.batch.CBCReportExtract.CBCReportExtraction;
import com.soma.mfinance.core.contract.model.Contract;
import com.soma.mfinance.core.workflow.ContractWkfStatus;
import com.soma.mfinance.core.workflow.model.history.EProcessType;
import com.soma.mfinance.core.workflow.model.history.HistoryProcess;
import com.soma.frmk.helper.SeuksaServicesHelper;
import com.soma.frmk.testing.BaseTestCase;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.junit.Test;
import org.seuksa.frmk.dao.hql.BaseRestrictions;

import java.util.List;

import static com.soma.mfinance.core.shared.FMEntityField.CONTRACT_STATUS;

public class TestCBC extends BaseTestCase implements SeuksaServicesHelper {

	@Override
	protected void authenticate() {
		super.authenticate("admin","11");
	}

	@Override
	public void setRequiredAuhentication(boolean requiredAuhentication) {
		super.setRequiredAuhentication(false);
	}

	@Test
	public void test() throws Exception {
		/*String[] array=new String[]{};
		BatchLauncher.main(array);*/
		//getContract4Upload();
		/*Quotation quotation = ENTITY_SRV.getById(Quotation.class,1l);

		System.out.println("Quotation ID : " + quotation.getId());*/
		new CBCReportExtraction().generate(null);
		//getContractCBC4Upload(83L);
		//new BatchSession().init();
		//hello();
		//getCustomerDocuments(ENTITY_SRV.getById(Contract.class,83).getQuotation(),"C");
		//new DocumentValidate().getCustomerCBCDocument(ENTITY_SRV.getById(Contract.class,83).getQuotation(),"C");

		//deletSpecailCharater("dfasdfasdf*****^^^^$$$!/");
	}

	public void getContract4Upload(){
		BaseRestrictions<Contract> restrictions = new BaseRestrictions<>(Contract.class);
		restrictions.addCriterion(Restrictions.ne(CONTRACT_STATUS, ContractWkfStatus.WTD));
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(HistoryProcess.class, "hisProc");
		detachedCriteria.createAlias("hisProc.contractWkfHistoryItem", "conHisItm", JoinType.INNER_JOIN);
		detachedCriteria.add(Restrictions.eq("processType", EProcessType.CB));
		detachedCriteria.setProjection(Projections.projectionList().add(Projections.property("conHisItm.entityId")));
		restrictions.addCriterion(Property.forName("id").notIn(detachedCriteria));
		//restrictions.setMaxResults(4);
		List<Contract> contracts = ENTITY_SRV.list(restrictions);
		int nbRows = contracts.size();
	}


	public void hello(){
		org.seuksa.frmk.i18n.I18N.message("transfer.amount");
	}
}
